--CONSULTA DOS CUPONS EMITIDOS BY DAY
-- 20/11/2018


--------------------------------------------VARIAVEIS DE PRODUCAO-------------------------------------------------------------

DECLARE @DATA_INI    DATE    = :DATA_INI    
DECLARE @DATA_FIM    DATE    = :DATA_FIM          
DECLARE @EMPRESA_INI NUMERIC    
DECLARE @EMPRESA_FIM NUMERIC    
IF ISNUMERIC(:EMPRESA_INI) = 1 SET @EMPRESA_INI = :EMPRESA_INI ELSE SET @EMPRESA_INI = NULL
IF ISNUMERIC(:EMPRESA_FIM) = 1 SET @EMPRESA_FIM = :EMPRESA_FIM ELSE SET @EMPRESA_FIM = NULL        

-------------------------------------------VARIAVEIS DE TESTE----------------------------------------------------------------

--DECLARE @EMPRESA_INI NUMERIC    
--DECLARE @EMPRESA_FIM NUMERIC          
--DECLARE @DATA_INI    DATE = '19/11/2018'
--DECLARE @DATA_FIM    DATE = '19/11/2018'



            
SELECT 
'Per�odo de '+CONVERT(VARCHAR, @DATA_INI, 103 )+' a '+ CONVERT (VARCHAR, @DATA_FIM, 103)          AS PERIODO, 
A.EMPRESA                                                                                         AS EMPRESA,
A.MOVIMENTO,
CAST (  (CASE WHEN A.VENDA_BRUTA   >  0 THEN A.VENDA_BRUTA         ELSE 0 END)       AS MONEY ) AS VENDA_BRUTA,
CAST (  (A.DEVOLUCAO*-1)                                                             AS MONEY ) AS DEVOLUCAO,
CAST (  (A.CANCELAMENTO*-1)                                                          AS MONEY ) AS CANCELAMENTO,
CAST (  (A.TOTAL_DEVOLUCAO*-1)                                                       AS MONEY ) AS TOTAL_DEVOLUCAO,
CAST (  (A.DESCONTO)                                                                 AS MONEY ) AS DESCONTO,
CAST (  (A.VENDA_LIQUIDA)                                                            AS MONEY ) AS VENDA_LIQUIDA,
A.VENDA_LIQUIDA   -   (CASE WHEN A.CLIENTE <=1000 THEN A.VENDA_LIQUIDA ELSE 0 END)  
                                                                                    AS VENDA_LIQUIDA_GERENCIAL,
CAST (  ( ISNULL(B.DINHEIRO,0)      )                                                AS MONEY ) AS DINHEIRO,
CAST (  ( ISNULL(B.CHEQUE,0)        )                                                AS MONEY ) AS CHEQUE,
CAST (  ( ISNULL(B.TEF_CREDITO,0)   )                                                AS MONEY ) +
CAST (  ( ISNULL(B.POS_CREDITO,0)   )                                                AS MONEY ) AS CARTAO_CREDITO,
CAST (  ( ISNULL(B.POS_DEBITO,0)    )                                                AS MONEY ) +
CAST (  ( ISNULL(B.TEF_DEBITO,0)    )                                                AS MONEY ) AS CARTAO_DEBITO,
CAST (  ( ISNULL(B.FATURADO,0)      )                                                           +
        ( ISNULL(B.CARTAO_PROPRIO,0))                                                           +
        ( CASE WHEN A.ESPECIE_FISCAL <> 'PDV' THEN A.VENDA_LIQUIDA ELSE 0 END)                  +
       -- DIFERENCA COLOCADA NO FATURADO --
          (A.VENDA_LIQUIDA)
         -(
           ( ISNULL(B.DINHEIRO,0)      )                                                
         + ( ISNULL(B.CHEQUE,0)        )                                                
         + ( ISNULL(B.TEF_CREDITO,0)   )                                                
         + ( ISNULL(B.POS_CREDITO,0)   )                                                
         + ( ISNULL(B.POS_DEBITO,0)    )                                                
         + ( ISNULL(B.TEF_DEBITO,0)    )                                                
         + ( ISNULL(B.FATURADO,0)      )                                                  
         + ( ISNULL(B.CARTAO_PROPRIO,0))                                                  
         + ( CASE WHEN A.ESPECIE_FISCAL <> 'PDV' THEN A.VENDA_LIQUIDA ELSE 0 END)
         + ( ISNULL(B.CONVENIO,0)      )                                                
         + (ISNULL(B.VALE_CREDITO,0)   )
          )
         -------------------------------------
                                                                                       AS MONEY ) AS FATURADO,
CAST (  ( ISNULL(B.CONVENIO,0)      )                                                  AS MONEY ) AS CONVENIO,
CAST (  (ISNULL(B.VALE_CREDITO,0)   )                                                  AS MONEY ) AS VALE_CREDITO,
CAST (  ( CASE WHEN A.CLIENTE <=1000 THEN A.VENDA_LIQUIDA ELSE 0 END) AS MONEY ) 
                                                                                                  AS FATURADO_INTRAGRUPO


  FROM      (
                  SELECT 
                  A.ESPECIE_FISCAL,
                  A.DOCUMENTO_NUMERO,
                  A.VENDA,
                  A.CAIXA,
                  A.EMPRESA,
                  A.MOVIMENTO,
                  A.CLIENTE,
                   SUM(A.QUANTIDADE   )                                                 AS QUANTIDADE    ,
                   SUM(A.VENDA_BRUTA  )                                                 AS VENDA_BRUTA   ,
                   SUM(A.DESCONTO     )                                                 AS DESCONTO      ,
                   
                   SUM( CASE WHEN A.VENDA_LIQUIDA < 0 AND A.FORMULARIO_ORIGEM IN(561664,0) --CANCELAMENTO DE NOTA FISCAL
                       THEN A.VENDA_LIQUIDA ELSE 0 END )                               AS CANCELAMENTO  ,
                   
                   SUM( CASE WHEN A.VENDA_LIQUIDA < 0 AND A.FORMULARIO_ORIGEM NOT IN(561664,0) --DEVOLU�AO DE VENDA
                       THEN A.VENDA_LIQUIDA ELSE 0 END )                               AS DEVOLUCAO     ,
                   
                   SUM(CASE WHEN A.VENDA_LIQUIDA < 0 THEN A.VENDA_LIQUIDA ELSE 0 END)   AS TOTAL_DEVOLUCAO,
                   
                   SUM(A.VENDA_LIQUIDA)                                                 AS VENDA_LIQUIDA
                  
                       FROM      VENDAS_ANALITICAS         A WITH(NOLOCK)
                  
                        WHERE 1=1
                        AND ( A.MOVIMENTO   >= @DATA_INI    OR @DATA_INI    IS NULL )
                        AND ( A.MOVIMENTO   <= @DATA_FIM    OR @DATA_FIM    IS NULL )
                        AND ( A.EMPRESA     >= @EMPRESA_INI OR @EMPRESA_INI IS NULL )
                        AND ( A.EMPRESA     <= @EMPRESA_FIM OR @EMPRESA_FIM IS NULL )  
                  
                   GROUP BY
                   A.ESPECIE_FISCAL,
                   A.DOCUMENTO_NUMERO,
                   A.VENDA,
                   A.CAIXA,
                   A.EMPRESA,
                   A.MOVIMENTO,
                   A.CLIENTE
            )                         A 
  LEFT JOIN PDV_FINALIZADORAS_COLUNAS B WITH(NOLOCK) ON A.ESPECIE_FISCAL  = 'PDV'
                                                    AND B.VENDA           = A.VENDA
                                                    AND B.CAIXA           = A.CAIXA
                                                    AND B.LOJA            = A.EMPRESA
                                                    AND B.MOVIMENTO       = A.MOVIMENTO
  INNER JOIN EMPRESAS_USUARIAS        C WITH(NOLOCK) ON C.EMPRESA_USUARIA = A.EMPRESA

              WHERE 1=1
              AND ( A.MOVIMENTO   >= @DATA_INI    OR @DATA_INI    IS NULL )
              AND ( A.MOVIMENTO   <= @DATA_FIM    OR @DATA_FIM    IS NULL )
              AND ( A.EMPRESA     >= @EMPRESA_INI OR @EMPRESA_INI IS NULL )
              AND ( A.EMPRESA     <= @EMPRESA_FIM OR @EMPRESA_FIM IS NULL )  

ORDER BY A.EMPRESA,A.MOVIMENTO