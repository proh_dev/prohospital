DECLARE @NF_FATURAMENTO		    NUMERIC(15) 
DECLARE @ENTIDADE		        NUMERIC(15)
DECLARE @FORMULARIO_ORIGEM_NOTA NUMERIC(15)
DECLARE @TAB_MASTER_ORIGEM_NOTA NUMERIC(15)
DECLARE @PEDIDO_VENDA		    NUMERIC(15)
DECLARE @REG_MASTER_ORIGEM      NUMERIC(15) 
DECLARE @EMPRESA_VALIDACAO      NUMERIC(15)
DECLARE @TAB_MASTER_ORIGEM      NUMERIC(15)

SET @NF_FATURAMENTO             = :NF_FATURAMENTO
SET @ENTIDADE                   = :ENTIDADE
SET @FORMULARIO_ORIGEM_NOTA     = :FORMULARIO_ORIGEM
SET @TAB_MASTER_ORIGEM_NOTA     = :TAB_MASTER_ORIGEM
SET @PEDIDO_VENDA               = :PEDIDO_VENDA
SET @REG_MASTER_ORIGEM          = :NF_FATURAMENTO
SET @EMPRESA_VALIDACAO          = :NF_FATURAMENTO
SET @TAB_MASTER_ORIGEM          = :TAB_MASTER_ORIGEM






-- ATUALIZA��O DO CODIGO CEST
EXEC  USP_ATUALIZAR_CEST @NF_FATURAMENTO, 'NF_FATURAMENTO'

/****************************************************************/
/* NOTA FISCAL DE SERVI�O, CALCULOS E OUTROS                    */
/****************************************************************/   

/****************************************************************/
/* EXCLUS�O DAS INFORMA��ES NA TABELA DE APURA��O               */
/****************************************************************/
  DELETE APURACAO_NF_FATURAMENTO_SERVICOS 
    FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
   WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO

/****************************************************************/
/* ATUALIZA��O DAS INFORMA��ES DOS IMPOSTOS CSSL, PIS E COFINS  */
/****************************************************************/
 UPDATE NF_FATURAMENTO_SERVICOS
    SET ALIQUOTA_PIS                = 0.00
       ,ALIQUOTA_COFINS             = 0.00
       ,ALIQUOTA_CSSL               = 0.00
       
       ,PIS_VALOR                   = 0.00
       ,COFINS_VALOR                = 0.00
       ,CSSL_VALOR                  = 0.00
    
       ,PIS_BASE_CALCULO            = 0.00
       ,COFINS_BASE_CALCULO         = 0.00
       ,CSSL_BASE_CALCULO           = 0.00
       
       ,PIS_RETIDO_ANTERIORMENTE    = 0.00
       ,COFINS_RETIDO_ANTERIORMENTE = 0.00
       ,CSSL_RETIDO_ANTERIORMENTE   = 0.00
       
       ,PIS_VALOR_RETENCAO          = 0.00
       ,COFINS_RETIDO               = 0.00
       ,CSSL_RETIDO                 = 0.00
       
   FROM NF_FATURAMENTO_SERVICOS          A WITH(NOLOCK)
   JOIN (SELECT A.NF_FATURAMENTO_COMPOSTO
           FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
          WHERE A.NF_FATURAMENTO_COMPOSTO = @NF_FATURAMENTO
       GROUP BY A.NF_FATURAMENTO_COMPOSTO
        )                                B             ON B.NF_FATURAMENTO_COMPOSTO = A.NF_FATURAMENTO
   
  WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO

/****************************************************************/
/* CRIA��O DA TEMP1                                             */
/****************************************************************/
IF OBJECT_ID ('TEMPDB..#TEMP_01') IS NOT NULL
   DROP TABLE #TEMP_01
  
  SELECT A.NF_FATURAMENTO
        ,A.ENTIDADE                                                                           AS ENTIDADE
        ,DBO.ZEROS(MONTH(C.VENCIMENTO),2)                                                     AS MES
        ,YEAR(C.VENCIMENTO)                                                                   AS ANO
        ,CONVERT(NUMERIC(15,2),SUM((B.TOTAL_SERVICO_TRIBUTADO / D.TOTAL_SERVICOS) * C.VALOR)) AS BASE
        ,ISNULL(B.PIS_ALIQUOTA,0.00)                                                          AS PIS_ALIQUOTA
        ,ISNULL(B.COFINS_ALIQUOTA,0.00)                                                       AS COFINS_ALIQUOTA
        ,ISNULL(B.CSSL_ALIQUOTA,0.00)                                                         AS CSSL_ALIQUOTA
    
    INTO #TEMP_01
    FROM NF_FATURAMENTO          A WITH(NOLOCK)
    JOIN (SELECT A.NF_FATURAMENTO      AS NF_FATURAMENTO
                ,SUM(B.TOTAL_SERVICO)  AS TOTAL_SERVICO_TRIBUTADO
                ,C.ALIQUOTA_PIS        AS PIS_ALIQUOTA
                ,C.ALIQUOTA_COFINS     AS COFINS_ALIQUOTA
                ,C.ALIQUOTA_CSSL       AS CSSL_ALIQUOTA
            FROM NF_FATURAMENTO          A WITH(NOLOCK)
            JOIN NF_FATURAMENTO_SERVICOS B WITH(NOLOCK)ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
            JOIN SERVICOS_VENDA          C WITH(NOLOCK)ON C.SERVICO_VENDA  = B.SERVICO_VENDA
           WHERE A.ENTIDADE     = @ENTIDADE
             AND (C.ALIQUOTA_CSSL   > 0
               OR C.ALIQUOTA_PIS    > 0
               OR C.ALIQUOTA_COFINS > 0)
         GROUP BY A.NF_FATURAMENTO
                 ,C.ALIQUOTA_PIS   
                 ,C.ALIQUOTA_COFINS
                 ,C.ALIQUOTA_CSSL  
         )                       B             ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
    JOIN NF_FATURAMENTO_PARCELAS C WITH(NOLOCK)ON C.NF_FATURAMENTO = A.NF_FATURAMENTO
    JOIN NF_FATURAMENTO_TOTAIS   D WITH(NOLOCK)ON D.NF_FATURAMENTO = A.NF_FATURAMENTO
  
   WHERE A.ENTIDADE        = @ENTIDADE
     AND A.NF_FATURAMENTO <= @NF_FATURAMENTO
   
GROUP BY A.NF_FATURAMENTO
        ,A.ENTIDADE
        ,MONTH(C.VENCIMENTO)
        ,YEAR(C.VENCIMENTO)
        ,B.PIS_ALIQUOTA
        ,B.COFINS_ALIQUOTA
        ,B.CSSL_ALIQUOTA
        
ORDER BY A.NF_FATURAMENTO

/****************************************************************/
/* INSERT APURACAO_NF_FATURAMENTO_SERVICOS                      */
/****************************************************************/
INSERT INTO APURACAO_NF_FATURAMENTO_SERVICOS
           (MES
           ,ANO
           ,ENTIDADE
           ,NF_FATURAMENTO
           ,NF_FATURAMENTO_COMPOSTO
           ,VALOR_BASE
           ,VALOR_TOTAL_BASE
           ,PIS_ALIQUOTA
           ,PIS_RETIDO_ANTERIORMENTE
           ,PIS_RETIDO
           ,COFINS_ALIQUOTA
           ,COFINS_RETIDO_ANTERIORMENTE
           ,COFINS_RETIDO
           ,CSSL_ALIQUOTA
           ,CSSL_RETIDO_ANTERIORMENTE
           ,CSSL_RETIDO)

     SELECT DISTINCT
            A.MES                      AS MES
           ,A.ANO                      AS ANO
           ,A.ENTIDADE                 AS ENTIDADE
           ,C.NF_FATURAMENTO           AS NF_FATURAMENTO
           ,A.NF_FATURAMENTO           AS NF_FATURAMENTO_COMPOSTO
           ,A.BASE                     AS VALOR_BASE
           ,0.00                       AS VALOR_TOTAL_BASE
           ,A.PIS_ALIQUOTA             AS PIS_ALIQUOTA
           ,0.00                       AS PIS_RETIDO_ANTERIORMENTE
           ,0.00                       AS PIS_RETIDO
           ,A.COFINS_ALIQUOTA          AS COFINS_ALIQUOTA
           ,0.00                       AS COFINS_RETIDO_ANTERIORMENTE
           ,0.00                       AS COFINS_RETIDO
           ,A.CSSL_ALIQUOTA            AS CSSL_ALIQUOTA
           ,0.00                       AS CSSL_RETIDO_ANTERIORMENTE
           ,0.00                       AS CSSL_RETIDO

       FROM #TEMP_01                A WITH(NOLOCK)
       JOIN NF_FATURAMENTO_PARCELAS C WITH(NOLOCK)ON MONTH(C.VENCIMENTO) = A.MES
                                                 AND YEAR (C.VENCIMENTO) = A.ANO
                                                        
      WHERE C.NF_FATURAMENTO  = @NF_FATURAMENTO 
   
/****************************************************************/
/* UPDATE BASE DE CALCULO                                       */
/****************************************************************/
    UPDATE APURACAO_NF_FATURAMENTO_SERVICOS
       SET VALOR_TOTAL_BASE            = B.VALOR_BASE_TOTAL
          ,PIS_RETIDO_ANTERIORMENTE    = ISNULL(C.PIS_RETIDO_ANTERIORMENTE,0)
          ,COFINS_RETIDO_ANTERIORMENTE = ISNULL(C.COFINS_RETIDO_ANTERIORMENTE,0)
          ,CSSL_RETIDO_ANTERIORMENTE   = ISNULL(C.CSSL_RETIDO_ANTERIORMENTE,0)
    
      FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
      JOIN (SELECT A.NF_FATURAMENTO     AS NF_FATURAMENTO
                  ,A.MES                AS MES
                  ,A.ANO                AS ANO
                  ,A.ENTIDADE           AS ENTIDADE
                  ,SUM(A.VALOR_BASE)    AS VALOR_BASE_TOTAL
              FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
             WHERE A.NF_FATURAMENTO  = @NF_FATURAMENTO
               AND A.NF_FATURAMENTO <= @NF_FATURAMENTO              
          GROUP BY A.NF_FATURAMENTO
                  ,A.MES
                  ,A.ANO
                  ,A.ENTIDADE
           )                                B             ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
                                                         AND B.MES            = A.MES
                                                         AND B.ANO            = A.ANO
                                                         AND B.ENTIDADE       = A.ENTIDADE
 LEFT JOIN (SELECT @NF_FATURAMENTO      AS NF_FATURAMENTO 
                  ,A.MES                AS MES
                  ,A.ANO                AS ANO
                  ,A.ENTIDADE           AS ENTIDADE
                  ,SUM(A.PIS_RETIDO)    AS PIS_RETIDO_ANTERIORMENTE
                  ,SUM(A.COFINS_RETIDO) AS COFINS_RETIDO_ANTERIORMENTE
                  ,SUM(A.CSSL_RETIDO)   AS CSSL_RETIDO_ANTERIORMENTE
              FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
             WHERE A.NF_FATURAMENTO    <> @NF_FATURAMENTO
               AND A.NF_FATURAMENTO    <= @NF_FATURAMENTO 
          GROUP BY A.MES
                  ,A.ANO
                  ,A.ENTIDADE
            HAVING SUM(A.PIS_RETIDO)    > 0
                OR SUM(A.COFINS_RETIDO) > 0
                OR SUM(A.CSSL_RETIDO)   > 0
           )                                C             ON C.NF_FATURAMENTO = A.NF_FATURAMENTO
                                                         AND C.MES            = A.MES
                                                         AND C.ANO            = A.ANO
                                                         AND C.ENTIDADE       = A.ENTIDADE
       
     WHERE A.NF_FATURAMENTO_COMPOSTO = @NF_FATURAMENTO
 
/****************************************************************/
/* UPDATE IMPOSTOS                                              */
/****************************************************************/
 UPDATE APURACAO_NF_FATURAMENTO_SERVICOS
    SET PIS_RETIDO    = ((A.VALOR_TOTAL_BASE * (A.PIS_ALIQUOTA / 100) - A.PIS_RETIDO_ANTERIORMENTE))
       ,COFINS_RETIDO = ((A.VALOR_TOTAL_BASE * (A.COFINS_ALIQUOTA / 100) - A.COFINS_RETIDO_ANTERIORMENTE))
       ,CSSL_RETIDO   = ((A.VALOR_TOTAL_BASE * (A.CSSL_ALIQUOTA / 100) - A.CSSL_RETIDO_ANTERIORMENTE))
  
   FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
 
  WHERE A.NF_FATURAMENTO_COMPOSTO = @NF_FATURAMENTO
    AND A.VALOR_TOTAL_BASE        > 5000

/****************************************************************/
/* UPDATE NF_FATURAMENTO_SERVICOS                               */
/****************************************************************/
 UPDATE NF_FATURAMENTO_SERVICOS
    SET ALIQUOTA_PIS                = B.PIS_ALIQUOTA
       ,ALIQUOTA_COFINS             = B.COFINS_ALIQUOTA
       ,ALIQUOTA_CSSL               = B.CSSL_ALIQUOTA
       
       ,PIS_VALOR                   = (B.VALOR_TOTAL_BASE * (B.PIS_ALIQUOTA / 100))
       ,COFINS_VALOR                = (B.VALOR_TOTAL_BASE * (B.COFINS_ALIQUOTA / 100))
       ,CSSL_VALOR                  = (B.VALOR_TOTAL_BASE * (B.CSSL_ALIQUOTA / 100))
    
       ,PIS_BASE_CALCULO            = B.VALOR_TOTAL_BASE
       ,COFINS_BASE_CALCULO         = B.VALOR_TOTAL_BASE
       ,CSSL_BASE_CALCULO           = B.VALOR_TOTAL_BASE
       
       ,PIS_RETIDO_ANTERIORMENTE    = B.PIS_RETIDO_ANTERIORMENTE
       ,COFINS_RETIDO_ANTERIORMENTE = B.COFINS_RETIDO_ANTERIORMENTE
       ,CSSL_RETIDO_ANTERIORMENTE   = B.CSSL_RETIDO_ANTERIORMENTE
       
       ,PIS_VALOR_RETENCAO          = B.PIS_RETIDO
       ,COFINS_RETIDO               = B.COFINS_RETIDO
       ,CSSL_RETIDO                 = B.CSSL_RETIDO
       
   FROM NF_FATURAMENTO_SERVICOS          A WITH(NOLOCK)
   JOIN APURACAO_NF_FATURAMENTO_SERVICOS B WITH(NOLOCK)ON B.NF_FATURAMENTO_COMPOSTO = A.NF_FATURAMENTO
   
  WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
   
/****************************************************************/
/* UPDATE NF_FATURAMENTO_RETENCOES                              */
/****************************************************************/   
 UPDATE NF_FATURAMENTO_RETENCOES
    SET PIS_RETENCAO      = B.PIS_RETIDO
       ,COFINS_RETENCAO   = B.COFINS_RETIDO
       ,CSSL_RETENCAO     = B.CSSL_RETIDO
       
   FROM NF_FATURAMENTO_RETENCOES         A WITH(NOLOCK)
   JOIN APURACAO_NF_FATURAMENTO_SERVICOS B WITH(NOLOCK)ON B.NF_FATURAMENTO_COMPOSTO = A.NF_FATURAMENTO
   
  WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
  
 --UPDATE APURACAO_NF_FATURAMENTO_SERVICOS 
 --   SET RETIDO = 'S'
 --  FROM APURACAO_NF_FATURAMENTO_SERVICOS A WITH(NOLOCK)
 -- WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
  
/****************************************************************/
/* THE END                                                      */
/****************************************************************/  


-------------------------
--  GERA NUMERO NOTA   --
-------------------------
DECLARE @EMPRESA    NUMERIC(15)
   	   ,@NF_ESPECIE VARCHAR(3)
	   ,@NF_SERIE   VARCHAR(3)
	   ,@NF_NUMERO  NUMERIC(15)

DECLARE @PEDIDO_VENDA_ATACADO NUMERIC(15)
	   ,@EMITIR_NFE           VARCHAR(1)
	   ,@FORMULARIO_ORIGEM    NUMERIC(15)
        

    SELECT @EMPRESA           = A.EMPRESA
	  ,@NF_ESPECIE        = A.NF_ESPECIE
	  ,@NF_SERIE          = A.NF_SERIE
	  ,@NF_FATURAMENTO    = A.NF_FATURAMENTO
	  ,@FORMULARIO_ORIGEM = A.FORMULARIO_ORIGEM
	  ,@EMITIR_NFE        = A.EMITIR_NFE
          ,@NF_NUMERO         = A.NF_NUMERO
  FROM NF_FATURAMENTO A WITH(NOLOCK)

 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO


IF ( ISNULL(@NF_NUMERO, 0) = 0 ) AND ( @EMITIR_NFE = 'S' )

BEGIN
    EXEC USP_CONTROLE_SEQUENCIA_NF_NUMERO @EMPRESA, @NF_SERIE, @NF_ESPECIE, 1, @NF_NUMERO OUTPUT, NULL, @FORMULARIO_ORIGEM, @NF_FATURAMENTO, @TAB_MASTER_ORIGEM

    UPDATE
        NF_FATURAMENTO
    SET
        NF_NUMERO           = @NF_NUMERO
    WHERE
        NF_FATURAMENTO      = @NF_FATURAMENTO
    AND ISNULL(NF_NUMERO,0) = 0
    AND @EMITIR_NFE = 'S'

END

     ------------------------------------------------
     -- ATUALIZA TITULOS DAS TABELAS DE FINANCEIRO --
     ------------------------------------------------
	If object_id('tempdb..#TEMP01') is not null
	   DROP TABLE #TEMP01


     SELECT A.TIPO ,
            A.CHAVE ,
            CAST ( @NF_NUMERO AS VARCHAR(10) ) + '/' +
            CAST(ROW_NUMBER() OVER (ORDER BY A.NF_FATURAMENTO ASC) AS VARCHAR(2)) AS TITULO 

       INTO #TEMP01

       FROM (

	     SELECT 0                        AS TIPO ,
	            A.NF_FATURAMENTO         AS NF_FATURAMENTO ,
	            A.NF_FATURAMENTO_PARCELA AS CHAVE
	       FROM NF_FATURAMENTO_PARCELAS A WITH(NOLOCK)
	      WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
   
        UNION ALL
   
	     SELECT 1                       AS TIPO ,
	            A.NF_FATURAMENTO        AS NF_FATURAMENTO ,
	            A.NF_FATURAMENTO_BOLETO AS CHAVE
	       FROM NF_FATURAMENTO_BOLETOS A WITH(NOLOCK)
	      WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO

	    UNION ALL
   
	     SELECT 2                         AS TIPO ,
	            A.NF_FATURAMENTO          AS NF_FATURAMENTO ,
	            A.NF_FATURAMENTO_DEPOSITO AS CHAVE
	       FROM NF_FATURAMENTO_DEPOSITOS A WITH(NOLOCK)
	      WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO

            ) A

			
	UPDATE NF_FATURAMENTO_PARCELAS
           SET TITULO = ISNULL(A.TITULO,'1')
	  FROM #TEMP01                 A WITH(NOLOCK)
	  JOIN NF_FATURAMENTO_PARCELAS B WITH(NOLOCK) ON B.NF_FATURAMENTO_PARCELA = A.CHAVE
	 WHERE A.TIPO = 0

	UPDATE NF_FATURAMENTO_BOLETOS
           SET TITULO = ISNULL(A.TITULO,'1')
	  FROM #TEMP01                 A WITH(NOLOCK)
	  JOIN NF_FATURAMENTO_BOLETOS  B WITH(NOLOCK) ON B.NF_FATURAMENTO_BOLETO = A.CHAVE
	 WHERE A.TIPO = 1

	UPDATE NF_FATURAMENTO_DEPOSITOS
           SET TITULO = ISNULL(A.TITULO,'1')
	  FROM #TEMP01                  A WITH(NOLOCK)
	  JOIN NF_FATURAMENTO_DEPOSITOS B WITH(NOLOCK) ON B.NF_FATURAMENTO_DEPOSITO = A.CHAVE
	 WHERE A.TIPO = 2



-------------------------------------
-- RATEIO DO FRETE NA BASE DE ICMS --
-------------------------------------
EXEC GERAR_RATEIO_BASE_ICMS_NF_FATURAMENTO @NF_FATURAMENTO
EXEC GERAR_RATEIO_DESPESAS_NF_FATURAMENTO  @NF_FATURAMENTO
EXEC GERAR_RATEIO_FRETE_NF_FATURAMENTO     @NF_FATURAMENTO
EXEC GERAR_RATEIO_SEGURO_NF_FATURAMENTO    @NF_FATURAMENTO

-----------------------------------------
--INTEGRACAO DE NOTA FISCAL ELETRONICA --
-----------------------------------------

--------------------------------
--INTEGRACAO DE NOTAS GERADAS --
--------------------------------
DELETE 
  FROM NOTAS_GERADAS 
 WHERE CHAVE = @NF_FATURAMENTO
   AND TIPO  = 1

INSERT INTO NOTAS_GERADAS ( 

            FORMULARIO_ORIGEM ,
            TAB_MASTER_ORIGEM ,
            REG_MASTER_ORIGEM ,
            CHAVE ,
            TIPO ,
            NF_NUMERO )
SELECT 
       ISNULL(A.FORMULARIO_ORIGEM , 769791 )AS FORMULARIO_ORIGEM,
       ISNULL(A.TAB_MASTER_ORIGEM , 753289 )AS TAB_MASTER_ORIGEM,
       A.NF_FATURAMENTO                     AS REG_MASTER_ORIGEM,
       A.NF_FATURAMENTO                     AS CHAVE ,
       1                                    AS TIPO ,
       A.NF_NUMERO

  FROM NF_FATURAMENTO A WITH(NOLOCK)
 WHERE A.NF_FATURAMENTO =  @NF_FATURAMENTO


----------------------------------------------------------------
----INTEGRACAO DO ESTOQUE DE LOTE E VALIDADE
----------------------------------------------------------------
DELETE LOTE_VALIDADE_LANCAMENTOS

  FROM LOTE_VALIDADE_LANCAMENTOS    A WITH(NOLOCK),
       NF_FATURAMENTO               B WITH(NOLOCK)

 WHERE A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM
   AND A.REG_MASTER_ORIGEM = B.NF_FATURAMENTO
   AND B.NF_FATURAMENTO    = @NF_FATURAMENTO

INSERT INTO LOTE_VALIDADE_LANCAMENTOS ( 
            FORMULARIO_ORIGEM ,
            TAB_MASTER_ORIGEM ,
            REG_MASTER_ORIGEM ,
            DATA ,
            LOTE_VALIDADE ,
            PRODUTO ,
            CENTRO_ESTOQUE,
            ENTRADA ,
            SAIDA,
            TIPO_MOVIMENTO )


SELECT A.FORMULARIO_ORIGEM,
       A.TAB_MASTER_ORIGEM,
       A.NF_FATURAMENTO,
       A.MOVIMENTO               AS DATA,
       F.LOTE_VALIDADE           AS LOTE_VALIDADE,
       D.PRODUTO                 AS PRODUTO,
       D.OBJETO_CONTROLE         AS CENTRO_ESTOQUE,
       0                         AS ENTRADA, 
       SUM(D.QUANTIDADE_ESTOQUE) AS SAIDA,
       3                         AS TIPO_MOVIMENTO
       
  FROM NF_FATURAMENTO                        A WITH(NOLOCK)
      JOIN NF_FATURAMENTO_PRODUTOS           D WITH(NOLOCK) ON D.NF_FATURAMENTO     = A.NF_FATURAMENTO
      JOIN PRODUTOS                          E WITH(NOLOCK) ON E.PRODUTO            = D.PRODUTO
      JOIN PRODUTOS_LOTE_VALIDADE            F WITH(NOLOCK) ON F.PRODUTO            =  D.PRODUTO
                                                           AND F.LOTE               =  D.LOTE
                                                           AND F.VALIDADE_DIGITACAO = D.VALIDADE_DIGITACAO
      JOIN OPERACOES_FISCAIS                 G WITH (NOLOCK)ON G.OPERACAO_FISCAL    = D.OPERACAO_FISCAL

	  JOIN EMPRESAS_USUARIAS                 H WITH(NOLOCK) ON H.EMPRESA_USUARIA    = A.EMPRESA  -- EMPRESA SAIDA
      JOIN PARAMETROS_ESTOQUE                B WITH(NOLOCK) ON B.EMPRESA_USUARIA    = H.EMPRESA_USUARIA
                                                            AND B.TIPO_CONTROLE     = 2 --Controle de Lote e Validade - Estoque Separado ( Se aplica a lojas com psicos)
      JOIN PRODUTOS_FARMACIA                 C WITH(NOLOCK) ON C.PRODUTO            =  D.PRODUTO
                                                           AND C.VENDA_CONTROLADA   = 'S'
 
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
   AND G.GERAR_ESTOQUE  = 'S'

GROUP BY
       A.FORMULARIO_ORIGEM,
       A.TAB_MASTER_ORIGEM,
       A.NF_FATURAMENTO,
       A.MOVIMENTO ,
       F.LOTE_VALIDADE,
       D.PRODUTO,
       D.OBJETO_CONTROLE,
       A.NF_NUMERO

/*

-----------------------------------------------------
-- TABELA TEMPOR�RIA DE DESDOBRAMENTO DE CART�ES   --
-- SER� UTILIZADA NA INTEGRA��O DE TITULOS_RECEBER --
-----------------------------------------------------
IF OBJECT_ID ('TEMPDB..##CARTOES') IS NOT NULL
   DROP TABLE ##CARTOES

-- DESDOBRAMENTO DAS PARCELAS --

SELECT B.NF_FATURAMENTO                                       AS NF_FATURAMENTO ,
       B.NF_FATURAMENTO_CARTAO                                AS NF_FATURAMENTO_CARTAO,
       ROW_NUMBER() OVER(ORDER BY NF_FATURAMENTO_CARTAO DESC) AS ORDEM ,
       B.VALOR                                                AS VALOR_TOTAL,
       B.PARCELAS                                             AS PARCELAS ,
	   ROUND(B.VALOR / B.PARCELAS , 2)                        AS VALOR_PARCELA ,
	   B.NSU + '/' + CONVERT(VARCHAR,ROW_NUMBER() OVER(ORDER BY NF_FATURAMENTO_CARTAO DESC))
	                                                           AS TITULO ,
       CONVERT(DATETIME,CONVERT(VARCHAR,A.EMISSAO   + (C.SEQUENCIA * D.DIAS_VENCIMENTO),103))
	                                                       AS VENCIMENTO ,
       D.ENTIDADE                                              AS ENTIDADE ,
       B.CLASSIF_FINANCEIRA                                    AS CLASSIF_FINANCEIRA
  INTO ##CARTOES
  FROM NF_FATURAMENTO         A WITH(NOLOCK)
  JOIN NF_FATURAMENTO_CARTOES B WITH(NOLOCK) ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
  JOIN VEZES                  C WITH(NOLOCK) ON C.SEQUENCIA      <= B.PARCELAS
  JOIN BANDEIRAS_CARTOES      D WITH(NOLOCK) ON D.BANDEIRA_CARTAO = B.BANDEIRA_CARTAO
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
 ORDER BY B.NF_FATURAMENTO_CARTAO , ORDEM

-- AJUSTE DE ARREDONDAMENTOS --

UPDATE ##CARTOES SET VALOR_PARCELA = VALOR_PARCELA + ISNULL(B.DIF,0)
  FROM ##CARTOES A WITH(NOLOCK)
  JOIN ( SELECT A.NF_FATURAMENTO        AS NF_FATURAMENTO ,
                A.NF_FATURAMENTO_CARTAO AS NF_FATURAMENTO_CARTAO,
                MAX(A.ORDEM)            AS ORDEM ,
				MAX(A.VALOR_TOTAL) - 
				SUM(A.VALOR_PARCELA)    AS DIF
		   FROM ##CARTOES A WITH(NOLOCK)
		  GROUP BY A.NF_FATURAMENTO ,
		           A.NF_FATURAMENTO_CARTAO ) B ON B.NF_FATURAMENTO        = A.NF_FATURAMENTO
		                                      AND B.NF_FATURAMENTO_CARTAO = A.NF_FATURAMENTO_CARTAO
		                                      AND B.ORDEM                 = A.ORDEM

*/

/*
 * DEPARTAMENTO FISCAL: CRISTIANO TENORIO - 19/07/2016 - ATUALIZACAO DOS CAMPOS DA NOTA DE FATURAMENTO CONFORME EMENDA 87
 */

DECLARE @MOVIMENTO      DATETIME
DECLARE @ESTADO_ORIGEM  VARCHAR(2)
DECLARE @ESTADO_DESTINO VARCHAR(2)

SELECT @MOVIMENTO      = A.MOVIMENTO
     , @ESTADO_ORIGEM  = D.ESTADO
     , @ESTADO_DESTINO = F.ESTADO
  FROM NF_FATURAMENTO          A WITH(NOLOCK)
  JOIN EMPRESAS_USUARIAS       B WITH(NOLOCK) ON  B.EMPRESA_USUARIA = A.EMPRESA
  JOIN ENTIDADES               C WITH(NOLOCK) ON  C.ENTIDADE        = B.ENTIDADE
  JOIN ENDERECOS               D WITH(NOLOCK) ON  D.ENTIDADE        = C.ENTIDADE
  JOIN NF_FATURAMENTO_ENTIDADE F WITH(NOLOCK) ON  F.NF_FATURAMENTO  = A.NF_FATURAMENTO 
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO

        
EXEC EMENDA_87_GT @NF_FATURAMENTO
                , @ESTADO_ORIGEM
                , @ESTADO_DESTINO
                , @MOVIMENTO


---------
-- NFE --
---------
DECLARE @AMBIENTE_NFE NUMERIC(15)

SELECT @AMBIENTE_NFE = A.AMBIENTE_NFE
FROM EMPRESAS_USUARIAS A WITH(NOLOCK)
WHERE A.EMPRESA_USUARIA = @EMPRESA

IF @EMITIR_NFE = 'S'
BEGIN

	--EQUALIZA A DATA DE EMISSAO
	UPDATE NF_FATURAMENTO
	   SET EMISSAO_NFE = DATEADD(DD, DATEDIFF(DD,CONVERT(DATE,CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET())),EMISSAO),CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET()) )
	 WHERE NF_FATURAMENTO = @NF_FATURAMENTO
	   
	--EQUALIZA A DATA DE SAIDA
	UPDATE NF_FATURAMENTO
	   SET SAIDA_NFE   = DATEADD(DD, DATEDIFF(DD,CONVERT(DATE,CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET())),SAIDA  ),CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET()) )
	 WHERE NF_FATURAMENTO = @NF_FATURAMENTO
	   
        --------------------------------------------------
        -- LIMPA NFE_CABECALHO COM ERRO OU N�O ENVIADA  --
        --------------------------------------------------
        EXEC LIMPAR_NFE_CABECALHO @NF_FATURAMENTO , @NF_NUMERO , @NF_SERIE , @EMPRESA


        DECLARE @REGISTRO_NFE NUMERIC(15)

        ------------------------------
        -- VERIFICA NOTA JA EMITIDA --
        ------------------------------
	IF NOT EXISTS (
	SELECT 1 FROM NFE_CABECALHO 
		WHERE XML_cNF_B03       = @NF_FATURAMENTO 
			  AND XML_serie_B07 = @NF_SERIE 
			  AND XML_nNF_B08   = @NF_NUMERO
			  AND EMPRESA       = @EMPRESA 
               )
	BEGIN

             ---------------------------
             -- GERANDO NFE_CABECALHO --
             ---------------------------
             EXEC GERAR_NFE_NF_FATURAMENTO @NF_FATURAMENTO 

        END ELSE 
            SELECT @REGISTRO_NFE = A.REGISTRO_NFE
              FROM NFE_CABECALHO A WITH(NOLOCK)
             WHERE A.XML_cNF_B03       = @NF_FATURAMENTO 
               AND A.FORMULARIO_ORIGEM = @FORMULARIO_ORIGEM 

        IF @REGISTRO_NFE > 0 
        BEGIN

               IF DBO.NFE_VERIFICA_SITUACAO_RETORNO(@REGISTRO_NFE) = 1
    	       BEGIN 
		    UPDATE NFE_CABECALHO SET CONSULTA_NFE_CHAVE        = 1
		                            ,RECUPERA_XML_DESTINATARIO = 1
			       	            ,STATUS                    = 2 
                     WHERE REGISTRO_NFE = @REGISTRO_NFE
               END

        END

     ----------------------------------------
     -- ENVIA COMANDO PARA ACIONAR ROBO NF-E
     ----------------------------------------
     IF OBJECT_ID('TEMPDB..#NFE_COMANDOS' ) IS NOT NULL 
     BEGIN

          SELECT @REGISTRO_NFE = A.REGISTRO_NFE
            FROM NFE_CABECALHO A WITH(NOLOCK)
           WHERE A.XML_cNF_B03       = @NF_FATURAMENTO 
             AND A.FORMULARIO_ORIGEM = @FORMULARIO_ORIGEM 

          INSERT INTO #NFE_COMANDOS ( COMANDO )
               VALUES ( 'PROCESSAR_NFE=' + RTRIM( LTRIM( STR( @REGISTRO_NFE ) ) ) )
    
    END

-----------------------------------------
--   ATUALIZA��O DO STATUS DO PEDIDO   --
-----------------------------------------

IF @PEDIDO_VENDA IS NOT NULL

   EXEC USP_PEDIDO_VENDA_UDP_STATUS @FORMULARIO_ORIGEM_NOTA, @TAB_MASTER_ORIGEM_NOTA, @PEDIDO_VENDA

END


----------------------------------------------------------
--    INSERINDO DADOS NA TABELA DE VENDAS ANALITICAS    --
----------------------------------------------------------


---------------------------------------------------------------------
--    LIMPAR DADOS NA TABELA DE VENDAS ANALITICAS ANTES DO MERGE   --
---------------------------------------------------------------------

DELETE VENDAS_ANALITICAS 
    FROM VENDAS_ANALITICAS            Z WITH(NOLOCK)
    JOIN NF_FATURAMENTO               A WITH(NOLOCK) ON A.FORMULARIO_ORIGEM = Z.FORMULARIO_ORIGEM
                                                    AND A.TAB_MASTER_ORIGEM = Z.TAB_MASTER_ORIGEM
                                                    AND A.REG_MASTER_ORIGEM = Z.REG_MASTER_ORIGEM
	JOIN NF_FATURAMENTO_PRODUTOS      B WITH(NOLOCK) ON A.NF_FATURAMENTO    = B.NF_FATURAMENTO
                                                    AND Z.PRODUTO           = B.PRODUTO
WHERE 1 = 1
	AND A.NF_FATURAMENTO = @NF_FATURAMENTO


MERGE VENDAS_ANALITICAS A
USING
(
SELECT 														          
	A.FORMULARIO_ORIGEM           AS FORMULARIO_ORIGEM                ,           
	A.TAB_MASTER_ORIGEM           AS TAB_MASTER_ORIGEM                ,        
	A.NF_FATURAMENTO              AS REG_MASTER_ORIGEM                ,           
	B.NF_FATURAMENTO_PRODUTO      AS REGISTRO_CONTROLE                ,           
	B.PRODUTO                     AS REGISTRO_CONTROLE_II             ,
	A.EMPRESA                     AS EMPRESA                          ,                     
	A.MOVIMENTO                   AS MOVIMENTO                        ,
	0                             AS CAIXA                            ,                       
	0                             AS VENDA                            ,                       
	B.PRODUTO                     AS PRODUTO                          ,                     
	A.VENDEDOR                    AS VENDEDOR                         ,                 
	A.OPERADOR                    AS OPERADOR                         ,
	A.ENTIDADE                    AS CLIENTE                          ,                     
	B.QUANTIDADE                  AS QUANTIDADE                       ,
															          
	B.VALOR_UNITARIO * B.QUANTIDADE 						          
								  AS VENDA_BRUTA                      ,
															          
	B.TOTAL_DESCONTO              AS TOTAL_DESCONTO                   ,
	0                             AS DESCONTO_NEGOCIADO               ,
	A.OPERACAO_FISCAL                                                 ,
	B.PIS_ALIQUOTA                                                    ,
	B.PIS_BASE_CALCULO                                                ,
	B.PIS_VALOR                                                       ,
	B.SITUACAO_TRIBUTARIA_PIS                                         ,
	B.COFINS_ALIQUOTA                                                 ,
	B.COFINS_BASE_CALCULO                                             ,
	B.COFINS_VALOR                                                    ,
	B.SITUACAO_TRIBUTARIA_COFINS                                      ,
	B.ICMS_ALIQUOTA                                                   ,
	B.ICMS_BASE_CALCULO                                               ,
	B.ICMS_VALOR                                                      ,
	B.SITUACAO_TRIBUTARIA                                             ,
	CONVERT(CHAR, 0)              AS ICMS_CODIGO_ECF                  ,              
	A.NF_ESPECIE                  AS ESPECIE_FISCAL                   , 
	8			                  AS DOCUMENTO_TIPO                   ,
	A.NF_NUMERO                   AS DOCUMENTO_NUMERO                 ,            
	0                             AS ECF_CAIXA                        ,   
	C.TIPO_BONIFICACAO            AS TIPO_BONIFICACAO                 , 
	C.BONIFICACAO                 AS BONIFICACAO                      ,
	NULL                          AS OBJETO_CONTROLE                  ,
	C.TIPO_COMISSAO               AS TIPO_COMISSAO                    ,
	C.COMISSAO                    AS COMISSAO                         ,
	A.FORMULARIO_ORIGEM           AS FORMULARIO_ORIGEM_RELACIONADO    ,   
	A.TAB_MASTER_ORIGEM           AS TAB_MASTER_ORIGEM_RELACIONADO    ,   
	A.NF_FATURAMENTO              AS REG_MASTER_ORIGEM_RELACIONADO    ,   
	B.NF_FATURAMENTO_PRODUTO      AS REGISTRO_CONTROLE_RELACIONADO    ,   
	B.PRODUTO                     AS REGISTRO_CONTROLE_II_RELACIONADO
FROM 
	NF_FATURAMENTO                    A WITH(NOLOCK)
	JOIN NF_FATURAMENTO_PRODUTOS      B WITH(NOLOCK) ON A.NF_FATURAMENTO    = B.NF_FATURAMENTO
	JOIN PRODUTOS_PARAMETROS_EMPRESAS C WITH(NOLOCK) ON A.EMPRESA           = C.EMPRESA             AND
							                            B.PRODUTO           = C.PRODUTO
	JOIN OPERACOES_FISCAIS            D WITH(NOLOCK) ON B.OPERACAO_FISCAL   = D.OPERACAO_FISCAL
WHERE 1 = 1
	AND A.NF_FATURAMENTO = @NF_FATURAMENTO
	AND D.TIPO_OPERACAO  = 3 -- 3 = VENDA
) AS B 
ON A.FORMULARIO_ORIGEM     = B.FORMULARIO_ORIGEM
AND A.TAB_MASTER_ORIGEM    = B.TAB_MASTER_ORIGEM
AND A.REG_MASTER_ORIGEM    = B.REG_MASTER_ORIGEM           
AND A.REGISTRO_CONTROLE    = B.REGISTRO_CONTROLE   
AND A.REGISTRO_CONTROLE_II = B.REGISTRO_CONTROLE_II   
WHEN NOT MATCHED BY TARGET THEN
INSERT
(
	FORMULARIO_ORIGEM,
	TAB_MASTER_ORIGEM,
	REG_MASTER_ORIGEM,
	REGISTRO_CONTROLE,
	REGISTRO_CONTROLE_II,
	EMPRESA,
	MOVIMENTO,
	CAIXA,
	VENDA,
	PRODUTO,
	VENDEDOR,
	OPERADOR,
	CLIENTE,
	QUANTIDADE,
	VENDA_BRUTA,
	DESCONTO,
	DESCONTO_NEGOCIADO,
	OPERACAO_FISCAL,
	PIS_ALIQUOTA,
	PIS_BASE_CALCULO,
	PIS_VALOR,
	SITUACAO_TRIBUTARIA_PIS,
	COFINS_ALIQUOTA,
	COFINS_BASE_CALCULO,
	COFINS_VALOR,
	SITUACAO_TRIBUTARIA_COFINS,
	ICMS_ALIQUOTA,
	ICMS_BASE_CALCULO,
	ICMS_VALOR,
	SITUACAO_TRIBUTARIA,
	ICMS_CODIGO_ECF,
	ESPECIE_FISCAL,
	DOCUMENTO_TIPO,
	DOCUMENTO_NUMERO,
	ECF_CAIXA,
	TIPO_BONIFICACAO,
	BONIFICACAO,
	OBJETO_CONTROLE,
	TIPO_COMISSAO,
	COMISSAO,
	FORMULARIO_ORIGEM_RELACIONADO,
	TAB_MASTER_ORIGEM_RELACIONADO,
	REG_MASTER_ORIGEM_RELACIONADO,
	REGISTRO_CONTROLE_RELACIONADO,
	REGISTRO_CONTROLE_II_RELACIONADO
)
VALUES
(
	B.FORMULARIO_ORIGEM,
	B.TAB_MASTER_ORIGEM,
	B.REG_MASTER_ORIGEM,
	B.REGISTRO_CONTROLE,
	B.REGISTRO_CONTROLE_II,
	B.EMPRESA,
	B.MOVIMENTO,
	B.CAIXA,
	B.VENDA,
	B.PRODUTO,
	B.VENDEDOR,
	B.OPERADOR,
	B.CLIENTE,
	B.QUANTIDADE,
	B.VENDA_BRUTA,
	B.TOTAL_DESCONTO,
	B.DESCONTO_NEGOCIADO,
	B.OPERACAO_FISCAL,
	B.PIS_ALIQUOTA,
	B.PIS_BASE_CALCULO,
	B.PIS_VALOR,
	B.SITUACAO_TRIBUTARIA_PIS,
	B.COFINS_ALIQUOTA,
	B.COFINS_BASE_CALCULO,
	B.COFINS_VALOR,
	B.SITUACAO_TRIBUTARIA_COFINS,
	B.ICMS_ALIQUOTA,
	B.ICMS_BASE_CALCULO,
	B.ICMS_VALOR,
	B.SITUACAO_TRIBUTARIA,
	B.ICMS_CODIGO_ECF,
	B.ESPECIE_FISCAL,
	B.DOCUMENTO_TIPO,
	B.DOCUMENTO_NUMERO,
	B.ECF_CAIXA,
	B.TIPO_BONIFICACAO,
	B.BONIFICACAO,
	B.OBJETO_CONTROLE,
	B.TIPO_COMISSAO,
	B.COMISSAO,
	B.FORMULARIO_ORIGEM_RELACIONADO,
	B.TAB_MASTER_ORIGEM_RELACIONADO,
	B.REG_MASTER_ORIGEM_RELACIONADO,
	B.REGISTRO_CONTROLE_RELACIONADO,
	B.REGISTRO_CONTROLE_II_RELACIONADO
)
WHEN MATCHED THEN
UPDATE SET
	FORMULARIO_ORIGEM = B.FORMULARIO_ORIGEM,
	TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM,
	REG_MASTER_ORIGEM = B.REG_MASTER_ORIGEM,
	REGISTRO_CONTROLE = B.REGISTRO_CONTROLE,
	REGISTRO_CONTROLE_II = B.REGISTRO_CONTROLE_II,
	EMPRESA = B.EMPRESA,
	MOVIMENTO = B.MOVIMENTO,
	CAIXA = B.CAIXA,
	VENDA = B.VENDA,
	PRODUTO = B.PRODUTO,
	VENDEDOR = B.VENDEDOR,
	OPERADOR = B.OPERADOR,
	CLIENTE = B.CLIENTE,
	QUANTIDADE = B.QUANTIDADE,
	VENDA_BRUTA = B.VENDA_BRUTA,
	DESCONTO = B.TOTAL_DESCONTO,
	DESCONTO_NEGOCIADO = B.DESCONTO_NEGOCIADO,
	OPERACAO_FISCAL = B.OPERACAO_FISCAL,
	PIS_ALIQUOTA = B.PIS_ALIQUOTA,
	PIS_BASE_CALCULO = B.PIS_BASE_CALCULO,
	PIS_VALOR = B.PIS_VALOR,
	SITUACAO_TRIBUTARIA_PIS = B.SITUACAO_TRIBUTARIA_PIS,
	COFINS_ALIQUOTA = B.COFINS_ALIQUOTA,
	COFINS_BASE_CALCULO = B.COFINS_BASE_CALCULO,
	COFINS_VALOR = B.COFINS_VALOR,
	SITUACAO_TRIBUTARIA_COFINS = B.SITUACAO_TRIBUTARIA_COFINS,
	ICMS_ALIQUOTA = B.ICMS_ALIQUOTA,
	ICMS_BASE_CALCULO = B.ICMS_BASE_CALCULO,
	ICMS_VALOR = B.ICMS_VALOR,
	SITUACAO_TRIBUTARIA = B.SITUACAO_TRIBUTARIA,
	ICMS_CODIGO_ECF = B.ICMS_CODIGO_ECF,
	ESPECIE_FISCAL = B.ESPECIE_FISCAL,
	DOCUMENTO_TIPO = B.DOCUMENTO_TIPO,
	DOCUMENTO_NUMERO = B.DOCUMENTO_NUMERO,
	ECF_CAIXA = B.ECF_CAIXA,
	TIPO_BONIFICACAO = B.TIPO_BONIFICACAO,
	BONIFICACAO = B.BONIFICACAO,
	OBJETO_CONTROLE = B.OBJETO_CONTROLE,
	TIPO_COMISSAO = B.TIPO_COMISSAO,
	COMISSAO = B.COMISSAO,
	FORMULARIO_ORIGEM_RELACIONADO = B.FORMULARIO_ORIGEM_RELACIONADO,
	TAB_MASTER_ORIGEM_RELACIONADO = B.TAB_MASTER_ORIGEM_RELACIONADO,
	REG_MASTER_ORIGEM_RELACIONADO = B.REG_MASTER_ORIGEM_RELACIONADO,
	REGISTRO_CONTROLE_RELACIONADO = B.REGISTRO_CONTROLE_RELACIONADO,
	REGISTRO_CONTROLE_II_RELACIONADO = B.REGISTRO_CONTROLE_II_RELACIONADO
--WHEN NOT MATCHED BY SOURCE AND A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM AND A.REG_MASTER_ORIGEM = B.REG_MASTER_ORIGEM AND A.FORMULARIO_ORIGEM = B.FORMULARIO_ORIGEM THEN
--DELETE;
;

--------------------------------------------------------------------------------------------------------------
--     COLOCADA COMO TRIGGER POIS POR ALGUMA RAZ�O N�O ESTAVA LEVANDO PARA O T�TULO NA GRAVA��O FINAL       --
--------------------------------------------------------------------------------------------------------------
-- POR FILIPE OLIVEIRA EM 18/07/2017 �S 15H20MIN
/*

-- ATUALIZA VENDEDOR T�TULOS --
IF OBJECT_ID ('TEMPDB..#UPDATE_TITULOS_RECEBER') IS NOT NULL
   DROP TABLE #UPDATE_TITULOS_RECEBER

SELECT A.REG_MASTER_ORIGEM,
       A.TITULO_RECEBER,
       B.VENDEDOR,
       C.PREGAO,
       C.EMPENHO

  INTO #UPDATE_TITULOS_RECEBER
  FROM TITULOS_RECEBER            A WITH(NOLOCK) 
  JOIN NF_FATURAMENTO             B WITH(NOLOCK) ON B.NF_FATURAMENTO    = A.REG_MASTER_ORIGEM
                                                AND A.FORMULARIO_ORIGEM = (SELECT NUMID FROM FORMULARIOS WHERE FORMULARIO LIKE 'NF_FATURAMENTO')
  LEFT
  JOIN NF_FATURAMENTO_OBSERVACOES C WITH(NOLOCK) ON C.NF_FATURAMENTO = B.NF_FATURAMENTO
  WHERE 1=1
    AND A.REG_MASTER_ORIGEM = @NF_FATURAMENTO



UPDATE TITULOS_RECEBER
   SET VENDEDOR = B.VENDEDOR,
       PREGAO   = B.PREGAO,
       EMPENHO  = B.EMPENHO

  FROM TITULOS_RECEBER         A WITH(NOLOCK)
  JOIN #UPDATE_TITULOS_RECEBER B WITH(NOLOCK) ON B.TITULO_RECEBER = A.TITULO_RECEBER

*/