DECLARE @PAGAMENTO_INI    DATETIME
DECLARE @PAGAMENTO_FIM    DATETIME
DECLARE @EMPRESA_INI      NUMERIC
DECLARE @EMPRESA_FIM      NUMERIC
DECLARE @CLASSIF          INT
DECLARE @OBJETO_CONTROLE  INT
DECLARE @EMPRESA_CONTABIL INT
DECLARE @OPCAO            VARCHAR(1)--C = Comercializaveis, N = N�o Comerc. T = Todos   

SET @PAGAMENTO_INI    = '01/11/2018'
SET @PAGAMENTO_FIM    = '30/11/2018'    
SET @EMPRESA_INI      = 1    
SET @EMPRESA_FIM      = 9999999
--SET @CLASSIF          = NULL 
SET @OBJETO_CONTROLE  = 1007
--SET @EMPRESA_CONTABIL = NULL
--SET @OPCAO            = 'T'
                                                          
--SET @PAGAMENTO_INI    = :PAGAMENTO_INI
--SET @PAGAMENTO_FIM    = :PAGAMENTO_FIM
--SET @EMPRESA_INI      = CASE WHEN ISNUMERIC(:EMPRESA_INI    )  = 1  THEN :EMPRESA_INI      ELSE 1         END
--SET @EMPRESA_FIM      = CASE WHEN ISNUMERIC(:EMPRESA_FIM    )  = 1  THEN :EMPRESA_FIM      ELSE 999999999 END
--SET @CLASSIF          = CASE WHEN ISNUMERIC(:CLASS)            = 1  THEN :CLASS            ELSE NULL      END
--SET @OBJETO_CONTROLE  = CASE WHEN ISNUMERIC(:OBJETO_CONTROLE)  = 1  THEN :OBJETO_CONTROLE  ELSE NULL      END
--SET @EMPRESA_CONTABIL = CASE WHEN ISNUMERIC(:EMPRESA_CONTABIL) = 1  THEN :EMPRESA_CONTABIL ELSE NULL      END
--SET @OPCAO            = CASE WHEN :OPCAO                IN('C','N') THEN :OPCAO            ELSE 'T'       END    


SELECT 
       X.CHAVE                   AS CHAVE,
       @PAGAMENTO_INI            AS DATA_INI,
       @PAGAMENTO_FIM            AS DATA_FIM,
       X.TITULO_PAGAR            AS TITULO_PAGAR ,
       X.CLASSIF_FINANCEIRA      AS CLASSIF_FINANCEIRA , 
       V.DESCRICAO               AS CLASSIF_FINANCEIRA_DESCRICAO ,
       CASE WHEN X.CLASSIF_FINANCEIRA = K.CLASSIF_COMERCIALIZAVEIS
            THEN 'COMERCIALIZ�VEL'
            ELSE 'N�O COMERCIALIZ�VEL'
       END                       AS TIPO_DESPESA ,
       X.OBJETO_CONTROLE         AS OBJETO_CONTROLE,
       W.DESCRICAO               AS OBJETO_CONTROLE_DESCRICAO , 
       X.DATA_PAGAMENTO          AS DATA_PAGAMENTO ,
       X.VENCIMENTO              AS VENCIMENTO ,
       X.EMPRESA                 AS EMPRESA ,
       X.ENTIDADE                AS ENTIDADE ,
       X.TITULO                  AS TITULO ,
       X.NOME_ENTIDADE           AS NOME_ENTIDADE ,
       Z.INSCRICAO_FEDERAL       AS INSCRICAO_FEDERAL ,
       X.DESCR_CLASSIF           AS DESCR_CLASSIF ,
       X.CONTA                   AS CONTA ,
       X.CHEQUE                  AS CHEQUE ,      

       SUM ( X.VALOR ) + 
       SUM ( X.ABATIMENTO )      AS VALOR , 
       SUM ( X.ABATIMENTO + X.DESCONTO )
                                 AS ABAT_DESC , 
       SUM ( X.TARIFA_BANCARIA ) AS TARIFA_BANCARIA , 
       SUM ( X.JUROS + X.MULTA ) AS JUROS_MULTA , 

       SUM ( X.VALOR )           + 
       SUM ( X.TARIFA_BANCARIA ) +
       SUM ( X.JUROS )           + 
       SUM ( X.MULTA )           - 
       SUM ( X.DESCONTO )        AS TOTAL

  FROM ( --------------------------
         -- PAGAMENTOS EM CHEQUE --
         --------------------------

         SELECT 
                A.REG_MASTER_ORIGEM                                  AS CHAVE,
                B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA ,
                D.OBJETO_CONTROLE                                    AS OBJETO_CONTROLE ,
                A.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                B.VENCIMENTO                                         AS VENCIMENTO ,
                B.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                B.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                G.DESCRICAO                                          AS CONTA ,
                'CH ' + DBO.ZEROS (CONVERT (VARCHAR (6), A.CHEQUE), 6)
                                                                     AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO ,                 
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_CHEQUES         A WITH (NOLOCK)
           JOIN PAGAMENTOS_CHEQUES_TITULOS B WITH (NOLOCK) ON B.PAGAMENTO_CHEQUE  = A.PAGAMENTO_CHEQUE
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA ,
                         OBJETO_CONTROLE    AS OBJETO_CONTROLE ,
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA, OBJETO_CONTROLE ) D ON D.TITULO_PAGAR    = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN CONTAS_BANCARIAS           G WITH (NOLOCK) ON A.CONTA_BANCARIA     = G.CONTA_BANCARIA
            JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                            H               ON H.EMPRESA_USUARIA          = B.EMPRESA 

          WHERE A.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                A.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL)                                    AND
                A.PAGAMENTO_CHEQUE NOT IN (SELECT ISNULL(PAGAMENTO_CHEQUE,0) FROM CANCELAMENTOS_CHEQUES WHERE PAGAMENTO_CHEQUE IS NOT NULL)
       GROUP BY A.REG_MASTER_ORIGEM,
                B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO ,
                B.EMPRESA ,
                B.ENTIDADE ,
                B.TITULO  ,
                E.NOME ,
                F.DESCRICAO,
                G.DESCRICAO,
                A.CHEQUE,
                B.VENCIMENTO,
                D.OBJETO_CONTROLE


      UNION ALL

         -------------------------
         -- PAGAMENTOS EM CAIXA --
         -------------------------

         SELECT C.REG_MASTER_ORIGEM                                  AS CHAVE,
                B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA ,
                D.OBJETO_CONTROLE                                    AS OBJETO_CONTROLE ,
                A.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                B.VENCIMENTO                                         AS VENCIMENTO ,
                B.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                B.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                'CAIXA'                                              AS CONTA , 
                NULL                                                 AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO , 
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_CAIXA         A WITH (NOLOCK)
           JOIN PAGAMENTOS_CAIXA_TITULOS B WITH (NOLOCK) ON B.PAGAMENTO_CAIXA = A.PAGAMENTO_CAIXA
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         REG_MASTER_ORIGEM  AS REG_MASTER_ORIGEM,
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA ,
                         OBJETO_CONTROLE    AS OBJETO_CONTROLE ,
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA, OBJETO_CONTROLE ) D ON D.TITULO_PAGAR = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                            H               ON H.EMPRESA_USUARIA          = B.EMPRESA 

          WHERE A.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                A.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL)                                       AND
                A.PAGAMENTO_CAIXA NOT IN (SELECT ISNULL(PAGAMENTO_CAIXA,0) FROM CANCELAMENTOS_PAGAMENTOS WHERE PAGAMENTO_CAIXA IS NOT NULL)
       GROUP BY C.REG_MASTER_ORIGEM,
                B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO ,
                B.EMPRESA ,
                B.ENTIDADE ,
                B.TITULO ,
                E.NOME ,
                F.DESCRICAO ,
                B.VENCIMENTO,
                D.OBJETO_CONTROLE

      UNION ALL

         ----------------------------
         -- PAGAMENTOS ESCRITURAIS --
         ----------------------------

         SELECT C.REG_MASTER_ORIGEM                                  AS CHAVE,                            
                B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA ,
                D.OBJETO_CONTROLE                                    AS OBJETO_CONTROLE ,
                A.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                B.VENCIMENTO                                         AS VENCIMENTO ,
                B.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                B.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                G.DESCRICAO                                          AS CONTA ,
                NULL                                                 AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO ,
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_ESCRITURAIS         A WITH (NOLOCK)
           JOIN PAGAMENTOS_ESCRITURAIS_TITULOS B WITH (NOLOCK) ON B.PAGAMENTO_ESCRITURAL = A.PAGAMENTO_ESCRITURAL
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         REG_MASTER_ORIGEM  AS REG_MASTER_ORIGEM,
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA ,
                         OBJETO_CONTROLE    AS OBJETO_CONTROLE ,
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA, OBJETO_CONTROLE ) D ON D.TITULO_PAGAR = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN CONTAS_BANCARIAS           G WITH (NOLOCK) ON A.CONTA_BANCARIA     = G.CONTA_BANCARIA
            JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                            H               ON H.EMPRESA_USUARIA    = B.EMPRESA 

          WHERE A.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                A.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL)                                       AND
                 A.PAGAMENTO_ESCRITURAL NOT IN (SELECT ISNULL(PAGAMENTO_ESCRITURAL,0) FROM CANCELAMENTOS_PAGAMENTOS WHERE PAGAMENTO_ESCRITURAL IS NOT NULL)
       GROUP BY C.REG_MASTER_ORIGEM,
                B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO ,
                B.EMPRESA ,
                B.ENTIDADE ,
                B.TITULO ,
                E.NOME ,
                F.DESCRICAO,
                B.VENCIMENTO ,
                G.DESCRICAO,
                D.OBJETO_CONTROLE

                
      UNION ALL

         ----------------------------
         -- PAGAMENTOS ESCRITURAIS --
         ----------------------------

         SELECT C.REG_MASTER_ORIGEM                                  AS CHAVE,
                B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA ,
                D.OBJETO_CONTROLE                                    AS OBJETO_CONTROLE ,
                B.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                A.VENCIMENTO                                         AS VENCIMENTO ,
                A.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                A.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                G.DESCRICAO                                          AS CONTA ,
                NULL                                                 AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO ,
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_ELETRONICOS         B WITH (NOLOCK)
           INNER JOIN TITULOS_PAGAR            A WITH (NOLOCK) ON B.TITULO_PAGAR = A.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         REG_MASTER_ORIGEM  AS REG_MASTER_ORIGEM,
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA ,
                         OBJETO_CONTROLE    AS OBJETO_CONTROLE ,
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA, OBJETO_CONTROLE ) D ON D.TITULO_PAGAR = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN CONTAS_BANCARIAS           G WITH (NOLOCK) ON B.CONTA_BANCARIA     = G.CONTA_BANCARIA
            JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                            H               ON H.EMPRESA_USUARIA    = A.EMPRESA 

          WHERE B.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                B.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL) 

       GROUP BY C.REG_MASTER_ORIGEM,
                B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                B.DATA_PAGAMENTO ,
                A.EMPRESA ,
                B.ENTIDADE ,
                A.TITULO ,
                E.NOME ,
                F.DESCRICAO,
                A.VENCIMENTO ,
                G.DESCRICAO,
                D.OBJETO_CONTROLE     

       
                ) X 
 

JOIN TITULOS_PAGAR      Y WITH(NOLOCK) ON Y.TITULO_PAGAR = X.TITULO_PAGAR
JOIN ENTIDADES          Z WITH(NOLOCK) ON Z.ENTIDADE     = Y.ENTIDADE
LEFT
JOIN PARAMETROS_FINANCEIRO K WITH(NOLOCK)ON K.GRUPO_EMPRESARIAL = 1
LEFT
JOIN OBJETOS_CONTROLE      W WITH(NOLOCK) ON W.OBJETO_CONTROLE = X.OBJETO_CONTROLE
LEFT
JOIN CLASSIF_FINANCEIRAS   V WITH(NOLOCK) ON V.CLASSIF_FINANCEIRA = X.CLASSIF_FINANCEIRA
WHERE   ( X.OBJETO_CONTROLE = @OBJETO_CONTROLE OR @OBJETO_CONTROLE IS NULL ) AND
        (@OPCAO = 'T' OR
        (DBO.TITULOS_PAGAR_COMERCIALIZAVEIS(X.TITULO_PAGAR) = 1 AND @OPCAO = 'C') OR
        (DBO.TITULOS_PAGAR_COMERCIALIZAVEIS(X.TITULO_PAGAR) = 0 AND @OPCAO = 'N'))

GROUP BY X.CHAVE        ,
         X.TITULO_PAGAR ,
         X.CLASSIF_FINANCEIRA , 
         X.DATA_PAGAMENTO ,
         X.EMPRESA ,
         X.ENTIDADE ,
         X.TITULO ,
         X.NOME_ENTIDADE ,
         X.DESCR_CLASSIF ,
         Z.INSCRICAO_FEDERAL,
         X.CONTA ,
         X.CHEQUE,
         X.VENCIMENTO,
       CASE WHEN X.CLASSIF_FINANCEIRA = K.CLASSIF_COMERCIALIZAVEIS
            THEN 'COMERCIALIZ�VEL'
            ELSE 'N�O COMERCIALIZ�VEL'
       END,
       X.OBJETO_CONTROLE,
       W.DESCRICAO,
       V.DESCRICAO      

UNION ALL
  ------------------------------------
  ---------- COFRES DE LOJA ----------
  ------------------------------------

  SELECT
         A.DESPESA_COFRE              AS CHAVE,                                                             
         @PAGAMENTO_INI               AS DATA_INI,                                                            
         @PAGAMENTO_FIM               AS DATA_FIM,                                                            
         NULL                         AS TITULO_PAGAR ,                                                     
         A.CLASSIF_FINANCEIRA         AS CLASSIF_FINANCEIRA ,
         V.DESCRICAO                  AS CLASSIF_FINANCEIRA_DESCRICAO ,                                                 
         CASE WHEN A.CLASSIF_FINANCEIRA = B.CLASSIF_COMERCIALIZAVEIS                                        
            THEN 'COMERCIALIZ�VEL'                                                                            
            ELSE 'N�O COMERCIALIZ�VEL'                                                                        
         END                          AS TIPO_DESPESA ,
         A.OBJETO_CONTROLE            AS OBJETO_CONTROLE , 
         F.DESCRICAO                  AS OBJETO_CONTROLE_DESC ,                                                    
         A.MOVIMENTO                  AS DATA_PAGAMENTO ,                                                     
         NULL                         AS VENCIMENTO ,                                                        
         A.EMPRESA                    AS EMPRESA ,                                                            
         A.ENTIDADE                   AS ENTIDADE ,                                                            
         NULL                         AS TITULO ,                                                            
         D.NOME                       AS NOME_ENTIDADE ,                                                    
         D.INSCRICAO_FEDERAL          AS INSCRICAO_FEDERAL ,                                                
         E.DESCRICAO                  AS DESCR_CLASSIF ,                                                    
        'DESP.COFR'                   AS CONTA ,
         NULL                           AS CHEQUE,                                
         A.DESPESA                    AS VALOR ,                                                                                   
         NULL                         AS ABAT_DESC ,                                                                                    
         NULL                         AS TARIFA_BANCARIA ,                                                                                
         NULL                         AS JUROS_MULTA ,                                                        
         A.DESPESA                    AS PAGAMENTO_TOTAL                                                    
    FROM DESPESAS_COFRES        A WITH(NOLOCK)                                                              
    LEFT                                                                                                     
    JOIN PARAMETROS_FINANCEIRO  B WITH(NOLOCK) ON B.CLASSIF_COMERCIALIZAVEIS = A.CLASSIF_FINANCEIRA            
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                C              ON C.EMPRESA_USUARIA          = A.EMPRESA                     
    JOIN ENTIDADES              D WITH(NOLOCK) ON D.ENTIDADE                 = A.ENTIDADE                    
    JOIN CLASSIF_FINANCEIRAS    E WITH(NOLOCK) ON E.CLASSIF_FINANCEIRA       = A.CLASSIF_FINANCEIRA            
    LEFT
    JOIN OBJETOS_CONTROLE       F WITH(NOLOCK) ON F.OBJETO_CONTROLE          = A.OBJETO_CONTROLE
    LEFT
    JOIN CLASSIF_FINANCEIRAS    V WITH(NOLOCK) ON V.CLASSIF_FINANCEIRA       = E.CLASSIF_FINANCEIRA
   WHERE  ( A.OBJETO_CONTROLE = @OBJETO_CONTROLE OR @OBJETO_CONTROLE IS NULL ) AND
          A.MOVIMENTO           >= @PAGAMENTO_INI                            AND                            
          A.MOVIMENTO           <= @PAGAMENTO_FIM                            AND                            
         (A.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL)               AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (B.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (B.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S'))
UNION ALL
  ---------------------------------------
  ---------- DESPESAS DE CAIXA ----------
  ---------------------------------------
  SELECT
         A.CAIXA_SAIDA                AS CHAVE,                                                                                          
         @PAGAMENTO_INI               AS DATA_INI,                                                                                         
         @PAGAMENTO_FIM               AS DATA_FIM,                                                                                         
         NULL                         AS TITULO_PAGAR ,                                                                                  
         A.CLASSIF_FINANCEIRA         AS CLASSIF_FINANCEIRA ,
         V.DESCRICAO                  AS CLASSIF_FINANCEIRA_DESCRICAO ,                                                                                          
         CASE WHEN A.CLASSIF_FINANCEIRA = B.CLASSIF_COMERCIALIZAVEIS                                                                     
            THEN 'COMERCIALIZ�VEL'                                                                                                         
            ELSE 'N�O COMERCIALIZ�VEL'                                                                                                     
         END                          AS TIPO_DESPESA ,                                                                                  
         A.OBJETO_CONTROLE            AS OBJETO_CONTROLE , 
         F.DESCRICAO                  AS OBJETO_CONTROLE_DESC ,
         A.MOVIMENTO                  AS DATA_PAGAMENTO ,                                                                                  
         NULL                         AS VENCIMENTO ,                                                                                     
         A.EMPRESA                    AS EMPRESA ,                                                                                         
         A.ENTIDADE                   AS ENTIDADE ,                                                                                         
         NULL                         AS TITULO ,                                                                                         
         D.NOME                       AS NOME_ENTIDADE ,    
         D.INSCRICAO_FEDERAL          AS INSCRICAO_FEDERAL ,                                                                             
         E.DESCRICAO                  AS DESCR_CLASSIF ,                                                                                 
         'DESP.CAIXA'                 AS CONTA ,
         NULL                           AS CHEQUE,                                                                                         
         A.VALOR                      AS VALOR ,                                                                                         
         NULL                         AS ABAT_DESC ,                                                                                    
         NULL                         AS TARIFA_BANCARIA ,                                                                                
         NULL                         AS JUROS_MULTA ,                                                                                    
         A.VALOR                      AS PAGAMENTO_TOTAL                                                                                 
                                                                                                                                         
    FROM CAIXA_SAIDAS           A WITH(NOLOCK)  
    LEFT                                                                                                                                  
    JOIN PARAMETROS_FINANCEIRO  B WITH(NOLOCK) ON B.CLASSIF_COMERCIALIZAVEIS = A.CLASSIF_FINANCEIRA                                         
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL)                                                              
                                C              ON C.EMPRESA_USUARIA          = A.EMPRESA                                                  
    JOIN ENTIDADES              D WITH(NOLOCK) ON D.ENTIDADE                 = A.ENTIDADE                                                 
    JOIN CLASSIF_FINANCEIRAS    E WITH(NOLOCK) ON E.CLASSIF_FINANCEIRA       = A.CLASSIF_FINANCEIRA
    LEFT
    JOIN OBJETOS_CONTROLE       F WITH(NOLOCK) ON F.OBJETO_CONTROLE          = A.OBJETO_CONTROLE
    LEFT
    JOIN CLASSIF_FINANCEIRAS    V WITH(NOLOCK) ON V.CLASSIF_FINANCEIRA       = E.CLASSIF_FINANCEIRA
   WHERE  ( A.OBJETO_CONTROLE = @OBJETO_CONTROLE OR @OBJETO_CONTROLE IS NULL ) AND
          A.MOVIMENTO           >= @PAGAMENTO_INI                            AND
          A.MOVIMENTO           <= @PAGAMENTO_FIM                            AND
         (A.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL)               AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (B.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (B.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S'))

UNION ALL
  ---------------------------------------
  ---------- PRESTA��ES DE CAIXA --------
  ---------------------------------------
 
 SELECT
         A.CAIXA_PRESTACAO_CONTA      AS CHAVE,                                                                                          
         @PAGAMENTO_INI               AS DATA_INI,                                                                                         
         @PAGAMENTO_FIM               AS DATA_FIM,                                                                                         
         NULL                         AS TITULO_PAGAR ,                                                                                  
         C.CLASSIF_FINANCEIRA         AS CLASSIF_FINANCEIRA ,
         V.DESCRICAO                  AS CLASSIF_FINANCEIRA_DESCRICAO ,                                                                                          
         CASE WHEN C.CLASSIF_FINANCEIRA = G.CLASSIF_COMERCIALIZAVEIS                                                                     
            THEN 'COMERCIALIZ�VEL'                                                                                                         
            ELSE 'N�O COMERCIALIZ�VEL'                                                                                                     
         END                          AS TIPO_DESPESA ,
         C.OBJETO_CONTROLE            AS OBJETO_CONTROLE , 
         H.DESCRICAO                  AS OBJETO_CONTROLE_DESC ,                                                                                           
         A.MOVIMENTO                  AS DATA_PAGAMENTO ,                                                                                  
         NULL                         AS VENCIMENTO ,                                                                                     
         A.EMPRESA                    AS EMPRESA ,                                                                                         
         A.ENTIDADE                   AS ENTIDADE ,                                                                                         
         C.NUMERO_DOCUMENTO           AS TITULO ,                                                                                         
         E.NOME                       AS NOME_ENTIDADE ,    
         E.INSCRICAO_FEDERAL          AS INSCRICAO_FEDERAL ,                                                                             
         F.DESCRICAO                  AS DESCR_CLASSIF ,                                                                                 
         'PREST.CONT'                 AS CONTA ,
         NULL                         AS CHEQUE,                                                                                         
         C.VALOR                      AS VALOR ,                                                                                         
         NULL                         AS ABAT_DESC ,                                                                                    
         NULL                         AS TARIFA_BANCARIA ,                                                                                
         NULL                         AS JUROS_MULTA ,                                                                                    
         C.VALOR                      AS PAGAMENTO_TOTAL   
         
    FROM CAIXA_PRESTACOES_CONTA       A WITH(NOLOCK)
    JOIN CAIXA_PRESTACOES_TOTAIS      B WITH(NOLOCK) ON B.CAIXA_PRESTACAO_CONTA = A.CAIXA_PRESTACAO_CONTA 
    JOIN CAIXA_PRESTACOES_DESPESAS    C WITH(NOLOCK) ON C.CAIXA_PRESTACAO_CONTA = A.CAIXA_PRESTACAO_CONTA 
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                      D              ON D.EMPRESA_USUARIA          = A.EMPRESA 
    JOIN ENTIDADES                    E WITH(NOLOCK) ON E.ENTIDADE                 = A.ENTIDADE
    JOIN CLASSIF_FINANCEIRAS          F WITH(NOLOCK) ON F.CLASSIF_FINANCEIRA       = C.CLASSIF_FINANCEIRA
    LEFT                                                                                                                                  
    JOIN PARAMETROS_FINANCEIRO        G WITH(NOLOCK) ON G.CLASSIF_COMERCIALIZAVEIS = C.CLASSIF_FINANCEIRA                                         
    LEFT
    JOIN OBJETOS_CONTROLE             H WITH(NOLOCK) ON H.OBJETO_CONTROLE          = C.OBJETO_CONTROLE
    LEFT
    JOIN CLASSIF_FINANCEIRAS          V WITH(NOLOCK) ON V.CLASSIF_FINANCEIRA       = F.CLASSIF_FINANCEIRA
   WHERE  ( C.OBJETO_CONTROLE = @OBJETO_CONTROLE OR @OBJETO_CONTROLE IS NULL ) AND
          A.MOVIMENTO       >= @PAGAMENTO_INI                            AND
          A.MOVIMENTO       <= @PAGAMENTO_FIM                            AND
         (C.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF IS NULL)           AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (G.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (G.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S')) AND     
          B.TOTAL_DESPESAS  > 0
