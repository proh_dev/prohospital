ALTER PROCEDURE [dbo].[USP_GERAR_NF_COMPRA_NF_FATURAMENTO_RETORNO] ( @NF_FATURAMENTO AS NUMERIC(15) )  AS  
                                                
  
BEGIN  
  
/*----------------------------*/  
/* DECLARA��O DE VARI�VEIS  --*/  
/*----------------------------*/  
  
DECLARE @FORMULARIO_ORIGEM   NUMERIC(15,0),  
        @TAB_MASTER_ORIGEM   NUMERIC(15,0),  
        @NF_COMPRA           NUMERIC(15,0)  
  
/*--------------------------------*/  
/* IDENTIFICA FORMULARIO ORIGEM --*/  
/*--------------------------------*/  
  
SELECT @TAB_MASTER_ORIGEM = NUMID  
  FROM TABELAS   
 WHERE TABELA = 'NF_COMPRA'  
   
 SELECT @FORMULARIO_ORIGEM = NUMID  
  FROM FORMULARIOS   
 WHERE FORMULARIO = 'NF_COMPRA'  
  
  
---------------------------------------------------------  
-- GERA TEMPORARIA COM OS DADOS DA NOTA DE FATURAMENTO --  
---------------------------------------------------------  
  
if object_id('tempdb..#NF_FATURAMENTO') is not null  
   DROP TABLE #NF_FATURAMENTO  
  
SELECT A.NF_FATURAMENTO  AS NF_FATURAMENTO            ,  
       A.EMPRESA         AS NF_FATURAMENTO_EMPRESA    ,  
       A.ENTIDADE        AS NF_FATURAMENTO_ENTIDADE   ,  
       C.EMPRESA_USUARIA AS NF_COMPRA_EMPRESA         ,  
       B.ENTIDADE        AS NF_COMPRA_ENTIDADE        ,  
       A.NF_NUMERO       AS NF_NUMERO                 ,  
       A.NF_SERIE        AS NF_SERIE                  ,  
       A.EMISSAO         AS EMISSAO                   ,  
       A.MOVIMENTO       AS MOVIMENTO                 ,  
         
    CASE WHEN A.OPERACAO_FISCAL = 143 THEN  144  
         WHEN A.OPERACAO_FISCAL = 129 THEN  142   
   ELSE 171   
    END AS NF_COMPRA_OPERACAO_FISCAL ,  
         
      
    G.OBJETO_CONTROLE_COMPRAS  AS OBJETO_CONTROLE           ,  
       F.TOTAL_GERAL     AS TOTAL_GERAL               ,  
       A.USUARIO_LOGADO  AS USUARIO_LOGADO            ,  
       NULL              AS PEDIDO_COMPRA  
         
  INTO #NF_FATURAMENTO  
         
  FROM NF_FATURAMENTO        A WITH(NOLOCK)  
  JOIN EMPRESAS_USUARIAS     B WITH(NOLOCK) ON A.EMPRESA         = B.EMPRESA_USUARIA  
  JOIN EMPRESAS_USUARIAS     C WITH(NOLOCK) ON A.ENTIDADE        = C.ENTIDADE  
  JOIN OPERACOES_FISCAIS     D WITH(NOLOCK) ON A.OPERACAO_FISCAL = D.OPERACAO_FISCAL  
  JOIN NF_FATURAMENTO_TOTAIS F WITH(NOLOCK) ON A.NF_FATURAMENTO  = F.NF_FATURAMENTO  
  JOIN PARAMETROS_COMPRAS    G WITH(NOLOCK) ON C.EMPRESA_USUARIA = G.EMPRESA_USUARIA  
  LEFT JOIN PEDIDOS_COMPRAS  H WITH(NOLOCK) ON A.NF_FATURAMENTO  = H.NF_FATURAMENTO  
    
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO  
   AND D.CONTRAPARTIDA_AUTOMATICA = 'S'  
   AND A.CONTRAPARTIDA_GERADA = 'N'
  
  
IF @@ROWCOUNT = 0 RETURN  
  
  
--PEGA A CHAVE DA NFE--  
DECLARE @CHAVE_NFE VARCHAR(50)  
  
SELECT @CHAVE_NFE = C.CHAVE_NFE  
 FROM NF_FATURAMENTO                          A WITH(NOLOCK)  
 JOIN NFE_CABECALHO                           C WITH(NOLOCK) ON C.XML_cNF_B03    = A.NF_FATURAMENTO  
                                                            AND C.XML_nNF_B08    = A.NF_NUMERO       
                                                            AND C.XML_serie_B07  = A.NF_SERIE        
                                                            AND C.EMPRESA        = A.EMPRESA  
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO  
  
  
/*******************************************************************************--*/  
/*--------------------------------------------------------------------------------*/  
/*--------------------------INCLUS�O MASTER NF_COMPRA ----------------------------*/  
/*--------------------------------------------------------------------------------*/  
/*******************************************************************************--*/  
  
INSERT INTO NF_COMPRA (  
  
       FORMULARIO_ORIGEM,   
       TAB_MASTER_ORIGEM,   
       EMPRESA,   
       ENTIDADE,   
       NF_ESPECIE,   
       NF_NUMERO,   
       NF_SERIE,   
    MODELO,  
       EMISSAO,   
       MOVIMENTO,   
       TOTAL_GERAL,   
       PROCESSAR,   
       DATA_HORA,   
       USUARIO_LOGADO,   
       PEDIDO_COMPRA,   
       ENCERRAR_PEDIDO_TOTAL,   
  RECEBIMENTO,  
       NF_FATURAMENTO,  
    STATUS_PROCESSAMENTO_INTEGRACAO,  
       CHAVE_NFE  ,
	  CONTRAPARTIDA_GERADA
  
)  
  
    SELECT   @FORMULARIO_ORIGEM                                  AS FORMULARIO_ORIGEM     ,  
             @TAB_MASTER_ORIGEM                                  AS TAB_MASTER_ORIGEM     ,   
             A.NF_COMPRA_EMPRESA                                 AS EMPRESA               ,  
             A.NF_COMPRA_ENTIDADE                                AS ENTIDADE              ,   
             'NFE'                                               AS NF_ESPECIE            ,  
             A.NF_NUMERO                                         AS NF_NUMERO             ,  
             A.NF_SERIE                                          AS NF_SERIE              ,  
    '55'                                                AS MODELO                ,  
             A.EMISSAO                                           AS EMISSAO               ,  
             CONVERT(DATETIME, CONVERT(VARCHAR, GETDATE(), 103)) AS MOVIMENTO             ,               
             A.TOTAL_GERAL                                       AS TOTAL_GERAL           ,                
             'N'                                                 AS PROCESSAR             ,   
             GETDATE()                                           AS DATA_HORA             ,   
             A.USUARIO_LOGADO                                    AS USUARIO_LOGADO        ,   
             A.PEDIDO_COMPRA                                     AS PEDIDO_COMPRA         ,  
             'S'                                                 AS ENCERRAR_PEDIDO_TOTAL ,   
             NULL                                                AS RECEBIMENTO           ,  
             A.NF_FATURAMENTO                                    AS NF_FATURAMENTO,  
    0                                                   AS STATUS_PROCESSAMENTO_INTEGRACAO,  
             @CHAVE_NFE                                          AS CHAVE_NFE  ,
			 'S'                                                 AS CONTRAPARTIDA_GERADA
  
  
      FROM      #NF_FATURAMENTO A WITH(NOLOCK)  
      LEFT JOIN NF_COMPRA       B WITH(NOLOCK) ON B.NF_NUMERO  = A.NF_NUMERO  
                                              AND B.NF_ESPECIE = 'NFE'  
             AND B.NF_SERIE   = A.NF_SERIE  
                                              AND B.ENTIDADE   = A.NF_COMPRA_ENTIDADE  
                                         
WHERE B.NF_COMPRA IS NULL  
  
PRINT 'CABE�ALHO OK'  
  
SET @NF_COMPRA = SCOPE_IDENTITY()  
      
                                         
IF @NF_COMPRA >0  
  
BEGIN  
  
  
/*FIM DA INCLUSAO DA MASTER--*/  
  
  
/*******************************************************************************--*/  
/*--------------------------------------------------------------------------------*/  
/*--------------------------INCLUS�O DETAIL DE PRODUTOS---------------------------*/  
/*--------------------------------------------------------------------------------*/  
/*******************************************************************************--*/  
  
DELETE FROM NF_COMPRA_PRODUTOS WHERE NF_COMPRA = @NF_COMPRA  
  
  
INSERT INTO NF_COMPRA_PRODUTOS (  
             FORMULARIO_ORIGEM,  
             TAB_MASTER_ORIGEM,  
             REG_MASTER_ORIGEM,   
             NF_COMPRA,   
             PEDIDO_COMPRA_PRODUTO,   
             PEDIDO_COMPRA,   
             REFERENCIA,   
             PRODUTO,   
             QUANTIDADE,   
    QUANTIDADE_MULTIPLO,  
             QUANTIDADE_EMBALAGEM,   
             UNIDADE_MEDIDA,   
             VALOR_UNITARIO,   
             TIPO_DESCONTO,   
             DESCONTO,   
             TOTAL_DESCONTO,   
             TOTAL_PRODUTO,   
             IPI_ALIQUOTA,   
             IPI_VALOR,   
             IPI_CREDITO,   
             CLASSIF_FISCAL_CODIGO,   
             SITUACAO_TRIBUTARIA,   
             IPI_ICMS,   
             ICMS_BASE_ORIGINAL,  
             ICMS_BASE_CALCULO,  
             ICMS_REDUCAO_BASE,   
             ICMS_ALIQUOTA,   
             ICMS_VALOR,   
             ICMS_CREDITO,   
             ICMS_ST_BASE_CALCULO,  
ICMS_ST_VALOR,  
             PIS_VALOR,  
             COFINS_VALOR,  
             OBJETO_CONTROLE,   
             QUANTIDADE_ESTOQUE,   
             PRECO_FABRICA,   
             PRECO_MAXIMO,   
             PRECO_VENDA,  
             CONFIRMAR,   
             IVA,   
             IVA_AJUSTADO,   
             LOTE,   
             VALIDADE,   
    VALIDADE_DIGITACAO,  
             OPERACAO_FISCAL ,  
             ICMS_ALIQUOTA_DECRETO_35346 ,  
             ICMS_VALOR_DECRETO_35346 )  
               
SELECT       @FORMULARIO_ORIGEM                                                         AS FORMULARIO_ORIGEM        ,  
             @TAB_MASTER_ORIGEM                                                         AS TAB_MASTER_ORIGEM        ,  
             @NF_COMPRA                                                                 AS REG_MASTER_ORIGEM        ,  
             @NF_COMPRA                                                                 AS NF_COMPRA                ,  
             NULL                                                                       AS PEDIDO_COMPRA_PRODUTO    ,  
             NULL                                                                       AS PEDIDO_COMPRA            ,  
             A.REFERENCIA                                                               AS REFERENCIA               ,  
             A.PRODUTO                                                                  AS PRODUTO                  ,  
             A.QUANTIDADE                                                               AS QUANTIDADE               ,  
    1                                                                          AS QUANTIDADE_MULTIPLO,  
             A.QUANTIDADE_EMBALAGEM                                                     AS QUANTIDADE_EMBALAGEM     ,  
             A.UNIDADE_MEDIDA                                                           AS UNIDADE_MEDIDA           ,  
             A.VALOR_UNITARIO                                                           AS VALOR_UNITARIO           ,  
             A.TIPO_DESCONTO                                                            AS TIPO_DESCONTO            ,  
             A.DESCONTO                                                                 AS DESCONTO                 ,  
             A.TOTAL_DESCONTO                                                           AS TOTAL_DESCONTO           ,  
             A.TOTAL_PRODUTO                                                            AS TOTAL_PRODUTO            ,  
             A.IPI_ALIQUOTA                                                             AS IPI_ALIQUOTA             ,  
             A.IPI_VALOR                                                                AS IPI_VALOR                ,  
             'N'                                                                        AS IPI_CREDITO              ,  
             A.CLASSIF_FISCAL_CODIGO                                                    AS CLASSIF_FISCAL_CODIGO    ,  
             A.SITUACAO_TRIBUTARIA                                                      AS SITUACAO_TRIBUTARIA      ,  
             'N'                                                                        AS IPI_ICMS                 ,  
             A.ICMS_BASE_ORIGINAL                                                       AS ICMS_BASE_ORIGINAL       ,  
             A.ICMS_BASE_CALCULO                                                        AS ICMS_BASE_ICMS           ,  
             A.ICMS_REDUCAO_BASE                                                        AS ICMS_REDUCAO_BASE        ,  
             A.ICMS_ALIQUOTA                                                            AS ICMS_ALIQUOTA            ,  
             A.ICMS_VALOR                                                               AS ICMS_VALOR               ,  
             CASE WHEN (SUBSTRING(A.SITUACAO_TRIBUTARIA,2,2) = '00' OR   
                        SUBSTRING(A.SITUACAO_TRIBUTARIA,2,2) = '20')  
                  THEN 'S'   
  ELSE 'N' END                                                               AS ICMS_CREDITO                ,  
             A.ICMS_SUBST_BASE                                                          AS ICMS_ST_BASE_CALCULO        ,  
             A.ICMS_SUBST_VALOR                                                         AS ICMS_ST_VALOR               ,  
             A.PIS_VALOR                                                                AS PIS_VALOR                   ,  
             A.COFINS_VALOR                                                             AS COFINS_VALOR                ,  
             B.OBJETO_CONTROLE                                                          AS OBJETO_CONTROLE             ,  
             A.QUANTIDADE_ESTOQUE                                                       AS QUANTIDADE_ESTOQUE          ,  
             ISNULL(C.PRECO_FABRICA, 0)                                                 AS PRECO_FABRICA               ,  
             ISNULL(C.PRECO_MAXIMO, 0)                                                  AS PRECO_MAXIMO                ,  
             ISNULL(C.PRECO_VENDA, 0)                                                   AS PRECO_VENDA                 ,  
             'S'                                                                        AS CONFIRMAR                   ,  
             A.IVA                                                                      AS IVA                         ,  
             A.IVA_AJUSTADO                                                             AS IVA_AJUSTADO                ,  
             A.LOTE                                                                     AS LOTE                        ,               
             A.VALIDADE                                                                 AS VALIDADE                    ,  
    A.VALIDADE_DIGITACAO                                                       AS VALIDADE_DIGITACAO ,  
             B.NF_COMPRA_OPERACAO_FISCAL                                                AS OPERACAO_FISCAL             ,  
             A.ICMS_ALIQUOTA_DECRETO_35346                                              AS ICMS_ALIQUOTA_DECRETO_35346 ,  
             A.ICMS_VALOR_DECRETO_35346                                                 AS ICMS_VALOR_DECRETO_35346  
  
     FROM NF_FATURAMENTO_PRODUTOS A WITH(NOLOCK)  
     JOIN #NF_FATURAMENTO         B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO  
LEFT JOIN PRODUTOS_VENDAS         C WITH(NOLOCK) ON C.PRODUTO        = A.PRODUTO  
                                             AND C.GRUPO_PRECO = 1  
  
  
PRINT 'ITENS OK'  
  
/*-- FIM DA INSERCAO DE PRODUTOS --*/  
  
  
/*******************************************************************************--*/  
/*--------------------------------------------------------------------------------*/  
/*--------------------------INCLUS�O DETAIL DE PARCELAS---------------------------*/  
/*--------------------------------------------------------------------------------*/  
/*******************************************************************************--*/  
  
DELETE FROM NF_COMPRA_PARCELAS WHERE NF_COMPRA = @NF_COMPRA  
  
INSERT INTO NF_COMPRA_PARCELAS (  
  
             FORMULARIO_ORIGEM,   
             TAB_MASTER_ORIGEM,   
             REG_MASTER_ORIGEM,   
             NF_COMPRA,   
             DIAS,   
             VENCIMENTO,   
             PERCENTUAL,   
             VALOR,   
             TITULO,   
             MULTA_ATRASO,   
             JUROS_ATRASO,   
             DESCONTO_VALOR,   
             DESCONTO_ATE,   
             LOCAL_COBRANCA,   
             CLASSIF_FINANCEIRA )  
  
    SELECT   @FORMULARIO_ORIGEM            AS FORMULARIO_ORIGEM  ,  
             @TAB_MASTER_ORIGEM            AS TAB_MASTER_ORIGEM  ,  
             @NF_COMPRA                    AS REG_MASTER_ORIGEM  ,  
             @NF_COMPRA                    AS NF_COMPRA          ,  
             A.DIAS                        AS DIAS               ,  
             A.VENCIMENTO    AS VENCIMENTO     ,  
             A.PERCENTUAL                  AS PERCENTUAL         ,  
             A.VALOR                        AS VALOR              ,  
             A.TITULO                      AS TITULO             ,  
             0                             AS MULTA_ATRASO       ,  
             0                             AS JUROS_ATRASO       ,  
             0                             AS DESCONTO_VALOR     ,  
             NULL                          AS DESCONTO_ATE       ,  
             NULL                          AS LOCAL_COBRANCA     ,  
             C.CLASSIF_FINANCEIRA          AS CLASSIF_FINANCEIRA  
  
     FROM      NF_FATURAMENTO_PARCELAS A WITH(NOLOCK)  
          JOIN #NF_FATURAMENTO         B WITH(NOLOCK) ON A.NF_FATURAMENTO  = B.NF_FATURAMENTO  
     LEFT JOIN PARAMETROS_COMPRAS      C WITH(NOLOCK) ON C.EMPRESA_USUARIA = B.NF_COMPRA_EMPRESA  
  
PRINT 'PARCELAS OK'  
  
/*FIM DA INSERCAO DAS PARCELAS --*/  
  
  
/****************************************************************--*/  
/*--------------INSERCAO DO LOTE E VALIDADE------------------------*/  
/****************************************************************--*/  
  
INSERT INTO PRODUTOS_LOTE_VALIDADE (   
  
            FORMULARIO_ORIGEM ,  
            TAB_MASTER_ORIGEM ,  
            REG_MASTER_ORIGEM ,  
            PRODUTO ,  
            LOTE ,  
            VALIDADE_DIGITACAO ,  
            VALIDADE  
             )  
  
SELECT DISTINCT  
       A.FORMULARIO_ORIGEM,  
       A.TAB_MASTER_ORIGEM,  
       A.REG_MASTER_ORIGEM,  
       A.PRODUTO,  
       A.LOTE,  
       A.VALIDADE_DIGITACAO ,  
       A.VALIDADE  
  
  FROM NF_COMPRA_LOTE_VALIDADE             A WITH(NOLOCK)  
     JOIN NF_COMPRA                        C WITH(NOLOCK) ON C.NF_COMPRA  = A.NF_COMPRA  
LEFT JOIN PRODUTOS_LOTE_VALIDADE           B WITH(NOLOCK) ON B.PRODUTO    = A.PRODUTO  
                                                         AND B.LOTE       = A.LOTE  
                                                         AND B.VALIDADE_DIGITACAO   = A.VALIDADE_DIGITACAO  
  
WHERE A.NF_COMPRA = @NF_COMPRA  
  AND B.PRODUTO  IS NULL  
  AND A.VALIDADE IS NOT NULL  
  
  
PRINT 'LOTE VALIDADE OK'  
  
/*FIM DA INSERCAO DE PRODUTOS --*/  
  
  
  
/*******************************************************************************--*/  
/*--------------------------------------------------------------------------------*/  
/*--------------------------INCLUS�O DETAIL DE TOTAIS-----------------------------*/  
/*--------------------------------------------------------------------------------*/  
/*******************************************************************************--*/  
  
DELETE FROM NF_COMPRA_TOTAIS WHERE NF_COMPRA = @NF_COMPRA  
  
INSERT INTO NF_COMPRA_TOTAIS (  
   
             FORMULARIO_ORIGEM,   
             TAB_MASTER_ORIGEM,   
             REG_MASTER_ORIGEM,   
             NF_COMPRA,   
             TOTAL_PRODUTOS,  
             TOTAL_IPI,  
             TOTAL_DESPESAS,  
             TOTAL_FRETE,  
             TOTAL_SEGURO,  
             TOTAL_SUBSTITUICAO,  
             TOTAL_REPASSE,  
             TOTAL_SERVICOS,  
             TOTAL_GERAL,  
             ICMS_BASE_CALCULO,  
             ICMS_VALOR,  
             ISS_ALIQUOTA,  
             ISS_VALOR,  
             DESCONTO_FINANCEIRO,  
             VALOR_PAGAR_ST_FORNECEDOR )  
  
    SELECT   @FORMULARIO_ORIGEM            AS FORMULARIO_ORIGEM         ,  
             @TAB_MASTER_ORIGEM            AS TAB_MASTER_ORIGEM         ,  
             @NF_COMPRA                    AS REG_MASTER_ORIGEM         ,  
             @NF_COMPRA                    AS NF_COMPRA                 ,  
             A.TOTAL_PRODUTOS              AS TOTAL_PRODUTOS            ,  
             A.TOTAL_IPI                   AS TOTAL_IPI                 ,  
             A.TOTAL_DESPESAS              AS TOTAL_DESPESAS            ,  
             A.TOTAL_FRETE                 AS TOTAL_FRETE               ,  
             A.TOTAL_SEGURO                AS TOTAL_SEGURO              ,  
A.ICMS_VALOR_SUBST   AS TOTAL_SUBSTITUICAO        ,  
             0                             AS TOTAL_REPASSE             ,  
             0                             AS TOTAL_SERVICOS            ,  
             A.TOTAL_GERAL                 AS TOTAL_GERAL               ,  
             A.ICMS_BASE_CALCULO           AS ICMS_BASE_CALCULO         ,  
             ISNULL(A.ICMS_VALOR,0)        AS ICMS_VALOR                ,  
             0                             AS ISS_ALIQUOTA              ,  
             0                             AS ISS_VALOR                 ,  
             0                             AS DESCONTO_FINANCEIRO       ,  
             0                             AS VALOR_PAGAR_ST_FORNECEDOR   
               
     FROM NF_FATURAMENTO_TOTAIS A WITH(NOLOCK)  
     JOIN #NF_FATURAMENTO       B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO  
               
  
PRINT 'TOTAIS OK'  
  
END  
  
END /* END DA PROCEDURE          --*/