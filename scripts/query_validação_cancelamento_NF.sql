DECLARE 
       @NF_ESPECIE      VARCHAR(10) ,
       @NF_SERIE        VARCHAR(10) ,
       @NF_NUMERO       NUMERIC(15) ,
       @EMPRESA         NUMERIC(15) ,
       @NF_CANCELAMENTO NUMERIC(15) ,
       @REGISTRO_NFE    NUMERIC(15) ,
       @ID_NOTA         VARCHAR(60)

SELECT
       @NF_ESPECIE      = :NF_ESPECIE      ,
       @NF_SERIE        = :NF_SERIE        ,
       @NF_NUMERO       = :NF_NUMERO       ,
       @EMPRESA         = :EMPRESA         ,
       @NF_CANCELAMENTO = :NF_CANCELAMENTO ,
       @REGISTRO_NFE    = :REGISTRO_NFE    ,
       @ID_NOTA         = :ID_NOTA     

--SELECT
--       @NF_ESPECIE       = 'NFE' ,
--       @NF_SERIE         = '1'   ,
--       @NF_NUMERO        = 5292  ,
--       @EMPRESA          = 2     ,
--       @NF_CANCELAMENTO = 403    ,
--       @REGISTRO_NFE    = 21178  ,
--       @ID_NOTA         = '24161209077965000238550010000052921000045580'


SELECT 'N�o Existe Nota Fiscal Lan�ada com os Dados Informados. Cancelamento N�o Pode Ser Conclu�do'

  FROM CANCELAMENTOS_NOTAS_FISCAIS A ,
       ( SELECT NF_ESPECIE,
                NF_SERIE,
                NF_NUMERO
           FROM NF_FATURAMENTO 
          WHERE NF_ESPECIE = @NF_ESPECIE
            AND NF_SERIE   = @NF_SERIE
            AND NF_NUMERO  = @NF_NUMERO   

UNION ALL

         SELECT NF_ESPECIE,
                NF_SERIE,
                NF_NUMERO
           FROM NF_COMPRA_DEVOLUCOES 
          WHERE NF_ESPECIE = @NF_ESPECIE
            AND NF_SERIE   = @NF_SERIE
            AND NF_NUMERO  = @NF_NUMERO
  
UNION ALL

         SELECT NF_ESPECIE,
                NF_SERIE,
                NF_NUMERO
           FROM NF_FATURAMENTO_DEVOLUCOES 
          WHERE NF_ESPECIE = @NF_ESPECIE
            AND NF_SERIE   = @NF_SERIE
            AND NF_NUMERO  = @NF_NUMERO   

UNION ALL


         SELECT NF_ESPECIE,
                NF_SERIE,
                NF_NUMERO
           FROM DEV_PRODUTOS_CAIXAS WITH(NOLOCK)
          WHERE NF_ESPECIE = @NF_ESPECIE
            AND NF_SERIE   = @NF_SERIE
	        AND NF_NUMERO  = @NF_NUMERO

UNION ALL

         SELECT NF_ESPECIE,
                NF_SERIE,
                NF_NUMERO
           FROM NF_COMPRA 
          WHERE NF_ESPECIE = @NF_ESPECIE
            AND NF_SERIE   = @NF_SERIE
            AND NF_NUMERO  = @NF_NUMERO   ) B

WHERE A.NF_ESPECIE = B.NF_ESPECIE
  AND A.NF_SERIE   = B.NF_SERIE
  AND A.NF_NUMERO  = B.NF_NUMERO
  AND A.NF_CANCELAMENTO = @NF_CANCELAMENTO

HAVING COUNT(*) = 0 

UNION ALL

SELECT 'Nota Fiscal j� Cancelada, N�o Pode ser Informada Novamente'

  FROM CANCELAMENTOS_NOTAS_FISCAIS A    
WHERE A.NF_ESPECIE = @NF_ESPECIE
  AND A.NF_SERIE   = @NF_SERIE
  AND A.NF_NUMERO  = @NF_NUMERO
  AND A.EMPRESA    = @EMPRESA

HAVING COUNT(*) > 1

UNION ALL

SELECT 'O campo Justificativa deve conter pelo menos 15 caracteres!' 
  FROM CANCELAMENTOS_NOTAS_FISCAIS 
 WHERE NF_CANCELAMENTO = @NF_CANCELAMENTO
   --AND LEN(:JUSTIFICATIVA) <= 14

   AND 1 = 2

UNION ALL

SELECT DBO.NFE_VALIDA_CANCELAMENTO( @REGISTRO_NFE, @ID_NOTA ) WHERE  DBO.NFE_VALIDA_CANCELAMENTO( @REGISTRO_NFE, @ID_NOTA )  IS NOT NULL

UNION ALL

SELECT 'J� existe uma NFe Devolu��o de Venda para esse Nota Fiscal!'
  FROM CANCELAMENTOS_NOTAS_FISCAIS A WITH(NOLOCK)
  JOIN NFE_CABECALHO               C WITH(NOLOCK) ON A.REGISTRO_NFE = C.REGISTRO_NFE
                                                 AND A.TIPO         = 1
                                                 AND A.CHAVE        = C.REG_MASTER_ORIGEM
  JOIN NF_FATURAMENTO_DEVOLUCOES   D WITH(NOLOCK) ON C.CHAVE_NFE    = D.CHAVE_NFE_REFERENCIADA
 WHERE A.NF_CANCELAMENTO = @NF_CANCELAMENTO 

 UNION ALL

 SELECT TOP 1 'N�o Existe APROVA��O para o Cancelamento da NFe. Cancelamento N�o Pode Ser Conclu�do'

  FROM CANCELAMENTOS_NOTAS_FISCAIS        A WITH(NOLOCK)
     
  WHERE A.NF_CANCELAMENTO = @NF_CANCELAMENTO
    AND A.APROVACAO_CANCELAMENTO     IS NULL


UNION ALL
	
SELECT TOP 1 'Cancelamento da NFe N�O aprovado. Verificar com o Gerente'

  FROM CANCELAMENTOS_NOTAS_FISCAIS        A WITH(NOLOCK)
  JOIN APROVACOES_CANCELAMENTOS_NF_ITENS  B WITH(NOLOCK) ON A.NF_NUMERO = B.NF_NUMERO
      
  WHERE A.NF_CANCELAMENTO = @NF_CANCELAMENTO
  AND ISNULL(B.APROVADO, 'N') = 'N'
  