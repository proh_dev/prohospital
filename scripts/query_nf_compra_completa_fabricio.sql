---------------------------------------------------------------------
--              Consulta de NF de COMPRA                           --
--              Desenvolvedor: Yno� Pedro                          --
--              10/10/2018				                           --
---------------------------------------------------------------------

DECLARE @DATA_RECEBIMENTO_INI DATETIME
DECLARE @DATA_RECEBIMENTO_FIM DATETIME
DECLARE @EMPRESA_INI          NUMERIC
DECLARE @EMPRESA_FIM          NUMERIC
DECLARE @ENTIDADE             NUMERIC


SET @DATA_RECEBIMENTO_INI = :DATA_RECEBIMENTO_INI
SET @DATA_RECEBIMENTO_FIM = :DATA_RECEBIMENTO_FIM
SET @EMPRESA_INI		  = :EMPRESA_INI
SET @EMPRESA_FIM		  = :EMPRESA_FIM
SET @ENTIDADE             = (CASE WHEN ISNUMERIC(:ENTIDADE)      = 1 THEN :ENTIDADE  ELSE NULL END)


--SET @DATA_RECEBIMENTO_INI = '01/09/2018'
--SET @DATA_RECEBIMENTO_FIM = '30/09/2018'
----SET @ENTIDADE			  = 55695
--SET @EMPRESA_INI		  = 1
--SET @EMPRESA_FIM		  = 5


if OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL DROP TABLE #TEMP

---------------------------------------------------------------------
--  TEMP QUE CAPTURA NOTAS COM BONIFICA��O                         --
---------------------------------------------------------------------
SELECT DISTINCT A.NF_COMPRA

INTO #TEMP
 FROM      NF_COMPRA                A WITH(NOLOCK)
 LEFT JOIN NF_COMPRA_PRODUTOS       D WITH(NOLOCK) ON A.NF_COMPRA       = D.NF_COMPRA 
 LEFT JOIN OPERACOES_FISCAIS        E WITH(NOLOCK) ON D.OPERACAO_FISCAL = E.OPERACAO_FISCAL

 WHERE D.OPERACAO_FISCAL IN(20,153)



---------------------------------------------------------------------
--  SELECT FINAL DA QUERY                                          --
---------------------------------------------------------------------
SELECT
       48                               AS FORMULARIO_ORIGEM
      ,41                               AS TAB_MASTER_ORIGEM
      ,A.NF_COMPRA                      AS REG_MASTER_ORIGEM
      ,A.NF_COMPRA                      AS NF_COMPRA
      ,B.NOME                           AS EMPRESA
	  ,A.ENTIDADE                       AS ENTIDADE
	  ,C.NOME                           AS NOME
	  ,A.NF_NUMERO                      AS NF_NUMERO
	  ,CASE WHEN D.NF_COMPRA IS NOT NULL
	        THEN  'B'
			ELSE  'C'               END AS STATUS

 FROM      NF_COMPRA                A WITH(NOLOCK)
 LEFT JOIN EMPRESAS_USUARIAS        B WITH(NOLOCK) ON A.EMPRESA         = B.EMPRESA_USUARIA
 JOIN      ENTIDADES                C WITH(NOLOCK) ON A.ENTIDADE        = C.ENTIDADE
 LEFT JOIN #TEMP                    D WITH(NOLOCK) ON A.NF_COMPRA       = D.NF_COMPRA


 WHERE 1=1
  
   AND  A.DATA_RECEBIMENTO >= @DATA_RECEBIMENTO_INI          
   AND  A.DATA_RECEBIMENTO <= @DATA_RECEBIMENTO_FIM          
   AND ( A.ENTIDADE          = @ENTIDADE OR @ENTIDADE IS NULL )
   AND ( A.EMPRESA          >= @EMPRESA_INI                   )
   AND ( A.EMPRESA          <= @EMPRESA_FIM                   )

 
 

