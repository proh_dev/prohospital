 DECLARE @EMPRESA   NUMERIC
 DECLARE @NF_NUMERO VARCHAR(30)
                          
                            
IF ISNUMERIC(:NF_NUMERO) = 1 
  BEGIN 
    SET @NF_NUMERO = :NF_NUMERO              
  END ELSE  
    SET @NF_NUMERO = NULL       


IF ISNUMERIC(:EMPRESA) = 1 
  BEGIN 
    SET @EMPRESA = :EMPRESA
  END ELSE  
    SET @EMPRESA = NULL

--------------------------------------
-- Autor :RSouza
--  Consulta para exibir o retorno do cancelamento, 
-- protocolo e msg de retorno do Sefaz.
--------------------------------------
 if object_id('tempdb..#NFE_CANCELAMENTOS') is not null    
   DROP TABLE #NFE_CANCELAMENTOS    

SELECT TOP 1000 A.* INTO #NFE_CANCELAMENTOS 
  FROM NFE_CANCELAMENTOS A WITH(NOLOCK)
 WHERE 1 = 1
  AND (A.EMPRESA = @EMPRESA OR @EMPRESA IS NULL )
  AND (SUBSTRING(A.IDNOTA,26,9) = DBO.ZEROS(@NF_NUMERO,9) OR @NF_NUMERO IS NULL )
ORDER BY NFE_CANCELAMENTO DESC  
  
----------------------------------------------
-- Captando MSG Sefaz e o protocolo de retorno
----------------------------------------------

DECLARE @RESULTADO TABLE (   CODIGO_RETORNO         VARCHAR(30) ,
                             DESCRICAO_RETORNO      VARCHAR(255),
                             CHAVE_NF_CANCELADA     VARCHAR(60),
                             PROTOCOLO_CANCELAMENTO VARCHAR(60) ,
                             NFE_CANCELAMENTO       NUMERIC )
DECLARE @NFE_CANCELAMENTO NUMERIC(15)


WHILE EXISTS (SELECT TOP 1 1 FROM #NFE_CANCELAMENTOS )
BEGIN 
SELECT TOP 1 @NFE_CANCELAMENTO = NFE_CANCELAMENTO FROM #NFE_CANCELAMENTOS
   INSERT INTO @RESULTADO 
   SELECT 
          CODIGO_RETORNO         
         ,DESCRICAO_RETORNO      
         ,CHAVE_NF_CANCELADA     
         ,PROTOCOLO_CANCELAMENTO 
         ,@NFE_CANCELAMENTO AS NFE_CANCELAMENTO 

 FROM DBO.NFE_MOTIVO_ERRO_CANCELAMENTO(@NFE_CANCELAMENTO)
           WHERE DESCRICAO_RETORNO IS NOT NULL
   DELETE FROM #NFE_CANCELAMENTOS WHERE NFE_CANCELAMENTO = @NFE_CANCELAMENTO
END 

----------------------------------------------
-- Resultado Final 
----------------------------------------------

SELECT TOP 1000
  D.FORMULARIO_ORIGEM       AS FORMULARIO_ORIGEM,
  D.TAB_MASTER_ORIGEM       AS TAB_MASTER_ORIGEM,
  D.REG_MASTER_ORIGEM       AS REG_MASTER_ORIGEM,
  A.NFE_CANCELAMENTO       AS NFE_CANCELAMENTO,
  A.EMPRESA                AS EMPRESA , 
  C.NOME                   AS DESCRICAO_EMPRESA , 
  A.IDNOTA                 AS CHAVE ,
  SUBSTRING(A.IDNOTA,26,9) AS NF_NUMERO,
  B.PROTOCOLO_CANCELAMENTO AS PROTOCOLO ,
  A.JUSTIFICATIVA          AS JUSTIFICATIVA ,          
  CASE WHEN A.STATUS = 4  AND (B.CODIGO_RETORNO IN ('135','101','155' , '151'))
       THEN 'NOTA CANCELADA'
       ELSE 'ERRO NO CANCELAMENTO'
  END                      AS STATUS_CANCELAMENTO,
  B.DESCRICAO_RETORNO      AS RETORNO,
  ISNULL(A.ROBO,1)         AS ROBO ,    
  ISNULL(CONVERT(VARCHAR(30),A.DATA_HORA,103) + ' ' +CONVERT(VARCHAR(30),A.DATA_HORA,108),'01-01-2012 00:00:00') 
                           AS DATA_HORA ,
  E.DATA_HORA			   AS DATA_EMISSAO
    
 FROM NFE_CANCELAMENTOS                    A WITH(NOLOCK)
 LEFT JOIN @RESULTADO                    B ON B.NFE_CANCELAMENTO = A.NFE_CANCELAMENTO --B.CHAVE_NF_CANCELADA = IDNOTA
 LEFT JOIN EMPRESAS_USUARIAS            C WITH(NOLOCK)ON C.EMPRESA_USUARIA = A.EMPRESA
 LEFT JOIN CANCELAMENTOS_NOTAS_FISCAIS    D WITH(NOLOCK)ON D.FORMULARIO_ORIGEM = A.FORMULARIO_ORIGEM AND
                                                         D.TAB_MASTER_ORIGEM = A.TAB_MASTER_ORIGEM AND
                                                         D.REG_MASTER_ORIGEM = A.REG_MASTER_ORIGEM
 JOIN NFE_CABECALHO                       E WITH(NOLOCK) ON A.REGISTRO_NFE = E.REGISTRO_NFE
WHERE 1 = 1
  AND (A.EMPRESA = @EMPRESA OR @EMPRESA IS NULL )
  AND (SUBSTRING(A.IDNOTA,26,9) = DBO.ZEROS(@NF_NUMERO,9) OR @NF_NUMERO IS NULL )
  
ORDER BY NFE_CANCELAMENTO DESC   --SELECT TOP 100 * FROM CANCELAMENTOS_NOTAS_FISCAIS WHERE REG_MASTER_ORIGEM = 101