/*
         Desenvolvedor : Ynoa Pedro
         Date:03/05/2018
                                       */
DECLARE @MES     NUMERIC
DECLARE @MES_FIM NUMERIC
DECLARE @ANO     NUMERIC
DECLARE @EMPRESA NUMERIC
DECLARE @PRODUTO NUMERIC
DECLARE @MARCA   NUMERIC
DECLARE @FAMILIA NUMERIC
DECLARE @GRUPO   NUMERIC

SET @MES      = 1
SET @MES_FIM  = 5
SET @ANO      = 2018
--SET @GRUPO    = 12
--SET @MARCA    = 19
--SET @FAMILIA  = 3
--SET @EMPRESA  = 10
--SET @PRODUTO  = 18265 

--SET @MES      = :MES
--SET @MES_FIM  = :MES_FIM
--SET @ANO      = :ANO

--IF ISNUMERIC(:PRODUTO) = 1
--	SET @PRODUTO = :PRODUTO
--   ELSE
--    SET @PRODUTO = NULL

--IF ISNUMERIC(:GRUPO) = 1
--	SET @GRUPO = :GRUPO
--   ELSE
--    SET @GRUPO = NULL

--IF ISNUMERIC(:FAMILIA) = 1
--	SET @FAMILIA = :FAMILIA
--   ELSE
--    SET @FAMILIA = NULL

--IF ISNUMERIC(:MARCA) = 1
--	SET @MARCA = :MARCA
--   ELSE
--    SET @MARCA = NULL

--IF ISNUMERIC(:EMPRESA) = 1 
--    SET @EMPRESA     = :EMPRESA         
--  ELSE 
--    SET @EMPRESA     = NULL



SELECT    
          A.EMPRESA                      AS EMPRESA ,
          H.NOME                         AS EMPRESA_NOME, 
		  DATEPART(YEAR, A.MOVIMENTO)    AS ANO ,  
          DATEPART(MONTH, A.MOVIMENTO)   AS MES , 
          A.PRODUTO                      AS PRODUTO,
          B.DESCRICAO                    AS DESCRICAO_PRODUTO,
          SUM ( A.QUANTIDADE )           AS QUANTIDADE 



          
     FROM VENDAS_ANALITICAS       A WITH(NOLOCK)
     JOIN PRODUTOS                B WITH(NOLOCK) ON B.PRODUTO               = A.PRODUTO
     JOIN MARCAS                  C WITH(NOLOCK) ON C.MARCA                 = B.MARCA
     JOIN SECOES_PRODUTOS         D WITH(NOLOCK) ON D.SECAO_PRODUTO         = B.SECAO_PRODUTO
     JOIN GRUPOS_PRODUTOS         E WITH(NOLOCK) ON E.GRUPO_PRODUTO         = B.GRUPO_PRODUTO
     JOIN SUBGRUPOS_PRODUTOS      F WITH(NOLOCK) ON F.SUBGRUPO_PRODUTO      = B.SUBGRUPO_PRODUTO
     LEFT                           
     JOIN VENDEDORES              G WITH(NOLOCK) ON G.VENDEDOR              = A.VENDEDOR
     JOIN EMPRESAS_USUARIAS       H WITH(NOLOCK) ON H.EMPRESA_USUARIA       = A.EMPRESA
     LEFT                                                                      
     JOIN ENTIDADES               I WITH(NOLOCK) ON I.ENTIDADE              = A.CLIENTE
     LEFT                                                                      
     JOIN FAMILIAS_PRODUTOS       J WITH(NOLOCK) ON J.FAMILIA_PRODUTO       = B.FAMILIA_PRODUTO
     LEFT
     JOIN GRUPOS_MARCAS_DETALHE   K WITH(NOLOCK) ON C.MARCA                 = K.MARCA
     LEFT                                                                   
     JOIN GRUPOS_MARCAS           L WITH(NOLOCK) ON K.GRUPO_MARCA           = L.GRUPO_MARCA
     LEFT
     JOIN CLASSIFICACOES_CLIENTES M WITH(NOLOCK) ON I.CLASSIFICACAO_CLIENTE = M.CLASSIFICACAO_CLIENTE

 WHERE 1=1
       AND (DATEPART(MONTH, A.MOVIMENTO)     >= @MES                            )
	   AND (DATEPART(MONTH, A.MOVIMENTO)     <= @MES_FIM                        )
       AND (DATEPART(YEAR, A.MOVIMENTO)       = @ANO	                        )
       AND (ISNULL(A.CLIENTE, 99999999)       > 1000 	                        )
	   AND (A.EMPRESA                         = @EMPRESA   OR   @EMPRESA IS NULL)
       AND (A.PRODUTO                         = @PRODUTO   OR   @PRODUTO IS NULL) 
	   AND (B.MARCA                           = @MARCA     OR   @MARCA   IS NULL)   
	   AND (B.FAMILIA_PRODUTO                 = @FAMILIA   OR   @FAMILIA IS NULL)   
	   AND (L.GRUPO_MARCA                     = @GRUPO     OR   @GRUPO   IS NULL)   
  
GROUP BY A.EMPRESA      ,
		 H.NOME   		,
		 A.PRODUTO  	,
		 B.DESCRICAO	,
		 A.MOVIMENTO    ,
		 A.QUANTIDADE
ORDER BY  A.EMPRESA