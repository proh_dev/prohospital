SELECT    
          A.EMPRESA                      AS EMPRESA , 
          A.MOVIMENTO                    AS DATA , 
          DATEPART(YEAR, A.MOVIMENTO)    AS ANO , 
          DATEPART(MONTH, A.MOVIMENTO)   AS MES , 
          A.PRODUTO                      AS PRODUTO,
          B.DESCRICAO                    AS DESCRICAO_PRODUTO,
          C.DESCRICAO                    AS MARCA ,
          D.DESCRICAO                    AS SECAO , 
          F.DESCRICAO                    AS SUBGRUPO , 
          E.DESCRICAO                    AS GRUPO ,
          H.NOME                         AS EMPRESA_NOME, 
          I.NOME                         AS CLIENTE_NOME,
          SUM ( A.QUANTIDADE )           AS QUANTIDADE ,
          SUM ( A.VENDA_BRUTA )          AS VENDA_BRUTA ,
          SUM ( A.DESCONTO )             AS DESCONTO ,
          SUM ( A.VENDA_BRUTA ) -
          SUM ( A.DESCONTO )             AS VENDA_LIQUIDA ,
          SUM ( A.ICMS )                 AS ICMS ,
          SUM ( A.PIS )                  AS PIS ,
          SUM ( A.COFINS )               AS COFINS ,
          SUM ( A.VENDA_BRUTA ) -
          SUM ( A.DESCONTO )    -
          SUM ( A.PIS )         -
          SUM ( A.COFINS )      -
          SUM ( A.ICMS )                 AS RECEITA_LIQUIDA  ,
          ISNULL(CAST(G.VENDEDOR AS VARCHAR) + ' - ' + G.NOME, '0 - SEM VENDEDOR') AS VENDEDOR,
		  B.CODIGO_REFERENCIA            AS REFERENCIA,
		  J.DESCRICAO                    AS FAMILIA_PRODUTO,
		  L.DESCRICAO                    AS GRUPO_MARCA,
		  M.DESCRICAO                    AS GRUPO_CLIENTE



          
     FROM VENDAS_DIARIAS          A WITH(NOLOCK)
     JOIN PRODUTOS                B WITH(NOLOCK) ON B.PRODUTO               = A.PRODUTO
     JOIN MARCAS                  C WITH(NOLOCK) ON C.MARCA                 = B.MARCA
     JOIN SECOES_PRODUTOS         D WITH(NOLOCK) ON D.SECAO_PRODUTO         = B.SECAO_PRODUTO
     JOIN GRUPOS_PRODUTOS         E WITH(NOLOCK) ON E.GRUPO_PRODUTO         = B.GRUPO_PRODUTO
     JOIN SUBGRUPOS_PRODUTOS      F WITH(NOLOCK) ON F.SUBGRUPO_PRODUTO      = B.SUBGRUPO_PRODUTO
     LEFT 					      
	 JOIN VENDEDORES              G WITH(NOLOCK) ON G.VENDEDOR              = A.VENDEDOR
     JOIN EMPRESAS_USUARIAS       H WITH(NOLOCK) ON H.EMPRESA_USUARIA       = A.EMPRESA
     LEFT					      										    
	 JOIN ENTIDADES               I WITH(NOLOCK) ON I.ENTIDADE              = A.CLIENTE
	 LEFT					      										    
	 JOIN FAMILIAS_PRODUTOS       J WITH(NOLOCK) ON J.FAMILIA_PRODUTO       = B.FAMILIA_PRODUTO
	 LEFT
	 JOIN GRUPOS_MARCAS_DETALHE   K WITH(NOLOCK) ON C.MARCA                 = K.MARCA
     LEFT														           
	 JOIN GRUPOS_MARCAS           L WITH(NOLOCK) ON K.GRUPO_MARCA           = L.GRUPO_MARCA
	 LEFT
	 JOIN CLASSIFICACOES_CLIENTES M WITH(NOLOCK) ON I.CLASSIFICACAO_CLIENTE = M.CLASSIFICACAO_CLIENTE

 WHERE 1=1
       AND A.MOVIMENTO >= :INICIO 
       AND A.MOVIMENTO <= :FIM
	   AND ISNULL(A.CLIENTE, 99999999) > 1000 -- SOMENTE VENDAS PARA CLIENTES
	   --AND A.MOVIMENTO >= '01/01/2018' 
    --   AND A.MOVIMENTO <= '31/01/2018'
          
GROUP BY A.MOVIMENTO , A.EMPRESA , A.PRODUTO, B.DESCRICAO, C.DESCRICAO,D.DESCRICAO, F.DESCRICAO, E.DESCRICAO,ISNULL(CAST(G.VENDEDOR AS VARCHAR) + ' - ' + G.NOME, '0 - SEM VENDEDOR'), H.NOME, I.NOME, B.CODIGO_REFERENCIA,J.DESCRICAO, L.DESCRICAO,M.DESCRICAO    

ORDER BY A.MOVIMENTO , A.EMPRESA