CREATE PROCEDURE [dbo].[GRAVACAO_FINAL_WMS_CHECKOUT_NOVA] ( @CHECKOUT NUMERIC ) AS       
      
BEGIN      
      
--BEGIN TRANSACTION      
----ROLLBACK      
----COMMIT      
      
--DECLARE @CHECKOUT                        NUMERIC = 10072      
      
      
DECLARE @ESTOQUE_TRANSFERENCIA           NUMERIC       
DECLARE @EMBALAGEM_SEPARACAO             VARCHAR(20)      
DECLARE @CENTRO_ESTOQUE                  NUMERIC      
DECLARE @TIPO_CONFERENCIA                NUMERIC      
DECLARE @EMPRESA                         NUMERIC      
DECLARE @COLETOR                         VARCHAR(1)      
      
DECLARE @FORMULARIO_ORIGEM               NUMERIC      
DECLARE @TAB_MASTER_ORIGEM               NUMERIC      
      
DECLARE @EMPRESA_WMS                     VARCHAR(1)      
DECLARE @USUARIO_LOGADO                  NUMERIC      
DECLARE @TIPO_ESTOQUE                    NUMERIC      
DECLARE @MENSAGEM                        VARCHAR(1000)      
DECLARE @CONFERENCIA_CHECKOUT_COLETOR    VARCHAR(1)      
DECLARE @CONFIRMAR VARCHAR(1)      
      
DECLARE @VOLUMES                         NUMERIC(6)      
DECLARE @REGISTRO_EMBALAGEM_SEPARACAO    NUMERIC      
      
--PEGA PARAMETROS DA MASTER--      
SELECT @TIPO_CONFERENCIA      = A.TIPO_CONFERENCIA,      
    @USUARIO_LOGADO           = A.USUARIO_LOGADO,      
    @EMBALAGEM_SEPARACAO      = A.EMBALAGEM_SEPARACAO,      
    @VOLUMES                  = A.VOLUMES,      
       @FORMULARIO_ORIGEM     = A.FORMULARIO_ORIGEM,      
       @TAB_MASTER_ORIGEM     = A.TAB_MASTER_ORIGEM,      
       @EMPRESA               = A.EMPRESA,      
       @EMPRESA_WMS           = ISNULL(C.EMPRESA_WMS,'N'),      
       --@CENTRO_ESTOQUE        = C.CENTRO_ESTOQUE_PADRAO,      
    @COLETOR               = ISNULL(A.COLETOR, 'N')       ,      
    @CONFERENCIA_CHECKOUT_COLETOR = ISNULL(C.CONFERENCIA_CHECKOUT_COLETOR, 'N'),      
    @REGISTRO_EMBALAGEM_SEPARACAO = A.REGISTRO_EMBALAGEM_SEPARACAO,      
    @CONFIRMAR                    = A.CONFIRMAR      
      
  FROM WMS_CHECKOUT               A WITH(NOLOCK)      
  JOIN EMPRESAS_USUARIAS          B WITH(NOLOCK) ON B.EMPRESA_USUARIA = A.EMPRESA      
  JOIN PARAMETROS_ESTOQUE         C WITH(NOLOCK) ON C.EMPRESA_USUARIA = A.EMPRESA      
      
 WHERE A.CHECKOUT = @CHECKOUT      
        
      
--PEGA UM ROMANEIO DO CHECKOUT--      
SELECT TOP 1 @ESTOQUE_TRANSFERENCIA = X.ESTOQUE_TRANSFERENCIA      
  FROM VW_WMS_CHECKOUT_ROMANEIOS  X WITH(NOLOCK)      
 WHERE X.CHECKOUT = @CHECKOUT      
      
      
--------------------------------------------------      
--ROTINA TRATADA PARA CHECKOUT COM WMS--      
--------------------------------------------------      
      
-- PARAMETRO DE EMPRESA WMS --      
SELECT @TIPO_ESTOQUE            = A.TIPO_ESTOQUE      
  FROM ESTOQUE_TRANSFERENCIAS A WITH(NOLOCK)      
 WHERE A.ESTOQUE_TRANSFERENCIA = @ESTOQUE_TRANSFERENCIA      
      
--------------------------------------------------------      
--TRATAMENTO DO VOLUME--      
--------------------------------------------------------      
      
--SE FOR N�O CHECKOUT POR EMBALAGEM--      
--PEGA OS VOLUMES BIPADOS--      
      
IF @EMBALAGEM_SEPARACAO = 'CX' AND ISNULL(@VOLUMES,0) = 0      
      
BEGIN      
      
SELECT @VOLUMES = SUM(X.VOLUMES)      
        
  FROM (      
      
SELECT COUNT(*) AS VOLUMES      
  FROM WMS_CHECKOUT                              X WITH(NOLOCK)      
  JOIN WMS_CHECKOUT_COLETA                       A WITH(NOLOCK) ON A.CHECKOUT = X.CHECKOUT      
  JOIN RECEBIMENTOS_FISICOS_ETIQUETAS_VOLUMES    B WITH(NOLOCK) ON B.CODIGO_BARRAS_VOLUME = A.CODIGO_BARRAS      
 WHERE X.CHECKOUT = @CHECKOUT      
   AND X.UNIFICACAO_VOLUMES = 'N'      
      
 UNION ALL      
      
SELECT COUNT(*) AS VOLUMES      
  FROM WMS_CHECKOUT                              X WITH(NOLOCK)      
  JOIN WMS_CHECKOUT_MANUAL                       A WITH(NOLOCK) ON A.CHECKOUT = X.CHECKOUT      
  JOIN RECEBIMENTOS_FISICOS_ETIQUETAS_VOLUMES    B WITH(NOLOCK) ON B.CODIGO_BARRAS_VOLUME = A.CODIGO_BARRAS      
 WHERE X.CHECKOUT = @CHECKOUT      
   AND X.UNIFICACAO_VOLUMES = 'N' ) X      
      
 --TRATA PARA SER ZERO SE N�O ACHAR--      
 SELECT @VOLUMES = ISNULL(@VOLUMES,0)      
      
END      
      
      
--SE FOR CHECKOUT POR EMBALAGEM--      
--VOLUMES = 1      
      
IF ISNULL(LEN(@EMBALAGEM_SEPARACAO),0) > 0 AND @EMBALAGEM_SEPARACAO <> 'CX'      
      
BEGIN      
      
  SET @VOLUMES = 1       
      
END      
      
      
 --ATUALIZA O VOLUME--      
 UPDATE WMS_CHECKOUT       
    SET VOLUMES = @VOLUMES        
  WHERE CHECKOUT = @CHECKOUT      
      
--------------------------------------------------------      
--FIM TRATAMENTO DO VOLUME--      
--------------------------------------------------------      
      
-------------------------------------------------------------      
--GERACAO DE TABELAS TEMPORARIAS PARA O PROCESSOS      
-------------------------------------------------------------      
      
if object_id('tempdb..#PRODUTOS')         is not null DROP TABLE #PRODUTOS      
if object_id('tempdb..#RESULTADOS')       is not null DROP TABLE #RESULTADOS      
if object_id('tempdb..#RESULTADOS_FINAL') is not null DROP TABLE #RESULTADOS_FINAL      
             
      
--GERA TEMP DE PRODUTOS--      
SELECT DISTINCT TMP.PRODUTO      
      
INTO #PRODUTOS      
      
FROM (      
       SELECT DISTINCT A.PRODUTO      
         FROM WMS_CHECKOUT_COLETA A WITH(NOLOCK)      
        WHERE A.CHECKOUT = @CHECKOUT      
      
        UNION ALL      
      
       SELECT DISTINCT A.PRODUTO      
         FROM WMS_CHECKOUT_MANUAL A WITH(NOLOCK)      
        WHERE A.CHECKOUT = @CHECKOUT      
     ) TMP      
      
             
----------------------------------------------------      
--PEGA OS PRODUTOS DA COLETA      
----------------------------------------------------      
      
  SELECT A.PRODUTO                                   AS PRODUTO,      
         C.ESTOQUE_TRANSFERENCIA                     AS ESTOQUE_TRANSFERENCIA,      
         C.LOTE_VALIDADE                             AS LOTE_VALIDADE ,      
   C.CODIGO_LOCALIZADOR                        AS CODIGO_LOCALIZADOR,      
   D.CENTRO_ESTOQUE_ORIGEM                     AS CENTRO_ESTOQUE,      
         SUM(C.QUANTIDADE * C.QUANTIDADE_EMBALAGEM ) AS QUANTIDADE,      
         SUM(C.QUANTIDADE_UNIT )                     AS QUANTIDADE_UNIT,             
         'S'                                         AS COLETOR      
               
    INTO #RESULTADOS      
               
    FROM PRODUTOS                          A WITH(NOLOCK)      
    JOIN #PRODUTOS                         B WITH(NOLOCK) ON A.PRODUTO = B.PRODUTO      
    JOIN WMS_CHECKOUT_COLETA               C WITH(NOLOCK) ON A.PRODUTO = C.PRODUTO      
 JOIN ESTOQUE_TRANSFERENCIAS            D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA = C.ESTOQUE_TRANSFERENCIA      
         
   WHERE C.CHECKOUT = @CHECKOUT      
      
GROUP BY A.PRODUTO ,      
         C.ESTOQUE_TRANSFERENCIA,      
         C.LOTE_VALIDADE,      
   C.CODIGO_LOCALIZADOR,      
   D.CENTRO_ESTOQUE_ORIGEM      
      
   UNION ALL      
      
----------------------------------------------------      
--PEGA OS PRODUTOS MANUAIS      
----------------------------------------------------      
      
  SELECT A.PRODUTO                                   AS PRODUTO,      
         B.ESTOQUE_TRANSFERENCIA                     AS ESTOQUE_TRANSFERENCIA,      
         B.LOTE_VALIDADE                             AS LOTE_VALIDADE,      
   B.CODIGO_LOCALIZADOR                        AS CODIGO_LOCALIZADOR,      
   D.CENTRO_ESTOQUE_ORIGEM                     AS CENTRO_ESTOQUE,      
         SUM(B.QUANTIDADE * B.QUANTIDADE_EMBALAGEM)  AS QUANTIDADE,      
         SUM(B.QUANTIDADE_UNIT )                     AS QUANTIDADE_UNIT,             
         'N'                                         AS COLETOR             
              
    FROM PRODUTOS                          A WITH(NOLOCK)       
    JOIN WMS_CHECKOUT_MANUAL               B WITH(NOLOCK) ON A.PRODUTO = B.PRODUTO      
 JOIN ESTOQUE_TRANSFERENCIAS            D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA      
       
   WHERE B.CHECKOUT = @CHECKOUT      
         
GROUP BY A.PRODUTO ,      
         B.ESTOQUE_TRANSFERENCIA,      
         B.LOTE_VALIDADE,      
   B.CODIGO_LOCALIZADOR,      
   D.CENTRO_ESTOQUE_ORIGEM      
      
--------------------------------------------------------      
--PEGA O CENTRO DE ESTOQUE--      
--------------------------------------------------------      
      
SELECT TOP 1 @CENTRO_ESTOQUE = A.CENTRO_ESTOQUE      
  FROM #RESULTADOS A       
      
--------------------------------------------------------      
--RESULTADO FINAL--      
--------------------------------------------------------      
                    
   SELECT A.PRODUTO,      
          A.LOTE_VALIDADE,      
    A.ESTOQUE_TRANSFERENCIA,      
    A.CODIGO_LOCALIZADOR,      
    A.CENTRO_ESTOQUE,      
          SUM(A.QUANTIDADE)      AS QUANTIDADE,      
          SUM(A.QUANTIDADE_UNIT) AS QUANTIDADE_UNIT                   
      
     INTO #RESULTADOS_FINAL      
             
     FROM #RESULTADOS A      
          
 GROUP BY A.PRODUTO,       
          A.ESTOQUE_TRANSFERENCIA,      
          A.LOTE_VALIDADE,      
    A.CODIGO_LOCALIZADOR,      
    A.CENTRO_ESTOQUE      
       
    HAVING SUM(A.QUANTIDADE_UNIT) > 0      
      
      
----------------------------------------------------       
--PEGA O SALDO DE LV PARA CASO O ROMANEIO SEJA UN--             
----------------------------------------------------      
      
if object_id('tempdb..#ESTOQUE_SALDO_LV') is not null      
   DROP TABLE #ESTOQUE_SALDO_LV      
      
     SELECT A.PRODUTO,      
            A.CENTRO_ESTOQUE      AS CENTRO_ESTOQUE,      
            B.LOTE                AS LOTE,      
            B.VALIDADE            AS VALIDADE,      
            A.LOTE_VALIDADE       AS LOTE_VALIDADE,      
            C.LOCALIZADOR         AS CODIGO_LOCALIZADOR ,      
            SUM(ISNULL(A.SALDO,0)) AS ESTOQUE_SALDO ,      
            ROW_NUMBER() OVER( PARTITION BY A.PRODUTO ORDER BY SUM(ISNULL(A.SALDO,0)) DESC )  AS SEQUENCIA        
                  
      INTO #ESTOQUE_SALDO_LV                     
            
       FROM #PRODUTOS      X WITH(NOLOCK)      
       JOIN ESTOQUE_ATUAL_LOCALIZADORES_LV_COM_CACHE      A WITH(NOLOCK) ON A.PRODUTO          = X.PRODUTO      
       JOIN PRODUTOS_LOTE_VALIDADE                        B WITH(NOLOCK) ON B.LOTE_VALIDADE    = A.LOTE_VALIDADE       
       JOIN CADASTROS_LOCALIZADORES                       C WITH(NOLOCK) ON C.LOCALIZADOR      = A.CODIGO_LOCALIZADOR                
                                                                        AND C.TIPO_LOCALIZADOR = 2 --PICKING--             
     WHERE A.CENTRO_ESTOQUE = @CENTRO_ESTOQUE      
      
    AND ( @TIPO_ESTOQUE = 2 --UN       
                
    OR      
      
    @EMBALAGEM_SEPARACAO IS NOT NULL      
      
        )      
      
   GROUP BY A.PRODUTO,      
            A.CENTRO_ESTOQUE,      
            B.LOTE         ,      
            B.VALIDADE     ,      
            A.LOTE_VALIDADE,      
            C.LOCALIZADOR        
      
                  
--ATUALIZA O VALOR DE LOTE E VALIDADE       
      
UPDATE #RESULTADOS_FINAL      
        
   SET LOTE_VALIDADE = B.LOTE_VALIDADE      
      
  FROM #RESULTADOS_FINAL A       
  JOIN #ESTOQUE_SALDO_LV B ON B.PRODUTO    = A.PRODUTO      
                          AND B.SEQUENCIA  =  1      
      
  WHERE A.LOTE_VALIDADE IS NULL      
      
      
--ATUALIZA O VALOR DE LOTE E VALIDADE       
      
UPDATE #RESULTADOS_FINAL      
        
   SET LOTE_VALIDADE = B.LOTE_VALIDADE      
      
  FROM #RESULTADOS_FINAL       A       
  JOIN VW_ULTIMO_LOTE_VALIDADE B ON B.PRODUTO    = A.PRODUTO      
      
  WHERE A.LOTE_VALIDADE IS NULL      
      
      
      
--ATUALIZA O VALOR DE LOTE E VALIDADE       
      
UPDATE #RESULTADOS_FINAL      
        
   SET CODIGO_LOCALIZADOR = B.CODIGO_LOCALIZADOR      
      
  FROM #RESULTADOS_FINAL A       
  JOIN PRODUTOS_LOCAIS   B ON B.PRODUTO    = A.PRODUTO      
      
WHERE B.OBJETO_CONTROLE = @CENTRO_ESTOQUE      
  AND @TIPO_ESTOQUE = 2 --UN   
      
      
      
--ATUALIZA O LOCALIZADOR PELO DADO DO ROMANEIO SE N�O TIVER      
--GEMADA--      
      
UPDATE #RESULTADOS_FINAL      
        
   SET CODIGO_LOCALIZADOR = B.CODIGO_LOCALIZADOR      
      
  FROM VW_WMS_CHECKOUT_ROMANEIOS                A       
  JOIN ESTOQUE_TRANSFERENCIAS_LOTE_VALIDADE     B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA      
  JOIN #RESULTADOS_FINAL                        C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA      
                                                              AND C.PRODUTO               = B.PRODUTO      
      
 WHERE C.CODIGO_LOCALIZADOR IS NULL      
   AND @TIPO_ESTOQUE = 1 --CX      
      
      
---------------------------------------------------------      
--GERA A TABELA DE RESULTADOS DO CHECKOUT      
---------------------------------------------------------      
      
 if object_id('tempdb..#TEMP_DELETE_01') is not null DROP TABLE #TEMP_DELETE_01      
      
SELECT A.CHECKOUT_RESUMO, CHECKOUT      
      
  INTO #TEMP_DELETE_01      
       
  FROM WMS_CHECKOUT_RESUMO A WITH(NOLOCK)      
 WHERE A.CHECKOUT = @CHECKOUT      
      
      
--REALIZA O DELETE USANDO A PK--      
DELETE WMS_CHECKOUT_RESUMO      
  FROM WMS_CHECKOUT_RESUMO A WITH(NOLOCK)      
  JOIN #TEMP_DELETE_01     X ON X.CHECKOUT_RESUMO = A.CHECKOUT_RESUMO      
                            AND X.CHECKOUT        = A.CHECKOUT      
      
        
--INSERE OS DADOS--        
INSERT INTO WMS_CHECKOUT_RESUMO (      
      
            CHECKOUT,      
            PRODUTO,      
   ESTOQUE_TRANSFERENCIA,      
            LOTE_VALIDADE,      
   CODIGO_LOCALIZADOR,      
   CENTRO_ESTOQUE_ORIGEM,      
            QUANTIDADE,      
            QUANTIDADE_UNIT )      
      
SELECT @CHECKOUT AS CHECKOUT,      
       PRODUTO,      
    ESTOQUE_TRANSFERENCIA,      
       LOTE_VALIDADE,      
    CODIGO_LOCALIZADOR,      
    CENTRO_ESTOQUE,      
       QUANTIDADE,      
       QUANTIDADE_UNIT      
             
  FROM #RESULTADOS_FINAL A       
      
ORDER BY A.PRODUTO      
      
       
 --PROCESSA A TRANSA��O DO ROMANEIO--      
       
    EXEC INTEGRA_ESTOQUE_TRANSFERENCIAS_TRANSACOES_CHECKOUT @CHECKOUT      
      
 --PROCESSA O ESTOQUE--      
    EXEC INTEGRA_ESTOQUE_LANCAMENTOS_CHECKOUT @CHECKOUT      
      
      
 ---------------------------------------------------------      
 --GERA A TRANSA��O WMS_EMBALAGENS_SEPARACOES_TRANSACOES--      
 --COMO 'Checkout'--      
 ---------------------------------------------------------      
      
 INSERT INTO WMS_EMBALAGENS_SEPARACOES_TRANSACOES (      
       
            FORMULARIO_ORIGEM,      
            TAB_MASTER_ORIGEM,      
            REG_MASTER_ORIGEM,      
            DATA_HORA        ,      
            DATA             ,      
            EMBALAGEM_SEPARACAO,      
            STATUS )      
       
     SELECT TOP 1       
          A.FORMULARIO_ORIGEM,      
            A.TAB_MASTER_ORIGEM,      
            A.CHECKOUT,      
            GETDATE() AS DATA_HORA,      
            CONVERT(DATE,A.DATA_HORA) AS DATA,      
            A.EMBALAGEM_SEPARACAO,      
            3 AS STATUS      
       
      FROM WMS_CHECKOUT                          A WITH(NOLOCK)      
 LEFT JOIN WMS_EMBALAGENS_SEPARACOES_TRANSACOES  B WITH(NOLOCK) ON B.FORMULARIO_ORIGEM = A.FORMULARIO_ORIGEM      
                                                               AND B.TAB_MASTER_ORIGEM = A.TAB_MASTER_ORIGEM      
                                                               AND B.REG_MASTER_ORIGEM = A.CHECKOUT      
  WHERE A.CHECKOUT = @CHECKOUT      
    AND B.REGISTRO            IS NULL      
    AND A.EMBALAGEM_SEPARACAO IS NOT NULL      
      
      
 ---------------------------------------------------------      
 --GERA A TRANSA��O WMS_EMBALAGENS_SEPARACOES_TRANSACOES--      
 --COMO 'Checkout Finalizado'--      
 ---------------------------------------------------------      
      
INSERT INTO WMS_EMBALAGENS_SEPARACOES_TRANSACOES (      
      
            FORMULARIO_ORIGEM,      
  TAB_MASTER_ORIGEM,      
            REG_MASTER_ORIGEM,      
            DATA_HORA        ,      
            DATA             ,      
            EMBALAGEM_SEPARACAO,      
            STATUS )      
       
     SELECT TOP 1       
          A.FORMULARIO_ORIGEM,      
            A.TAB_MASTER_ORIGEM,      
            A.REG_MASTER_ORIGEM,      
            GETDATE() AS DATA_HORA,      
            CONVERT(DATE,A.DATA_HORA) AS DATA,      
            ISNULL(A.EMBALAGEM_SEPARACAO_EXPEDICAO,  A.EMBALAGEM_SEPARACAO ) AS EMBALAGEM_SEPARACAO,      
            4 AS STATUS      
       
      FROM WMS_CHECKOUT                          A WITH(NOLOCK)      
 LEFT JOIN WMS_EMBALAGENS_SEPARACOES_TRANSACOES  B WITH(NOLOCK) ON B.FORMULARIO_ORIGEM   = A.FORMULARIO_ORIGEM      
                                                               AND B.TAB_MASTER_ORIGEM   = A.TAB_MASTER_ORIGEM      
                                     AND B.REG_MASTER_ORIGEM   = A.CHECKOUT      
                  AND B.EMBALAGEM_SEPARACAO = ISNULL(A.EMBALAGEM_SEPARACAO_EXPEDICAO,  A.EMBALAGEM_SEPARACAO )      
                  AND B.STATUS              = 4 --CHECKOUT FINALIZADO--      
  WHERE A.CHECKOUT          = @CHECKOUT      
    AND B.REGISTRO            IS NULL      
    AND A.EMBALAGEM_SEPARACAO IS NOT NULL      
 AND A.CONFIRMAR = 'S'      
      
      
---------------------------------------------------------      
 --GERA A TRANSA��O WMS_EMBALAGENS_SEPARACOES_TRANSACOES--      
 --LIBERA O VOLUME ORIGEM AUTOMATICAMENTE      
 ---------------------------------------------------------      
      
      
INSERT INTO WMS_EMBALAGENS_SEPARACOES_TRANSACOES (      
      
            FORMULARIO_ORIGEM,      
            TAB_MASTER_ORIGEM,      
            REG_MASTER_ORIGEM,      
            DATA_HORA        ,      
            DATA             ,      
            EMBALAGEM_SEPARACAO,      
            STATUS )      
       
     SELECT TOP 1       
          A.FORMULARIO_ORIGEM,      
            A.TAB_MASTER_ORIGEM,      
            A.REG_MASTER_ORIGEM,      
            GETDATE() AS DATA_HORA,      
            CONVERT(DATE,A.DATA_HORA) AS DATA,      
            A.EMBALAGEM_SEPARACAO     AS EMBALAGEM_SEPARACAO,      
            0 AS STATUS      
       
      FROM WMS_CHECKOUT                            A WITH(NOLOCK)      
 LEFT JOIN WMS_EMBALAGENS_SEPARACOES_STATUS_ATUAL  B WITH(NOLOCK) ON B.EMBALAGEM_SEPARACAO = A.EMBALAGEM_SEPARACAO      
                    AND B.STATUS              = 0 --LIBERADO--      
  WHERE A.CHECKOUT          = @CHECKOUT      
    AND B.REGISTRO            IS NULL      
    AND A.EMBALAGEM_SEPARACAO           IS NOT NULL      
 AND A.EMBALAGEM_SEPARACAO_EXPEDICAO IS NOT NULL      
 AND A.EMBALAGEM_SEPARACAO <> A.EMBALAGEM_SEPARACAO_EXPEDICAO      
 AND A.CONFIRMAR = 'S'      
    
    
  -------------------------------    
 -- ATUALIZA NUMERO DO VOLUME --    
 -------------------------------    
UPDATE WMS_CHECKOUT_VOLUMES     
   SET VOLUME = B.VOLUME    
  FROM WMS_CHECKOUT_VOLUMES A WITH(NOLOCK)    
  JOIN (  SELECT A.CHECKOUT_VOLUME,    
                 ROW_NUMBER() OVER(PARTITION BY A.CHECKOUT ORDER BY A.CHECKOUT_VOLUME) AS VOLUME    
            FROM WMS_CHECKOUT_VOLUMES A WITH(NOLOCK)    
           WHERE A.CHECKOUT = @CHECKOUT ) B ON A.CHECKOUT_VOLUME = B.CHECKOUT_VOLUME    
     
     
 ---------------------------------------------------    
 -- ATUALIZA DADOS DE VOLUMES E PESO NO ROMANEIO  --    
 ---------------------------------------------------    
 UPDATE ESTOQUE_TRANSFERENCIAS    
    SET VOLUMES            = C.VOLUMES,    
     PESO_BRUTO_TOTAL   = C.TOTAL_PESO_BRUTO,    
  PESO_LIQUIDO_TOTAL = C.TOTAL_PESO_LIQUIDO    
   FROM ESTOQUE_TRANSFERENCIAS            A WITH(NOLOCK)    
   JOIN (    
           SELECT DISTINCT CHECKOUT, ESTOQUE_TRANSFERENCIA      
             FROM WMS_CHECKOUT_RESUMO A WITH(NOLOCK)    
            WHERE CHECKOUT = @CHECKOUT    
        )                                 B ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
   JOIN ( SELECT A.CHECKOUT       AS CHECKOUT,    
                 COUNT(A.VOLUME)             AS VOLUMES,    
           SUM(A.TOTAL_PESO_BRUTO)     AS TOTAL_PESO_BRUTO,    
           SUM(A.TOTAL_PESO_LIQUIDO)   AS TOTAL_PESO_LIQUIDO    
    
            FROM WMS_CHECKOUT_VOLUMES A WITH(NOLOCK)     
    
        GROUP BY A.CHECKOUT ) C             ON C.CHECKOUT = B.CHECKOUT    
      
  WHERE B.CHECKOUT = @CHECKOUT    
    
      
    
      
END 