DECLARE @OPERACAO AS VARCHAR(10)
DECLARE @DATA_INI AS DATETIME
DECLARE @DATA_FIM AS DATETIME
DECLARE @ENTIDADE AS VARCHAR(10)
DECLARE @PRODUTO  AS NUMERIC              

SET @DATA_INI = :DATA_INICIAL    
SET @DATA_FIM = :DATA_FINAL     

SELECT @OPERACAO = ISNULL(DBO.NUMERICO_NULL(:OPERACAO ),0)
SELECT @PRODUTO  = ISNULL(DBO.NUMERICO_NULL(:PRODUTO  ),0)

 
SELECT        C.NF_NUMERO                AS NUMERO_NOTA          ,
              C.EMISSAO                  AS DATA_EMISSAO         ,
              C.EMPRESA                  AS EMPRESA              ,
              C.MOVIMENTO                AS MOVIMENTO            ,
              C.PEDIDO_COMPRA            AS PEDIDO_COMPRA        ,
              C.RECEBIMENTO              AS NUMERO_RECEBIMENTO   ,
              D.NOME                     AS DISTRIBUIDORA        ,
              C.PROCESSAR                AS PROCESSAR            ,
              B.PRODUTO                  AS PRODUTO              ,
              E.DESCRICAO                AS NOME_PRODUTO         ,
              A.DESCRICAO                AS CFOP                 ,
              B.VALOR_UNITARIO           AS VALOR_UNITARIO       ,
              B.QUANTIDADE               AS QUANTIDADE           ,
              B.QUANTIDADE_EMBALAGEM     AS EMBALAGEM            ,
              B.TOTAL_PRODUTO            AS TOTAL                ,
              F.CUSTO                    AS CUSTO                ,
			  B.IPI_VALOR                AS IPI                  ,


     FROM OPERACOES_FISCAIS              A WITH(NOLOCK)
     JOIN NF_COMPRA_PRODUTOS             B WITH(NOLOCK) ON B.OPERACAO_FISCAL   = A.OPERACAO_FISCAL
     JOIN NF_COMPRA                      C WITH(NOLOCK) ON C.NF_COMPRA         = B.NF_COMPRA
     JOIN ENTIDADES                      D WITH(NOLOCK) ON D.ENTIDADE          = C.ENTIDADE
     JOIN PRODUTOS                       E WITH(NOLOCK) ON E.PRODUTO           = B.PRODUTO
LEFT JOIN ESPELHO                         F WITH(NOLOCK) ON F.NF_COMPRA_PRODUTO = B.NF_COMPRA_PRODUTO
WHERE ( ( @OPERACAO <> '0' AND B.OPERACAO_FISCAL = @OPERACAO ) OR 
        ( @OPERACAO  = '0') )

AND   ( ( @PRODUTO <> '0' AND B.PRODUTO = @PRODUTO) OR
        ( @PRODUTO  = '0' ) )

AND   C.MOVIMENTO >= @DATA_INI           
AND   C.MOVIMENTO <= @DATA_FIM             

ORDER BY MOVIMENTO