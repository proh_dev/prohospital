-- perguntar porque esta gerando arquivo relatorio
-- CRIAR PRODUTO MARCA GRUPO SUB	GRUPO 
USE PBS_HOMOLOGACAO_DADOS
   SELECT --:DATA_I
		   '01/01/2018'                                                   AS DATA_I ,
          --:DATA_F
		  '10/01/2018'                                                          AS DATA_F , 
          C.PRODUTO                                                       AS PRODUTO , 
          C.DESCRICAO                                                     AS DESCRICAO , 
          SUM ( A.QUANTIDADE )                                            AS QUANTIDADE ,
          SUM ( A.VENDA_BRUTA )                                           AS VENDA_BRUTA , 
          SUM ( A.DESCONTO )                                              AS DESCONTO , 
          SUM ( A.DESCONTO_AUTOMATICO )                                   AS DESCONTO_AUTOMATICO , 
          SUM ( A.DESCONTO ) -  
          SUM ( A.DESCONTO_AUTOMATICO )                                   AS DESCONTO_CONCEDIDO ,
          SUM ( A.VENDA_LIQUIDA )                                         AS VENDA_LIQUIDA , 
          SUM ( A.IMPOSTOS )                                              AS IMPOSTOS ,
          SUM ( A.QUANTIDADE * ISNULL ( B.CUSTO_CONTABIL , 0 ) )          AS CMV , 


          SUM ( CASE WHEN A.TIPO_BONIFICACAO = 'V' 
                     THEN A.QUANTIDADE * A.BONIFICACAO
                     ELSE A.VENDA_LIQUIDA * ( A.BONIFICACAO / 100 ) END ) AS BONIFICACAO , 


          SUM ( CASE WHEN A.TIPO_COMISSAO = 'V' 
                     THEN A.QUANTIDADE * A.COMISSAO
                     ELSE A.VENDA_LIQUIDA * ( A.COMISSAO / 100 ) END ) AS COMISSAO ,


          SUM ( A.VENDA_LIQUIDA ) - 
          SUM ( A.IMPOSTOS ) - 
          SUM ( A.QUANTIDADE * ISNULL ( B.CUSTO_CONTABIL , 0 ) )          AS LUCRO_BRUTO ,
		  
		  C.GRUPO_PRODUTO												  AS GRUPO_PRODUTO,
		  C.NCM															  AS NCM 

     INTO #TEMP_01
     FROM VENDAS_ANALITICAS                   A WITH(NOLOCK)
     JOIN EMPRESAS_USUARIAS                   X WITH(NOLOCK) ON X.EMPRESA_USUARIA = A.EMPRESA --TESTAR SYNTAX DEPOIS COM INNER
LEFT JOIN CUSTO_MEDIO_MENSAL_EMPRESA_CONTABIL B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO AND B.EMPRESA_CONTABIL = X.EMPRESA_CONTABIL AND B.MES = MONTH ( A.MOVIMENTO ) AND B.ANO = YEAR ( A.MOVIMENTO ) 
LEFT JOIN PRODUTOS                            C WITH(NOLOCK) ON C.PRODUTO = A.PRODUTO
    WHERE A.MOVIMENTO >= '01/01/2018'  AND 
          A.MOVIMENTO <= '10/01/2018' 

          --AND ISNULL(A.CLIENTE,999999) >= CASE WHEN ISNULL(:ENTRE_EMPRESAS,'S') = 'S' THEN 0 ELSE 1001 END  --SYNTAX ??

 GROUP BY C.PRODUTO , C.DESCRICAO , C.GRUPO_PRODUTO, C.NCM

SELECT '01/01/2018'                                         AS DATA_I , 
       '10/01/2018'                                        AS DATA_F , 
       PRODUTO                                        AS PRODUTO , 
       DESCRICAO                                      AS DESCRICAO ,
       QUANTIDADE                                     AS QUANTIDADE , 
       VENDA_BRUTA                                    AS VENDA_BRUTA ,
       DESCONTO                                       AS DESCONTO , 
	   
 
       CASE WHEN VENDA_BRUTA > 0 THEN 
     ( DESCONTO / VENDA_BRUTA )      * 100 ELSE 0 END AS PDESCONTO ,  --PDESCONTO STANDS FOR PORCETAGEM DESCONTO NO PRODUTO     

       DESCONTO_AUTOMATICO                            AS DESCONTO_AUTOMATICO ,
       CASE WHEN VENDA_BRUTA > 0 THEN 
     ( DESCONTO_AUTOMATICO/VENDA_BRUTA)*100 ELSE 0 END AS PDESCONTO_AUTOMATICO , 

       DESCONTO_CONCEDIDO                             AS DESCONTO_CONCEDIDO , 
       CASE WHEN VENDA_BRUTA > 0 THEN 
     ( DESCONTO_CONCEDIDO/VENDA_BRUTA)*100 ELSE 0 END AS PDESCONTO_CONCEDIDO , 

       VENDA_LIQUIDA                                  AS VENDA_LIQUIDA ,
       IMPOSTOS                                       AS IMPOSTOS , 
       CASE WHEN VENDA_LIQUIDA > 0 THEN 
     ( IMPOSTOS / VENDA_LIQUIDA )    * 100 ELSE 0 END AS PIMPOSTOS , 
       CMV                                            AS CMV , 
       CASE WHEN VENDA_LIQUIDA > 0 THEN
     ( CMV / VENDA_LIQUIDA )         * 100 ELSE 0 END AS PCMV , 
       COMISSAO                                       AS COMISSAO ,
       CASE WHEN VENDA_LIQUIDA > 0 THEN 
     ( COMISSAO / VENDA_LIQUIDA )    * 100 ELSE 0 END AS PCOMISSAO , 
       LUCRO_BRUTO                                    AS LUCRO_BRUTO , 
       CASE WHEN VENDA_LIQUIDA > 0 THEN
     ( LUCRO_BRUTO / VENDA_LIQUIDA ) * 100 ELSE 0 END AS PLUCRO_BRUTO ,
       CASE WHEN QUANTIDADE <> 0
            THEN VENDA_LIQUIDA / QUANTIDADE ELSE 0 END AS PRECO_MEDIO ,
       CASE WHEN QUANTIDADE <> 0 
            THEN CMV / QUANTIDADE ELSE 0 END          AS CUSTO_MEDIO ,
	   GRUPO_PRODUTO                                  AS GRUPO_PRODUTO,
	   NCM											  AS NCM
  FROM #TEMP_01

DROP TABLE #TEMP_01
             