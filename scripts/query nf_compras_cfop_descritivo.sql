/* ====================================================== */   
/* CONSULTA DE NOTAS FISCAIS ENTRADAS                     */
/* VERS�O 2                                               */
/* AUTOR: YNOA PEDRO & PAULO ANDERSON(APRENDIZ)           */
/* DATA : 10-02-2018                                      */
/* ====================================================== */  

--DECLARE @EMPRESA_INI  NUMERIC(15,0) = 2
--DECLARE @EMPRESA_FIM  NUMERIC(15,0) = 2
--DECLARE @ENTIDADE     NUMERIC(15,0) --= 4 
--DECLARE @DATA_INICIAL DATE          ='01/3/2017'  
--DECLARE @DATA_FINAL   DATE          ='26/02/2018'


DECLARE @EMPRESA_INI  NUMERIC(15,0) 
DECLARE @EMPRESA_FIM  NUMERIC(15,0) 
DECLARE @ENTIDADE     NUMERIC(15,0) 
DECLARE @DATA_INICIAL DATE          = :DATA_INICIAL   
DECLARE @DATA_FINAL   DATE          = :DATA_FINAL   


IF ISNUMERIC(:EMPRESA_INI) = 1         
    SET @EMPRESA_INI = :EMPRESA_INI              
  ELSE  
    SET @EMPRESA_INI = NULL       


IF ISNUMERIC(:EMPRESA_FIM) = 1 
    SET @EMPRESA_FIM = :EMPRESA_FIM
 ELSE  
    SET @EMPRESA_FIM = NULL

IF ISNUMERIC(:ENTIDADE) = 1 
    SET @ENTIDADE = :ENTIDADE
 ELSE  
    SET @ENTIDADE = NULL  

SELECT     
    A.MOVIMENTO                                                             AS MOVIMENTO
   ,A.EMPRESA                                                               AS EMPRESA
   ,A.ENTIDADE                                                              AS ENTIDADE
   ,G.NOME                                                                  AS NOME_ENTIDADE
   ,A.NF_NUMERO                                                             AS NF_NUMERO
   ,K.VALOR_PRODUTO_LIQUIDO                                                 AS TOTAL_VALOR_PRODUTO
   ,B.QUANTIDADE                                                            AS QUANTIDADE_PRODUTO
   ,B.PRODUTO                                                               AS PRODUTO
   ,D.DESCRICAO                                                             AS DESCRICAO
   ,K.CUSTO                                                                 AS TOTAL_GERAL
   ,K.FRETE                                                                 AS FRETE
   ,B.CLASSIF_FISCAL_CODIGO                                                 AS NCM                                                      
   ,CFOP_INTERNA                                                            AS CFOP
   ,C.DESCRICAO                                                             AS DESCRICAO_CFOP
   ,ISNULL(CASE WHEN ISNULL(E.ESTADO,'RJ') = J.ESTADO 
                   THEN CASE WHEN SUBSTRING(B.SITUACAO_TRIBUTARIA,2,2) IN ('10','60','30','70') 
                             THEN ISNULL(C.CFOP_INTERNA_ST, '0.000')
                             ELSE ISNULL(C.CFOP_INTERNA, '0.000') 
                        END
                   ELSE CASE WHEN SUBSTRING(B.SITUACAO_TRIBUTARIA,2,2) IN ('10','60','30','70')
                             THEN ISNULL(C.CFOP_INTERESTADUAL_ST,'0.000')                    
                             ELSE ISNULL(C.CFOP_INTERESTADUAL,'0.000') 
                        END 
              END,'9.999')                                                  AS CFOP
   ,B.SITUACAO_TRIBUTARIA                                                   AS CST
   ,B.ICMS_ALIQUOTA                                                         AS ICMS_ALIQUOTA
   ,B.ICMS_VALOR                                                            AS ICMS_VALOR -- VALOR_CREDITO
   
      
   FROM NF_COMPRA                  A WITH(NOLOCK)
   JOIN NF_COMPRA_PRODUTOS         B WITH(NOLOCK)ON B.NF_COMPRA           = A.NF_COMPRA
   ---- opera��o fiscal / produto
   JOIN OPERACOES_FISCAIS          C WITH(NOLOCK)ON C.OPERACAO_FISCAL     = B.OPERACAO_FISCAL
   JOIN PRODUTOS                   D WITH(NOLOCK)ON D.PRODUTO             = B.PRODUTO
   ---- entidade
  LEFT JOIN ENDERECOS              E WITH(NOLOCK)ON E.ENTIDADE            = A.ENTIDADE
 --INNER JOIN NF_COMPRA_TOTAIS       F WITH(NOLOCK)ON F.NF_COMPRA           = A.NF_COMPRA
  LEFT JOIN ENTIDADES              G WITH(NOLOCK)ON G.ENTIDADE            = A.ENTIDADE
  LEFT JOIN ENTIDADES_FISCAL       P WITH(NOLOCK)ON P.ENTIDADE            = A.ENTIDADE
  LEFT JOIN SITUACOES_TRIBUTARIAS  Y WITH(NOLOCK)ON Y.SITUACAO_TRIBUTARIA = B.SITUACAO_TRIBUTARIA
  ---- empresa usuaria
 INNER JOIN EMPRESAS_USUARIAS      I WITH(NOLOCK)ON I.EMPRESA_USUARIA     = A.EMPRESA
  LEFT JOIN ENDERECOS              J WITH(NOLOCK)ON J.ENTIDADE            = I.ENTIDADE
  LEFT JOIN ESPELHO                K WITH(NOLOCK)ON K.NF_COMPRA_PRODUTO   = B.NF_COMPRA_PRODUTO
         
  WHERE 1=1
      --AND SUBSTRING(B.SITUACAO_TRIBUTARIA,2,2)        IN ('00','20')
        AND (ISNULL(A.PROCESSAR,'N')                    = 'S'                                   )
        AND (A.MOVIMENTO                               >=  @DATA_INICIAL                        )  
        AND (A.MOVIMENTO                               <=  @DATA_FINAL                          )  
        AND (A.EMPRESA                                 >=  @EMPRESA_INI  OR @EMPRESA_INI IS NULL) 
        AND (A.EMPRESA                                 <=  @EMPRESA_FIM  OR @EMPRESA_FIM IS NULL)  
        AND (A.ENTIDADE                                 =  @ENTIDADE     OR @ENTIDADE    IS NULL)   

                                 
ORDER BY A.MOVIMENTO 