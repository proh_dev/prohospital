/*Inserir Dados Conforme Saldo do Pedido*/
                   
DECLARE @SOLICITACAO_FATURAMENTO NUMERIC(15) = :SOLICITACAO_FATURAMENTO
DECLARE @PEDIDO_VENDA            NUMERIC(15) = :PEDIDO_VENDA       
                   
DELETE SOLICITACOES_FATURAMENTOS_PRODUTOS
 WHERE SOLICITACAO_FATURAMENTO_PRODUTO IN (SELECT SOLICITACAO_FATURAMENTO_PRODUTO 
                                           FROM SOLICITACOES_FATURAMENTOS_PRODUTOS WITH(NOLOCK)
                                          WHERE SOLICITACAO_FATURAMENTO = @SOLICITACAO_FATURAMENTO
                                          )
                      
INSERT 
  INTO SOLICITACOES_FATURAMENTOS_PRODUTOS
      (                                        
       FORMULARIO_ORIGEM                  ,
       TAB_MASTER_ORIGEM                  ,
       REG_MASTER_ORIGEM                  ,
       SOLICITACAO_FATURAMENTO            ,
       PRODUTO                            ,
       QUANTIDADE                         ,
       QUANTIDADE_ORIGINAL                ,
       ESTOQUE_SALDO, 
       RESERVA,
       ESTOQUE_DISPONIVEL,
       UNIDADE_MEDIDA,
       VALOR_UNITARIO    ,
       TOTAL_DESCONTO    ,
       TOTAL_PRODUTO
      )                                                   
                     
SELECT       
       B.FORMULARIO_ORIGEM                   AS FORMULARIO_ORIGEM       ,
       B.TAB_MASTER_ORIGEM                   AS TAB_MASTER_ORIGEM       ,
       B.SOLICITACAO_FATURAMENTO             AS REG_MASTER_ORIGEM       ,
       B.SOLICITACAO_FATURAMENTO             AS SOLICITACAO_FATURAMENTO,
       A.PRODUTO                             AS PRODUTO                 , 
       A.SALDO                               AS QUANTIDADE,
       A.SALDO                               AS QUANTIDADE_ORIGINAL,
       ISNULL(E.ESTOQUE_SALDO,0)             AS ESTOQUE_SALDO,
       ISNULL(D.RESERVA,0)                   AS RESERVA,
       ISNULL(E.ESTOQUE_SALDO,0) - ISNULL(D.RESERVA,0) - A.SALDO
                                             AS ESTOQUE_DISPONIVEL ,
       P.UNIDADE_MEDIDA                      AS UNIDADE_MEDIDA,
       Z.VALOR_UNITARIO    ,
       Z.TOTAL_DESCONTO    ,
       Z.TOTAL_PRODUTO
         
FROM 
    PEDIDOS_VENDAS_PRODUTOS_SALDO         A WITH(NOLOCK)
    JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA Z WITH(NOLOCK) ON Z.PEDIDO_VENDA            = A.PEDIDO_VENDA
                                                        AND Z.PRODUTO                 = A.PRODUTO  
    JOIN SOLICITACOES_FATURAMENTOS        B WITH(NOLOCK) ON B.SOLICITACAO_FATURAMENTO = @SOLICITACAO_FATURAMENTO
    JOIN EMPRESAS_ESTOQUES                C WITH(NOLOCK) ON B.EMPRESA                 = C.EMPRESA_USUARIA
                                                        AND C.OBJETO_CONTROLE         = Z.OBJETO_CONTROLE
    LEFT JOIN ESTOQUE_RESERVAS_SALDO      D WITH(NOLOCK) ON C.OBJETO_CONTROLE         = D.CENTRO_ESTOQUE
                                                        AND A.PRODUTO                 = D.PRODUTO
    LEFT JOIN ESTOQUE_ATUAL               E WITH(NOLOCK) ON C.OBJETO_CONTROLE         = E.CENTRO_ESTOQUE                            
                                                        AND A.PRODUTO                 = E.PRODUTO
    JOIN PRODUTOS                         P WITH(NOLOCK) ON P.PRODUTO                 = A.PRODUTO        
                                                        
WHERE 
    A.PEDIDO_VENDA = @PEDIDO_VENDA    
AND A.SALDO > 0


DELETE SOLICITACOES_FATURAMENTOS_PRODUTOS_TOTAIS 
 WHERE SOLICITACAO_FATURAMENTO_TOTAL IN (SELECT SOLICITACAO_FATURAMENTO_TOTAL 
                                           FROM SOLICITACOES_FATURAMENTOS_PRODUTOS_TOTAIS WITH(NOLOCK)
                                          WHERE SOLICITACAO_FATURAMENTO = @SOLICITACAO_FATURAMENTO
                                         )
                           
INSERT 
  INTO SOLICITACOES_FATURAMENTOS_PRODUTOS_TOTAIS
      (                                        
           FORMULARIO_ORIGEM,
           TAB_MASTER_ORIGEM,
           REG_MASTER_ORIGEM,
           SOLICITACAO_FATURAMENTO,
           TOTAL_VALOR,
           TOTAL_QUANTIDADE
      )                               

SELECT
       A.FORMULARIO_ORIGEM                   AS FORMULARIO_ORIGEM       ,
       A.TAB_MASTER_ORIGEM                   AS TAB_MASTER_ORIGEM       ,
       A.SOLICITACAO_FATURAMENTO             AS REG_MASTER_ORIGEM       ,
       A.SOLICITACAO_FATURAMENTO             AS SOLICITACAO_FATURAMENTO ,
       SUM(A.TOTAL_PRODUTO)                  AS TOTAL_VALOR,
       COUNT(A.CONTADOR)                      AS TOTAL_QUANTIDADE

      FROM  SOLICITACOES_FATURAMENTOS_PRODUTOS A WITH(NOLOCK)
WHERE 
    A.SOLICITACAO_FATURAMENTO = @SOLICITACAO_FATURAMENTO


GROUP BY
A.FORMULARIO_ORIGEM        ,
A.TAB_MASTER_ORIGEM        ,
A.SOLICITACAO_FATURAMENTO  ,
A.SOLICITACAO_FATURAMENTO  