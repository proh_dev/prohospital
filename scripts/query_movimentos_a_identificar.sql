/* ====================================================== */
/* CONSULTA DE MOVIMENTOS BANCARIOS A IDENTIFICAR         */
/* AUTOR: YNOA PEDRO                                      */
/* DATA:  28-02-2018                                      */                   
/* ====================================================== */
DECLARE @CONTA_BANCARIA NUMERIC(15)
DECLARE @DATA_INI       DATETIME
DECLARE @DATA_FIM       DATETIME
DECLARE @EMPRESA_INI    NUMERIC
DECLARE @EMPRESA_FIM    NUMERIC
            
SET @CONTA_BANCARIA = :CONTA_BANCARIA
SET @DATA_INI       = :DATA_INI
SET @DATA_FIM       = :DATA_FIM


--SET @CONTA_BANCARIA = 1
--SET @DATA_INI       = '01/01/2018'
--SET @DATA_FIM       = '31/12/2018'



SELECT          A.MOVIMENTO_BANCARIO		       AS CHAVE,
                A.DATA_HORA                        AS DATA,
                B.TRANSACAO_BANCARIA               AS TRANSACAO_BANCARIA,
                B.AG_ORIGEM                        AS AGENCIA_ORIGEM,
				C.DESCRICAO                        AS DESCRICAO,
                D.DESCRICAO                        AS PROCESSO,
                B.VALOR                            AS VALOR,
				B.MUNICIPIO                        AS MUNICIPIO,
				B.DOC_ORIGEM                       AS DOCUMENTO_ORIGEM,
				B.OBSERVACAO                       AS OBSERVACAO
				
				

           FROM  MOVIMENTOS_BANCARIOS                  A WITH(NOLOCK)

           JOIN  MOVIMENTOS_BANCARIOS_LANCAMENTOS      B WITH (NOLOCK) ON A.MOVIMENTO_BANCARIO      = B.MOVIMENTO_BANCARIO

		   JOIN TIPOS_TRANSACOES_BANCARIAS             C WITH (NOLOCK) ON B.TRANSACAO_BANCARIA      = C.TIPO_TRANSACAO_BANCARIA
                                                                
		   JOIN TRANSACOES_BANCARIAS                   D WITH (NOLOCK) ON C.TIPO_TRANSACAO_BANCARIA =	D.TRANSACAO_BANCARIA	   
		                                                                 
          WHERE 1 = 1
            AND A.CONTA_BANCARIA = @CONTA_BANCARIA
            AND A.MOVIMENTO     >= @DATA_INI
            AND A.MOVIMENTO     <= @DATA_FIM
			AND B.TRANSACAO_BANCARIA = 12
