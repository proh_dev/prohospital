 DECLARE @NF_COMPRA NUMERIC  
 DECLARE @RECEBIMENTO NUMERIC
                   


     IF ISNUMERIC(:NF_COMPRA)= 1 SET @NF_COMPRA  = :NF_COMPRA  ELSE SET @NF_COMPRA  = NULL
	 --SET @NF_COMPRA   =20386



SET @RECEBIMENTO = (SELECT RECEBIMENTO FROM NF_COMPRA WHERE NF_COMPRA = @NF_COMPRA)
 

--GERA TEMP DE PEDIDOS--
 if object_id('tempdb..#PEDIDOS') is not null
   DROP TABLE #PEDIDOS
   
   
 SELECT DISTINCT A.NF_COMPRA,A.PEDIDO_COMPRA
 
   INTO #PEDIDOS
 
   FROM NF_COMPRA        A WITH(NOLOCK)
  WHERE A.NF_COMPRA      = @NF_COMPRA
   
   
------------------------------------------------
 --VERIFICACAO DOS ITENS PEDIDO CONTRA NOTA/VALORES
------------------------------------------------

 if object_id('tempdb..#ITENS') is not null
   DROP TABLE #ITENS
 
 
 SELECT DISTINCT A.PRODUTO
     
   INTO #ITENS
 
   FROM (
 
 
    SELECT DISTINCT C.PRODUTO 
		FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
		JOIN RECEBIMENTOS_VOLUMES_NF             B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
		JOIN NF_COMPRA_PRODUTOS                  C WITH(NOLOCK) ON C.NF_COMPRA   = B.NF_COMPRA

	WHERE C.NF_COMPRA   = @NF_COMPRA

  UNION ALL
  
  SELECT DISTINCT C.PRODUTO 
    FROM #PEDIDOS                            A WITH(NOLOCK)
    JOIN PEDIDOS_COMPRAS_PRODUTOS            C WITH(NOLOCK) ON C.PEDIDO_COMPRA  = A.PEDIDO_COMPRA 
  

  UNION ALL
  
  SELECT DISTINCT B.PRODUTO 
    FROM RECEBIMENTOS_VOLUMES							  A WITH(NOLOCK)
    JOIN RECEBIMENTOS_VOLUMES_PRODUTOS					  B WITH(NOLOCK) ON B.RECEBIMENTO  = A.RECEBIMENTO
  WHERE (A.RECEBIMENTO = @RECEBIMENTO OR @RECEBIMENTO IS NULL)

  UNION ALL
  SELECT DISTINCT C.PRODUTO 
    FROM RECEBIMENTOS_VOLUMES							  A WITH(NOLOCK)
    JOIN RECEBIMENTOS_VOLUMES_PRODUTOS					  B WITH(NOLOCK) ON B.RECEBIMENTO  = A.RECEBIMENTO
	JOIN APROVACOES_DIVERGENCIAS_NF_COMPRA_PRODUTOS       C WITH(NOLOCK) ON C.RECEBIMENTO  = A.RECEBIMENTO
  WHERE (A.RECEBIMENTO = @RECEBIMENTO OR @RECEBIMENTO IS NULL)

             
   ) A



  
  SELECT B.RECEBIMENTO,
	     --As duas linhas abaixo concatenam as notas por recebimento, mas como o recebimento � o parametro que define o comportamento da query sempre temos um resultado concatenado
		 --exemplo bem util de uso 
         DBO.CONCATENA_NOTAS   ( B.RECEBIMENTO ) AS NOTAS,
         DBO.CONCATENA_PEDIDOS ( B.RECEBIMENTO ) AS PEDIDOS,
                  
         A.PRODUTO,
         C.DESCRICAO,
         ISNULL(Y.QUANTIDADE_PEDIDO,0)       AS QUANTIDADE_PEDIDO,
         ISNULL(X.QUANTIDADE_NOTA  ,0)       AS QUANTIDADE_NOTA,
         ISNULL(Z.QUANTIDADE_RECEBIMENTO,0 ) AS QUANTIDADE_RECEBIDA,
		 ISNULL(B1.VALOR_PEDIDO,0)           AS TOTAL_PEDIDO,
		 ISNULL(A1.VALOR_NOTA,0)             AS TOTAL_NOTA,

        CASE WHEN ( ( ISNULL(B1.VALOR_PEDIDO,0) - ISNULL(A1.VALOR_NOTA,0) ) <>  0  ) 
             THEN CASE WHEN ( (  ISNULL(B1.VALOR_PEDIDO,0) - ISNULL(A1.VALOR_NOTA,0) ) <>  0  )
                       THEN ( ( ISNULL(A1.VALOR_NOTA,0) - ISNULL(B1.VALOR_PEDIDO,0) ) )
					   ELSE 0
                    END
              ELSE 0
         END AS DIVERGENCIA_NOTA,

	     CASE WHEN  ( (ISNULL(B1.VALOR_PEDIDO,0) - ISNULL(A1.VALOR_NOTA,0) ) <>  0  )  THEN 'Pedido <> Nota'
                       
					    ELSE 'OK'
                   
                   
          END  AS MENSAGEM_NOTA,

         CASE WHEN           ( ( ISNULL(Y.QUANTIDADE_PEDIDO, 0)        - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  ) OR 
                             ( ( ISNULL(Y.QUANTIDADE_PEDIDO, 0)        - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) OR          
                             ( ( ISNULL(X.QUANTIDADE_NOTA, 0)          - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) 
	

              THEN CASE WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0)		   - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  )
                        THEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0)		   - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) )
																	   
                        WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0)		   - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) 
                        THEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0)		   - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) )
																	   
                        WHEN ( ( ISNULL(X.QUANTIDADE_NOTA  ,0)		   - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  )
                        THEN ( ( ISNULL(X.QUANTIDADE_NOTA  ,0)		   - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) )
																	   

                        ELSE 0
                    END
              ELSE 0
         END AS DIVERGENCIA,

                  
         CASE WHEN ISNULL(Y.QUANTIDADE_PEDIDO,0) = 0
              THEN 'Produto Sem Pedido'
              ELSE CASE --WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  ) THEN 'Pedido <> Nota'
                        WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) THEN 'Pedido <> Fisico'
                        WHEN ( ( ISNULL(X.QUANTIDADE_NOTA  ,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) THEN 'Nota <> Fisico'
                        ELSE 'OK'
                   END
                   
         END AS MENSAGEM,
		 CASE WHEN ISNULL(Y.QUANTIDADE_PEDIDO,0) = 0
              THEN 'Produto Sem Pedido'
              ELSE CASE WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  ) THEN 'Pedido <> Nota'
                        ELSE 'OK'
                   END
                   
         END AS MENSAGEM_PEDIDO,
         1   AS ITEM
                       
         
    FROM #ITENS						A WITH(NOLOCK)    
       JOIN NF_COMPRA				B WITH(NOLOCK) ON B.NF_COMPRA     = @NF_COMPRA
	   JOIN PRODUTOS				C WITH(NOLOCK) ON C.PRODUTO       = A.PRODUTO 
	   LEFT
       JOIN PEDIDOS_COMPRAS			E WITH(NOLOCK) ON E.PEDIDO_COMPRA = B.PEDIDO_COMPRA
	   JOIN PEDIDOS_COMPRAS_TOTAIS  F WITH(NOLOCK) ON F.PEDIDO_COMPRA = E.PEDIDO_COMPRA
	    
	   JOIN  RECEBIMENTOS_VOLUMES   G WITH(NOLOCK) ON G.RECEBIMENTO = @RECEBIMENTO


    LEFT JOIN ( SELECT C.PRODUTO,
                     SUM(C.QUANTIDADE_ESTOQUE) AS QUANTIDADE_NOTA                     
                FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
                JOIN RECEBIMENTOS_VOLUMES_NF             B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
                JOIN NF_COMPRA_PRODUTOS                  C WITH(NOLOCK) ON C.NF_COMPRA   = B.NF_COMPRA
               WHERE C.NF_COMPRA = @NF_COMPRA 
			     AND (A.RECEBIMENTO = @RECEBIMENTO  OR @RECEBIMENTO IS NULL)
              GROUP BY C.PRODUTO ) X ON X.PRODUTO = A.PRODUTO

  LEFT JOIN ( SELECT C.PRODUTO,
                     SUM(C.QUANTIDADE_ESTOQUE) AS QUANTIDADE_PEDIDO                     
                FROM #PEDIDOS                            A WITH(NOLOCK)
                JOIN PEDIDOS_COMPRAS_PRODUTOS            C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA
              GROUP BY C.PRODUTO ) Y ON Y.PRODUTO = A.PRODUTO             

  LEFT JOIN ( SELECT B.PRODUTO,
                     SUM(B.QUANTIDADE_UNIT) AS QUANTIDADE_RECEBIMENTO                    
                FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
                JOIN RECEBIMENTOS_VOLUMES_PRODUTOS       B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
               WHERE (A.RECEBIMENTO = @RECEBIMENTO  OR @RECEBIMENTO IS NULL)
              GROUP BY B.PRODUTO ) Z ON Z.PRODUTO = A.PRODUTO  
			  
    LEFT JOIN ( SELECT C.PRODUTO,
                     SUM(C.VALOR_UNITARIO)              AS VALOR_NOTA                     
                FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
                JOIN RECEBIMENTOS_VOLUMES_NF             B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
                JOIN NF_COMPRA_PRODUTOS                  C WITH(NOLOCK) ON C.NF_COMPRA   = B.NF_COMPRA
               WHERE C.NF_COMPRA = @NF_COMPRA 
			     AND (A.RECEBIMENTO = @RECEBIMENTO  OR @RECEBIMENTO IS NULL)
              GROUP BY C.PRODUTO ) A1 ON A1.PRODUTO = A.PRODUTO

  LEFT JOIN ( SELECT C.PRODUTO,
                     SUM(C.VALOR_UNITARIO)               AS VALOR_PEDIDO                     
                FROM #PEDIDOS                            A WITH(NOLOCK)
                JOIN PEDIDOS_COMPRAS_PRODUTOS            C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA
              GROUP BY C.PRODUTO ) B1 ON B1.PRODUTO = A.PRODUTO    


  

          
  ORDER BY MENSAGEM
  