CREATE VIEW [dbo].[ESTOQUE_RESERVAS_TRANSACOES] AS  
  
       
--PEGA ROMANEIO EM ABERTO--        
SELECT A.ESTOQUE_TRANSFERENCIA                                      AS DOCUMENTO,        
       'ROMANEIO'                                                   AS TIPO,        
       A.CENTRO_ESTOQUE_ORIGEM                                      AS CENTRO_ESTOQUE,        
       A.EMPRESA                                                    AS EMPRESA,        
       A.MOVIMENTO                                                  AS DATA,        
       CAST(( B.QTDE_ORIGINAL - B.ACERTO_FALTA ) AS NUMERIC(15,0) ) AS RESERVA,        
       B.PRODUTO                                                    AS PRODUTO,    
       A.ENTIDADE                                                   AS ENTIDADE,    
       A.PEDIDO_VENDA                                               AS PEDIDO_VENDA    
          
  FROM ESTOQUE_TRANSFERENCIAS              A WITH(NOLOCK)        
  JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES   B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA        
  JOIN ESTOQUE_TRANSFERENCIAS_SALDO        C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA          
                                                         AND C.PRODUTO                = B.PRODUTO          
               AND C.ESTADO                <> 3   -- NOTA EMITIDA      
  LEFT       
  JOIN    (      
            SELECT  A.PRODUTO,        
                    A.ESTOQUE_TRANSFERENCIA        
             FROM NF_FATURAMENTO_PRODUTOS      A WITH(NOLOCK)        
             JOIN ESTOQUE_TRANSFERENCIAS       C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA        
             LEFT       
          JOIN CANCELAMENTOS_NOTAS_FISCAIS  B WITH(NOLOCK) ON B.CHAVE = A.NF_FATURAMENTO        
                                                             AND B.TIPO  = 1         
            WHERE  B.NF_CANCELAMENTO IS NULL        
         GROUP BY  A.PRODUTO,        
                   A.ESTOQUE_TRANSFERENCIA       
       )                                  X ON X.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA        
                                       --    AND X.PRODUTO               = B.PRODUTO        
   LEFT       
   JOIN (       
         SELECT DISTINCT A.ESTOQUE_TRANSFERENCIA        
           FROM APROVACAO_ESTOQUE_TRANSF_ITENS A WITH(NOLOCK)       
        )                                 Y ON Y.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA        
        
   LEFT       
   JOIN (       
         SELECT DISTINCT A.ESTOQUE_TRANSFERENCIA        
           FROM ESTOQUE_TRANSFERENCIAS                 A WITH(NOLOCK)        
           JOIN DEPOSITO_FATURAMENTOS_ITENS            B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA        
           JOIN APROVACOES_FATURAMENTOS_FUTUROS        C WITH(NOLOCK) ON C.DEPOSITO_FATURAMENTO  = B.DEPOSITO_FATURAMENTO        
          WHERE C.PROCESSAR = 'S'                               
        )                                Z ON Z.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA        
   LEFT       
   JOIN (       
          SELECT DISTINCT A.ESTOQUE_TRANSFERENCIA        
            FROM ESTOQUE_TRANSFERENCIAS                 A WITH(NOLOCK)        
            JOIN DEPOSITO_FATURAMENTOS_ITENS            B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA        = A.ESTOQUE_TRANSFERENCIA        
            JOIN APROVACOES_FATURAMENTOS_FUTUROS        C WITH(NOLOCK) ON C.DEPOSITO_FATURAMENTO         = B.DEPOSITO_FATURAMENTO        
            JOIN ESTORNOS_FATURAMENTOS_FUTUROS          D WITH(NOLOCK) ON D.APROVACAO_FATURAMENTO_FUTURO = C.APROVACAO_FATURAMENTO_FUTURO        
           WHERE D.PROCESSAR = 'S'        
                                                                      
        )                                W ON W.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA        
        
  LEFT JOIN (    
    
                 SELECT DISTINCT A.PEDIDO_VENDA    
                   FROM CANCELAMENTOS_RESERVAS_DETALHE A WITH(NOLOCK)    
                   JOIN CANCELAMENTOS_RESERVAS   B WITH(NOLOCK) ON B.CANCELAMENTO_RESERVA = A.CANCELAMENTO_RESERVA    
                  WHERE 1=1    
                  AND B.PROCESSAR = 'S'    
    
            )                            P           ON P.PEDIDO_VENDA = A.PEDIDO_VENDA    
     
 WHERE A.CANCELADO <> 'S'        
   AND X.PRODUTO IS NULL        
   AND Y.ESTOQUE_TRANSFERENCIA IS NULL        
   AND ( ( Z.ESTOQUE_TRANSFERENCIA IS NULL AND W.ESTOQUE_TRANSFERENCIA IS NULL )         
       OR        
      ( W.ESTOQUE_TRANSFERENCIA IS NOT NULL )        
       )        
  AND ( B.QTDE_ORIGINAL - B.ACERTO_FALTA ) > 0      
    
  AND P.PEDIDO_VENDA IS NULL -- SE TIVER BLOQUEADO GLOBAL N�O APARECER       
           
  --AND A.ESTOQUE_TRANSFERENCIA = 10125      
      
UNION ALL      
      
SELECT A.NF_FATURAMENTO           AS DOCUMENTO,        
       'NOTA_FISCAL_NAO_EMITIDA'  AS TIPO,        
       D.OBJETO_CONTROLE          AS CENTRO_ESTOQUE,        
       B.EMPRESA                  AS EMPRESA,        
       CONVERT(DATE,B.DATA_HORA)  AS DATA,        
       CAST(SUM(A.QUANTIDADE) AS NUMERIC(15,0) )          AS RESERVA,        
       A.PRODUTO                  AS PRODUTO,    
    B.ENTIDADE                 AS ENTIDADE,    
    B.PEDIDO_VENDA             AS PEDIDO_VENDA    
        
  FROM NF_FATURAMENTO_PRODUTOS          A WITH(NOLOCK)        
  JOIN NF_FATURAMENTO                   B WITH(NOLOCK) ON B.NF_FATURAMENTO      = A.NF_FATURAMENTO      
  JOIN PEDIDOS_VENDAS                   C WITH(NOLOCK) ON C.PEDIDO_VENDA        = B.PEDIDO_VENDA      
  JOIN EMPRESAS_ESTOQUES                D WITH(NOLOCK) ON D.EMPRESA_USUARIA     = B.EMPRESA        
                                                      AND D.TIPO_ESTOQUE        = 2      
  LEFT      
  JOIN NFE_CABECALHO                    E WITH(NOLOCK) ON E.XML_cNF_B03         = B.NF_FATURAMENTO      
                                                      AND E.XML_nNF_B08         = B.NF_NUMERO           
                                                      AND E.XML_serie_B07       = B.NF_SERIE            
                                                      AND E.EMPRESA          = B.EMPRESA             
  LEFT       
  JOIN NFE_LOG                          F WITH(NOLOCK) ON F.REGISTRO_NFE      = E.REGISTRO_NFE      
                                                      AND F.STATUS            = 'XML DO DESTINATARIO'      
  LEFT
  JOIN 
    LEFT JOIN (    
    
                 SELECT DISTINCT A.PEDIDO_VENDA    
                   FROM CANCELAMENTOS_RESERVAS_DETALHE A WITH(NOLOCK)    
                   JOIN CANCELAMENTOS_RESERVAS         B WITH(NOLOCK) ON B.CANCELAMENTO_RESERVA = A.CANCELAMENTO_RESERVA    
                  WHERE 1=1    
                  AND B.PROCESSAR = 'S'    
    
            )                            P           ON P.PEDIDO_VENDA = B.PEDIDO_VENDA                      
    
WHERE F.REGISTRO_NFE_ERRO IS NULL      
AND P.PEDIDO_VENDA IS NULL -- SE TIVER BLOQUEADO GLOBAL N�O APARECER         
GROUP BY       
         A.NF_FATURAMENTO,      
         D.OBJETO_CONTROLE,        
         B.EMPRESA,        
         CONVERT(DATE,B.DATA_HORA),        
         A.PRODUTO    ,    
   B.ENTIDADE     ,    
   B.PEDIDO_VENDA     
      
      
      
UNION ALL      
       
--PEGA PEDIDO DE VENDA EM ABERTO--        
--SELECT A.PEDIDO_VENDA             AS DOCUMENTO,        
--       'PEDIDO_VENDA'             AS TIPO,        
--       C.OBJETO_CONTROLE          AS CENTRO_ESTOQUE,        
--       B.EMPRESA                  AS EMPRESA,        
--       CONVERT(DATE,B.DATA_HORA)  AS DATA,        
--       A.SALDO           AS RESERVA,        
--       A.PRODUTO        
        
--  FROM PEDIDOS_VENDAS_PRODUTOS_SALDO    A WITH(NOLOCK)        
--  JOIN PEDIDOS_VENDAS                   B WITH(NOLOCK) ON B.PEDIDO_VENDA        = A.PEDIDO_VENDA        
--  JOIN EMPRESAS_ESTOQUES                C WITH(NOLOCK) ON C.EMPRESA_USUARIA     = B.EMPRESA        
--                                                      AND C.TIPO_ESTOQUE        = 2      
      
--AND A.PEDIDO_VENDA NOT IN (72, 82, 84, 98, 104, 105, 150, 176, 221, 232, 252)                       
        
--WHERE 1=1      
--AND A.SALDO > 0        
      
--UNION ALL       
      
--PEGA SOLICITACAO QUE AINDA N�O FOI FEITO ROMANEIO --        
      
SELECT A.SOLICITACAO_FATURAMENTO              AS DOCUMENTO,        
       'SOLICITACAO_FATURAMENTO'              AS TIPO,        
       C.OBJETO_CONTROLE                      AS CENTRO_ESTOQUE,        
       B.EMPRESA                        AS EMPRESA,        
       CONVERT(DATE,B.DATA_HORA)              AS DATA,        
       CAST(SUM(A.QUANTIDADE) AS NUMERIC(15,0) )                      AS RESERVA,        
       ISNULL(A.PRODUTO_SUBSTITUTO,A.PRODUTO) AS PRODUTO,    
    B.ENTIDADE                             AS ENTIDADE,    
    B.PEDIDO_VENDA                         AS PEDIDO_VENDA    
        
  FROM SOLICITACOES_FATURAMENTOS_PRODUTOS    A WITH(NOLOCK)        
  JOIN SOLICITACOES_FATURAMENTOS             B WITH(NOLOCK) ON B.SOLICITACAO_FATURAMENTO        = A.SOLICITACAO_FATURAMENTO        
  JOIN EMPRESAS_ESTOQUES                     C WITH(NOLOCK) ON C.EMPRESA_USUARIA                = B.EMPRESA        
                                                           AND C.TIPO_ESTOQUE                   = 2         
  LEFT      
  JOIN ESTOQUE_TRANSFERENCIAS                D WITH(NOLOCK) ON D.SOLICITACAO_FATURAMENTO        = A.SOLICITACAO_FATURAMENTO      
                                                           AND D.CANCELADO                      = 'N'      
  LEFT      
 JOIN NF_FATURAMENTO_TRANSF                 E WITH(NOLOCK) ON E.ESTOQUE_TRANSFERENCIA          = D.ESTOQUE_TRANSFERENCIA      
 LEFT JOIN (    
    
                 SELECT DISTINCT A.PEDIDO_VENDA    
                   FROM CANCELAMENTOS_RESERVAS_DETALHE A WITH(NOLOCK)    
                   JOIN CANCELAMENTOS_RESERVAS         B WITH(NOLOCK) ON B.CANCELAMENTO_RESERVA = A.CANCELAMENTO_RESERVA    
                  WHERE 1=1    
                  AND B.PROCESSAR = 'S'    
    
            )                            P           ON P.PEDIDO_VENDA = B.PEDIDO_VENDA     
    
       
  WHERE D.ESTOQUE_TRANSFERENCIA IS NULL      
    AND E.ESTOQUE_TRANSFERENCIA IS NULL      
    
 AND P.PEDIDO_VENDA IS NULL -- SE TIVER BLOQUEADO GLOBAL N�O APARECER      
      
 GROUP BY      
 A.SOLICITACAO_FATURAMENTO             ,              
 C.OBJETO_CONTROLE                     ,      
 B.EMPRESA                             ,      
 CONVERT(DATE,B.DATA_HORA)             ,      
 A.QUANTIDADE                          ,      
 ISNULL(A.PRODUTO_SUBSTITUTO,A.PRODUTO),    
 B.ENTIDADE     ,    
 B.PEDIDO_VENDA 