CREATE PROCEDURE [dbo].[EMAIL_ENVIO_LEMBRETE_CLIENTES]  AS   
BEGIN                      
                

DECLARE @VENCIMENTO_INI          DATE
DECLARE @VENCIMENTO_FIM          DATE
DECLARE @EMPRESA_INI             NUMERIC
DECLARE @EMPRESA_FIM             NUMERIC
DECLARE @ENTIDADE_INI            NUMERIC
DECLARE @ENTIDADE_FIM            NUMERIC
DECLARE @EMPRESA_CONTABIL        NUMERIC                    
DECLARE @MODALIDADE              NUMERIC(15)                          
DECLARE @VENDEDOR                NUMERIC(15)    
DECLARE @CARTOES                 VARCHAR(1) = 'T' 
DECLARE @MOSTRAR_PAGOS           VARCHAR(1) = 'N' 
DECLARE @COBRANCA_LOJA           VARCHAR(1) = 'N' 
DECLARE @COBRANCA_DISTRIBUIDORA  VARCHAR(1) = 'N' 
DECLARE @MOSTRAR_FUNCIONARIOS    VARCHAR(1) = 'N' 
DECLARE @GRUPO_CLIENTE           NUMERIC(15,0) 
DECLARE @ORIGEM                  NUMERIC(15,0)


DECLARE @EMAIL VARCHAR(50)
DECLARE @TITULO_EMAIL VARCHAR(50)
DECLARE @TABELA VARCHAR(MAX)
DECLARE @ENTIDADE NUMERIC(15)
 
 
--DECLARE @ENTIDADE_CONCENTRADORA NUMERIC(15) = CASE WHEN ISNUMERIC(:ENTIDADE_CONCENTRADORA) =  0 THEN NULL         ELSE :ENTIDADE_CONCENTRADORA END
                           
SET  @VENCIMENTO_INI   = CAST(GETDATE() AS DATE)
SET  @VENCIMENTO_FIM   = CAST(GETDATE() AS DATE)
--SET  @VENCIMENTO_INI   = '01/09/2018'
--SET  @VENCIMENTO_FIM   = '01/09/2018'
SET  @EMPRESA_INI      = 1
SET  @EMPRESA_FIM      = 1000
SET  @ENTIDADE_INI     = 1
SET  @ENTIDADE_FIM     = 99999
SET  @EMPRESA_CONTABIL = NULL
SET  @MODALIDADE       = 2
SET  @MOSTRAR_PAGOS    = 'N'
--SET  @CARTOES          = NULL
SET  @GRUPO_CLIENTE    = 12
DECLARE @ENTIDADE_CONCENTRADORA NUMERIC(15) = NULL


if object_id('tempdb..#DADOSEMAIL') is not null    
   DROP TABLE #DADOSEMAIL	 
	 
if object_id('tempdb..#EMAILS') is not null    
   DROP TABLE #EMAILS

CREATE TABLE #DADOSEMAIL 
    ([EMAIL] VARCHAR(100) null,
     [TITULO] VARCHAR(50) null,
     [EMISSAO] DATE null,
	 [VENCIMENTO] DATE NULL,
	 [VALOR] NUMERIC(15) NULL, 
	 [ENTIDADE] NUMERIC(15),
	 [NOME] VARCHAR(50))

	 INSERT INTO #DADOSEMAIL 
	 
	 SELECT   
			(SELECT TOP 1 EMAIL FROM EMAIL A WHERE B.ENTIDADE = ENTIDADE AND PREFERENCIAL = 'S') AS EMAIL,
			A.TITULO,
			A.EMISSAO,
			A.VENCIMENTO,
			A.VALOR,
			B.ENTIDADE,
			B.NOME

     FROM TITULOS_RECEBER             A (NOLOCK) 
LEFT JOIN ENTIDADES                   B (NOLOCK) ON A.ENTIDADE                = B.ENTIDADE
LEFT JOIN TITULOS_RECEBER_SALDO       C (NOLOCK) ON A.TITULO_RECEBER          = C.TITULO_RECEBER
     JOIN FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                      D             ON D.EMPRESA_USUARIA      = A.EMPRESA
LEFT JOIN MODALIDADES_TITULOS         E WITH(NOLOCK)ON E.MODALIDADE           = A.MODALIDADE
LEFT JOIN (
            SELECT DISTINCT ENTIDADE FROM BANDEIRAS_CARTOES WITH(NOLOCK)
          )                           G              ON G.ENTIDADE            = A.ENTIDADE 
LEFT JOIN NF_FATURAMENTO              F WITH(NOLOCK) ON A.TAB_MASTER_ORIGEM   = 753289
                                                    AND F.NF_FATURAMENTO      = A.REG_MASTER_ORIGEM
LEFT JOIN VENDEDORES                  H WITH(NOLOCK) ON H.VENDEDOR            = F.VENDEDOR 
LEFT JOIN FORMULARIOS                 I WITH(NOLOCK) ON I.NUMID               = A.FORMULARIO_ORIGEM   --formulario origem 
LEFT JOIN ENDERECOS                   J WITH(NOLOCK) ON J.ENTIDADE            = A.ENTIDADE
OUTER APPLY (
             SELECT TOP 1 ENTIDADE, DDD AS DDD, NUMERO AS NUMERO , CONTATO
             FROM TELEFONES XX WITH(NOLOCK)
             WHERE TIPO_TELEFONE       = 6
               AND XX.ENTIDADE = A.ENTIDADE
          )                           K
OUTER APPLY (
             SELECT TOP 1 ENTIDADE, DDD AS DDD, NUMERO AS NUMERO , CONTATO
             FROM TELEFONES K2 WITH(NOLOCK)
             WHERE K2.ENTIDADE = A.ENTIDADE
          )                           K1
                                                    
LEFT JOIN NF_FATURAMENTO_OBSERVACOES  L WITH(NOLOCK) ON L.NF_FATURAMENTO      = F.NF_FATURAMENTO
LEFT JOIN LIQUIDACAO_NOTAS_DETALHE    M WITH(NOLOCK) ON M.NF_FATURAMENTO      = F.NF_FATURAMENTO
LEFT JOIN FUNCIONARIOS                N WITH(NOLOCK) ON N.ENTIDADE            = A.ENTIDADE
LEFT JOIN (
            SELECT 
                  A.TITULO_RECEBER   AS TITULO_RECEBER,
                  MAX(A.DATA)        AS DATA_RECEBIMENTO
            
              FROM TITULOS_RECEBER_TRANSACOES A WITH(NOLOCK)
              JOIN TITULOS_RECEBER_SALDO      B WITH(NOLOCK) ON B.TITULO_RECEBER = A.TITULO_RECEBER
            
                 WHERE 1=1
                   AND A.DEBITO > 0
                   AND B.SITUACAO_TITULO = 2
            GROUP BY A.TITULO_RECEBER
          )                           O                      ON O. TITULO_RECEBER = A.TITULO_RECEBER

    WHERE 1=1                                                                                                                                                  AND                                                                   
            ISNULL ( C.SITUACAO_TITULO , 0 ) < (CASE WHEN @MOSTRAR_PAGOS = 'S' THEN 3 ELSE 2 END)                                                              AND
           (A.VENCIMENTO            >= @VENCIMENTO_INI                                                                                                       ) AND
           (A.VENCIMENTO            <= @VENCIMENTO_FIM                                                                                                       ) AND
            A.EMPRESA               >= @EMPRESA_INI                                                                                                            AND
            A.EMPRESA               <= @EMPRESA_FIM                                                                                                            AND
            A.ENTIDADE              >= @ENTIDADE_INI                                                                                                           AND
            A.ENTIDADE              <= @ENTIDADE_FIM                                                                                                           AND
           (A.MODALIDADE             = @MODALIDADE     OR @MODALIDADE IS NULL)                                                                                 AND
           (B.CLASSIFICACAO_CLIENTE  = @GRUPO_CLIENTE  OR @GRUPO_CLIENTE IS NULL)                                                                              AND
           (F.VENDEDOR               = @VENDEDOR       OR @VENDEDOR   IS NULL)                                                                                 AND
           (A.FORMULARIO_ORIGEM      = @ORIGEM         OR @ORIGEM IS NULL)                                                                                     AND
           ( CASE WHEN G.ENTIDADE          IS NULL THEN 'N' ELSE 'S' END = @CARTOES               OR @CARTOES               = 'T'   )                          AND
           ( CASE WHEN N.ENTIDADE          IS NULL THEN 'N' ELSE 'S' END = @MOSTRAR_FUNCIONARIOS  OR @MOSTRAR_FUNCIONARIOS  = 'S'   )                          AND
           --ACHAR FORMA DE PEGAR CLASSIFICACAO_CLIENT DINAMICAMENTE
           ( CASE WHEN B.CLASSIFICACAO_CLIENTE = 3 THEN 'S' ELSE 'N' END = @MOSTRAR_FUNCIONARIOS  OR @MOSTRAR_FUNCIONARIOS  = 'S'   )                          AND
           (B.CONCENTRADOR = @ENTIDADE_CONCENTRADORA       OR           @ENTIDADE_CONCENTRADORA IS NULL         )                                              AND                                                                                                                   
           ( CASE WHEN @COBRANCA_LOJA            = 'S' THEN ISNULL(I.NUMID,0) ELSE NULL END NOT IN  ( 176897, 61, 70 ) OR @COBRANCA_LOJA          <> 'S'   )   AND         
           ( CASE WHEN @COBRANCA_DISTRIBUIDORA   = 'S' THEN ISNULL(I.NUMID,0) ELSE NULL END NOT IN  ( 176897, 72, 70 ) OR @COBRANCA_DISTRIBUIDORA <> 'S'   )          
             --AND A.ENTIDADE IN(55940,56596)

		
 ORDER BY D.EMPRESA_CONTABIL, A.EMPRESA, B.NOME, CAST(B.ENTIDADE AS VARCHAR), A.VENCIMENTO

SELECT   
    EMAIL,ENTIDADE
    , ROW_NUMBER() OVER(ORDER BY [EMAIL]) AS [CONTADOR] 
INTO    #EMAILS
FROM    #DADOSEMAIL
GROUP BY EMAIL,ENTIDADE

DECLARE @LINHA INT = 1

WHILE  @LINHA <= (SELECT COUNT(*) FROM #EMAILS)
BEGIN
    SET @EMAIL = (SELECT [EMAIL] FROM #EMAILS WHERE [CONTADOR] = @LINHA)
	SET @ENTIDADE = (SELECT [ENTIDADE] FROM #EMAILS WHERE [CONTADOR]=@LINHA)

	SET @TABELA =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 16px;  
              }  
              .alinhameto {  
               text-align: center;  
              }  
              </style>'                    
			  
			  +  
    N'<H1 class="formato">AVISO DE TITULOS EM ATRASO <br>
  </H1>' +  
    N'<table border="10">' +  
    N'<tr>   
           <th>Documento</th>  
           <th>Data Emiss�o</th>  
           <th>Data Vencimento</th>  
           <th>Valor</th>
		   <th>Nome</th>    
      </tr>'                             +  
  
    CAST ( (   


  
 SELECT   
    td = TITULO                                       ,  '',  
    td = CONVERT(VARCHAR,VENCIMENTO,103)                    ,  '',  
    td = CONVERT(VARCHAR,VENCIMENTO,103)                                      ,  '',  
    td = 'R$ '+CAST(VALOR AS VARCHAR(15))             ,  '',
	td = CAST(NOME AS VARCHAR(50)) 
	                    
                                           
  

  FROM #DADOSEMAIL
  WHERE   [EMAIL] = @EMAIL AND [ENTIDADE] = @ENTIDADE

  
 
  
              FOR XML PATH('tr'), TYPE   
    ) AS NVARCHAR(MAX) ) +  

    N'</table>'  
	
	+   N'<P class="formato" >Comunicamos que o(s) titulos abaixo encontram-se em aberto favor regularizar<br> 
	                          o pagamento em at� 5 (cinco) dias da data do vencimento evitando assim <br> 
							  despesas adicionais com titulos protestados(s)<br><br></P> 
							  
							  Caso este(s)  titulos estejam quitados, favor desconsiderar este e-mail.
							  
							  Qualquer d�vida entre em contato<br>
							  Fixo: (85) 3452-3100<br>
							  Whatsapp:(85) 9 9412-0273<br>
							  Email: ketiny.andrade@prohospital.com.br. 
							  ';
  


SET @TITULO_EMAIL = 'AVISO DE TITULOS EM ATRASO'

    EXEC msdb.DBO.SP_SEND_DBMAIL
        @recipients = @EMAIL 
        , @subject = @TITULO_EMAIL
        , @body_format = 'html'
        , @body = @TABELA 

    SET @LINHA = @LINHA +1

END

END