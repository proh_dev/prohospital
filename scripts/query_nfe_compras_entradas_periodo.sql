/*==============================================================*/
/*    Query Base a que contem os dados mais corretos            */
/*==============================================================*/


--DECLARE @DATA_INI    DATE    = :DATA_INI    
--DECLARE @DATA_FIM    DATE    = :DATA_FIM          
--DECLARE @EMPRESA_INI NUMERIC    
--DECLARE @EMPRESA_FIM NUMERIC    
--IF ISNUMERIC(:EMPRESA_INI) = 1 SET @EMPRESA_INI = :EMPRESA_INI ELSE SET @EMPRESA_INI = NULL
--IF ISNUMERIC(:EMPRESA_FIM) = 1 SET @EMPRESA_FIM = :EMPRESA_FIM ELSE SET @EMPRESA_FIM = NULL
               
      
                          
DECLARE @EMPRESA_INI NUMERIC    
DECLARE @EMPRESA_FIM NUMERIC          
DECLARE @DATA_INI DATE = '01/01/2018'
DECLARE @DATA_FIM DATE = '31/01/2018'


           SELECT 
                    B.FORMULARIO_ORIGEM,
                    B.TAB_MASTER_ORIGEM,
                    B.REG_MASTER_ORIGEM,
                    B.NF_COMPRA                                AS NF_COMPRA ,
                    B.EMPRESA                                  AS EMPRESA    ,
                    B.ENTIDADE                                 AS ENTIDADE   , 
                    C.ENTIDADE                                 AS ENTIDADE_FORNECEDOR  ,  
                    C.NOME_FANTASIA                            AS NOME  ,       
                    B.NF_NUMERO                                AS NUMERO     ,
                    B.EMISSAO                                  AS EMISSAO ,
                    B.MOVIMENTO                                AS MOVIMENTO  ,
                    B.PROCESSAR                                AS PROCESSAR,   
                    ISNULL(D.TOTAL_BRUTO,0)                    AS TOTAL_BRUTO ,
                    ISNULL(D.TOTAL_DESCONTO,0)                 AS TOTAL_DESCONTO ,
                    ISNULL(D.TOTAL_BRUTO - D.TOTAL_DESCONTO,0) AS TOTAL_LIQUIDO,
                    D.TOTAL_SERVICOS                           AS TOTAL_SERVICO,
                    D.TOTAL_IPI                                AS TOTAL_IPI,
                    B.TOTAL_GERAL                              AS TOTAL_GERAL    
            
              FROM NF_COMPRA          B WITH(NOLOCK)
              JOIN ENTIDADES          C WITH(NOLOCK) ON C.ENTIDADE   = B.ENTIDADE
              JOIN NF_COMPRA_TOTAIS   D WITH(NOLOCK) ON D.NF_COMPRA  = B.NF_COMPRA
              
              WHERE 1=1
              AND ( B.MOVIMENTO   >= @DATA_INI OR @DATA_INI IS NULL )
              AND ( B.MOVIMENTO   <= @DATA_FIM OR @DATA_FIM IS NULL )

              ORDER BY B.EMPRESA
