DECLARE @EMPRESA NUMERIC(15,0)
DECLARE @DATA_INI DATETIME
DECLARE @DATA_FIM DATETIME
DECLARE @DESCONSIDERAR_NOTA VARCHAR(1)
             
--SET @EMPRESA  = 1000 
--SET @DATA_INI = '01/08/2018'
--SET @DATA_FIM = '31/08/2018'
--SET @DESCONSIDERAR_NOTA = 'N'

SET @EMPRESA  = :EMPRESA
SET @DATA_INI = :DATA_INI
SET @DATA_FIM = :DATA_FIM
SET @DESCONSIDERAR_NOTA  = :DESCONSIDERAR_NOTA

SELECT DISTINCT 
G.NUMID,
CAST ( G.DESCRICAO                          AS VARCHAR ) AS PROCESSO_PROCFIT,
CAST ( DBO.NUMERICO(C.INSCRICAO_FEDERAL)    AS VARCHAR ) AS emp_cnpj,       
CAST ( DBO.ZEROS(E.EMPRESA_CONTABIL,3)      AS VARCHAR ) AS emp_centro_resultado,
CAST ( CONVERT(VARCHAR,A.MOVIMENTO,103)     AS VARCHAR ) AS doc_data_entrada,
CAST ( CONVERT(VARCHAR,A.EMISSAO  ,103)     AS VARCHAR ) AS doc_data_emissao,
CAST ( ISNULL(H.NF_ESPECIE,'')              AS VARCHAR ) AS doc_especie,
CAST ( ISNULL (H.NF_NUMERO, A.TITULO_PAGAR) AS VARCHAR ) AS doc_numero,
A.VALOR                                                  AS doc_valor,
CAST ( DBO.NUMERICO(F.INSCRICAO_FEDERAL)    AS VARCHAR ) AS emitente_cnpj,
CAST ( F.NOME                               AS VARCHAR ) AS emitente_nome,
CAST ( CASE WHEN G.DESCRICAO LIKE '%CAIXA%' THEN 1 ELSE 2 END AS VARCHAR )
                                                         AS agente_tipo,
CAST ( K.BANCO                              AS VARCHAR ) AS agente_numero_banco,
CAST ( K.AGENCIA + '-' + K.DIGITO_AGENCIA   AS VARCHAR ) AS agente_agencia,
CAST ( K.CONTA_CORRENTE + '-' + K.DIGITO_CONTA AS VARCHAR ) 
                                                         AS agente_conta,
CAST ( A.TITULO                             AS VARCHAR ) AS titulo_numero,
CAST ( 1                                    AS VARCHAR ) AS titulo_parcela,
CAST ( 1                                    AS VARCHAR ) AS titulo_quantidade_parcelas,
CAST ( CONVERT(VARCHAR,A.VENCIMENTO  ,103)  AS VARCHAR ) AS titulo_data_vencimento,
CAST ( CONVERT(VARCHAR,N.DATA_PAGTO  ,103)  AS VARCHAR ) AS titulo_data_pagamento,
A.VALOR                                                  AS titulo_valor_principal,
CAST ( A.DESCONTO_VALOR                     AS VARCHAR ) AS titulo_valor_desconto,
CAST ( A.MULTA_ATRASO+A.JUROS_ATRASO        AS VARCHAR ) AS titulo_valor_multa_juros,
CAST ( A.TARIFA_BANCARIA                    AS VARCHAR ) AS titulo_valor_taxa,
A.VALOR                                                  AS titulo_valor_total,
B1.ESCRITURAVEL                                          AS escrituravel,
B1.OBJETO_CONTROLE                                       AS objeto_controle,
O.DESCRICAO                                              AS objeto_controle_desc,
B1.CLASSIF_FINANCEIRA                                    AS classificacao_financeira,
P.DESCRICAO                                              AS classificacao_financeira_desc

  FROM TITULOS_PAGAR                   A WITH(NOLOCK)
  JOIN TITULOS_PAGAR_SALDO             B WITH(NOLOCK) ON B.TITULO_PAGAR         = A.TITULO_PAGAR
  JOIN TITULOS_PAGAR_CLASSIFICACOES   B1 WITH(NOLOCK) ON B1.TITULO_PAGAR        = A.TITULO_PAGAR
  JOIN ENTIDADES                       C WITH(NOLOCK) ON C.ENTIDADE             = A.EMPRESA
  JOIN EMPRESAS_USUARIAS               E WITH(NOLOCK) ON E.EMPRESA_USUARIA      = C.ENTIDADE
  JOIN ENTIDADES                       F WITH(NOLOCK) ON F.ENTIDADE             = A.ENTIDADE
  LEFT                                                                             
  JOIN FORMULARIOS                     G WITH(NOLOCK) ON G.NUMID                = A.FORMULARIO_ORIGEM
  LEFT                                                                                
  JOIN NF_COMPRA                       H WITH(NOLOCK) ON H.NF_COMPRA            = A.REG_MASTER_ORIGEM
                                                     AND A.FORMULARIO_ORIGEM    = 48
  LEFT
  JOIN PAGAMENTOS_ESCRITURAIS_TITULOS  I WITH(NOLOCK) ON I.TITULO_PAGAR         = A.TITULO_PAGAR
  LEFT
  JOIN PAGAMENTOS_ESCRITURAIS          J WITH(NOLOCK) ON J.PAGAMENTO_ESCRITURAL = I.PAGAMENTO_ESCRITURAL
  LEFT
  JOIN CONTAS_BANCARIAS                K WITH(NOLOCK) ON K.CONTA_BANCARIA       = J.CONTA_BANCARIA
  LEFT
  JOIN CONTAS_PAGAR                    L WITH(NOLOCK) ON L.CONTAS_PAGAR         = A.REG_MASTER_ORIGEM
                                                     AND A.FORMULARIO_ORIGEM    = 8
  LEFT
  JOIN CONTAS_PAGAR_PREVISOES_DETALHES M WITH(NOLOCK) ON M.CHAVE                = L.CHAVE
                                                     AND M.EMPRESA              = L.EMPRESA
  JOIN (
         SELECT A.TITULO_PAGAR, MAX(DATA) AS DATA_PAGTO
           FROM TITULOS_PAGAR_TRANSACOES A WITH(NOLOCK)
           JOIN TITULOS_PAGAR_SALDO      B WITH(NOLOCK) ON B.TITULO_PAGAR = A.TITULO_PAGAR
          WHERE B.SITUACAO_TITULO > 1 -- SOMENTE PAGOS
            AND A.DEBITO          > 0 -- SOMENTE TRANSAÇÕES DE PAGAMENTO
       GROUP BY A.TITULO_PAGAR
       )                               N              ON N.TITULO_PAGAR         = A.TITULO_PAGAR
  LEFT
  JOIN CENTROS_CUSTO                   O              ON O.OBJETO_CONTROLE      = B1.OBJETO_CONTROLE
  LEFT
  JOIN CLASSIF_FINANCEIRAS             P              ON P.CLASSIF_FINANCEIRA   = B1.CLASSIF_FINANCEIRA

  WHERE 1=1
  AND B.SITUACAO_TITULO > 1      
  AND A.EMPRESA    = @EMPRESA
  AND N.DATA_PAGTO >= @DATA_INI
  AND N.DATA_PAGTO <= @DATA_FIM
  AND(@DESCONSIDERAR_NOTA = 'S' AND G.NUMID <> 177982 OR @DESCONSIDERAR_NOTA = 'N')
