--CREATE PROCEDURE [dbo].[ENVIO_EMAIL_SOL_CANC_NOTAS] ( @SOLICITACAO_CANCELAMENTO_NF NUMERIC(15,0) ) AS                      
--BEGIN                      


                
SET NOCOUNT ON      
  
DECLARE @SOLICITACAO_CANCELAMENTO_NF        NUMERIC(15,0)  = 1


DECLARE @TITULO_EMAIL    VARCHAR(50)  
DECLARE @tableHTML       NVARCHAR(MAX)   
DECLARE @EMPRESA_ORIGEM  VARCHAR(50)  
DECLARE @EMPRESA_DESTINO VARCHAR(50)  
DECLARE @EMAIL           VARCHAR(250)  = 'pedro.mota@prohospital.com.br'
  

  
SET @TITULO_EMAIL = 'Novo Romaneio para separa��o! N�: ' + CAST ( @ROMANEIO AS VARCHAR (50) )  
  
SELECT @EMPRESA_ORIGEM  = CAST (B.CENTRO_ESTOQUE_ORIGEM AS VARCHAR (50))  + ' - ' + D.NOME_FANTASIA,
       @EMPRESA_DESTINO = CAST (B.CENTRO_ESTOQUE_DESTINO AS VARCHAR (50)) + ' - ' + E.NOME_FANTASIA,
       @EMAIL           = G.EMAIL
  
FROM ESTOQUE_TRANSFERENCIAS_PRODUTOS A WITH(NOLOCK)  
   JOIN ESTOQUE_TRANSFERENCIAS          B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA  
   JOIN PRODUTOS                        C WITH(NOLOCK) ON C.PRODUTO               = A.PRODUTO  
   JOIN CENTROS_ESTOQUE                D1 WITH(NOLOCK) ON D1.OBJETO_CONTROLE      = B.CENTRO_ESTOQUE_ORIGEM  
   JOIN EMPRESAS_USUARIAS               D WITH(NOLOCK) ON D.EMPRESA_USUARIA       = D1.EMPRESA
   JOIN CENTROS_ESTOQUE                E1 WITH(NOLOCK) ON E1.OBJETO_CONTROLE      = B.CENTRO_ESTOQUE_DESTINO
   JOIN EMPRESAS_USUARIAS               E WITH(NOLOCK) ON E.EMPRESA_USUARIA       = E1.EMPRESA
   LEFT  
   JOIN MARCAS                          F WITH(NOLOCK) ON F.MARCA                 = C.MARCA  
   JOIN PARAMETROS_ESTOQUE              G WITH(NOLOCK) ON G.EMPRESA_USUARIA       = D.EMPRESA_USUARIA  
  WHERE A.ESTOQUE_TRANSFERENCIA = @ROMANEIO  
  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;  
              }  
              </style>'                    +  
    N'<H1 class="formato" >Novo Romaneio de Separa��o <br>  
 Romaneio n� '  + CAST(@ROMANEIO AS VARCHAR) + '   <br>  
 Loja Origem: ' + @EMPRESA_ORIGEM            + '   <br>  
 Loja Destino: '+ @EMPRESA_DESTINO           + '   <br>  
  
  </H1>' +  
    N'<table border="1" class="formato">' +  
    N'<tr>   
           <th>PRODUTO</th>  
           <th>MARCA</th>  
           <th>QUANTIDADE</th>  
           <th>UNIDADE</th>  
           <th>EAN</th>  
           <th>REFERENCIA</th>  
      </tr>'                             +  
  
    CAST ( (   
  
 SELECT   
             td = C.DESCRICAO                                                          ,  '',  
    td = CAST (C.MARCA AS VARCHAR) + ' - ' + F.DESCRICAO                      ,  '',  
    td = A.QUANTIDADE_UNITARIA                                                ,  '',  
    td = C.UNIDADE_MEDIDA                                                     ,  '',  
    td = ISNULL(G.EAN,0)                                                      ,  '',  
    td = ISNULL(C.CODIGO_REFERENCIA,0)                                          
  
   FROM ESTOQUE_TRANSFERENCIAS_PRODUTOS A WITH(NOLOCK)  
   JOIN ESTOQUE_TRANSFERENCIAS          B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA  
   JOIN PRODUTOS                        C WITH(NOLOCK) ON C.PRODUTO               = A.PRODUTO  
   JOIN CENTROS_ESTOQUE                D1 WITH(NOLOCK) ON D1.OBJETO_CONTROLE      = B.CENTRO_ESTOQUE_ORIGEM  
   JOIN EMPRESAS_USUARIAS               D WITH(NOLOCK) ON D.EMPRESA_USUARIA       = D1.EMPRESA
   JOIN CENTROS_ESTOQUE                E1 WITH(NOLOCK) ON E1.OBJETO_CONTROLE      = B.CENTRO_ESTOQUE_DESTINO
   JOIN EMPRESAS_USUARIAS               E WITH(NOLOCK) ON E.EMPRESA_USUARIA       = E1.EMPRESA
   LEFT  
   JOIN MARCAS                          F WITH(NOLOCK) ON F.MARCA                 = C.MARCA  
   LEFT  
   JOIN (SELECT PRODUTO, MAX(EAN) EAN  
              FROM PRODUTOS_EAN   
          GROUP BY PRODUTO  
           )                               G              ON G.PRODUTO               = C.PRODUTO  
  WHERE A.ESTOQUE_TRANSFERENCIA = @ROMANEIO  
  
              FOR XML PATH('tr'), TYPE   
    ) AS NVARCHAR(MAX) ) +  
    N'</table>' ;  
  
  
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML';  
  
  
  
                     
--END 