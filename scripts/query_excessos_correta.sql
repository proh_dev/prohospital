        
--alter PROCEDURE [dbo].[PROCESSAMENTO_REMANEJAMENTO_LOJAS_EXCESSOS] ( @REMANEJAMENTO_ESTOQUE_LOJA NUMERIC ) AS          
        
--BEGIN          
        
BEGIN TRANSACTION        
--ROLLBACK        
--COMMIT        
        
--================================================================================--        
--PARTE 1 - IDENTIFICACAO DA NECESSIDADE DE TRANSFERENCIA DE CADA LOJA E OS EXCESSOS        
--================================================================================--        
        
DECLARE @MOVIMENTO               DATETIME        
        
DECLARE @DIAS_CURVA_A            NUMERIC        
DECLARE @DIAS_CURVA_B            NUMERIC        
DECLARE @DIAS_CURVA_C            NUMERIC        
DECLARE @DIAS_CURVA_D            NUMERIC        
DECLARE @DIAS_CURVA_E            NUMERIC        
        
        
DECLARE @APENAS_CONTROLADOS      VARCHAR (1)        
DECLARE @MENOS_CONTROLADOS       VARCHAR(1)        
        
DECLARE @MES                     NUMERIC (02)        
DECLARE @ANO                     NUMERIC (04)        
        
DECLARE @PERCENTUAL_EXCESSO      NUMERIC(6,2)        
DECLARE @MINIMO_ORIGEM           VARCHAR(1)        
DECLARE @MINIMO_DESTINO          VARCHAR(1)        
        
DECLARE @TIPO_REMANEJAMENTO_LOJA NUMERIC         
        
DECLARE @DIAS_CONSIDERAR_TRANSITO    NUMERIC         
        
DECLARE @REMANEJAMENTO_ESTOQUE_LOJA  NUMERIC            
    SET @REMANEJAMENTO_ESTOQUE_LOJA =  3291
        
        
   --DEFINICAO DE PARAMETROS--        
   SELECT @DIAS_CURVA_A           = ISNULL(A.DIAS_CURVA_A,0)          ,                
          @DIAS_CURVA_B           = ISNULL(A.DIAS_CURVA_B,0)          ,                
          @DIAS_CURVA_C           = ISNULL(A.DIAS_CURVA_C,0)          ,                
          @DIAS_CURVA_D           = ISNULL(A.DIAS_CURVA_D,0)          ,                
          @DIAS_CURVA_E           = ISNULL(A.DIAS_CURVA_E,0)          ,                
                          
          @APENAS_CONTROLADOS     = ISNULL(A.APENAS_CONTROLADOS,'N')  ,                
          @MENOS_CONTROLADOS      = ISNULL(A.MENOS_CONTROLADOS ,'N')  ,                
          @PERCENTUAL_EXCESSO     = A.PERCENTUAL_EXCESSO              ,        
          @MINIMO_ORIGEM          = A.MINIMO_ORIGEM,        
          @MINIMO_DESTINO         = A.MINIMO_DESTINO,        
          @MOVIMENTO              = A.MOVIMENTO  ,        
          @TIPO_REMANEJAMENTO_LOJA = A.TIPO_REMANEJAMENTO_LOJA ,  
    @DIAS_CONSIDERAR_TRANSITO = A.DIAS_CONSIDERAR_TRANSITO       
                          
     FROM REMANEJAMENTOS_ESTOQUES_LOJAS A WITH(NOLOCK)                
    WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA           
            
        
        
        
--DEFINE MES E ANO--        
        
SELECT @ANO = A.ANO, @MES = A.MES        
  FROM VALOR_BASE A WITH(NOLOCK)        
 ORDER BY A.ANO DESC, A.MES DESC        
        
        
IF @ANO IS NULL SET @ANO = YEAR  ( @MOVIMENTO )        
IF @MES IS NULL SET @MES = MONTH ( @MOVIMENTO )        
        
        
---------------------------------------------------------        
-- GERA TABELA TEMPOR�RIA DE PRODUTOS CONFORME FILTROS --        
---------------------------------------------------------        
        
IF OBJECT_ID('TEMPDB..#PRODUTOS') IS NOT NULL DROP TABLE #PRODUTOS        
        
        
  SELECT A.PRODUTO        
          
    INTO #PRODUTOS        
          
    FROM REMANEJAMENTOS_ESTOQUES_LOJAS_PRODUTOS_FILTROS A WITH(NOLOCK)        
   WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA        
        
        
        
-------------------------------------        
--DEFINICAO DOS CENTROS DE ESTOQUES--        
-------------------------------------        
        
IF OBJECT_ID('TEMPDB..#EMPRESAS_CENTROS_ESTOQUE') IS NOT NULL DROP TABLE #EMPRESAS_CENTROS_ESTOQUE        
        
SELECT DISTINCT         
       A.EMPRESA_USUARIA AS EMPRESA,        
       C.OBJETO_CONTROLE AS CENTRO_ESTOQUE        
          
  INTO #EMPRESAS_CENTROS_ESTOQUE        
    
  FROM EMPRESAS_USUARIAS                      A WITH(NOLOCK)        
  JOIN REMANEJAMENTOS_ESTOQUES_LOJAS_ORIGENS  B WITH(NOLOCK) ON B.EMPRESA         = A.EMPRESA_USUARIA        
  JOIN EMPRESAS_ESTOQUES                      C WITH(NOLOCK) ON C.EMPRESA_USUARIA = A.EMPRESA_USUARIA        
                                                            AND C.TIPO_ESTOQUE    = 2         
 WHERE B.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA        
        
           
------------------------------------------------------------        
--DEFINICAO DO SALDO DO PRODUTO EM POR PRODUTO POR LOJA        
--VINCULADA AO DEPOSITO SENDO FATURADO        
------------------------------------------------------------        
        
IF OBJECT_ID('TEMPDB..#ESTOQUE_LOJA') IS NOT NULL DROP TABLE #ESTOQUE_LOJA        
        
  SELECT D.EMPRESA                          AS EMPRESA,        
         A.PRODUTO                          AS PRODUTO,        
                 
         CASE WHEN @MINIMO_ORIGEM = 'S' AND ( CONVERT(DATE,ISNULL(F.VALIDADE_MINIMO, GETDATE())) >= CONVERT(DATE, GETDATE() ) )        
              THEN ISNULL(F.ESTOQUE_MINIMO, 0)        
              ELSE 0        
         END                                AS ESTOQUE_MINIMO,        
        
         ISNULL(F.ESTOQUE_MAXIMO,0)         AS ESTOQUE_MAXIMO,        
         ISNULL(F.ESTOQUE_SEGURANCA,0)      AS ESTOQUE_SEGURANCA,                                   
                 
         CASE WHEN ISNULL(E.ESTOQUE_SALDO, 0) < 0         
              THEN 0         
              ELSE ISNULL(E.ESTOQUE_SALDO, 0)         
         END                               AS ESTOQUE_SALDO,        
                     
         ISNULL(F.SITUACAO_PRODUTO_COMPRA, 1)  AS SITUACAO_PRODUTO_COMPRA,        
         ISNULL(F.SITUACAO_PRODUTO,1)          AS SITUACAO_PRODUTO        
        
    INTO #ESTOQUE_LOJA        
        
    FROM PRODUTOS                       A WITH(NOLOCK)        
    JOIN #PRODUTOS                      B WITH(NOLOCK)ON B.PRODUTO         = A.PRODUTO         
    JOIN SECOES_PRODUTOS                C WITH(NOLOCK)ON C.SECAO_PRODUTO   = A.SECAO_PRODUTO        
                                                     AND C.COMERCIALIZAVEL = 'S'        
                                                                      
   JOIN #EMPRESAS_CENTROS_ESTOQUE       D WITH(NOLOCK) ON 1 = 1        
                 
LEFT JOIN ESTOQUE_ATUAL                 E WITH(NOLOCK) ON E.CENTRO_ESTOQUE = D.CENTRO_ESTOQUE        
                                                      AND E.PRODUTO        = A.PRODUTO        
                 
LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS  F WITH(NOLOCK) ON F.PRODUTO         = A.PRODUTO        
                                                      AND F.EMPRESA         = D.EMPRESA        
        
ORDER BY PRODUTO, EMPRESA        
        
        
--CRIA��O DE INDICES--        
        
CREATE INDEX ESTOQUE_EMPRESA_01 ON #ESTOQUE_LOJA(PRODUTO)        
        
CREATE INDEX ESTOQUE_EMPRESA_02 ON #ESTOQUE_LOJA(EMPRESA)        
        
CREATE INDEX ESTOQUE_EMPRESA_03 ON #ESTOQUE_LOJA(PRODUTO, EMPRESA)        
         
         
--------------------------------------------------------                
--DEFINICAO DO TRANSFERENCIAS PENDENTES NA ORIGEM                
--------------------------------------------------------                
                
if object_id('tempdb..#ROMANEIOS_PENDENTES_SAIDA') is not null                  
   DROP TABLE #ROMANEIOS_PENDENTES_SAIDA        
                
 SELECT B.PRODUTO,               
        A.ESTOQUE_TRANSFERENCIA,                 
        A.CENTRO_ESTOQUE_ORIGEM AS EMPRESA,        
       (CASE WHEN SUM(B.CONFERENCIA) > 0                 
             THEN SUM(B.CONFERENCIA)                
             ELSE SUM(B.QTDE_ORIGINAL - B.ACERTO_FALTA)                
        END) AS TRANSF_PENDENTE_SAIDA            
                    
  INTO #ROMANEIOS_PENDENTES_SAIDA                
                                                          
  FROM ESTOQUE_TRANSFERENCIAS                    A WITH(NOLOCK)                      
       JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES    B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA        
       JOIN #PRODUTOS                            E WITH(NOLOCK) ON E.PRODUTO               = B.PRODUTO        
       JOIN #EMPRESAS_CENTROS_ESTOQUE            F WITH(NOLOCK) ON F.EMPRESA               = A.CENTRO_ESTOQUE_ORIGEM           
       JOIN CENTROS_ESTOQUE                      G WITH(NOLOCK) ON G.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_ORIGEM        
       JOIN CENTROS_ESTOQUE                      H WITH(NOLOCK) ON H.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_DESTINO        
                
   LEFT JOIN ( SELECT DISTINCT A.ESTOQUE_TRANSFERENCIA                
                 FROM NF_FATURAMENTO_TRANSF               A WITH(NOLOCK)                 
                 JOIN NF_FATURAMENTO                      B WITH(NOLOCK) ON B.NF_FATURAMENTO          = A.NF_FATURAMENTO                
                 JOIN ESTOQUE_TRANSFERENCIAS              C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA   = A.ESTOQUE_TRANSFERENCIA        
            LEFT JOIN CANCELAMENTOS_NOTAS_FISCAIS         D WITH(NOLOCK) ON D.CHAVE                   = A.NF_FATURAMENTO        
                                                                        AND D.TIPO                    = 1               
                         
                 JOIN #EMPRESAS_CENTROS_ESTOQUE           E WITH(NOLOCK) ON E.EMPRESA                 = B.EMPRESA        
                WHERE D.NF_CANCELAMENTO IS NULL            
                  AND C.MOVIMENTO >= CONVERT( DATE, GETDATE() - @DIAS_CONSIDERAR_TRANSITO ) ) X ON X.ESTOQUE_TRANSFERENCIA =  A.ESTOQUE_TRANSFERENCIA                
                         
 WHERE 1 = 1                                        
   AND X.ESTOQUE_TRANSFERENCIA IS NULL                
   AND A.MOVIMENTO >= CONVERT( DATE, GETDATE() - @DIAS_CONSIDERAR_TRANSITO )            
   AND G.EMPRESA <> H.EMPRESA --PEGAR APENAS ROMANEIOS ENTRE LOJAS--        
                      
  GROUP BY B.PRODUTO, A.ESTOQUE_TRANSFERENCIA, A.CENTRO_ESTOQUE_ORIGEM        
                  
  HAVING ( (CASE WHEN SUM(B.CONFERENCIA) > 0                 
                 THEN SUM(B.CONFERENCIA)                
                 ELSE SUM(B.QTDE_ORIGINAL - B.ACERTO_FALTA)                
             END) ) > 0          
            
            
--CONSOLIDA��O POR PRODUTO--            
            
if object_id('tempdb..#PENDENCIAS_TRANSF_SAIDA') is not null                  
   DROP TABLE #PENDENCIAS_TRANSF_SAIDA        
            
SELECT A.PRODUTO,            
       A.EMPRESA,        
       SUM(A.TRANSF_PENDENTE_SAIDA) AS TRANSF_PENDENTE_SAIDA        
               
  INTO #PENDENCIAS_TRANSF_SAIDA        
                   
  FROM #ROMANEIOS_PENDENTES_SAIDA A                
 GROUP BY A.PRODUTO, A.EMPRESA        
                     
--FIM TRANSITO ORIGEM--                
        
                
--------------------------------------------------------                
--DEFINICAO DO TRANSFERENCIAS PENDENTES NO DESTINO                
--------------------------------------------------------                
            
if object_id('tempdb..#ROMANEIOS_PENDENTES_ENTRADA') is not null                  
   DROP TABLE #ROMANEIOS_PENDENTES_ENTRADA        
                             
 SELECT B.PRODUTO,                    
        A.ESTOQUE_TRANSFERENCIA,            
        A.CENTRO_ESTOQUE_DESTINO  AS EMPRESA,            
       (CASE WHEN SUM(B.CONFERENCIA) > 0                 
             THEN SUM(B.CONFERENCIA)                
             ELSE SUM(B.QTDE_ORIGINAL - B.ACERTO_FALTA)                
        END)  AS TRANSF_PENDENTE_ENTRADA                
                    
   INTO #ROMANEIOS_PENDENTES_ENTRADA                
                                                          
  FROM ESTOQUE_TRANSFERENCIAS                    A WITH(NOLOCK)        
       JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES    B WITH(NOLOCK)ON B.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA          
       JOIN #PRODUTOS                            C WITH(NOLOCK)ON C.PRODUTO                = B.PRODUTO        
       JOIN #EMPRESAS_CENTROS_ESTOQUE            F WITH(NOLOCK) ON F.EMPRESA               = A.CENTRO_ESTOQUE_DESTINO           
       JOIN CENTROS_ESTOQUE                      G WITH(NOLOCK) ON G.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_ORIGEM        
       JOIN CENTROS_ESTOQUE                      H WITH(NOLOCK) ON H.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_DESTINO        
        
                                                         
  LEFT JOIN ( SELECT DISTINCT A.ESTOQUE_TRANSFERENCIA                
                FROM NF_FATURAMENTO_TRANSF               A WITH(NOLOCK)                 
                JOIN ESTOQUE_TRANSFERENCIAS_RECEBIMENTOS B WITH(NOLOCK) ON B.NF_FATURAMENTO          = A.NF_FATURAMENTO                
                JOIN ESTOQUE_TRANSFERENCIAS              C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA   = A.ESTOQUE_TRANSFERENCIA                       
                JOIN #EMPRESAS_CENTROS_ESTOQUE           D WITH(NOLOCK) ON D.EMPRESA                 = C.EMPRESA           
               WHERE C.MOVIMENTO >= CONVERT( DATE, GETDATE() - @DIAS_CONSIDERAR_TRANSITO )      
        AND ISNULL( B.PROCESSAR , 'N' ) = 'S' ) X ON X.ESTOQUE_TRANSFERENCIA =  A.ESTOQUE_TRANSFERENCIA                
                         
 WHERE 1 = 1                                        
   AND X.ESTOQUE_TRANSFERENCIA IS NULL                
   AND A.MOVIMENTO >= CONVERT( DATE, GETDATE() - @DIAS_CONSIDERAR_TRANSITO )         
   AND G.EMPRESA <> H.EMPRESA --PEGAR APENAS ROMANEIOS ENTRE LOJAS--                  
                 
 GROUP BY B.PRODUTO , A.ESTOQUE_TRANSFERENCIA, A.CENTRO_ESTOQUE_DESTINO                
         
 HAVING (CASE WHEN SUM(B.CONFERENCIA) > 0                 
              THEN SUM(B.CONFERENCIA)                
              ELSE SUM(B.QTDE_ORIGINAL - B.ACERTO_FALTA)                
         END) > 0         
            
            
--CONSOLIDA POR LOJA E PRODUTO--            
            
IF object_id('tempdb..#PENDENCIAS_TRANSF_ENTRADA') IS NOT NULL                
    DROP TABLE #PENDENCIAS_TRANSF_ENTRADA        
            
SELECT A.PRODUTO,            
       A.EMPRESA,            
       SUM(A.TRANSF_PENDENTE_ENTRADA) AS TRANSF_PENDENTE_ENTRADA        
               
  INTO #PENDENCIAS_TRANSF_ENTRADA        
                   
  FROM #ROMANEIOS_PENDENTES_ENTRADA A WITH(NOLOCK)            
 GROUP BY A.PRODUTO, A.EMPRESA            
                
--FIM TRANSITO DESTINO--          
        
          
--------------------------------------------------------        
--DEFINICAO DO SALDO DE PEDIDO PENDENTE - LOJA        
--------------------------------------------------------        
        
IF object_id('tempdb..#PEDIDOS_PENDENTES') IS NOT NULL                
    DROP TABLE #PEDIDOS_PENDENTES        
        
   SELECT A.PRODUTO        AS PRODUTO ,          
          B.EMPRESA        AS EMPRESA ,        
          SUM(A.SALDO )    AS PEDIDOS        
        
     INTO #PEDIDOS_PENDENTES        
        
     FROM PEDIDOS_COMPRAS_PRODUTOS_SALDO A WITH(NOLOCK)         
     JOIN PEDIDOS_COMPRAS                B WITH(NOLOCK) ON B.PEDIDO_COMPRA   = A.PEDIDO_COMPRA        
     JOIN EMPRESAS_USUARIAS              C WITH(NOLOCK) ON C.EMPRESA_USUARIA = B.EMPRESA                                                               
     JOIN PRODUTOS                       E WITH(NOLOCK) ON E.PRODUTO         = A.PRODUTO        
     JOIN #PRODUTOS                      H WITH(NOLOCK) ON H.PRODUTO         = A.PRODUTO        
     JOIN PEDIDOS_COMPRAS_PRODUTOS       I WITH(NOLOCK) ON I.PEDIDO_COMPRA_PRODUTO = A.PEDIDO_COMPRA_PRODUTO        
                                                       AND I.PRODUTO         = A.PRODUTO        
                                                               
     JOIN #EMPRESAS_CENTROS_ESTOQUE      J WITH(NOLOCK) ON J.EMPRESA         = B.EMPRESA           
            
    WHERE 1 = 1       
      AND A.ESTADO = 1        
        
 GROUP BY A.PRODUTO, B.EMPRESA        
 HAVING SUM(A.SALDO ) > 0         
        
        
----------------------------------------        
-- GERA TABELA TEMPORARIA DA SUGESTAO --        
----------------------------------------        
        
IF object_id('tempdb..#NECESSIDADE_LOJA') IS NOT NULL                
    DROP TABLE #NECESSIDADE_LOJA        
            
   SELECT DISTINCT               
                                            
          D.EMPRESA,        
          A.PRODUTO            AS PRODUTO,        
          A.DESCRICAO_REDUZIDA AS DESCRICAO,        
          A.FATOR_EMBALAGEM    AS QUANTIDADE_EMBALAGEM,        
          ISNULL(H.MES_1, 0)   AS VENDA_MES_ANTEC,                           
          ISNULL(H.MES_2, 0)   AS VENDA_MES_ANT,        
          ISNULL(H.MES_3, 0)   AS VENDA_MES_ATUAL,        
          ISNULL(H.MES_4, 0)   AS VENDA_30_DIAS,        
          ISNULL(H.CURVA, 'E') AS CURVA,        
                  
          CASE WHEN ISNULL(H.CURVA, 'E') = 'A' THEN ISNULL(@DIAS_CURVA_A, 1)                 
               WHEN ISNULL(H.CURVA, 'E') = 'B' THEN ISNULL(@DIAS_CURVA_B, 1)                 
               WHEN ISNULL(H.CURVA, 'E') = 'C' THEN ISNULL(@DIAS_CURVA_C, 1)                 
               WHEN ISNULL(H.CURVA, 'E') = 'D' THEN ISNULL(@DIAS_CURVA_D, 1)                 
               ELSE ISNULL(@DIAS_CURVA_E, 1)                 
          END                                AS DIAS_ESTOQUE,                
                                             
          ISNULL(H.DEMANDA_DIA_PONDERADA, 0) AS DEMANDA_DIA,        
          ISNULL(H.DEMANDA_DIA_PONDERADA, 0) AS DEMANDA_DIA_PONDERADA,        
                          
          D.ESTOQUE_SALDO                                                   AS ESTOQUE_SALDO,            
          ISNULL(E.TRANSF_PENDENTE_SAIDA,0)                                 AS TRANSF_PENDENTE_SAIDA,        
          ISNULL(F.TRANSF_PENDENTE_ENTRADA,0)                               AS TRANSF_PENDENTE_ENTRADA,        
          ISNULL(G.PEDIDOS,0)                                               AS PEDIDOS_PENDENTES,        
        
          D.ESTOQUE_SALDO                                         -        
          ISNULL(E.TRANSF_PENDENTE_SAIDA,0)                                 AS ESTOQUE_SALDO_EXCESSO,        
        
          D.ESTOQUE_SALDO                                                   -        
          ISNULL(E.TRANSF_PENDENTE_SAIDA,0)                                 +        
          ISNULL(F.TRANSF_PENDENTE_ENTRADA,0)                               +        
          ISNULL(G.PEDIDOS,0)                                               AS ESTOQUE_SALDO_VIRTUAL,        
                  
                                        
          ISNULL(D.ESTOQUE_MINIMO, 0)                                       AS ESTOQUE_MINIMO,        
          ISNULL(D.ESTOQUE_MAXIMO, 0)                                       AS ESTOQUE_MAXIMO,        
          ISNULL(D.ESTOQUE_SEGURANCA, 0)                                    AS ESTOQUE_SEGURANCA,        
        
          ROUND(ISNULL(H.DEMANDA_DIA_PONDERADA, 0) * (CASE WHEN H.CURVA = 'A' THEN ISNULL(@DIAS_CURVA_A, 1)                 
                                                           WHEN H.CURVA = 'B' THEN ISNULL(@DIAS_CURVA_B, 1)                 
                                                           WHEN H.CURVA = 'C' THEN ISNULL(@DIAS_CURVA_C, 1)                 
                                                           WHEN H.CURVA = 'D' THEN ISNULL(@DIAS_CURVA_D, 1)                 
                                                           ELSE ISNULL(@DIAS_CURVA_E, 1)                 
                                                      END), 0)             AS VALOR_BASE,        
                          
          CAST(0.00 AS NUMERIC(15,2))                                       AS LIMITE_CORTE_EXCESSO,        
          CAST(0.00 AS NUMERIC(15,2))                                       AS QUANTIDADE_EXCESSO        
                 
                                      
     INTO #NECESSIDADE_LOJA        
        
     FROM PRODUTOS                              A WITH(NOLOCK)        
     JOIN #PRODUTOS                             B WITH(NOLOCK) ON B.PRODUTO       = A.PRODUTO        
     JOIN #EMPRESAS_CENTROS_ESTOQUE             C WITH(NOLOCK) ON 1 = 1         
                                                                                                                                                                            
     JOIN #ESTOQUE_LOJA                         D WITH(NOLOCK) ON D.EMPRESA = C.EMPRESA        
                                                              AND D.PRODUTO = A.PRODUTO        
                       
LEFT JOIN #PENDENCIAS_TRANSF_SAIDA              E WITH(NOLOCK) ON E.PRODUTO          = A.PRODUTO        
                                                              AND E.EMPRESA          = D.EMPRESA        
                                                                                          
LEFT JOIN #PENDENCIAS_TRANSF_ENTRADA            F WITH(NOLOCK) ON F.PRODUTO          = A.PRODUTO        
                                                              AND F.EMPRESA          = D.EMPRESA        
        
LEFT JOIN #PEDIDOS_PENDENTES                    G WITH(NOLOCK) ON G.PRODUTO          = A.PRODUTO        
                                                              AND G.EMPRESA          = D.EMPRESA        
                              
                                                                                          
LEFT JOIN VALOR_BASE                            H WITH (NOLOCK) ON H.PRODUTO = A.PRODUTO        
                                                               AND H.MES     = @MES        
                                                               AND H.ANO     = @ANO        
                                                               AND H.EMPRESA = D.EMPRESA        
WHERE 1 = 1        
  AND A.SITUACAO_PRODUTO = 1        
  AND D.SITUACAO_PRODUTO = 1
  AND((A.STATUS_ATIVO_TRANSFERENCIA = 1 AND C.EMPRESA = 1000) OR (C.EMPRESA <> 1000) )       
        
        
        
--------------------------------------------------------------------        
--ATUALIZA O EXECESSO        
--------------------------------------------------------------------        
        
--ATUALIZA O LIMITE_CORTE_EXCESSO--        
        
UPDATE #NECESSIDADE_LOJA         
           
   SET LIMITE_CORTE_EXCESSO = ( CASE WHEN @TIPO_REMANEJAMENTO_LOJA  = 1 --EMAX ESEG        
                                     THEN DBO.MAIOR_VALOR( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO )         
                                     ELSE ( CASE WHEN DBO.MAIOR_VALOR( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO ) > A.VALOR_BASE        
                                                 THEN DBO.MAIOR_VALOR( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO )         
                                                 ELSE A.VALOR_BASE        
                                            END )        
                                                  
                                  END )  * ( @PERCENTUAL_EXCESSO / 100.00 )        
        
  FROM #NECESSIDADE_LOJA A         
        
        
--ATUALIZA O LIMITE_CORTE_EXCESSO--        
        
UPDATE #NECESSIDADE_LOJA         
           
   SET QUANTIDADE_EXCESSO = CASE WHEN A.ESTOQUE_SALDO_EXCESSO > A.LIMITE_CORTE_EXCESSO        
                                 THEN A.ESTOQUE_SALDO_EXCESSO - A.LIMITE_CORTE_EXCESSO        
                                 ELSE 0.00        
                             END         
        
  FROM #NECESSIDADE_LOJA A         
        
        
---------------------------------------------------------------        
-----GERA A TABELA DE RESULTADOS        
---------------------------------------------------------------        
        
DELETE FROM REMANEJAMENTOS_ESTOQUES_LOJAS_EXCESSOS WHERE REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA         
        
        
INSERT INTO REMANEJAMENTOS_ESTOQUES_LOJAS_EXCESSOS  (         
        
        
            FORMULARIO_ORIGEM                      ,                
            TAB_MASTER_ORIGEM                      ,               
            REG_MASTER_ORIGEM                      ,        
            REMANEJAMENTO_ESTOQUE_LOJA             ,                
            PRODUTO                                ,                
            EMPRESA                                ,                
            VENDA_MES_ANTEC                        ,                
            VENDA_MES_ANT         ,            
            VENDA_30_DIAS         ,           
            CURVA           ,          
            ESTOQUE_SALDO         ,         
            ESTOQUE_SALDO_EXCESSO                  ,         
            TRANSF_PENDENTE_SAIDA       ,           
            TRANSF_PENDENTE_ENTRADA       ,          
            PEDIDOS_PENDENTES        ,           
            DEMANDA_DIA_PONDERADA       ,           
			ESTOQUE_MINIMO         ,           
            ESTOQUE_SEGURANCA                      ,                
            ESTOQUE_MAXIMO         ,           
            VALOR_BASE          ,           
            LIMITE_CORTE_EXCESSO       ,           
            QUANTIDADE_EXCESSO        ,           
            QUANTIDADE_EMBALAGEM       ,           
            QUANTIDADE_SUGERIDA_UNITARIA,        
            QUANTIDADE_REMANEJADA            )            
        
        
SELECT B.FORMULARIO_ORIGEM                                 ,        
       B.TAB_MASTER_ORIGEM                                 ,        
       B.REMANEJAMENTO_ESTOQUE_LOJA AS REG_MASTER_ORIGEM   ,        
       B.REMANEJAMENTO_ESTOQUE_LOJA                        ,        
       A.PRODUTO                                           ,        
       A.EMPRESA                                             ,        
       A.VENDA_MES_ANTEC           ,        
       A.VENDA_MES_ANT            ,        
       A.VENDA_30_DIAS            ,        
       A.CURVA              ,        
       A.ESTOQUE_SALDO            ,        
       A.ESTOQUE_SALDO_EXCESSO                             ,        
       A.TRANSF_PENDENTE_SAIDA          ,        
       A.TRANSF_PENDENTE_ENTRADA          ,        
       A.PEDIDOS_PENDENTES           ,        
       A.DEMANDA_DIA_PONDERADA          ,        
       A.ESTOQUE_MINIMO            ,        
       A.ESTOQUE_SEGURANCA                                   ,        
       A.ESTOQUE_MAXIMO            ,        
       A.VALOR_BASE             ,        
       A.LIMITE_CORTE_EXCESSO           ,        
       A.QUANTIDADE_EXCESSO           ,        
       A.QUANTIDADE_EMBALAGEM           ,        
       0 AS QUANTIDADE_SUGERIDA_UNITARIA  ,        
       0 AS QUANTIDADE_REMANEJADA                              
               
 FROM #NECESSIDADE_LOJA             A WITH(NOLOCK)        
 JOIN REMANEJAMENTOS_ESTOQUES_LOJAS B WITH(NOLOCK) ON B.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA         
         
         
         
--ATUALIZA A ORDEM DAS EMPRESAS PARA O WHILE DE EXECU��O--         
UPDATE REMANEJAMENTOS_ESTOQUES_LOJAS_ORIGENS        
        
   SET ORDEM     = X.ID,        
       EXCESSO   = X.EXCESSO        
                       
   FROM REMANEJAMENTOS_ESTOQUES_LOJAS_ORIGENS A WITH(NOLOCK)        
   JOIN ( SELECT A.EMPRESA,        
                 SUM(A.QUANTIDADE_EXCESSO) AS EXCESSO,        
                 ROW_NUMBER() OVER (PARTITION BY @REMANEJAMENTO_ESTOQUE_LOJA  ORDER BY SUM(A.QUANTIDADE_EXCESSO) DESC ) AS ID        
            FROM #NECESSIDADE_LOJA A         
        GROUP BY A.EMPRESA ) X ON X.EMPRESA = A.EMPRESA        
          
  WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA         
         
         SELECT * FROM #NECESSIDADE_LOJA

--END --END PROCEDURE-- 