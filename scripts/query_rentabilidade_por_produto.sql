DECLARE @MOVIMENTO_INICIAL DATE 
DECLARE @MOVIMENTO_FINAL   DATE 
DECLARE @EMPRESA           NUMERIC

SET @MOVIMENTO_INICIAL= :MOVIMENTO_INICIAL
SET @MOVIMENTO_FINAL  = :MOVIMENTO_FINAL
IF ISNUMERIC(:EMPRESA)= 1 SET @EMPRESA  = :EMPRESA  ELSE SET @EMPRESA  = NULL


--SET @EMPRESA          = 5
--SET @MOVIMENTO_INICIAL= '01/05/2018'
--SET @MOVIMENTO_FINAL  = '31/05/2018'
--DECLARE @LOGIN VARCHAR(MAX)= 'YPEDRO'

   SELECT @MOVIMENTO_INICIAL                                              AS DATA_I ,
          @MOVIMENTO_FINAL                                                AS DATA_F ,
		  A.EMPRESA                                                       AS EMPRESA, 
		  D.DESCRICAO                                                     AS GRUPO_MARCA,
		  E.DESCRICAO                                                     AS MARCA,
          C.PRODUTO                                                       AS PRODUTO , 
          C.DESCRICAO                                                     AS DESCRICAO , 
          SUM ( A.QUANTIDADE )                                            AS QUANTIDADE ,
          SUM ( A.VENDA_BRUTA )                                           AS VENDA_BRUTA , 
          SUM ( A.DESCONTO )                                              AS DESCONTO , 
          SUM ( A.DESCONTO_AUTOMATICO )                                   AS DESCONTO_AUTOMATICO , 
          SUM ( A.DESCONTO ) -  
          SUM ( A.DESCONTO_AUTOMATICO )                                   AS DESCONTO_CONCEDIDO ,
          SUM ( A.VENDA_LIQUIDA )                                         AS VENDA_LIQUIDA , 
          SUM ( A.IMPOSTOS )                                              AS IMPOSTOS ,
          SUM ( A.QUANTIDADE * ISNULL ( B.CUSTO_CONTABIL , 0 ) )          AS CMV , 

          SUM ( CASE WHEN A.TIPO_BONIFICACAO = 'V' 
                     THEN A.QUANTIDADE * A.BONIFICACAO
                     ELSE A.VENDA_LIQUIDA * ( A.BONIFICACAO / 100 ) END ) AS BONIFICACAO , 


          SUM ( CASE WHEN A.TIPO_COMISSAO = 'V' 
                     THEN A.QUANTIDADE * A.COMISSAO
                     ELSE A.VENDA_LIQUIDA * ( A.COMISSAO / 100 ) END ) AS COMISSAO ,

          SUM ( A.VENDA_LIQUIDA ) - 
          SUM ( A.IMPOSTOS ) - 
          SUM ( A.QUANTIDADE * ISNULL ( B.CUSTO_CONTABIL , 0 ) )          AS LUCRO_BRUTO,
		  F.DESCRICAO                                                     AS GRUPO_TRIBUT


     INTO #TEMP_01
     FROM VENDAS_ANALITICAS                   A WITH(NOLOCK)
     JOIN EMPRESAS_USUARIAS                   X WITH(NOLOCK) ON X.EMPRESA_USUARIA  = A.EMPRESA
     JOIN UDF_USUARIO_EMPRESAS(@LOGIN)        Y              ON X.EMPRESA_USUARIA  = Y.EMPRESA_USUARIA
LEFT JOIN CUSTO_MEDIO_MENSAL_EMPRESA_CONTABIL B WITH(NOLOCK) ON B.PRODUTO          = A.PRODUTO AND B.EMPRESA_CONTABIL = X.EMPRESA_CONTABIL AND B.MES = MONTH ( A.MOVIMENTO ) AND B.ANO = YEAR ( A.MOVIMENTO ) 
LEFT JOIN PRODUTOS                            C WITH(NOLOCK) ON C.PRODUTO          = A.PRODUTO
LEFT JOIN GRUPOS_MARCAS                       D WITH(NOLOCK) ON D.GRUPO_MARCA      = C.GRUPO_PRODUTO 
LEFT JOIN MARCAS                              E WITH(NOLOCK) ON C.MARCA            = E.MARCA
LEFT JOIN GRUPOS_TRIBUTARIOS                  F WITH(NOLOCK) ON F.GRUPO_TRIBUTARIO = C.GRUPO_TRIBUTARIO
    WHERE A.MOVIMENTO BETWEEN @MOVIMENTO_INICIAL AND @MOVIMENTO_FINAL
	  AND (A.EMPRESA = @EMPRESA                  OR  @EMPRESA IS NULL)
 GROUP BY C.PRODUTO , C.DESCRICAO,D.DESCRICAO, E.DESCRICAO, F.DESCRICAO,A.EMPRESA
								
SELECT @MOVIMENTO_INICIAL                             AS DATA_I , 
       @MOVIMENTO_FINAL                               AS DATA_F , 
	   EMPRESA                                        AS EMPRESA,
       PRODUTO                                        AS PRODUTO , 
       DESCRICAO                                      AS DESCRICAO ,
       MARCA                                          AS MARCA,
	   GRUPO_MARCA                                    AS GRUPO_MARCA,
	   GRUPO_TRIBUT                                   AS GRUPO_TRIBUT,
       QUANTIDADE                                     AS QUANTIDADE , 
       VENDA_BRUTA                                    AS VENDA_BRUTA ,
       DESCONTO                                       AS DESCONTO , 


       CASE WHEN VENDA_BRUTA > 0 THEN 
     ( DESCONTO / VENDA_BRUTA )      * 100 ELSE 0 END AS PDESCONTO ,       

       DESCONTO_AUTOMATICO                            AS DESCONTO_AUTOMATICO ,
       CASE WHEN VENDA_BRUTA > 0 THEN 
     ( DESCONTO_AUTOMATICO/VENDA_BRUTA)*100 ELSE 0 END AS PDESCONTO_AUTOMATICO , 

       DESCONTO_CONCEDIDO                             AS DESCONTO_CONCEDIDO , 
       CASE WHEN VENDA_BRUTA > 0 THEN 
     ( DESCONTO_CONCEDIDO/VENDA_BRUTA)*100 ELSE 0 END AS PDESCONTO_CONCEDIDO , 

       VENDA_LIQUIDA                                  AS VENDA_LIQUIDA ,
       IMPOSTOS                                       AS IMPOSTOS , 
       CASE WHEN VENDA_LIQUIDA > 0 THEN 
     ( IMPOSTOS / VENDA_LIQUIDA )    * 100 ELSE 0 END AS PIMPOSTOS , 
       CMV                                            AS CMV , 
       CASE WHEN VENDA_LIQUIDA > 0 THEN
     ( CMV / VENDA_LIQUIDA )         * 100 ELSE 0 END AS PCMV , 
       COMISSAO                                       AS COMISSAO ,
       CASE WHEN VENDA_LIQUIDA > 0 THEN 
     ( COMISSAO / VENDA_LIQUIDA )    * 100 ELSE 0 END AS PCOMISSAO , 
       LUCRO_BRUTO                                    AS LUCRO_BRUTO , 
       CASE WHEN VENDA_LIQUIDA > 0 THEN
     ( LUCRO_BRUTO / VENDA_LIQUIDA ) * 100 ELSE 0 END AS PLUCRO_BRUTO ,
       CASE WHEN QUANTIDADE <> 0
            THEN VENDA_LIQUIDA / QUANTIDADE ELSE 0 END AS PRECO_MEDIO ,
       CASE WHEN QUANTIDADE <> 0 
            THEN CMV / QUANTIDADE ELSE 0 END          AS CUSTO_MEDIO
  FROM #TEMP_01

DROP TABLE #TEMP_01