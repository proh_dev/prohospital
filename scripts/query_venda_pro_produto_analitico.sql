DECLARE @MOVIMENTO_INICIAL DATE         
DECLARE @MOVIMENTO_FINAL   DATE         
DECLARE @PRODUTO           NUMERIC(15)  
DECLARE @EMPRESA           NUMERIC(15)  
DECLARE @SUBGRUPO          NUMERIC(15)  
DECLARE @GRUPO             NUMERIC(15)
DECLARE @FAMILIA           NUMERIC(15)
DECLARE @MARCA			   NUMERIC(15) 


 --grupo loja familia e marca,


  SET @MOVIMENTO_INICIAL= :MOVIMENTO_INICIAL
  SET @MOVIMENTO_FINAL  = :MOVIMENTO_FINAL 
  SET @PRODUTO          = (CASE WHEN ISNUMERIC(:PRODUTO)  = 1 THEN :PRODUTO  ELSE NULL END)
  SET @EMPRESA          = (CASE WHEN ISNUMERIC(:EMPRESA)  = 1 THEN :EMPRESA  ELSE NULL END)
  SET @SUBGRUPO         = (CASE WHEN ISNUMERIC(:SUBGRUPO) = 1 THEN :SUBGRUPO ELSE NULL END)
  SET @GRUPO            = (CASE WHEN ISNUMERIC(:GRUPO)    = 1 THEN :GRUPO    ELSE NULL END)
  SET @FAMILIA          = (CASE WHEN ISNUMERIC(:FAMILIA)  = 1 THEN :FAMILIA  ELSE NULL END)
  SET @MARCA            = (CASE WHEN ISNUMERIC(:MARCA)    = 1 THEN :MARCA    ELSE NULL END)

  --SET @MOVIMENTO_INICIAL  = '01/01/2017'
  --SET @MOVIMENTO_FINAL    = '31/12/2018'
  ----SET @PRODUTO            = 64622
  --SET @EMPRESA            = 1000
  --SET @SUBGRUPO           = NULL
  





SELECT                                                                                                             
    A.EMPRESA                                                                                   AS EMPRESA                ,                    
   -- C.NOME                                                                                     AS EMPRESA               ,
    RTRIM(CONVERT(CHAR, A.CLIENTE)) + ' - '+ F.NOME + ISNULL(' TEL: ' + H.TELEFONES,'')        AS CLIENTE                 ,  -- Adicionado 15/02/2016 - Carlos
    D.DESCRICAO_REDUZIDA + ' - ' + RTRIM(CONVERT(CHAR, A.PRODUTO)) + ISNULL(' - ' + E.EAN, '') AS PRODUTO                 ,
    A.MOVIMENTO                                                                                AS DATA                    ,
    A.CAIXA                                                                                    AS DOCUMENTO               ,
    A.QUANTIDADE                                                                               AS QUANTIDADE              ,
    A.VENDA_BRUTA                                                                              AS PRECO_BRUTO             ,
    A.DESCONTO_AUTOMATICO                                                                      AS DESCONTO_AUTOMATICO     ,
    A.DESCONTO_CONCEDIDO                                                                       AS DESCONTO_CONCEDIDO      ,

    ( A.VENDA_LIQUIDA/
    A.QUANTIDADE      )                                                                         AS VALOR_UN               , 

    CONVERT(NUMERIC(15,2),
           ((A.DESCONTO_AUTOMATICO + A.DESCONTO_CONCEDIDO * 100) /
           A.VENDA_BRUTA))                                                                     AS PERC_DESCONTO           ,

    (A.VENDA_BRUTA - (A.DESCONTO_AUTOMATICO + A.DESCONTO_CONCEDIDO))                           AS PRECO_LIQUIDO           ,
    A.VENDA_LIQUIDA                                                                            AS TOTAL                   ,
    A.DOCUMENTO_NUMERO                                                                         AS DOCUMENTO_NUMERO        , -- Adicionado 15/02/2016 - Carlos
    G.DESCRICAO                                                                                AS DOCUMENTO_TIPO          , -- Adicionado 15/02/2016 - Carlos
    A.OPERACAO_FISCAL                                                                          AS OPERACAO_FISCAL         ,
	I.DESCRICAO                                                                                AS MARCA                   ,
	J.DESCRICAO																				   AS GRUPO 				  ,
	K.DESCRICAO																				   AS FAMILIA

      
FROM
    VENDAS_ANALITICAS                 A WITH(NOLOCK)
    JOIN UDF_USUARIO_EMPRESAS(1)      B                          ON A.EMPRESA        = B.EMPRESA_USUARIA
    JOIN EMPRESAS_USUARIAS            C WITH(NOLOCK)             ON A.EMPRESA        = C.EMPRESA_USUARIA
    JOIN PRODUTOS                     D WITH(NOLOCK)             ON A.PRODUTO        = D.PRODUTO
    LEFT JOIN
        (                      
            SELECT TOP 1
                PRODUTO AS PRODUTO,
                EAN     AS EAN
            FROM
                PRODUTOS_EAN A WITH(NOLOCK)
        )                             E                          ON D.PRODUTO         = E.PRODUTO

                                         
    LEFT JOIN ENTIDADES                          F WITH(NOLOCK)  ON F.ENTIDADE        = A.CLIENTE
    LEFT JOIN TIPOS_DOCUMENTOS_VENDAS_ANALITICAS G WITH(NOLOCK)  ON G.DOCUMENTO_TIPO  = A.DOCUMENTO_TIPO
    LEFT JOIN (

    SELECT  ENTIDADE ,  
          COALESCE(  
           (SELECT '(' + CAST(DDD AS VARCHAR) + ') ' + CAST(NUMERO AS VARCHAR(10)) + ' / ' AS [text()]  
            FROM TELEFONES AS O WITH(NOLOCK)   
            WHERE O.ENTIDADE  = C.ENTIDADE
            ORDER BY ENTIDADE
            FOR XML PATH(''), TYPE).value('.[1]', 'VARCHAR(MAX)'), '') AS TELEFONES  
         FROM ENTIDADES AS C WITH(NOLOCK)   
         GROUP BY ENTIDADE

              )                                   H              ON H.ENTIDADE         = F.ENTIDADE
	LEFT JOIN MARCAS                              I WITH(NOLOCK) ON D.MARCA            = I.MARCA
	LEFT JOIN GRUPOS_PRODUTOS                     J WITH(NOLOCK) ON D.GRUPO_PRODUTO    = J.GRUPO_PRODUTO
	LEFT JOIN FAMILIAS_PRODUTOS                   K WITH(NOLOCK) ON D.FAMILIA_PRODUTO  = K.FAMILIA_PRODUTO
WHERE
    A.MOVIMENTO BETWEEN @MOVIMENTO_INICIAL AND @MOVIMENTO_FINAL         
    AND (A.PRODUTO          = @PRODUTO  OR @PRODUTO  IS NULL)
    AND (A.EMPRESA          = @EMPRESA  OR @EMPRESA  IS NULL)
    AND (D.SUBGRUPO_PRODUTO = @SUBGRUPO OR @SUBGRUPO IS NULL)     
	AND (D.GRUPO_PRODUTO    = @GRUPO    OR @GRUPO    IS NULL)       
	AND (D.FAMILIA_PRODUTO  = @FAMILIA  OR @FAMILIA  IS NULL)        
	AND (D.MARCA            = @MARCA    OR @MARCA	 IS NULL)	    
    AND A.CLIENTE > 1000