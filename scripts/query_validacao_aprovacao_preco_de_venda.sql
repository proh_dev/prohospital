SELECT 'N�o � poss�vel salvar uma aprova��o sem especificar entradas!'
  FROM APROVACAO_ENTRADAS_ESTOQUES A WITH(NOLOCK)
  LEFT JOIN APROVACAO_ENTRADAS_ITENS B WITH(NOLOCK) ON B.APROVACAO_ENTRADA = A.APROVACAO_ENTRADA
WHERE B.APROVACAO_ENTRADA IS NULL
  AND A.APROVACAO_ENTRADA = :APROVACAO_ENTRADA

UNION ALL

----------------------------------------------------------------
----------------------------------------------------------------
-- TRAVA DE PRODUTO SEM PRECO DE VENDA                        --
-- ADICIONADO POR YNOA PEDRO 13/08/2018                       --
----------------------------------------------------------------

SELECT TOP 1 'Produto' + ' - ' + CAST(D.PRODUTO AS VARCHAR(MAX)) + 
' Sem Precifica��o Favor Validar se no Cadastro do Produto o Pre�o de Venda Referente � Empresa ' +  CAST(A.FILIAL  AS VARCHAR(MAX)) +
' ou o Custo Base do Produto Est�o Preenchidos Adequadamente!' 
  FROM APROVACAO_ENTRADAS_ESTOQUES          A WITH(NOLOCK)
  JOIN APROVACAO_ENTRADAS_ITENS             B WITH(NOLOCK) ON B.APROVACAO_ENTRADA  = A.APROVACAO_ENTRADA
  JOIN ENTRADAS_ESTOQUE_AVULSAS             C WITH(NOLOCK) ON C.ENTRADA_ESTOQUE    = B.ENTRADA_ESTOQUE
  JOIN ENTRADAS_ESTOQUE_AVULSAS_PRODUTOS    D WITH(NOLOCK) ON D.ENTRADA_ESTOQUE    = C.ENTRADA_ESTOQUE
  JOIN PRODUTOS                             E WITH(NOLOCK) ON E.PRODUTO            = D.PRODUTO
  JOIN PRODUTOS_PARAMETROS_EMPRESAS         F WITH(NOLOCK) ON F.PRODUTO            = D.PRODUTO
  JOIN EMPRESAS_USUARIAS                    G WITH(NOLOCK) ON G.EMPRESA_USUARIA    = A.FILIAL

WHERE 1=1
  AND A.APROVACAO_ENTRADA =  :APROVACAO_ENTRADA
  AND A.EMPRESA           =   F.EMPRESA 
  AND  ( (F.PRECO_VENDA = 0 OR F.PRECO_VENDA IS NULL ) AND  (E.CUSTO_BASE = 0 OR E.CUSTO_BASE IS NULL ) )




