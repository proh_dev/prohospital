ALTER FUNCTION [dbo].[GERA_OBS_NF_FATURAMENTO]      
(@NF_FATURAMENTO        NUMERIC(15))      
RETURNS VARCHAR(MAX)      
AS      
BEGIN      

DECLARE @CONTA                 VARCHAR(MAX) = ''       
DECLARE @OBSERVACAO            VARCHAR(MAX) = ''      
DECLARE @ESTOQUE_TRANSFERENCIA VARCHAR(MAX) = ''      
DECLARE @DEPOSITO_FATURAMENTO  VARCHAR(MAX) = ''      
      
DECLARE @OBSERVACAO_TRIBUTARIA TABLE       
([GRUPO_TRIBUTARIO]      [NUMERIC](15),      
 [OBSERVACAO_TRIBUTARIO] [VARCHAR](MAX))      

       
   -----------------------------------      
   -- RECUPERA INFORMA합ES DE:      --      
   --  * CONTA BANCARIA  --         
   -----------------------------------      
      
  SELECT TOP 1 @CONTA  =  @CONTA + (ISNULL(CAST(B.DESCRICAO AS VARCHAR(1000)),'')) + (ISNULL(CAST(B.AGENCIA AS VARCHAR(1000)),'')) + 'Cc:' +  (ISNULL(CAST(B.CONTA_CORRENTE AS VARCHAR(1000)),''))        
    FROM NF_FATURAMENTO                  A WITH(NOLOCK)  
	JOIN CONTAS_BANCARIAS                B WITH(NOLOCK) ON A.EMPRESA = B.EMPRESA     
	WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
	  AND B.OBSERVACAO_NOTA = 'S'

	    
      
   -----------------------------------      
   -- RECUPERA INFORMA합ES DE:      --      
   --  * OBSERVA플O DA NOTA FISCAL  --         
   -----------------------------------      
      
  SELECT @OBSERVACAO = @OBSERVACAO + DBO.SUPRIMIR_CHAR_ESPECIAL (ISNULL(CAST(B.OBSERVACAO_NOTAFISCAL AS VARCHAR(1000)),''))               
    FROM NF_FATURAMENTO              A WITH(NOLOCK)      
    JOIN SOLICITACOES_FATURAMENTOS   B WITH(NOLOCK) ON B.SOLICITACAO_FATURAMENTO = A.SOLICITACAO_FATURAMENTO        
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO      
   AND A.SOLICITACAO_FATURAMENTO IS NOT NULL      
       
   -----------------------------------      
   -- RECUPERA INFORMA합ES DE:      --      
   --  * PRODUTOS CONTROLADOS       --      
   -----------------------------------       
         
  SELECT TOP 1  @OBSERVACAO = @OBSERVACAO + 'PORTARIA 344/98'      
    FROM NF_FATURAMENTO A WITH(NOLOCK)      
   WHERE A.VENDA_CONTROLADA = 'S'         
         
   -----------------------------------      
   -- RECUPERA INFORMA합ES DE:      --      
   --  * SOLICITA플O DE FATURAMENTO --       
   --  * PERDIDO DE VENDA           --      
   --  * ROMANITA플O DE FATURAMENTO --         
   -----------------------------------      
         
  SELECT @ESTOQUE_TRANSFERENCIA = @ESTOQUE_TRANSFERENCIA + CONVERT(VARCHAR, A.ESTOQUE_TRANSFERENCIA) + ' | '      
    FROM NF_FATURAMENTO_TRANSF A WITH(NOLOCK)      
   WHERE A.NF_FATURAMENTO      = @NF_FATURAMENTO       
    
  SELECT @DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO + CONVERT(VARCHAR, A.DEPOSITO_FATURAMENTO) + ' | '      
    FROM NF_FATURAMENTO A WITH(NOLOCK)      
   WHERE A.NF_FATURAMENTO      = @NF_FATURAMENTO      
         
IF ISNUMERIC(@ESTOQUE_TRANSFERENCIA) = 1  
  
BEGIN  
  
   SELECT @ESTOQUE_TRANSFERENCIA = SUBSTRING(@ESTOQUE_TRANSFERENCIA, 1, LEN(@ESTOQUE_TRANSFERENCIA) - 2)         
  
END  
            
  SELECT @OBSERVACAO = @OBSERVACAO + '| SOL.FAT: '   + CONVERT(VARCHAR, B.SOLICITACAO_FATURAMENTO)      
                                   + '| PED.VENDA: ' + CONVERT(VARCHAR, B.PEDIDO_VENDA)      
                                   + '| ROM.: '      + @ESTOQUE_TRANSFERENCIA      
                                   + '| DEP FAT: '   + @DEPOSITO_FATURAMENTO      
    FROM NF_FATURAMENTO                   A WITH(NOLOCK)        
    JOIN SOLICITACOES_FATURAMENTOS B WITH(NOLOCK) ON B.SOLICITACAO_FATURAMENTO = A.SOLICITACAO_FATURAMENTO          
   WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO       
         
      
   -----------------------------------      
   --   RECUPERA OBSERVA합ES DA     --      
   --   CLASSIFICA플O TRIBUTARIA    --         
   -----------------------------------      
       
 INSERT INTO @OBSERVACAO_TRIBUTARIA ( GRUPO_TRIBUTARIO,      
                                      OBSERVACAO_TRIBUTARIO)      
     SELECT DISTINCT      
            X.GRUPO_TRIBUTARIO,      
            C.OBSERVACAO      
             
       FROM NF_FATURAMENTO            A WITH(NOLOCK)      
       JOIN NF_FATURAMENTO_PRODUTOS   B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO      
CROSS APPLY DBO.DADOS_FISCAIS_PRODUTOS ( B.PRODUTO,      
                                         A.EMPRESA,      
                                         A.ENTIDADE,      
                                         A.OPERACAO_FISCAL,      
                                         B.TOTAL_PRODUTO,      
                                         B.IPI_ICMS ,      
                                              B.QUANTIDADE,       
                                              B.TOTAL_DESCONTO)   X,      
            OBSERVACOES_FISCAIS       C       
      WHERE X.OBSERVACAO_FISCAL       = C.OBSERVACAO_FISCAL      
        AND A.NF_FATURAMENTO  = @NF_FATURAMENTO      
            
     SELECT @OBSERVACAO = @OBSERVACAO + CONVERT(VARCHAR, A.GRUPO_TRIBUTARIO) + ' - ' + A.OBSERVACAO_TRIBUTARIO  + '; '      
       FROM @OBSERVACAO_TRIBUTARIA A      
         
RETURN(@OBSERVACAO)      
      
END 