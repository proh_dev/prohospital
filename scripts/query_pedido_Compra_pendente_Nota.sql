DECLARE @DATA DATE
        
SET @DATA = CAST(GETDATE() AS DATE)
        
--SELECT 
--  @DATA
--, ( DATEADD(DAY,(CASE WHEN (DATEPART(DW,@DATA )) = 2 THEN -4 WHEN (DATEPART(DW,@DATA )) = 3 THEN -4 ELSE -2 END),@DATA) )
--, ( DATEADD(DAY,(CASE WHEN (DATEPART(DW,@DATA )) = 2 THEN -2 WHEN (DATEPART(DW,@DATA )) = 3 THEN  0 ELSE  0 END),@DATA) )

SELECT 
       104                                      AS FORMULARIO_ORIGEM,
	   188                                      AS TAB_MASTER_ORIGEM,
	   A.REG_MASTER_ORIGEM ,
       A.PEDIDO_COMPRA                          AS PEDIDO_COMPRA,
       A.DATA_HORA                              AS DATA_DIGITACAO,
       A.EMPRESA                                AS EMPRESA,
       A.ENTIDADE                               AS ENTIDADE,
       D.NOME                                   AS FORNECEDOR,
       CONVERT(VARCHAR,A.DATA_FATURAMENTO, 103) AS DATA_FATURAMENTO

  FROM PEDIDOS_COMPRAS                 A WITH(NOLOCK)
  LEFT
  JOIN NF_COMPRA                       B WITH(NOLOCK) ON B.PEDIDO_COMPRA = A.PEDIDO_COMPRA
  LEFT
  JOIN (
            SELECT PEDIDO_COMPRA, MAX(ESTADO) ESTADO 
              FROM PEDIDOS_COMPRAS_PRODUTOS_SALDO WITH(NOLOCK) 
          GROUP BY PEDIDO_COMPRA
       )                               C              ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA
                                                     AND C.ESTADO        = 2
  JOIN ENTIDADES                       D WITH(NOLOCK) ON D.ENTIDADE      = A.ENTIDADE

 WHERE 1=1
   AND B.NF_COMPRA        IS NULL
   AND C.PEDIDO_COMPRA    IS NULL                                                       
   --AND A.DATA_FATURAMENTO > ( DATEADD(DAY,(CASE WHEN (DATEPART(DW,@DATA )) = 2 THEN -4 WHEN (DATEPART(DW,@DATA )) = 3 THEN -4 ELSE -2 END),@DATA) )
   AND A.DATA_FATURAMENTO < ( DATEADD(DAY,(CASE WHEN (DATEPART(DW,@DATA )) = 2 THEN -2 WHEN (DATEPART(DW,@DATA )) = 3 THEN  0 ELSE  0 END),@DATA) )
      AND A.EMPRESA NOT IN (500, 1000)

   ORDER BY A.DATA_FATURAMENTO                                          