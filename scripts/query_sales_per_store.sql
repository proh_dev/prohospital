   --Primeira query feita a modo de testes
   --Executar a query para pegar detalhes das vendas por loja com informa��es da loja do produto e do vendedor
   --Desenvolvedor = Yno� Pedro


DECLARE @LOJA NUMERIC(6)

--IF ISNUMERIC(:LOJA) = 1 
--  BEGIN 
--    SET @LOJA = :LOJA
--  END ELSE  
--    SET @LOJA = NULL

 SET @LOJA = NULL
SELECT   A.LOJA                            AS COD_LOJA          ,
         C.NOME_RESUMIDO                   AS NOME_LOJA         ,
	     A.PRODUTO                         AS COD_PRODUTO       ,
         B.DESCRICAO                       AS NOME_PRODUTO      ,
         A.QUANTIDADE                      AS QTD_VENDIDA       ,
         A.PRECO                           AS PRECO             ,
	     A.VENDEDOR                        AS COD_VENDEDOR      ,
         D.NOME                            AS NOME_VENDEDOR     ,
         A.MOVIMENTO                       AS DATA_VENDA        ,
         A.ECF_CUPOM                       AS ECF               ,
         A.TOTAL_PRECO_BRUTO               AS PRECO_BRUTO       ,
         A.TOTAL_PRECO_LIQUIDO             AS PRECO_LIQUIDO     

FROM PDV_ITENS   A WITH(NOLOCK)
JOIN PRODUTOS    B WITH(NOLOCK) ON A.PRODUTO = B.PRODUTO
LEFT 
JOIN LOJAS       C ON C.LOJA = A.LOJA
LEFT 
JOIN VENDEDORES  D ON A.VENDEDOR = D.VENDEDOR
    WHERE (A.LOJA = @LOJA OR @LOJA IS NULL)




