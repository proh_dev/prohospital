/*Valida��o produtos CST e NCM no proc warning
  Desenvolvedor: Yno� Pedro
  Data 06/06/2018
*/
DECLARE @NF_COMPRA NUMERIC

--SET @NF_COMPRA = 24941
SET @NF_COMPRA = :NF_COMPRA

SELECT 'ALERTA: Produto com CST e/ou NCM diferentes do Cadastro, favor Verificar' + ' '+ 'Produto' + ' '+ MAX(CONVERT(VARCHAR(15),A.PRODUTO))

FROM NF_COMPRA_PRODUTOS                                 A WITH(NOLOCK) 
JOIN PRODUTOS                                           B WITH(NOLOCK) ON A.PRODUTO           = B.PRODUTO 
JOIN GRUPOS_TRIBUTARIOS_PARAMETROS                      C WITH(NOLOCK) ON B.GRUPO_TRIBUTARIO  = C.GRUPO_TRIBUTARIO  

WHERE 1=1
  AND A.NF_COMPRA = :NF_COMPRA
  AND C.OPERACAO_FISCAL = 12
  AND (A.SITUACAO_TRIBUTARIA <> C.SITUACAO_TRIBUTARIA OR A.CLASSIF_FISCAL_CODIGO  <> B.NCM)