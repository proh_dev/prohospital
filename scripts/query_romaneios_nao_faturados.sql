CREATE PROCEDURE [dbo].[EMAIL_ENVIO_SOLICITACAO_PENDENTE] AS   
BEGIN                      
                
SET NOCOUNT ON    
DECLARE @tableHTML       NVARCHAR(MAX)
DECLARE @EMAIL           VARCHAR(250)  
DECLARE @TITULO_EMAIL    VARCHAR(50) = 'Solicita��es Pendentes de Despacho'
DECLARE @EMPRESA_ORIGEM  VARCHAR(50)
DECLARE @EMPRESA_DESTINO NUMERIC(15)

--IF OBJECT_ID('TEMPDB..#APROVACOES' ) IS NOT NULL DROP TABLE #APROVACOES
IF OBJECT_ID('TEMPDB..#DADOS_ENVIO') IS NOT NULL DROP TABLE #DADOS_ENVIO
IF OBJECT_ID('TEMPDB..#EMPRESAS'   ) IS NOT NULL DROP TABLE #EMPRESAS

--SELECT DISTINCT
--       B.SOLICITACAO_TRANSFERENCIA
--  INTO #APROVACOES
--  FROM APROVACAO_SOLICITACAO_TRANSF          A WITH(NOLOCK)
--  JOIN APROVACAO_SOLICITACAO_TRANSF_PRODUTOS B WITH(NOLOCK) ON A.APROVACAO_SOLICITACAO_TRANSF = B.APROVACAO_SOLICITACAO_TRANSF

---------------------------------------------------------------------------------------------------------------------------------
SELECT 
      A.APROVACAO_SOLICITACAO_TRANSF           AS APROVACAO
     ,A.DATA_PREVISTA_DESPACHO                 AS DATA_PREVISTA
     ,A.MOVIMENTO                              AS MOVIMENTO
     ,A.CENTRO_ESTOQUE_ORIGEM                  AS ORIGEM_COD
     ,D.NOME_FANTASIA                          AS ORIGEM
	 ,I.EMAIL                                  AS EMAIL
	 

	 INTO #DADOS_ENVIO
 	 FROM APROVACAO_SOLICITACAO_TRANSF                       A WITH(NOLOCK) 									            
     JOIN ESTOQUE_TRANSFERENCIAS                             B WITH(NOLOCK) ON A.APROVACAO_SOLICITACAO_TRANSF     = B.APROVACAO_SOLICITACAO_TRANSF
LEFT JOIN EMPRESAS_USUARIAS                                  D WITH(NOLOCK) ON A.CENTRO_ESTOQUE_ORIGEM            = D.EMPRESA_USUARIA               
LEFT JOIN EMPRESAS_USUARIAS                                  E WITH(NOLOCK) ON A.EMPRESA                          = E.EMPRESA_USUARIA
LEFT JOIN NF_FATURAMENTO_TRANSF                              F WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA            = F.ESTOQUE_TRANSFERENCIA									            
LEFT JOIN NF_FATURAMENTO                                     G WITH(NOLOCK) ON F.NF_FATURAMENTO                   = G.NF_FATURAMENTO
LEFT JOIN ESTOQUE_TRANSFERENCIAS_CANCELAMENTOS_ROMANEIOS     H WITH(NOLOCK) ON H.ESTOQUE_TRANSFERENCIA            = B.ESTOQUE_TRANSFERENCIA
     JOIN PARAMETROS_ESTOQUE                                 I WITH(NOLOCK) ON E.EMPRESA_USUARIA                  = I.EMPRESA_USUARIA
    WHERE 1=1
	 AND DATEDIFF(DAY,A.DATA_PREVISTA_DESPACHO,GETDATE())  > 1
	 AND H.ESTOQUE_TRANSFERENCIA_CANCELAMENTO_ROMANEIO IS NULL
	 AND (F.FATURAMENTO_TRANSF IS NULL OR G.EMITIR_NFE = 'N')




-------------------------------------------------------------------------------------------------------------------------------

SELECT DISTINCT
       A.ORIGEM AS NOME_ORIGEM
	  ,A.ORIGEM_COD
	  ,A.EMAIL
  INTO #EMPRESAS
  FROM #DADOS_ENVIO A WITH(NOLOCK)


WHILE EXISTS( SELECT TOP 1 1 FROM #EMPRESAS (NOLOCK) )
BEGIN

	SELECT TOP 1 @EMPRESA_ORIGEM = NOME_ORIGEM , @EMPRESA_DESTINO = A.ORIGEM_COD , @EMAIL = A.EMAIL FROM #EMPRESAS A WITH(NOLOCK) ORDER BY A.ORIGEM_COD ASC
--------------------------------------------------------------------------------------------------------------------------------

SET @tableHTML =       
    
    N'<style type="text/css">        
              .formato {        
               font-family: Verdana, Geneva, sans-serif;        
               font-size: 14px;     
      font-style: normal;    
      font-weight: bold;       
              }        
              .formato2 {        
               font-family: Verdana, Geneva, sans-serif;        
               font-size: 14px;     
      font-style: normal;    
      font-weight: normal;       
              }        
              .alinhameto {        
               text-align: center;        
              }        
              </style>'                    +        
            N'<H1 class="formato" >Solicita��o Pendentes de Separa��o <br>        
			EMPRESA: '+ @EMPRESA_ORIGEM + '<br> <br>    
			Voc� est� recebendo uma c�pia da Aprova��o realizado nesta data.   <br>     
			Caso haja alguma diverg�ncia com quantidades e/ou valores descritos abaixo, pedimos entrar em contato com nosso representante. <br><br>     
     </H1>         
    ' +        
    N'<table border="1" class="formato">' +        
    N'<tr>         
           <th>SOLICITACAO</th>        
           <th>DATA PREVISTA</th>        
           <th>ORIGEM</th>        
           <th>MOVIMENTO</th>   
      </tr>'                              +        
        
    CAST ( (         
        
  SELECT         
    td = CAST( A.APROVACAO  AS VARCHAR(30) )       ,  '' ,        
    td = CONVERT( VARCHAR , A.DATA_PREVISTA , 103 )  ,  '' ,        
    td = A.ORIGEM                                    ,  '' ,        
    td = CONVERT( VARCHAR , A.MOVIMENTO , 103 )      ,  ''    
        
   FROM #DADOS_ENVIO A WITH(NOLOCK) WHERE A.ORIGEM_COD = @EMPRESA_DESTINO   
        
              FOR XML PATH('tr'), TYPE         
    ) AS NVARCHAR(MAX) )   
     
     
 ;



   DELETE FROM #EMPRESAS WHERE ORIGEM_COD = @EMPRESA_DESTINO

  EXEC MSDB.DBO.SP_SEND_DBMAIL        
        
  @BLIND_COPY_RECIPIENTS   = @EMAIL,        
  @SUBJECT                 = @TITULO_EMAIL ,        
  @BODY                    = @tableHTML,        
  @BODY_FORMAT             = 'HTML';   
  
  WAITFOR DELAY '00:00:05'  


END
END