CREATE PROCEDURE [dbo].[BAIXAR_PEDIDO_COMPRA] ( @NF_COMPRA NUMERIC, @ENCERRAR_PEDIDO_TOTAL VARCHAR(1) = 'N' ) AS   
  
BEGIN  
  
--BEGIN TRANSACTION  
--ROLLBACK  
--COMMIT  
  
--DECLARE @NF_COMPRA NUMERIC  
--    SET @NF_COMPRA = 968  
  
if object_id('tempdb..#PEDIDOS_NOTA') is not null  
   DROP TABLE #PEDIDOS_NOTA  
  
SELECT DISTINCT X.PEDIDO_COMPRA, X.NF_COMPRA  
  
  INTO #PEDIDOS_NOTA  
  
  FROM (  
  
SELECT A.PEDIDO_COMPRA, A.NF_COMPRA  
  FROM NF_COMPRA  A WITH(NOLOCK)   
 WHERE A.NF_COMPRA = @NF_COMPRA  
  
UNION ALL  
  
SELECT A.PEDIDO_COMPRA, A.NF_COMPRA  
  FROM NF_COMPRA_PRODUTOS  A WITH(NOLOCK)   
  JOIN PEDIDOS_COMPRAS     B WITH(NOLOCK)  ON B.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
 WHERE A.NF_COMPRA = @NF_COMPRA ) X  
  
  
---------------------------------------------  
-- INTEGRACAO DE BAIXA DO PEDIDO DE COMPRA --  
---------------------------------------------  
  
DELETE PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES   
  FROM PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES A WITH(NOLOCK),  
       NF_COMPRA                           B WITH(NOLOCK)  
 WHERE A.REG_MASTER_ORIGEM = B.NF_COMPRA  
   AND A.FORMULARIO_ORIGEM = B.FORMULARIO_ORIGEM  
   AND A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM  
   AND B.NF_COMPRA         = @NF_COMPRA  
  
---------------------------------------------  
--MOVIMENTA QUANTIDADE RECEBIDA--  
---------------------------------------------  
  
PRINT 'QTDE RECEBIDA'  
  
INSERT INTO PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES (   
  
            FORMULARIO_ORIGEM ,  
            TAB_MASTER_ORIGEM ,  
            REG_MASTER_ORIGEM ,  
            REGISTRO_CONTROLE ,  
            REGISTRO_CONTROLE_II ,  
            DATA ,  
            PEDIDO_COMPRA ,  
            PEDIDO_COMPRA_PRODUTO ,  
            PRODUTO ,  
            PEDIDO ,  
            RECEBIMENTO ,  
            CANCELAMENTO )  
  
  
SELECT A.FORMULARIO_ORIGEM                 AS FORMULARIO_ORIGEM ,  
       A.TAB_MASTER_ORIGEM                 AS TAB_MASTER_ORIGEM ,  
       A.NF_COMPRA                         AS REG_MASTER_ORIGEM ,  
       B.PRODUTO                           AS REGISTRO_CONTROLE ,  
       1                                   AS REGISTRO_CONTROLE_II ,  
       A.MOVIMENTO                         AS DATA,  
       B.PEDIDO_COMPRA                     AS PEDIDO_COMPRA ,  
       C.PEDIDO_COMPRA_PRODUTO             AS PEDIDO_COMPRA_PRODUTO ,  
       B.PRODUTO                           AS PRODUTO ,  
       0                                   AS PEDIDO  ,  
         
       CASE WHEN B.QUANTIDADE_NOTA > C.SALDO_PEDIDO  
            THEN C.SALDO_PEDIDO  
            ELSE B.QUANTIDADE_NOTA  
       END                                 AS RECEBIMENTO, --PARA EVITAR QUE FIQUE SALDO NEGATIVO DE PEDIDO--         
       0                                   AS CANCELAMENTO                                                         
                                                                        
  FROM NF_COMPRA          A WITH(NOLOCK)   
    
  JOIN ( SELECT A.NF_COMPRA,  
                A.PRODUTO,  
                A.PEDIDO_COMPRA,  
                SUM(A.QUANTIDADE_ESTOQUE) AS QUANTIDADE_NOTA  
           FROM NF_COMPRA_PRODUTOS A WITH(NOLOCK)  
           JOIN PEDIDOS_COMPRAS    B WITH(NOLOCK) ON B.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
           JOIN #PEDIDOS_NOTA      C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
          WHERE A.NF_COMPRA = @NF_COMPRA  
       GROUP BY A.NF_COMPRA,  
                A.PRODUTO,  
                A.PEDIDO_COMPRA ) B ON B.NF_COMPRA     = A.NF_COMPRA  
  
  JOIN ( SELECT A.PRODUTO,  
                A.PEDIDO_COMPRA,  
                MAX(A.PEDIDO_COMPRA_PRODUTO)     AS PEDIDO_COMPRA_PRODUTO,  
                ISNULL(E.PEDIDO_ELETRONICO, 'N') AS PEDIDO_ELETRONICO,  
                SUM(C.SALDO) AS SALDO_PEDIDO  
           FROM PEDIDOS_COMPRAS_PRODUTOS       A WITH(NOLOCK)  
           JOIN PEDIDOS_COMPRAS_PRODUTOS_SALDO C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
                                                             AND C.PRODUTO       = A.PRODUTO  
                               
           JOIN #PEDIDOS_NOTA                  D WITH(NOLOCK) ON D.PEDIDO_COMPRA = A.PEDIDO_COMPRA             
           JOIN PEDIDOS_COMPRAS                E WITH(NOLOCK) ON E.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
                                                                        
       GROUP BY A.PRODUTO ,  
                A.PEDIDO_COMPRA,  
                ISNULL(E.PEDIDO_ELETRONICO, 'N') ) C ON C.PEDIDO_COMPRA  = B.PEDIDO_COMPRA  
                                                    AND C.PRODUTO        = B.PRODUTO  
                                                                                                                                     
 WHERE A.NF_COMPRA = @NF_COMPRA  
   AND ( CASE WHEN B.QUANTIDADE_NOTA > C.SALDO_PEDIDO  
              THEN C.SALDO_PEDIDO  
              ELSE B.QUANTIDADE_NOTA  
          END ) > 0   
  
----MOVIMENTA SALDO RESTANTE--  
  
--PRINT 'SALDO RESTANTE'  
  
INSERT INTO PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES (   
  
            FORMULARIO_ORIGEM ,  
            TAB_MASTER_ORIGEM ,  
            REG_MASTER_ORIGEM ,  
            REGISTRO_CONTROLE ,  
            REGISTRO_CONTROLE_II ,  
            DATA ,  
            PEDIDO_COMPRA ,  
            PEDIDO_COMPRA_PRODUTO ,  
            PRODUTO ,  
            PEDIDO ,  
            RECEBIMENTO ,  
            CANCELAMENTO )  
  
SELECT A.FORMULARIO_ORIGEM                 AS FORMULARIO_ORIGEM ,  
       A.TAB_MASTER_ORIGEM                 AS TAB_MASTER_ORIGEM ,  
       A.NF_COMPRA                         AS REG_MASTER_ORIGEM ,  
       C.PRODUTO                           AS REGISTRO_CONTROLE ,  
       1                                   AS REGISTRO_CONTROLE_II ,  
       A.MOVIMENTO                         AS DATA,  
       B.PEDIDO_COMPRA                     AS PEDIDO_COMPRA ,  
       C.PEDIDO_COMPRA_PRODUTO             AS PEDIDO_COMPRA_PRODUTO ,  
       C.PRODUTO                           AS PRODUTO ,  
       0                                   AS PEDIDO  ,         
       0                                   AS RECEBIMENTO,  
       C.SALDO_PEDIDO                      AS CANCELAMENTO                                                         
                                                                        
  FROM NF_COMPRA          A WITH(NOLOCK)   
  JOIN #PEDIDOS_NOTA      B WITH(NOLOCK) ON B.NF_COMPRA = A.NF_COMPRA  
    
  JOIN ( SELECT A.PRODUTO,  
                A.PEDIDO_COMPRA,  
                MAX(A.PEDIDO_COMPRA_PRODUTO)     AS PEDIDO_COMPRA_PRODUTO,  
                ISNULL(E.PEDIDO_ELETRONICO, 'N') AS PEDIDO_ELETRONICO,  
                SUM(C.SALDO) AS SALDO_PEDIDO  
           FROM PEDIDOS_COMPRAS_PRODUTOS       A WITH(NOLOCK)  
           JOIN PEDIDOS_COMPRAS_PRODUTOS_SALDO C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
                                                             AND C.PRODUTO       = A.PRODUTO  
                                                               
           JOIN #PEDIDOS_NOTA                  D WITH(NOLOCK) ON D.PEDIDO_COMPRA = A.PEDIDO_COMPRA             
           JOIN PEDIDOS_COMPRAS                E WITH(NOLOCK) ON E.PEDIDO_COMPRA = A.PEDIDO_COMPRA  
                                                                        
       GROUP BY A.PRODUTO ,  
                A.PEDIDO_COMPRA,  
                ISNULL(E.PEDIDO_ELETRONICO, 'N') ) C ON C.PEDIDO_COMPRA  = B.PEDIDO_COMPRA  
                                                                                                                                     
 WHERE A.NF_COMPRA = @NF_COMPRA  
   AND C.SALDO_PEDIDO > 0  
   AND ( ( A.ENCERRAR_PEDIDO_TOTAL = 'S' ) OR ( C.PEDIDO_ELETRONICO = 'S' ) )     
     
  
END  