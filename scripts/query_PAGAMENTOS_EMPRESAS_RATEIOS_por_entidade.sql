/*Adicionando par�metro por entidade!!
  Autor: Yno� Pedro 14/03/2018*/

DECLARE @PAGAMENTO_INI    DATETIME
DECLARE @PAGAMENTO_FIM    DATETIME                                
DECLARE @EMPRESA_INI      NUMERIC         
DECLARE @EMPRESA_FIM      NUMERIC
DECLARE @EMPRESA_CONTABIL NUMERIC
DECLARE @CLASSIF_INI      NUMERIC
DECLARE @CLASSIF_FIM      NUMERIC
DECLARE @OPCAO            VARCHAR(1)
DECLARE @FATOR            VARCHAR(1)
DECLARE @ESCRITURAVEL     VARCHAR(1)
DECLARE @ENTIDADE         NUMERIC     
                        
--SET     @PAGAMENTO_INI    = '01/02/2018' 
--SET     @PAGAMENTO_FIM    = '14/03/2018'
--SET     @EMPRESA_INI      = 1000         
--SET     @EMPRESA_FIM      = 1000 
--SET     @EMPRESA_CONTABIL = NULL 
--SET     @CLASSIF_INI      = NULL
--SET     @CLASSIF_FIM      = NULL 
--SET     @OPCAO            = 'T'
--SET     @FATOR            = 'T' 
--SET     @ESCRITURAVEL     = 'T' 
--SET     @ENTIDADE         = NULL

SET @PAGAMENTO_INI    = :PAGAMENTO_INI
SET @PAGAMENTO_FIM    = :PAGAMENTO_FIM
SET @EMPRESA_INI      = CASE WHEN :EMPRESA_INI      <> '' THEN :EMPRESA_INI      ELSE 1         END
SET @EMPRESA_FIM      = CASE WHEN :EMPRESA_FIM      <> '' THEN :EMPRESA_FIM      ELSE 999999999 END
SET @EMPRESA_CONTABIL = CASE WHEN :EMPRESA_CONTABIL <> '' THEN :EMPRESA_CONTABIL ELSE NULL      END
SET @CLASSIF_INI      = CASE WHEN :CLASSIF_INI      <> '' THEN :CLASSIF_INI      ELSE 1         END 
SET @CLASSIF_FIM      = CASE WHEN :CLASSIF_FIM      <> '' THEN :CLASSIF_FIM      ELSE 999999999 END 
SET @OPCAO            = CASE WHEN :OPCAO            <> '' THEN :OPCAO            ELSE 'T'       END        
SET @FATOR            = CASE WHEN :FATOR            <> '' THEN :FATOR            ELSE 'T'       END        
SET @ESCRITURAVEL     = CASE WHEN :ESCRITURAVEL     <> '' THEN :ESCRITURAVEL     ELSE 'T'       END       

IF ISNUMERIC(:ENTIDADE) = 1 SET @ENTIDADE        = :ENTIDADE        ELSE SET @ENTIDADE  = NULL



IF OBJECT_ID('TEMPDB..#FINAL'            ) IS NOT NULL DROP TABLE #FINAL

  -----------------------------------------------
  ---------- CONTAS A PAGAR COM RATEIO ----------
  -----------------------------------------------
  SELECT 
         CASE FORMULARIO_ORIGEM_TITULO
              WHEN 8      THEN 'C.PAGAR'
              WHEN 48     THEN 'NF COMP'
              WHEN 260069 THEN 'IMP.TIT'
              WHEN 61      THEN 'ANT.PAG'
              WHEN 145      THEN 'PREST.C'
              WHEN 177982 THEN 'NF.DEV'
              WHEN 242270 THEN 'F.COMIS'
              WHEN 278548 THEN 'BANRISUL'
              WHEN 344656 THEN 'FACTORY'
              WHEN 356592 THEN 'CANC.COB'
              WHEN 364670 THEN 'REN.PAG'
              WHEN 437952 THEN 'UNI.DUP'
              WHEN 444445 THEN 'COMIS.COR'
              WHEN 461408 THEN 'REDECARD'
              WHEN 476323 THEN 'C.EMPR'
              WHEN 479947 THEN 'BENEF.EXT'
              WHEN 484184 THEN 'MAN.CONTR'
              WHEN 538284 THEN 'CONH.COMP'
              WHEN 556935 THEN 'CANC.FACT'
              ELSE 'N.IDENT'   
         END                                           AS ORIGEM, --CONTAS PAGAR
         D.REG_MASTER_ORIGEM_TITULO                    AS REGISTRO,
         A.TITULO                                      AS TITULO,
         H.EMPRESA_CONTABIL                            AS EMPRESA_CONTABIL,
         H.EMPRESA_CONTABIL_NOME                       AS EMPRESA_CONTABIL_NOME,
         H.EMPRESA_USUARIA                             AS EMPRESA,
         H.EMPRESA_USUARIA_NOME                        AS EMPRESA_NOME,
         A.ENTIDADE                                    AS ENTIDADE,
         A.MOVIMENTO                                   AS MOVIMENTO,
         A.VENCIMENTO                                  AS VENCIMENTO,
         D.REG_MASTER_ORIGEM_PAGAMENTO                 AS PAGAMENTO,
         D.DATA_PAGAMENTO                              AS DATA_PAGAMENTO,
         B.CLASSIF_FINANCEIRA                          AS CLASSIF_FINANCEIRA,
         ISNULL(F.RATEIO,100)                          AS FATOR,
         A.VALOR                                       AS VALOR_ORIGINAL,
         SUM(ROUND(B.VALOR * (ISNULL(F.RATEIO,100)/100),2)*
         ( D.PAGAMENTO_TOTAL    / A.VALOR ))           AS VALOR,
         D.PAGAMENTO_TOTAL                             AS VALOR_PAGAMENTO,
         D.CONTA_BANCARIA                              AS CONTA_BANCARIA ,
         B.ESCRITURAVEL                                AS ESCRITURAVEL
    INTO #FINAL

    FROM TITULOS_PAGAR                A WITH(NOLOCK)
    JOIN TITULOS_PAGAR_CLASSIFICACOES B WITH(NOLOCK) ON B.TITULO_PAGAR      = A.TITULO_PAGAR     
    JOIN TITULOS_PAGAR_SALDO          C WITH(NOLOCK) ON C.TITULO_PAGAR       = A.TITULO_PAGAR
                                                    AND ISNULL(C.SITUACAO_TITULO,0) = 2     
    JOIN PAGAMENTOS                   D WITH(NOLOCK) ON D.TITULO_PAGAR       = A.TITULO_PAGAR  
    LEFT
    JOIN CONTAS_PAGAR                 E WITH(NOLOCK) ON E.FORMULARIO_ORIGEM = A.FORMULARIO_ORIGEM
                                                    AND E.TAB_MASTER_ORIGEM = A.TAB_MASTER_ORIGEM
                                                    AND E.CONTAS_PAGAR      = A.REG_MASTER_ORIGEM
    LEFT                                                                    
    JOIN CONTAS_PAGAR_RATEIOS         F WITH(NOLOCK) ON F.CONTAS_PAGAR       = E.CONTAS_PAGAR
    LEFT                                             
    JOIN PARAMETROS_FINANCEIRO        G WITH(NOLOCK) ON G.CLASSIF_COMERCIALIZAVEIS = B.CLASSIF_FINANCEIRA
                                                    AND G.GRUPO_EMPRESARIAL        = 1
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                      H              ON H.EMPRESA_USUARIA          = ISNULL(F.EMPRESA,A.EMPRESA)  
   WHERE  D.DATA_PAGAMENTO       >= @PAGAMENTO_INI                            AND
          D.DATA_PAGAMENTO       <= @PAGAMENTO_FIM                            AND       
         (B.CLASSIF_FINANCEIRA   >= @CLASSIF_INI   OR @CLASSIF_INI   IS NULL) AND
         (B.CLASSIF_FINANCEIRA   <= @CLASSIF_FIM   OR @CLASSIF_FIM   IS NULL) AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (G.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (G.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S'))


GROUP BY

CASE FORMULARIO_ORIGEM_TITULO
              WHEN 8      THEN 'C.PAGAR'
              WHEN 48     THEN 'NF COMP'
              WHEN 260069 THEN 'IMP.TIT'
              WHEN 61      THEN 'ANT.PAG'
              WHEN 145      THEN 'PREST.C'
              WHEN 177982 THEN 'NF.DEV'
              WHEN 242270 THEN 'F.COMIS'
              WHEN 278548 THEN 'BANRISUL'
              WHEN 344656 THEN 'FACTORY'
              WHEN 356592 THEN 'CANC.COB'
              WHEN 364670 THEN 'REN.PAG'
              WHEN 437952 THEN 'UNI.DUP'
              WHEN 444445 THEN 'COMIS.COR'
              WHEN 461408 THEN 'REDECARD'
              WHEN 476323 THEN 'C.EMPR'
              WHEN 479947 THEN 'BENEF.EXT'
              WHEN 484184 THEN 'MAN.CONTR'
              WHEN 538284 THEN 'CONH.COMP'
              WHEN 556935 THEN 'CANC.FACT'
              ELSE 'N.IDENT'   
         END                                 ,
         D.REG_MASTER_ORIGEM_TITULO          ,
         A.TITULO                            ,
         H.EMPRESA_CONTABIL                  ,
         H.EMPRESA_CONTABIL_NOME             ,
         H.EMPRESA_USUARIA                   ,
         H.EMPRESA_USUARIA_NOME              ,
         A.ENTIDADE                          ,
         A.MOVIMENTO                         ,
         A.VENCIMENTO                        ,
         D.REG_MASTER_ORIGEM_PAGAMENTO       ,
         D.DATA_PAGAMENTO                    ,
         B.CLASSIF_FINANCEIRA                ,
         ISNULL(F.RATEIO,100)                ,
         A.VALOR                             ,
         D.PAGAMENTO_TOTAL         ,
         D.CONTA_BANCARIA,
         B.ESCRITURAVEL
          
UNION ALL
  ------------------------------------
  ---------- COFRES DE LOJA ----------
  ------------------------------------
   SELECT
         'DESP.COFR'                                   AS ORIGEM, --CAIXAS LOJAS
         A.DESPESA_COFRE                               AS REGISTRO,
         NULL                                          AS TITULO,
         C.EMPRESA_CONTABIL                            AS EMPRESA_CONTABIL,
         C.EMPRESA_CONTABIL_NOME                       AS EMPRESA_CONTABIL_NOME,
         A.EMPRESA                                     AS EMPRESA,
         C.EMPRESA_USUARIA_NOME                        AS EMPRESA_NOME,
         A.ENTIDADE                                    AS ENTIDADE,
         A.MOVIMENTO                                   AS MOVIMENTO,
         NULL                                          AS VENCIMENTO,
         NULL                                          AS PAGAMENTO,
         A.MOVIMENTO                                   AS DATA_PAGAMENTO,
         A.CLASSIF_FINANCEIRA                          AS CLASSIF_FINANCEIRA,
         100                                           AS FATOR,
         A.DESPESA                                     AS VALOR_ORIGINAL,
         A.DESPESA                                     AS VALOR,
         A.DESPESA                                     AS VALOR_PAGAMENTO,
         NULL                                          AS CONTA_BANCARIA,
         'N'                                           AS ESCRITURAVEL

    FROM DESPESAS_COFRES        A WITH(NOLOCK)  
    LEFT                                             
    JOIN PARAMETROS_FINANCEIRO  B WITH(NOLOCK) ON B.CLASSIF_COMERCIALIZAVEIS = A.CLASSIF_FINANCEIRA
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                C              ON C.EMPRESA_USUARIA          = A.EMPRESA 
   WHERE  A.MOVIMENTO           >= @PAGAMENTO_INI                            AND
          A.MOVIMENTO           <= @PAGAMENTO_FIM                            AND       
         (A.CLASSIF_FINANCEIRA  >= @CLASSIF_INI   OR @CLASSIF_INI   IS NULL) AND
         (A.CLASSIF_FINANCEIRA  <= @CLASSIF_FIM   OR @CLASSIF_FIM   IS NULL) AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (B.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (B.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S'))
UNION ALL
  ---------------------------------------
  ---------- DESPESAS DE CAIXA ----------
  ---------------------------------------
   SELECT 
         'DESP.CAIXA'                                  AS ORIGEM, --DESPESAS CAIXA
         A.CAIXA_SAIDA                                 AS REGISTRO,
         NULL                                          AS TITULO,
         C.EMPRESA_CONTABIL                            AS EMPRESA_CONTABIL,
         C.EMPRESA_CONTABIL_NOME                       AS EMPRESA_CONTABIL_NOME,
         A.EMPRESA                                     AS EMPRESA,
         C.EMPRESA_USUARIA_NOME                        AS EMPRESA_NOME,
         A.ENTIDADE                                    AS ENTIDADE,
         A.MOVIMENTO                                   AS MOVIMENTO,
         NULL                                          AS VENCIMENTO,
         NULL                                          AS PAGAMENTO,
         A.MOVIMENTO                                   AS DATA_PAGAMENTO,
         A.CLASSIF_FINANCEIRA                          AS CLASSIF_FINANCEIRA,
         100                                           AS FATOR, 
         --ISNULL(F.RATEIO,100)                          AS FATOR, 
         A.VALOR                                       AS VALOR_ORIGINAL,
         ROUND(A.VALOR * (100/100),2)                  AS VALOR,
         ROUND(A.VALOR * (100/100),2)                  AS VALOR_PAGAMENTO,
         --ROUND(A.VALOR * (ISNULL(F.RATEIO,100)/100),2) AS VALOR,
         --ROUND(A.VALOR * (ISNULL(F.RATEIO,100)/100),2) AS VALOR_PAGAMENTO,
         NULL                                          AS CONTA_BANCARIA,
         'N'                                           AS ESCRITURAVEL

    FROM CAIXA_SAIDAS           A WITH(NOLOCK)  
    LEFT                                             
    JOIN PARAMETROS_FINANCEIRO  B WITH(NOLOCK) ON B.CLASSIF_COMERCIALIZAVEIS = A.CLASSIF_FINANCEIRA
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                C              ON C.EMPRESA_USUARIA          = A.EMPRESA 
    --LEFT                                                                    
    --JOIN CAIXA_SAIDAS_RATEIOS         F WITH(NOLOCK) ON F.CAIXA_SAIDA       = A.CAIXA_SAIDA
   WHERE  A.MOVIMENTO           >= @PAGAMENTO_INI                            AND
          A.MOVIMENTO           <= @PAGAMENTO_FIM                            AND
         (A.CLASSIF_FINANCEIRA  >= @CLASSIF_INI   OR @CLASSIF_INI   IS NULL) AND
         (A.CLASSIF_FINANCEIRA  <= @CLASSIF_FIM   OR @CLASSIF_FIM   IS NULL) AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (B.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (B.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S'))
UNION ALL
  ---------------------------------------
  ---------- PRESTA��ES DE CAIXA --------
  ---------------------------------------
   SELECT 
         'PREST.CONT'                                  AS ORIGEM, --PRESTACOES DE CAIXA
         A.CAIXA_PRESTACAO_CONTA                       AS REGISTRO,
         NULL                                          AS TITULO,
         D.EMPRESA_CONTABIL                            AS EMPRESA_CONTABIL,
         D.EMPRESA_CONTABIL_NOME                       AS EMPRESA_CONTABIL_NOME,
         A.EMPRESA                                     AS EMPRESA,
         D.EMPRESA_USUARIA_NOME                        AS EMPRESA_NOME,
         A.ENTIDADE                                    AS ENTIDADE,
         A.MOVIMENTO                                   AS MOVIMENTO,
         NULL                                          AS VENCIMENTO,
         NULL                                          AS PAGAMENTO,
         A.MOVIMENTO                                   AS DATA_PAGAMENTO,
         C.CLASSIF_FINANCEIRA                          AS CLASSIF_FINANCEIRA,
         100                                           AS FATOR,
         B.TOTAL_REEMBOLSO_CAIXA                       AS VALOR_ORIGINAL,
         CAST(B.TOTAL_REEMBOLSO_CAIXA * (SUM(C.VALOR) / B.TOTAL_DESPESAS) AS NUMERIC(15,2)) AS VALOR,
         CAST(B.TOTAL_REEMBOLSO_CAIXA * (SUM(C.VALOR) / B.TOTAL_DESPESAS) AS NUMERIC(15,2)) AS VALOR_PAGAMENTO,
         NULL                                          AS CONTA_BANCARIA,
         'N'                                           AS ESCRITURAVEL

    FROM CAIXA_PRESTACOES_CONTA       A WITH(NOLOCK)
    JOIN CAIXA_PRESTACOES_TOTAIS      B WITH(NOLOCK) ON B.CAIXA_PRESTACAO_CONTA = A.CAIXA_PRESTACAO_CONTA 
    JOIN CAIXA_PRESTACOES_DESPESAS    C WITH(NOLOCK) ON C.CAIXA_PRESTACAO_CONTA = A.CAIXA_PRESTACAO_CONTA 
    JOIN DBO.FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                      D              ON D.EMPRESA_USUARIA          = A.EMPRESA 
    JOIN ENTIDADES                    E WITH(NOLOCK) ON E.ENTIDADE                 = A.ENTIDADE
    JOIN CLASSIF_FINANCEIRAS          F WITH(NOLOCK) ON F.CLASSIF_FINANCEIRA       = C.CLASSIF_FINANCEIRA
    LEFT                                                                                                                                  
    JOIN PARAMETROS_FINANCEIRO        G WITH(NOLOCK) ON G.CLASSIF_COMERCIALIZAVEIS = C.CLASSIF_FINANCEIRA                 
   WHERE  A.MOVIMENTO       >= @PAGAMENTO_INI                            AND
          A.MOVIMENTO       <= @PAGAMENTO_FIM                            AND
         (C.CLASSIF_FINANCEIRA   >= @CLASSIF_INI   OR @CLASSIF_INI   IS NULL) AND
         (C.CLASSIF_FINANCEIRA   <= @CLASSIF_FIM   OR @CLASSIF_FIM   IS NULL) AND
         ((@OPCAO = 'T' OR @OPCAO IS NULL)                      OR
          (G.PARAMETRO_FINANCEIRO IS     NULL AND @OPCAO = 'N') OR
          (G.PARAMETRO_FINANCEIRO IS NOT NULL AND @OPCAO = 'S')) AND
          B.TOTAL_REEMBOLSO_CAIXA > 0
GROUP BY
         A.CAIXA_PRESTACAO_CONTA      
        ,D.EMPRESA_CONTABIL          
        ,D.EMPRESA_CONTABIL_NOME     
        ,A.EMPRESA                   
        ,D.EMPRESA_USUARIA_NOME      
        ,A.ENTIDADE                  
        ,A.MOVIMENTO                 
        ,C.CLASSIF_FINANCEIRA         
        ,B.TOTAL_REEMBOLSO_CAIXA     
        ,B.TOTAL_DESPESAS


     -- FINAL
   SELECT 
         CASE @OPCAO
              WHEN 'C' THEN 'Comercializ�veis'
              WHEN 'N' THEN 'N�o Comercializ�veis'
              ELSE          'Comerc./ N�o Comerc.'
         END                                                               AS OPCAO             ,
         CASE @FATOR
              WHEN 'S' THEN 'Somente Rateios'
              WHEN 'N' THEN 'Sem Rateios'
              ELSE          'Todos'
         END                                                               AS RATEIO            ,
         A.ORIGEM                                                          AS ORIGEM            ,
         A.REGISTRO                                                        AS REGISTRO          ,
         A.TITULO                                                          AS TITULO            ,
         A.EMPRESA                                                         AS EMPRESA           ,
         CONVERT(VARCHAR,A.EMPRESA)+'-'         + A.EMPRESA_NOME           AS EMPRESA_DESC      ,
         CONVERT(VARCHAR,A.EMPRESA_CONTABIL)+'-'+ A.EMPRESA_CONTABIL_NOME  AS EMPRESA_CONTABIL  ,
         CONVERT(VARCHAR,A.ENTIDADE)+'-'        + B.NOME                   AS ENTIDADE          ,
         A.MOVIMENTO                                                       AS MOVIMENTO         ,
         A.VENCIMENTO                                                      AS VENCIMENTO        ,
         A.PAGAMENTO                                                       AS PAGAMENTO         ,
         A.DATA_PAGAMENTO                                                  AS DATA_PAGAMENTO    ,
         CONVERT(VARCHAR,A.CLASSIF_FINANCEIRA)+'-'+
                 C.DESCRICAO                                               AS CLASSIF_FINANCEIRA,
         A.FATOR                                                           AS FATOR             ,
         A.VALOR_ORIGINAL                                                  AS VALOR_ORIGINAL    ,
         A.VALOR                                                           AS VALOR             ,
         A.VALOR_PAGAMENTO                                                 AS VALOR_PAGAMENTO,
         @PAGAMENTO_INI                                                    AS PAGAMENTO_INI    ,
         @PAGAMENTO_FIM                                                    AS PAGAMENTO_FIM    ,
         @EMPRESA_INI                                                      AS EMPRESA_INI      ,
         @EMPRESA_FIM                                                      AS EMPRESA_FIM      ,
         @EMPRESA_CONTABIL                                                 AS EMPRESA_CONTABIL ,
         @CLASSIF_INI                                                      AS CLASSIF_INI      ,
         @CLASSIF_FIM                                                      AS CLASSIF_FIM      ,
         @OPCAO                                                            AS OPCAO            ,
         @FATOR                                                            AS FATOR            ,
         A.CONTA_BANCARIA                                                  AS CONTA_BANCARIA   ,
ISNULL ( 'AG: ' + CAST( D.AGENCIA AS VARCHAR ) + ' ' + CAST( ISNULL(D.DIGITO_AG_CONTA,'') AS VARCHAR ) + ' / CONTA: ' + CAST( D.CONTA_CORRENTE AS VARCHAR ) + '-' + CAST( ISNULL(D.DIGITO_CONTA,'') AS VARCHAR ) , A.ORIGEM )
                                                                           AS DESCR_CONTA,
        ISNULL(@ESCRITURAVEL,'T' )                                         AS ESCRITURAVEL
   
   
    FROM #FINAL              A WITH(NOLOCK)
    JOIN ENTIDADES           B WITH(NOLOCK) ON B.ENTIDADE           = A.ENTIDADE
    JOIN CLASSIF_FINANCEIRAS C WITH(NOLOCK) ON C.CLASSIF_FINANCEIRA = A.CLASSIF_FINANCEIRA
    LEFT 
    JOIN CONTAS_BANCARIAS    D WITH(NOLOCK) ON D.CONTA_BANCARIA     = A.CONTA_BANCARIA

WHERE 1=1
AND (
   A.FATOR  = ( CASE WHEN @FATOR = 'N' THEN 100 ELSE 0 END )
OR A.FATOR  < ( CASE WHEN @FATOR = 'S' THEN 100 ELSE 0 END )
OR @FATOR = 'T' 
    )
AND (
   A.ESCRITURAVEL = @ESCRITURAVEL OR ISNULL(@ESCRITURAVEL,'T') = 'T'
    )
AND (
  A.ENTIDADE = @ENTIDADE OR @ENTIDADE IS NULL
  )


ORDER BY       
         --C.NOME,
         A.EMPRESA,   
         C.DESCRICAO,       
         EMPRESA_CONTABIL_NOME,  
         B.NOME,
         A.MOVIMENTO