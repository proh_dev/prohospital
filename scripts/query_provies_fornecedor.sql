DECLARE @ENTIDADE     NUMERIC(15) = 1001

--DECLARE @ENTIDADE     NUMERIC(15) = CASE WHEN ISNUMERIC(:ENTIDADE    ) =  0 THEN 0            ELSE :ENTIDADE       END


SELECT    X.TITULO             
        , X.TIPO               
        , X.EMPRESA            
        , X.NOME               
        , X.ENTIDADE               
        , X.CREDOR             
        , X.CLASSIF_FINANCEIRA 
        , X.DESCRICAO          
        , X.VENCIMENTO  
        , CAST(DATEPART(YEAR, X.VENCIMENTO) AS VARCHAR) + '/' + CAST(DATEPART(MONTH, X.VENCIMENTO)AS VARCHAR) AS ANO_MES
        , X.VALOR  
        , X.SALDO            
        , X.EMPRESA_CONTABIL    

FROM (                   


  SELECT   A.TITULO             AS TITULO
         , A.TIPO               AS TIPO
         , A.EMPRESA            AS EMPRESA
         , A.NOME               AS NOME
         , A.ENTIDADE           AS ENTIDADE
         , A.CREDOR             AS CREDOR
         , A.CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA
         , A.DESCRICAO          AS DESCRICAO
         , A.VENCIMENTO         AS VENCIMENTO
         , A.VALOR              AS VALOR
         , A.SALDO                AS SALDO
         , B.EMPRESA_CONTABIL    AS EMPRESA_CONTABIL

    FROM TITULOS_PAGAR_PREVISOES A WITH(NOLOCK)
    JOIN EMPRESAS_USUARIAS       B WITH(NOLOCK) ON B.EMPRESA_USUARIA = A.EMPRESA

   WHERE 1=1
     AND A.ENTIDADE  = @ENTIDADE
     AND A.TIPO = 'PREVIS�O' 
     AND A.SALDO > 0

UNION ALL


 SELECT    A.TITULO             AS TITULO
         , A.TIPO               AS TIPO
         , A.EMPRESA            AS EMPRESA
         , A.NOME               AS NOME
         , A.ENTIDADE           AS ENTIDADE
         , A.CREDOR             AS CREDOR
         , A.CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA
         , A.DESCRICAO          AS DESCRICAO
         , A.VENCIMENTO         AS VENCIMENTO
         , A.VALOR              AS VALOR
         , A.SALDO                AS SALDO
         , B.EMPRESA_CONTABIL    AS EMPRESA_CONTABIL

    FROM TITULOS_PAGAR_PREVISOES A WITH(NOLOCK)
    JOIN EMPRESAS_USUARIAS       B WITH(NOLOCK) ON B.EMPRESA_USUARIA = A.EMPRESA

   WHERE 1=1
     AND A.ENTIDADE  = @ENTIDADE
     AND A.TIPO = 'PROVIS�O' 
     AND A.SALDO > 0


) X

   ORDER BY X.VENCIMENTO 