--DROP PROCEDURE [dbo].[ENVIO_EMAIL_NF_SAIDA] 
--ALTER PROCEDURE [dbo].[ENVIO_EMAIL_NF_SAIDA] ( @NF_FATURAMENTO NUMERIC(15,0)) AS                      
--BEGIN                      
                
SET NOCOUNT ON      
--DECLARE @NF_FATURAMENTO NUMERIC(15, 0)= 43541
--DECLARE @ENTIDADE       NUMERIC(15, 0)= 9


DECLARE @EMPRESA                            VARCHAR(50)	
DECLARE @DESTINO                            VARCHAR(50)		                    
DECLARE @TITULO_EMAIL                       VARCHAR(50)  
DECLARE @tableHTML                          NVARCHAR(MAX)    
DECLARE @NF_NUMERO                          VARCHAR(50)  
DECLARE @EMAIL                              VARCHAR(250) 
DECLARE @ROMANEIO                           VARCHAR(250)



  
SELECT @TITULO_EMAIL = 'Nota Fiscal Faturada: Mercadoria para Loja  ' + CAST (A.ENTIDADE  AS VARCHAR (50)),
       @DESTINO                 = CAST (A.ENTIDADE       AS VARCHAR (50))  + ' - ' +  H.NOME,
       @EMPRESA                 = CAST (A.EMPRESA         AS VARCHAR (50)) + ' - ' + G.NOME,     
       @NF_NUMERO               = CAST (A.NF_NUMERO       AS VARCHAR (50)),
	   @ROMANEIO                = CAST (E.ESTOQUE_TRANSFERENCIA AS VARCHAR(250)),
       @EMAIL                   = 'pedro.mota@prohospital.com.br'--
      
   FROM NF_FATURAMENTO                  A WITH(NOLOCK)  
   JOIN PARAMETROS_ESTOQUE              B WITH(NOLOCK) ON A.EMPRESA               = B.EMPRESA_USUARIA  
   JOIN DEPOSITO_FATURAMENTOS           D WITH(NOLOCK) ON D.DEPOSITO_FATURAMENTO  = A.DEPOSITO_FATURAMENTO
   JOIN DEPOSITO_FATURAMENTOS_ITENS     E WITH(NOLOCK) ON E.DEPOSITO_FATURAMENTO  = D.DEPOSITO_FATURAMENTO
   LEFT
   JOIN USUARIOS                        F WITH(NOLOCK) ON F.USUARIO               = A.USUARIO_LOGADO
   JOIN EMPRESAS_USUARIAS               G WITH(NOLOCK) ON G.EMPRESA_USUARIA       = A.EMPRESA      
   JOIN ENTIDADES                       H WITH(NOLOCK) ON H.ENTIDADE              = A.ENTIDADE
  WHERE 1=1
		AND A.NF_FATURAMENTO = @NF_FATURAMENTO
		AND A.ENTIDADE <= 1000
		
  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;
              }  
              </style>'                    +  
    N'<H1 class="formato" >Nota Fiscal Faturada: Mercadoria para Loja             <br> <br>  
 Empresa de Destino:   <br>'+ @DESTINO                                      + '   <br> <br>  
 Remetente:<br>'            + @EMPRESA                                      + '   <br> <br> 
 Nota n�:  <br>'            + @NF_NUMERO                                    + '   <br> <br>  
 Romaneio: <br>'            + @ROMANEIO                                     + '   <br> <br>   
  
  </H1>'
  
BEGIN
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML';  
END
  
--END 