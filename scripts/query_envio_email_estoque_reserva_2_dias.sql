  SELECT 
       A.PRODUTO,    
       A.CENTRO_ESTOQUE,
       A.EMPRESA,      
       A.DOCUMENTO,                    
       A.TIPO,
       A.DATA,
       C.PEDIDO_VENDA,
       D.NOME AS CLIENTE,
       B.DESCRICAO,                           
       DATEDIFF(DAY,A.DATA,GETDATE()) AS DIAS_PENDENTES,
       A.RESERVA     

  FROM ESTOQUE_RESERVAS_TRANSACOES A WITH(NOLOCK)
  JOIN PRODUTOS                    B WITH(NOLOCK) ON B.PRODUTO      = A.PRODUTO
  LEFT
  JOIN PEDIDOS_VENDAS              C WITH(NOLOCK) ON C.PEDIDO_VENDA = A.PEDIDO_VENDA
  LEFT
  JOIN ENTIDADES                   D WITH(NOLOCK) ON D.ENTIDADE     = C.ENTIDADE

 WHERE 1=1
   AND A.CENTRO_ESTOQUE <  1000
   AND A.DATA           <= GETDATE()-2

ORDER BY 
       A.CENTRO_ESTOQUE,
       A.EMPRESA,   
       DATEDIFF(DAY,A.DATA,GETDATE()) DESC,
       A.PRODUTO,      
       A.DATA
                                                