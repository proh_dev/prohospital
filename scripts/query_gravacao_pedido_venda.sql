DECLARE @PEDIDO_COMPRA_ENCERRAMENTO NUMERIC =:PEDIDO_COMPRA_ENCERRAMENTO

-----------------------------------------------
-- ATUALIZAÇÃO DO SALDO DO PEDIDO            --
-----------------------------------------------

    DELETE PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES 
      FROM PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES  A WITH(NOLOCK)
      JOIN PEDIDOS_COMPRAS_ENCERRAMENTO         B WITH(NOLOCK) ON B.PEDIDO_COMPRA_ENCERRAMENTO = A.REG_MASTER_ORIGEM
                                                              AND B.TAB_MASTER_ORIGEM          = A.TAB_MASTER_ORIGEM
                                                              AND B.FORMULARIO_ORIGEM          = A.FORMULARIO_ORIGEM
     WHERE B.PEDIDO_COMPRA_ENCERRAMENTO = @PEDIDO_COMPRA_ENCERRAMENTO



-------------------------------------------------------
--GERACAO DA TABELA DE TRANSACOES DO PEDIDO DE COMPRA
-------------------------------------------------------

INSERT INTO PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES ( 
            REGISTRO_CONTROLE ,
            REGISTRO_CONTROLE_II ,
            FORMULARIO_ORIGEM ,
            TAB_MASTER_ORIGEM ,
            REG_MASTER_ORIGEM ,
            PEDIDO_COMPRA ,
            PEDIDO_COMPRA_PRODUTO,
            PRODUTO ,
            DATA,
            PEDIDO ,
            RECEBIMENTO ,
            CANCELAMENTO )


     SELECT B.PEDIDO_COMPRA_ENCERRAMENTO  AS REGISTRO_CONTROLE     ,
            1                             AS REGISTRO_CONTROLE_II  ,
            B.FORMULARIO_ORIGEM           AS FORMULARIO_ORIGEM     , 
            B.TAB_MASTER_ORIGEM           AS TAB_MASTER_ORIGEM     ,
            B.REG_MASTER_ORIGEM           AS REG_MASTER_ORIGEM     ,
            C.PEDIDO_COMPRA               AS PEDIDO_COMPRA         ,
            B.PEDIDO_COMPRA_PRODUTO       AS PEDIDO_COMPRA_PRODUTO ,    
            B.PRODUTO                     AS PRODUTO               ,
            CAST ( A.DATA_HORA AS DATE )  AS DATA                  ,
            0                             AS PEDIDO                ,
            0                             AS RECEBIMENTO           ,
            B.QTDE_CONFIRMADA             AS CANCELAMENTO            
                        
       FROM PEDIDOS_COMPRAS_ENCERRAMENTO          A WITH(NOLOCK), 
            PEDIDOS_COMPRAS_ENCERRAMENTO_PRODUTOS B WITH(NOLOCK),
            PEDIDOS_COMPRAS_PRODUTOS_SALDO        C WITH(NOLOCK)      
      WHERE A.PEDIDO_COMPRA_ENCERRAMENTO = B.PEDIDO_COMPRA_ENCERRAMENTO
        AND B.PRODUTO                    = C.PRODUTO
        AND B.PEDIDO_COMPRA_ENCERRAMENTO =@PEDIDO_COMPRA_ENCERRAMENTO
        AND C.SALDO                      > 0

  UNION ALL

     SELECT B.PEDIDO_COMPRA_ENCERRAMENTO  AS REGISTRO_CONTROLE     ,
            1                             AS REGISTRO_CONTROLE_II  ,
            B.FORMULARIO_ORIGEM           AS FORMULARIO_ORIGEM     , 
            B.TAB_MASTER_ORIGEM           AS TAB_MASTER_ORIGEM     ,
            B.REG_MASTER_ORIGEM           AS REG_MASTER_ORIGEM     ,          
            C.PEDIDO_COMPRA               AS PEDIDO_COMPRA         ,
            C.PEDIDO_COMPRA_PRODUTO       AS PEDIDO_COMPRA_PRODUTO ,    
            C.PRODUTO                     AS PRODUTO               ,
            CAST ( A.DATA_HORA AS DATE )  AS DATA                  ,
            0                             AS PEDIDO                ,
            0                             AS RECEBIMENTO           ,
            C.SALDO                       AS CANCELAMENTO                 
                        
       FROM PEDIDOS_COMPRAS_ENCERRAMENTO       A WITH(NOLOCK), 
            PEDIDOS_COMPRAS_ENCERRAMENTO_TOTAL B WITH(NOLOCK),
            PEDIDOS_COMPRAS_PRODUTOS_SALDO     C WITH(NOLOCK)      
      WHERE A.PEDIDO_COMPRA_ENCERRAMENTO = B.PEDIDO_COMPRA_ENCERRAMENTO
        AND B.PEDIDO_COMPRA              = C.PEDIDO_COMPRA
        AND B.PEDIDO_COMPRA_ENCERRAMENTO =@PEDIDO_COMPRA_ENCERRAMENTO
        AND C.SALDO                      > 0
