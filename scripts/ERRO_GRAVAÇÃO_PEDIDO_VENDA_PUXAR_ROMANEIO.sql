--DECLARE @REMANEJAMENTO_ESTOQUE_LOJA NUMERIC (15,0) = :FILTRO_1
--DECLARE @LOJA_DESTINO               NUMERIC (15,0) = :ENTIDADE
--DECLARE @EMPRESA_USUARIA            NUMERIC (15,0) = :EMPRESA
--DECLARE @PEDIDO_VENDA               NUMERIC (15,0) = :PEDIDO_VENDA
--DECLARE @TABELA_PRECO_MAXIMO        NUMERIC (15,0) = :TABELA_PRECO_MAXIMO
                                     


DECLARE @REMANEJAMENTO_ESTOQUE_LOJA NUMERIC (15,0) = 2706
DECLARE @LOJA_DESTINO               NUMERIC (15,0) = 9
DECLARE @EMPRESA_USUARIA            NUMERIC (15,0) = 1000
DECLARE @PEDIDO_VENDA               NUMERIC (15,0) = 22
DECLARE @TABELA_PRECO_MAXIMO        NUMERIC (15,0) = 10
          


DECLARE @OPERACAO_FISCAL   NUMERIC(5)     = (SELECT OPERACAO_FISCAL FROM TIPOS_PEDIDOS_VENDAS WITH(NOLOCK) WHERE TIPO_PEDIDO_VENDA = 1 )        
DECLARE @IPI_ICMS          VARCHAR(1)     = 'N'                

DELETE PEDIDOS_VENDAS_PRODUTOS_COMPLETA WHERE PEDIDO_VENDA = @PEDIDO_VENDA

INSERT INTO PEDIDOS_VENDAS_PRODUTOS_COMPLETA        
                                                      (        
                                                            FORMULARIO_ORIGEM,        
                                                            TAB_MASTER_ORIGEM,        
                                                            REG_MASTER_ORIGEM,        
                                                            REG_LOG_INCLUSAO,        
                                                            PEDIDO_VENDA,        
                                                            ORCAMENTO_PRODUTO,        
                                                            ORCAMENTO,        
                                                            EAN,        
                                                            REFERENCIA,        
                                                            PRODUTO,        
                                                            QUANTIDADE,        
                                                            UNIDADE_MEDIDA,        
                                                            OPERACAO_FISCAL,        
                                                            GRUPO_TRIBUTARIO,        
                                                            CLASSIF_FISCAL_CODIGO,        
                                                            CLASSIF_FISCAL_LETRA,        
                                                            VALOR_UNITARIO,        
                                                            COMISSAO,        
                                                            TIPO_DESCONTO,        
                                                            DESCONTO,        
                                                            TOTAL_DESCONTO,        
                                                            TOTAL_PRODUTO,        
                                                            OBJETO_CONTROLE,        
                                                            PESO_BRUTO,        
                                                            PESO_LIQUIDO,        
                                                            SITUACAO_TRIBUTARIA_IPI,        
                                                            IPI_BASE_ORIGINAL,        
                                                            IPI_REDUCAO_BASE,        
                                                            IPI_VALOR_REDUCAO,        
                                                            IPI_BASE_CALCULO,        
                                                            IPI_ALIQUOTA,        
                                                            IPI_VALOR,        
                                                            IPI_ICMS,        
                                                            IPI_DEVIDO,        
                                                            IPI_ISENTO,        
                                                            IPI_OUTROS,        
                                                            IPI_VALOR_CONTABIL,        
                                                            SITUACAO_TRIBUTARIA,        
                                                            ICMS_BASE_ORIGINAL,        
                                                            ICMS_REDUCAO_BASE,        
                                                            ICMS_VALOR_REDUCAO,        
                                                            ICMS_BASE_CALCULO,        
                                                            ICMS_ALIQUOTA,        
                                                            ICMS_VALOR,        
                                                            ICMS_DEVIDO,        
                                                            ICMS_ISENTO,        
                                                            ICMS_OUTROS,        
                                                            ICMS_VALOR_CONTABIL,        
                                                            ICMS_ST_IVA,        
                                                            ICMS_ST_IVA_AJUSTADO,        
                                                            ICMS_ST_BASE_CALCULO,        
                                                            ICMS_ST_ALIQUOTA,        
                                                            ICMS_ST_FATOR_REDUCAO,        
                                                            ICMS_ST_VALOR,        
                                                            ICMS_ST_RET_IVA,        
                                                            ICMS_ST_RET_IVA_AJUSTADO,        
                                                            ICMS_ST_RET_BASE_CALCULO,        
                                                            ICMS_ST_RET_ALIQUOTA,        
                                                            ICMS_ST_RET_FATOR_REDUCAO,        
                                                            ICMS_ST_RET_VALOR,        
                                                            SITUACAO_TRIBUTARIA_PIS,        
                                                            PIS_BASE_CALCULO,        
                                                            PIS_ALIQUOTA,        
                                                            PIS_VALOR,        
                                                            SITUACAO_TRIBUTARIA_COFINS,        
                                                            COFINS_BASE_CALCULO,        
                                                            COFINS_ALIQUOTA,        
                                                            COFINS_VALOR,        
                                                            RATEIO_FRETE,        
                                                            RATEIO_DESPESAS,        
                                                            RATEIO_SEGURO,        
                                                            RATEIO_ICMS_BASE_CALCULO,        
                                                            SALDO_ATUAL,        
                                                            QUANTIDADE_DIGITADA,        
                                                            VALOR_UNITARIO_BASE,        
                                                            INCLUIR_ICMS_ST_RET_VALOR,        
                                                            FRETE_GERENCIAL,        
                                                            TOTAL_FRETE_GERENCIAL,        
                                                            CUSTO_TOTAL_ITEM,        
                                                            TIPO_COMISSAO,        
                                                            RENTABILIDADE,        
                                                            CUSTO_PRODUTO,        
                                                            FATOR_EMBALAGEM,        
                                                            TOTAL_PRODUTO_SEM_DESCONTO,        
                                                            CUSTO_PRODUTO_TOTAL,        
                                                            COMISSAO_VALOR,        
                                                            VALOR_FRETE,        
                                                            TIPO_CUSTO,        
                                                            PRODUTO_SEM_ESTOQUE        
                                                      )        
        
        
SELECT         
        
B.FORMULARIO_ORIGEM                                         FORMULARIO_ORIGEM,        
B.TAB_MASTER_ORIGEM                                         TAB_MASTER_ORIGEM,        
A.REMANEJAMENTO_ESTOQUE_LOJA                                REG_MASTER_ORIGEM,        
A.REMANEJAMENTO_ESTOQUE_LOJA_QUANT                          REG_LOG_INCLUSAO,        
@PEDIDO_VENDA                                               PEDIDO_VENDA,        
NULL                                                        ORCAMENTO_PRODUTO,        
NULL                                                        ORCAMENTO,        
NULL                                                        EAN,        
NULL                                                        REFERENCIA,        
A.PRODUTO                                                   PRODUTO,        
A.QUANTIDADE                                                QUANTIDADE,        
P.UNIDADE_MEDIDA                                            UNIDADE_MEDIDA,        
12                                                          OPERACAO_FISCAL,        
E.GRUPO_TRIBUTARIO                                          GRUPO_TRIBUTARIO,        
P.NCM                                                       CLASSIF_FISCAL_CODIGO,        
NULL                                                        CLASSIF_FISCAL_LETRA,        
ISNULL(C.PRECO_MAXIMO,B.PRECO_VENDA)                        VALOR_UNITARIO,        
0.00                                                        COMISSAO,        
3                                                           TIPO_DESCONTO,        
0                                                           DESCONTO,        
0                                                           TOTAL_DESCONTO,        
B.PRECO_VENDA * A.QUANTIDADE                                TOTAL_PRODUTO,        
A.EMPRESA_ORIGEM                                            OBJETO_CONTROLE,        
0                                                           PESO_BRUTO,        
0                                                           PESO_LIQUIDO,        
E.SITUACAO_TRIBUTARIA_IPI                                   SITUACAO_TRIBUTARIA_IPI,        
E.IPI_BASE_ORIGINAL                                         IPI_BASE_ORIGINAL,        
E.IPI_REDUCAO_BASE                                          IPI_REDUCAO_BASE,        
E.IPI_VALOR_REDUCAO                                         IPI_VALOR_REDUCAO,        
E.IPI_BASE_CALCULO                                          IPI_BASE_CALCULO,        
E.IPI_ALIQUOTA                                              IPI_ALIQUOTA,        
E.IPI_VALOR                                                 IPI_VALOR,        
'N'                                                         IPI_ICMS,        
E.IPI_DEVIDO                                                IPI_DEVIDO,        
E.IPI_ISENTO                                                IPI_ISENTO,        
E.IPI_OUTROS                                                IPI_OUTROS,        
E.IPI_VALOR_CONTABIL                                        IPI_VALOR_CONTABIL,        
E.SITUACAO_TRIBUTARIA                                       SITUACAO_TRIBUTARIA,        
E.ICMS_BASE_ORIGINAL                                        ICMS_BASE_ORIGINAL,        
E.ICMS_REDUCAO_BASE                                         ICMS_REDUCAO_BASE,        
E.ICMS_VALOR_REDUCAO                                        ICMS_VALOR_REDUCAO,        
E.ICMS_BASE_CALCULO                                         ICMS_BASE_CALCULO,        
E.ICMS_ALIQUOTA                                             ICMS_ALIQUOTA,        
E.ICMS_VALOR                                                ICMS_VALOR,        
E.ICMS_DEVIDO                                               ICMS_DEVIDO,        
E.ICMS_ISENTO                                               ICMS_ISENTO,        
E.ICMS_OUTROS                                               ICMS_OUTROS,        
E.ICMS_VALOR_CONTABIL                                       ICMS_VALOR_CONTABIL,        
E.ICMS_ST_IVA                                               ICMS_ST_IVA,        
E.ICMS_ST_IVA_AJUSTADO                                      ICMS_ST_IVA_AJUSTADO,        
E.ICMS_ST_BASE_CALCULO                                      ICMS_ST_BASE_CALCULO,        
E.ICMS_ST_ALIQUOTA                                          ICMS_ST_ALIQUOTA,        
E.ICMS_ST_FATOR_REDUCAO                                     ICMS_ST_FATOR_REDUCAO,        
E.ICMS_ST_VALOR                                             ICMS_ST_VALOR,        
E.ICMS_ST_RET_IVA                                           ICMS_ST_RET_IVA,        
E.ICMS_ST_RET_IVA_AJUSTADO                                  ICMS_ST_RET_IVA_AJUSTADO,        
E.ICMS_ST_RET_BASE_CALCULO                                  ICMS_ST_RET_BASE_CALCULO,        
E.ICMS_ST_RET_ALIQUOTA                                      ICMS_ST_RET_ALIQUOTA,        
E.ICMS_ST_RET_FATOR_REDUCAO                                 ICMS_ST_RET_FATOR_REDUCAO,        
E.ICMS_ST_RET_VALOR                                         ICMS_ST_RET_VALOR,        
E.SITUACAO_TRIBUTARIA_PIS                                   SITUACAO_TRIBUTARIA_PIS,        
E.PIS_BASE_CALCULO                                          PIS_BASE_CALCULO,        
E.PIS_ALIQUOTA                                              PIS_ALIQUOTA,        
E.PIS_VALOR                                                 PIS_VALOR,        
E.SITUACAO_TRIBUTARIA_COFINS                                SITUACAO_TRIBUTARIA_COFINS,        
E.COFINS_BASE_CALCULO                                       COFINS_BASE_CALCULO,        
E.COFINS_ALIQUOTA                                           COFINS_ALIQUOTA,        
E.COFINS_VALOR                                              COFINS_VALOR,        
0                                                           RATEIO_FRETE,        
0                                                           RATEIO_DESPESAS,        
0                                                           RATEIO_SEGURO,        
0                                                           RATEIO_ICMS_BASE_CALCULO,        
NULL                                                        SALDO_ATUAL,        
A.QUANTIDADE                                                QUANTIDADE_DIGITADA,        
ISNULL(C.PRECO_MAXIMO,B.PRECO_VENDA)                        VALOR_UNITARIO_BASE,        
'N'                                                         INCLUIR_ICMS_ST_RET_VALOR,        
0                                                           FRETE_GERENCIAL,        
0                                                           TOTAL_FRETE_GERENCIAL,        
F.CUSTO_FINAL * A.QUANTIDADE                                CUSTO_TOTAL_ITEM,        
'P'                                                         TIPO_COMISSAO,        
/*( (ISNULL(C.PRECO_MAXIMO,B.PRECO_VENDA) * A.QUANTIDADE) * 100 / (CASE WHEN ISNULL(F.CUSTO_FINAL,0.00) <= 0 THEN 1 ELSE F.CUSTO_FINAL END)  ) - 100*/
ROUND( ((( ( ISNULL(C.PRECO_MAXIMO,B.PRECO_VENDA) * A.QUANTIDADE ) * 100) / ( F.CUSTO_FINAL * A.QUANTIDADE ) ) ) - 100.00, 2 )
                                                            RENTABILIDADE,       
F.CUSTO_FINAL                                               CUSTO_PRODUTO,        
P.FATOR_EMBALAGEM                                           FATOR_EMBALAGEM,        
(ISNULL(C.PRECO_MAXIMO,B.PRECO_VENDA) * A.QUANTIDADE)       TOTAL_PRODUTO_SEM_DESCONTO,        
F.CUSTO_FINAL * A.QUANTIDADE                                CUSTO_PRODUTO_TOTAL,        
0                                                           COMISSAO_VALOR,        
0                                                           VALOR_FRETE,        
'M'                                                         TIPO_CUSTO,        
'N'                                                         PRODUTO_SEM_ESTOQUE        
        
  FROM REMANEJAMENTOS_ESTOQUES_LOJAS_QUANTIDADES A WITH(NOLOCK)        
  JOIN PRODUTOS_VENDAS                           B WITH(NOLOCK) ON B.PRODUTO      = A.PRODUTO
                                                               AND B.GRUPO_PRECO  = 18          /* TABELA PRE�O PADR�O*/
  LEFT
  JOIN TABELAS_PRECOS_MAXIMO_PRODUTOS            C WITH(NOLOCK) ON C.PRODUTO              = A.PRODUTO               
                                                               AND C.TABELA_PRECO_MAXIMO  = @TABELA_PRECO_MAXIMO    
  JOIN PRODUTOS                                  P WITH(NOLOCK) ON P.PRODUTO              = A.PRODUTO        
  OUTER APPLY DBO.DADOS_FISCAIS_PRODUTOS         
    (         
        A.PRODUTO,        
        A.EMPRESA_ORIGEM,        
        A.EMPRESA_DESTINO,        
        @OPERACAO_FISCAL ,        
        (ISNULL(C.PRECO_MAXIMO,B.PRECO_VENDA) * A.QUANTIDADE),        
        @IPI_ICMS ,        
        A.QUANTIDADE,        
        0.00        
    ) E         
  OUTER APPLY DBO.RETORNA_CUSTO_PRODUTO_PEDIDO_VENDA
         
    (         
        A.PRODUTO,        
        A.EMPRESA_ORIGEM
    ) F
        
 WHERE 1=1        
  AND A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA
  AND A.EMPRESA_DESTINO            = @LOJA_DESTINO


/*--------------------------------------------------------------*/
/*--------------------------------------------------------------*/

DELETE PEDIDOS_VENDAS_TOTAIS WHERE PEDIDO_VENDA = @PEDIDO_VENDA        
        
INSERT INTO PEDIDOS_VENDAS_TOTAIS        
                                  (        
                                       PEDIDO_VENDA,        
                                       TOTAL_PRODUTOS,        
                                       TOTAL_IPI,        
                                       TOTAL_SERVICOS,        
                                       TOTAL_FRETE,        
                                       INCLUIR_FRETE_ICMS,        
                                       TOTAL_DESPESAS,        
                                       INCLUIR_DESPESA_ICMS,        
                                       TOTAL_SEGURO,        
                                       INCLUIR_SEGURO_ICMS,        
                                       TOTAL_GERAL,        
                                       ICMS_BASE_CALCULO,        
                                       ICMS_VALOR,        
                                       ICMS_ST_BASE_CALCULO,        
                                       ICMS_ST_VALOR,        
                                       SUB_TOTAL,        
                                       ICMS_FRETE,        
                                       ICMS_DESPESAS,        
                                       ICMS_SEGURO,        
                                       ICMS_BASE_CALCULO_PRODUTOS,        
                                       ICMS_VALOR_PRODUTOS,        
                                       RENTABILIDADE_TOTAL,        
                                       CUSTO_PRODUTO_TOTAL,        
                                       PIS_VALOR,        
                                       COFINS_VALOR,        
                                       TOTAL_COMISSAO,        
                                       TOTAL_ICMS_ST_RET_VALOR,       
                                       TOTAL_PRODUTO_SEM_DESCONTO,        
                                       TOTAL_FRETE_GERENCIAL        
                                  )        
        
SELECT        
                                       @PEDIDO_VENDA,        
                                       SUM(A.TOTAL_PRODUTO) AS TOTAL_PRODUTO,        
                                       0 TOTAL_IPI,        
                                       0 TOTAL_SERVICOS,        
                                       0 TOTAL_FRETE,        
                                       'N' INCLUIR_FRETE_ICMS,        
                                       0 TOTAL_DESPESAS,        
                                       'N' INCLUIR_DESPESA_ICMS,        
                                       0 TOTAL_SEGURO,        
                                       'N' INCLUIR_SEGURO_ICMS,        
                                       SUM(A.TOTAL_PRODUTO)             AS  TOTAL_GERAL,        
                                       SUM(A.ICMS_BASE_CALCULO        ) AS ICMS_BASE_CALCULO        ,        
                                       SUM(A.ICMS_VALOR               ) AS ICMS_VALOR               ,        
                                       SUM(A.ICMS_ST_BASE_CALCULO     ) AS ICMS_ST_BASE_CALCULO     ,        
                                       SUM(A.ICMS_ST_VALOR            ) AS ICMS_ST_VALOR            ,        
                                       SUM(A.TOTAL_PRODUTO            ) AS TOTAL_PRODUTO             ,        
                                       0 ICMS_FRETE,        
                                       0 ICMS_DESPESAS,        
                                       0 ICMS_SEGURO,        
                                       0 ICMS_BASE_CALCULO_PRODUTOS,        
                                       0 ICMS_VALOR_PRODUTOS,        
                                       ROUND( (((SUM(A.TOTAL_PRODUTO) * 100) / SUM(A.CUSTO_TOTAL_ITEM)) ) - 100.00, 2 )
                                                                    AS  RENTABILIDADE_TOTAL,        
                                       SUM(A.CUSTO_PRODUTO_TOTAL)   AS CUSTO_PRODUTO_TOTAL,        
                                       SUM(A.PIS_VALOR          )   AS PIS_VALOR          ,        
                                       SUM(A.COFINS_VALOR       )   AS COFINS_VALOR       ,        
                                       SUM(A.COMISSAO_VALOR     )   AS COMISSAO_VALOR     ,        
                                       0 TOTAL_ICMS_ST_RET_VALOR,        
                                       SUM(A.TOTAL_PRODUTO_SEM_DESCONTO) AS TOTAL_PRODUTO_SEM_DESCONTO,        
                                       SUM(A.TOTAL_FRETE_GERENCIAL     ) AS TOTAL_FRETE_GERENCIAL        
        
 FROM  PEDIDOS_VENDAS_PRODUTOS_COMPLETA      A WITH(NOLOCK)        
        
 WHERE 1=1        
  AND A.PEDIDO_VENDA = @PEDIDO_VENDA



:FILTRO_1(String[4])='2706' 
:ENTIDADE(Float)=9 
:EMPRESA(Float)=1000 
:PEDIDO_VENDA(Float)=22274 
:TABELA_PRECO_MAXIMO(Float)=10
Error: A instru��o foi finalizada.
N�o � poss�vel inserir uma linha de chave duplicada no objeto 'dbo.PEDIDOS_VENDAS_PRODUTOS_COMPLETA' com �ndice exclusivo 'UN_PEDIDOS_VENDAS_PRODUTOS_COMPLETA'. O valor de chave duplicada � (22274, 19238).
Error: A instru��o foi finalizada.
N�o � poss�vel inserir o valor NULL na coluna 'TOTAL_PRODUTOS', tabela 'PBS_PROHOSPITAL_DADOS.dbo.PEDIDOS_VENDAS_TOTAIS'; a coluna n�o permite nulos. Falha em INSERT.
UPDATE PBS_LOG_DADOS..EXECUCOES_SQL SET FIM = GETDATE(), MENSAGEM = 'A instru��o foi finalizada.
N�o � poss�vel inserir o valor NULL na coluna 'TOTAL_PRODUTOS', tabela 'PBS_PROHOSPITAL_DADOS.dbo.PEDIDOS_VENDAS_TOTAIS'; a coluna n�o permite nulos. Falha em INSERT.
A instru��o foi finalizada.
N�o � poss�vel inserir uma linha de chave duplicada no objeto 'dbo.PEDIDOS_VENDAS_PRODUTOS_COMPLETA' com �ndice exclusivo 'UN_PEDIDOS_VENDAS_PRODUTOS_COMPLETA'. O valor de chave duplicada � (22274, 19238).' WHERE REGISTRO = 3189564
Error: Sintaxe incorreta pr�xima a 'TOTAL_PRODUTOS'.
INSERT INTO PBS_LOG_DADOS..EXECUCOES_MENSAGENS ( CHAVE , MENSAGEM , USUARIO ) VALUES ( '1351324' , 'A instru��o foi finalizada.
N�o � poss�vel inserir o valor NULL na coluna 'TOTAL_PRODUTOS', tabela 'PBS_PROHOSPITAL_DADOS.dbo.PEDIDOS_VENDAS_TOTAIS'; a coluna n�o permite nulos. Falha em INSERT.
A instru��o foi finalizada.
N�o � poss�vel inserir uma linha de chave duplicada no objeto 'dbo.PEDIDOS_VENDAS_PRODUTOS_COMPLETA' com �ndice exclusivo 'UN_PEDIDOS_VENDAS_PRODUTOS_COMPLETA'. O valor de chave duplicada � (22274, 19238). Log SQL:3189564' , 'ypedro' )
Error: Sintaxe incorreta pr�xima a 'TOTAL_PRODUTOS'.