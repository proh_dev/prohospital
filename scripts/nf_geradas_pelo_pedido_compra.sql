---------------------------------------------------------
--      CONSULTA DE NOTAS GERADAS POR PEDIDO           --
--      DESENVOLVIDA POR YNOA PEDRO                    --
--      27/08/2018                                     --
---------------------------------------------------------

DECLARE @PEDIDO_COMPRA NUMERIC


--SET @PEDIDO_COMPRA = 18338
SET @PEDIDO_COMPRA = :PEDIDO_COMPRA

SELECT 48                                  AS FORMULARIO_ORIGEM
      ,41                                  AS TAB_MASTER_ORIGEM
	  ,A.NF_COMPRA                         AS REG_MASTER_ORIGEM
      ,A.NF_COMPRA                         AS NF_COMPRA
	  ,A.PEDIDO_COMPRA                     AS PEDIDO_COMPRA
	  ,A.EMPRESA                           AS COD_EMPRESA
	  ,C.NOME                              AS EMPRESA
	  ,A.ENTIDADE                          AS COD_ENTIDADE
	  ,D.NOME                              AS ENTIDADE
	  ,A.NF_NUMERO                         AS NF_NUMERO
	  ,A.MOVIMENTO                         AS MOVIMENTO
	  ,A.RECEBIMENTO                       AS RECEBIMENTO
	  ,A.CHAVE_NFE                         AS CHAVE
	  ,A.TOTAL_GERAL                       AS TOTAL_PEDIDO


FROM NF_COMPRA                             A WITH(NOLOCK)
JOIN PEDIDOS_COMPRAS                       B WITH(NOLOCK) ON A.PEDIDO_COMPRA  = B.PEDIDO_COMPRA
JOIN EMPRESAS_USUARIAS                     C WITH(NOLOCK) ON A.EMPRESA        = C.EMPRESA_USUARIA
JOIN ENTIDADES                             D WITH(NOLOCK) ON A.ENTIDADE       = D.ENTIDADE


WHERE A.PEDIDO_COMPRA = @PEDIDO_COMPRA

