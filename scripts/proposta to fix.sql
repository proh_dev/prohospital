DECLARE @PROPOSTA                   NUMERIC(15,0) = 11336--:PROPOSTA
DECLARE @PROPOSTA_LOTE_UNICO        VARCHAR(1)
DECLARE @TAMANHO_DESCRICAO_DOTACAO  NUMERIC(15,0)
DECLARE @TAMANHO_SUBITEM            NUMERIC(15,0)
                         
         
                      
SELECT @PROPOSTA_LOTE_UNICO = A.LOTE      
  FROM PROPOSTAS_LOTES A WITH(NOLOCK)      
 WHERE 1=1                     
  AND A.PROPOSTA = @PROPOSTA                        
  AND A.LOTE     = 'U'   
                           
SET @TAMANHO_DESCRICAO_DOTACAO = 30
                                       
SELECT @TAMANHO_DESCRICAO_DOTACAO = MAX(LEN(C1.DESCRICAO))
         FROM PROPOSTAS_DOTACOES AS C1                                  
        WHERE 1=1
        AND C1.REG_MASTER_ORIGEM = @PROPOSTA        

        
SELECT @TAMANHO_SUBITEM = MAX(LEN(A.SUB_ITEM))
         FROM PROPOSTAS_ITENS_LOTES_DETALHES AS A                                 
        WHERE 1=1
        AND A.PROPOSTA = @PROPOSTA                               
                                                    
                                                                                            
SELECT 
A.PROPOSTA                                                                                                                              AS PROPOSTA,
G.EMPRESA_USUARIA                                                                                                                       AS EMPRESA_COD                ,
G.NOME                                                                                                                                  AS EMPRESA                    ,
H.TIPO_ENDERECO + ' ' + H.ENDERECO + ', ' + CAST( H.NUMERO AS VARCHAR ) + ' - ' + H.BAIRRO                                              AS ENDERECO                   ,
'CEP: ' + H.CEP + ' - ' + H.CIDADE + '/' + H.ESTADO                                                                                     AS ENDERECO_2                 ,
'('+ CAST(I.DDD AS VARCHAR) +') '+ SUBSTRING ( CAST ( I.NUMERO AS VARCHAR ),1,4) + '-' + SUBSTRING ( CAST ( I.NUMERO AS VARCHAR ),5,4)  AS TELEFONE                   ,
CAST(D.CLIENTE AS VARCHAR) + '-' + F.NOME                                                                                               AS CLIENTE                    ,
D.ATT                                                                                                                                   AS ATENCIOSAMENTE             ,
D.DESCRICAO                                                                                                                             AS REFERENTE                  ,
J.TIPO_ENDERECO + ' ' + J.ENDERECO + ', ' + CAST( J.NUMERO AS VARCHAR ) + ' - ' + J.BAIRRO                                              AS ENDERECO_CLIENTE           ,
'CEP: ' + J.CEP + ' - ' + J.CIDADE + '/' + J.ESTADO                                                                                     AS ENDERECO_CLIENTE_2         ,
CAST(D.OBJETO AS VARCHAR(MAX) )                                                                                                         AS OBJETO                     ,
D.DOCUMENTO                                                                                                                             AS DOCUMENTO                  ,
CASE WHEN D.PROPOSTA_TIPO IN (5,6,8) OR ISNUMERIC(DBO.NUMERICO(D.PROPOSTA_TIPO_NUMERO)) = 1 THEN '' ELSE 'Documento:' END               AS DOCUMENTO_1                ,
CASE WHEN D.PROPOSTA_TIPO IN (5,6,8) OR ISNUMERIC(DBO.NUMERICO(D.PROPOSTA_TIPO_NUMERO)) = 1 THEN '' ELSE D.DOCUMENTO  END               AS DOCUMENTO_2                ,
D.MOVIMENTO                                                                                                                             AS MOVIMENTO                  ,
D.PREGAO                                                                                                                                AS PREGAO                     ,
'Dt Abert: ' + CONVERT(VARCHAR,K1.DATA,103)                                                                                             AS DATA                       ,
'Hora: ' + SUBSTRING(CAST(DBO.ZEROS(D.HORA,4) AS VARCHAR(50)),1,2) + ':' + SUBSTRING(CAST(DBO.ZEROS(D.HORA,4) AS VARCHAR),3,2)          AS HORA                       ,
'Preg�o: ' + CAST(K.PREGAO_NUMERO AS VARCHAR)                                                                                           AS PREGAO_NUMERO              ,
CAST(R.DESCRICAO AS VARCHAR)                                                                                                            AS PROPOSTA_TIPO              ,
CASE WHEN ISNUMERIC(DBO.NUMERICO(D.PROPOSTA_TIPO_NUMERO)) = 1 THEN 'N� Doc: ' + CAST(D.PROPOSTA_TIPO_NUMERO AS VARCHAR) ELSE '' END     AS PROPOSTA_TIPO_NUMERO       ,
D.VALIDADE                                                                                                                              AS VALIDADE                   ,
D.PRAZO_ENTREGA                                                                                                                         AS PRAZO_ENTREGA              ,
D.CONDICAO_PAGAMENTO                                                                                                                    AS CONDICAO_PAGAMENTO         ,
L.NOME                                                                                                                                  AS REPRESENTANTE_LEGAL        ,
UPPER(E.DESCRICAO)                                                                                                                      AS DESCRICAO                  ,
CASE 
  WHEN D.EXIBIR_SOMENTE_SUBITEM = 'S' THEN '' ELSE CAST(DBO.ZEROS(A.ITEM,@TAMANHO_SUBITEM) AS VARCHAR) END                              AS ITEM                       ,
DBO.ZEROS(A.SUB_ITEM,@TAMANHO_SUBITEM)                                                                                                  AS SUB_ITEM                   ,
UPPER(REPLACE(REPLACE(CAST(A.DESCRICAO AS VARCHAR(MAX) ),CHAR(13) + CHAR(10) ,' ')  ,'�',' ') )
+ CASE WHEN LEN(A.REGISTRO_ANVISA) <> 0 THEN CHAR(13) + 'Registro Anvisa: ' + A.REGISTRO_ANVISA ELSE '' END
+ CHAR(13) + CHAR(10)
-- Fun��o para converter as dota��es em um peda�o da linha da descri��o do produto ----------------------------------------------------------------------------------------------------------------------------
+ COALESCE(
(SELECT '-Dota��o:' + DBO.ESPACOS_CHAR(C1.DESCRICAO_DOTACAO, @TAMANHO_DESCRICAO_DOTACAO) + ' >> Qtde = ' + 
-- Fun��o para converter n�mero em varchar no formato brasileiro --
   DBO.ESPACOS_CHAR(SUBSTRING ( REPLACE(CONVERT(VARCHAR,CAST(C1.QUANTIDADE AS MONEY),1),',','.') ,1 ,LEN(REPLACE(CONVERT(VARCHAR,CAST(C1.QUANTIDADE AS MONEY),1),',','.'))-3 ), 7)                     --+
--   REPLACE ( SUBSTRING ( REPLACE(CONVERT(VARCHAR,CAST(C1.QUANTIDADE AS MONEY),1),',','.') ,LEN(REPLACE(CONVERT(VARCHAR,CAST(C1.QUANTIDADE AS MONEY),1),',','.'))-2 ,3 ) ,'.',',')
--                                                              
                                                             + CHAR(13) AS [text()]
         FROM PROPOSTAS_ITENS_LOTES_DETALHES_SUB_01 AS C1 WITH(NOLOCK)
         LEFT 
         JOIN PROPOSTAS_DOTACOES                    AS C2 WITH(NOLOCK) ON C2.DESCRICAO = C1.DESCRICAO_DOTACAO
                                                                      AND C2.PROPOSTA  = @PROPOSTA
        WHERE 1=1
        AND C1.PROPOSTA_ITEM_LOTE_DETALHE = A.PROPOSTA_ITEM_LOTE_DETALHE
         ORDER BY C2.PROPOSTA_DOTACAO, C1.PROPOSTA_ITEM_LOTE_DETALHE
         FOR XML PATH(''), TYPE).value('.[1]', 'VARCHAR(MAX)'), '') 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
+ CASE 
    WHEN D.VALOR_EXTENSO_UNITARIO = 'S' THEN + CHAR(13) + 'R$ Unit Item: ' + UPPER(DBO.FN_NUMERO_POR_EXTENSO(A.PRECO)) 
    ELSE '' 
  END
+ CASE 
    WHEN D.VALOR_EXTENSO_UNITARIO = 'S' THEN + CHAR(13) + 'R$ Total Item: ' + UPPER(DBO.FN_NUMERO_POR_EXTENSO(A.VALOR_TOTAL)) 
    ELSE '' 
  END
                                                                                                                                        AS DESCRICAO                  ,
A.QUANTIDADE_CONFIRMADA                                                                                                                 AS QUANTIDADE                 ,
--C1.DESCRICAO                                                                                                                            AS DOTACAO_DESCRICAO,
A.UNIDADE_MEDIDA                                                                                                                        AS UNIDADE_MEDIDA             ,
Z.PRECO_CONFIRMADO                                                                                                                      AS PRECO                      ,
CAST(A.FABRICANTE AS VARCHAR(MAX))                                                                                                      AS FABRICANTE                 ,
Z.VALOR_TOTAL_CONFIRMADO                                                                                                                AS VALOR_TOTAL                ,
L.INSCRICAO_FEDERAL                                                                                                                     AS INSCRICAO_FEDERAL          ,
CASE WHEN D.VALOR_EXTENSO_LOTE     = 'S' THEN 'Total Lote:'                                        ELSE '' END                          AS CAMPO_TOTAL_LOTE           ,
CASE WHEN D.VALOR_EXTENSO_LOTE     = 'S' THEN 'R$ Total Lote:'                                     ELSE '' END                          AS CAMPO_TOTAL_LOTE_RS        ,
CASE WHEN D.VALOR_EXTENSO_UNITARIO = 'S' THEN UPPER(DBO.FN_NUMERO_POR_EXTENSO(Z.VALOR_TOTAL_CONFIRMADO))                    ELSE '' END AS TOTAL_UNITARIO_EXTENSO     ,
CASE WHEN D.VALOR_EXTENSO_UNITARIO = 'S' THEN UPPER(DBO.FN_NUMERO_POR_EXTENSO(Z.PRECO_CONFIRMADO))                          ELSE '' END AS TOTAL_PRECO_EXTENSO        ,
CASE WHEN D.VALOR_EXTENSO_LOTE     = 'S' THEN M.TOTAL_LOTE_EXTENSO                                 ELSE '' END                          AS TOTAL_LOTE_EXTENSO         ,
CASE 
   WHEN D.VALOR_EXTENSO_LOTE     = 'S' THEN
   SUBSTRING ( REPLACE(CONVERT(VARCHAR,CAST(M.VALOR_TOTAL_LOTE AS MONEY),1),',','.') ,1 ,LEN(REPLACE(CONVERT(VARCHAR,CAST(M.VALOR_TOTAL_LOTE AS MONEY),1),',','.'))-3 )                       +
   REPLACE ( SUBSTRING ( REPLACE(CONVERT(VARCHAR,CAST(M.VALOR_TOTAL_LOTE AS MONEY),1),',','.') ,LEN(REPLACE(CONVERT(VARCHAR,CAST(M.VALOR_TOTAL_LOTE AS MONEY),1),',','.'))-2 ,3 ) ,'.',',')
   ELSE '' 
END                                                                                                                                     AS TOTAL_LOTE                 ,
CASE WHEN D.VALOR_EXTENSO_GLOBAL   = 'S' THEN N.TOTAL_EXTENSO                                      ELSE '' END                          AS TOTAL_EXTENSO              ,
cast(replace(cast(Q.DECLARACAO_RODAPE as nvarchar(max)),'<p>','Preg�o: '+ISNULL(K.PREGAO_NUMERO,'')) as ntext)                          AS RODAPE                     ,
CASE WHEN D.PAPEL_TIMBRADO = 'S' THEN CAST(U.RODAPE AS ntext)   END                                                                     AS RODAPE_EMPRESA

,A.PROPOSTA_ITEM_LOTE_DETALHE

  FROM PROPOSTAS_ITENS_LOTES_DETALHES         A WITH(NOLOCK)
  LEFT
  JOIN PROPOSTAS_ITENS_LOTES                  B WITH(NOLOCK) ON B.PROPOSTA_ITEM_LOTE         = A.PROPOSTA_ITEM_LOTE
  JOIN PROPOSTAS                              D WITH(NOLOCK) ON D.PROPOSTA                   = A.PROPOSTA
  LEFT
  JOIN PROPOSTAS_LOTES                        E WITH(NOLOCK) ON E.LOTE                       = A.ITEM
                                                            AND E.PROPOSTA                   = D.PROPOSTA
  LEFT
  JOIN ENTIDADES                              F WITH(NOLOCK) ON F.ENTIDADE                   = D.CLIENTE
  LEFT
  JOIN EMPRESAS_USUARIAS                      G WITH(NOLOCK) ON G.EMPRESA_USUARIA            = D.EMPRESA_USUARIA
  LEFT
  JOIN ENDERECOS                              H WITH(NOLOCK) ON H.ENTIDADE                   = G.EMPRESA_USUARIA
  LEFT
  JOIN TELEFONES                              I WITH(NOLOCK) ON I.ENTIDADE                   = G.EMPRESA_USUARIA
                                                            AND I.PADRAO                     = 'S'
  LEFT
  JOIN ENDERECOS                              J WITH(NOLOCK) ON J.ENTIDADE                   = F.ENTIDADE
  LEFT
  JOIN PREGOES                                K WITH(NOLOCK) ON K.PREGAO                     = D.PREGAO
  LEFT
  JOIN PREGOES_EVENTOS                       K1 WITH(NOLOCK) ON K1.PREGAO                     = K.PREGAO
                                                            AND K1.PREGAO_TIPO_EVENTO         = 1 -- EVENTOS DE ABERTURA DE PREG�O
  LEFT
  JOIN ENTIDADES                              L WITH(NOLOCK) ON L.ENTIDADE                   = D.REPRESENTANTE_LEGAL
-- PEGAR EXTENSO DO VALOR POR LOTE -------------------------------------------------------------------------------------------------------------------------------------
  LEFT
  JOIN (
          SELECT ISNULL(B.PROPOSTA,A.PROPOSTA) PROPOSTA, A.PROPOSTA_ITEM_LOTE, A.ITEM, SUM(Z.VALOR_TOTAL_CONFIRMADO) AS VALOR_TOTAL_LOTE, UPPER(DBO.fn_numero_por_extenso(SUM(A.VALOR_TOTAL_CONFIRMADO))) AS TOTAL_LOTE_EXTENSO
            FROM PROPOSTAS_ITENS_LOTES_DETALHES        A WITH(NOLOCK)
            LEFT
            JOIN PROPOSTAS_ITENS_LOTES                 B WITH(NOLOCK) ON B.PROPOSTA_ITEM_LOTE         = A.PROPOSTA_ITEM_LOTE
            JOIN PROPOSTAS_FECHAMENTOS_ITENS           Z WITH(NOLOCK) ON Z.PROPOSTA_ITEM_LOTE_DETALHE = A.PROPOSTA_ITEM_LOTE_DETALHE
            WHERE 1=1
             AND ISNULL(B.PROPOSTA,A.PROPOSTA) = @PROPOSTA
           GROUP BY ISNULL(B.PROPOSTA,A.PROPOSTA), A.PROPOSTA_ITEM_LOTE, A.ITEM
       )                                     M              ON M.PROPOSTA                   = ISNULL(B.PROPOSTA,A.PROPOSTA)
                                                           AND M.ITEM                       = A.ITEM
-- PEGAR EXTENSO DO VALOR TOTAL -----------------------------------------------------------------------------------------------------------------------------------------
  LEFT
  JOIN (
          SELECT ISNULL(B.PROPOSTA,A.PROPOSTA) PROPOSTA, SUM(A.VALOR_TOTAL_CONFIRMADO) AS VALOR_TOTAL, UPPER(DBO.fn_numero_por_extenso(SUM(A.VALOR_TOTAL_CONFIRMADO))) AS TOTAL_EXTENSO
            FROM PROPOSTAS_ITENS_LOTES_DETALHES        A WITH(NOLOCK)
            LEFT
            JOIN PROPOSTAS_ITENS_LOTES                 B WITH(NOLOCK) ON B.PROPOSTA_ITEM_LOTE         = A.PROPOSTA_ITEM_LOTE
            JOIN PROPOSTAS_FECHAMENTOS_ITENS           Z WITH(NOLOCK) ON Z.PROPOSTA_ITEM_LOTE_DETALHE = A.PROPOSTA_ITEM_LOTE_DETALHE
            WHERE 1=1
             AND ISNULL(B.PROPOSTA,A.PROPOSTA) = @PROPOSTA
           GROUP BY ISNULL(B.PROPOSTA,A.PROPOSTA)
       )                                     N              ON N.PROPOSTA                   = ISNULL(B.PROPOSTA,A.PROPOSTA)
  LEFT
  JOIN PROPOSTAS_RODAPE                      P WITH(NOLOCK) ON P.PROPOSTA                   = D.PROPOSTA
  LEFT
  JOIN PROPOSTAS_DECLARACOES_FINAIS          Q WITH(NOLOCK) ON Q.PROPOSTA_DECLARACAO_FINAL  = P.PROPOSTA_DECLARACAO_FINAL
  LEFT
  JOIN PROPOSTAS_TIPOS                       R WITH(NOLOCK) ON R.PROPOSTA_TIPO              = D.PROPOSTA_TIPO
  LEFT
  JOIN EMPRESAS_USUARIAS                     U WITH(NOLOCK) ON U.EMPRESA_USUARIA            = D.EMPRESA_USUARIA
  JOIN PROPOSTAS_FECHAMENTOS_ITENS           Z WITH(NOLOCK) ON Z.PROPOSTA_ITEM_LOTE_DETALHE = A.PROPOSTA_ITEM_LOTE_DETALHE                                
                                
  WHERE 1=1                              
   AND A.PROPOSTA = @PROPOSTA

ORDER BY A.PROPOSTA_ITEM_LOTE_DETALHE--, DBO.ZEROS(ISNULL(A.ITEM,0),3), DBO.ZEROS(SUB_ITEM,4)