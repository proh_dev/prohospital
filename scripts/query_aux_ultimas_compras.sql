DECLARE @ENTIDADE   NUMERIC   

                          
                                                                
--SET @ENTIDADE        = 95252

--SET @DATAINI         = :DATA_INICIAL
--SET @DATAFIM         = :DATA_FINAL

IF ISNUMERIC(:ENTIDADE) = 1 
    SET @ENTIDADE     = :ENTIDADE         
  ELSE 
    SET @ENTIDADE     = NULL


                              
SELECT
    A.EMPRESA                                                                                   AS EMPRESA        ,  
    W.NOME                                                                                      AS N_EMPRESA      ,
    A.PRODUTO                                                                                   AS PRODUTO        ,
    P.DESCRICAO                                                                                 AS DESCRICAO      ,     
    P.UNIDADE_MEDIDA                                                                            AS UNIDADE_MEDIDA ,
	SUM( A.QUANTIDADE )                                                                         AS QUANTIDADE     ,
	( A.VENDA_LIQUIDA/
	A.QUANTIDADE      )                                                                         AS VALOR_UN       ,                                
    SUM( A.VENDA_LIQUIDA )                                                                      AS VENDA_LIQUIDA  ,
    A.OPERACAO_FISCAL                                                                           AS OPERACAO_FISCAL,
    E.DESCRICAO                                                                                 AS MARCA          ,
    A.MOVIMENTO 

 FROM VENDAS_ANALITICAS                   A WITH(NOLOCK) 
 LEFT							          
 JOIN ENTIDADES                           B WITH(NOLOCK) ON B.ENTIDADE              = A.CLIENTE
 LEFT							          
 JOIN CLASSIFICACOES_CLIENTES             C WITH(NOLOCK) ON C.CLASSIFICACAO_CLIENTE = B.CLASSIFICACAO_CLIENTE
 LEFT							          
 JOIN VENDEDORES                          D WITH(NOLOCK) ON D.VENDEDOR              = A.VENDEDOR
 LEFT							          
 JOIN EMPRESAS_USUARIAS                   W WITH(NOLOCK) ON W.EMPRESA_USUARIA       = A.EMPRESA
 JOIN PRODUTOS                            P WITH(NOLOCK) ON P.PRODUTO               = A.PRODUTO
 JOIN MARCAS                              E WITH(NOLOCK) ON E.MARCA                 = P.MARCA


WHERE 1=1                      
    AND ( A.CLIENTE              = @ENTIDADE         OR @ENTIDADE IS NULL )
GROUP BY 
A.EMPRESA                                                                                 ,
A.PRODUTO                                                                                 ,
P.DESCRICAO                                                                               ,
P.UNIDADE_MEDIDA                                                                          ,
A.OPERACAO_FISCAL,
E.DESCRICAO,
A.MOVIMENTO,
W.NOME,
A.QUANTIDADE,
A.VENDA_LIQUIDA
  HAVING SUM( A.VENDA_LIQUIDA ) <> 0
                    
ORDER BY 
A.MOVIMENTO  DESC                                                                                        