DECLARE @DATAINI           DATETIME
DECLARE @DATAFIM           DATETIME
DECLARE @ENTIDADE          NUMERIC   
DECLARE @EMPRESA_INI       NUMERIC (15,0)
DECLARE @EMPRESA_FIM       NUMERIC (15,0)
DECLARE @PRODUTO           NUMERIC

                          
                                                                
--SET @DATAINI         = '01/01/2017'         
--SET @DATAFIM         = '31/01/2018'
--SET @ENTIDADE        = 133007 
--SET @EMPRESA_INI     = NULL
--SET @EMPRESA_FIM     = NULL
--SET @PRODUTO         = 19238    

SET @DATAINI                                                                                           = :DATA_INICIAL
SET @DATAFIM                                                                                           = :DATA_FINAL

IF ISNUMERIC(:ENTIDADE) = 1 
    SET @ENTIDADE         = :ENTIDADE         
  ELSE 
    SET @ENTIDADE     = NULL

IF ISNUMERIC(:EMPRESA_INI) = 1         
    SET @EMPRESA_INI = :EMPRESA_INI              
  ELSE  
    SET @EMPRESA_INI = NULL       

IF ISNUMERIC(:EMPRESA_FIM) = 1 
    SET @EMPRESA_FIM = :EMPRESA_FIM
 ELSE  
    SET @EMPRESA_FIM = NULL

IF ISNUMERIC(:PRODUTO) = 1
    SET @PRODUTO = :PRODUTO
ELSE
    SET @PRODUTO = NULL
                              
SELECT  
    A.EMPRESA                                                                                   AS EMPRESA        ,
    A.CLIENTE                                                                                   AS COD_CLIENTE    ,
    W.NOME                                                                                      AS N_EMPRESA      ,
    ISNULL(B.NOME,'0 - CONSUMIDOR FINAL')                                                       AS CLIENTE        ,
    ISNULL(CAST(B.CLASSIFICACAO_CLIENTE AS VARCHAR) + ' - ' + C.DESCRICAO,'0 - A DEFINIR')      AS GRUPO_CLIENTE  ,
    ISNULL(CAST(A.VENDEDOR AS VARCHAR ) + ' - ' + D.NOME ,'0 - SEM VENDEDOR')                   AS VENDEDOR       , 
    A.PRODUTO                                                                                   AS PRODUTO        ,
    P.DESCRICAO                                                                                 AS DESCRICAO      , 
    SUM( A.QUANTIDADE )                                                                         AS QUANTIDADE     ,    
    P.UNIDADE_MEDIDA                                                                            AS UNIDADE_MEDIDA ,
    SUM( A.VENDA_LIQUIDA )                                                                      AS VENDA_LIQUIDA  ,
    A.OPERACAO_FISCAL                                                                           AS OPERACAO_FISCAL,
    E.DESCRICAO                                                                                 AS MARCA,
    A.DOCUMENTO_NUMERO,
    A.MOVIMENTO,
    F.TELEFONE

 FROM VENDAS_ANALITICAS          A WITH(NOLOCK) 
 LEFT
 JOIN ENTIDADES                  B WITH(NOLOCK) ON B.ENTIDADE              = A.CLIENTE
 LEFT
 JOIN CLASSIFICACOES_CLIENTES    C WITH(NOLOCK) ON C.CLASSIFICACAO_CLIENTE = B.CLASSIFICACAO_CLIENTE
 LEFT
 JOIN VENDEDORES                 D WITH(NOLOCK) ON D.VENDEDOR              = A.VENDEDOR
 LEFT
 JOIN EMPRESAS_USUARIAS          W WITH(NOLOCK) ON W.EMPRESA_USUARIA       = A.EMPRESA
 JOIN PRODUTOS                   P WITH(NOLOCK) ON P.PRODUTO               = A.PRODUTO
 JOIN MARCAS                     E WITH(NOLOCK) ON E.MARCA                 = P.MARCA
 LEFT
 JOIN (
SELECT A.ENTIDADE, MAX( '(' + CAST(A.DDD AS VARCHAR) + ') ' + CAST(A.NUMERO AS VARCHAR) ) + CASE WHEN COUNT(*) > 1 THEN ' (...)' ELSE '' END  AS TELEFONE
 FROM TELEFONES A WITH(NOLOCK)
GROUP BY A.ENTIDADE
      )                          F              ON F.ENTIDADE              = A.CLIENTE

WHERE 1=1                      
    AND A.MOVIMENTO BETWEEN @DATAINI AND @DATAFIM 
    AND ( A.CLIENTE              = @ENTIDADE         OR @ENTIDADE IS NULL )
    AND A.CLIENTE IS NOT NULL
    AND (A.EMPRESA              >=  @EMPRESA_INI  OR @EMPRESA_INI IS NULL) 
    AND (A.EMPRESA              <=  @EMPRESA_FIM  OR @EMPRESA_FIM IS NULL)
    AND (A.PRODUTO               = @PRODUTO OR @PRODUTO IS NULL                            )  
    --AND (W.ENTIDADE                                 =  @ENTIDADE     OR @ENTIDADE    IS NULL)  
  
GROUP BY 
A.EMPRESA                                                                                 ,
A.CLIENTE                                                                                 ,
ISNULL(B.NOME,'0 - CONSUMIDOR FINAL')                                                     ,
ISNULL(CAST(B.CLASSIFICACAO_CLIENTE AS VARCHAR) + ' - ' + C.DESCRICAO,'0 - A DEFINIR')    ,
ISNULL(CAST(A.VENDEDOR AS VARCHAR ) + ' - ' + D.NOME ,'0 - SEM VENDEDOR')                 ,
A.PRODUTO                                                                                 ,
P.DESCRICAO                                                                               ,
P.UNIDADE_MEDIDA                                                                          ,
A.OPERACAO_FISCAL,
E.DESCRICAO,
A.DOCUMENTO_NUMERO,
A.MOVIMENTO,
F.TELEFONE,
W.NOME

  HAVING SUM( A.VENDA_LIQUIDA ) <> 0
                    
ORDER BY 
A.EMPRESA                                                                                ,   
ISNULL(CAST(B.CLASSIFICACAO_CLIENTE AS VARCHAR) + ' - ' + C.DESCRICAO,'0 - A DEFINIR')   ,   
ISNULL(CAST(A.VENDEDOR AS VARCHAR ) + ' - ' + D.NOME ,'0 - SEM VENDEDOR')                ,
ISNULL(B.NOME,'0 - CONSUMIDOR FINAL')             