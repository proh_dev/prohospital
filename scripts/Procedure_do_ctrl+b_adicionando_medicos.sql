ALTER PROCEDURE [dbo].[USP_SINCRONIZA_PRODUTOS_LOJA]      
AS      
      
SET XACT_ABORT ON      
      
DECLARE @EMPRESA NUMERIC(15)      
DECLARE @COMANDO NVARCHAR(MAX)      
      
SELECT TOP 1 @EMPRESA = A.EMPRESA FROM PARAMETROS A      
      
IF @EMPRESA IS NULL      
BEGIN      
 RAISERROR('Empresa não Configurada no Banco Loja', 11, 0)      
 RETURN      
END      
      
EXEC USP_ATUALIZA_LINKED_SERVER_RETAGUARDA      
      
SET @COMANDO =       
'SELECT      
    [PRODUTO]      
      ,[DESCRICAO]      
      ,[DESCRICAO_COMPLETA]      
      ,[MARCA]      
      ,[PRECO]      
      ,[DESCONTO_PADRAO]      
      ,[DESCONTO_MAXIMO]      
      ,[TIPO_ICMS]      
      ,[ALIQUOTA_ICMS]      
      ,[IAT]      
      ,[IPPT]      
      ,[SECAO_PRODUTO]      
      ,[GRUPO_PRODUTO]      
      ,[SUBGRUPO_PRODUTO]      
      ,[DESCONTO_PROGRESSIVO]      
      ,[DATA_HORA]      
      ,[UNIDADE_MEDIDA]      
      ,[CONTROLADO]      
      ,[DESCONTO_PADRAO_PRAZO]      
      ,[DESCONTO_MAXIMO_PRAZO]      
      ,[PRODUTO_MANIPULADO]      
      ,[PMC_FPOPULAR]         
      ,[QUANTIDADE_FP]      
      ,[CFOP]      
      ,[NCM]      
      ,[CST]      
FROM       
 PRODUTOS_EXPORTACAO_LOJAS A      
WHERE      
 A.EMPRESA = ' + CONVERT(VARCHAR(15), @EMPRESA)      
      
BEGIN TRANSACTION      
      
TRUNCATE TABLE PRODUTOS      
      
INSERT INTO PRODUTOS      
(      
    [PRODUTO]      
      ,[DESCRICAO]      
      ,[DESCRICAO_COMPLETA]      
      ,[MARCA]      
      ,[PRECO]      
      ,[DESCONTO_PADRAO]      
      ,[DESCONTO_MAXIMO]      
      ,[TIPO_ICMS]      
      ,[ALIQUOTA_ICMS]      
      ,[IAT]      
      ,[IPPT]      
      ,[SECAO_PRODUTO]      
      ,[GRUPO_PRODUTO]      
      ,[SUBGRUPO_PRODUTO]      
      ,[DESCONTO_PROGRESSIVO]      
      ,[DATA_HORA]      
      ,[UNIDADE_MEDIDA]      
      ,[CONTROLADO]      
      ,[DESCONTO_PADRAO_PRAZO]      
      ,[DESCONTO_MAXIMO_PRAZO]      
      ,[PRODUTO_MANIPULADO]      
      ,[PMC_FPOPULAR]      
      ,[QUANTIDADE_FP]      
      ,[CFOP]      
      ,[NCM]      
      ,[CST]      
)      
EXEC (@COMANDO) AT RETAGUARDA      
      
      
/*----------    */  
/* MARCAS --    */  
/*----------    */  
SET @COMANDO = 'SELECT MARCA, DESCRICAO FROM MARCAS (NOLOCK)'      
      
TRUNCATE TABLE MARCAS      
      
INSERT INTO MARCAS (MARCA, DESCRICAO)      
EXEC(@COMANDO) AT RETAGUARDA      
      
      
/*-------    */  
/* EAN --    */  
/*-------    */  
SET @COMANDO =       
'SELECT EAN      
       ,PRODUTO      
       ,PRODUTO_EAN      
       ,1 AS QUANTIDADE_EMBALAGEM      
FROM       
 PRODUTOS_EAN A (NOLOCK)'      
      
      
TRUNCATE TABLE EAN       
      
INSERT INTO EAN (      
        EAN      
       ,PRODUTO      
       ,PRODUTO_EAN      
       ,QUANTIDADE_EMBALAGEM      
)      
EXEC(@COMANDO) AT RETAGUARDA      
      
      
/*--------------------    */  
/* CONVENIOS_GRUPOS --    */  
/*--------------------    */  
SET @COMANDO = 'SELECT ENTIDADE, GRUPO_PRODUTO, TIPO, DESCONTO, CO_PAGAMENTO FROM ENTIDADES_CONVENIOS_GRUPOS (NOLOCK)'      
      
TRUNCATE TABLE CONVENIOS_GRUPOS      
      
INSERT INTO CONVENIOS_GRUPOS (ENTIDADE, GRUPO_PRODUTO, TIPO, DESCONTO_PRODUTO, CO_PAGAMENTO)      
EXEC(@COMANDO) AT RETAGUARDA      
      
      
/*-----------------------    */  
/* CONVENIOS_SUBGRUPOS --    */  
/*-----------------------    */  
SET @COMANDO = 'SELECT ENTIDADE, SUBGRUPO_PRODUTO, TIPO, DESCONTO, CO_PAGAMENTO FROM ENTIDADES_CONVENIOS_SUBGRUPOS (NOLOCK)'      
      
TRUNCATE TABLE CONVENIOS_SUBGRUPOS      
      
INSERT INTO CONVENIOS_SUBGRUPOS (ENTIDADE, SUBGRUPO_PRODUTO, TIPO, DESCONTO_PRODUTO, CO_PAGAMENTO)      
EXEC(@COMANDO) AT RETAGUARDA      
      
/*--------------    */  
/* CORRELATOS --    */  
/*--------------    */  
SET @COMANDO = 'SELECT PRODUTO, CORRELATO, PRODUTO_CORRELATO FROM PRODUTOS_CORRELATOS (NOLOCK)'      
      
TRUNCATE TABLE CORRELATOS      
      
INSERT INTO CORRELATOS (      
       PRODUTO,      
    CORRELATO,      
    PRODUTO_CORRELATO      
)      
EXEC(@COMANDO) AT RETAGUARDA      
      
      
/*---------------    */  
/* SUBSTITUTOS --    */  
/*---------------    */  
SET @COMANDO = 'SELECT PRODUTO, SUBSTITUTO, PRODUTO_SUBSTITUTO FROM PRODUTOS_SUBSTITUTOS (NOLOCK)'      
      
TRUNCATE TABLE SUBSTITUTOS      
      
INSERT INTO SUBSTITUTOS (      
       PRODUTO,      
    SUBSTITUTO,      
    PRODUTO_SUBSTITUTO      
)      
EXEC(@COMANDO) AT RETAGUARDA      
          
/*--------------    */  
/* OPERADORES --    */  
/*--------------    */  
SET @COMANDO = 'EXEC USP_OPERADORES ' + CONVERT(VARCHAR, @EMPRESA)      
      
TRUNCATE TABLE OPERADORES      
      
INSERT INTO OPERADORES (      
      OPERADOR,      
   NOME ,      
   SENHA ,      
   DESCONTO_MAXIMO ,      
   VENDA ,      
   ABERTURA_CAIXA ,       
   FECHAMENTO_CAIXA ,      
   CANCELAMENTO_ITEM ,      
   CANCELAMENTO_CUPOM ,      
   LEITURA_X ,      
   REDUCAO_Z ,      
   FUNCOES ,      
   SANGRIA_CAIXA ,      
   SUPRIMENTO_CAIXA ,      
   SUPERVISOR ,      
   DESCONTO_TOTAL_BALCAO ,      
   DESCONTO_PDV ,      
   DATA_HORA ,      
   USUARIO_FP,      
   SENHA_FP,      
   CARGO_DESCONTO,      
              CANCELAMENTO_CUPOM_ANDAMENTO      
)      
      
EXEC(@COMANDO) AT RETAGUARDA      
      
  
      
/*------------------    */  
/* SECOES CLOSEUP --    */  
/*------------------    */  
SET @COMANDO = 'SELECT SECAO_PRODUTO FROM SECOES_PRODUTOS_SOLICITA_RECEITA (NOLOCK)  '       
      
TRUNCATE TABLE SECOES_CLOSEUP      
      
INSERT INTO SECOES_CLOSEUP (      
                             SECAO_PRODUTO     
                           )      
EXEC(@COMANDO) AT RETAGUARDA    
  
  
/*------------------    */  
/* TIPOS PRESCRITORES   */  
/*------------------    */  
SET @COMANDO = 'SELECT CR_PRESCRITOR, DESCRICAO FROM CR_PRESCRITOR (NOLOCK)  '       
      
TRUNCATE TABLE CR_PRESCRITOR      
      
INSERT INTO CR_PRESCRITOR (      
                             CR_PRESCRITOR,  
        DESCRICAO  
                           )      
EXEC(@COMANDO) AT RETAGUARDA   

/*-----------------------------------------*/                         
/* TIPOS MÉDICOS                           */
/* Solicitada adição Ynoa Pedro 14/09/2018 */ 
/*-----------------------------------------*/  
SET @COMANDO = 'SELECT MEDICO, NOME ,CODIGO_CR,CR , UF FROM MEDICOS (NOLOCK)  '       
      
TRUNCATE TABLE MEDICOS      
      
INSERT INTO MEDICOS (      
        MEDICO,  
        NOME,
		CODIGO_CR ,
		CR,
		UF 
                           )      
EXEC(@COMANDO) AT RETAGUARDA   
  
  
  
/*------------------    */  
/* TIPOS DOCUMENTOS     */  
/*------------------    */  
SET @COMANDO = 'SELECT TIPO_DOCUMENTO, DESCRICAO FROM TIPOS_DOCUMENTOS (NOLOCK)  '       
      
TRUNCATE TABLE TIPOS_DOCUMENTOS      
      
      SET IDENTITY_INSERT TIPOS_DOCUMENTOS ON
      
INSERT INTO TIPOS_DOCUMENTOS (      
                             TIPO_DOCUMENTO,  
        DESCRICAO  
                           )      
EXEC(@COMANDO) AT RETAGUARDA     
  
  
   SET IDENTITY_INSERT TIPOS_DOCUMENTOS OFF
  
/*----------------------  */  
/* TIPOS ORGAO_EXPEDITOR  */  
/*----------------------  */  
SET @COMANDO = 'SELECT SNGPC_ORGAO_EXPEDITOR AS CODIGO_ORGAO_EXPEDITOR,  
 ORGAO_EXPEDITOR AS ORGAO_EXPEDIDOR, DESCRICAO FROM SNGPC_ORGAO_EXPEDITOR (NOLOCK)  '       
      
TRUNCATE TABLE ORGAO_EXPEDIDORES      
      
INSERT INTO ORGAO_EXPEDIDORES (      
                             CODIGO_ORGAO_EXPEDITOR,  
        ORGAO_EXPEDIDOR,  
        DESCRICAO  
                           )      
EXEC(@COMANDO) AT RETAGUARDA     
  
  
COMMIT 