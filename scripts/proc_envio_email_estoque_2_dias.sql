--ALTER PROCEDURE [dbo].[ENVIO_EMAIL_APRV_CANC] ( @APROVACAO_CANCELAMENTO NUMERIC(15,0), @EMPRESA NUMERIC(15,0) ) AS                      
--BEGIN                      
                
--SET NOCOUNT ON      
  
--DECLARE @MOVIMENTO DATE 

	                    
DECLARE @TITULO_EMAIL                       VARCHAR(50)  
DECLARE @EMAIL                              VARCHAR(250) 
DECLARE @CONTEUDO_EMAIL                     VARCHAR(250)
DECLARE @tableHTML                          NVARCHAR(MAX) 
DECLARE @PRODUTO                            VARCHAR(50)
DECLARE @PRODUTO_NOME                       VARCHAR(50)
DECLARE @CENTRO_ESTOQUE                     NUMERIC
DECLARE @MOVIMENTO                          DATE      

SET @MOVIMENTO = '24/04/2018'  
  
--IF OBJECT_ID('TEMPDB..#DATA_HOJE') IS NOT NULL DROP TABLE #DATA_HOJE

--SELECT @MOVIMENTO AS HOJE
--INTO #DATA_HOJE


SET @TITULO_EMAIL            = ' ALERTA de Produto no centro de estoque'      
SET @EMAIL                    = 'pedromota99@gmail.com'               

SELECT  
	   @PRODUTO                 = CAST(B.PRODUTO AS VARCHAR (50)) + '-' + C.DESCRICAO   


	   
FROM ESTOQUE_LANCAMENTOS            A WITH(NOLOCK)
JOIN ESTOQUE_ATUAL                  B WITH(NOLOCK) ON A.PRODUTO   = B.PRODUTO
JOIN PRODUTOS                       C WITH(NOLOCK) ON A.PRODUTO   = C.PRODUTO

WHERE A.CENTRO_ESTOQUE = 991
  AND B.ESTOQUE_SALDO  > 0
  AND DATEDIFF(WEEKDAY, A.DATA, @MOVIMENTO) > 2
  --AND (DATEPART(DW,A.HOJE ))  <> 1


  SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;
              }  
              </style>'                    +  
    N'<H1 class="formato" > O seguinte Produto encontra-se no Centro de Estoque  991 a mais de 2 dias uteis             <br> <br>  
 Produto:   <br>'+ @PRODUTO                                                                                      + '   <br> <br>  


  
  </H1>'
      
  

  


  BEGIN   

   EXEC MSDB.DBO.SP_SEND_DBMAIL  
     
     @RECIPIENTS   = @EMAIL,  
     @SUBJECT      = @TITULO_EMAIL ,  
     @BODY         = @tableHTML,  
     @BODY_FORMAT  = 'HTML';  

   END        
  

             
--END 
