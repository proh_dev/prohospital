--CREATE PROCEDURE [dbo].[ENVIO_EMAIL_SOL_CANC_NFE] ( @SOLICITACAO_CANCELAMENTO_NF NUMERIC(15,0), @EMPRESA NUMERIC(15,0) ) AS                      
--BEGIN                      
                
SET NOCOUNT ON      
  
DECLARE @SOLICITACAO_CANCELAMENTO_NF        NUMERIC(15,0) = 1--:SOLICITACAO_CANCELAMENTO_NF -- 
DECLARE @EMPRESA		                    NUMERIC(15,0) = 10 --:EMPRESA --

DECLARE @EMPRESA_SOLICITANTE                VARCHAR(50)				                    
DECLARE @TITULO_EMAIL                       VARCHAR(50)  
DECLARE @tableHTML                          NVARCHAR(MAX)   
DECLARE @SOLICITANTE                        VARCHAR(50)  
DECLARE @NF_NUMERO                          VARCHAR(50)  
DECLARE @EMAIL                              VARCHAR(250) 
DECLARE @JUSTIFICATIVA                      VARCHAR(250)
DECLARE @REFATURA_NOTA						VARCHAR(30)
DECLARE @CLIENTE     						VARCHAR(250)
  

  
SET @TITULO_EMAIL = 'Nova Solicitação de cancelamento de Nota ' + CAST ( @SOLICITACAO_CANCELAMENTO_NF AS VARCHAR (50) )  
  
SELECT @SOLICITANTE             = CAST (A.USUARIO_LOGADO  AS VARCHAR (50)) + ' - ' + C.NOME,
       @EMPRESA_SOLICITANTE     = CAST (A.EMPRESA         AS VARCHAR (50)) + ' - ' + D.NOME,     
       @NF_NUMERO               = CAST (A.NF_NUMERO       AS VARCHAR (50)),
	   @JUSTIFICATIVA           = CAST (A.JUSTIFICATIVA   AS VARCHAR (250)),
	   @REFATURA_NOTA           = CASE WHEN A.REFATURA_NOTA = 'S' THEN 'Sim' ELSE 'Não' END ,
       @EMAIL                   = 'pedro.mota@prohospital.com.br',--B.EMAIL_APROVADOR,
       @CLIENTE                 = CAST (A.CLIENTE         AS VARCHAR (50)) + ' - ' + E.NOME

   FROM SOLICITACOES_CANCELAMENTOS_NF   A WITH(NOLOCK)  
   JOIN PARAMETROS_FISCAIS              B WITH(NOLOCK) ON A.EMPRESA        = B.EMPRESA_USUARIA  
   LEFT
   JOIN USUARIOS                        C WITH(NOLOCK) ON A.USUARIO_LOGADO = C.USUARIO
   JOIN EMPRESAS_USUARIAS               D WITH(NOLOCK) ON A.EMPRESA        = D.EMPRESA_USUARIA
   JOIN ENTIDADES                       E WITH(NOLOCK) ON E.ENTIDADE       = A.CLIENTE
  WHERE 1=1
        AND A.SOLICITACAO_CANCELAMENTO_NF = @SOLICITACAO_CANCELAMENTO_NF 
		AND A.EMPRESA = @EMPRESA

  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;
              }  
              </style>'                    +  
    N'<H1 class="formato" >Nova Solicitação de Cancelamento de nota <br>    <br>  
 Solicitação nº '     + CAST(@SOLICITACAO_CANCELAMENTO_NF AS VARCHAR) + '   <br> <br>  
 Solicitante: '       + @SOLICITANTE                                  + '   <br> <br> 
 Empresa: '           + @EMPRESA_SOLICITANTE                          + '   <br> <br> 
 Nota nº: '           + @NF_NUMERO                                    + '   <br> <br>  
 Cliente: '           + @CLIENTE                                      + '   <br> <br> 
 Refatura Nota? '     + @REFATURA_NOTA                                + '   <br> <br> 
 Justificativa: '     + @JUSTIFICATIVA                                + '   <br> <br>   
  
  </H1>'
  
  
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML';  
  
  
--END 