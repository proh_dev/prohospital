-----------------------------------------------
--  CONSULTA DE ROMANEIOS LINKADOS COM NF    --
--  AUTOR: YNOA PEDRO                        --
--  DATA : 07/2018                           --
-----------------------------------------------
DECLARE @ESTOQUE_TRANSF NUMERIC


SET @ESTOQUE_TRANSF = 15372
--SET @ESTOQUE_TRANSF = :ESTOQUE_TRANSFERENCIA

SELECT DISTINCT
       769791                    AS FORMULARIO_ORIGEM,
       753289                    AS TAB_MASTER_ORIGEM,
       A.NF_FATURAMENTO          AS REG_MASTER_ORIGEM,
       A.NF_FATURAMENTO,
       A.EMPRESA,
       A.MOVIMENTO,
       A.NF_NUMERO,
       F.DESCRICAO AS OPERACAO_FISCAL,
       A.VENDA_CONTROLADA,
       A.ENTIDADE ,
       B.NOME     ,
       C.TOTAL_GERAL   ,   
	   G.ESTOQUE_TRANSFERENCIA,                            

       CASE WHEN D.REGISTRO_NFE IS NOT NULL
            THEN CASE WHEN E.NFE_CANCELAMENTO IS NOT NULL 
                      THEN CASE WHEN E.STATUS = 4 
                                THEN 'CANCELADA NO SEFAZ'
                                ELSE 'CANCELAMENTO PENDENTE DE VALIDA��O NO SEFAZ'
                           END
                      ELSE CASE WHEN D.STATUS = 4 
                                THEN 'VALIDADA NO SEFAZ'
                                ELSE 'INCONSISTENTE NO SEFAZ'
                           END
                  END                                                    
            ELSE 'PENDENTE ENVIO AO SEFAZ'

    END AS ESTADO
        
  FROM      NF_FATURAMENTO               A WITH(NOLOCK)
       JOIN ENTIDADES                    B WITH(NOLOCK)ON B.ENTIDADE               = A.ENTIDADE
       JOIN OPERACOES_FISCAIS            F WITH(NOLOCK)ON F.OPERACAO_FISCAL        = A.OPERACAO_FISCAL
  LEFT JOIN NF_FATURAMENTO_TOTAIS        C WITH(NOLOCK)ON C.NF_FATURAMENTO         = A.NF_FATURAMENTO  
  LEFT JOIN NFE_CABECALHO                D WITH(NOLOCK) ON A.NF_FATURAMENTO        = D.XML_cNF_B03
                                                       AND A.NF_NUMERO             = D.XML_nNF_B08
                                                       AND A.NF_SERIE              = D.XML_serie_B07
                                                       AND A.EMPRESA               = D.EMPRESA
                                          									       
  LEFT JOIN NFE_CANCELAMENTOS            E WITH(NOLOCK) ON E.IDNOTA                = D.CHAVE_NFE
       JOIN NF_FATURAMENTO_TRANSF        G WITH(NOLOCK) ON A.NF_FATURAMENTO        = G.NF_FATURAMENTO 
	   JOIN ESTOQUE_TRANSFERENCIAS       H WITH(NOLOCK) ON H.ESTOQUE_TRANSFERENCIA = G.ESTOQUE_TRANSFERENCIA
                                            

 WHERE G.ESTOQUE_TRANSFERENCIA = @ESTOQUE_TRANSF
 
ORDER BY EMPRESA, ENTIDADE


