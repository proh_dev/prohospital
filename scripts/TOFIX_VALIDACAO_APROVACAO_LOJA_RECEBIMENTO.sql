 DECLARE @RECEBIMENTO_APROVACAO NUMERIC 
     SET @RECEBIMENTO_APROVACAO   = :RECEBIMENTO_APROVACAO

--GERA TEMP DE RECEBIMENTOS--
 if object_id('tempdb..#RECEBIMENTOS_TEMP') is not null
   DROP TABLE #RECEBIMENTOS_TEMP

SELECT A.RECEBIMENTO

INTO #RECEBIMENTOS_TEMP

  FROM APROVACOES_RECEB_VOLUMES_ITENS A WITH(NOLOCK)
 WHERE 1=1
 AND A.RECEBIMENTO_APROVACAO = @RECEBIMENTO_APROVACAO


--GERA TEMP DE PEDIDOS--
 if object_id('tempdb..#PEDIDOS') is not null
   DROP TABLE #PEDIDOS
   
   
 SELECT DISTINCT A.PEDIDO_COMPRA
 
   INTO #PEDIDOS
 
   FROM RECEBIMENTOS_VOLUMES_NF   A WITH(NOLOCK)
   JOIN #RECEBIMENTOS_TEMP        X WITH(NOLOCK) ON X.RECEBIMENTO = A.RECEBIMENTO
 
   
   
------------------------------------------------
 --VERIFICACAO DOS ITENS PEDIDO CONTRA NOTA
------------------------------------------------

 if object_id('tempdb..#ITENS') is not null
   DROP TABLE #ITENS
 
 
 SELECT DISTINCT A.PRODUTO
     
   INTO #ITENS
 
   FROM (
 
 
  SELECT DISTINCT C.PRODUTO 
    FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
    JOIN RECEBIMENTOS_VOLUMES_NF             B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
    JOIN NF_COMPRA_PRODUTOS                  C WITH(NOLOCK) ON C.NF_COMPRA   = B.NF_COMPRA
   JOIN #RECEBIMENTOS_TEMP                   X WITH(NOLOCK) ON X.RECEBIMENTO = A.RECEBIMENTO
  
  UNION ALL
  
  SELECT DISTINCT C.PRODUTO 
    FROM #PEDIDOS                            A WITH(NOLOCK)
    JOIN PEDIDOS_COMPRAS_PRODUTOS            C WITH(NOLOCK) ON C.PEDIDO_COMPRA  = A.PEDIDO_COMPRA 
  

  UNION ALL
  
  SELECT DISTINCT B.PRODUTO 
    FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
    JOIN RECEBIMENTOS_VOLUMES_PRODUTOS       B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
    JOIN #RECEBIMENTOS_TEMP                  X WITH(NOLOCK) ON X.RECEBIMENTO = A.RECEBIMENTO
  
   ) A

 if object_id('tempdb..#RECEBIMENTOS') is not null
   DROP TABLE #RECEBIMENTOS

  
  SELECT B.RECEBIMENTO,
         DBO.CONCATENA_NOTAS   ( B.RECEBIMENTO ) AS NOTAS,
         DBO.CONCATENA_PEDIDOS ( B.RECEBIMENTO ) AS PEDIDOS,
                  
         A.PRODUTO,
         C.DESCRICAO,
         ISNULL(Y.QUANTIDADE_PEDIDO,0) AS QUANTIDADE_PEDIDO,
         ISNULL(X.QUANTIDADE_NOTA  ,0) AS QUANTIDADE_NOTA,
         ISNULL(Z.QUANTIDADE_RECEBIMENTO,0 ) AS QUANTIDADE_RECEBIDA, 

         CASE WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  ) OR 
                   ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) OR          
                   ( ( ISNULL(X.QUANTIDADE_NOTA  ,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  )
              THEN CASE WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  )
                        THEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) )
                        WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) 
                        THEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) )
                        WHEN ( ( ISNULL(X.QUANTIDADE_NOTA  ,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  )
                        THEN ( ( ISNULL(X.QUANTIDADE_NOTA  ,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) )
                        ELSE 0
                    END
              ELSE 0
         END AS DIVERGENCIA,

                  
         CASE WHEN ISNULL(Y.QUANTIDADE_PEDIDO,0) = 0
              THEN 'Produto Sem Pedido'
              ELSE CASE WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(X.QUANTIDADE_NOTA         ,0 ) ) <>  0  ) THEN 'Pedido <> Nota'
                        WHEN ( ( ISNULL(Y.QUANTIDADE_PEDIDO,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) THEN 'Pedido <> Fisico'
                        WHEN ( ( ISNULL(X.QUANTIDADE_NOTA  ,0) - ISNULL(Z.QUANTIDADE_RECEBIMENTO  ,0 ) ) <>  0  ) THEN 'Nota <> Fisico'
                        ELSE 'OK'
                   END
                   
         END AS MENSAGEM,
         1   AS ITEM

   INTO #RECEBIMENTOS                       
         
    FROM #ITENS                  A WITH(NOLOCK)    
   JOIN #RECEBIMENTOS_TEMP       B WITH(NOLOCK) ON 1=1
       
  LEFT JOIN ( SELECT C.PRODUTO,
                     SUM(C.QUANTIDADE_ESTOQUE) AS QUANTIDADE_NOTA                     
                FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
                JOIN RECEBIMENTOS_VOLUMES_NF             B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
                JOIN NF_COMPRA_PRODUTOS                  C WITH(NOLOCK) ON C.NF_COMPRA   = B.NF_COMPRA
                JOIN #RECEBIMENTOS_TEMP                  X WITH(NOLOCK) ON X.RECEBIMENTO = A.RECEBIMENTO
              GROUP BY C.PRODUTO ) X ON X.PRODUTO = A.PRODUTO

  LEFT JOIN ( SELECT C.PRODUTO,
                     SUM(C.QUANTIDADE_ESTOQUE) AS QUANTIDADE_PEDIDO                     
                FROM #PEDIDOS                            A WITH(NOLOCK)
                JOIN PEDIDOS_COMPRAS_PRODUTOS            C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA
              GROUP BY C.PRODUTO ) Y ON Y.PRODUTO = A.PRODUTO
       JOIN PRODUTOS   C WITH(NOLOCK) ON C.PRODUTO = A.PRODUTO              

  LEFT JOIN ( SELECT B.PRODUTO,
                     SUM(B.QUANTIDADE_UNIT) AS QUANTIDADE_RECEBIMENTO                    
                FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
                JOIN RECEBIMENTOS_VOLUMES_PRODUTOS       B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
                JOIN #RECEBIMENTOS_TEMP                  X WITH(NOLOCK) ON X.RECEBIMENTO = A.RECEBIMENTO
              GROUP BY B.PRODUTO ) Z ON Z.PRODUTO = A.PRODUTO    
          
  ORDER BY MENSAGEM
  



SELECT 'ATEN��O!!!!.Recebimento N�mero: ' + CAST(A.RECEBIMENTO AS VARCHAR(15)) + ' ' +
       'Foi Informado ' + CAST(COUNT(*) AS VARCHAR(15)) + ' Vezes. Favor Verificar'
 FROM  APROVACOES_RECEB_VOLUMES_ITENS  A WITH(NOLOCK)
 LEFT  JOIN APROVACOES_RECEB_VOLUMES   B WITH(NOLOCK)ON B.RECEBIMENTO_APROVACAO = A.RECEBIMENTO_APROVACAO                                                        
WHERE A.RECEBIMENTO_APROVACAO =@RECEBIMENTO_APROVACAO

GROUP BY A.RECEBIMENTO
HAVING COUNT(*) > 1

UNION ALL

SELECT 'N�o � poss�vel Salvar Aprova��o sem Informar o Recebimento'
  FROM APROVACOES_RECEB_VOLUMES_ITENS A WITH (NOLOCK)
 WHERE A.RECEBIMENTO_APROVACAO = @RECEBIMENTO_APROVACAO
HAVING COUNT(*) = 0
  
UNION ALL


SELECT TOP 1 'N�o � Poss�vel Realizar Aprova��o sem Recebimento Lote validade Realizado!!'
     FROM RECEBIMENTOS_VOLUMES                A WITH(NOLOCK)
     JOIN RECEBIMENTOS_VOLUMES_NF             X WITH(NOLOCK) ON X.RECEBIMENTO = A.RECEBIMENTO
     JOIN NF_COMPRA_PRODUTOS                  Z WITH(NOLOCK) ON Z.NF_COMPRA   = X.NF_COMPRA     
     JOIN PRODUTOS_FARMACIA                   B WITH(NOLOCK)ON B.PRODUTO      = Z.PRODUTO
     JOIN APROVACOES_RECEB_VOLUMES_ITENS      C WITH(NOLOCK)ON C.RECEBIMENTO  = A.RECEBIMENTO  
LEFT JOIN RECEBIMENTOS_LOTE_VALIDADE          D WITH(NOLOCK)ON D.NF_COMPRA    = X.NF_COMPRA
LEFT JOIN RECEBIMENTOS_LOTE_VALIDADE_PRODUTOS E WITH(NOLOCK)ON E.RECEBIMENTO  = D.RECEBIMENTO
 WHERE C.RECEBIMENTO_APROVACAO = @RECEBIMENTO_APROVACAO
   AND B.VENDA_CONTROLADA      = 'S'
   AND (D.RECEBIMENTO IS NULL OR E.PRODUTO IS NULL)
AND 1 = 2 
-- COMENTADO POR TATIANE ESPECIFICAMENTE PARA A FARMACONDE QUE N�O UTILIZARA CONTROLADOS NO SISTEMA!
-- PEDIDO DE PEREIRA E AUTORIZADO POR GEMADA

UNION ALL


SELECT TOP 1 'N�o � poss�vel realizar aprova��o quando h� diverg�ncias! CONTATE O SETOR DE COMPRAS!'
     FROM #RECEBIMENTOS                      A WITH(NOLOCK)
	 LEFT
	 JOIN APROVACOES_DIVERGENCIAS_NF_COMPRA  B WITH(NOLOCK) ON B.RECEBIMENTO = A.RECEBIMENTO
	 WHERE 1=1
     AND A.DIVERGENCIA <> 0
     AND ( B.PROCESSAR    = 'N' OR B.PROCESSAR IS NULL )