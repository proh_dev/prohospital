SELECT   
       A.FECHAMENTO_CONVENIO_CARTAO AS FECHAMENTO_CONVENIO_CARTAO,   
       M.FUNCIONARIO                AS FUNCIONARIO,
       C.LOJA                       AS EMPRESA,
       C.MOVIMENTO                  AS DATA ,    
       C.CLIENTE                    AS CLIENTE ,    
       SUM(CASE WHEN C.MOVIMENTO >= ISNULL(J.DATA_INICIO_ERPM_PDV,'31/12/2999')     
                THEN CASE WHEN C.STATUS = 'A' THEN C.VALOR ELSE VALOR * (-1) END     
                ELSE CASE WHEN C.STATUS = 'A' THEN C.VALOR ELSE 0 END    
           END)                     AS VALOR,
       DBO.NUMERICO(N.ECF_CUPOM)    AS DOCUMENTO
    
  FROM FECHAMENTOS_CONVENIOS_CARTOES          A WITH(NOLOCK)    
  LEFT JOIN FECHAMENTOS_CONVENIOS_EMPRESARIAL B WITH(NOLOCK) ON B.FECHAMENTO_CONVENIO_CARTAO = A.FECHAMENTO_CONVENIO_CARTAO    
  LEFT JOIN PDV_CONVENIOS                     C WITH(NOLOCK) ON C.CODIGO_CONVENIO = B.ENTIDADE    
  LEFT JOIN EMPRESAS_USUARIAS                 D WITH(NOLOCK) ON D.EMPRESA_USUARIA = C.LOJA    
  INNER JOIN CLIENTES_SALDOS                  H WITH(NOLOCK) ON H.CLIENTE = C.CLIENTE AND    
                                                                H.TIPO    = C.TIPO_CLIENTE    
  INNER JOIN PARAMETROS_CONVENIOS_CARTOES     I WITH(NOLOCK) ON I.GRUPO_EMPRESARIAL = 1 AND    
                                                                I.ATUALIZA_LIMITE_BAIXA = 'N'    
  LEFT JOIN PARAMETROS_VENDAS                 J WITH(NOLOCK) ON J.EMPRESA_USUARIA = D.EMPRESA_USUARIA    
       --JOIN FECHAMENTOS_CONVENIOS_EMPRESAS    L WITH(NOLOCK) ON A.FECHAMENTO_CONVENIO_CARTAO = L.FECHAMENTO_CONVENIO_CARTAO   
       JOIN ENTIDADES_CONVENIOS_CARTOES       M WITH(NOLOCK) ON M.CONVENIO_CARTAO = C.CLIENTE
       JOIN PDV_VENDAS                        N WITH(NOLOCK) ON N.VENDA     = C.VENDA
                                                            AND N.LOJA      = C.LOJA
                                                            AND N.MOVIMENTO = C.MOVIMENTO
                                                            AND N.CAIXA     = C.CAIXA
    
 WHERE 1=1                                                           AND
       C.FECHAMENTO_CONVENIO_CARTAO IS NULL                          AND    
       C.DATA_FECHAMENTO <= B.DATA_FECHAMENTO                        AND  

       
       M.FUNCIONARIO NOT IN(  62360
                        , 61029
                        , 151623
                        , 59935
                        , 116388  ) 
       

 GROUP BY     
A.FECHAMENTO_CONVENIO_CARTAO ,
M.FUNCIONARIO                ,
C.LOJA                       ,
C.MOVIMENTO                  ,
C.CLIENTE       ,
DBO.NUMERICO(N.ECF_CUPOM) 

HAVING  SUM(CASE WHEN C.MOVIMENTO >= ISNULL(J.DATA_INICIO_ERPM_PDV,'31/12/2999')     
                THEN CASE WHEN C.STATUS = 'A' THEN C.VALOR ELSE VALOR * (-1) END     
                ELSE CASE WHEN C.STATUS = 'A' THEN C.VALOR ELSE 0 END    
            END) > 0            

UNION ALL

SELECT  
       D.ENTIDADE                   AS FUNCIONARIO,
       A.EMPRESA                    AS EMPRESA,
       A.MOVIMENTO                  AS DATA ,    
       NULL                         AS CLIENTE ,    
       A.VALOR                      AS VALOR,
       A.VALOR                      AS VALOR_COBRADO,
       A.TITULO                     AS DOCUMENTO

           FROM TITULOS_RECEBER        A WITH(NOLOCK)
           JOIN ENTIDADES              B WITH(NOLOCK) ON B.ENTIDADE       = A.ENTIDADE
           JOIN TITULOS_RECEBER_SALDO  C WITH(NOLOCK) ON C.TITULO_RECEBER = A.TITULO_RECEBER
           LEFT
           JOIN FUNCIONARIOS           D WITH(NOLOCK) ON D.ENTIDADE       = A.ENTIDADE
           LEFT 
           JOIN ENTIDADES              E WITH(NOLOCK) ON E.ENTIDADE  = A.EMPRESA
           LEFT 
           JOIN ENTIDADES              F WITH(NOLOCK) ON F.ENTIDADE = E.CONCENTRADOR
           JOIN FECHAMENTOS_CONVENIOS_EMPRESARIAL G WITH(NOLOCK) ON G.ENTIDADE                   = A.EMPRESA
           JOIN FECHAMENTOS_CONVENIOS_CARTOES     H WITH(NOLOCK) ON H.FECHAMENTO_CONVENIO_CARTAO = G.FECHAMENTO_CONVENIO_CARTAO
  WHERE 1=1
    AND C.SITUACAO_TITULO = 1
    AND A.VENCIMENTO <= CAST( CAST(H.DIA_FECHAMENTO_FIM AS VARCHAR) + '/' + CAST(H.MES AS VARCHAR) + '/' + CAST(H.ANO AS VARCHAR) AS DATE)
    AND D.ENTIDADE IS NOT NULL
    
    AND D.ENTIDADE   NOT IN( 62360
                        , 61029
                        , 151623
                        , 59935
                        , 116388  )
    AND A.VALOR > 0
    

    ORDER BY FUNCIONARIO, DATA




