-----------------------------------------------------------
-- ROTINA QUE GERA A SUGESTAO DE COMPRAS PARA O DEPOSITO --
-----------------------------------------------------------
                   
DECLARE @MES       NUMERIC(2)
DECLARE @ANO       NUMERIC(4)
DECLARE @PRODUTO   NUMERIC(15)
DECLARE @REGISTRO  NUMERIC             
                              
SET @PRODUTO = 13028--:PRODUTO

if object_id('tempdb..#EMPRESAS_ESTOQUES') is not null
   DROP TABLE #EMPRESAS_ESTOQUES
                                            
SELECT DISTINCT
       A.FILIAL          AS FILIAL,
       A.EMPRESA_USUARIA AS EMPRESA,
       B.OBJETO_CONTROLE AS CENTRO_ESTOQUE
                                        
  INTO #EMPRESAS_ESTOQUES

  FROM EMPRESAS_USUARIAS A WITH(NOLOCK)
  JOIN EMPRESAS_ESTOQUES B WITH(NOLOCK) ON A.EMPRESA_USUARIA = B.EMPRESA_USUARIA
 WHERE A.ATIVO = 'S'
   AND B.TIPO_ESTOQUE =2


if object_id('tempdb..#EMPRESAS_TRANSF_ENTRADA') is not null
   DROP TABLE #EMPRESAS_TRANSF_ENTRADA
                                            
SELECT DISTINCT
       B.FILIAL          AS FILIAL,
       B.EMPRESA         AS EMPRESA,
       A.PRODUTO         AS PRODUTO,
       MAX(A.DATA)       AS ULTIMA_TRANSFERENCIA_ENTRADA
       
  INTO #EMPRESAS_TRANSF_ENTRADA

  FROM ESTOQUE_LANCAMENTOS A WITH(NOLOCK)
  JOIN #EMPRESAS_ESTOQUES  B WITH(NOLOCK) ON B.CENTRO_ESTOQUE = A.CENTRO_ESTOQUE
 WHERE A.PRODUTO        = @PRODUTO
   AND A.TIPO_MOVIMENTO = 4
 GROUP BY
 B.FILIAL        ,
 B.EMPRESA       ,
 A.PRODUTO       
 
   

------------------------------------------------
-- DEFINE MES E ANO PARA FILTRO DO VALOR BASE --
------------------------------------------------
/*     

-- COMENTADO POR FILIPE OLIVEIRA EM 26/07/2017

SET @REGISTRO = (SELECT MAX(REGISTRO) FROM VALOR_BASE WITH(NOLOCK) WHERE PRODUTO = @PRODUTO)

SELECT @MES = A.MES,
       @ANO = A.ANO
  FROM VALOR_BASE A WITH(NOLOCK)
  
  WHERE REGISTRO = @REGISTRO 

*/

SELECT @MES = datepart(MONTH,GETDATE()),
       @ANO = datepart(YEAR,GETDATE())

IF ( SELECT COUNT(*) FROM VALOR_BASE WITH(NOLOCK) WHERE MES = @MES AND ANO = @ANO ) = 0
BEGIN
     SET @MES = @MES - 1
     IF  @MES < 1 SET @MES = 12
END 

SELECT X.FILIAL                                 AS FILIAL , 
       X.PRODUTO                                AS PRODUTO,
       X.DESCRICAO                              AS DESCRICAO , 
       ISNULL(X.MES_0,0)                        AS MES_0,    
       ISNULL(X.MES_1,0)                        AS MES_1,    
       ISNULL(X.MES_2,0)                        AS MES_2, 
       ISNULL(X.MES_3,0)                        AS MES_3, 
       ISNULL(X.MES_4,0)                        AS MES_4, 
	   ISNULL(X.MES_5,0)                        AS MES_5,
	   ISNULL(X.MES_6,0)                        AS MES_6,
       ISNULL(X.DEMANDA_DIA_PONDERADA,0)        AS DEMANDA_DIA_PONDERADA , 
       ISNULL(X.VALOR_BASE,0)                   AS VALOR_BASE , 
       ISNULL(X.ESTOQUE_SALDO,0)                AS ESTOQUE_SALDO ,
       ISNULL(X.PEDIDO,0)                       AS PEDIDO,
       A.ULTIMA_ENTRADA                         AS ULTIMA_ENTRADA ,
       V.ULTIMA_VENDA                           AS ULTIMA_VENDA ,
       ISNULL(D.CUSTO_CONTABIL,0)               AS CUSTO_MEDIO,
       ISNULL(E.ULTIMO_CUSTO,0)                 AS ULTIMO_CUSTO,
       ISNULL(E.ULTIMA_COMPRA,0)                AS ULTIMA_COMPRA,
       ISNULL(H.ULTIMA_TRANSFERENCIA_ENTRADA,0) AS ULTIMA_TRF_ENTRADA,
       ISNULL(X.ESTOQUE_MINIMO,0)               AS ESTOQUE_MINIMO,
       ISNULL(X.ESTOQUE_MAXIMO,0)               AS ESTOQUE_MAXIMO,
       ISNULL(X.ESTOQUE_SEGURANCA,0)            AS ESTOQUE_SEGURANCA,
       X.VALIDADE_MINIMO                        AS VALIDADE_MINIMO,
       
       ISNULL(X.PRECO_MAXIMO,0)                 AS PRECO_MAXIMO,
       ISNULL(X.PRECO_FABRICA,0)                AS PRECO_FABRICA,  
       ISNULL(X.DESCONTO_PADRAO,0)              AS DESCONTO_PADRAO,
       ISNULL(X.DESCONTO_MAXIMO,0)              AS DESCONTO_MAXIMO,
       ISNULL(X.PRECO_VENDA,0)                  AS PRECO_VENDA,
       X.TABELA_PRECO_MAXIMO                    AS TABELA_PRECO_MAXIMO,
       ISNULL(F.DESCRICAO,'PRE�O DE CADASTRO') 
                                                AS TABELA_PRECO_MAXIMO_DESCRICAO,
       X.TABELA_DESCONTO                        AS TABELA_DESCONTO,
       ISNULL(G.DESCRICAO,'DESCONTO DE CADASTRO')   
                                                AS TABELA_DESCONTO_DESCRICAO,
       ISNULL(X.CURVA,'')                       AS CURVA
                
       
  FROM (                                                         
         SELECT A.FILIAL                       AS FILIAL , 
                A.EMPRESA                      AS EMPRESA,
                D.PRODUTO                      AS PRODUTO ,
                D.DESCRICAO                    AS DESCRICAO , 
                C.MES_0                        AS MES_0 , 
                C.MES_1                        AS MES_1 , 
                C.MES_2                        AS MES_2 , 
                C.MES_3                        AS MES_3 , 
                C.MES_4                        AS MES_4 , 
				C.MES_5                        AS MES_5 ,
				C.MES_6                        AS MES_6 ,
                ISNULL(C.DEMANDA_DIA_PONDERADA,0)
                                               AS DEMANDA_DIA_PONDERADA , 
                ISNULL(C.VALOR_BASE,0)         AS VALOR_BASE , 
                ISNULL ( SUM(E.ESTOQUE_SALDO) , 0 ) AS ESTOQUE_SALDO ,         
                ISNULL ( F.SALDO , 0 )         AS PEDIDO,
                ISNULL(G.ESTOQUE_MINIMO,0)     AS ESTOQUE_MINIMO,
                ISNULL(G.ESTOQUE_MAXIMO,0)     AS ESTOQUE_MAXIMO,
                ISNULL(G.ESTOQUE_SEGURANCA,0)  AS ESTOQUE_SEGURANCA,
                G.VALIDADE_MINIMO              AS VALIDADE_MINIMO,
                                
                ISNULL(G.PRECO_MAXIMO,0)       AS PRECO_MAXIMO,
                ISNULL(G.PRECO_FABRICA,0)      AS PRECO_FABRICA,  
                ISNULL(G.DESCONTO_PADRAO,0)    AS DESCONTO_PADRAO,
                ISNULL(G.DESCONTO_MAXIMO,0)    AS DESCONTO_MAXIMO,
                ISNULL(G.PRECO_VENDA,0)        AS PRECO_VENDA,
                ISNULL(G.TABELA_PRECO_MAXIMO,0)AS TABELA_PRECO_MAXIMO,
                ISNULL(G.TABELA_DESCONTO,0)    AS TABELA_DESCONTO,
                ISNULL(C.CURVA,'')             AS CURVA

           FROM #EMPRESAS_ESTOQUES A WITH(NOLOCK)
           JOIN PRODUTOS           D WITH(NOLOCK) ON D.PRODUTO         = @PRODUTO
      LEFT JOIN VALOR_BASE         C WITH(NOLOCK) ON C.EMPRESA         = A.EMPRESA
                                                 AND C.MES             = @MES
                                                 AND C.ANO             = @ANO
                                                 AND C.PRODUTO         = D.PRODUTO
      LEFT JOIN ESTOQUE_ATUAL      E WITH(NOLOCK) ON E.PRODUTO         = D.PRODUTO 
                                                 AND E.CENTRO_ESTOQUE  = A.CENTRO_ESTOQUE
                                                 
      LEFT JOIN ( SELECT A.EMPRESA , SUM ( B.SALDO ) AS SALDO,
                         B.PRODUTO                   AS PRODUTO
                    FROM PEDIDOS_COMPRAS A WITH(NOLOCK)
                    JOIN PEDIDOS_COMPRAS_PRODUTOS_SALDO B WITH(NOLOCK) ON B.PEDIDO_COMPRA = A.PEDIDO_COMPRA 
                                                                      AND B.ESTADO < 1
                    JOIN PRODUTOS                       C WITH(NOLOCK) ON C.PRODUTO = B.PRODUTO
                   WHERE C.PRODUTO = @PRODUTO
                GROUP BY A.EMPRESA , B.PRODUTO ) F ON F.EMPRESA = A.EMPRESA
                                                  AND F.PRODUTO = D.PRODUTO
       LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS    G WITH(NOLOCK) ON G.EMPRESA     = A.EMPRESA
                                                               AND G.PRODUTO     = D.PRODUTO


        GROUP BY A.FILIAL                      , 
                 A.EMPRESA                     ,
                 D.PRODUTO                     ,
                 D.DESCRICAO                   , 
                 C.MES_0                       , 
                 C.MES_1                       , 
                 C.MES_2                       , 
                 C.MES_3                       , 
                 C.MES_4                       , 
				 C.MES_5                       ,  
				 C.MES_6 					   ,
                 ISNULL(C.CURVA,'')            ,
                 ISNULL(C.DEMANDA_DIA_PONDERADA,0),
                 ISNULL(C.VALOR_BASE,0)         , 
                 ISNULL ( F.SALDO , 0 )       ,
                 ISNULL(G.ESTOQUE_MINIMO,0)   ,
                 ISNULL(G.ESTOQUE_MAXIMO,0)   ,
                 ISNULL(G.ESTOQUE_SEGURANCA,0),
                 G.VALIDADE_MINIMO,
                 G.PRECO_MAXIMO,
                 G.PRECO_FABRICA,                
                 G.DESCONTO_PADRAO,
                 G.DESCONTO_MAXIMO,
                 G.PRECO_VENDA,
                 G.TABELA_PRECO_MAXIMO,
                 G.TABELA_DESCONTO ) X
            
      LEFT JOIN ( SELECT MAX(A.DATA) AS ULTIMA_ENTRADA ,
                         B.EMPRESA,                
                         A.PRODUTO   AS PRODUTO
                    FROM ESTOQUE_LANCAMENTOS      A WITH(NOLOCK)
              INNER JOIN NF_COMPRA                B WITH(NOLOCK) ON A.FORMULARIO_ORIGEM = B.FORMULARIO_ORIGEM 
                                                                AND A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM
                                                                AND A.REG_MASTER_ORIGEM = B.NF_COMPRA
                                                                
              INNER JOIN PRODUTOS                 C WITH(NOLOCK) ON C.PRODUTO           = A.PRODUTO 
                   WHERE C.PRODUTO = @PRODUTO
              GROUP BY A.PRODUTO,
                       B.EMPRESA ) A ON A.PRODUTO = X.PRODUTO
                                    AND A.EMPRESA = X.EMPRESA                                 
              
      LEFT JOIN CUSTO_MEDIO_MENSAL                D WITH(NOLOCK) ON D.PRODUTO = X.PRODUTO 
                                                                AND D.MES     = @MES 
                                                                AND D.ANO     = @ANO
      LEFT JOIN ( SELECT MAX(A.MOVIMENTO) AS ULTIMA_VENDA ,
                         A.EMPRESA,                
                         A.PRODUTO   AS PRODUTO
                    FROM VENDAS_ANALITICAS        A WITH(NOLOCK)                                                               
                   WHERE A.PRODUTO = @PRODUTO
                     AND A.GERA_DEMANDA = 1
              GROUP BY A.PRODUTO,
                       A.EMPRESA ) V ON V.PRODUTO = X.PRODUTO
                                    AND V.EMPRESA = X.EMPRESA       
                                                                
      LEFT JOIN ULTIMO_CUSTO                      E WITH(NOLOCK) ON E.PRODUTO = X.PRODUTO
      LEFT JOIN TABELAS_PRECOS_MAXIMO             F WITH(NOLOCK) ON F.TABELA_PRECO_MAXIMO = X.TABELA_PRECO_MAXIMO
      LEFT JOIN TABELAS_DESCONTOS                 G WITH(NOLOCK) ON G.TABELA_DESCONTO     = X.TABELA_DESCONTO
      LEFT JOIN #EMPRESAS_TRANSF_ENTRADA          H WITH(NOLOCK) ON H.PRODUTO             = X.PRODUTO
                                                                AND H.EMPRESA             = X.EMPRESA


ORDER BY X.DESCRICAO, X.FILIAL