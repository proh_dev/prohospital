/**********************************************/
--CONSULTA DE TITULOS A RECEBER POR ENTIDADE --
--DESENVOLVIDA POR YNOA PEDRO 24/09/2018     --
--BASEADA NO RELATORIO DE FICHA FINANCEIRA   --
/**********************************************/

DECLARE @VENCIMENTO_INI          DATETIME
DECLARE @VENCIMENTO_FIM          DATETIME
DECLARE @EMPRESA_INI             NUMERIC
DECLARE @EMPRESA_FIM             NUMERIC
DECLARE @ENTIDADE_INI            NUMERIC
DECLARE @ENTIDADE_FIM            NUMERIC
DECLARE @EMPRESA_CONTABIL        NUMERIC
DECLARE @VENDEDOR                NUMERIC(15)    
DECLARE @GRUPO_CLIENTE           NUMERIC(15,0)
                           
--SET  @VENCIMENTO_INI   = '01/09/2018'      
--SET  @VENCIMENTO_FIM   = '30/09/2018'
--SET  @EMPRESA_INI      = 1000
--SET  @EMPRESA_FIM      = 1000
--SET  @ENTIDADE_INI     = 1
--SET  @ENTIDADE_FIM     = 99999
----SET  @GRUPO_CLIENTE    = NULL

DECLARE @ENTIDADE_CONCENTRADORA NUMERIC(15) = CASE WHEN ISNUMERIC(:ENTIDADE_CONCENTRADORA) =  0 THEN NULL         ELSE :ENTIDADE_CONCENTRADORA END
    SET @VENCIMENTO_INI          = CASE WHEN :VENCIMENTO_INI              = '' THEN '01/01/1900' ELSE :VENCIMENTO_INI     END 
    SET @VENCIMENTO_FIM          = CASE WHEN :VENCIMENTO_FIM              = '' THEN '31/12/2999' ELSE :VENCIMENTO_FIM     END 
    SET @EMPRESA_INI             = CASE WHEN ISNUMERIC(:EMPRESA_INI     ) = 0  THEN 0            ELSE :EMPRESA_INI        END 
    SET @EMPRESA_FIM             = CASE WHEN ISNUMERIC(:EMPRESA_FIM     ) = 0  THEN 99999        ELSE :EMPRESA_FIM        END 
    SET @ENTIDADE_INI            = CASE WHEN ISNUMERIC(:ENTIDADE_INI    ) = 0  THEN 0            ELSE :ENTIDADE_INI       END 
    SET @ENTIDADE_FIM            = CASE WHEN ISNUMERIC(:ENTIDADE_FIM    ) = 0  THEN 999999999999 ELSE :ENTIDADE_FIM       END 
    SET @EMPRESA_CONTABIL        = CASE WHEN ISNUMERIC(:EMPRESA_CONTABIL) = 0  THEN NULL         ELSE :EMPRESA_CONTABIL   END
    SET @VENDEDOR                = CASE WHEN ISNUMERIC(:VENDEDOR        ) = 0  THEN NULL         ELSE :VENDEDOR           END

 
   SELECT 
          A.EMPENHO,
          A.PREGAO,
          F.EMISSAO               AS EMISSAO,
          A.TITULO_RECEBER        AS TITULO_RECEBER ,
          A.EMPRESA               AS EMPRESA ,    
          D.EMPRESA_USUARIA_NOME  AS EMPRESA_NOME,       
          A.TITULO                AS TITULO , 
          A.MOVIMENTO             AS MOVIMENTO , 
          A.VENCIMENTO            AS VENCIMENTO , 
          cast(B.ENTIDADE AS VARCHAR) + ' - ' +
          B.NOME                      + ' - ' +
          B.INSCRICAO_FEDERAL  AS ENTIDADE ,
          A.VALOR                 AS VALOR_ORIGINAL ,
          C.SALDO                 AS SALDO ,
          D.EMPRESA_CONTABIL      AS EMPRESA_CONTABIL,     
          D.EMPRESA_CONTABIL_NOME AS EMPRESA_CONTABIL_NOME,
          E.DESCRICAO             AS MODALIDADE,
          A.ENTIDADE              AS ENTIDADE_FILTRO,
          CAST ( F.VENDEDOR AS VARCHAR)  + ' - '+      H.NOME AS VENDEDOR,
          F.VENDEDOR              AS COD_VENDEDOR,
          ISNULL(I.NUMID,0)                  AS NUMID,
          ISNULL(I.FORMULARIO,'IMPORTACAO')  AS ORIGEM,  --formulario origem 
          F.OPERACAO_FISCAL,
          P.DESCRICAO                       AS GRUPO_CLIENTE,
          CASE WHEN A.NF_LIQUIDADA = 'S' THEN 'LIQUIDADA' ELSE '' END AS NF_LIQUIDADA,
          B.NOME,
          O.DATA_RECEBIMENTO,
          CASE WHEN C.SALDO <= 0 
               THEN A.VALOR                 
               ELSE A.VALOR - C.SALDO
          END                     AS VALOR_RECEBIDO,
	      CASE WHEN ( DATEDIFF(D, GETDATE(), A.VENCIMENTO ) ) < 0 THEN ( DATEDIFF(D, GETDATE(), A.VENCIMENTO ) ) * -1 ELSE 0 END
                       AS DIAS_ATRASO

     FROM TITULOS_RECEBER             A (NOLOCK) 
LEFT JOIN ENTIDADES                   B (NOLOCK) ON A.ENTIDADE                = B.ENTIDADE
LEFT JOIN TITULOS_RECEBER_SALDO       C (NOLOCK) ON A.TITULO_RECEBER          = C.TITULO_RECEBER
     JOIN FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                      D             ON D.EMPRESA_USUARIA      = A.EMPRESA
LEFT JOIN MODALIDADES_TITULOS         E WITH(NOLOCK)ON E.MODALIDADE           = A.MODALIDADE
LEFT JOIN (
            SELECT DISTINCT ENTIDADE FROM BANDEIRAS_CARTOES WITH(NOLOCK)
          )                           G              ON G.ENTIDADE            = A.ENTIDADE 
LEFT JOIN NF_FATURAMENTO              F WITH(NOLOCK) ON A.TAB_MASTER_ORIGEM   = 753289
                                                    AND F.NF_FATURAMENTO      = A.REG_MASTER_ORIGEM
LEFT JOIN VENDEDORES                  H WITH(NOLOCK) ON H.VENDEDOR            = F.VENDEDOR 
LEFT JOIN FORMULARIOS                 I WITH(NOLOCK) ON I.NUMID               = A.FORMULARIO_ORIGEM   --formulario origem 
                                                    
LEFT JOIN NF_FATURAMENTO_OBSERVACOES  L WITH(NOLOCK) ON L.NF_FATURAMENTO      = F.NF_FATURAMENTO
LEFT JOIN LIQUIDACAO_NOTAS_DETALHE    M WITH(NOLOCK) ON M.NF_FATURAMENTO      = F.NF_FATURAMENTO
LEFT JOIN FUNCIONARIOS                N WITH(NOLOCK) ON N.ENTIDADE            = A.ENTIDADE
LEFT JOIN (
            SELECT 
                  A.TITULO_RECEBER   AS TITULO_RECEBER,
                  MAX(A.DATA)        AS DATA_RECEBIMENTO
            
              FROM TITULOS_RECEBER_TRANSACOES A WITH(NOLOCK)
              JOIN TITULOS_RECEBER_SALDO      B WITH(NOLOCK) ON B.TITULO_RECEBER = A.TITULO_RECEBER
            
                 WHERE 1=1
                   AND A.DEBITO > 0
                   AND B.SITUACAO_TITULO = 2
            GROUP BY A.TITULO_RECEBER
          )                           O                      ON O. TITULO_RECEBER      = A.TITULO_RECEBER
LEFT JOIN CLASSIFICACOES_CLIENTES     P WITH(NOLOCK)         ON B.CLASSIFICACAO_CLIENTE = P.CLASSIFICACAO_CLIENTE

    WHERE 1=1                                                                                                                                                  AND
	        C.SITUACAO_TITULO  < 2                                                                                                                             AND                                                                                                                                                                                                                     
            A.VENCIMENTO            >= @VENCIMENTO_INI                                                                                                         AND
            A.VENCIMENTO            <= @VENCIMENTO_FIM                                                                                                         AND
            A.EMPRESA               >= @EMPRESA_INI                                                                                                            AND
            A.EMPRESA               <= @EMPRESA_FIM                                                                                                            AND
            A.ENTIDADE              >= @ENTIDADE_INI                                                                                                           AND
            A.ENTIDADE              <= @ENTIDADE_FIM                                                                                                           AND
           (B.CLASSIFICACAO_CLIENTE  = @GRUPO_CLIENTE  OR @GRUPO_CLIENTE IS NULL)                                                                              AND
           (F.VENDEDOR               =  @VENDEDOR      OR @VENDEDOR   IS NULL)                                                                                 

                                                                       
 ORDER BY D.EMPRESA_CONTABIL, A.EMPRESA, B.NOME, CAST(B.ENTIDADE AS VARCHAR), A.VENCIMENTO,A.EMISSAO