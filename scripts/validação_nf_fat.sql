DECLARE @NF_FATURAMENTO NUMERIC(15)    = :NF_FATURAMENTO
DECLARE @ENTIDADE NUMERIC(15)          = :ENTIDADE
DECLARE @EMPRESA NUMERIC (15)          = :EMPRESA
DECLARE @FORMULARIO_ORIGEM NUMERIC(15) = :FORMULARIO_ORIGEM

-------------------------------------
-- RATEIO DO FRETE NA BASE DE ICMS --
-------------------------------------
EXEC GERAR_RATEIO_BASE_ICMS_NF_FATURAMENTO @NF_FATURAMENTO
EXEC GERAR_RATEIO_DESPESAS_NF_FATURAMENTO  @NF_FATURAMENTO
EXEC GERAR_RATEIO_FRETE_NF_FATURAMENTO     @NF_FATURAMENTO
EXEC GERAR_RATEIO_SEGURO_NF_FATURAMENTO    @NF_FATURAMENTO




-------------------------------------------------------------------------------------
--- VALIDA��O DE OPERA��O FISCAL DE TRANSFER�NCIA / VENDA ENTRE EMPRESAS USU�RIAS ---
-------------------------------------------------------------------------------------
-- CRIADO POR FILIPE OLIVEIRA EM 06/06/2017


DECLARE @OPERACAO_FISCAL_PEDIDO_VAREJO_VENDA   NUMERIC(15)
DECLARE @OPERACAO_FISCAL_PEDIDO_VAREJO_TRANSF  NUMERIC(15)
DECLARE @OPERACAO_FISCAL_CORRETA               NUMERIC(15)
DECLARE @OPERACAO_FISCAL_USADA                 NUMERIC(15) = :OPERACAO_FISCAL
DECLARE @MENSAGEM                              VARCHAR(100)     
    
        SELECT @OPERACAO_FISCAL_PEDIDO_VAREJO_VENDA   = MAX(A.OPERACAO_FISCAL)
          FROM PARAMETROS_VENDAS     A WITH(NOLOCK)
          JOIN EMPRESAS_USUARIAS     B WITH(NOLOCK) ON B.EMPRESA_USUARIA     = A.EMPRESA_USUARIA
                                                   AND B.EMPRESA_USUARIA     = @EMPRESA
    


        SELECT @OPERACAO_FISCAL_PEDIDO_VAREJO_TRANSF = MAX(A.OPERACAO_TRANSF)
          FROM PARAMETROS_VENDAS A WITH(NOLOCK)
          JOIN EMPRESAS_USUARIAS B WITH(NOLOCK) ON  B.EMPRESA_USUARIA = A.EMPRESA_USUARIA
                                                AND B.EMPRESA_USUARIA = @EMPRESA


      SELECT   @OPERACAO_FISCAL_CORRETA = CASE WHEN ( ISNULL(B.EMPRESA_CONTABIL,0) <> ISNULL(D.EMPRESA_CONTABIL,0) )
                                               THEN   @OPERACAO_FISCAL_PEDIDO_VAREJO_VENDA
                                               ELSE   @OPERACAO_FISCAL_PEDIDO_VAREJO_TRANSF
                                           END
      
       FROM NF_FATURAMENTO     A WITH(NOLOCK)
       JOIN EMPRESAS_USUARIAS  B WITH(NOLOCK) ON B.EMPRESA_USUARIA = A.EMPRESA
       JOIN EMPRESAS_USUARIAS  D WITH(NOLOCK) ON D.ENTIDADE        = A.ENTIDADE
       JOIN OPERACOES_FISCAIS  E WITH(NOLOCK) ON E.OPERACAO_FISCAL = A.OPERACAO_FISCAL

      WHERE 1=1 
      AND A.NF_FATURAMENTO = @NF_FATURAMENTO

IF @OPERACAO_FISCAL_USADA IN (SELECT OPERACAO_FISCAL FROM OPERACOES_FISCAIS WITH(NOLOCK) WHERE TIPO_OPERACAO IN (3, 9) ) -- VALIDA SOMENTE VENDA E TRANSFER�NCIA

BEGIN

SELECT @MENSAGEM = 'Opera��o Fiscal indevida! Favor verificar!'
 WHERE     @OPERACAO_FISCAL_CORRETA <> ISNULL(@OPERACAO_FISCAL_USADA, @OPERACAO_FISCAL_CORRETA) 
       AND @EMPRESA <> @ENTIDADE

END


---------------------------------------------------------------------------------------------------------------------------------------

---------------------------------
-- VALIADA TIPO DE NOTA FISCAL --
---------------------------------
SELECT TOP 1
       'A Empresa Usu�ria "'+CONVERT(VARCHAR(15),A.EMPRESA)+
       '" N�O est� parametrizada com a Esp�cie "'+A.NF_ESPECIE+'" e Serie "'+A.NF_SERIE+
       '". Por favor verifique o Cadastro da Empresa Usu�ria.'
  FROM NF_FATURAMENTO A WITH(NOLOCK)
  LEFT JOIN PARAMETROS_NF_FATURAMENTO B WITH(NOLOCK) ON B.EMPRESA_USUARIA = A.EMPRESA
                                                    AND B.ESPECIE = A.NF_ESPECIE
                                                    AND B.SERIE = A.NF_SERIE
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
   AND B.PARAMETRO_NF IS NULL

UNION ALL

-----------------------------------------------------------------------------------------------
--TRAVA PARA NAO PERMITIR OPERACAO FISCAL NA MASTER DIFERENTE DA OPERACAO FISCAL DOS PRODUTOS--
-----------------------------------------------------------------------------------------------

SELECT TOP 1 'Opera��o Fiscal Utilizada no Produto ' + CONVERT(VARCHAR(15),A.PRODUTO) + ' - ' + 
             C.DESCRICAO + ' Divergente da Opera��o da Nota Fiscal, Favor Corrigir!'
FROM NF_FATURAMENTO_PRODUTOS A WITH(NOLOCK)
INNER JOIN NF_FATURAMENTO    B WITH(NOLOCK)ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
INNER JOIN PRODUTOS          C WITH(NOLOCK)ON C.PRODUTO        = A.PRODUTO
WHERE A.OPERACAO_FISCAL <> B.OPERACAO_FISCAL
  AND A.NF_FATURAMENTO   = @NF_FATURAMENTO

UNION ALL

SELECT DISTINCT 'Aten��o, Entidade Diferente da Ficha de Dados do Destinat�rio, Favor Verificar!'
      FROM NF_FATURAMENTO          A WITH(NOLOCK)
INNER JOIN NF_FATURAMENTO_ENTIDADE B WITH(NOLOCK)ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
WHERE A.ENTIDADE <> B.ENTIDADE
  AND A.NF_FATURAMENTO = @NF_FATURAMENTO

UNION ALL

SELECT TOP 1 'Empresa Emissora Difere da Filial do Cupom Fiscal'
  FROM NF_FATURAMENTO         A WITH(NOLOCK)
  JOIN NF_FATURAMENTO_CUPOM   B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
 
 WHERE A.EMPRESA <> B.FILIAL
   AND A.NF_FATURAMENTO =@NF_FATURAMENTO

UNION ALL



SELECT 'Produto ('+CAST(B.PRODUTO as varchar(15))+'-'+cast(d.descricao as varchar(45))+')'+char(13)+'Obrigat�rio informar LOTE/VALIDADE' 
       FROM NF_FATURAMENTO       A WITH(NOLOCK)
       JOIN NF_FATURAMENTO_PRODUTOS       B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
       JOIN PRODUTOS_RASTREABILIDADE C WITH(NOLOCK) ON C.PRODUTO = B.PRODUTO
       JOIN PRODUTOS                 D WITH(NOLOCK) ON D.PRODUTO = B.PRODUTO
       WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
         AND C.CONTROLE_RASTREABILIDADE = 'S'
         AND ( ISNULL(B.LOTE,'')='' OR ISNULL(B.VALIDADE_DIGITACAO,'')='' ) 
         AND EXISTS(SELECT * FROM NF_FATURAMENTO       A WITH(NOLOCK)
                             JOIN NF_FATURAMENTO_PRODUTOS       B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
                             JOIN PRODUTOS_RASTREABILIDADE C WITH(NOLOCK) ON C.PRODUTO = B.PRODUTO
                             JOIN PRODUTOS                 D WITH(NOLOCK) ON D.PRODUTO = B.PRODUTO
                             WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
                               AND C.CONTROLE_RASTREABILIDADE = 'S'
                               AND ( ISNULL(B.LOTE,'')='' OR ISNULL(B.VALIDADE_DIGITACAO,'')='' ) )
        
		AND A.EMPRESA = 1000 -- SOMENTE EMPRESA MATRIZ
UNION ALL

------------------------------------------------------
-- VALIDA PARCELAS -- 
------------------------------------------------------

SELECT 'Total das Parcelas '           + CAST(ISNULL ( SUM ( C.VALOR ) , 0 ) AS VARCHAR(15)) + 
       ' N�o Confere Com Total Geral ' + CAST(MAX(B.TOTAL_GERAL) AS VARCHAR(15))     

  FROM NF_FATURAMENTO             A WITH(NOLOCK)

  LEFT JOIN NF_FATURAMENTO_TOTAIS B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO

  LEFT JOIN ( 
             SELECT A.NF_FATURAMENTO , ISNULL(SUM(A.VALOR),0) AS VALOR
               FROM NF_FATURAMENTO_PARCELAS A WITH(NOLOCK)
              WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
			  GROUP BY A.NF_FATURAMENTO
              UNION ALL
             SELECT A.NF_FATURAMENTO , ISNULL(SUM(A.VALOR),0) AS VALOR
               FROM NF_FATURAMENTO_BOLETOS A WITH(NOLOCK)
              WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
              GROUP BY A.NF_FATURAMENTO
              UNION ALL
             SELECT A.NF_FATURAMENTO , ISNULL(SUM(A.VALOR),0) AS VALOR
               FROM NF_FATURAMENTO_DEPOSITOS A WITH(NOLOCK)
              WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
              GROUP BY A.NF_FATURAMENTO
              UNION ALL
             SELECT A.NF_FATURAMENTO , ISNULL(SUM(A.VALOR),0) AS VALOR
               FROM NF_FATURAMENTO_CHEQUES A WITH(NOLOCK)
              WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
              GROUP BY A.NF_FATURAMENTO
              UNION ALL
             SELECT A.NF_FATURAMENTO , ISNULL(SUM(A.VALOR),0) AS VALOR
               FROM NF_FATURAMENTO_CARTOES A WITH(NOLOCK)
              WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO 
             GROUP BY A.NF_FATURAMENTO
            
	  	  ) C ON C.NF_FATURAMENTO = A.NF_FATURAMENTO

 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO

 
 AND ( SELECT COUNT(*) 
         FROM NF_FATURAMENTO_PRODUTOS        A WITH(NOLOCK) 
         JOIN OPERACOES_FISCAIS              B WITH(NOLOCK) ON A.OPERACAO_FISCAL = B.OPERACAO_FISCAL
        WHERE A.NF_FATURAMENTO   = @NF_FATURAMENTO
          AND A.OPERACAO_FISCAL  = B.OPERACAO_FISCAL 
          AND B.GERAR_FINANCEIRO = 'S' 
          ) >= 1
 
 GROUP BY A.NF_FATURAMENTO ,
          B.TOTAL_GERAL 

   HAVING ABS( MAX(ISNULL(B.TOTAL_GERAL,0)) - ISNULL ( SUM ( C.VALOR ) , 0 ) ) > 0


UNION ALL

------------------------------------------
--CHECAGEM SE A NOTA FOI EMITIDA  SEFAZ --
------------------------------------------

SELECT TOP 1 'ATEN��O!!!!!.Nota Fiscal J� Processada no SEFAZ. N�o pode Ser Alterada'

  FROM NFE_CABECALHO   A WITH(NOLOCK)
  JOIN NFE_LOG         B WITH(NOLOCK) ON A.REGISTRO_NFE      = B.REGISTRO_NFE
                                     AND B.STATUS            = 'XML DO DESTINATARIO'
  JOIN NF_FATURAMENTO  C WITH(NOLOCK) ON C.NF_FATURAMENTO    = A.XML_cNF_B03
                                     AND C.NF_NUMERO         = A.XML_nNF_B08
                                     AND C.NF_SERIE          = A.XML_serie_B07
                                     AND C.EMPRESA           = A.EMPRESA

  WHERE C.NF_FATURAMENTO = @NF_FATURAMENTO
  

UNION ALL

SELECT
    'A quantidade do produto '+CONVERT(VARCHAR(18),A.PRODUTO) +' - '+DBO.PROPERCASE(C.DESCRICAO_REDUZIDA)+
    ' est� diferente do pedido:'+CHAR(10)+CHAR(13)+
    'Qtde. Pedido: '+CONVERT(VARCHAR(18),B.QUANTIDADE)+ CHAR(10)+CHAR(13)+
    'Qtde. NF: '+CONVERT(VARCHAR(18),A.QUANTIDADE)
FROM
    (   SELECT
            A.NF_FATURAMENTO    AS NF_FATURAMENTO,
            B.PRODUTO           AS PRODUTO,
            SUM(B.QUANTIDADE_ESTOQUE)   AS QUANTIDADE
        FROM 
            NF_FATURAMENTO                  A WITH(NOLOCK)
            JOIN NF_FATURAMENTO_PRODUTOS    B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
        WHERE 
            A.NF_FATURAMENTO = @NF_FATURAMENTO
        GROUP BY 
            A.NF_FATURAMENTO,
            B.PRODUTO
     ) A
     JOIN   (
                SELECT
                    A.NF_FATURAMENTO    AS NF_FATURAMENTO,
                    B.PRODUTO           AS PRODUTO,
                    B.QUANTIDADE        AS QUANTIDADE
                FROM
                    NF_FATURAMENTO                  A WITH(NOLOCK) 
                    JOIN PEDIDOS_VENDAS_PRODUTOS    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
                WHERE 
                    A.NF_FATURAMENTO = @NF_FATURAMENTO

                UNION ALL

                SELECT
                    A.NF_FATURAMENTO    AS NF_FATURAMENTO,
                    B.PRODUTO           AS PRODUTO,
                    B.QUANTIDADE        AS QUANTIDADE
                FROM
                    NF_FATURAMENTO                           A WITH(NOLOCK) 
                    JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
                WHERE 
                    A.NF_FATURAMENTO = @NF_FATURAMENTO
            ) B     ON A.PRODUTO        = B.PRODUTO
                   AND A.NF_FATURAMENTO = B.NF_FATURAMENTO
    JOIN PRODUTOS  C WITH(NOLOCK) ON A.PRODUTO = C.PRODUTO
WHERE 

    A.QUANTIDADE <> B.QUANTIDADE

AND 1=2 -- PROVISORIAMENTE COLOCADA EM 11/02/2017 POR FILIPE OLIVEIRA

UNION ALL

SELECT
    'O produto '+CONVERT(VARCHAR(18),B.PRODUTO) +' - '+DBO.PROPERCASE(D.DESCRICAO_REDUZIDA)+
    ' n�o faz parte do pedido'+CHAR(10)
FROM
    NF_FATURAMENTO                  A WITH(NOLOCK)
    JOIN NF_FATURAMENTO_PRODUTOS    B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
    LEFT JOIN   (
                    SELECT
                        A.NF_FATURAMENTO    AS NF_FATURAMENTO,
                        B.PRODUTO           AS PRODUTO
                    FROM
                        NF_FATURAMENTO                  A WITH(NOLOCK) 
                        JOIN PEDIDOS_VENDAS_PRODUTOS    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
                    WHERE 
                        A.NF_FATURAMENTO = @NF_FATURAMENTO

                    UNION ALL

                    SELECT
                        A.NF_FATURAMENTO    AS NF_FATURAMENTO,
                        B.PRODUTO           AS PRODUTO
                    FROM
                        NF_FATURAMENTO                           A WITH(NOLOCK) 
                        JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
                    WHERE 
                        A.NF_FATURAMENTO = @NF_FATURAMENTO
                ) C     ON B.PRODUTO        = C.PRODUTO
                       AND B.NF_FATURAMENTO = C.NF_FATURAMENTO
    JOIN PRODUTOS D WITH(NOLOCK) ON B.PRODUTO = D.PRODUTO

WHERE 

    A.NF_FATURAMENTO =  @NF_FATURAMENTO
    AND c.NF_FATURAMENTO IS NULL

AND 0=1

UNION ALL

SELECT
    'Falta incluir na nota o produto '+CONVERT(VARCHAR(18),C.PRODUTO) +' - '+DBO.PROPERCASE(D.DESCRICAO_REDUZIDA)    
FROM
    NF_FATURAMENTO                  A WITH(NOLOCK)
    JOIN   (
                    SELECT
                        A.NF_FATURAMENTO    AS NF_FATURAMENTO,
                        B.PRODUTO           AS PRODUTO
                    FROM
                        NF_FATURAMENTO                  A WITH(NOLOCK) 
                        JOIN PEDIDOS_VENDAS_PRODUTOS    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
                    WHERE 
                        A.NF_FATURAMENTO = @NF_FATURAMENTO

                    UNION ALL

                    SELECT
                        A.NF_FATURAMENTO    AS NF_FATURAMENTO,
                        B.PRODUTO           AS PRODUTO
                    FROM
                        NF_FATURAMENTO                           A WITH(NOLOCK) 
                        JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
                    WHERE 
                        A.NF_FATURAMENTO = @NF_FATURAMENTO
                )                       C              ON A.NF_FATURAMENTO = C.NF_FATURAMENTO
    LEFT JOIN NF_FATURAMENTO_PRODUTOS   B WITH(NOLOCK) ON C.NF_FATURAMENTO = B.NF_FATURAMENTO
                                                      AND C.PRODUTO        = B.PRODUTO
    JOIN PRODUTOS D WITH(NOLOCK) ON C.PRODUTO = D.PRODUTO

WHERE 

    A.NF_FATURAMENTO = @NF_FATURAMENTO
    AND B.NF_FATURAMENTO IS NULL
AND 0=1

UNION ALL

--------------------------------------------------------------
-- VALIDA��O SE EXISTE A PARAMETRIZA��O DO GRUPO TRIBUT�RIO --
-- ALBERTO EM 26/04/2014                                    --
--------------------------------------------------------------

SELECT 'Parametriza��o n�o Encontrada para: ' + CHAR(13) +
       'Grupo Tribut�rio: ' + LTRIM(RTRIM(STR(E.GRUPO_TRIBUTARIO))) +  CHAR(13) +
       'Opera��o Fiscal.: ' + LTRIM(RTRIM(STR(A.OPERACAO_FISCAL ))) +  CHAR(13) +
       'Estado Origem...: ' + CASE WHEN H.NFE_TIPO_OPERACAO = '1' THEN C.ESTADO ELSE D.ESTADO END + CHAR(13) +
       'Estado Destino..: ' + CASE WHEN H.NFE_TIPO_OPERACAO = '1' THEN D.ESTADO ELSE C.ESTADO END + CHAR(13)

  FROM NF_FATURAMENTO_PRODUTOS            A WITH(NOLOCK)
  JOIN OPERACOES_FISCAIS                  H WITH(NOLOCK) ON H.OPERACAO_FISCAL   = A.OPERACAO_FISCAL
  JOIN EMPRESAS_USUARIAS                  B WITH(NOLOCK) ON B.EMPRESA_USUARIA   = @EMPRESA
  JOIN ENDERECOS                          C WITH(NOLOCK) ON C.ENTIDADE          = B.ENTIDADE -- ENDERE�O DA EMPRESA USUARIA
  JOIN ENDERECOS                          D WITH(NOLOCK) ON D.ENTIDADE          = @ENTIDADE  -- ENDERE�O DA ENTIDADE
  JOIN PRODUTOS                           E WITH(NOLOCK) ON E.PRODUTO           = A.PRODUTO
  JOIN GRUPOS_TRIBUTARIOS                 F WITH(NOLOCK) ON F.GRUPO_TRIBUTARIO  = E.GRUPO_TRIBUTARIO
  LEFT JOIN GRUPOS_TRIBUTARIOS_PARAMETROS G WITH(NOLOCK) ON G.GRUPO_TRIBUTARIO  = F.GRUPO_TRIBUTARIO
                                                       AND  G.OPERACAO_FISCAL   = A.OPERACAO_FISCAL
						       AND((H.NFE_TIPO_OPERACAO = '0'        -- NF ENTRADA 
					               AND  G.ESTADO_DESTINO    = C.ESTADO   -- UF DA EMPRESA
						       AND  G.ESTADO_ORIGEM     = D.ESTADO)  -- UF DA ENTIDADE
						        OR (H.NFE_TIPO_OPERACAO = '1'        -- NF SAIDA
						       AND  G.ESTADO_ORIGEM     = C.ESTADO   -- UF DA EMPRESA
						       AND  G.ESTADO_DESTINO    = D.ESTADO)) -- UF DA ENTIDADE

 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
   AND G.GRUPO_TRIBUTARIO IS NULL
 
  UNION ALL

 SELECT TOP 1 'ATEN��O: A Entidade n�o pode conter o mesmo registro da Empresa!'
   FROM NF_FATURAMENTO          A WITH(NOLOCK)
   JOIN OPERACOES_FISCAIS       B WITH(NOLOCK)ON B.OPERACAO_FISCAL = A.OPERACAO_FISCAL
  WHERE B.TIPO_OPERACAO NOT IN ( 18, 17, 7 )
    AND A.EMPRESA        = A.ENTIDADE
    AND A.NF_FATURAMENTO = @NF_FATURAMENTO          

   UNION ALL
   -----------------------------------------------------------------------------------------------
   --  C�LIO LOZATTO 27/01/15 ATIVIDADE CLAITON
   -----------------------------------------------------------------------------------------------
SELECT TOP 1 'Aten��o !!! O centro de Estoque informado nos itens n�o pertence a Empresa da Nota, Verifique'
  FROM NF_FATURAMENTO          A WITH(NOLOCK)
  JOIN NF_FATURAMENTO_PRODUTOS B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
  LEFT JOIN CENTROS_ESTOQUE    C WITH(NOLOCK) ON C.EMPRESA        = A.EMPRESA
                                             AND C.OBJETO_CONTROLE = B.OBJETO_CONTROLE
WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
  AND C.OBJETO_CONTROLE IS NULL

UNION ALL

-- Jo�o Vitor, 12/01/2015
SELECT top 1 'O c�digo de IBGE do munic�pio � inv�lido. Por favor, verifique o campo na guia de dados dos destinat�rio!'
  FROM NF_FATURAMENTO_ENTIDADE A WITH(NOLOCK)
  LEFT JOIN MUNICIPIOS		   B WITH(NOLOCK) ON B.MUNICIPIO = A.MUNICIPIO
 WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
   AND (
            A.MUNICIPIO IS NULL
         OR A.CODIGO_IBGE <> B.CODIGO_IBGE
	   )


-------------------------------------------------------
-- **INCLUIDA TRAVA POR FILIPE OLIVEIRA - 25/01/2016 **
-------------------------------------------------------
-- Trava para impedir que um cupom que tenha nota em substitui��o pago como Cart�o de cr�dito ou d�bito, dinhero, cheque ou outros tenham t�tulos gerados em duplicidade --
-- Trava tamb�m que notas de cupons que sejam boleto, sejam geradas sem a gera��o das parcelas.


   
   UNION ALL

SELECT DISTINCT 'N�o pode ser feito t�tulos financeiros quando a venda for finalizada em '+F.DESCRICAO +' Cupom n� ' +CAST(B.ECF_CUPOM AS VARCHAR)
FROM  NF_FATURAMENTO_CUPOM     A WITH(NOLOCK)
JOIN  PDV_VENDAS               B WITH(NOLOCK) ON A.CUPOM          = B.ECF_CUPOM
                                             AND A.FILIAL         = B.LOJA
                                             AND A.MOVIMENTO      = B.MOVIMENTO
JOIN  NF_FATURAMENTO           C WITH(NOLOCK) ON A.NF_FATURAMENTO = C.NF_FATURAMENTO
JOIN  PDV_FINALIZADORAS        D WITH(NOLOCK) ON B.VENDA          = D.VENDA
                                             AND B.CAIXA          = D.CAIXA
                                             AND B.MOVIMENTO      = D.MOVIMENTO
JOIN  NF_FATURAMENTO_PARCELAS  E WITH(NOLOCK) ON E.NF_FATURAMENTO = C.NF_FATURAMENTO
JOIN  TIPOS_FINALIZADORAS      F WITH(NOLOCK) ON D.TIPO           = F.TIPO

WHERE 1=1
AND D.TIPO      IN (1,2,3,4,5,6,8,9)
AND C.NF_FATURAMENTO =  @NF_FATURAMENTO

   UNION ALL

SELECT DISTINCT 'A nota n�o pode ser gerada sem t�tulos financeiros quando a venda for finalizada como '+F.DESCRICAO +' Cupom n� ' +CAST(B.ECF_CUPOM AS VARCHAR)
FROM       NF_FATURAMENTO_CUPOM     A WITH(NOLOCK)
JOIN       PDV_VENDAS               B WITH(NOLOCK) ON A.CUPOM          = B.ECF_CUPOM
                                                  AND A.FILIAL         = B.LOJA
                                                  AND A.MOVIMENTO      = B.MOVIMENTO
JOIN       NF_FATURAMENTO           C WITH(NOLOCK) ON A.NF_FATURAMENTO = C.NF_FATURAMENTO
JOIN       PDV_FINALIZADORAS        D WITH(NOLOCK) ON B.VENDA          = D.VENDA
                                                  AND B.CAIXA          = D.CAIXA
                                                  AND B.MOVIMENTO      = D.MOVIMENTO
LEFT JOIN  NF_FATURAMENTO_PARCELAS  E WITH(NOLOCK) ON E.NF_FATURAMENTO = C.NF_FATURAMENTO
JOIN       TIPOS_FINALIZADORAS      F WITH(NOLOCK) ON D.TIPO           = F.TIPO

WHERE 1=1
AND D.TIPO      IN (7)
AND C.NF_FATURAMENTO =  @NF_FATURAMENTO
AND E.NF_FATURAMENTO IS NULL



-------------------------------------------------------
-- **INCLUIDA TRAVA POR FILIPE OLIVEIRA - 11/05/2016 **
-------------------------------------------------------
-- Trava para n�o permitir diferenc�as entre a nota e o checkout --

   
   UNION ALL

SELECT DISTINCT 'Nota Fiscal com produtos diferentes do que foi passado no checkout!'

FROM NF_FATURAMENTO_TRANSF                 A WITH(NOLOCK)
JOIN CONFERENCIAS_ESTOQUE_TRANF            D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA     = A.ESTOQUE_TRANSFERENCIA
LEFT
JOIN CONFERENCIAS_ESTOQUE_TRANF_PRODUTOS   E WITH(NOLOCK) ON E.CONFERENCIA_TRANSFERENCIA = D.CONFERENCIA_TRANSFERENCIA
JOIN NF_FATURAMENTO                        B WITH(NOLOCK) ON B.NF_FATURAMENTO            = A.NF_FATURAMENTO
LEFT
JOIN NF_FATURAMENTO_PRODUTOS               C WITH(NOLOCK) ON C.NF_FATURAMENTO            = A.NF_FATURAMENTO
                                                         AND C.PRODUTO                   = E.PRODUTO

WHERE 1=1
AND A.NF_FATURAMENTO = @NF_FATURAMENTO
AND C.PRODUTO IS NULL

GROUP BY A.ESTOQUE_TRANSFERENCIA, E.PRODUTO

HAVING SUM(C.QUANTIDADE_ESTOQUE) <> SUM(E.QUANTIDADE)


-------------------------------------------------------
-- **INCLUIDA TRAVA POR FILIPE OLIVEIRA - 17/05/2016 **
-------------------------------------------------------
-- Trava para n�o permitir emitir nota sem produto --

   UNION ALL

SELECT DISTINCT 'Nota Fiscal sem produtos! Favor corrigir!'
FROM NF_FATURAMENTO                        B WITH(NOLOCK) 
LEFT
JOIN NF_FATURAMENTO_PRODUTOS               C WITH(NOLOCK) ON C.NF_FATURAMENTO            = B.NF_FATURAMENTO

WHERE 1=1
AND B.NF_FATURAMENTO = @NF_FATURAMENTO
AND C.PRODUTO IS NULL


UNION ALL 

    SELECT @MENSAGEM WHERE @MENSAGEM IS NOT NULL

-------------------------------------------------------
-- **INCLUIDA TRAVA POR FILIPE OLIVEIRA - 09/06/2017 **
-------------------------------------------------------
-- Trava para clientes p�blicos sem Empenho --

UNION ALL

SELECT 'Esse cliente � classificado como �rg�o P�blico e exige o preenchimento do Preg�o/Empenho!'

  FROM NF_FATURAMENTO_OBSERVACOES A WITH(NOLOCK)
  JOIN NF_FATURAMENTO             B WITH(NOLOCK) ON B.NF_FATURAMENTO           = A.NF_FATURAMENTO
  JOIN PESSOAS_JURIDICAS          C WITH(NOLOCK) ON C.ENTIDADE                 = B.ENTIDADE
  JOIN CLASSIFICACOES_CLIENTES    D WITH(NOLOCK) ON D.CLASSIFICACAO_CLIENTE    = C.CLASSIFICACAO_CLIENTE

WHERE 1=1
 AND A.NF_FATURAMENTO            = @NF_FATURAMENTO
 AND D.EXIGIR_EMPENHO            = 'S'
 AND LEN(isnull(A.EMPENHO,''))   < 1
 AND LEN(isnull(A.PREGAO ,''))   < 1

 --------------------------------------------------------------
 -- TRAVA QUE OBRIGA USER A PREENCHER ABA FORMA DE PAGAMENTO --
 --------------------------------------------------------------

UNION ALL

SELECT 'Obrigat�rio Preencher a aba F4 - Forma de pagamento!' 
FROM NF_FATURAMENTO                    A WITH(NOLOCK) 
LEFT
JOIN NF_FATURAMENTO_FORMAS_PAGAMENTOS  B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO
WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO
  AND B.NF_FATURAMENTO_FORMA_PAGAMENTO IS NULL

 --------------------------------------------------------------
 -- TRAVA QUE OBRIGA USER A PREENCHER ABA FORMA DE PAGAMENTO --
 -- Adicionado por Ynoa Pedro 20/07/2018                     --
 --------------------------------------------------------------
UNION ALL
  
SELECT 'Obrigat�rio Preencher a aba F4 - Forma de pagamento!' 
FROM NF_FATURAMENTO                    A WITH(NOLOCK) 
LEFT
JOIN NF_FATURAMENTO_FORMAS_PAGAMENTOS  B WITH(NOLOCK) ON A.NF_FATURAMENTO = B.NF_FATURAMENTO

WHERE A.NF_FATURAMENTO = @NF_FATURAMENTO


