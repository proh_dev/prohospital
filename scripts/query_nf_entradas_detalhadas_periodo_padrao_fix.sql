/*
--  Altera��o e melhoria em consulta de nf digitadas no periodo
--  Autor = Yno� Pedro
--  20/03/2018 
--  30/08/2018 --  Nova Altera��o!
*/

DECLARE @EMPRESA_INI            NUMERIC
DECLARE @EMPRESA_FIM            NUMERIC
DECLARE @NCREDOR                NUMERIC  
DECLARE @FORMULARIO_PROCESSO    NUMERIC                                                                                   
DECLARE @MOVIMENTO_INI          DATE 
DECLARE @MOVIMENTO_FIM          DATE


--SET @MOVIMENTO_INI         = :MOVIMENTO_INI      
--SET @MOVIMENTO_FIM         = :MOVIMENTO_FIM      
--IF ISNUMERIC(:EMPRESA_INI) = 1 SET @EMPRESA_INI           = :EMPRESA_INI           ELSE SET @EMPRESA_INI           = NULL
--IF ISNUMERIC(:EMPRESA_FIM) = 1 SET @EMPRESA_FIM           = :EMPRESA_FIM           ELSE SET @EMPRESA_FIM           = NULL
--IF ISNUMERIC(:NCREDOR)     = 1 SET @NCREDOR               = :NCREDOR               ELSE SET @NCREDOR               = NULL
--IF ISNUMERIC(:NUMID)       = 1 SET @FORMULARIO_PROCESSO   = :NUMID                 ELSE SET @FORMULARIO_PROCESSO   = NULL
                                                                                         
 
SET @MOVIMENTO_INI            = '01/01/2018'
SET @MOVIMENTO_FIM            = '14/06/2018'
SET @EMPRESA_INI              = 1000      
SET @EMPRESA_FIM              = 1000
SET @NCREDOR                  = NULL   

  
  
      SELECT
              48                         AS FORMULARIO_ORIGEM   
             ,41                         AS TAB_MASTER_ORIGEM
             ,A.NF_COMPRA                AS REG_MASTER_ORIGEM

             ,A.NF_COMPRA                AS NF_COMPRA
             ,A.EMPRESA                  AS EMPRESA 
             ,A.EMISSAO                  AS EMISSAO
             ,A.MOVIMENTO                AS MOVIMENTO
             --,A.DATA_PROCESSAMENTO       AS PROCESSAMENTO           
             ,A.ENTIDADE                 AS ENTIDADE
             ,C.NOME                     AS CREDOR 
             ,A.NF_NUMERO                AS NF_NUMERO 
             ,E.TOTAL_GERAL              AS VALOR
             --,CASE WHEN D.ICMS_CREDITO = 'S'
             --            THEN SUM(D.ICMS_VALOR) 
             --          ELSE 0
             --            END            AS ICMS_VALOR 

            
                     
        FROM NF_COMPRA                  A WITH (NOLOCK)    
        LEFT JOIN EMPRESAS_USUARIAS     B WITH (NOLOCK)ON B.EMPRESA_USUARIA           = A.EMPRESA        
        LEFT JOIN ENTIDADES             C WITH (NOLOCK)ON C.ENTIDADE                  = A.ENTIDADE 
             JOIN NF_COMPRA_TOTAIS      E WITH (NOLOCK)ON E.NF_COMPRA                 = A.NF_COMPRA
               
       WHERE 1=1
             AND ( ISNULL(A.DATA_RECEBIMENTO,A.MOVIMENTO)   >=  @MOVIMENTO_INI                                   )  
             AND ( ISNULL(A.DATA_RECEBIMENTO,A.MOVIMENTO)   <=  @MOVIMENTO_FIM                                   )     
             AND (A.EMPRESA                                 >=  @EMPRESA_INI   OR @EMPRESA_INI            IS NULL)
             AND (A.EMPRESA                                 <=  @EMPRESA_FIM   OR @EMPRESA_FIM            IS NULL)
             AND (A.ENTIDADE                                 =  @NCREDOR       OR @NCREDOR                IS NULL)
			 AND ( @FORMULARIO_PROCESSO                      = 48              OR @FORMULARIO_PROCESSO   IS NULL )
             AND ISNULL(A.PROCESSAR,'N')                     =  'S'              
             
GROUP BY A.NF_COMPRA,
A.EMPRESA,
A.ENTIDADE ,
C.NOME ,
A.EMISSAO ,
A.NF_NUMERO, 
A.MOVIMENTO, 
A.DATA_PROCESSAMENTO , 
E.TOTAL_GERAL,
A.FORMULARIO_ORIGEM,
A.TAB_MASTER_ORIGEM         
--ORDER BY C.NOME                                                                                                                     

UNION ALL

      
      SELECT                      
                     177982                      AS FORMULARIO_ORIGEM   
					,773844                      AS TAB_MASTER_ORIGEM
					,A.NF_FATURAMENTO_DEVOLUCAO  AS REG_MASTER_ORIGEM
												 
					,A.NF_FATURAMENTO_DEVOLUCAO  AS NF_COMPRA
					,A.EMPRESA                   AS EMPRESA 
					,A.EMISSAO                   AS EMISSAO
					,A.MOVIMENTO                 AS MOVIMENTO
                    ,A.NF_NUMERO                 AS NF_NUMERO             
					,A.ENTIDADE                  AS ENTIDADE
					,C.NOME                      AS CREDOR 
					,E.TOTAL_GERAL               AS VALOR
	
              FROM NF_FATURAMENTO_DEVOLUCOES          A WITH (NOLOCK)											 
			  LEFT JOIN EMPRESAS_USUARIAS             B WITH (NOLOCK)ON B.EMPRESA_USUARIA           = A.EMPRESA			  						             		 
			  LEFT JOIN ENTIDADES                     C WITH (NOLOCK)ON C.ENTIDADE                  = A.ENTIDADE  
			       JOIN NF_FATURAMENTO_TOTAIS         E WITH (NOLOCK)ON E.NF_FATURAMENTO            = A.NF_FATURAMENTO_DEVOLUCAO
										                
              
              WHERE 1=1
              AND ( A.MOVIMENTO            >= @MOVIMENTO_INI   OR @MOVIMENTO_INI               IS NULL )
              AND ( A.MOVIMENTO            <= @MOVIMENTO_FIM   OR @MOVIMENTO_FIM               IS NULL )
              AND ( A.EMPRESA              >= @EMPRESA_INI     OR @EMPRESA_INI                 IS NULL )
              AND ( A.EMPRESA              <= @EMPRESA_FIM     OR @EMPRESA_FIM                 IS NULL )
              AND ( A.ENTIDADE              = @NCREDOR         OR @NCREDOR                     IS NULL )
              AND ( @FORMULARIO_PROCESSO    = 177982           OR @FORMULARIO_PROCESSO         IS NULL )

GROUP BY A.NF_FATURAMENTO_DEVOLUCAO,
 A.EMPRESA, 
 A.ENTIDADE ,
 C.NOME, 
 A.EMISSAO ,
 A.NF_NUMERO,
 A.MOVIMENTO,
 E.TOTAL_GERAL,
 A.FORMULARIO_ORIGEM,
 A.TAB_MASTER_ORIGEM,
 A.EMITIR_NFE,
 --D.ICMS_VALOR,
 A.NF_NUMERO_CLIENTE
             