-------------------------------------------------------
-- QUERY PEDIDOS PENDENTES DE FATURAMENTO HOJE EMAIL --
-- DESENVOLVEDOR: YNOA PEDRO                         --
-- DATA 16-10-2018                                   --
-- FATURAMENTOS_FUTUROS                              --
-------------------------------------------------------

SELECT  
	 A.PEDIDO_VENDA                     AS PEDIDO_VENDA
    ,A.USUARIO_LOGADO                   AS USUARIO
	,D.NOME                             AS SOLICITANTE
	,A.EMPRESA                          AS EMPRESA
	,B.NOME                             AS EMPRESA_NOME
	,A.ENTIDADE                         AS ENTIDADE
	,C.NOME                             AS ENTIDADE_NOME
	,A.DATA_FATURAMENTO_PREVISTA        AS DATA_PREVISTA_FAT

    FROM PEDIDOS_VENDAS                  A WITH(NOLOCK)
    JOIN EMPRESAS_USUARIAS               B WITH(NOLOCK) ON A.EMPRESA        = B.EMPRESA_USUARIA
    JOIN ENTIDADES                       C WITH(NOLOCK) ON A.ENTIDADE       = C.ENTIDADE
LEFT JOIN USUARIOS                       D WITH(NOLOCK) ON A.USUARIO_LOGADO = D.USUARIO 

    WHERE 1=1
      AND A.DATA_FATURAMENTO_PREVISTA =  CAST(GETDATE() AS DATE)
      AND A.FATURAMENTO_FUTURO        = 'S'
 