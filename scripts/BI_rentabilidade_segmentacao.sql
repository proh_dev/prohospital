   SELECT A.EMPRESA                                              AS EMPRESA , 
          MONTH ( A.MOVIMENTO )                                  AS MES , 
          YEAR  ( A.MOVIMENTO )                                  AS ANO ,
          D.DESCRICAO                                            AS SECAO , 
          E.DESCRICAO                                            AS GRUPO , 
          F.DESCRICAO                                            AS SUBGRUPO , 
          G.DESCRICAO                                            AS MARCA , 
          SUM ( A.VENDA_BRUTA   )                                AS VENDA_BRUTA ,
          SUM ( A.DESCONTO      )                                AS DESCONTO , 
          SUM ( A.VENDA_LIQUIDA )                                AS VENDA_LIQUIDA ,
          SUM ( A.IMPOSTOS      )                                AS IMPOSTOS , 
          SUM ( A.QUANTIDADE * ISNULL ( B.CUSTO_CONTABIL , 0 ) ) AS CMV , 
          SUM ( A.VENDA_LIQUIDA ) - 
          SUM ( A.IMPOSTOS      ) - 
          SUM ( A.QUANTIDADE * ISNULL ( B.CUSTO_CONTABIL , 0 ) ) AS LUCRO_BRUTO,
		  C.DESCRICAO                                            AS PRODUTO
     FROM VENDAS_ANALITICAS  A WITH(NOLOCK)
LEFT JOIN CUSTO_MEDIO_MENSAL B WITH(NOLOCK) ON B.PRODUTO          = A.PRODUTO AND B.MES = MONTH ( A.MOVIMENTO ) AND B.ANO = YEAR ( A.MOVIMENTO )
LEFT JOIN PRODUTOS           C WITH(NOLOCK) ON C.PRODUTO          = A.PRODUTO
LEFT JOIN SECOES_PRODUTOS    D WITH(NOLOCK) ON D.SECAO_PRODUTO    = C.SECAO_PRODUTO
LEFT JOIN GRUPOS_PRODUTOS    E WITH(NOLOCK) ON E.GRUPO_PRODUTO    = C.GRUPO_PRODUTO
LEFT JOIN SUBGRUPOS_PRODUTOS F WITH(NOLOCK) ON F.SUBGRUPO_PRODUTO = C.SUBGRUPO_PRODUTO
LEFT JOIN MARCAS             G WITH(NOLOCK) ON G.MARCA            = C.MARCA
    WHERE A.MOVIMENTO >= :DATA_I AND A.MOVIMENTO <= :DATA_F
 GROUP BY A.EMPRESA , D.DESCRICAO , E.DESCRICAO , F.DESCRICAO , G.DESCRICAO , MONTH ( A.MOVIMENTO ) , YEAR ( A.MOVIMENTO ),   C.DESCRICAO 

