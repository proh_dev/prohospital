BEGIN TRANSACTION
	--rollback
	--commit

	DECLARE @NF_FATURAMENTO = 408

	INSERT
	INTO TITULOS_RECEBER(
	REGISTRO_CONTROLE , 
	REGISTRO_CONTROLE_II ,
	FORMULARIO_ORIGEM , 
	TAB_MASTER_ORIGEM , 
	REG_MASTER_ORIGEM , 
	EMPRESA , 
	TITULO , 
	ENTIDADE , 
	EMISSAO , 
	MOVIMENTO , 
	VENCIMENTO ,
	DESCONTO_ATE , 
	DESCONTO_VALOR , 
	JUROS_ATRASO , 
	JUROS_DISPENSADO , 
	MULTA_ATRASO , 
	MULTA_DISPENSADA , 
	VALOR ,
	TARIFA_BANCARIA,
	ANTECIPACAO , 
	RETENCAO_IR , 
	RETENCAO_DARF , 
	RETENCAO_ISS , 
	RETENCAO_INSS , 
	RETENCAO_GPS , 
	RETENCAO_PIS , 
	RETENCAO_COFINS,
	RETENCAO_CSSL ,
	RETENCAO_COPICS , 
	ANTECIPACAO_RECEBER ,
	CCONTABIL ,
	VALOR_ABATIMENTO ,
	TIPO_COBRANCA ,
	MODALIDADE ,
	DOCUMENTO ,       
	VENDEDOR ,
	CONTA_BANCARIA ,
	CHEQUE_BANCO,  
	CHEQUE_AGENCIA,  
	CHEQUE_CONTA,  
	CHEQUE_NUMERO,  
	CHEQUE_ENTIDADE,  
	CHEQUE_INSCRICAO_FEDERAL,
	CHEQUE_NOME
	)

SELECT A.NF_FATURAMENTO_PARCELA               AS REGISTRO_CONTROLE , 
       1                                      AS REGISTRO_CONTROLE_II ,
       B.FORMULARIO_ORIGEM                    AS FORMULARIO_ORIGEM , 
       B.TAB_MASTER_ORIGEM                    AS TAB_MASTER_ORIGEM , 
       B.NF_FATURAMENTO                       AS REG_MASTER_ORIGEM , 
       B.EMPRESA                              AS EMPRESA , 
       A.TITULO                               AS TITULO , 
       B.ENTIDADE                             AS ENTIDADE , 
       B.EMISSAO                              AS EMISSAO , 
       B.MOVIMENTO                            AS MOVIMENTO , 
       A.VENCIMENTO                           AS VENCIMENTO ,
       A.DESCONTO_ATE                         AS DESCONTO_ATE , 
       A.DESCONTO_VALOR                       AS DESCONTO_VALOR , 
       A.JUROS_ATRASO                         AS JUROS_ATRASO , 
       'N'                                    AS JUROS_DISPENSADO , 
       A.MULTA_ATRASO                         AS MULTA_ATRASO , 
       'N'                                    AS MULTA_DISPENSADA , 
       A.VALOR                                AS VALOR ,
       0                                      AS TARIFA_BANCARIA,
       'N'                                    AS ANTECIPACAO , 
       0                                      AS RETENCAO_IR , 
       NULL                                   AS RETENCAO_DARF , 
       0                                      AS RETENCAO_ISS , 
       0                                      AS RETENCAO_INSS , 
       NULL                                   AS RETENCAO_GPS , 
       0                                      AS RETENCAO_PIS , 
       0                                      AS RETENCAO_COFINS,
       0                                      AS RETENCAO_CSSL ,
       0                                      AS RETENCAO_COPICS , 
       A.ANTECIPACAO_RECEBER                  AS ANTECIPACAO_RECEBER ,
       ISNULL(C.CCONTABIL, D.CCONTABIL)       AS CCONTABIL ,
       0                                      AS VALOR_ABATIMENTO ,
       1                                      AS TIPO_COBRANCA ,
       ISNULL(A.MODALIDADE,2)                 AS MODALIDADE ,
       NULL                                   AS DOCUMENTO ,       
	   NULL                                   AS VENDEDOR ,
       NULL                                   AS CONTA_BANCARIA ,
       NULL                                   AS CHEQUE_BANCO,  
       NULL                                   AS CHEQUE_AGENCIA,  
       NULL                                   AS CHEQUE_CONTA,  
       NULL                                   AS CHEQUE_NUMERO,  
       NULL                                   AS CHEQUE_ENTIDADE,  
       NULL                                   AS CHEQUE_INSCRICAO_FEDERAL,  
       NULL                                   AS CHEQUE_NOME

  FROM      NF_FATURAMENTO_PARCELAS A WITH(NOLOCK)
       JOIN NF_FATURAMENTO          B WITH(NOLOCK)ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
  LEFT JOIN ENTIDADES_CCONTABIL     C WITH(NOLOCK,INDEX(UQ_ENTIDADES_CCONTABIL_TIPOS)) ON C.ENTIDADE        = B.ENTIDADE
                                                                                      AND C.TIPO_CCONTABIL  = 1
  LEFT JOIN PARAMETROS_CCONTABIL    D WITH(NOLOCK)ON D.GRUPO_EMPRESARIAL = 1 AND D.TIPO_CCONTABIL  = 1
  LEFT JOIN PARAMETROS_VENDAS       E WITH(NOLOCK)ON E.EMPRESA_USUARIA = B.EMPRESA 
   LEFT JOIN OPERACOES_FISCAIS F WITH(NOLOCK)ON F.OPERACAO_FISCAL = B.OPERACAO_FISCAL
   LEFT JOIN NF_FATURAMENTO         Z WITH(NOLOCK)ON Z.NF_FATURAMENTO  = A.NF_FATURAMENTO
   WHERE A.NF_FATURAMENTO         = @NF_FATURAMENTO
   AND ISNULL(B.EMITIR_NFE,'N') = 'S'
   AND (F.GERAR_FINANCEIRO = 'S')