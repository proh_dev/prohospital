-----------------------------------------------------------
-- ROTINA QUE GERA A SUGESTAO DE COMPRAS PARA O DEPOSITO --
-----------------------------------------------------------
--
DECLARE @SUGESTAO_COMPRA    NUMERIC --=-- :SUGESTAO_COMPRA
DECLARE @DIAS_CURVA_A       NUMERIC
DECLARE @DIAS_CURVA_B       NUMERIC
DECLARE @DIAS_CURVA_C       NUMERIC
DECLARE @DIAS_CURVA_D       NUMERIC
DECLARE @DIAS_CURVA_E       NUMERIC
DECLARE @EFETIVIDADE        NUMERIC
DECLARE @TIPO_PRECO         NUMERIC
DECLARE @MOVIMENTO          DATETIME
DECLARE @GRUPO_COMPRA       NUMERIC
DECLARE @TIPO_RELATORIO     NUMERIC
DECLARE @ESTOQUE_ZERADO     VARCHAR(1)
DECLARE @PEDIDOS_PENDENTES  VARCHAR(1)
DECLARE @FORMULARIO_ORIGEM  NUMERIC
DECLARE @TAB_MASTER_ORIGEM  NUMERIC
DECLARE @REG_MASTER_ORIGEM  NUMERIC
DECLARE @LEADTIME_INTERNO   NUMERIC
DECLARE @TIPO_CALCULO       NUMERIC
DECLARE @ZERAR_QUANTIDADE   VARCHAR(1)
DECLARE @PROCESSAR          VARCHAR(1)
DECLARE @PROCESSAR_PARCIAL  VARCHAR(1)
DECLARE @APENAS_CONTROLADOS VARCHAR(1)
DECLARE @MENOS_CONTROLADOS  VARCHAR(1)
DECLARE @FORNECEDOR         NUMERIC
DECLARE @PROGRAMACAO_SUGESTAO NUMERIC     

---------------------------------------------------
-- ATRIBUI��O DOS PAR�METROS AS VARIAVEIS LOCAIS --
--------------------------------------------------

SET @SUGESTAO_COMPRA    = 2001          --:SUGESTAO_COMPRA           
SET @DIAS_CURVA_A       = 30           --:DIAS_CURVA_A            
SET @DIAS_CURVA_B       = 30           --:DIAS_CURVA_B            
SET @DIAS_CURVA_C       = 30           --:DIAS_CURVA_C            
SET @DIAS_CURVA_D       = 30           --:DIAS_CURVA_D            
SET @DIAS_CURVA_E       = 30           --:DIAS_CURVA_E            
SET @GRUPO_COMPRA       = NULL           --:GRUPO_COMPRA            
SET @MOVIMENTO          = '03/05/2018' --:MOVIMENTO            
SET @TIPO_PRECO         = 4  --:TIPO_PRECO            
SET @TIPO_RELATORIO     = 1            --:TIPO_RELATORIO            
SET @EFETIVIDADE        = 100          --:EFETIVIDADE            
SET @PEDIDOS_PENDENTES  = 'S'          --:PEDIDOS_PENDENTES            
SET @ESTOQUE_ZERADO     = 'N'          --:ESTOQUE_ZERADO             
SET @LEADTIME_INTERNO   = 15           --:LEADTIME            
SET @TIPO_CALCULO       = 3             
SET @PROGRAMACAO_SUGESTAO = NULL      
SET @ZERAR_QUANTIDADE   = 'N'          
SET @PROCESSAR_PARCIAL  = 'N'    
SET @APENAS_CONTROLADOS = 'N'    
SET @MENOS_CONTROLADOS  = 'N'    
SET @PROCESSAR = 'S'


SELECT

@DIAS_CURVA_A         = A.DIAS_CURVA_A,
@DIAS_CURVA_B         = A.DIAS_CURVA_B,
@DIAS_CURVA_C         = A.DIAS_CURVA_C,
@DIAS_CURVA_D         = A.DIAS_CURVA_D,
@DIAS_CURVA_E         = A.DIAS_CURVA_E,
@GRUPO_COMPRA         = A.GRUPO_COMPRA,
@MOVIMENTO            = A.MOVIMENTO,
@TIPO_PRECO           = A.TIPO_PRECO,
@TIPO_RELATORIO       = A.TIPO_RELATORIO,
@EFETIVIDADE          = A.EFETIVIDADE,
@PEDIDOS_PENDENTES    = A.PEDIDOS_PENDENTES,
@ESTOQUE_ZERADO       = A.ESTOQUE_ZERADO,
@LEADTIME_INTERNO     = A.LEADTIME,
@TIPO_CALCULO         = A.TIPO_CALCULO,
@PROGRAMACAO_SUGESTAO = A.PROGRAMACAO_SUGESTAO,
@ZERAR_QUANTIDADE     = A. ZERAR_QUANTIDADE_COMPRA ,      
@PROCESSAR_PARCIAL    = A.PROCESSAR_PARCIAL,
@APENAS_CONTROLADOS   = A.APENAS_CONTROLADOS,
@MENOS_CONTROLADOS    = A.MENOS_CONTROLADOS ,
@PROCESSAR 			  = A.PROCESSAR

FROM SUGESTOES_COMPRAS A WITH(NOLOCK)
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA

 --SELECT 
 --@DIAS_CURVA_A         ,
 --@DIAS_CURVA_B         ,
 --@DIAS_CURVA_C         ,
 --@DIAS_CURVA_D         ,
 --@DIAS_CURVA_E         ,
 --@GRUPO_COMPRA         ,
 --@MOVIMENTO            ,
 --@TIPO_PRECO           ,
 --@TIPO_RELATORIO       ,
 --@EFETIVIDADE          ,
 --@PEDIDOS_PENDENTES    ,
 --@ESTOQUE_ZERADO       ,
 --@LEADTIME_INTERNO     ,
 --@TIPO_CALCULO         ,
 --@PROGRAMACAO_SUGESTAO ,
 --@ZERAR_QUANTIDADE     ,
 --@PROCESSAR_PARCIAL    ,
 --@APENAS_CONTROLADOS   ,
 --@MENOS_CONTROLADOS    ,
 --@PROCESSAR 



     EXEC GERAR_SUGESTOES_COMPRAS @SUGESTAO_COMPRA
                                , @DIAS_CURVA_A
                                , @DIAS_CURVA_B
                                , @DIAS_CURVA_C
                                , @DIAS_CURVA_D
                                , @DIAS_CURVA_E
                                , @EFETIVIDADE
                                , @TIPO_PRECO
                                , @MOVIMENTO
                                , @GRUPO_COMPRA
                                , @TIPO_RELATORIO
                                , @ESTOQUE_ZERADO
                                , @PEDIDOS_PENDENTES
                                , @LEADTIME_INTERNO
                                , @TIPO_CALCULO
                                , @APENAS_CONTROLADOS
                                , @MENOS_CONTROLADOS
                                , @ZERAR_QUANTIDADE
                                , @PROCESSAR
                                , @PROCESSAR_PARCIAL
                                ,NULL
                                ,@FORNECEDOR

      --------------------------------
      -- RETORNA OS FLAG'S PARA 'N' --
      --------------------------------

      --UPDATE SUGESTOES_COMPRAS 
      --   SET PROCESSAR         = 'N'
      --     , PROCESSAR_PARCIAL = 'N'
      -- WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA


   --   --------------------------
   --   -- MANUTEN��O DE M�NIMO --
   --   --------------------------


   --   UPDATE PRODUTOS_PARAMETROS_EMPRESAS
   --      SET ESTOQUE_MINIMO  =   A.ESTOQUE_MINIMO_NOVO
   --        , VALIDADE_MINIMO =  '31/12/2099'

   --     FROM SUGESTOES_COMPRAS_RESULTADO  A WITH(NOLOCK)
   --     JOIN PRODUTOS_PARAMETROS_EMPRESAS C WITH(NOLOCK) ON C.PRODUTO = A.PRODUTO 
   --                                                     AND C.EMPRESA = A.EMPRESA

   --    WHERE A.SUGESTAO_COMPRA        =  @SUGESTAO_COMPRA
   --      AND ISNULL(A.ESTOQUE_MINIMO_NOVO,99999999) <> C.ESTOQUE_MINIMO 
		 --AND ISNULL(A.ESTOQUE_MINIMO_NOVO,99999999) <>  99999999
               

--------------------------------------------
----ATUALIZA STATUS DE COMPRA PARA INATIVO
--------------------------------------------

--UPDATE PRODUTOS_PARAMETROS_EMPRESAS

--   SET SITUACAO_PRODUTO_COMPRA = 2

--  FROM SUGESTOES_COMPRAS_RESULTADO  A WITH(NOLOCK)
--  JOIN PRODUTOS_PARAMETROS_EMPRESAS B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO
--                                                  AND B.EMPRESA = A.EMPRESA
                                                  
-- WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA
--   AND A.INATIVAR_COMPRA = 'S'