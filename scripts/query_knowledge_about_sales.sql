   --Primeira query feita a modo de testes
   --Executar a query para pegar detalhes das vendas por loja com informa��es da loja do produto e do vendedor
   --Desenvolvedor = Yno� Pedro

DECLARE @LOJA NUMERIC(6)

IF ISNUMERIC(:LOJA) = 1 
  BEGIN 
    SET @LOJA = :LOJA
  END ELSE  
    SET @LOJA = NULL

-- SET @LOJA = NULL
SELECT PDV.LOJA                            AS COD_LOJA
       , L.NOME_RESUMIDO                   AS NOME_LOJA, PDV.PRODUTO
       , P.DESCRICAO                       AS NOME_PRODUTO
       , PDV.QUANTIDADE                    AS QTD_VENDIDA
       , PDV.PRECO                         AS PRECO
	   , PDV.VENDEDOR                      AS COD_VENDEDOR
       , V.NOME                            AS NOME_VENDEDOR
       , PDV.MOVIMENTO                     AS DATA_VENDA
       , PDV.ECF_CUPOM                     AS ECF
       , PDV.TOTAL_PRECO_BRUTO             AS PRECO_BRUTO
       , PDV.TOTAL_PRECO_LIQUIDO           AS PRECO_LIQUIDO

FROM PDV_ITENS PDV WITH(NOLOCK)
JOIN PRODUTOS P WITH(NOLOCK) ON PDV.PRODUTO = P.PRODUTO
LEFT JOIN LOJAS L ON L.LOJA = PDV.LOJA
LEFT JOIN VENDEDORES V ON PDV.VENDEDOR = V.VENDEDOR
    WHERE (PDV.LOJA = @LOJA OR @LOJA IS NULL)



