DECLARE @CLASSIF INT
DECLARE @EMPRESA_CONTABIL INT

SET @CLASSIF          = 0
SET @EMPRESA_CONTABIL = 0
          
DECLARE @PAGAMENTO_INI DATETIME = :PAGAMENTO_INI
DECLARE @PAGAMENTO_FIM DATETIME = :PAGAMENTO_FIM
DECLARE @EMPRESA_INI   NUMERIC  = :EMPRESA_INI
DECLARE @EMPRESA_FIM   NUMERIC  = :EMPRESA_FIM


--SET @CLASSIF          = 0--:CLASS
--SET @EMPRESA_CONTABIL = 0--:EMPRESA_CONTABIL

--DECLARE @PAGAMENTO_INI DATETIME = '01/01/2017'
--DECLARE @PAGAMENTO_FIM DATETIME = '31/01/2017'
--DECLARE @EMPRESA_INI   NUMERIC = 1
--DECLARE @EMPRESA_FIM   NUMERIC = 1000



if object_id('tempdb..#TABELA_BASE') is not null
   DROP TABLE #TABELA_BASE

SELECT 
       @PAGAMENTO_INI            AS DATA_INI,
       @PAGAMENTO_FIM            AS DATA_FIM,
       X.EMPRESA                 AS EMPRESA ,
       X.CLASSIF_FINANCEIRA      AS CLASSIF_FINANCEIRA ,
       X.DESCR_CLASSIF           AS DESCR_CLASSIF ,

       SUM ( X.VALOR )           + 
       SUM ( X.TARIFA_BANCARIA ) +
       SUM ( X.JUROS )           + 
       SUM ( X.MULTA )           - 
       SUM ( X.DESCONTO )        AS TOTAL

   INTO #TABELA_BASE

  FROM ( --------------------------
         -- PAGAMENTOS EM CHEQUE --
         --------------------------

         SELECT B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                B.VENCIMENTO                                         AS VENCIMENTO ,
                B.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                B.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                G.DESCRICAO                                          AS CONTA ,
                'CH ' + DBO.ZEROS (CONVERT (VARCHAR (6), A.CHEQUE), 6)
                                                                     AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO ,                 
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_CHEQUES         A WITH (NOLOCK)
           JOIN PAGAMENTOS_CHEQUES_TITULOS B WITH (NOLOCK) ON B.PAGAMENTO_CHEQUE  = A.PAGAMENTO_CHEQUE
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA , 
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA ) D ON D.TITULO_PAGAR    = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN CONTAS_BANCARIAS           G WITH (NOLOCK) ON A.CONTA_BANCARIA     = G.CONTA_BANCARIA
            JOIN EMPRESAS_USUARIAS          H WITH (NOLOCK) ON B.EMPRESA            = H.EMPRESA_USUARIA

          WHERE A.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                A.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                B.EMPRESA        >= CASE WHEN @EMPRESA_INI  IS NULL THEN 0       ELSE @EMPRESA_INI    END AND
                B.EMPRESA        <= CASE WHEN @EMPRESA_FIM  IS NULL THEN 99999  ELSE @EMPRESA_FIM    END AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF=0)                                       AND
                (H.EMPRESA_CONTABIL = @EMPRESA_CONTABIL OR @EMPRESA_CONTABIL=0)                       AND
                A.PAGAMENTO_CHEQUE NOT IN (SELECT ISNULL(PAGAMENTO_CHEQUE,0) FROM CANCELAMENTOS_CHEQUES WHERE PAGAMENTO_CHEQUE IS NOT NULL)
       GROUP BY B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO ,
                B.EMPRESA ,
                B.ENTIDADE ,
                B.TITULO  ,
                E.NOME ,
                F.DESCRICAO,
                G.DESCRICAO,
                A.CHEQUE,
                B.VENCIMENTO


      UNION ALL

         -------------------------
         -- PAGAMENTOS EM CAIXA --
         -------------------------

         SELECT B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                B.VENCIMENTO                                         AS VENCIMENTO ,
                B.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                B.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                'CAIXA'                                              AS CONTA , 
                NULL                                                 AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO , 
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_CAIXA         A WITH (NOLOCK)
           JOIN PAGAMENTOS_CAIXA_TITULOS B WITH (NOLOCK) ON B.PAGAMENTO_CAIXA = A.PAGAMENTO_CAIXA
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA , 
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA ) D ON D.TITULO_PAGAR = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN EMPRESAS_USUARIAS          H WITH (NOLOCK) ON B.EMPRESA            = H.EMPRESA_USUARIA

          WHERE A.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                A.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                B.EMPRESA        >= CASE WHEN @EMPRESA_INI  IS NULL THEN 0      ELSE @EMPRESA_INI    END AND
                B.EMPRESA        <= CASE WHEN @EMPRESA_FIM  IS NULL THEN 99999  ELSE @EMPRESA_FIM    END AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF=0)                                       AND
                (H.EMPRESA_CONTABIL = @EMPRESA_CONTABIL OR @EMPRESA_CONTABIL=0)                       AND
                A.PAGAMENTO_CAIXA NOT IN (SELECT ISNULL(PAGAMENTO_CAIXA,0) FROM CANCELAMENTOS_PAGAMENTOS WHERE PAGAMENTO_CAIXA IS NOT NULL)
       GROUP BY B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO ,
                B.EMPRESA ,
                B.ENTIDADE ,
                B.TITULO ,
                E.NOME ,
                F.DESCRICAO ,
                B.VENCIMENTO

      UNION ALL

         ----------------------------
         -- PAGAMENTOS ESCRITURAIS --
         ----------------------------

         SELECT B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                B.VENCIMENTO                                         AS VENCIMENTO ,
                B.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                B.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                G.DESCRICAO                                          AS CONTA ,
                NULL                                                 AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO ,
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_ESCRITURAIS         A WITH (NOLOCK)
           JOIN PAGAMENTOS_ESCRITURAIS_TITULOS B WITH (NOLOCK) ON B.PAGAMENTO_ESCRITURAL = A.PAGAMENTO_ESCRITURAL
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA , 
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA ) D ON D.TITULO_PAGAR = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN CONTAS_BANCARIAS           G WITH (NOLOCK) ON A.CONTA_BANCARIA     = G.CONTA_BANCARIA
            JOIN EMPRESAS_USUARIAS          H WITH (NOLOCK) ON B.EMPRESA            = H.EMPRESA_USUARIA

          WHERE A.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                A.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                B.EMPRESA        >= CASE WHEN @EMPRESA_INI IS NULL THEN 0       ELSE @EMPRESA_INI    END AND
                B.EMPRESA        <= CASE WHEN @EMPRESA_FIM  IS NULL THEN 99999  ELSE @EMPRESA_FIM    END AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF=0)                                       AND
                (H.EMPRESA_CONTABIL = @EMPRESA_CONTABIL OR @EMPRESA_CONTABIL=0)                       AND
                 A.PAGAMENTO_ESCRITURAL NOT IN (SELECT ISNULL(PAGAMENTO_ESCRITURAL,0) FROM CANCELAMENTOS_PAGAMENTOS WHERE PAGAMENTO_ESCRITURAL IS NOT NULL)
       GROUP BY B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                A.DATA_PAGAMENTO ,
                B.EMPRESA ,
                B.ENTIDADE ,
                B.TITULO ,
                E.NOME ,
                F.DESCRICAO,
                B.VENCIMENTO ,
                G.DESCRICAO

                
      UNION ALL

         ----------------------------
         -- PAGAMENTOS ELETRONICOS --
         ----------------------------

         SELECT B.TITULO_PAGAR                                       AS TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA                                 AS CLASSIF_FINANCEIRA , 
                B.DATA_PAGAMENTO                                     AS DATA_PAGAMENTO , 
                A.VENCIMENTO                                         AS VENCIMENTO ,
                A.EMPRESA                                            AS EMPRESA ,
                B.ENTIDADE                                           AS ENTIDADE ,
                A.TITULO                                             AS TITULO ,
                E.NOME                                               AS NOME_ENTIDADE ,
                F.DESCRICAO                                          AS DESCR_CLASSIF ,
                G.DESCRICAO                                          AS CONTA ,
                NULL                                                 AS CHEQUE ,
                SUM ( ( B.VALOR_ABATIMENTO   / C.VALOR ) * D.VALOR ) AS ABATIMENTO ,
                SUM ( ( B.TARIFA_BANCARIA    / C.VALOR ) * D.VALOR ) AS TARIFA_BANCARIA , 
                SUM ( ( B.PAGAMENTO_VALOR    / C.VALOR ) * D.VALOR ) AS VALOR ,
                SUM ( ( B.PAGAMENTO_JUROS    / C.VALOR ) * D.VALOR ) AS JUROS ,
                SUM ( ( B.PAGAMENTO_MULTA    / C.VALOR ) * D.VALOR ) AS MULTA ,
                SUM ( ( B.PAGAMENTO_DESCONTO / C.VALOR ) * D.VALOR ) AS DESCONTO ,
                SUM ( ( B.PAGAMENTO_TOTAL    / C.VALOR ) * D.VALOR ) AS PAGAMENTO_TOTAL
           FROM PAGAMENTOS_ELETRONICOS         B WITH (NOLOCK)
           INNER JOIN TITULOS_PAGAR            A WITH (NOLOCK) ON B.TITULO_PAGAR = A.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         VALOR              AS VALOR
                    FROM TITULOS_PAGAR WITH(NOLOCK) ) C ON C.TITULO_PAGAR = B.TITULO_PAGAR
           JOIN ( SELECT TITULO_PAGAR       AS TITULO_PAGAR , 
                         CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA , 
                         SUM ( VALOR )      AS VALOR
                    FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
                GROUP BY TITULO_PAGAR , CLASSIF_FINANCEIRA ) D ON D.TITULO_PAGAR = B.TITULO_PAGAR
            JOIN ENTIDADES                  E WITH (NOLOCK) ON B.ENTIDADE           = E.ENTIDADE
            JOIN CLASSIF_FINANCEIRAS        F WITH (NOLOCK) ON F.CLASSIF_FINANCEIRA = D.CLASSIF_FINANCEIRA
            JOIN CONTAS_BANCARIAS           G WITH (NOLOCK) ON B.CONTA_BANCARIA     = G.CONTA_BANCARIA
            JOIN EMPRESAS_USUARIAS          H WITH (NOLOCK) ON A.EMPRESA            = H.EMPRESA_USUARIA

          WHERE B.DATA_PAGAMENTO >= @PAGAMENTO_INI AND
                B.DATA_PAGAMENTO <= @PAGAMENTO_FIM AND
                A.EMPRESA        >= CASE WHEN @EMPRESA_INI IS NULL THEN 0       ELSE @EMPRESA_INI    END AND
                A.EMPRESA        <= CASE WHEN @EMPRESA_FIM  IS NULL THEN 99999  ELSE @EMPRESA_FIM    END AND
                (D.CLASSIF_FINANCEIRA = @CLASSIF OR @CLASSIF=0)                                       AND 
                (H.EMPRESA_CONTABIL = @EMPRESA_CONTABIL OR @EMPRESA_CONTABIL=0)

       GROUP BY B.TITULO_PAGAR , 
                D.CLASSIF_FINANCEIRA , 
                B.DATA_PAGAMENTO ,
                A.EMPRESA ,
                B.ENTIDADE ,
                A.TITULO ,
                E.NOME ,
                F.DESCRICAO,
                A.VENCIMENTO ,
                G.DESCRICAO     ) X 
 

JOIN TITULOS_PAGAR      Y WITH(NOLOCK) ON Y.TITULO_PAGAR = X.TITULO_PAGAR

GROUP BY 
         X.EMPRESA ,
         X.DESCR_CLASSIF,
         X.CLASSIF_FINANCEIRA
ORDER BY X.EMPRESA, X.DESCR_CLASSIF


SELECT 
       @PAGAMENTO_INI                                                    AS DATA_INI,
       @PAGAMENTO_FIM                                                    AS DATA_FIM,
       'FATURAMENTO'                                                     AS DESCR_CLASSIF,
       '01 - VENDAS'                                                     AS TIPO,
       SUM( CASE WHEN A.EMPRESA = 1    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_01,
       SUM( CASE WHEN A.EMPRESA = 2    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_02,
       SUM( CASE WHEN A.EMPRESA = 3    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_03,
       SUM( CASE WHEN A.EMPRESA = 4    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_04,
       SUM( CASE WHEN A.EMPRESA = 5    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_05,
       SUM( CASE WHEN A.EMPRESA = 6    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_06,
       SUM( CASE WHEN A.EMPRESA = 7    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_07,
       SUM( CASE WHEN A.EMPRESA = 8    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_08,
       SUM( CASE WHEN A.EMPRESA = 9    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_09,
       SUM( CASE WHEN A.EMPRESA = 10   THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_10,
       SUM( CASE WHEN A.EMPRESA = 1000 THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_1000,

       SUM( CASE WHEN A.EMPRESA = 1    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 2    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 3    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 4    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 5    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 6    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 7    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 8    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 9    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 10   THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 1000 THEN A.VENDA_LIQUIDA ELSE 0 END ) AS TOTAL_GERAL


  FROM VENDAS_ANALITICAS A WITH(NOLOCK)
  WHERE 1=1
AND A.EMPRESA   >= @EMPRESA_INI
AND A.EMPRESA   <= @EMPRESA_FIM
AND A.MOVIMENTO >= @PAGAMENTO_INI
AND A.MOVIMENTO <= @PAGAMENTO_FIM

UNION ALL

SELECT 
       @PAGAMENTO_INI                                                    AS DATA_INI,
       @PAGAMENTO_FIM                                                    AS DATA_FIM,
       'FATURAMENTO'                                                     AS DESCR_CLASSIF,
       '02 - VENDAS INTRAGRUPO'                                          AS TIPO,
       SUM( CASE WHEN A.EMPRESA = 1    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_01,
       SUM( CASE WHEN A.EMPRESA = 2    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_02,
       SUM( CASE WHEN A.EMPRESA = 3    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_03,
       SUM( CASE WHEN A.EMPRESA = 4    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_04,
       SUM( CASE WHEN A.EMPRESA = 5    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_05,
       SUM( CASE WHEN A.EMPRESA = 6    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_06,
       SUM( CASE WHEN A.EMPRESA = 7    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_07,
       SUM( CASE WHEN A.EMPRESA = 8    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_08,
       SUM( CASE WHEN A.EMPRESA = 9    THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_09,
       SUM( CASE WHEN A.EMPRESA = 10   THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_10,
       SUM( CASE WHEN A.EMPRESA = 1000 THEN A.VENDA_LIQUIDA ELSE 0 END ) AS EMP_1000,

       SUM( CASE WHEN A.EMPRESA = 1    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 2    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 3    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 4    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 5    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 6    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 7    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 8    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 9    THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 10   THEN A.VENDA_LIQUIDA ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 1000 THEN A.VENDA_LIQUIDA ELSE 0 END ) AS TOTAL_GERAL

  FROM VENDAS_ANALITICAS A WITH(NOLOCK)
  WHERE 1=1
AND A.EMPRESA   >= @EMPRESA_INI
AND A.EMPRESA   <= @EMPRESA_FIM
AND A.MOVIMENTO >= @PAGAMENTO_INI
AND A.MOVIMENTO <= @PAGAMENTO_FIM
AND ISNULL(A.CLIENTE,0) >= 1 AND ISNULL(A.CLIENTE,0) <= 1000 --informa que o cliente � uma das empresas do grupo

UNION ALL             

SELECT 
       A.DATA_INI,
       A.DATA_FIM,
       A.DESCR_CLASSIF,
       ISNULL(CAST(DBO.ZEROS(C.ORDEM,2) AS VARCHAR),'99') + ' - ' +
       ISNULL(C.DESCRICAO, 'SEM AGRUPAMENTO')                    AS TIPO,
       SUM( CASE WHEN A.EMPRESA = 1    THEN A.TOTAL ELSE 0 END ) AS EMP_01,
       SUM( CASE WHEN A.EMPRESA = 2    THEN A.TOTAL ELSE 0 END ) AS EMP_02,
       SUM( CASE WHEN A.EMPRESA = 3    THEN A.TOTAL ELSE 0 END ) AS EMP_03,
       SUM( CASE WHEN A.EMPRESA = 4    THEN A.TOTAL ELSE 0 END ) AS EMP_04,
       SUM( CASE WHEN A.EMPRESA = 5    THEN A.TOTAL ELSE 0 END ) AS EMP_05,
       SUM( CASE WHEN A.EMPRESA = 6    THEN A.TOTAL ELSE 0 END ) AS EMP_06,
       SUM( CASE WHEN A.EMPRESA = 7    THEN A.TOTAL ELSE 0 END ) AS EMP_07,
       SUM( CASE WHEN A.EMPRESA = 8    THEN A.TOTAL ELSE 0 END ) AS EMP_08,
       SUM( CASE WHEN A.EMPRESA = 9    THEN A.TOTAL ELSE 0 END ) AS EMP_09,
       SUM( CASE WHEN A.EMPRESA = 10   THEN A.TOTAL ELSE 0 END ) AS EMP_10,
       SUM( CASE WHEN A.EMPRESA = 1000 THEN A.TOTAL ELSE 0 END ) AS EMP_1000,

       SUM( CASE WHEN A.EMPRESA = 1    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 2    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 3    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 4    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 5    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 6    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 7    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 8    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 9    THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 10   THEN A.TOTAL ELSE 0 END ) +
       SUM( CASE WHEN A.EMPRESA = 1000 THEN A.TOTAL ELSE 0 END ) AS TOTAL_GERAL

 FROM #TABELA_BASE A
 LEFT
 JOIN CLASSIF_FINANCEIRAS        B WITH(NOLOCK) ON B.CLASSIF_FINANCEIRA = A.CLASSIF_FINANCEIRA
 LEFT
 JOIN CLASSIF_FINANCEIRAS_GRUPOS C WITH(NOLOCK) ON C.CLASSIF_FINANCEIRA_GRUPO = B.CLASSIF_FINANCEIRA_GRUPO
 GROUP BY
       A.DATA_INI,
       A.DATA_FIM,
       A.DESCR_CLASSIF,
       ISNULL(CAST(DBO.ZEROS(C.ORDEM,2) AS VARCHAR),'99') + ' - ' +
       ISNULL(C.DESCRICAO, 'SEM AGRUPAMENTO')
 ORDER BY TIPO, DESCR_CLASSIF
