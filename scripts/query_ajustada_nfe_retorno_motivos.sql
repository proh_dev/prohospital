ALTER FUNCTION [dbo].[NFE_RETORNO_MOTIVOS] ( @XML VARCHAR(MAX)) RETURNS VARCHAR(8000) AS  
  
BEGIN  
  
--SELECT DBO.NFE_RETORNO_MOTIVOS(CONTEUDO)  
--  FROM NFE_LOG (NOLOCK)   
-- WHERE REGISTRO_NFE = 17  
  --DECLARE @XML VARCHAR(MAX) = '<retConsReciNFe xmlns="http://www.portalfiscal.inf.br/nfe" xmlns:ns2="http://www.portalfiscal.inf.br/nfe/wsdl/NFeRetAutorizacao4" versao="4.00"><tpAmb>1</tpAmb><verAplic>CE_NFe_V4.0.13</verAplic><nRec>231000333532876</nRec><cStat>104</cStat><xMotivo>Lote processado</xMotivo><cUF>23</cUF><dhRecbto>2018-10-26T09:07:19-03:00</dhRecbto><protNFe versao="4.00"><infProt><tpAmb>1</tpAmb><verAplic>CE_NFe_V4.0.13</verAplic><chNFe>23181000291784000405550010000731061000618711</chNFe><dhRecbto>2018-10-26T09:07:19-03:00</dhRecbto><cStat>108</cStat><xMotivo>Servico Paralisado Momentaneamente (curto prazo) [Erro: CCC]</xMotivo></infProt></protNFe></retConsReciNFe>'
  DECLARE @POSICAO_INICIO  INT  
  DECLARE @POSICAO_FIM     INT  
  DECLARE @MOTIVO          VARCHAR(8000)  
  DECLARE @XML_ERRO        XML  
  
        --IF @XML IS NULL   
        --   GOTO PULAR   
          
        if @XML LIKE '%ERRO:%'  
		BEGIN
           SET @XML = '<RetPreValidacao><cStat>000</cStat><xMotivo>'+( SELECT SUBSTRING(@XML,CHARINDEX('ERRO:',@XML )+5,LEN(@XML))+'</xMotivo></RetPreValidacao>' )  
		   SET @XML = REPLACE(@XML,'</xMotivo></infProt></protNFe></retConsReciNFe>','')
		END
		 
        if @XML LIKE '%NF-e INCONSISTENTE:%'  
        BEGIN
		   SET @XML = '<RetPreValidacao><cStat>000</cStat><xMotivo>'+( SELECT SUBSTRING(@XML,CHARINDEX('NF-e INCONSISTENTE:',@XML )+19,LEN(@XML))+'</xMotivo></RetPreValidacao>' )  
		   SET @XML = REPLACE(@XML,'</xMotivo></infProt></protNFe></retConsReciNFe>','')
		END  

	 
	  SET @XML_ERRO = dbo.Remove_Xml_Namespace( CONVERT(XML, @XML) )  
  
                        
        --valida��o da nf-e  
        if @XML_ERRO.exist('//retConsReciNFe') = 1  
         SET @XML_ERRO = @XML_ERRO.query('//protNFe')   
        ELSE  
        --valida��o da nf-e  
        if @XML_ERRO.exist('retEnviNFe') = 1  
       SET @XML_ERRO = @XML_ERRO.query('retEnviNFe')   
        ELSE  
        --valida��o do cancelamento servidor de eventos  
        if @XML_ERRO.exist('retEnvEvento') = 1  
       SET @XML_ERRO = @XML_ERRO.query('//retEvento')   
        ELSE  
        --valida��o do cancelamento outro tipo de retorno  
        if @XML_ERRO.exist('procEventoNFe') = 1  
       SET @XML_ERRO = @XML_ERRO.query('//retEvento')   
        ELSE  
        --valida��o do componente antes de enviar  
        if @XML_ERRO.exist('RetPreValidacao') = 1  
       SET @XML_ERRO = @XML_ERRO.query('RetPreValidacao')   
  
  
  SELECT @MOTIVO = @XML_ERRO.value('(//cStat)[1]', 'varchar(5)') +'-'+ @XML_ERRO.value('(//xMotivo)[1]', 'varchar(1000)')   
          
  
        PULAR:  
  
  RETURN ( @MOTIVO )  
  --SELECT @MOTIVO  
  
END  
  
   
  
  