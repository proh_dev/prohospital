DECLARE @CONTA_BANCARIA NUMERIC(15)
DECLARE @DATA_INI       DATETIME
DECLARE @DATA_FIM       DATETIME
DECLARE @EMPRESA_INI    NUMERIC
DECLARE @EMPRESA_FIM    NUMERIC

--SET @CONTA_BANCARIA = :CONTA_BANCARIA
--SET @DATA_INI       = :DATA_INI
--SET @DATA_FIM       = :DATA_FIM
--SET @EMPRESA_INI    = :EMPRESA_INI
--SET @EMPRESA_FIM    = :EMPRESA_FIM

SET @CONTA_BANCARIA = 1
SET @DATA_INI       = '01/01/2018'
SET @DATA_FIM       = '31/03/2018'
SET @EMPRESA_INI    = 1
SET @EMPRESA_FIM    = 1000

SELECT X.CHAVE,
       X.DATA                 AS DATA,
       X.TRANSACAO_BANCARIA,
       X.DESCRICAO,
       X.ORIGEM,
       X.DOCUMENTO,
       X.PROCESSO,
       SUM(X.VALOR)            AS VALOR,
       SUM(X.CREDITO)          AS CREDITO,
       SUM(X.DEBITO)           AS DEBITO,
       SUM(X.VALOR_CONCILIADO) AS VALOR_CONCILIADO

  FROM ( SELECT A.MOVIMENTO_BANCARIO               AS CHAVE,
                A.DATA,
                C.TRANSACAO_BANCARIA               AS TRANSACAO_BANCARIA,
                C.DESCRICAO                        AS DESCRICAO,
                A.REG_MASTER_ORIGEM                AS ORIGEM,
                ISNULL (A.NUMERO, 0)               AS DOCUMENTO,
                B.DESCRICAO                        AS PROCESSO,
                CASE WHEN A.CREDITO = 0
                     THEN A.DEBITO
                     ELSE A.CREDITO
                 END                               AS VALOR,
            
                CASE WHEN A.CREDITO > 0
                     THEN A.CREDITO
                     ELSE 0.00
                 END                               AS CREDITO,
            
                CASE WHEN A.DEBITO > 0
                     THEN A.DEBITO
                     ELSE 0.00
                 END                               AS DEBITO,
            
                CASE WHEN A.CREDITO = 0
                     THEN A.DEBITO
                     ELSE A.CREDITO
                 END                               AS VALOR_CONCILIADO

           FROM      BANCOS_MOVIMENTACAO                 A WITH (NOLOCK)
                JOIN TIPOS_TRANSACOES_BANCARIAS          B WITH (NOLOCK) ON A.FORMULARIO_ORIGEM       = B.ORIGEM
                                                                        AND B.TIPO_MOVIMENTO          = 'C'
                JOIN TRANSACOES_BANCARIAS                C WITH (NOLOCK) ON B.TIPO_TRANSACAO_BANCARIA = C.TIPO_TRANSACAO_BANCARIA
                                                                        AND B.TIPO_MOVIMENTO          = C.TIPO_MOVIMENTO
                                                                        AND C.TIPO                    = 'C' --TRAZER SOMENTE O QUE � TIPO CONCILIA��O
           LEFT JOIN MOVIMENTOS_BANCARIOS_LANCAMENTOS    E WITH (NOLOCK) ON A.MOVIMENTO_BANCARIO      = E.CHAVE

          WHERE 1 = 1
            --AND E.CHAVE IS NULL
            AND A.CONTA_BANCARIA     = @CONTA_BANCARIA
            AND A.DATA              >= @DATA_INI
            AND A.DATA              <= @DATA_FIM
            AND A.CREDITO            > 0
			AND E.TRANSACAO_BANCARIA = 12

) X

 GROUP BY X.ORIGEM,
          X.TRANSACAO_BANCARIA,
          X.DESCRICAO,
          X.DOCUMENTO,
          X.PROCESSO,
          X.CHAVE,
          X.DATA
 
 ORDER BY X.TRANSACAO_BANCARIA
