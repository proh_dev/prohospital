--CREATE FUNCTION [dbo].[FN_PRECIFICACAO_PRODUTOS_ST]    
--(    
-- @EMPRESA NUMERIC(15,0),  
-- @CLIENTE NUMERIC(15,0),  
-- @PRODUTO NUMERIC(15,0)  
    
--)    
  
--RETURNS @PRECIFICACAO_PRODUTOS_ST TABLE    
--(  
--PRODUTO                           NUMERIC(15,0),  
--DESCRICAO                         VARCHAR(250),  
--GRUPO_TERMO_ACORDO_CE             NUMERIC(15,0),  
--DESCRICAO_TERMO                   VARCHAR(200),  
--MARGEM_AGREGACAO_DEMAIS_PRODUTOS  NUMERIC(15,2),  
--ALIQUOTA_SAIDA                    NUMERIC(15,2),  
--ALIQUOTA_ENTRADA                  NUMERIC(15,2),  
--ULTIMO_CUSTO_GERENCIAL            NUMERIC(15,2),  
--REGIAO                            NUMERIC(15,0),  
--TIPO_FORNECEDOR_DECRETO_CE        NUMERIC(15,0),  
--SIMPLES                           VARCHAR(1),  
--DECRETO_PARTICIPANTE              VARCHAR(1),  
--PERCENTUAL_ST                     NUMERIC(15,6),  
--AL�QUOTA_ENTRADA_DC               NUMERIC(15,6),  
--ICMS_VALOR                        NUMERIC(15,6),  
--CUSTO_FINAL                       NUMERIC(15,6),  
--IMPOSTOS_FEDERAIS                 NUMERIC(15,6),  
--DESPESA_ADM                       NUMERIC(15,6),  
--DESPESA_FIN                       NUMERIC(15,6),  
--CUSTO_BASE_PRECO        NUMERIC(15,6)  
--)  
  
--AS  
  
--BEGIN   
  
--INSERT INTO @PRECIFICACAO_PRODUTOS_ST  
--(  
--PRODUTO                           ,  
--DESCRICAO                         ,  
--GRUPO_TERMO_ACORDO_CE             ,  
--DESCRICAO_TERMO                   ,  
--MARGEM_AGREGACAO_DEMAIS_PRODUTOS  ,  
--ALIQUOTA_SAIDA                    ,  
--ALIQUOTA_ENTRADA                  ,  
--ULTIMO_CUSTO_GERENCIAL            ,  
--REGIAO                            ,  
--TIPO_FORNECEDOR_DECRETO_CE        ,  
--SIMPLES                           ,  
--DECRETO_PARTICIPANTE              ,  
--PERCENTUAL_ST                     ,  
--AL�QUOTA_ENTRADA_DC               ,  
--ICMS_VALOR                        ,  
--CUSTO_FINAL                       ,  
--IMPOSTOS_FEDERAIS                 ,  
--DESPESA_ADM                       ,  
--DESPESA_FIN                       ,  
--CUSTO_BASE_PRECO        
--)  
  
  
DECLARE @CLIENTE NUMERIC = 95616
DECLARE @EMPRESA NUMERIC = 1000  
DECLARE @PRODUTO NUMERIC = 101610
  
SELECT   
A.PRODUTO                                                                                      AS PRODUTO,  
A.DESCRICAO                                                                                    AS DESCRICAO,  
A.GRUPO_TERMO_ACORDO_CE                                                                        AS GRUPO_TERMO_ACORDO_CE,  
B.DESCRICAO                                                                                    AS DESCRICAO_TERMO,  
B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS                                                             AS MARGEM_AGREGACAO_DEMAIS_PRODUTOS,  
B.ALIQUOTA_SAIDA                                                                               AS ALIQUOTA_SAIDA,  
B.ALIQUOTA_ENTRADA                                                                             AS ALIQUOTA_ENTRADA,  
COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL)                                           AS ULTIMO_CUSTO_GERENCIAL,  
F.REGIAO                                                                                       AS REGIAO,  
E.TIPO_FORNECEDOR_DECRETO_CE                                                                   AS TIPO_FORNECEDOR_DECRETO_CE,  
E.SIMPLES                                                                                      AS SIMPLES,  
E.DECRETO_PARTICIPANTE                                                                        AS DECRETO_PARTICIPANTE,  
CASE WHEN B.PRECIFICACAO = 'S' THEN  
 ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_SAIDA   / 100 + -- AL�Q SAIDA   
 ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_ENTRADA / 100   -- AL�Q ENTRADA   
     ELSE I.ALIQUOTA_ICMS  
END  
                                                                                               AS PERCENTUAL_ST,  
CASE WHEN B.PRECIFICACAO = 'N' THEN C.ICMS_ALIQUOTA ELSE 0.00 END                              AS AL�QUOTA_ENTRADA_DC,  
CASE WHEN B.PRECIFICACAO = 'S' THEN  
COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * (  
 ( ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_SAIDA   / 100 +  
   ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_ENTRADA / 100  ) / 100 )   
ELSE  
( COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * I.ALIQUOTA_ICMS / 100 ) -   
( COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * C.ICMS_ALIQUOTA / 100 )   
END                                                                                            AS ICMS_VALOR,  
  
  
CASE WHEN B.PRECIFICACAO = 'S' THEN  
COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) + (  
COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * (  
 ( ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_SAIDA   / 100 +  
   ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_ENTRADA / 100  ) / 100 ) )  
ELSE  
COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) + (  
                             ( COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * I.ALIQUOTA_ICMS / 100 ) -   
                             ( COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * C.ICMS_ALIQUOTA / 100 )   
                           )  
END                                                                                            AS CUSTO_FINAL,  
  
J.CSSL + J.IRPJ + I.ALIQUOTA_COFINS + I.ALIQUOTA_PIS                                           AS IMPOSTOS_FEDERAIS,  
ISNULL(K.DESPESA_ADM,0)                                                                        AS DESPESA_ADM,  
ISNULL(K.DESPESA_FIN,0)                                                                        AS DESPESA_FIN,  
  
( COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) + ( COALESCE(L.CUSTO_APLICADO, C.ULTIMO_CUSTO_GERENCIAL) * (  
 ( ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_SAIDA   / 100 +  
   ( 100 + B.MARGEM_AGREGACAO_DEMAIS_PRODUTOS ) * B.ALIQUOTA_ENTRADA / 100  ) / 100 ) ) )  
 /  
 ( 1 - ( ( J.CSSL + J.IRPJ + I.ALIQUOTA_COFINS + I.ALIQUOTA_PIS + ISNULL(K.DESPESA_ADM,0) +ISNULL(K.DESPESA_FIN,0) ) / 100 ) )   
                                                                                               AS CUSTO_BASE_PRECO  
  
  
  FROM PRODUTOS                      A WITH(NOLOCK)   
  JOIN GRUPOS_TERMO_ACORDO_CE        B WITH(NOLOCK) ON B.GRUPO_TERMO_ACORDO_CE = A.GRUPO_TERMO_ACORDO_CE  
  JOIN ULTIMO_CUSTO_MATRIZ           C WITH(NOLOCK) ON C.PRODUTO               = A.PRODUTO  
  JOIN ENDERECOS                     D WITH(NOLOCK) ON D.ENTIDADE              = C.ENTIDADE  
  LEFT  
  JOIN ENTIDADES_DECRETO_CE          E WITH(NOLOCK) ON E.ENTIDADE              = C.ENTIDADE  
  JOIN REGIOES_ESTADOS               F WITH(NOLOCK) ON F.ESTADO                = D.ESTADO  
  JOIN ENDERECOS                     G WITH(NOLOCK) ON G.ENTIDADE              = @CLIENTE  
  JOIN ENDERECOS                     H WITH(NOLOCK) ON H.ENTIDADE              = @EMPRESA  
  JOIN GRUPOS_TRIBUTARIOS_PARAMETROS I WITH(NOLOCK) ON I.ESTADO_ORIGEM         = G.ESTADO            -- EMPRESA ORIGEM  
                                                   AND I.ESTADO_DESTINO        = H.ESTADO            -- CLIENTE  
                                                   AND I.OPERACAO_FISCAL       = 12                  -- VERIFICAR  
                                                   AND I.GRUPO_TRIBUTARIO      = A.GRUPO_TRIBUTARIO  
  JOIN GRUPOS_TRIBUTARIOS            J WITH(NOLOCK) ON J.GRUPO_TRIBUTARIO      = A.GRUPO_TRIBUTARIO  
  JOIN EMPRESAS_USUARIAS             K WITH(NOLOCK) ON K.EMPRESA_USUARIA       = @EMPRESA  
  LEFT  
  JOIN VW_CUSTOS_ESPECIFICOS_PRODUTOS L             ON L.PRODUTO               = A.PRODUTO  
  
 WHERE 1=1  
   AND A.PRODUTO =  @PRODUTO  
   --AND A.PRODUTO IN (SELECT PRODUTO FROM PROPOSTAS_ITENS_LOTES_DETALHES WHERE PROPOSTA >= 10106)  
   --AND B.GRUPO_TERMO_ACORDO_CE = 1  
  
 IF @@ROWCOUNT = 0 RETURN --Caso nenhuma linha seja inserida na tabela Temporaria, nada deve ser feito, o return ajuda no desempenho    
  
  RETURN    
  
--END