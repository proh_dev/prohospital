  
  
--CREATE PROCEDURE [dbo].[PROCESSAMENTO_REMANEJAMENTO_LOJAS_RATEIOS] ( @REMANEJAMENTO_ESTOQUE_LOJA NUMERIC, @EMPRESA_ORIGEM NUMERIC ) AS    
  
--BEGIN    
  
BEGIN TRANSACTION  
--ROLLBACK  
--COMMIT  
  
  
DECLARE @REMANEJAMENTO_ESTOQUE_LOJA  NUMERIC  
DECLARE @EMPRESA_ORIGEM              NUMERIC = 1      
    SET @REMANEJAMENTO_ESTOQUE_LOJA =  3291
  
  
-------------------------------------------------------  
--DEFINIR TEMP DOS PRODUTOS EM EXCESSO PARA A LOJA EM QUEST�O--  
---------------------------------------------------------  
  
IF OBJECT_ID('TEMPDB..#PRODUTOS_EXCESSO_LOJA') IS NOT NULL DROP TABLE #PRODUTOS_EXCESSO_LOJA  
  
  
SELECT A.PRODUTO,  
       A.QUANTIDADE_EXCESSO    AS QUANTIDADE_EXCESSO  
         
  INTO #PRODUTOS_EXCESSO_LOJA         
         
  FROM REMANEJAMENTOS_ESTOQUES_LOJAS_EXCESSOS A WITH(NOLOCK)  
 WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA  
   AND A.QUANTIDADE_EXCESSO > 0   
   AND A.EMPRESA             = @EMPRESA_ORIGEM  
     
           
-------------------------------------------------------  
--DEFINIR TEMP DOS PRODUTOS E LOJAS PARA RECEBER O EXCESSO  
---------------------------------------------------------  
  
IF OBJECT_ID('TEMPDB..#PRODUTOS_NECESSIDADES_LOJA') IS NOT NULL DROP TABLE #PRODUTOS_NECESSIDADES_LOJA  
  
  
SELECT A.*,         
       CAST(0 AS NUMERIC(15,6)) AS PROPORCAO_RATEIO,  
       CAST(0 AS NUMERIC(15,2)) AS QUANTIDADE_SUGERIDA_UNITARIA_ATUAL,  
       A.QUANTIDADE_SUGERIDA_UNITARIA AS QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO  
                
  INTO #PRODUTOS_NECESSIDADES_LOJA         
         
  FROM REMANEJAMENTOS_ESTOQUES_LOJAS_NECESSIDADES A WITH(NOLOCK)  
 WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA  
   AND A.QUANTIDADE_EXCESSO         =  0   
   AND A.EMPRESA <> @EMPRESA_ORIGEM  
         
     
--CALCULA A PROPOR��O--   
UPDATE #PRODUTOS_NECESSIDADES_LOJA  
     
   SET PROPORCAO_RATEIO = CASE WHEN X.TOTAL_LIMITE_CORTE_EXCESSO > 0   
                               THEN LIMITE_CORTE_EXCESSO / X.TOTAL_LIMITE_CORTE_EXCESSO  
                               ELSE 0  
                          END  
     
 FROM #PRODUTOS_NECESSIDADES_LOJA A WITH(NOLOCK)  
 JOIN ( SELECT A.PRODUTO,  
               SUM(A.LIMITE_CORTE_EXCESSO) AS TOTAL_LIMITE_CORTE_EXCESSO  
          FROM #PRODUTOS_NECESSIDADES_LOJA A   
      GROUP BY A.PRODUTO ) X ON X.PRODUTO = A.PRODUTO  
         
  
--CALCULA O RATEIO DO EXCESSO     
UPDATE #PRODUTOS_NECESSIDADES_LOJA  
     
   SET QUANTIDADE_SUGERIDA_UNITARIA_ATUAL  = CASE WHEN ( ESTOQUE_SALDO_VIRTUAL                                +   
                                                        CEILING( X.QUANTIDADE_EXCESSO * A.PROPORCAO_RATEIO )  +   
                                                        QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO ) > LIMITE_CORTE_EXCESSO  
  
                                                  THEN LIMITE_CORTE_EXCESSO - ( ESTOQUE_SALDO_VIRTUAL + QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO )  
                                                    
                                                  ELSE CEILING( X.QUANTIDADE_EXCESSO * A.PROPORCAO_RATEIO )  
                                                    
                                            END  
     
                                                          
  FROM #PRODUTOS_NECESSIDADES_LOJA A WITH(NOLOCK)  
  JOIN ( SELECT A.PRODUTO,  
                SUM(A.QUANTIDADE_EXCESSO) AS QUANTIDADE_EXCESSO  
           FROM #PRODUTOS_EXCESSO_LOJA A   
       GROUP BY A.PRODUTO ) X ON X.PRODUTO = A.PRODUTO         
  
  
UPDATE #PRODUTOS_NECESSIDADES_LOJA SET QUANTIDADE_SUGERIDA_UNITARIA_ATUAL = 0 WHERE QUANTIDADE_SUGERIDA_UNITARIA_ATUAL < 0   
  
  
--ATUALIZA A QUANTIDADE ACUMULADA--  
UPDATE #PRODUTOS_NECESSIDADES_LOJA   
    
   SET QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO = QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO + QUANTIDADE_SUGERIDA_UNITARIA_ATUAL  
                                                                                                                                    
                                                       
  
--**********************************************************************************---  
---------------------------------------------------------------------------------------  
--ROTINA PARA ACERTAR AS DIFERENCAS DE RATEIO QUE EXCEDEREM O EXCESSO DA LOJA ORIGEM  
---------------------------------------------------------------------------------------  
--**********************************************************************************---  
  
DECLARE @PRODUTO_AJUSTE NUMERIC  
DECLARE @EMPRESA_AJUSTE NUMERIC  
  
IF OBJECT_ID('TEMPDB..#RATEIO_AJUSTE') IS NOT NULL DROP TABLE #RATEIO_AJUSTE  
  
SELECT A.PRODUTO,                
       MAX(X.QUANTIDADE_EXCESSO) - SUM(A.QUANTIDADE_SUGERIDA_UNITARIA_ATUAL) AS DIFERENCA,  
       SUM(A.QUANTIDADE_SUGERIDA_UNITARIA_ATUAL)                             AS QUANTIDADE_SUGERIDA_UNITARIA_ATUAL  
  
  INTO #RATEIO_AJUSTE  
                
  FROM #PRODUTOS_NECESSIDADES_LOJA A   
  JOIN ( SELECT A.PRODUTO,  
                SUM(A.QUANTIDADE_EXCESSO) AS QUANTIDADE_EXCESSO  
           FROM #PRODUTOS_EXCESSO_LOJA A   
       GROUP BY A.PRODUTO ) X ON X.PRODUTO = A.PRODUTO         
   
 GROUP BY A.PRODUTO  
           
 HAVING MAX(X.QUANTIDADE_EXCESSO) - SUM(A.QUANTIDADE_SUGERIDA_UNITARIA) < 0   
   
   
 --WHILE PARA ACERTO--  
 WHILE EXISTS ( SELECT TOP 1 1   
                  FROM #RATEIO_AJUSTE A   
                 WHERE A.DIFERENCA < 0 )  
                   
                   
                   
      BEGIN   
        
             --PEGA O PRODUTO--  
             SELECT TOP 1 @PRODUTO_AJUSTE = A.PRODUTO  
               FROM #RATEIO_AJUSTE A WITH(NOLOCK)  
              WHERE A.DIFERENCA < 0  
           ORDER BY A.PRODUTO  
              
            --PEGA A EMPRESA--  
             SELECT TOP 1 @EMPRESA_AJUSTE = A.EMPRESA  
               FROM #PRODUTOS_NECESSIDADES_LOJA A WITH(NOLOCK)  
              WHERE A.PRODUTO = @PRODUTO_AJUSTE  
                AND A.QUANTIDADE_SUGERIDA_UNITARIA_ATUAL >= 1                 
           ORDER BY A.QUANTIDADE_SUGERIDA_UNITARIA_ATUAL DESC  
                
              --ACERTA O RATEIO                
              UPDATE #PRODUTOS_NECESSIDADES_LOJA  
                
                 SET QUANTIDADE_SUGERIDA_UNITARIA_ATUAL = QUANTIDADE_SUGERIDA_UNITARIA_ATUAL - 1  
                  
                FROM #PRODUTOS_NECESSIDADES_LOJA A WITH(NOLOCK)  
               WHERE A.EMPRESA = @EMPRESA_AJUSTE  
                 AND A.PRODUTO = @PRODUTO_AJUSTE  
                   
                                    
              UPDATE #RATEIO_AJUSTE SET DIFERENCA = DIFERENCA + 1  WHERE PRODUTO  = @PRODUTO_AJUSTE  
                     
                
      END   
        
  
       --ATUALIZA A QUANTIDADE ACUMULADA--  
       UPDATE #PRODUTOS_NECESSIDADES_LOJA   
    
          SET QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO = QUANTIDADE_SUGERIDA_UNITARIA + QUANTIDADE_SUGERIDA_UNITARIA_ATUAL  
                                      
                                                  
      -------------------------------------------------------------------  
      --ATUALIZA A QUANTIDADE ACUMULADA NA TABELA FISICA  
      -------------------------------------------------------------------  
        
      UPDATE REMANEJAMENTOS_ESTOQUES_LOJAS_NECESSIDADES  
        
         SET QUANTIDADE_SUGERIDA_UNITARIA = B.QUANTIDADE_SUGERIDA_UNITARIA_ACUMULADO  
        
        FROM REMANEJAMENTOS_ESTOQUES_LOJAS_NECESSIDADES A WITH(NOLOCK)  
        JOIN #PRODUTOS_NECESSIDADES_LOJA                B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO  
                                                                      AND B.EMPRESA = A.EMPRESA  
         
       WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA  
        
  
      -------------------------------------------------------------------  
      --ATUALIZA A EMPRESA ORIGEM COMO PROCESSADO EXCESSO = 'S'  
      -------------------------------------------------------------------  
        
      UPDATE REMANEJAMENTOS_ESTOQUES_LOJAS_ORIGENS  
        
         SET PROCESSADO_EXCESSO = 'S'  
        
       WHERE REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA  
         AND EMPRESA                    = @EMPRESA_ORIGEM  
  
        
      -------------------------------------------------------------------  
      --GERA A TABELA DE QUANTIDADES  
      --QUE SER� UTILIZADA PARA A GERA��O DOS ROMANEIOS  
      -------------------------------------------------------------------  
        
        
      DELETE REMANEJAMENTOS_ESTOQUES_LOJAS_QUANTIDADES   
        FROM REMANEJAMENTOS_ESTOQUES_LOJAS_QUANTIDADES A WITH(NOLOCK)  
       WHERE A.REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA  
         AND A.EMPRESA_ORIGEM             = @EMPRESA_ORIGEM  
           
       INSERT INTO REMANEJAMENTOS_ESTOQUES_LOJAS_QUANTIDADES (    
             
                   REMANEJAMENTO_ESTOQUE_LOJA,  
                   EMPRESA_ORIGEM,  
                   EMPRESA_DESTINO,  
                   PRODUTO,  
                   QUANTIDADE,  
       MARCA )  
                     
                     
            SELECT @REMANEJAMENTO_ESTOQUE_LOJA,  
                   @EMPRESA_ORIGEM,  
                   A.EMPRESA AS EMPRESA_DESTINO,  
                   A.PRODUTO,  
                   A.QUANTIDADE_SUGERIDA_UNITARIA_ATUAL AS QUANTIDADE,  
       C.DESCRICAO                          AS MARCA  
  
                                        
              FROM #PRODUTOS_NECESSIDADES_LOJA A    
              JOIN PRODUTOS                    B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO  
         LEFT JOIN MARCAS                      C WITH(NOLOCK) ON C.MARCA   = B.MARCA  
               
    WHERE 1=1  
    AND A.QUANTIDADE_SUGERIDA_UNITARIA_ATUAL > 0   


	SELECT * FROM REMANEJAMENTOS_ESTOQUES_LOJAS_QUANTIDADES WHERE REMANEJAMENTO_ESTOQUE_LOJA = @REMANEJAMENTO_ESTOQUE_LOJA


                
  
--END --END PROCEDURE--  
  
  
  
  
  
  