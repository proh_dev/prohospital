/* ====================================================== */
/* CONSULTA DE NOTAS FISCAIS ELETRONICAS DE ENTRADAS      */
/* AUTOR   : YNOA PEDRO 	                              */
/* DATA    : '/'                                          */                   
/* ====================================================== */  

DECLARE @TIPO_DOCUMENTO        VARCHAR(1)
DECLARE @ENTIDADE              NUMERIC
DECLARE @EMPRESA_INI           NUMERIC(15,0) 
DECLARE @EMPRESA_FIM           NUMERIC(15,0) 
DECLARE @VALIDACAO_NFE         VARCHAR(1)           
DECLARE @DATA_INI              DATETIME
DECLARE @DATA_FIM              DATETIME
DECLARE @VENDA_CONTROLADA      VARCHAR(1)
DECLARE @VENDEDOR              NUMERIC
DECLARE @CLASSIFICACAO_CLIENTE NUMERIC
DECLARE @STATUS_NOTAS          VARCHAR(1)
                      
                  
                                            
SET @TIPO_DOCUMENTO        = '0'
SET @DATA_INI              = '01/01/2017'
SET @DATA_FIM              = '31/01/2017'
SET @VENDA_CONTROLADA      = 'N'
SET @VENDEDOR              = NULL
SET @CLASSIFICACAO_CLIENTE = NULL
SET @STATUS_NOTAS          = 'T' -- (E=Emitidas / C=Canceladas / T=Todas)

IF @VENDA_CONTROLADA IS NULL OR @VENDA_CONTROLADA  = '' SET @VENDA_CONTROLADA = 'N'

   SET @ENTIDADE    = NULL
   SET @EMPRESA_INI = NULL
   SET @EMPRESA_FIM = NULL

--SET @TIPO_DOCUMENTO   = :TIPO_DOCUMENTO -- = TODOS, '1'= NF FATURAMENTO, '2'= DV FAT , '3'= DV COMPRAS , '4'= NF_FATURAMENTO DEVOLUCOES
--SET @DATA_INI         = :DATA_INI
--SET @DATA_FIM         = :DATA_FIM
--SET @VENDA_CONTROLADA = 'N'

--IF ISNUMERIC(:VENDEDOR) = 1
--   SET @VENDEDOR = :VENDEDOR
--ELSE
--   SET @VENDEDOR = NULL

--IF @VENDA_CONTROLADA IS NULL OR @VENDA_CONTROLADA  = '' SET @VENDA_CONTROLADA = 'N'

--IF ISNUMERIC(:ENTIDADE) = 1 
--   SET @ENTIDADE = :ENTIDADE
--ELSE
--   SET @ENTIDADE = NULL

--IF ISNUMERIC(:EMPRESA_INI) = 1         
--    SET @EMPRESA_INI = :EMPRESA_INI              
--  ELSE  
--    SET @EMPRESA_INI = NULL       


--IF ISNUMERIC(:EMPRESA_FIM) = 1 
--    SET @EMPRESA_FIM = :EMPRESA_FIM
-- ELSE  
--    SET @EMPRESA_FIM = NULL

--IF ISNUMERIC(:CLASSIFICACAO_CLIENTE) = 1
--   SET @CLASSIFICACAO_CLIENTE = :CLASSIFICACAO_CLIENTE
--ELSE
--   SET @CLASSIFICACAO_CLIENTE = NULL

--SET @STATUS_NOTAS          = :STATUS_NOTAS -- (E=Emitidas / C=Canceladas / T=Todas)


--IF LEN(@VALIDACAO_NFE) = 0 SET @VALIDACAO_NFE = '0'


--GERACAO DE TABELA TEMPORARIA--

--NOTA DE FATURAMENTO--                                                                  

if object_id('tempdb..#NF_FATURAMENTO_RECIBOS_NFE') is not null
   DROP TABLE #NF_FATURAMENTO_RECIBOS_NFE

 SELECT MAX(B.REGISTRO_NFE) AS REGISTRO_NFE,
        A.NF_FATURAMENTO,
        CASE WHEN MAX( ISNULL(E.PRODUTO_FARMACIA,0) ) <> 0
             THEN 'S'
             ELSE 'N'
        END AS VENDA_CONTROLADA

    INTO #NF_FATURAMENTO_RECIBOS_NFE
                                                                         
    FROM NF_FATURAMENTO A WITH(NOLOCK) 
    JOIN NFE_CABECALHO  B WITH(NOLOCK)  ON B.XML_cNF_B03   = A.NF_FATURAMENTO
                                       AND B.XML_serie_B07 = A.NF_SERIE 
                                       AND B.XML_nNF_B08   = A.NF_NUMERO
                                       AND B.EMPRESA       = A.EMPRESA  
    JOIN NFE_LOG        C WITH(NOLOCK)  ON C.REGISTRO_NFE  = B.REGISTRO_NFE
                                       AND C.STATUS        = 'XML DO DESTINATARIO'
    JOIN NF_FATURAMENTO_PRODUTOS D WITH(NOLOCK)  ON D.NF_FATURAMENTO = A.NF_FATURAMENTO
LEFT JOIN PRODUTOS_FARMACIA      E WITH(NOLOCK)  ON E.PRODUTO        = D.PRODUTO
                                                AND E.VENDA_CONTROLADA = 'S'
                        
   WHERE 1            = 1
     AND (A.EMISSAO       >= @DATA_INI OR @DATA_INI = 0)
     AND (A.EMISSAO       <= @DATA_FIM OR @DATA_FIM = 0)
     AND (A.ENTIDADE       = @ENTIDADE OR @ENTIDADE IS NULL)
     AND (A.EMPRESA       >=  @EMPRESA_INI   OR @EMPRESA_INI           IS NULL)
     AND (A.EMPRESA       <=  @EMPRESA_FIM   OR @EMPRESA_FIM           IS NULL)
     AND (@TIPO_DOCUMENTO  = '1' OR @TIPO_DOCUMENTO = '0' )
     
GROUP BY A.NF_FATURAMENTO


--DEVOLUCAO DE VENDAS--

if object_id('tempdb..#DEV_PRODUTOS_CAIXAS_RECIBOS_NFE') is not null
   DROP TABLE #DEV_PRODUTOS_CAIXAS_RECIBOS_NFE

  SELECT MAX(B.REGISTRO_NFE) AS REGISTRO_NFE,
         A.DEVOLUCAO_PRODUTO,
        CASE WHEN MAX( ISNULL(E.PRODUTO_FARMACIA,0) ) <> 0
             THEN 'S'
             ELSE 'N'
        END AS VENDA_CONTROLADA         

    INTO #DEV_PRODUTOS_CAIXAS_RECIBOS_NFE

    FROM DEV_PRODUTOS_CAIXAS A WITH(NOLOCK) 
    JOIN NFE_CABECALHO       B WITH(NOLOCK)  ON B.XML_cNF_B03   = A.DEVOLUCAO_PRODUTO
                                            AND B.XML_serie_B07 = A.NF_SERIE 
                                            AND B.XML_nNF_B08   = A.NF_NUMERO
                                            AND B.EMPRESA       = A.EMPRESA  
                                                   
    JOIN NFE_LOG             C WITH(NOLOCK)  ON C.REGISTRO_NFE  = B.REGISTRO_NFE
                                            AND C.STATUS         = 'XML DO DESTINATARIO'    
                                            
    JOIN DEV_PRODUTOS            D WITH(NOLOCK)  ON D.DEVOLUCAO_PRODUTO = A.DEVOLUCAO_PRODUTO
LEFT JOIN PRODUTOS_FARMACIA      E WITH(NOLOCK)  ON E.PRODUTO        = D.PRODUTO
                                                AND E.VENDA_CONTROLADA = 'S'
                                                                                           
   WHERE 1            = 1
   AND (A.MOVIMENTO       >= @DATA_INI OR @DATA_INI = 0)
   AND (A.MOVIMENTO       <= @DATA_FIM OR @DATA_FIM = 0)
   AND (A.ENTIDADE         = @ENTIDADE OR @ENTIDADE IS NULL)
   AND (A.EMPRESA         >=  @EMPRESA_INI   OR @EMPRESA_INI           IS NULL)
   AND (A.EMPRESA         <=  @EMPRESA_FIM   OR @EMPRESA_FIM           IS NULL) 
   AND (@TIPO_DOCUMENTO    = '2' OR @TIPO_DOCUMENTO = '0' )   
   
   

GROUP BY A.DEVOLUCAO_PRODUTO

--NOTA DE DEVOLUCAO DE COMPRAS--

if object_id('tempdb..#NF_COMPRA_DEVOLUCOES_RECIBOS_NFE') is not null
   DROP TABLE #NF_COMPRA_DEVOLUCOES_RECIBOS_NFE

  SELECT MAX(B.REGISTRO_NFE) AS REGISTRO_NFE,
         A.NF_COMPRA_DEVOLUCAO,
        CASE WHEN MAX( ISNULL(E.PRODUTO_FARMACIA,0) ) <> 0
             THEN 'S'
             ELSE 'N'
        END AS VENDA_CONTROLADA         

    INTO #NF_COMPRA_DEVOLUCOES_RECIBOS_NFE

    FROM NF_COMPRA_DEVOLUCOES A WITH(NOLOCK) 
    JOIN NFE_CABECALHO        B WITH(NOLOCK)  ON B.XML_cNF_B03   = A.NF_COMPRA_DEVOLUCAO
                                             AND B.XML_serie_B07 = A.NF_SERIE 
                                             AND B.XML_nNF_B08   = A.NF_NUMERO
                                             AND B.EMPRESA       = A.EMPRESA  
    JOIN NFE_LOG              C WITH(NOLOCK)  ON C.REGISTRO_NFE  = B.REGISTRO_NFE
                                             AND C.STATUS        = 'XML DO DESTINATARIO'
                                             
    JOIN NF_COMPRA_DEVOLUCOES_PRODUTOS D WITH(NOLOCK)  ON D.NF_COMPRA_DEVOLUCAO = A.NF_COMPRA_DEVOLUCAO
LEFT JOIN PRODUTOS_FARMACIA            E WITH(NOLOCK)  ON E.PRODUTO        = D.PRODUTO
                                                      AND E.VENDA_CONTROLADA = 'S'                                             
                    
   WHERE 1            = 1
   AND (A.EMISSAO       >= @DATA_INI OR @DATA_INI = 0)
   AND (A.EMISSAO       <= @DATA_FIM OR @DATA_FIM = 0)
   AND (A.ENTIDADE       = @ENTIDADE OR @ENTIDADE IS NULL)
   AND (A.EMPRESA       >=  @EMPRESA_INI   OR @EMPRESA_INI           IS NULL)
   AND (A.EMPRESA       <=  @EMPRESA_FIM   OR @EMPRESA_FIM           IS NULL) 
   AND (@TIPO_DOCUMENTO  = '3' OR @TIPO_DOCUMENTO = '0' )   
   

GROUP BY A.NF_COMPRA_DEVOLUCAO


-- NF FATURAMENTO DEVOLUCOES 

if object_id('tempdb..#NF_FATURAMENTO_DEVOLUCOES_RECIBOS_NFE') is not null
   DROP TABLE #NF_FATURAMENTO_DEVOLUCOES_RECIBOS_NFE

 SELECT MAX(B.REGISTRO_NFE) AS REGISTRO_NFE,
        A.NF_FATURAMENTO_DEVOLUCAO,
        CASE WHEN MAX( ISNULL(E.PRODUTO_FARMACIA,0) ) <> 0
             THEN 'S'
             ELSE 'N'
        END AS VENDA_CONTROLADA

    INTO #NF_FATURAMENTO_DEVOLUCOES_RECIBOS_NFE

    FROM NF_FATURAMENTO_DEVOLUCOES A WITH(NOLOCK) 
    JOIN NFE_CABECALHO             B WITH(NOLOCK)  ON B.XML_cNF_B03   = A.NF_FATURAMENTO_DEVOLUCAO
                                       AND B.XML_serie_B07 = A.NF_SERIE 
                                       AND B.XML_nNF_B08   = A.NF_NUMERO
                                       AND B.EMPRESA       = A.EMPRESA  
    JOIN NFE_LOG                   C WITH(NOLOCK)  ON C.REGISTRO_NFE  = B.REGISTRO_NFE
                                       AND C.STATUS        = 'XML DO DESTINATARIO'
    JOIN NF_FATURAMENTO_DEVOLUCOES_PRODUTOS D WITH(NOLOCK)  ON D.NF_FATURAMENTO_DEVOLUCAO = A.NF_FATURAMENTO_DEVOLUCAO
LEFT JOIN PRODUTOS_FARMACIA      E WITH(NOLOCK)  ON E.PRODUTO        = D.PRODUTO
                                                AND E.VENDA_CONTROLADA = 'S'
                        
   WHERE 1            = 1
     AND (A.EMISSAO       >= @DATA_INI OR @DATA_INI = 0)
     AND (A.EMISSAO       <= @DATA_FIM OR @DATA_FIM = 0)
     AND (A.ENTIDADE       = @ENTIDADE OR @ENTIDADE IS NULL)
     AND (A.EMPRESA       >=  @EMPRESA_INI   OR @EMPRESA_INI           IS NULL)
     AND (A.EMPRESA       <=  @EMPRESA_FIM   OR @EMPRESA_FIM           IS NULL)
     AND (@TIPO_DOCUMENTO = '4' OR @TIPO_DOCUMENTO = '0' )
     
GROUP BY A.NF_FATURAMENTO_DEVOLUCAO

-----------------------------------------------------
------------GERA RESULTADO---------------------------
-----------------------------------------------------


if object_id('tempdb..#RESULTADO') is not null
   DROP TABLE #RESULTADO

--PEGA AS NOTAS DE FATURAMENTO--

SELECT DISTINCT
       A.FORMULARIO_ORIGEM,
       A.TAB_MASTER_ORIGEM,       
       '1'                                        AS TIPO_DOCUMENTO,
       'NF.FATURAMENTO'                           AS DESCRICAO_TIPO_DOCUMENTO,        
       A.NF_FATURAMENTO                           AS REGISTRO,
       A.EMPRESA                                  AS EMPRESA,
       A.ENTIDADE                                 AS ENTIDADE,
       A.NF_NUMERO                                AS DOCUMENTO,
       A.PEDIDO_VENDA                             AS PEDIDO,
       A.MOVIMENTO                                AS MOVIMENTO,
       dbo.NFE_PROTOCOLO ( C.REGISTRO_NFE )       AS PROTOCOLO_SEFAZ,
       dbo.NFE_CHAVE     ( C.REGISTRO_NFE )       AS CHAVE,
       B.XML_UF_E12                               AS ESTADO_DESTINO,
       ( SELECT TOP 1 A.XML_CFOP_I08
           FROM NFE_ITENS A WITH(NOLOCK)
          WHERE A.REGISTRO_NFE = B.REGISTRO_NFE ) AS CFOP,
       D.TOTAL_FRETE                              AS TOTAL_FRETE,
       D.TOTAL_GERAL                              AS TOTAL_GERAL,
       D.ICMS_BASE_CALCULO                        AS ICMS_BASE_CALCULO,
       D.ICMS_VALOR                               AS ICMS_VALOR,
       C.REGISTRO_NFE                             AS REGISTRO_NFE,
       C.VENDA_CONTROLADA                         AS VENDA_CONTROLADA,
       A.OPERACAO_FISCAL                          AS OPERACAO_FISCAL,
       NULL                                       AS NF_NUMERO_ORIGEM,
       E.EMPENHO                                  AS EMPENHO,
       E.PREGAO                                   AS PREGAO,
       A.VENDEDOR                                 AS VENDEDOR
             
  INTO #RESULTADO
                  
  FROM NF_FATURAMENTO              A WITH(NOLOCK) 
  JOIN #NF_FATURAMENTO_RECIBOS_NFE C WITH(NOLOCK) ON C.NF_FATURAMENTO = A.NF_FATURAMENTO
  JOIN NF_FATURAMENTO_TOTAIS       D WITH(NOLOCK) ON D.NF_FATURAMENTO = A.NF_FATURAMENTO
  JOIN NFE_CABECALHO               B WITH(NOLOCK) ON B.REGISTRO_NFE   = C.REGISTRO_NFE
  LEFT
  JOIN NF_FATURAMENTO_OBSERVACOES  E WITH(NOLOCK) ON E.NF_FATURAMENTO = A.NF_FATURAMENTO

 WHERE 1 = 1
   AND (A.MOVIMENTO       >= @DATA_INI OR @DATA_INI = 0)
   AND (A.MOVIMENTO       <= @DATA_FIM OR @DATA_FIM = 0)
   AND (A.ENTIDADE         = @ENTIDADE OR @ENTIDADE IS NULL) 
   AND (A.VENDEDOR         = @VENDEDOR OR @VENDEDOR IS NULL)
   
   
   UNION ALL
   
--PEGA AS NOTAS DE DEVOLUCAO FATURAMENTO--

SELECT DISTINCT
       A.FORMULARIO_ORIGEM,
       A.TAB_MASTER_ORIGEM,       
       '2'                                        AS TIPO_DOCUMENTO,
       'NF.DEV.VENDA'                             AS DESCRICAO_TIPO_DOCUMENTO,        
       A.DEVOLUCAO_PRODUTO                        AS REGISTRO,
       A.EMPRESA                                  AS EMPRESA,
       A.ENTIDADE                                 AS ENTIDADE,
       A.NF_NUMERO                                AS DOCUMENTO,
       NULL                                       AS PEDIDO,
       A.MOVIMENTO                                AS MOVIMENTO,
       dbo.NFE_PROTOCOLO ( C.REGISTRO_NFE )       AS PROTOCOLO_SEFAZ,
       dbo.NFE_CHAVE     ( C.REGISTRO_NFE )       AS CHAVE,
       B.XML_UF_E12                               AS ESTADO_DESTINO,
       ( SELECT TOP 1 A.XML_CFOP_I08
           FROM NFE_ITENS A WITH(NOLOCK)
          WHERE A.REGISTRO_NFE = B.REGISTRO_NFE ) AS CFOP,
       0.00                                       AS TOTAL_FRETE,
       D.VALOR_TOTAL_DEVOLUCAO                    AS TOTAL_GERAL,
       0                                          AS ICMS_BASE_CALCULO,
       0.00                                       AS ICMS_VALOR,
       C.REGISTRO_NFE                             AS REGISTRO_NFE      ,
       C.VENDA_CONTROLADA                         AS VENDA_CONTROLADA ,
       A.OPERACAO_FISCAL                          AS OPERACAO_FISCAL     ,
       A.CUPOM                                    AS NF_NUMERO_ORIGEM,
       NULL                                       AS EMPENHO,
       NULL                                       AS PREGAO,
       A.VENDEDOR                                 AS VENDEDOR
                               
  FROM DEV_PRODUTOS_CAIXAS                    A WITH(NOLOCK) 
  JOIN #DEV_PRODUTOS_CAIXAS_RECIBOS_NFE       C WITH(NOLOCK) ON C.DEVOLUCAO_PRODUTO = A.DEVOLUCAO_PRODUTO
  JOIN DEV_PRODUTOS_TOTAIS                    D WITH(NOLOCK) ON D.DEVOLUCAO_PRODUTO = A.DEVOLUCAO_PRODUTO
  JOIN NFE_CABECALHO                          B WITH(NOLOCK) ON B.REGISTRO_NFE      = C.REGISTRO_NFE

 WHERE 1 = 1
   AND (A.MOVIMENTO       >= @DATA_INI OR @DATA_INI = 0)
   AND (A.MOVIMENTO       <= @DATA_FIM OR @DATA_FIM = 0)
   AND (A.ENTIDADE         = @ENTIDADE OR @ENTIDADE IS NULL)
   AND (A.VENDEDOR         = @VENDEDOR OR @VENDEDOR IS NULL)

   
   UNION ALL
   
   --PEGA AS NOTAS DE DEVOLUCAO COMPRA --

    SELECT DISTINCT
           A.FORMULARIO_ORIGEM,
           A.TAB_MASTER_ORIGEM,       
          '3'                                      AS TIPO_DOCUMENTO,
          'NF. DEV. COMPRA'                        AS DESCRICAO_TIPO_DOCUMENTO,        
          A.NF_COMPRA_DEVOLUCAO                    AS REGISTRO,
          A.EMPRESA                                AS EMPRESA,
          A.ENTIDADE                               AS ENTIDADE,
          A.NF_NUMERO                              AS DOCUMENTO,
          NULL                                     AS PEDIDO,
          A.MOVIMENTO                              AS MOVIMENTO,
          dbo.NFE_PROTOCOLO ( C.REGISTRO_NFE )     AS PROTOCOLO_SEFAZ,
          dbo.NFE_CHAVE     ( C.REGISTRO_NFE )     AS CHAVE,
          B.XML_UF_E12                             AS ESTADO_DESTINO,
          ( SELECT TOP 1 A.XML_CFOP_I08
              FROM NFE_ITENS A WITH(NOLOCK)
             WHERE A.REGISTRO_NFE = B.REGISTRO_NFE
          )                                        AS CFOP,
          D.TOTAL_FRETE                            AS TOTAL_FRETE,
          D.TOTAL_GERAL                            AS TOTAL_GERAL,
          D.ICMS_BASE_CALCULO                      AS ICMS_BASE_CALCULO,
          D.ICMS_VALOR                             AS ICMS_VALOR,
          C.REGISTRO_NFE                           AS REGISTRO_NFE  ,
          C.VENDA_CONTROLADA                       AS VENDA_CONTROLADA  ,
          A.OPERACAO_FISCAL                        AS OPERACAO_FISCAL ,
          E.NF_NUMERO                              AS NF_NUMERO_ORIGEM,
          NULL                                     AS EMPENHO,
          NULL                                     AS PREGAO,
          NULL                                     AS VENDEDOR
                                
     FROM NF_COMPRA_DEVOLUCOES              A WITH(NOLOCK) 
LEFT JOIN #NF_COMPRA_DEVOLUCOES_RECIBOS_NFE C WITH(NOLOCK) ON C.NF_COMPRA_DEVOLUCAO = A.NF_COMPRA_DEVOLUCAO
     JOIN NFE_CABECALHO                     B WITH(NOLOCK) ON B.REGISTRO_NFE        = C.REGISTRO_NFE
     JOIN NF_COMPRA_DEVOLUCOES_TOTAIS       D WITH(NOLOCK) ON D.NF_COMPRA_DEVOLUCAO = A.NF_COMPRA_DEVOLUCAO
LEFT JOIN NF_COMPRA                         E WITH(NOLOCK) ON E.NF_COMPRA           = A.NF_COMPRA

    WHERE 1            = 1
   AND (A.EMISSAO       >= @DATA_INI OR @DATA_INI = 0)
   AND (A.EMISSAO       <= @DATA_FIM OR @DATA_FIM = 0)
   AND (A.ENTIDADE       = @ENTIDADE OR @ENTIDADE IS NULL)  

UNION ALL

--PEGA AS NOTAS DE FATURAMENTO DEVOLUÇÕES--

SELECT DISTINCT
       A.FORMULARIO_ORIGEM,
       A.TAB_MASTER_ORIGEM,       
       '4'                                        AS TIPO_DOCUMENTO,
       'NF.FATURAMENTO DEVOLUCAO'                 AS DESCRICAO_TIPO_DOCUMENTO,        
       A.NF_FATURAMENTO_DEVOLUCAO                 AS REGISTRO,
       A.EMPRESA                                  AS EMPRESA,
       A.ENTIDADE                                 AS ENTIDADE,
       A.NF_NUMERO                                AS DOCUMENTO,
       NULL                                       AS PEDIDO,
       A.MOVIMENTO                                AS MOVIMENTO,
       dbo.NFE_PROTOCOLO ( C.REGISTRO_NFE )       AS PROTOCOLO_SEFAZ,
       dbo.NFE_CHAVE     ( C.REGISTRO_NFE )       AS CHAVE,
       B.XML_UF_E12                               AS ESTADO_DESTINO,
       ( SELECT TOP 1 A.XML_CFOP_I08
           FROM NFE_ITENS A WITH(NOLOCK)
          WHERE A.REGISTRO_NFE = B.REGISTRO_NFE ) AS CFOP,
       D.TOTAL_FRETE                              AS TOTAL_FRETE,
       D.TOTAL_GERAL                              AS TOTAL_GERAL,
       D.ICMS_BASE_CALCULO                        AS ICMS_BASE_CALCULO,
       D.ICMS_VALOR                               AS ICMS_VALOR,
       C.REGISTRO_NFE                             AS REGISTRO_NFE,
       C.VENDA_CONTROLADA                         AS VENDA_CONTROLADA,
       A.OPERACAO_FISCAL                          AS OPERACAO_FISCAL,
       E.NF_NUMERO                                AS NF_NUMERO_ORIGEM,
       NULL                                       AS EMPENHO,
       NULL                                       AS PREGAO,
       A.VENDEDOR                                 AS VENDEDOR
                  
  FROM NF_FATURAMENTO_DEVOLUCOES              A WITH(NOLOCK) 
  JOIN #NF_FATURAMENTO_DEVOLUCOES_RECIBOS_NFE C WITH(NOLOCK) ON C.NF_FATURAMENTO_DEVOLUCAO = A.NF_FATURAMENTO_DEVOLUCAO
  JOIN NF_FATURAMENTO_DEVOLUCOES_TOTAIS       D WITH(NOLOCK) ON D.NF_FATURAMENTO_DEVOLUCAO = A.NF_FATURAMENTO_DEVOLUCAO
  JOIN NFE_CABECALHO                          B WITH(NOLOCK) ON B.REGISTRO_NFE             = C.REGISTRO_NFE
LEFT JOIN NF_FATURAMENTO                      E WITH(NOLOCK) ON E.NF_FATURAMENTO           = A.NF_FATURAMENTO_ORIGEM
 
 WHERE 1 = 1
   AND (A.MOVIMENTO       >= @DATA_INI OR @DATA_INI = 0)
   AND (A.MOVIMENTO       <= @DATA_FIM OR @DATA_FIM = 0)
   AND (A.ENTIDADE         = @ENTIDADE OR @ENTIDADE IS NULL)
   AND (A.VENDEDOR         = @VENDEDOR OR @VENDEDOR IS NULL)

 ORDER BY MOVIMENTO,
          TIPO_DOCUMENTO,
          DOCUMENTO


-----------------------------------------------------
------------SQL FINAL NOTA FISCAL--------------------
-----------------------------------------------------

   SELECT 
          A.REGISTRO   AS REG_MASTER_ORIGEM,
          A.FORMULARIO_ORIGEM,        
          A.TAB_MASTER_ORIGEM,       
          A.TIPO_DOCUMENTO,
          A.DESCRICAO_TIPO_DOCUMENTO, 
          A.REGISTRO,
          A.EMPRESA,
          A.ENTIDADE,
          B.NOME, 
          A.DOCUMENTO,
          A.PEDIDO,
          NULL PEDIDO_WEB,
          A.MOVIMENTO,
          A.PROTOCOLO_SEFAZ,
          A.CHAVE,
          A.ESTADO_DESTINO,
          A.CFOP,
          A.TOTAL_FRETE,
          A.TOTAL_GERAL,
          A.ICMS_BASE_CALCULO,
          A.ICMS_VALOR,
          CASE WHEN C.NFE_CANCELAMENTO IS NOT NULL 
               THEN 'CANCELADA'       
               ELSE 'EMITIDA'               
          END AS STATUS_NOTA,
          A.REGISTRO_NFE,
          A.VENDA_CONTROLADA,
          A.OPERACAO_FISCAL,
          A.NF_NUMERO_ORIGEM,
          1                  AS CONTADOR           ,
          A.EMPENHO,
          A.PREGAO,
          D.TOTAL_DEVOLVIDO,
          CAST(ISNULL(A.VENDEDOR,103) AS VARCHAR) + '-' + ISNULL(E.NOME,'SEM VENDEDOR') AS VENDEDOR,
          F.DESCRICAO                                                                   AS GRUPO_CLIENTE
         


     FROM #RESULTADO              A 
     JOIN ENTIDADES               B WITH(NOLOCK)  ON B.ENTIDADE  = A.ENTIDADE
     LEFT
     JOIN NFE_CANCELAMENTOS       C WITH(NOLOCK)  ON C.IDNOTA    = A.CHAVE
                                                 AND C.STATUS    = 4
     LEFT 
     JOIN (

              SELECT B.NF_FATURAMENTO_DEVOLUCAO, B.NF_NUMERO, A.EMPRESA, A.ENTIDADE, C.CHAVE_NFE, C.REGISTRO_NFE,
              SUM(CASE WHEN E.NFE_CANCELAMENTO IS NOT NULL 
                   THEN 0
                   ELSE D.TOTAL_GERAL 
                  END
                  ) AS TOTAL_DEVOLVIDO

                FROM NF_FATURAMENTO                     A WITH(NOLOCK)
                JOIN NF_FATURAMENTO_DEVOLUCOES          B WITH(NOLOCK) ON B.NF_FATURAMENTO_ORIGEM     = A.NF_FATURAMENTO
                JOIN NFE_CABECALHO                      C WITH(NOLOCK) ON C.FORMULARIO_ORIGEM         = (SELECT NUMID FROM FORMULARIOS WHERE FORMULARIO LIKE 'NF_FATURAMENTO')
                                                                      AND C.REG_MASTER_ORIGEM         = B.NF_FATURAMENTO_ORIGEM
                JOIN NF_FATURAMENTO_DEVOLUCOES_TOTAIS   D WITH(NOLOCK) ON D.NF_FATURAMENTO_DEVOLUCAO  = B.NF_FATURAMENTO_DEVOLUCAO
                LEFT JOIN NFE_CANCELAMENTOS             E WITH(NOLOCK) ON E.IDNOTA                    = C.CHAVE_NFE
                                                                      AND E.STATUS                    = 4
               WHERE 1=1
               GROUP BY B.NF_FATURAMENTO_DEVOLUCAO, B.NF_NUMERO, A.EMPRESA, A.ENTIDADE, C.CHAVE_NFE, C.REGISTRO_NFE

         )                   D ON D.REGISTRO_NFE = A.REGISTRO_NFE
    LEFT 
    JOIN VENDEDORES              E WITH(NOLOCK) ON E.VENDEDOR = A.VENDEDOR
    LEFT
    JOIN CLASSIFICACOES_CLIENTES F WITH(NOLOCK) ON F.CLASSIFICACAO_CLIENTE = B.CLASSIFICACAO_CLIENTE


     
   WHERE (A.VENDA_CONTROLADA                         = @VENDA_CONTROLADA                  OR @VENDA_CONTROLADA <> 'S' )
     AND (E.VENDEDOR                                 = @VENDEDOR                          OR @VENDEDOR              IS NULL)
     AND (F.CLASSIFICACAO_CLIENTE                    = @CLASSIFICACAO_CLIENTE             OR @CLASSIFICACAO_CLIENTE IS NULL)
     AND (A.EMPRESA                                 >= @EMPRESA_INI                       OR @EMPRESA_INI           IS NULL)
     AND (A.EMPRESA                                 <= @EMPRESA_FIM                       OR @EMPRESA_FIM           IS NULL)
     AND (
          CASE WHEN C.NFE_CANCELAMENTO IS NOT NULL 
               THEN 'C'
               ELSE 'E'
          END = @STATUS_NOTAS OR ISNULL(@STATUS_NOTAS, 'X') NOT IN ('C', 'E')
         )
                                             
                                                         
                                            
 ORDER BY 
          REG_MASTER_ORIGEM,
          A.FORMULARIO_ORIGEM,        
          A.TAB_MASTER_ORIGEM, 
 
 TIPO_DOCUMENTO,
 MOVIMENTO