 ALTER PROCEDURE USP_PROCFIT_GERAR_NF_COMPRA_NF_FATURAMENTO  
AS  
BEGIN  
  
/*  
Demanda 161152  
  
Formul�rio: NF Compra  
Situa��o: Vendas entre empresas Pro Hospital.  
Descri��o: Ao ocorrer opera��es de vendas em que o remetente e destinat�rio for empresas usu�rias Pro Hospital ou E.C. Aquino, por exemplo, vendas entre empresa 01   
e destinat�rio empresa usu�ria 03, as notas de entrada devem constar no formul�rio de notas fiscais de compra e outras entradas.  
  
Por tanto como sinalizado por David em email, deve-se: Atualizar o Processo de gera��o das NF_Compra serem automatizadas para notas de vendas entre empresas do grupo.  
  
Assunto surgiu a partir do e-mail encaminhado por David Mariano - Desenvolvimento Fiscal, em 22/08/2016:  
Bom dia.  
Claiton, � necess�rio atualizar no cliente o processo de gera��o das NF_Compra automaticamente para notas de vendas entre empresas do grupo. J� troquei uma ideia com o Gemada sobre isso na semana passada.  
Para resolver algumas quest�es fiscais, as notas no m�s 07 processei manualmente.  
Qualquer coisa me procure, obrigado.  
  
*/  
  
DECLARE @NF_FATURAMENTO NUMERIC  
  
if object_id('tempdb..#TEMP_NOTAS') is not null    
    DROP TABLE #TEMP_NOTAS  
  
  
SELECT  
    DISTINCT   
    A.NF_FATURAMENTO  
INTO  
    #TEMP_NOTAS  
FROM  
    NF_FATURAMENTO          A WITH(NOLOCK)  
    JOIN PARAMETROS_VENDAS  B WITH(NOLOCK) ON B.EMPRESA_USUARIA  = A.EMPRESA  
    JOIN NFE_CABECALHO      C WITH(NOLOCK) ON C.XML_cNF_B03      = A.NF_FATURAMENTO  
                                          AND C.XML_nNF_B08      = A.NF_NUMERO  
                                          AND C.XML_serie_B07    = A.NF_SERIE  
                                          AND C.EMPRESA          = A.EMPRESA  
                                          AND C.STATUS = 4  
    LEFT JOIN NF_COMPRA     D WITH(NOLOCK) ON D.NF_FATURAMENTO     = A.NF_FATURAMENTO  
    LEFT JOIN NF_COMPRA     E WITH(NOLOCK) ON E.NF_NUMERO          = A.NF_NUMERO  
                                          AND E.ENTIDADE           = A.EMPRESA  
               AND E.NF_SERIE           = A.NF_SERIE  
      
    JOIN EMPRESAS_USUARIAS  F WITH(NOLOCK) ON F.EMPRESA_USUARIA = A.EMPRESA  
    JOIN EMPRESAS_USUARIAS  G WITH(NOLOCK) ON G.ENTIDADE        = A.ENTIDADE  
    JOIN OPERACOES_FISCAIS  H WITH(NOLOCK) ON H.OPERACAO_FISCAL = A.OPERACAO_FISCAL  
 -- Para validar notas canceladas. --  
    LEFT  
 JOIN NOTAS_FISCAIS_ESTADO I WITH(NOLOCK) ON I.CHAVE = A.NF_FATURAMENTO  
                                            AND I.TIPO   = 1  
                                            AND I.ESTADO = 2   
  
WHERE  
    H.CONTRAPARTIDA_AUTOMATICA = 'S'  
  AND A.CONTRAPARTIDA_GERADA = 'N'
	
AND ( D.NF_COMPRA IS NULL AND E.NF_COMPRA IS NULL )  
AND I.NF_ESTADO IS NULL  
  
  
WHILE EXISTS(SELECT TOP 1 1 FROM #TEMP_NOTAS)  
BEGIN  
    SELECT TOP 1 @NF_FATURAMENTO = A.NF_FATURAMENTO FROM #TEMP_NOTAS A  
    DELETE FROM #TEMP_NOTAS WHERE NF_FATURAMENTO = @NF_FATURAMENTO  
      
    EXEC USP_GERAR_NF_COMPRA_NF_FATURAMENTO_RETORNO @NF_FATURAMENTO  
END  
  
END