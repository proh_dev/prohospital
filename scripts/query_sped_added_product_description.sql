DECLARE @TIPO VARCHAR(1)
DECLARE @SPED NUMERIC(15)

SET @TIPO = 'S'--:TIPO
SET @SPED = 399--:SPED              

SELECT C.DESCRICAO	
     , SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),5,4) AS MOVIMENTO
     , A1.SER                  AS SERIE
     , A1.NUM_DOC              AS NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS EMISSAO
     , DBO.ZEROS(A.CST_ICMS,3) AS CST_ICMS
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3) AS CFOP
     , B.DESCRICAO
     , A.ALIQ_ICMS
     , SUM(A.VL_OPR)           AS VL_OPR
     , SUM(A.VL_BC_ICMS)       AS VL_BC_ICMS
     , SUM(A.VL_ICMS)          AS VL_ICMS
	 , A2.DESCR_ITEM           AS ITEM
     
 FROM SPED_FISCAL_C190  A  WITH(NOLOCK)
  JOIN SPED_FISCAL_C100 A1 WITH(NOLOCK)ON  A1.FORMULARIO_ORIGEM  = A.FORMULARIO_ORIGEM
                                       AND A1.TAB_MASTER_ORIGEM  = A.TAB_MASTER_ORIGEM
                                       AND A1.REG_MASTER_ORIGEM  = A.REG_MASTER_ORIGEM
                                       AND A1.SPED_FISCAL        = A.SPED_FISCAL
  LEFT
  JOIN SPED_FISCAL_0200 A2 WITH(NOLOCK)ON A2.SPED_FISCAL        = A.SPED_FISCAL

   JOIN CFOP             B WITH(NOLOCK)ON B.CFOP                 = SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
   JOIN FORMULARIOS      C WITH(NOLOCK)ON C.NUMID                = A.FORMULARIO_ORIGEM

 WHERE A.SPED_FISCAL = @SPED
   AND A.CFOP > CASE @TIPO WHEN 'E'
                           THEN '0000'
                           WHEN 'S'
                           THEN '5000'
                           WHEN 'T'
                           THEN '0000'
                 END
   AND A.CFOP < CASE @TIPO WHEN 'E'
                           THEN '5000'
                           WHEN 'S'
                           THEN '8000'
                           WHEN 'T'
                           THEN '9999'
                 END
                
 GROUP
    BY C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),5,4)
     , A1.SER
     , A1.NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) 
     , DBO.ZEROS(A.CST_ICMS,3)
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)   
     , B.DESCRICAO
     , A.ALIQ_ICMS
	 ,A2.DESCR_ITEM      

 UNION ALL

SELECT C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),5,4) AS MOVIMENTO
     , A1.SER                  AS SERIE
     , A1.NUM_DOC              AS NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS EMISSAO
     , DBO.ZEROS(A.CST_ICMS,3) AS CST_ICMS
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3) AS CFOP
	 , B.DESCRICAO
     , A.ALIQ_ICMS
     , SUM(A.VL_OPR)           AS VL_OPR
     , SUM(A.VL_BC_ICMS)       AS VL_BC_ICMS
     , SUM(A.VL_ICMS)          AS VL_ICMS
	 , A2.DESCR_ITEM           AS ITEM
    
 FROM SPED_FISCAL_C190  A  WITH(NOLOCK)
  JOIN SPED_FISCAL_C100 A1 WITH(NOLOCK) ON A1.FORMULARIO_ORIGEM    = A.FORMULARIO_ORIGEM
                                       AND A1.TAB_MASTER_ORIGEM    = A.TAB_MASTER_ORIGEM
                                       AND A1.REG_MASTER_ORIGEM    = A.REG_MASTER_ORIGEM
                                       AND A1.SPED_FISCAL          = A.SPED_FISCAL
  LEFT
  JOIN SPED_FISCAL_0200 A2 WITH(NOLOCK) ON A2.SPED_FISCAL          = A.SPED_FISCAL

  JOIN CFOP             B  WITH(NOLOCK) ON B.CFOP                  = SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
  JOIN FORMULARIOS      C  WITH(NOLOCK) ON C.NUMID                 = A.FORMULARIO_ORIGEM

 WHERE A.SPED_FISCAL = @SPED
   AND A.CFOP > CASE @TIPO WHEN 'E'
                           THEN '0000'
                           WHEN 'S'
                           THEN '5000'
                           WHEN 'T'
                           THEN '0000'
                 END
   AND A.CFOP < CASE @TIPO WHEN 'E'
                           THEN '5000'
                           WHEN 'S'
                           THEN '8000'
                           WHEN 'T'
                           THEN '9999'
                 END
                
 GROUP
    BY C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_E_S,8),5,4)
     , A1.SER
     , A1.NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) 
     , DBO.ZEROS(A.CST_ICMS,3)
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
     , B.DESCRICAO
     , A.ALIQ_ICMS
	 ,A2.DESCR_ITEM 

 UNION ALL

SELECT C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS MOVIMENTO
     , A1.SER                  AS SERIE
     , A1.NUM_DOC              AS NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS EMISSAO
     , DBO.ZEROS(A.CST_ICMS,3) AS CST_ICMS
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3) AS CFOP
     , B.DESCRICAO
     , A.ALIQ_ICMS
     , SUM(A.VL_OPR)           AS VL_OPR
     , SUM(A.VL_BC_ICMS)       AS VL_BC_ICMS
     , SUM(A.VL_ICMS)          AS VL_ICMS
	 , A2.DESCR_ITEM           AS ITEM
     
 FROM SPED_FISCAL_C190  A  WITH(NOLOCK)
  JOIN SPED_FISCAL_C100 A1 WITH(NOLOCK) ON A1.FORMULARIO_ORIGEM  = A.FORMULARIO_ORIGEM
                                       AND A1.TAB_MASTER_ORIGEM  = A.TAB_MASTER_ORIGEM
                                       AND A1.REG_MASTER_ORIGEM  = A.REG_MASTER_ORIGEM
                                       AND A1.SPED_FISCAL        = A.SPED_FISCAL
  LEFT
  JOIN SPED_FISCAL_0200 A2 WITH(NOLOCK) ON A2.SPED_FISCAL        = A.SPED_FISCAL

  JOIN CFOP             B WITH(NOLOCK)  ON B.CFOP                = SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
  JOIN FORMULARIOS      C WITH(NOLOCK)  ON C.NUMID               = A.FORMULARIO_ORIGEM

 WHERE A.SPED_FISCAL = @SPED
   AND A.CFOP > CASE @TIPO WHEN 'E'
                           THEN '0000'
                           WHEN 'S'
                           THEN '5000'
                           WHEN 'T'
                           THEN '0000'
                 END
   AND A.CFOP < CASE @TIPO WHEN 'E'
                           THEN '5000'
                           WHEN 'S'
                           THEN '8000'
                           WHEN 'T'
                           THEN '9999'
                 END
                
 GROUP
    BY C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4)
     , A1.SER
     , A1.NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) 
     , DBO.ZEROS(A.CST_ICMS,3)
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
     , B.DESCRICAO
     , A.ALIQ_ICMS
	 ,A2.DESCR_ITEM 

 UNION ALL

SELECT C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS MOVIMENTO
     , A1.SER                  AS SERIE
     , A1.NUM_DOC              AS NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS EMISSAO
     , DBO.ZEROS(A.CST_ICMS,3) AS CST_ICMS
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3) AS CFOP
     , B.DESCRICAO
     , A.ALIQ_ICMS
     , SUM(A.VL_OPR)           AS VL_OPR
     , SUM(A.VL_BC_ICMS)       AS VL_BC_ICMS
     , SUM(A.VL_ICMS)          AS VL_ICMS
	 , A2.DESCR_ITEM           AS ITEM
     
 FROM SPED_FISCAL_C190  A  WITH(NOLOCK)
  JOIN SPED_FISCAL_C100 A1 WITH(NOLOCK)ON  A1.FORMULARIO_ORIGEM  = A.FORMULARIO_ORIGEM
                                       AND A1.TAB_MASTER_ORIGEM  = A.TAB_MASTER_ORIGEM
                                       AND A1.REG_MASTER_ORIGEM  = A.REG_MASTER_ORIGEM
                                       AND A1.SPED_FISCAL        = A.SPED_FISCAL
  LEFT
  JOIN SPED_FISCAL_0200 A2 WITH(NOLOCK)ON  A2.SPED_FISCAL        = A.SPED_FISCAL

  JOIN CFOP             B  WITH(NOLOCK)ON  B.CFOP                = SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
  JOIN FORMULARIOS      C  WITH(NOLOCK)ON  C.NUMID               = A.FORMULARIO_ORIGEM
   
 WHERE A.SPED_FISCAL = @SPED
   AND A.CFOP > CASE @TIPO WHEN 'E'
                           THEN '0000'
                           WHEN 'S'
                           THEN '5000'
                           WHEN 'T'
                           THEN '0000'
                 END
   AND A.CFOP < CASE @TIPO WHEN 'E'
                           THEN '5000'
                           WHEN 'S'
                           THEN '8000'
                           WHEN 'T'
                           THEN '9999'
                 END
                
 GROUP
    BY C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4)
     , A1.SER
     , A1.NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) 
     , DBO.ZEROS(A.CST_ICMS,3)
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
     , B.DESCRICAO
     , A.ALIQ_ICMS
	 ,A2.DESCR_ITEM 

 UNION ALL

SELECT 'Mapas Resumos' AS DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS MOVIMENTO
     , NULL                    AS SERIE
     , NULL                    AS NUM_DOC
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) AS EMISSAO
     , DBO.ZEROS(A.CST_ICMS,3) AS CST_ICMS
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3) AS CFOP
     , B.DESCRICAO
     , A.ALIQ_ICMS
     , SUM(A.VL_OPR)           AS VL_OPR
     , SUM(A.VL_BC_ICMS)       AS VL_BC_ICMS
     , SUM(A.VL_ICMS)          AS VL_ICMS
     , A2.DESCR_ITEM           AS ITEM
     
 FROM SPED_FISCAL_C190  A  WITH(NOLOCK)
  JOIN SPED_FISCAL_C100 A1 WITH(NOLOCK)ON  A1.FORMULARIO_ORIGEM  = A.FORMULARIO_ORIGEM
                                       AND A1.TAB_MASTER_ORIGEM  = A.TAB_MASTER_ORIGEM
                                       AND A1.REG_MASTER_ORIGEM  = A.REG_MASTER_ORIGEM
                                       AND A1.SPED_FISCAL        = A.SPED_FISCAL
  LEFT
  JOIN SPED_FISCAL_0200 A2 WITH(NOLOCK)ON  A2.SPED_FISCAL        = A.SPED_FISCAL

  JOIN CFOP             B  WITH(NOLOCK)ON  B.CFOP                = SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
  JOIN FORMULARIOS      C  WITH(NOLOCK)ON  C.NUMID               = A.FORMULARIO_ORIGEM

 WHERE A.SPED_FISCAL = @SPED
   AND A.CFOP > CASE @TIPO WHEN 'E'
                           THEN '0000'
                           WHEN 'S'
                           THEN '5000'
                           WHEN 'T'
                           THEN '0000'
                 END
   AND A.CFOP < CASE @TIPO WHEN 'E'
                           THEN '5000'
                           WHEN 'S'
                           THEN '8000'
                           WHEN 'T'
                           THEN '9999'
                 END
                
 GROUP
    BY C.DESCRICAO
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4)
     , SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),1,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),3,2) + '/'
     + SUBSTRING(DBO.ZEROS(A1.DT_DOC,8),5,4) 
     , DBO.ZEROS(A.CST_ICMS,3)
     , SUBSTRING(CONVERT(VARCHAR,A.CFOP),1,1) + '.' + SUBSTRING(CONVERT(VARCHAR,A.CFOP),2,3)
     , B.DESCRICAO
     , A.ALIQ_ICMS
	 ,A2.DESCR_ITEM 
 ORDER
    BY 1,2,3
