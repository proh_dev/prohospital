 
CREATE PROCEDURE [dbo].[ENVIO_EMAIL_DESPESA] ( @CAIXA_SAIDA NUMERIC(15,0)) AS                      
BEGIN                      
                
SET NOCOUNT ON      
--DECLARE @CAIXA_SAIDA NUMERIC = 9462


--financeiroadm@prohospital.com.br
         
DECLARE @TITULO_EMAIL                       VARCHAR(250)  
DECLARE @tableHTML                          NVARCHAR(MAX)    
DECLARE @REGISTRO_DESPESA                   VARCHAR(250)
DECLARE @DATA_HORA                          DATETIME
DECLARE @USUARIO                            VARCHAR(250)
DECLARE @ENTIDADE							VARCHAR(250)
DECLARE @OBJETO_CONTROLE					VARCHAR(250)
DECLARE @VALOR								VARCHAR(250)
DECLARE @CLASSIF_FIN                        VARCHAR(250)
DECLARE @EMAIL                              VARCHAR(250)
DECLARE @OBS                                VARCHAR(250)
      
 SELECT  @TITULO_EMAIL = ' Nova despesa de Caixa da Empresa ' + CAST (A.EMPRESA AS VARCHAR(50))
     
        ,@REGISTRO_DESPESA = CAST (A.DESPESA_COFRE  AS VARCHAR(50))     
        ,@DATA_HORA        = CAST (A.MOVIMENTO      AS VARCHAR(50))
		,@USUARIO          = D.NOME                   
		,@ENTIDADE		   = F.NOME			
		,@OBJETO_CONTROLE  = E.DESCRICAO  				
	    ,@CLASSIF_FIN	   = C.DESCRICAO
	    ,@VALOR            = CAST (A.DESPESA        AS VARCHAR(50))
		,@EMAIL            = 'pedro.mota@prohospital.com.br'
		,@OBS              = A.OBSERVACAO


    FROM DESPESAS_COFRES     A WITH(NOLOCK)
    JOIN EMPRESAS_USUARIAS   B WITH(NOLOCK) ON B.EMPRESA_USUARIA    = A.EMPRESA
	JOIN CLASSIF_FINANCEIRAS C WITH(NOLOCK) ON C.CLASSIF_FINANCEIRA = A.CLASSIF_FINANCEIRA
	JOIN USUARIOS            D WITH(NOLOCK) ON D.USUARIO            = A.USUARIO_LOGADO
	JOIN OBJETOS_CONTROLE    E WITH(NOLOCK) ON E.OBJETO_CONTROLE    = A.OBJETO_CONTROLE
	JOIN ENTIDADES           F WITH(NOLOCK) ON F.ENTIDADE           = A.ENTIDADE
	WHERE A.DESPESA_COFRE = @CAIXA_SAIDA
		


  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;
              }  
              </style>'                    +  
    N'<H1 class="formato" >Nova Despesa no caixa                                                        <br> <br>  
     Registro :            <br>'            + @REGISTRO_DESPESA                                   + '   <br> <br>  
     Data:                 <br>'            + CAST(@DATA_HORA AS VARCHAR)                         + '   <br> <br> 
     Usuario:              <br>'            + @USUARIO                                            + '   <br> <br>  
     Entidade:             <br>'            + @ENTIDADE		                                      + '   <br> <br>   
     Obejeto Controle:     <br>'     	    + @OBJETO_CONTROLE                                    + '   <br> <br>   
     Classif Financeira:   <br>'		    + @CLASSIF_FIN	                                      + '   <br> <br>
	 OBSERVACAO:           <br>'	        + @OBS                                                + '   <br> <br>
     Valor:                <br>'	        + @VALOR                                              + '   <br> <br>
 </H1>'

BEGIN
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML';  
END
  
END 