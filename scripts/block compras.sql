UPDATE RECEBIMENTOS_FISICOS_LANCAMENTOS 
SET CONTAGEM_ABERTA = 'S'
  FROM RECEBIMENTOS_FISICOS_LANCAMENTOS A WITH(NOLOCK)
  WHERE A.RECEBIMENTO = @P


--block de titulos receber para verificar tambem

DELETE TITULOS_RECEBER        
            FROM TITULOS_RECEBER A , #INTEGRACAO_RECEBER B        
           WHERE A.FORMULARIO_ORIGEM = B.FORMULARIO_ORIGEM And        
                 A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM And        
                 A.REG_MASTER_ORIGEM = ISNULL ( B.REG_MASTER_ORIGEM , B.REGISTRO_CONTROLE 


---verificar tambem
 DELETE FROM SOLICITACOES_FATURAMENTOS
WHERE
  SOLICITACAO_FATURAMENTO = @P


  --also verificar 
  DELETE FROM WMS_CHECKOUT
WHERE
  CHECKOUT = @P

  --also
  (@P1 float)DECLARE @FORMULARIO VARCHAR(50)
SET @FORMULARIO = 'WMS_CHECKOUT'
DECLARE @VERSAO VARCHAR(50)
SET @VERSAO = '8.0002'
DECLARE @LOGIN VARCHAR(50)
SET @LOGIN = 'joaojunior'
DECLARE @EMPRESA_LOCAL NUMERIC(15)
SET @EMPRESA_LOCAL = 0


EXEC GRAVACAO_FINAL_WMS_CHECKOUT_NOVA @P1
SELECT 1 AS STATUS

--also
(@P1 float)SELECT * FROM RECEBIMENTOS_VOLUMES_NF WITH(NOLOCK)
WHERE (RECEBIMENTO = '21867') AND RECEBIMENTO = @P1  

--also

DELETE TITULOS_RECEBER        
            FROM TITULOS_RECEBER A , #INTEGRACAO_RECEBER B        
           WHERE A.FORMULARIO_ORIGEM = B.FORMULARIO_ORIGEM And        
                 A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM And        
                 A.REG_MASTER_ORIGEM = 
				 ISNULL ( B.REG_MASTER_ORIGEM , B.REGISTRO_CONTROLE
				 
				 
				
--also
(@P1 float)SELECT * FROM RECEBIMENTOS_VOLUMES_NF WITH(NOLOCK)
WHERE (RECEBIMENTO = '21867') AND RECEBIMENTO = @P1  

--also
(@P1 float)SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
CREATE TABLE #INTEGRACAO_ESTOQUE (
              REGISTRO_CONTROLE    NUMERIC(15),
              REGISTRO_CONTROLE_II NUMERIC(15),
              FORMULARIO_ORIGEM    NUMERIC(15),
              TAB_MASTER_ORIGEM    NUMERIC(15),
              REG_MASTER_ORIGEM    NUMERIC(15),
              CENTRO_ESTOQUE       NUMERIC(15),
              PRODUTO              NUMERIC(15),
              DATA                 DATETIME,
              ESTOQUE_ENTRADA      NUMERIC(15,8) NOT NULL DEFAULT 0,
              ESTOQUE_SAIDA        NUMERIC(15,8) NOT NULL DEFAULT 0,
              CUSTO                NUMERIC(15,8) NOT NULL DEFAULT 0,
              ATUALIZAR_CUSTO      VARCHAR(01), 

-- also
(@P1 float)SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @FORMULARIO VARCHAR(50)
SET @FORMULARIO = 'APROVACOES_RECEB_VOLUMES'
DECLARE @VERSAO VARCHAR(50)
SET @VERSAO = '8.0002'
DECLARE @LOGIN VARCHAR(50)
SET @LOGIN = 'leontina'
DECLARE @EMPRESA_LOCAL NUMERIC(15)
SET @EMPRESA_LOCAL = 0
 DECLARE @RECEBIMENTO_APROVACAO NUMERIC 
     SET @RECEBIMENTO_APROVACAO   = @P1

/*GERA TEMP DE RECEBIMENTOS--*/
 if object_id('tempdb..#RECEBIMENTOS_TEMP') is not null


 ---
 @P1 float)SELECT * FROM NF_FATURAMENTO_SERVICOS WITH(NOLOCK)
WHERE (NF_FATURAMENTO = 48938) AND NF_FATURAMENTO = @P1 


---------------------------------------------
(@P1 float,@P2 float,@P3 float,@P4 float,@P5 float,@P6 float,@P7 float,@P8 float,@P9 float)SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
CREATE TABLE #INTEGRACAO_RECEBER (
              REGISTRO_CONTROLE    NUMERIC(15),
              REGISTRO_CONTROLE_II NUMERIC(15),
              FORMULARIO_ORIGEM    NUMERIC(15),
              TAB_MASTER_ORIGEM    NUMERIC(15),
              REG_MASTER_ORIGEM    NUMERIC(15),
              EMPRESA              NUMERIC(15),
              TITULO               VARCHAR(15),
              ENTIDADE             NUMERIC(15),
              EMISSAO              DATETIME,
              MOVIMENTO            DATETIME,
              VENCIMENTO           DATETIME,
              DESCONTO_ATE         DATETIME,
              DESCONTO_VALOR       NUMERIC(15,2),
              JUROS_ATRASO         NUMERIC(15,2),
              JUROS_DISPENSADO     VARCHAR(1),
              MULTA_ATRASO         NUMERIC(15,2),
              MULTA_DISPENSADA     VARCHAR(1),
              VALOR                NUMERIC(15,2),
              TARIFA_BANCARIA      NUMERIC(15,2),
              ANTECIPACAO          VARCHAR(1),
              RETENCAO_IR          NUMERIC(15,2),
              RETENCAO_DARF        NUMERIC(6),
              RETENCAO_ISS         NUMERIC(15,2),
              RETENCAO_INSS        NUMERIC(15,2),
              RETENCAO_GPS         NUMERIC(6),
              RETENCAO_PIS         NUMERIC(15,2),
              RETENCAO_COFINS      NUMERIC(15,2),
              RETENCAO_CSSL        NUMERIC(15,2),
              RETENCAO_COPICS      NUMERIC(15,2),
              ANTECIPACAO_RECEBER  NUMERIC(15),
              CCONTABIL            NUMERIC(15),
              VALOR_ABATIMENTO     NUMERIC(15,2),
              TIPO_COBRANCA        NUMERIC(5),
              MODALIDADE           NUMERIC(5),
              DOCUMENTO            NUMERIC(15),
              VENDEDOR             NUMERIC(15),
              CONTA_BANCARIA       NUMERIC(15),
              CHEQUE_BANCO         NUMERIC(03),
              CHEQUE_AGENCIA       NUMERIC(05),
              CHEQUE_CONTA         VARCHAR(20),
              CHEQUE_NUMERO        VARCHAR(15),
              CHEQUE_ENTIDADE      NUMERIC(15),
              CHEQUE_INSCRICAO_FEDERAL VARCHAR(20),
              CHEQUE_NOME          VARCHAR(60))
INSERT INTO #INTEGRACAO_RECEBER (
             REGISTRO_CONTROLE ,
             REGISTRO_CONTROLE_II ,
             FORMULARIO_ORIGEM ,
             TAB_MASTER_ORIGEM ,
             REG_MASTER_ORIGEM ,
             EMPRESA ,
             TITULO ,
             ENTIDADE ,
             EMISSAO ,
             MOVIMENTO ,
             VENCIMENTO ,
             DESCONTO_ATE ,
             DESCONTO_VALOR ,
             JUROS_ATRASO ,
             JUROS_DISPENSADO ,
             MULTA_ATRASO ,
             MULTA_DISPENSADA ,
             VALOR ,
             TARIFA_BANCARIA ,
             ANTECIPACAO ,
             RETENCAO_IR ,
             RETENCAO_DARF,
             RETENCAO_ISS ,
             RETENCAO_INSS ,
             RETENCAO_GPS,
             RETENCAO_PIS ,
             RETENCAO_COFINS,
             RETENCAO_CSSL,
             RETENCAO_COPICS,
             ANTECIPACAO_RECEBER,
             CCONTABIL ,
             VALOR_ABATIMENTO,
             TIPO_COBRANCA,
             MODALIDADE,
             DOCUMENTO,
             VENDEDOR,
             CONTA_BANCARIA,
             CHEQUE_BANCO,
             CHEQUE_AGENCIA,
             CHEQUE_CONTA,
             CHEQUE_NUMERO,
             CHEQUE_ENTIDADE,
             CHEQUE_INSCRICAO_FEDERAL,
             CHEQUE_NOME)
SELECT A.NF_FATURAMENTO_PARCELA               AS REGISTRO_CONTROLE , 
       1                                      AS REGISTRO_CONTROLE_II ,
       B.FORMULARIO_ORIGEM                    AS FORMULARIO_ORIGEM , 
       B.TAB_MASTER_ORIGEM                    AS TAB_MASTER_ORIGEM , 
       B.NF_FATURAMENTO                       AS REG_MASTER_ORIGEM , 
       B.EMPRESA                              AS EMPRESA , 
       A.TITULO                               AS TITULO , 
       B.ENTIDADE                             AS ENTIDADE , 
       B.EMISSAO                              AS EMISSAO , 
       B.MOVIMENTO                            AS MOVIMENTO , 
       A.VENCIMENTO                           AS VENCIMENTO ,
       A.DESCONTO_ATE                         AS DESCONTO_ATE , 
       A.DESCONTO_VALOR                       AS DESCONTO_VALOR , 
       A.JUROS_ATRASO                         AS JUROS_ATRASO , 
       'N'                                    AS JUROS_DISPENSADO , 
       A.MULTA_ATRASO                         AS MULTA_ATRASO , 
       'N'                                    AS MULTA_DISPENSADA , 
       A.VALOR                                AS VALOR ,
       0                                      AS TARIFA_BANCARIA,
       'N'                                    AS ANTECIPACAO , 
       0                                      AS RETENCAO_IR , 
       NULL                                   AS RETENCAO_DARF , 
       0                                      AS RETENCAO_ISS , 
       0                                      AS RETENCAO_INSS , 
       NULL                                   AS RETENCAO_GPS , 
       0                                      AS RETENCAO_PIS , 
       0                                      AS RETENCAO_COFINS,
       0                                      AS RETENCAO_CSSL ,
       0                                      AS RETENCAO_COPICS , 
       A.ANTECIPACAO_RECEBER                  AS ANTECIPACAO_RECEBER ,
       ISNULL(C.CCONTABIL, D.CCONTABIL)       AS CCONTABIL ,
       0                                      AS VALOR_ABATIMENTO ,
       1                                      AS TIPO_COBRANCA ,
       ISNULL(A.MODALIDADE,2)                 AS MODALIDADE ,
       NULL                                   AS DOCUMENTO ,       
	   NULL                                   AS VENDEDOR ,
       NULL                                   AS CONTA_BANCARIA ,
       NULL                                   AS CHEQUE_BANCO,  
       NULL                                   AS CHEQUE_AGENCIA,  
       NULL                                   AS CHEQUE_CONTA,  
       NULL                                   AS CHEQUE_NUMERO,  
       NULL                                   AS CHEQUE_ENTIDADE,  
       NULL                                   AS CHEQUE_INSCRICAO_FEDERAL,  
       NULL                                   AS CHEQUE_NOME

  FROM      NF_FATURAMENTO_PARCELAS A WITH(NOLOCK)
       JOIN NF_FATURAMENTO          B WITH(NOLOCK)ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
  LEFT JOIN ENTIDADES_CCONTABIL     C WITH(NOLOCK,INDEX(UQ_ENTIDADES_CCONTABIL_TIPOS)) ON C.ENTIDADE        = B.ENTIDADE
                                                                                      AND C.TIPO_CCONTABIL  = 1
  LEFT JOIN PARAMETROS_CCONTABIL    D WITH(NOLOCK)ON D.GRUPO_EMPRESARIAL = 1 AND D.TIPO_CCONTABIL  = 1
  LEFT JOIN PARAMETROS_VENDAS       E WITH(NOLOCK)ON E.EMPRESA_USUARIA = B.EMPRESA 
   LEFT JOIN OPERACOES_FISCAIS F WITH(NOLOCK)ON F.OPERACAO_FISCAL = B.OPERACAO_FISCAL
   LEFT JOIN NF_FATURAMENTO         Z WITH(NOLOCK)ON Z.NF_FATURAMENTO  = A.NF_FATURAMENTO
   WHERE A.NF_FATURAMENTO         = @P1
   AND ISNULL(B.EMITIR_NFE,'N') = 'S'
   AND (F.GERAR_FINANCEIRO = 'S')
INSERT INTO #INTEGRACAO_RECEBER (
             REGISTRO_CONTROLE ,
             REGISTRO_CONTROLE_II ,
             FORMULARIO_ORIGEM ,
             TAB_MASTER_ORIGEM ,
             REG_MASTER_ORIGEM ,
             EMPRESA ,
             TITULO ,
             ENTIDADE ,
             EMISSAO ,
             MOVIMENTO ,
             VENCIMENTO ,
             DESCONTO_ATE ,
             DESCONTO_VALOR ,
             JUROS_ATRASO ,
             JUROS_DISPENSADO ,
             MULTA_ATRASO ,
             MULTA_DISPENSADA ,
             VALOR ,
             TARIFA_BANCARIA ,
             ANTECIPACAO ,
             RETENCAO_IR ,
             RETENCAO_DARF,
             RETENCAO_ISS ,
             RETENCAO_INSS ,
             RETENCAO_GPS,
             RETENCAO_PIS ,
             RETENCAO_COFINS,
             RETENCAO_CSSL,
             RETENCAO_COPICS,
             ANTECIPACAO_RECEBER,
             CCONTABIL ,
             VALOR_ABATIMENTO,
             TIPO_COBRANCA,
             MODALIDADE,
             DOCUMENTO,
             VENDEDOR,
             CONTA_BANCARIA,
             CHEQUE_BANCO,
             CHEQUE_AGENCIA,
             CHEQUE_CONTA,
             CHEQUE_NUMERO,
             CHEQUE_ENTIDADE,
             CHEQUE_INSCRICAO_FEDERAL,
             CHEQUE_NOME)
  SELECT  A.NF_FATURAMENTO_BOLETO           AS REGISTRO_CONTROLE , 
          1                                 AS REGISTRO_CONTROLE_II ,
          A.FORMULARIO_ORIGEM               AS FORMULARIO_ORIGEM , 
          A.TAB_MASTER_ORIGEM               AS TAB_MASTER_ORIGEM , 
          A.NF_FATURAMENTO                  AS REG_MASTER_ORIGEM , 
          B.EMPRESA                         AS EMPRESA , 
          A.TITULO                          AS TITULO , 
          B.ENTIDADE                        AS ENTIDADE , 
          B.EMISSAO                         AS EMISSAO , 
          B.MOVIMENTO                       AS MOVIMENTO , 
          A.VENCIMENTO                      AS VENCIMENTO ,
          A.DESCONTO_ATE                    AS DESCONTO_ATE , 
          A.DESCONTO_VALOR                  AS DESCONTO_VALOR , 
          A.JUROS_ATRASO                    AS JUROS_ATRASO , 
          'N'                               AS JUROS_DISPENSADO , 
          A.MULTA_ATRASO                    AS MULTA_ATRASO , 
          'N'                               AS MULTA_DISPENSADA , 
          A.VALOR                           AS VALOR ,
          0                                 AS TARIFA_BANCARIA,
          'N'                               AS ANTECIPACAO , 
          0                                 AS RETENCAO_IR , 
          NULL                              AS RETENCAO_DARF , 
          0                                 AS RETENCAO_ISS , 
          0                                 AS RETENCAO_INSS , 
          NULL                              AS RETENCAO_GPS , 
          0                                 AS RETENCAO_PIS , 
          0                                 AS RETENCAO_COFINS,
          0                                 AS RETENCAO_CSSL ,
          0                                 AS RETENCAO_COPICS , 
          A.ANTECIPACAO_RECEBER             AS ANTECIPACAO_RECEBER,
          ISNULL(C.CCONTABIL, D.CCONTABIL)  AS CCONTABIL,
          0                                 AS VALOR_ABATIMENTO,
          CASE WHEN ISNULL(A.CONTA_BANCARIA,0) = 0
               THEN 0 /*CARTEIRA*/
               ELSE 1 /*COBRAN�A SIMPLES*/
          END                               AS TIPO_COBRANCA,
	  1                                 AS MODALIDADE, /*BOLETO*/
	  NULL                              AS DOCUMENTO,
	  NULL                              AS VENDEDOR ,
          A.CONTA_BANCARIA                  AS CONTA_BANCARIA,
          NULL                              AS CHEQUE_BANCO,  
          NULL                              AS CHEQUE_AGENCIA,  
          NULL   			    AS CHEQUE_CONTA,  
          NULL                              AS CHEQUE_NUMERO,  
          NULL                              AS CHEQUE_ENTIDADE,  
          NULL                              AS CHEQUE_INSCRICAO_FEDERAL,  
          NULL                              AS CHEQUE_NOME  

     FROM NF_FATURAMENTO_BOLETOS     A WITH (NOLOCK)
LEFT JOIN NF_FATURAMENTO             B WITH (NOLOCK)ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
     JOIN ENTIDADES                  G WITH (NOLOCK)ON G.ENTIDADE        = B.ENTIDADE
LEFT JOIN GRUPOS_CONTABEIS_CCONTABIL C WITH (NOLOCK)ON C.GRUPO_CONTABIL  = G.GRUPO_CONTABIL AND  
                                                       C.TIPO_CCONTABIL  = 1
LEFT JOIN PARAMETROS_CCONTABIL       D WITH (NOLOCK)ON D.TIPO_CCONTABIL  = 1
LEFT JOIN PARAMETROS_VENDAS          E WITH (NOLOCK)ON E.EMPRESA_USUARIA = B.EMPRESA

    WHERE A.NF_FATURAMENTO = @P2 
      AND A.ANTECIPACAO_RECEBER IS NULL
      /*AND B.TELEVENDA IS NULL*/
      AND ISNULL(B.EMITIR_NFE,'N') = 'S'

INSERT INTO #INTEGRACAO_RECEBER (
             REGISTRO_CONTROLE ,
             REGISTRO_CONTROLE_II ,
             FORMULARIO_ORIGEM ,
             TAB_MASTER_ORIGEM ,
             REG_MASTER_ORIGEM ,
             EMPRESA ,
             TITULO ,
             ENTIDADE ,
             EMISSAO ,
             MOVIMENTO ,
             VENCIMENTO ,
             DESCONTO_ATE ,
             DESCONTO_VALOR ,
             JUROS_ATRASO ,
             JUROS_DISPENSADO ,
             MULTA_ATRASO ,
             MULTA_DISPENSADA ,
             VALOR ,
             TARIFA_BANCARIA ,
             ANTECIPACAO ,
             RETENCAO_IR ,
             RETENCAO_DARF,
             RETENCAO_ISS ,
             RETENCAO_INSS ,
             RETENCAO_GPS,
             RETENCAO_PIS ,
             RETENCAO_COFINS,
             RETENCAO_CSSL,
             RETENCAO_COPICS,
             ANTECIPACAO_RECEBER,
             CCONTABIL ,
             VALOR_ABATIMENTO,
             TIPO_COBRANCA,
             MODALIDADE,
             DOCUMENTO,
             VENDEDOR,
             CONTA_BANCARIA,
             CHEQUE_BANCO,
             CHEQUE_AGENCIA,
             CHEQUE_CONTA,
             CHEQUE_NUMERO,
             CHEQUE_ENTIDADE,
             CHEQUE_INSCRICAO_FEDERAL,
             CHEQUE_NOME)
SELECT A.NF_FATURAMENTO_DEPOSITO              AS REGISTRO_CONTROLE , 
       2                                      AS REGISTRO_CONTROLE_II ,
       B.FORMULARIO_ORIGEM                    AS FORMULARIO_ORIGEM , 
       B.TAB_MASTER_ORIGEM                    AS TAB_MASTER_ORIGEM , 
       B.NF_FATURAMENTO                       AS REG_MASTER_ORIGEM , 
       B.EMPRESA                              AS EMPRESA , 
       A.TITULO                               AS TITULO , 
       B.ENTIDADE                             AS ENTIDADE , 
       B.EMISSAO                              AS EMISSAO , 
       B.MOVIMENTO                            AS MOVIMENTO , 
       A.VENCIMENTO                           AS VENCIMENTO ,
       A.DESCONTO_ATE                         AS DESCONTO_ATE , 
       A.DESCONTO_VALOR                       AS DESCONTO_VALOR , 
       A.JUROS_ATRASO                         AS JUROS_ATRASO , 
       'N'                                    AS JUROS_DISPENSADO , 
       A.MULTA_ATRASO                         AS MULTA_ATRASO , 
       'N'                                    AS MULTA_DISPENSADA , 
       A.VALOR                                AS VALOR ,
       0                                      AS TARIFA_BANCARIA,
       'N'                                    AS ANTECIPACAO , 
       0                                      AS RETENCAO_IR , 
       NULL                                   AS RETENCAO_DARF , 
       0                                      AS RETENCAO_ISS , 
       0                                      AS RETENCAO_INSS , 
       NULL                                   AS RETENCAO_GPS , 
       0                                      AS RETENCAO_PIS , 
       0                                      AS RETENCAO_COFINS,
       0                                      AS RETENCAO_CSSL ,
       0                                      AS RETENCAO_COPICS , 
       A.ANTECIPACAO_RECEBER                  AS ANTECIPACAO_RECEBER ,
       ISNULL(C.CCONTABIL, D.CCONTABIL)       AS CCONTABIL ,
       0                                      AS VALOR_ABATIMENTO ,
       1                                      AS TIPO_COBRANCA ,
       ISNULL(A.MODALIDADE,2)                 AS MODALIDADE ,
       NULL                                   AS DOCUMENTO ,       
	   NULL                                   AS VENDEDOR ,
       NULL                                   AS CONTA_BANCARIA ,
       NULL                                   AS CHEQUE_BANCO,  
       NULL                                   AS CHEQUE_AGENCIA,  
       NULL                                   AS CHEQUE_CONTA,  
       NULL                                   AS CHEQUE_NUMERO,  
       NULL                                   AS CHEQUE_ENTIDADE,  
       NULL                                   AS CHEQUE_INSCRICAO_FEDERAL,  
       NULL                                   AS CHEQUE_NOME

  FROM      NF_FATURAMENTO_DEPOSITOS A WITH(NOLOCK)
       JOIN NF_FATURAMENTO           B WITH(NOLOCK)ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
  LEFT JOIN ENTIDADES_CCONTABIL      C WITH(NOLOCK)ON C.ENTIDADE        = B.ENTIDADE
                                                 AND C.TIPO_CCONTABIL  = 1
  LEFT JOIN PARAMETROS_CCONTABIL     D WITH(NOLOCK)ON D.TIPO_CCONTABIL  = 1
  LEFT JOIN PARAMETROS_VENDAS        E WITH(NOLOCK)ON E.EMPRESA_USUARIA = B.EMPRESA


   
   LEFT JOIN OPERACOES_FISCAIS F WITH(NOLOCK)ON F.OPERACAO_FISCAL = B.OPERACAO_FISCAL
   WHERE A.NF_FATURAMENTO         = @P3
   AND ISNULL(B.EMITIR_NFE,'N') = 'S'
   AND (F.GERAR_FINANCEIRO = 'S')
INSERT INTO #INTEGRACAO_RECEBER (
             REGISTRO_CONTROLE ,
             REGISTRO_CONTROLE_II ,
             FORMULARIO_ORIGEM ,
             TAB_MASTER_ORIGEM ,
             REG_MASTER_ORIGEM ,
             EMPRESA ,
             TITULO ,
             ENTIDADE ,
             EMISSAO ,
             MOVIMENTO ,
             VENCIMENTO ,
             DESCONTO_ATE ,
             DESCONTO_VALOR ,
             JUROS_ATRASO ,
             JUROS_DISPENSADO ,
             MULTA_ATRASO ,
             MULTA_DISPENSADA ,
             VALOR ,
             TARIFA_BANCARIA ,
             ANTECIPACAO ,
             RETENCAO_IR ,
             RETENCAO_DARF,
             RETENCAO_ISS ,
             RETENCAO_INSS ,
             RETENCAO_GPS,
             RETENCAO_PIS ,
             RETENCAO_COFINS,
             RETENCAO_CSSL,
             RETENCAO_COPICS,
             ANTECIPACAO_RECEBER,
             CCONTABIL ,
             VALOR_ABATIMENTO,
             TIPO_COBRANCA,
             MODALIDADE,
             DOCUMENTO,
             VENDEDOR,
             CONTA_BANCARIA,
             CHEQUE_BANCO,
             CHEQUE_AGENCIA,
             CHEQUE_CONTA,
             CHEQUE_NUMERO,
             CHEQUE_ENTIDADE,
             CHEQUE_INSCRICAO_FEDERAL,
             CHEQUE_NOME)
  SELECT  A.NF_FATURAMENTO_CHEQUE           AS REGISTRO_CONTROLE , 
          3                                 AS REGISTRO_CONTROLE_II ,
          A.FORMULARIO_ORIGEM               AS FORMULARIO_ORIGEM , 
          A.TAB_MASTER_ORIGEM               AS TAB_MASTER_ORIGEM , 
          A.NF_FATURAMENTO                  AS REG_MASTER_ORIGEM , 
          B.EMPRESA                         AS EMPRESA , 
          A.CHEQUE_NUMERO                   AS TITULO , 
          B.ENTIDADE                        AS ENTIDADE , 
          B.EMISSAO                         AS EMISSAO , 
          B.MOVIMENTO                       AS MOVIMENTO , 
          A.VENCIMENTO                      AS VENCIMENTO ,
          NULL                              AS DESCONTO_ATE , 
          0                                 AS DESCONTO_VALOR , 
          0                                 AS JUROS_ATRASO , 
          'N'                               AS JUROS_DISPENSADO , 
          0                                 AS MULTA_ATRASO , 
          'N'                               AS MULTA_DISPENSADA , 
          A.VALOR                           AS VALOR ,
          0                                 AS TARIFA_BANCARIA,
          'N'                               AS ANTECIPACAO , 
          0                                 AS RETENCAO_IR , 
          NULL                              AS RETENCAO_DARF , 
          0                                 AS RETENCAO_ISS , 
          0                                 AS RETENCAO_INSS , 
          NULL                              AS RETENCAO_GPS , 
          0                                 AS RETENCAO_PIS , 
          0                                 AS RETENCAO_COFINS,
          0                                 AS RETENCAO_CSSL ,
          0                                 AS RETENCAO_COPICS , 
          NULL                              AS ANTECIPACAO_RECEBER,
          ISNULL(C.CCONTABIL, D.CCONTABIL)  AS CCONTABIL,
          0                                 AS VALOR_ABATIMENTO,
          0                                 AS TIPO_COBRANCA, /*CARTEIRA*/
	  3                                 AS MODALIDADE, /*CHEQUES*/
          NULL                              AS DOCUMENTO,
	  NULL                              AS VENDEDOR ,
          NULL                              AS CONTA_BANCARIA ,
          A.BANCO                           AS CHEQUE_BANCO,  
          A.AGENCIA                         AS CHEQUE_AGENCIA,  
          A.CONTA                           AS CHEQUE_CONTA,  
          A.CHEQUE_NUMERO                   AS CHEQUE_NUMERO,  
          A.CHEQUE_ENTIDADE                 AS CHEQUE_ENTIDADE,  
          A.CHEQUE_INSCRICAO_FEDERAL        AS CHEQUE_INSCRICAO_FEDERAL,  
          A.CHEQUE_NOME                     AS CHEQUE_NOME  

     FROM NF_FATURAMENTO_CHEQUES     A WITH (NOLOCK)
LEFT JOIN NF_FATURAMENTO             B WITH (NOLOCK)ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
     JOIN ENTIDADES                  G WITH (NOLOCK)ON G.ENTIDADE        = B.ENTIDADE
LEFT JOIN GRUPOS_CONTABEIS_CCONTABIL C WITH (NOLOCK)ON C.GRUPO_CONTABIL  = G.GRUPO_CONTABIL AND  
                                                       C.TIPO_CCONTABIL  = 1
LEFT JOIN PARAMETROS_CCONTABIL       D WITH (NOLOCK)ON D.TIPO_CCONTABIL  = 1
LEFT JOIN PARAMETROS_VENDAS          E WITH (NOLOCK)ON E.EMPRESA_USUARIA = B.EMPRESA

    WHERE A.NF_FATURAMENTO = @P4 
      AND B.TELEVENDA IS NULL
      AND ISNULL(B.EMITIR_NFE,'N') = 'S'


CREATE TABLE #INTEGRACAO_RECEBER_CLASSIFICACOES (
              REGISTRO_CONTROLE    NUMERIC(15),
              REGISTRO_CONTROLE_II NUMERIC(15),
              FORMULARIO_ORIGEM    NUMERIC(15),
              TAB_MASTER_ORIGEM    NUMERIC(15),
              REG_MASTER_ORIGEM    NUMERIC(15),
              CLASSIF_FINANCEIRA   NUMERIC(15),
              OBJETO_CONTROLE      NUMERIC(15),
              VALOR                NUMERIC(15,2) )
INSERT INTO #INTEGRACAO_RECEBER_CLASSIFICACOES (
             REGISTRO_CONTROLE , 
             REGISTRO_CONTROLE_II , 
             FORMULARIO_ORIGEM , 
             TAB_MASTER_ORIGEM , 
             REG_MASTER_ORIGEM , 
             CLASSIF_FINANCEIRA , 
             OBJETO_CONTROLE , 
             VALOR )
  SELECT A.NF_FATURAMENTO_PARCELA AS REGISTRO_CONTROLE , 
         1                        AS REGISTRO_CONTROLE_II ,
         B.FORMULARIO_ORIGEM      AS FORMULARIO_ORIGEM , 
         B.TAB_MASTER_ORIGEM      AS TAB_MASTER_ORIGEM , 
         B.NF_FATURAMENTO         AS REG_MASTER_ORIGEM ,
         A.CLASSIF_FINANCEIRA     AS CLASSIF_FINANCEIRA ,
         C.OBJETO_CONTROLE        AS OBJETO_CONTROLE , 
         SUM ( A.VALOR )          AS VALOR

    FROM NF_FATURAMENTO_PARCELAS A WITH(NOLOCK),
         NF_FATURAMENTO          B WITH(NOLOCK),
         CLASSIF_FINANCEIRAS     C WITH(NOLOCK)

   WHERE A.NF_FATURAMENTO         = @P5
     AND A.NF_FATURAMENTO         = B.NF_FATURAMENTO
     AND C.CLASSIF_FINANCEIRA     = A.CLASSIF_FINANCEIRA
     AND ISNULL(B.EMITIR_NFE,'N') = 'S'

GROUP BY A.NF_FATURAMENTO_PARCELA ,  
         A.CLASSIF_FINANCEIRA , 
         B.FORMULARIO_ORIGEM , 
         B.TAB_MASTER_ORIGEM , 
         B.NF_FATURAMENTO , 
         A.CLASSIF_FINANCEIRA ,
         C.OBJETO_CONTROLE
INSERT INTO #INTEGRACAO_RECEBER_CLASSIFICACOES (
             REGISTRO_CONTROLE , 
             REGISTRO_CONTROLE_II , 
             FORMULARIO_ORIGEM , 
             TAB_MASTER_ORIGEM , 
             REG_MASTER_ORIGEM , 
             CLASSIF_FINANCEIRA , 
             OBJETO_CONTROLE , 
             VALOR )
  SELECT A.NF_FATURAMENTO_BOLETO  AS REGISTRO_CONTROLE , 
         1                        AS REGISTRO_CONTROLE_II ,
         A.FORMULARIO_ORIGEM      AS FORMULARIO_ORIGEM , 
         A.TAB_MASTER_ORIGEM      AS TAB_MASTER_ORIGEM , 
         A.NF_FATURAMENTO         AS REG_MASTER_ORIGEM ,
         A.CLASSIF_FINANCEIRA     AS CLASSIF_FINANCEIRA , 
         C.OBJETO_CONTROLE        AS OBJETO_CONTROLE    ,
         CONVERT(NUMERIC(15,2), 
	    SUM(A.VALOR)*C.FATOR) AS VALOR

    FROM NF_FATURAMENTO_BOLETOS  A WITH(NOLOCK)
    JOIN NF_FATURAMENTO          B WITH(NOLOCK)ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
 CROSS APPLY DBO.[OBJETOS_CONTROLES_ORIGENS]('NF_FATURAMENTO',A.NF_FATURAMENTO) C
   WHERE A.NF_FATURAMENTO = @P6
     AND B.TELEVENDA IS NULL

GROUP BY A.NF_FATURAMENTO_BOLETO ,  
         A.CLASSIF_FINANCEIRA , 
         A.FORMULARIO_ORIGEM , 
         A.TAB_MASTER_ORIGEM , 
         A.NF_FATURAMENTO    ,
         C.OBJETO_CONTROLE   ,
	 C.FATOR

INSERT INTO #INTEGRACAO_RECEBER_CLASSIFICACOES (
             REGISTRO_CONTROLE , 
             REGISTRO_CONTROLE_II , 
             FORMULARIO_ORIGEM , 
             TAB_MASTER_ORIGEM , 
             REG_MASTER_ORIGEM , 
             CLASSIF_FINANCEIRA , 
             OBJETO_CONTROLE , 
             VALOR )
  SELECT A.NF_FATURAMENTO_DEPOSITO AS REGISTRO_CONTROLE , 
         2                         AS REGISTRO_CONTROLE_II ,
         B.FORMULARIO_ORIGEM       AS FORMULARIO_ORIGEM , 
         B.TAB_MASTER_ORIGEM       AS TAB_MASTER_ORIGEM , 
         B.NF_FATURAMENTO          AS REG_MASTER_ORIGEM ,
         A.CLASSIF_FINANCEIRA      AS CLASSIF_FINANCEIRA ,
         C.OBJETO_CONTROLE         AS OBJETO_CONTROLE , 
         SUM ( A.VALOR )           AS VALOR

    FROM NF_FATURAMENTO_DEPOSITOS A WITH(NOLOCK),
         NF_FATURAMENTO           B WITH(NOLOCK),
         CLASSIF_FINANCEIRAS      C WITH(NOLOCK)

   WHERE A.NF_FATURAMENTO         = @P7
     AND A.NF_FATURAMENTO         = B.NF_FATURAMENTO
     AND C.CLASSIF_FINANCEIRA     = A.CLASSIF_FINANCEIRA
     AND ISNULL(B.EMITIR_NFE,'N') = 'S'

GROUP BY A.NF_FATURAMENTO_DEPOSITO ,  
         A.CLASSIF_FINANCEIRA , 
         B.FORMULARIO_ORIGEM , 
         B.TAB_MASTER_ORIGEM , 
         B.NF_FATURAMENTO , 
         A.CLASSIF_FINANCEIRA ,
         C.OBJETO_CONTROLE
INSERT INTO #INTEGRACAO_RECEBER_CLASSIFICACOES (
             REGISTRO_CONTROLE , 
             REGISTRO_CONTROLE_II , 
             FORMULARIO_ORIGEM , 
             TAB_MASTER_ORIGEM , 
             REG_MASTER_ORIGEM , 
             CLASSIF_FINANCEIRA , 
             OBJETO_CONTROLE , 
             VALOR )
 SELECT A.NF_FATURAMENTO_CHEQUE   AS REGISTRO_CONTROLE , 
         3                         AS REGISTRO_CONTROLE_II ,
         A.FORMULARIO_ORIGEM       AS FORMULARIO_ORIGEM , 
         A.TAB_MASTER_ORIGEM       AS TAB_MASTER_ORIGEM , 
         A.NF_FATURAMENTO          AS REG_MASTER_ORIGEM ,
         A.CLASSIF_FINANCEIRA      AS CLASSIF_FINANCEIRA , 
         C.OBJETO_CONTROLE         AS OBJETO_CONTROLE    ,
         CONVERT(NUMERIC(15,2), 
         SUM(A.VALOR)*C.FATOR) AS VALOR

    FROM NF_FATURAMENTO_CHEQUES   A WITH(NOLOCK)
    JOIN NF_FATURAMENTO           B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
 CROSS APPLY DBO.[OBJETOS_CONTROLES_ORIGENS]('NF_FATURAMENTO',A.NF_FATURAMENTO) C
   WHERE A.NF_FATURAMENTO = @P8
     AND B.TELEVENDA IS NULL

GROUP BY A.NF_FATURAMENTO_CHEQUE ,  
         A.CLASSIF_FINANCEIRA , 
         A.FORMULARIO_ORIGEM , 
         A.TAB_MASTER_ORIGEM , 
         A.NF_FATURAMENTO    ,
         C.OBJETO_CONTROLE   ,
		 C.FATOR
          

CREATE TABLE #INTEGRACAO_ESTOQUE (
              REGISTRO_CONTROLE    NUMERIC(15),
              REGISTRO_CONTROLE_II NUMERIC(15),
              FORMULARIO_ORIGEM    NUMERIC(15),
              TAB_MASTER_ORIGEM    NUMERIC(15),
              REG_MASTER_ORIGEM    NUMERIC(15),
              CENTRO_ESTOQUE       NUMERIC(15),
              PRODUTO              NUMERIC(15),
              DATA                 DATETIME,
              ESTOQUE_ENTRADA      NUMERIC(15,8) NOT NULL DEFAULT 0,
              ESTOQUE_SAIDA        NUMERIC(15,8) NOT NULL DEFAULT 0,
              CUSTO                NUMERIC(15,8) NOT NULL DEFAULT 0,
              ATUALIZAR_CUSTO      VARCHAR(01),
              DOCUMENTO            NUMERIC(15),
              TIPO_MOVIMENTO       NUMERIC(15),
              ENTIDADE             NUMERIC(15),
              LOTE_VALIDADE        NUMERIC(15) )
INSERT INTO #INTEGRACAO_ESTOQUE (
             REGISTRO_CONTROLE ,
             REGISTRO_CONTROLE_II ,
             FORMULARIO_ORIGEM ,
             TAB_MASTER_ORIGEM ,
             REG_MASTER_ORIGEM ,
             CENTRO_ESTOQUE ,
             PRODUTO ,
             DATA ,
             ESTOQUE_ENTRADA ,
             ESTOQUE_SAIDA ,
             CUSTO ,
             ATUALIZAR_CUSTO ,
             DOCUMENTO ,
             TIPO_MOVIMENTO ,
             ENTIDADE ,
             LOTE_VALIDADE )
    SELECT B.NF_FATURAMENTO_PRODUTO AS REGISTRO_CONTROLE,
           0                        AS REGISTRO_CONTROLE_II,
           B.FORMULARIO_ORIGEM      AS FORMULARIO_ORIGEM, 
           B.TAB_MASTER_ORIGEM      AS TAB_MASTER_ORIGEM,
           B.NF_FATURAMENTO         AS REG_MASTER_ORIGEM,
           B.OBJETO_CONTROLE        AS CENTRO_ESTOQUE,
           B.PRODUTO                AS PRODUTO, 
           A.MOVIMENTO              AS DATA,
           0                        AS ESTOQUE_ENTRADA,
           B.QUANTIDADE_ESTOQUE     AS ESTOQUE_SAIDA,
           ISNULL(F.CUSTO_CONTABIL, B.VALOR_UNITARIO )
		                             AS CUSTO		 ,  
           E.ATUALIZAR_CUSTO_ESTOQUE AS ATUALIZAR_CUSTO,		     	      	       
           A.NF_NUMERO               AS DOCUMENTO,
            CASE WHEN E.TIPO_OPERACAO = 18 /* PERDA/ROUBO/VENCIMENTO*/
                THEN 17
                ELSE
                    CASE WHEN C.EMPRESA_CONTABIL = ISNULL(D.EMPRESA_CONTABIL,0)  
                         THEN 3  /* TRANSFERENCIA*/
                         ELSE 21 /* VENDA*/
                    END
           END                       AS TIPO_MOVIMENTO,		   
           A.ENTIDADE                AS ENTIDADE ,
           J.LOTE_VALIDADE           AS LOTE_VALIDADE

      FROM NF_FATURAMENTO                  A WITH(NOLOCK)
      JOIN NF_FATURAMENTO_PRODUTOS         B WITH(NOLOCK) ON B.NF_FATURAMENTO     = A.NF_FATURAMENTO

      JOIN EMPRESAS_USUARIAS               C WITH(NOLOCK) ON C.EMPRESA_USUARIA    = A.EMPRESA  /* EMPRESA SAIDA*/
      LEFT JOIN EMPRESAS_USUARIAS          D WITH(NOLOCK) ON D.ENTIDADE           = A.ENTIDADE /* EMPRESA ENTRADA*/

      JOIN OPERACOES_FISCAIS               E WITH(NOLOCK) ON E.OPERACAO_FISCAL    = B.OPERACAO_FISCAL
      LEFT JOIN CUSTO_MEDIO_MENSAL_EMPRESA F WITH(NOLOCK) ON F.PRODUTO            = B.PRODUTO
                                                         AND F.EMPRESA            = C.EMPRESA_CONTABIL
                                                         AND F.MES                = MONTH(A.MOVIMENTO)
                                                         AND F.ANO                = YEAR (A.MOVIMENTO)
     LEFT JOIN PRODUTOS_LOTE_VALIDADE      J WITH(NOLOCK) ON J.PRODUTO            = B.PRODUTO
                                                         AND J.LOTE_VALIDADE      = B.LOTE_VALIDADE
                                                         
     WHERE A.NF_FATURAMENTO = @P9
       AND E.GERAR_ESTOQUE  = 'S'
EXEC INTEGRACAO 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNSSNNNSNNNNNNNNNNNNNNNNNNNNNNNN'
/* REGISTRO NF_FATURAMENTO=48945*/
/* ACIONAR  NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNSSNNNSNNNNNNNNNNNNNNNNNNNNNNNN*/