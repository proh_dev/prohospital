--SP_HELP VW_SOLICITACAO_APROVACAO_NF

CREATE VIEW VW_SOLICITACAO_APROVACAO_NF
AS
SELECT B.APROVACAO_CANCELAMENTO                        AS APROVACAO        ,
	   A.SOLICITACAO_CANCELAMENTO_NF                   AS SOLICITACAO      ,
	   A.USUARIO_LOGADO                                AS SOLICITANTE      ,
	   C.USUARIO_LOGADO                                AS APROVADOR        ,
	   A.NF_NUMERO                                     AS NF_NUMERO        ,
	   A.NF_SERIE                                      AS NF_SERIE         ,
	   A.CLIENTE                                       AS CLIENTE          ,
	   A.EMPRESA                                       AS EMPRESA          ,
	   A.JUSTIFICATIVA                                 AS JUSTIFICATIVA


FROM SOLICITACOES_CANCELAMENTOS_NF                 A WITH(NOLOCK)
JOIN APROVACOES_CANCELAMENTOS_NF_ITENS             B WITH(NOLOCK) ON A.SOLICITACAO_CANCELAMENTO_NF = B.SOLICITACAO_CANCELAMENTO_NF
JOIN APROVACOES_CANCELAMENTOS_NF                   C WITH(NOLOCK) ON B.APROVACAO_CANCELAMENTO      = C.APROVACAO_CANCELAMENTO