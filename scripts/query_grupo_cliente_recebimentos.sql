-- BACKUP 29/09/2014
                              

DECLARE @EMPRESA_INI NUMERIC(13)  
DECLARE @EMPRESA_FIM NUMERIC(13)     
DECLARE @EMPRESA_CONTABIL NUMERIC(15)
DECLARE @DATA_INICIAL     DATETIME
DECLARE @DATA_FINAL       DATETIME
DECLARE @GRUPO_CLIENTE    NUMERIC (13)

--SET @DATA_INICIAL      ='01/06/2018'
--SET @DATA_FINAL        ='07/06/2018'
--SET @EMPRESA_INI       =1    
--SET @EMPRESA_FIM       =99999
--SET @EMPRESA_CONTABIL  =NULL
--SET @GRUPO_CLIENTE     = NULL

SET @DATA_INICIAL      =:DATA_INICIAL --'01/01/2015'--
SET @DATA_FINAL        =:DATA_FINAL   --'30/09/2015'--
SET @EMPRESA_INI       = CASE WHEN ISNUMERIC(:EMPRESA_INI)      = 1 THEN :EMPRESA_INI      ELSE 1          END
SET @EMPRESA_FIM       = CASE WHEN ISNUMERIC(:EMPRESA_FIM)      = 1 THEN :EMPRESA_FIM      ELSE 9999999999 END
SET @EMPRESA_CONTABIL  = CASE WHEN ISNUMERIC(:EMPRESA_CONTABIL) = 1 THEN :EMPRESA_CONTABIL ELSE NULL       END
SET @GRUPO_CLIENTE     = CASE WHEN ISNUMERIC(:GRUPO_CLIENTE)    = 1 THEN :GRUPO_CLIENTE    ELSE NULL       END


SELECT B.TITULO_RECEBER                     AS TITULO_RECEBER , 
       C.EMPRESA                            AS EMPRESA ,
       E.EMPRESA_CONTABIL                   AS REDE , 
       E.EMPRESA_CONTABIL_NOME_FANTASIA     AS NOME_REDE , 
       E.EMPRESA_USUARIA_FILIAL             AS FILIAL ,   
       D.ENTIDADE                           AS ENTIDADE , 
       D.NOME                               AS NOME , 
       C.TITULO                             AS TITULO , 
       C.VENCIMENTO                         AS VENCIMENTO ,
       A.DATA_RECEBIMENTO                   AS DATA_RECEBIMENTO ,
       C.VALOR                              AS VALOR , 
       B.RECEBIMENTO_TOTAL                  AS RECEBIMENTO_TOTAL ,
       'CAIXA'                              AS CONTA,
       E.EMPRESA_CONTABIL                   AS EMPRESA_CONTABIL,                         
       I.DESCRICAO                          AS MODALIDADE,
       ISNULL(V.VENDEDOR,103)               AS VENDEDOR,
       ISNULL(V.NOME,'SEM VENDEDOR')        AS VENDEDOR_NOME,
	   J.DESCRICAO                          AS GRUPO_CLIENTE

       
  FROM RECEBIMENTOS_CAIXA            A WITH(NOLOCK)
  JOIN RECEBIMENTOS_CAIXA_TITULOS    B WITH(NOLOCK) ON B.RECEBIMENTO_CAIXA     = A.RECEBIMENTO_CAIXA
  JOIN TITULOS_RECEBER               C WITH(NOLOCK) ON C.TITULO_RECEBER        = B.TITULO_RECEBER
  JOIN ENTIDADES                     D WITH(NOLOCK) ON D.ENTIDADE              = C.ENTIDADE
  JOIN FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL)       
                                     E              ON E.EMPRESA_USUARIA       = C.EMPRESA
LEFT JOIN CANCELAMENTOS_RECEBIMENTOS G WITH(NOLOCK) ON A.RECEBIMENTO_CAIXA     = G.RECEBIMENTO_CAIXA 
LEFT JOIN MODALIDADES_TITULOS        I WITH(NOLOCK) ON I.MODALIDADE            = C.MODALIDADE  
LEFT JOIN VENDEDORES                 V WITH(NOLOCK) ON V.VENDEDOR              = C.VENDEDOR  
LEFT JOIN CLASSIFICACOES_CLIENTES    J WITH(NOLOCK) ON J.CLASSIFICACAO_CLIENTE = D.CLASSIFICACAO_CLIENTE


 WHERE 1=1
   AND  A.DATA_RECEBIMENTO >= @DATA_INICIAL 
   AND  A.DATA_RECEBIMENTO <= @DATA_FINAL   
   AND  G.RECEBIMENTO_CAIXA IS NULL
   AND (D.CLASSIFICACAO_CLIENTE = @GRUPO_CLIENTE OR @GRUPO_CLIENTE IS NULL)
       
 UNION ALL
 
SELECT B.TITULO_RECEBER                     AS TITULO_RECEBER , 
       C.EMPRESA                            AS EMPRESA ,
       F.EMPRESA_CONTABIL                   AS REDE , 
       F.EMPRESA_CONTABIL_NOME_FANTASIA     AS NOME_REDE , 
       F.EMPRESA_USUARIA_FILIAL             AS FILIAL , 
       D.ENTIDADE                           AS ENTIDADE , 
       D.NOME                               AS NOME , 
       C.TITULO                             AS TITULO , 
       C.VENCIMENTO                         AS VENCIMENTO , 
       A.DATA_RECEBIMENTO                   AS DATA_RECEBIMENTO ,
       C.VALOR                              AS VALOR , 
       B.RECEBIMENTO_TOTAL                  AS RECEBIMENTO_TOTAL ,
       E.DESCRICAO                          AS CONTA,
       F.EMPRESA_CONTABIL                   AS EMPRESA_CONTABIL,
       J.DESCRICAO                          AS MODALIDADE,
       ISNULL(V.VENDEDOR,103)               AS VENDEDOR,
       ISNULL(V.NOME,'SEM VENDEDOR')        AS VENDEDOR_NOME,
	   K.DESCRICAO                          AS GRUPO_CLIENTE

                                  
  FROM RECEBIMENTOS_BANCOS           A WITH(NOLOCK)
  JOIN RECEBIMENTOS_BANCOS_TITULOS   B WITH(NOLOCK) ON B.RECEBIMENTO_BANCO  = A.RECEBIMENTO_BANCO
  JOIN TITULOS_RECEBER               C WITH(NOLOCK) ON C.TITULO_RECEBER     = B.TITULO_RECEBER
  JOIN ENTIDADES                     D WITH(NOLOCK) ON D.ENTIDADE           = C.ENTIDADE
  JOIN CONTAS_BANCARIAS              E WITH(NOLOCK) ON E.CONTA_BANCARIA     = A.CONTA_BANCARIA
  JOIN FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL)
                                     F              ON F.EMPRESA_USUARIA   = C.EMPRESA
LEFT JOIN CANCELAMENTOS_RECEBIMENTOS H WITH(NOLOCK) ON A.RECEBIMENTO_BANCO = H.RECEBIMENTO_BANCO
  JOIN EMPRESAS_USUARIAS             I WITH(NOLOCK) ON A.EMPRESA           = I.ENTIDADE    
LEFT JOIN MODALIDADES_TITULOS        J WITH(NOLOCK) ON J.MODALIDADE        = C.MODALIDADE     
LEFT JOIN VENDEDORES                 V WITH(NOLOCK) ON V.VENDEDOR          = C.VENDEDOR
LEFT JOIN CLASSIFICACOES_CLIENTES    K WITH(NOLOCK) ON K.CLASSIFICACAO_CLIENTE = D.CLASSIFICACAO_CLIENTE
  
 WHERE 1=1
   AND A.DATA_RECEBIMENTO >= @DATA_INICIAL 
   AND A.DATA_RECEBIMENTO <= @DATA_FINAL           
   AND H.RECEBIMENTO_BANCO IS NULL     
   AND (D.CLASSIFICACAO_CLIENTE = @GRUPO_CLIENTE OR @GRUPO_CLIENTE IS NULL)
UNION ALL
-- RECEBIMENTOS PDV
SELECT A.TITULO_RECEBER                     AS TITULO_RECEBER , 
       C.EMPRESA                            AS EMPRESA ,
       E.EMPRESA_CONTABIL                   AS REDE , 
       E.EMPRESA_CONTABIL_NOME_FANTASIA     AS NOME_REDE , 
       E.EMPRESA_USUARIA_FILIAL             AS FILIAL , 
       D.ENTIDADE                           AS ENTIDADE , 
       D.NOME                               AS NOME , 
       C.TITULO                             AS TITULO , 
       C.VENCIMENTO                         AS VENCIMENTO ,
       A.MOVIMENTO                          AS DATA_RECEBIMENTO ,
       C.VALOR                              AS VALOR,
       A.VALOR_RECEBIMENTO                  AS RECEBIMENTO_TOTAL ,
       'PDV'                                AS CONTA,
       E.EMPRESA_CONTABIL                   AS EMPRESA_CONTABIL,
       I.DESCRICAO                          AS MODALIDADE,
       ISNULL(V.VENDEDOR,103)               AS VENDEDOR,
       ISNULL(V.NOME,'SEM VENDEDOR')        AS VENDEDOR_NOME,
	   F.DESCRICAO                          AS GRUPO_CLIENTE

     FROM PDV_RECEBIMENTOS_TITULOS          A WITH(NOLOCK) 
     --JOIN PDV_RECEBIMENTOS_FINALIZADORAS    B WITH(NOLOCK) ON B.RECEBIMENTO              = A.RECEBIMENTO
     --                                                     AND B.LOJA                     = A.LOJA
        --                                                  AND B.MOVIMENTO                = A.MOVIMENTO
        --                                                  AND B.CAIXA                    = A.CAIXA
     JOIN TITULOS_RECEBER                   C WITH(NOLOCK) ON C.TITULO_RECEBER           = A.TITULO_RECEBER
     JOIN ENTIDADES                         D WITH(NOLOCK) ON D.ENTIDADE                 = C.ENTIDADE
     JOIN FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL)
                                            E              ON E.EMPRESA_USUARIA           = C.EMPRESA
--LEFT JOIN CANCELAMENTOS_RECEBIMENTOS        G WITH(NOLOCK) ON G.RECEBIMENTO_CAIXA        = A.RECEBIMENTO_CAIXA 
     JOIN EMPRESAS_USUARIAS                 H WITH(NOLOCK) ON H.FILIAL                   = A.LOJA    
LEFT JOIN MODALIDADES_TITULOS               I WITH(NOLOCK) ON I.MODALIDADE               = C.MODALIDADE
LEFT JOIN VENDEDORES                        V WITH(NOLOCK) ON V.VENDEDOR                 = C.VENDEDOR
LEFT JOIN CLASSIFICACOES_CLIENTES           F WITH(NOLOCK) ON F.CLASSIFICACAO_CLIENTE    = D.CLASSIFICACAO_CLIENTE

    WHERE 1=1
	 AND  A.MOVIMENTO >= @DATA_INICIAL 
     AND  A.MOVIMENTO <= @DATA_FINAL   
	 AND (D.CLASSIFICACAO_CLIENTE = @GRUPO_CLIENTE OR @GRUPO_CLIENTE IS NULL)
 GROUP BY  A.TITULO_RECEBER   
          ,C.EMPRESA          
          ,E.EMPRESA_CONTABIL                
          ,E.EMPRESA_CONTABIL_NOME_FANTASIA  
          ,E.EMPRESA_USUARIA_FILIAL          
          ,D.ENTIDADE         
          ,D.NOME             
          ,C.TITULO           
          ,C.VENCIMENTO         
          ,A.MOVIMENTO        
          ,C.VALOR                    
          ,A.VALOR_RECEBIMENTO          
          ,E.EMPRESA_CONTABIL 
          ,I.DESCRICAO
          ,A.RECEBIMENTO  
          ,ISNULL(V.VENDEDOR,103)    
          ,ISNULL(V.NOME,'SEM VENDEDOR') 
		  ,F.DESCRICAO