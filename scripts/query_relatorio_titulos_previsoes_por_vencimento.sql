SET DATEFORMAT DMY  
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ      

--DECLARE @VENCIMENTO_INI   DATETIME    = '01/04/2018'
--DECLARE @VENCIMENTO_FIM   DATETIME    = '15/04/2018'
--DECLARE @EMPRESA_INI      NUMERIC(15) = 1000
--DECLARE @EMPRESA_FIM      NUMERIC(15) = 1000
--DECLARE @ENTIDADE_INI     NUMERIC(15) = NULL
--DECLARE @ENTIDADE_FIM     NUMERIC(15) = NULL
--DECLARE @CLASSIF_INI      NUMERIC(15) = NULL
--DECLARE @CLASSIF_FIM      NUMERIC(15) = NULL
--DECLARE @TIPO             VARCHAR(2)  = 'C'
                                      
                              
DECLARE @VENCIMENTO_INI   DATE
DECLARE @VENCIMENTO_FIM   DATE
DECLARE @EMPRESA_INI      NUMERIC(15) 
DECLARE @EMPRESA_FIM      NUMERIC(15) 
DECLARE @ENTIDADE_INI     NUMERIC(15) 
DECLARE @ENTIDADE_FIM     NUMERIC(15) 
DECLARE @CLASSIF_INI      NUMERIC(15) 
DECLARE @CLASSIF_FIM      NUMERIC(15) 
DECLARE @TIPO             VARCHAR(2)  
                                        
SET @VENCIMENTO_INI    = :VENCIMENTO_INI
SET @VENCIMENTO_FIM    = :VENCIMENTO_FIM
IF ISNUMERIC(:EMPRESA_INI     )                              = 1 SET @EMPRESA_INI       = :EMPRESA_INI       ELSE SET @EMPRESA_INI      = NULL
IF ISNUMERIC(:EMPRESA_FIM     )                              = 1 SET @EMPRESA_FIM         = :EMPRESA_FIM          ELSE SET @EMPRESA_FIM         = NULL
IF ISNUMERIC(:ENTIDADE_INI    )                              = 1 SET @ENTIDADE_INI        = :ENTIDADE_INI         ELSE SET @ENTIDADE_INI        = NULL
IF ISNUMERIC(:ENTIDADE_FIM    )                              = 1 SET @ENTIDADE_FIM        = :ENTIDADE_FIM         ELSE SET @ENTIDADE_FIM        = NULL
IF ISNUMERIC(:CLASSIF_INI     )                              = 1 SET @CLASSIF_INI         = :CLASSIF_INI          ELSE SET @CLASSIF_INI         = NULL
IF ISNUMERIC(:CLASSIF_FIM     )                              = 1 SET @CLASSIF_FIM         = :CLASSIF_FIM          ELSE SET @CLASSIF_FIM         = NULL
IF LEN(:TIPO)                                               >= 1 SET @TIPO              = :TIPO                 ELSE SET @TIPO              = NULL
                                                                                                           

 if object_id('tempdb..#SALDO_RECEBER_ENTIDADES') is not null
   DROP TABLE #SALDO_RECEBER_ENTIDADES


SELECT  A.ENTIDADE   AS ENTIDADE, 
        SUM(B.SALDO) AS SALDO_DEVEDOR

  INTO #SALDO_RECEBER_ENTIDADES

  FROM TITULOS_RECEBER          A WITH(NOLOCK)
  JOIN TITULOS_RECEBER_SALDO    B WITH(NOLOCK) ON B.TITULO_RECEBER  = A.TITULO_RECEBER
  LEFT
  JOIN EMPRESAS_USUARIAS        C WITH(NOLOCK) ON C.EMPRESA_USUARIA = A.ENTIDADE

  WHERE B.SITUACAO_TITULO = 1
    AND C.EMPRESA_USUARIA IS NULL

  GROUP BY A.ENTIDADE


 if object_id('tempdb..#SALDO_RECEBER_EMPRESAS_USUARIAS') is not null
   DROP TABLE #SALDO_RECEBER_EMPRESAS_USUARIAS


SELECT  C.EMPRESA_CONTABIL                  AS EMPRESA_CONTABIL,
        A.ENTIDADE                          AS ENTIDADE, 
        DBO.FN_EMPRESA_CONTABIL(A.EMPRESA)  AS EMPRESA_CONTABIL_CREDORA,
        SUM(B.SALDO)                        AS SALDO_DEVEDOR

  INTO #SALDO_RECEBER_EMPRESAS_USUARIAS

  FROM TITULOS_RECEBER          A WITH(NOLOCK)
  JOIN TITULOS_RECEBER_SALDO    B WITH(NOLOCK) ON B.TITULO_RECEBER  = A.TITULO_RECEBER
  LEFT
  JOIN EMPRESAS_USUARIAS        C WITH(NOLOCK) ON C.EMPRESA_USUARIA = A.ENTIDADE

  WHERE B.SITUACAO_TITULO = 1
    AND C.EMPRESA_USUARIA IS NOT NULL

  GROUP BY A.ENTIDADE, C.EMPRESA_CONTABIL, DBO.FN_EMPRESA_CONTABIL(A.EMPRESA)



if object_id('tempdb..#TRANSACOES_PAGAR') is not null
   DROP TABLE #TRANSACOES_PAGAR

SELECT X.TITULO_PAGAR,
       SUM (X.DESCONTO)          AS DESCONTO,
       SUM (X.ABATIMENTO)        AS ABATIMENTOS,
       SUM (X.TARIFA_BANCARIA)   AS TARIFA_BANCARIA,
       SUM (X.JUROS_ATRASO)      AS JUROS_ATRASO,
       SUM (X.MULTA_ATRASO)      AS MULTA_ATRASO,
       SUM (X.PAG_ANTERIOR)      AS PAG_ANTERIOR
  INTO #TRANSACOES_PAGAR
 FROM (

-----------------------------
-- GERA TRANSACOES ZERADAS --
-----------------------------

SELECT A.TITULO_PAGAR,
       A.DESCONTO_VALOR
                      AS DESCONTO,
       0              AS ABATIMENTO,
       A.TARIFA_BANCARIA
                      AS TARIFA_BANCARIA,
       A.JUROS_ATRASO AS JUROS_ATRASO,
       A.MULTA_ATRASO AS MULTA_ATRASO,
       0              AS PAG_ANTERIOR
  FROM TITULOS_PAGAR               A WITH (NOLOCK)
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM

UNION ALL

SELECT A.TITULO_PAGAR,
       ISNULL (SUM (A.DESCONTO_VALOR),0) - 
       ISNULL (SUM (B.DEBITO),0) AS DESCONTO,
       0              AS ABATIMENTO,
       0              AS TARIFA_BANCARIA,
       0              AS JUROS_ATRASO,
       0              AS MULTA_ATRASO,
       0              AS PAG_ANTERIOR
  FROM TITULOS_PAGAR            A WITH (NOLOCK) 
  JOIN TITULOS_PAGAR_TRANSACOES B WITH (NOLOCK) ON A.TITULO_PAGAR = B.TITULO_PAGAR
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM
   AND B.TRANSACAO_FINANCEIRA IN (3)
 GROUP BY A.TITULO_PAGAR
 
 UNION ALL
 
 ---------------------
 -- TARIFA BANC�RIA --
 ---------------------
 
 SELECT A.TITULO_PAGAR,
        0              AS DESCONTO,
        0              AS ABATIMENTO,
        SUM (A.TARIFA_BANCARIA)-
        SUM (B.DEBITO) AS TARIFA_BANCARIA,
        0              AS JUROS_ATRASO,
        0              AS MULTA_ATRASO,
        0              AS PAG_ANTERIOR
  FROM TITULOS_PAGAR            A WITH (NOLOCK) 
  JOIN TITULOS_PAGAR_TRANSACOES B WITH (NOLOCK) ON A.TITULO_PAGAR = B.TITULO_PAGAR
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM
   AND B.TRANSACAO_FINANCEIRA IN (20)
 GROUP BY A.TITULO_PAGAR
 
 UNION ALL
 
 -----------
 -- JUROS --
 -----------

 SELECT A.TITULO_PAGAR,
        0              AS DESCONTO,
        0              AS ABATIMENTO,
        0              AS TARIFA_BANCARIA,
       SUM (A.JUROS_ATRASO) -
       SUM (B.CREDITO) AS JUROS_ATRASO,
       0               AS MULTA_ATRASO,
       0               AS PAG_ANTERIOR
  FROM TITULOS_PAGAR            A WITH (NOLOCK) 
  JOIN TITULOS_PAGAR_TRANSACOES B WITH (NOLOCK) ON A.TITULO_PAGAR = B.TITULO_PAGAR
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM
   AND B.TRANSACAO_FINANCEIRA IN (9)
 GROUP BY A.TITULO_PAGAR
 
 UNION ALL
 
 -----------
 -- MULTA --
 -----------
 
 SELECT A.TITULO_PAGAR,
       0              AS DESCONTO,
       0              AS ABATIMENTO,
       0              AS TARIFA_BANCARIA,
       0              AS JUROS_ATRASO,
       SUM (A.MULTA_ATRASO) -
       SUM (B.CREDITO)AS MULTA,
       0              AS PAG_ANTERIOR
  FROM TITULOS_PAGAR            A WITH (NOLOCK) 
  JOIN TITULOS_PAGAR_TRANSACOES B WITH (NOLOCK) ON A.TITULO_PAGAR = B.TITULO_PAGAR
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM
   AND B.TRANSACAO_FINANCEIRA IN (10)
 GROUP BY A.TITULO_PAGAR
 
 UNION ALL
 
 ---------------
 -- PAGAMENTO --
 --------------- 
 
  SELECT A.TITULO_PAGAR,
         0              AS DESCONTO,
         0              AS ABATIMENTO,
         0              AS TARIFA_BANCARIA,
         0              AS JUROS_ATRASO,
         0              AS MULTA_ATRASO,
         SUM (B.DEBITO) AS PAG_ANTERIOR
  FROM TITULOS_PAGAR            A WITH (NOLOCK) 
  JOIN TITULOS_PAGAR_TRANSACOES B WITH (NOLOCK) ON A.TITULO_PAGAR = B.TITULO_PAGAR
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM
   AND B.TRANSACAO_FINANCEIRA IN (11,21)
 GROUP BY A.TITULO_PAGAR

UNION ALL

------------------
---- ABATIMENTO --
------------------
 SELECT A.TITULO_PAGAR,
        0              AS DESCONTO,
        SUM (B.DEBITO)AS ABATIMENTO,
        0              AS TARIFA_BANCARIA,
        0              AS JUROS_ATRASO,
        0              AS MULTA_ATRASO,
        0              AS PAG_ANTERIOR
  FROM TITULOS_PAGAR            A WITH (NOLOCK) 
  JOIN TITULOS_PAGAR_TRANSACOES B WITH (NOLOCK) ON A.TITULO_PAGAR = B.TITULO_PAGAR
 WHERE A.VENCIMENTO >= @VENCIMENTO_INI
   AND A.VENCIMENTO <= @VENCIMENTO_FIM
   AND B.TRANSACAO_FINANCEIRA IN (2, 22)
 GROUP BY A.TITULO_PAGAR) X
GROUP BY X.TITULO_PAGAR



 --------------------------------------------
 -- RELAT�RIO  TITULOS E PREVISOES A PAGAR --
 --------------------------------------------   

   -------------
   -- TITULOS --
   -------------   

   SELECT ISNULL(@VENCIMENTO_INI,'01/01/1927')                          AS VENCIMENTO_INICIAL
        , ISNULL(@VENCIMENTO_FIM,'01/01/2099')                          AS VENCIMENTO_FINAL
        , ISNULL(@EMPRESA_INI,1)                                        AS EMPRESA_INICIAL
        , ISNULL(@EMPRESA_FIM,9999999)                                  AS EMPRESA_FINAL
        , CASE WHEN @TIPO = 'T'  THEN 'TITULOS A PAGAR'
               WHEN @TIPO = 'P'  THEN 'PREVIS�ES A PAGAR'
               WHEN @TIPO = 'TP' THEN 'TITULOS E PREVIS�ES A PAGAR' END AS SUB_TITULO
        , ISNULL(@CLASSIF_INI ,1)                                       AS CLASSIF_INICIAL
        , ISNULL(@CLASSIF_FIM ,9999999)                                 AS CLASSIF_FINAL
        , ISNULL(@ENTIDADE_INI,1)                                       AS ENTIDADE_INICIAL
        , ISNULL(@ENTIDADE_FIM,9999999)                                 AS ENTIDADE_FINAL
        , A.TITULO_PAGAR                                                AS REGISTRO
        , A.REG_MASTER_ORIGEM                                           AS CHAVE
        , ISNULL(F.DESCRICAO,'TRANSPORTADORA')                          AS DESCR_CLASSIF
        , X.EMPRESA_CONTABIL                                            AS MATRIZ
        , Y.NOME                                                        AS MATRIZ_NOME
        , A.EMPRESA                                                     AS FILIAL
        , X.NOME                                                        AS FILIAL_NOME
        , X.NOME_FANTASIA                                               AS FILIAL_NOME_FANTASIA
        , A.TITULO                                                      AS TITULO
        , A.EMISSAO                                                     AS EMISSAO
        , A.MOVIMENTO                                                   AS MOVIMENTO
        , A.VENCIMENTO                                                  AS VENCIMENTO
        , CAST( B.ENTIDADE AS VARCHAR) + '-' + B.NOME                                                        
                                                                        AS ENTIDADE
        ,  A.VALOR - (A.RETENCAO_IR + A.RETENCAO_ISS    + A.RETENCAO_INSS +
                     A.RETENCAO_PIS + A.RETENCAO_COFINS + A.RETENCAO_CSSL +
                     A.RETENCAO_COPICS)                                    AS VALOR_ORIGINAL 

        , ISNULL (E.ABATIMENTOS     , 0 )                                AS ABATIMENTOS
        , ISNULL (E.DESCONTO        , 0 )                                AS DESCONTO
        , ISNULL (E.TARIFA_BANCARIA , 0 )                                AS TARIFA_BANCARIA
        , ISNULL (E.JUROS_ATRASO    , 0 )                                AS JUROS
        , ISNULL (E.MULTA_ATRASO    , 0 )                                AS MULTA
        , ISNULL (E.PAG_ANTERIOR    , 0 ) + ISNULL( S.SALDO_DEVEDOR ,0 ) + ISNULL (T.SALDO_DEVEDOR ,0 )
                                                                         AS PAG_ANTERIOR
        , ISNULL( D.VALOR - ( ISNULL ( E.ABATIMENTOS     , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( E.DESCONTO        , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( E.PAG_ANTERIOR    , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_IR     , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_ISS    , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_INSS   , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_PIS    , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_COFINS , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_CSSL   , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( A.RETENCAO_COPICS , 0) * ( D.VALOR / A.VALOR ) )  
                  + ( ISNULL ( E.TARIFA_BANCARIA , 0) + --* ( D.VALOR / A.VALOR ) +
                      ISNULL ( E.JUROS_ATRASO    , 0) * ( D.VALOR / A.VALOR ) +
                      ISNULL ( E.MULTA_ATRASO    , 0) * ( D.VALOR / A.VALOR ) )  
          , A.VALOR - (A.RETENCAO_IR + A.RETENCAO_ISS    + A.RETENCAO_INSS +
                               A.RETENCAO_PIS + A.RETENCAO_COFINS + A.RETENCAO_CSSL +
                               A.RETENCAO_COPICS) )

                                                                        AS SALDO
        , 'T'                                                           AS SITUACAO
        , ISNULL(@TIPO,'TP')                                            AS TIPO 
        , A1.MODALIDADE
        , G.DESCRICAO                                                   AS MODALIDADE_TITULO

     FROM TITULOS_PAGAR_PREVISOES                               A1 WITH (NOLOCK)
LEFT JOIN TITULOS_PAGAR                                          A WITH (NOLOCK) ON A.TITULO              = A1.TITULO
                                                                                AND A.EMPRESA             = A1.EMPRESA
                                                                                AND A.ENTIDADE            = A1.ENTIDADE
                                                                                AND A.VENCIMENTO          = A1.VENCIMENTO
                                                                                AND A.VALOR               = A1.VALOR                                                                                
LEFT JOIN ENTIDADES                                              B WITH (NOLOCK) ON A.ENTIDADE            = B.ENTIDADE
LEFT JOIN TITULOS_PAGAR_SALDO                                    C WITH (NOLOCK) ON A.TITULO_PAGAR        = C.TITULO_PAGAR
LEFT JOIN (   SELECT TITULO_PAGAR       AS TITULO_PAGAR
                   , CLASSIF_FINANCEIRA AS CLASSIF_FINANCEIRA
                   , SUM ( VALOR )      AS VALOR
                FROM TITULOS_PAGAR_CLASSIFICACOES WITH(NOLOCK)
            GROUP BY TITULO_PAGAR
                   , CLASSIF_FINANCEIRA
          )                                                      D               ON D.TITULO_PAGAR       = A.TITULO_PAGAR
LEFT JOIN #TRANSACOES_PAGAR                                      E WITH (NOLOCK) ON A.TITULO_PAGAR       = E.TITULO_PAGAR
LEFT JOIN CLASSIF_FINANCEIRAS                                    F WITH (NOLOCK) ON D.CLASSIF_FINANCEIRA = F.CLASSIF_FINANCEIRA
     JOIN EMPRESAS_USUARIAS                                      X WITH (NOLOCK) ON X.EMPRESA_USUARIA    = A.EMPRESA
     JOIN EMPRESAS_USUARIAS                                      Y WITH (NOLOCK) ON Y.EMPRESA_USUARIA    = X.EMPRESA_CONTABIL
     LEFT
     JOIN MODALIDADES_TITULOS                                    G WITH (NOLOCK) ON G.MODALIDADE         = A1.MODALIDADE     
     LEFT
     JOIN #SALDO_RECEBER_ENTIDADES                               S WITH(NOLOCK) ON S.ENTIDADE            = A.ENTIDADE
     LEFT
     JOIN #SALDO_RECEBER_EMPRESAS_USUARIAS                       T WITH(NOLOCK) ON T.EMPRESA_CONTABIL_CREDORA    = DBO.FN_EMPRESA_CONTABIL(A.EMPRESA)
                                                                               AND T.ENTIDADE                    = A.ENTIDADE

    WHERE ISNULL ( C.SITUACAO_TITULO , 0 ) < 2
      AND ( A.VENCIMENTO         >= @VENCIMENTO_INI OR @VENCIMENTO_INI   IS NULL )
      AND ( A.VENCIMENTO         <= @VENCIMENTO_FIM OR @VENCIMENTO_FIM   IS NULL )
      AND ( A.EMPRESA            >= @EMPRESA_INI    OR @EMPRESA_INI      IS NULL )
      AND ( A.EMPRESA            <= @EMPRESA_FIM    OR @EMPRESA_FIM      IS NULL )
      AND ( D.CLASSIF_FINANCEIRA >= @CLASSIF_INI    OR D.CLASSIF_FINANCEIRA IS NULL OR @CLASSIF_INI IS NULL )
      AND ( D.CLASSIF_FINANCEIRA <= @CLASSIF_FIM    OR D.CLASSIF_FINANCEIRA IS NULL OR @CLASSIF_FIM IS NULL )
      AND ( A.ENTIDADE           >= @ENTIDADE_INI   OR @ENTIDADE_INI     IS NULL )
      AND ( A.ENTIDADE           <= @ENTIDADE_FIM   OR @ENTIDADE_FIM     IS NULL )
      AND (( @TIPO LIKE '%T%') OR
           ( @TIPO IS NULL))
      AND A1.TIPO  = 'PROVIS�O'
      AND A1.SALDO > 0

    UNION ALL

   ---------------
   -- PREVIS�ES --
   ---------------

   SELECT @VENCIMENTO_INI                                               AS VENCIMENTO_INICIAL
        , @VENCIMENTO_FIM                                               AS VENCIMENTO_FINAL
        , ISNULL(@EMPRESA_INI,1)                                        AS EMPRESA_INICIAL
        , ISNULL(@EMPRESA_FIM,9999999)                                  AS EMPRESA_FINAL
        , CASE WHEN @TIPO = 'T'  THEN 'TITULOS A PAGAR'
               WHEN @TIPO = 'P'  THEN 'PREVIS�ES A PAGAR'
               WHEN @TIPO = 'TP' THEN 'TITULOS E PREVIS�ES A PAGAR' END AS SUB_TITULO
        , ISNULL(@CLASSIF_INI,1)                                        AS CLASSIF_INICIAL
        , ISNULL(@CLASSIF_FIM,9999999)                                  AS CLASSIF_FINAL
        , ISNULL(@ENTIDADE_INI,1)                                       AS ENTIDADE_INICIAL
        , ISNULL(@ENTIDADE_FIM,9999999)                                 AS ENTIDADE_FINAL
        , 0                                                             AS REGISTRO
        , 0                                                             AS CHAVE
        , C.DESCRICAO                                                   AS DESCR_CLASSIF
        , X.EMPRESA_CONTABIL                                            AS MATRIZ
        , Y.NOME                                                        AS MATRIZ_NOME
        , A.EMPRESA                                                     AS FILIAL
        , X.NOME                                                        AS FILIAL_NOME
        , X.NOME_FANTASIA                                               AS FILIAL_NOME_FANTASIA
        , A.TITULO                                                      AS TITULO
        , A.VENCIMENTO                                                  AS EMISSAO
        , A.VENCIMENTO                                                  AS MOVIMENTO
        , A.VENCIMENTO                                                  AS VENCIMENTO
        , CAST( B.ENTIDADE AS VARCHAR) + '-' + B.NOME                                                        
                                                                        AS ENTIDADE
        , A.VALOR                                                       AS VALOR_ORIGINAL
        , 0                                                             AS ABATIMENTOS
        , 0                                                             AS DESCONTO
        , 0                                                             AS TARIFA_BANCARIA
        , 0                                                             AS JUROS
        , 0                                                             AS MULTA
        , ISNULL( S.SALDO_DEVEDOR ,0 ) +ISNULL (T.SALDO_DEVEDOR ,0 )    AS PAG_ANTERIOR
        , A.SALDO                                                       AS SALDO
        , 'P'                                                           AS SITUACAO
        , ISNULL(@TIPO,'TP')                                            AS TIPO         
        , A.MODALIDADE
        , D.DESCRICAO                                                   AS MODALIDADE_TITULO        

     FROM TITULOS_PAGAR_PREVISOES     A WITH (NOLOCK)
LEFT JOIN ENTIDADES                   B WITH (NOLOCK) ON A.ENTIDADE                = B.ENTIDADE
     JOIN CLASSIF_FINANCEIRAS         C WITH (NOLOCK) ON A.CLASSIF_FINANCEIRA      = C.CLASSIF_FINANCEIRA
     JOIN EMPRESAS_USUARIAS           X WITH (NOLOCK) ON X.EMPRESA_USUARIA         = A.EMPRESA
     JOIN EMPRESAS_USUARIAS           Y WITH (NOLOCK) ON Y.EMPRESA_USUARIA         = X.EMPRESA_CONTABIL
     LEFT
     JOIN MODALIDADES_TITULOS         D WITH (NOLOCK) ON D.MODALIDADE              = A.MODALIDADE
     LEFT
     JOIN #SALDO_RECEBER_ENTIDADES                               S WITH(NOLOCK) ON S.ENTIDADE            = A.ENTIDADE
     LEFT
     JOIN #SALDO_RECEBER_EMPRESAS_USUARIAS                       T WITH(NOLOCK) ON T.EMPRESA_CONTABIL_CREDORA    = DBO.FN_EMPRESA_CONTABIL(A.EMPRESA)
                                                                               AND T.ENTIDADE                    = A.ENTIDADE
                               
    WHERE 1=1
      AND ( A.VENCIMENTO         >= @VENCIMENTO_INI OR @VENCIMENTO_INI IS NULL )
      AND ( A.VENCIMENTO         <= @VENCIMENTO_FIM OR @VENCIMENTO_FIM IS NULL )
      AND ( A.EMPRESA            >= @EMPRESA_INI    OR @EMPRESA_INI    IS NULL )
      AND ( A.EMPRESA            <= @EMPRESA_FIM    OR @EMPRESA_FIM    IS NULL )
      AND ( A.CLASSIF_FINANCEIRA >= @CLASSIF_INI    OR @CLASSIF_INI    IS NULL )
      AND ( A.CLASSIF_FINANCEIRA <= @CLASSIF_FIM    OR @CLASSIF_FIM    IS NULL )
      AND ( A.ENTIDADE           >= @ENTIDADE_INI   OR @ENTIDADE_INI   IS NULL )
      AND ( A.ENTIDADE           <= @ENTIDADE_FIM   OR @ENTIDADE_FIM   IS NULL )
      AND (( @TIPO LIKE '%P%') OR
           ( @TIPO IS NULL))
      AND A.TIPO = 'PREVIS�O'
      AND A.TIPO_PREVISAO IN (1, 3)

UNION ALL

   SELECT @VENCIMENTO_INI                                               AS VENCIMENTO_INICIAL
        , @VENCIMENTO_FIM                                               AS VENCIMENTO_FINAL
        , ISNULL(@EMPRESA_INI,1)                                        AS EMPRESA_INICIAL
        , ISNULL(@EMPRESA_FIM,9999999)                                  AS EMPRESA_FINAL
        , CASE WHEN @TIPO = 'T'  THEN 'TITULOS A PAGAR'
               WHEN @TIPO = 'P'  THEN 'PREVIS�ES A PAGAR'
               WHEN @TIPO = 'TP' THEN 'TITULOS E PREVIS�ES A PAGAR' END AS SUB_TITULO
        , ISNULL(@CLASSIF_INI,1)                                        AS CLASSIF_INICIAL
        , ISNULL(@CLASSIF_FIM,9999999)                                  AS CLASSIF_FINAL
        , ISNULL(@ENTIDADE_INI,1)                                       AS ENTIDADE_INICIAL
        , ISNULL(@ENTIDADE_FIM,9999999)                                 AS ENTIDADE_FINAL
        , 0                                                             AS REGISTRO
        , 0                                                             AS CHAVE
        , C.DESCRICAO                                                   AS DESCR_CLASSIF
        , X.EMPRESA_CONTABIL                                            AS MATRIZ
        , Y.NOME                                                        AS MATRIZ_NOME
        , A.EMPRESA                                                     AS FILIAL
        , X.NOME                                                        AS FILIAL_NOME
        , X.NOME_FANTASIA                                               AS FILIAL_NOME_FANTASIA
        , A.TITULO                                                      AS TITULO
        , A.VENCIMENTO                                                  AS EMISSAO
        , A.VENCIMENTO                                                  AS MOVIMENTO
        , A.VENCIMENTO                                                  AS VENCIMENTO
        , CAST( B.ENTIDADE AS VARCHAR) + '-' + B.NOME                                                        
                                                                        AS ENTIDADE
        , A.VALOR                                                       AS VALOR_ORIGINAL
        , 0                                                             AS ABATIMENTOS
        , 0                                                             AS DESCONTO
        , 0                                                             AS TARIFA_BANCARIA
        , 0                                                             AS JUROS
        , 0                                                             AS MULTA
        , ISNULL( S.SALDO_DEVEDOR ,0 ) +ISNULL (T.SALDO_DEVEDOR ,0 )    AS PAG_ANTERIOR
        , A.SALDO                                                       AS SALDO
        , 'P'                                                           AS SITUACAO
        , ISNULL(@TIPO,'TP')                                            AS TIPO         
        , A.MODALIDADE
        , D.DESCRICAO                                                   AS MODALIDADE_TITULO        

     FROM TITULOS_PAGAR_PREVISOES     A WITH (NOLOCK)
LEFT JOIN ENTIDADES                   B WITH (NOLOCK) ON A.ENTIDADE                = B.ENTIDADE
     JOIN CLASSIF_FINANCEIRAS         C WITH (NOLOCK) ON A.CLASSIF_FINANCEIRA      = C.CLASSIF_FINANCEIRA
     JOIN EMPRESAS_USUARIAS           X WITH (NOLOCK) ON X.EMPRESA_USUARIA         = A.EMPRESA
     JOIN EMPRESAS_USUARIAS           Y WITH (NOLOCK) ON Y.EMPRESA_USUARIA         = X.EMPRESA_CONTABIL
     LEFT
     JOIN MODALIDADES_TITULOS         D WITH (NOLOCK) ON D.MODALIDADE              = A.MODALIDADE
     LEFT
     JOIN #SALDO_RECEBER_ENTIDADES                               S WITH(NOLOCK) ON S.ENTIDADE            = A.ENTIDADE
     LEFT
     JOIN #SALDO_RECEBER_EMPRESAS_USUARIAS                       T WITH(NOLOCK) ON T.EMPRESA_CONTABIL_CREDORA    = DBO.FN_EMPRESA_CONTABIL(A.EMPRESA)
                                                                               AND T.ENTIDADE                    = A.ENTIDADE
                               
    WHERE 1=1
      AND ( A.VENCIMENTO         >= @VENCIMENTO_INI OR @VENCIMENTO_INI IS NULL )
      AND ( A.VENCIMENTO         <= @VENCIMENTO_FIM OR @VENCIMENTO_FIM IS NULL )
      AND ( A.EMPRESA            >= @EMPRESA_INI    OR @EMPRESA_INI    IS NULL )
      AND ( A.EMPRESA            <= @EMPRESA_FIM    OR @EMPRESA_FIM    IS NULL )
      AND ( A.CLASSIF_FINANCEIRA >= @CLASSIF_INI    OR @CLASSIF_INI    IS NULL )
      AND ( A.CLASSIF_FINANCEIRA <= @CLASSIF_FIM    OR @CLASSIF_FIM    IS NULL )
      AND ( A.ENTIDADE           >= @ENTIDADE_INI   OR @ENTIDADE_INI   IS NULL )
      AND ( A.ENTIDADE           <= @ENTIDADE_FIM   OR @ENTIDADE_FIM   IS NULL )
      AND (( @TIPO LIKE '%C%') OR
           ( @TIPO IS NULL))
      AND A.TIPO = 'PREVIS�O'
      AND A.TIPO_PREVISAO IN (2)

 ORDER BY 
          MATRIZ
        , FILIAL
        , DESCR_CLASSIF
        , ENTIDADE
        , VENCIMENTO
        , SALDO

 if object_id('tempdb..#SALDO_RECEBER_ENTIDADES') is not null
   DROP TABLE #SALDO_RECEBER_ENTIDADES

 if object_id('tempdb..#SALDO_RECEBER_EMPRESAS_USUARIAS') is not null
   DROP TABLE #SALDO_RECEBER_EMPRESAS_USUARIAS

   if object_id('tempdb..#TRANSACOES_PAGAR') is not null
   DROP TABLE #TRANSACOES_PAGAR