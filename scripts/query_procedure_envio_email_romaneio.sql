ALTER PROCEDURE [dbo].[EMAIL_ENVIO_ROMANEIO] ( @GERACAO_ROMANEIO_WMS NUMERIC(15,0)) AS   
BEGIN                      
                
SET NOCOUNT ON      

--DECLARE @GERACAO_ROMANEIO_WMS NUMERIC
DECLARE @USUARIO_LOGADO NVARCHAR(MAX)
DECLARE @DATA DATE
DECLARE @CENTRO_ESTOQUE_ORIGEM NUMERIC
DECLARE @PRODUTO NUMERIC
DECLARE @DESCRICAO VARCHAR
DECLARE @SOLICITACAO_FATURAMENTO NUMERIC
DECLARE @NOME NVARCHAR
DECLARE @EMAIL VARCHAR(250) 
DECLARE @TITULO_EMAIL VARCHAR(50)  
DECLARE @tableHTML NVARCHAR(MAX)
DECLARE @GERACOES_ROMANEIOS_WMS_SOLICITACOES NVARCHAR(MAX)
DECLARE @EMAIL_ENVIO_ROMANEIO VARCHAR(250)

SELECT 
         @TITULO_EMAIL = 'PEDIDO PROHOSPITAL: ROMANEIO EM SEPARA��O ' + CAST( @GERACAO_ROMANEIO_WMS AS VARCHAR ),
		 @EMAIL = H.EMAIL_ENVIO_ROMANEIO,
		 @GERACAO_ROMANEIO_WMS = CAST(A.GERACAO_ROMANEIO_WMS AS VARCHAR(50))   ,
		 @USUARIO_LOGADO       =CAST(A.USUARIO_LOGADO AS VARCHAR(50))         ,
		 @DATA                 =CAST(A.DATA_HORA AS VARCHAR(50))              ,
		 @CENTRO_ESTOQUE_ORIGEM = CAST(A.CENTRO_ESTOQUE_ORIGEM AS VARCHAR(50))  ,
		 @PRODUTO = CAST(C.PRODUTO AS VARCHAR(50))                ,
		 @DESCRICAO = CAST(D.DESCRICAO AS VARCHAR(50))              ,
		 @SOLICITACAO_FATURAMENTO = CAST(E.SOLICITACAO_FATURAMENTO AS VARCHAR(50)),
		 @GERACOES_ROMANEIOS_WMS_SOLICITACOES = CAST(F.NOME AS NVARCHAR(MAX))

FROM GERACOES_ROMANEIOS_WMS              A WITH(NOLOCK)
LEFT
JOIN PEDIDOS_VENDAS                      B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
LEFT
JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA    C WITH(NOLOCK) ON B.PEDIDO_VENDA = C.PEDIDO_VENDA
LEFT
JOIN PRODUTOS                            D WITH(NOLOCK) ON D.PRODUTO      = C.PRODUTO
LEFT 
JOIN GERACOES_ROMANEIOS_WMS_SOLICITACOES E WITH(NOLOCK) ON E.PEDIDO_VENDA = A.PEDIDO_VENDA
LEFT 
JOIN ENTIDADES                           F WITH(NOLOCK) ON E.ENTIDADE     = F.ENTIDADE
JOIN EMPRESAS_USUARIAS                   G WITH(NOLOCK) ON G.EMPRESA_USUARIA      = A.EMPRESA
JOIN PARAMETROS_ESTOQUE                  H WITH(NOLOCK) ON H.EMPRESA_USUARIA      =G.EMPRESA_USUARIA

 
WHERE A.GERACAO_ROMANEIO_WMS = @GERACAO_ROMANEIO_WMS
AND E.ENTIDADE < 11

SET @tableHTML =  N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;  
              }  
              </style>' +
			  
			  'Romaneio N� '          + CAST(@GERACAO_ROMANEIO_WMS AS VARCHAR)        + '<br>
 Usuario  N� '          + CAST(@USUARIO_LOGADO  AS VARCHAR)                           + '<br>
 Data =      '          + CAST(@DATA     AS VARCHAR)                                  + '<br>
 Centro de Origem = '   + CAST(@CENTRO_ESTOQUE_ORIGEM AS VARCHAR)                     + '<br> 
 Destino =      '       + CAST(@GERACOES_ROMANEIOS_WMS_SOLICITACOES AS NVARCHAR(MAX)) + '<br><br><br>'
			  
			  +
			  
			  N'<table border = "1" class = "formato">' +
			   N'<tr>   
           <th>PRODUTO</th>
		   <th>DESCRI��O</th>
		   <th>QUANTIDADE</th>  
      </tr>' +

	      CAST ( (   
  
 SELECT   
    td = C.PRODUTO           ,  '',   
    td = C.DESCRICAO         ,  '',  
    td = B.QUANTIDADE                                                        
                                        
   FROM GERACOES_ROMANEIOS_WMS A WITH(NOLOCK)
   LEFT  
   JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA B WITH(NOLOCK) ON B.PEDIDO_VENDA = A.PEDIDO_VENDA  
   LEFT
   JOIN PRODUTOS                        C WITH(NOLOCK) ON C.PRODUTO       = B.PRODUTO  
   
  WHERE A.GERACAO_ROMANEIO_WMS = @GERACAO_ROMANEIO_WMS 
  
              FOR XML PATH('tr'), TYPE   
    ) AS NVARCHAR(MAX) ) +  
    N'</table>' ;  
	  
	        
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML'  ;
  
  
END 




  

