SET DATEFORMAT DMY

--DECLARE @DATA_INI                            DATE    = :DATA_INI    
--DECLARE @DATA_FIM                            DATE    = :DATA_FIM          
--DECLARE @EMPRESA_INI                         NUMERIC    
--DECLARE @EMPRESA_FIM                         NUMERIC    
--DECLARE @INSCRICAO_FEDERAL_DESTINATARIO_INI  VARCHAR(30) 
--DECLARE @INSCRICAO_FEDERAL_DESTINATARIO_FIM  VARCHAR(30)                                                   
--DECLARE @EMITENTE                            NUMERIC 
--DECLARE @FORMULARIO_PROCESSO                 NUMERIC 
--DECLARE @DATA_RECEBIMENTO_INI                DATE 
--DECLARE @DATA_RECEBIMENTO_FIM                DATE 
                                        
--IF ISNUMERIC(:EMPRESA_INI)                      = 1 SET @EMPRESA_INI          = :EMPRESA_INI          ELSE SET @EMPRESA_INI          = NULL
--IF ISNUMERIC(:EMPRESA_FIM)                      = 1 SET @EMPRESA_FIM          = :EMPRESA_FIM          ELSE SET @EMPRESA_FIM          = NULL
--IF ISNUMERIC(:EMITENTE)                         = 1 SET @EMITENTE             = :EMITENTE             ELSE SET @EMITENTE             = NULL
--IF ISNUMERIC(:NUMID)                            = 1 SET @FORMULARIO_PROCESSO  = :NUMID                ELSE SET @FORMULARIO_PROCESSO  = NULL
--IF ISDATE( LEFT( cast(:DATA_RECEBIMENTO_INI as datetime2) , 23 ) ) = 1 SET @DATA_RECEBIMENTO_INI = :DATA_RECEBIMENTO_INI ELSE SET @DATA_RECEBIMENTO_INI = NULL
--IF ISDATE( LEFT( cast(:DATA_RECEBIMENTO_FIM as datetime2) , 23 ) ) = 1 SET @DATA_RECEBIMENTO_FIM = :DATA_RECEBIMENTO_FIM ELSE SET @DATA_RECEBIMENTO_FIM = NULL

                                 
--SELECT @INSCRICAO_FEDERAL_DESTINATARIO_INI = E.INSCRICAO_FEDERAL FROM ENTIDADES E WITH(NOLOCK) WHERE E.ENTIDADE = @EMPRESA_INI
--SELECT @INSCRICAO_FEDERAL_DESTINATARIO_FIM = E.INSCRICAO_FEDERAL FROM ENTIDADES E WITH(NOLOCK) WHERE E.ENTIDADE = @EMPRESA_FIM            
                                                     

DECLARE @EMPRESA_INI                        NUMERIC  = 1000
DECLARE @EMPRESA_FIM                        NUMERIC  = 1000    
DECLARE @DATA_INI                           DATE = '01/05/2018'
DECLARE @DATA_FIM                           DATE = '30/05/2018'
DECLARE @INSCRICAO_FEDERAL_DESTINATARIO_INI VARCHAR(30) 
DECLARE @INSCRICAO_FEDERAL_DESTINATARIO_FIM VARCHAR(30)
DECLARE @EMITENTE                           NUMERIC  = NULL
DECLARE @FORMULARIO_PROCESSO                NUMERIC  = NULL
DECLARE @DATA_RECEBIMENTO_INI               DATE     = NULL
DECLARE @DATA_RECEBIMENTO_FIM               DATE     = NULL


SELECT @INSCRICAO_FEDERAL_DESTINATARIO_INI = E.INSCRICAO_FEDERAL FROM ENTIDADES E WITH(NOLOCK) WHERE E.ENTIDADE = @EMPRESA_INI
SELECT @INSCRICAO_FEDERAL_DESTINATARIO_FIM = E.INSCRICAO_FEDERAL FROM ENTIDADES E WITH(NOLOCK) WHERE E.ENTIDADE = @EMPRESA_FIM


--if object_id('tempdb..#TEMP_ENTIDADES_1') is not null
--   DROP TABLE #TEMP_ENTIDADES_1

--SELECT ENTIDADE, INSCRICAO_FEDERAL AS INSCRICAO_FEDERAL 

--  INTO #TEMP_ENTIDADES_1

--    FROM ENTIDADES     A WITH(NOLOCK) 
--    JOIN NFE_CABECALHO B WITH(NOLOCK) ON ISNULL(DBO.FN_MASCARA_CNPJ(B.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(B.XML_CPF_E03)) = A.INSCRICAO_FEDERAL
--              WHERE 1=1
--              AND ( CAST(SUBSTRING(B.XML_dhEmi_B09,1,10) AS DATE)   >= @DATA_INI    OR @DATA_INI    IS NULL )
--              AND ( CAST(SUBSTRING(B.XML_dhEmi_B09,1,10) AS DATE)   <= @DATA_FIM    OR @DATA_FIM    IS NULL )
--              AND ( B.EMPRESA                                       >= @EMPRESA_INI OR @EMPRESA_INI IS NULL )
--              AND ( B.EMPRESA                                       <= @EMPRESA_FIM OR @EMPRESA_FIM IS NULL )   

--if object_id('tempdb..#TEMP_ENTIDADES') is not null
--   DROP TABLE #TEMP_ENTIDADES

--SELECT INSCRICAO_FEDERAL, MAX(ENTIDADE) AS ENTIDADE

--  INTO #TEMP_ENTIDADES

--    FROM #TEMP_ENTIDADES_1 WITH(NOLOCK) 
--GROUP BY INSCRICAO_FEDERAL

--CREATE INDEX IX_TEMP_ENTIDADES ON #TEMP_ENTIDADES (INSCRICAO_FEDERAL)

           SELECT 
                    ISNULL(A.DATA_RECEBIMENTO,A.MOVIMENTO)                                                    AS MOVIMENTO,
                    A.NF_NUMERO                                                                               AS NUMERO,
                    A.NF_SERIE                                                                                AS SERIE,
                    A.EMISSAO                                                                                 AS EMISSAO,
                    A.EMPRESA                                                                                 AS EMPRESA,
                    D.NOME_FANTASIA                                                                           AS NOME_EMPRESA,
                    A.ENTIDADE                                                                                AS ENTIDADE,
                    E.NOME_FANTASIA                                                                           AS NOME_EMITENTE,
                    A.NF_COMPRA                                                                               AS NF_COMPRA,
                    C.TOTAL_GERAL                                                                             AS TOTAL_GERAL,
                    B.ICMS_BASE_CALCULO                                                                       AS BASE_CALCULO_ICMS,
                    B.ICMS_ALIQUOTA                                                                           AS AL�QUOTA_ICMS,
                    B.ICMS_VALOR                                                                              AS IMPOSTO_CRED,
                    B.ICMS_ST_BASE_CALCULO                                                                    AS BASE_CALCULO_ST,
                    B.ICMS_ST_VALOR                                                                           AS VALOR_ICMS_ST,
                    B.PIS_ALIQUOTA                                                                            AS PIS_ALIQUOTA,
                    B.PIS_VALOR                                                                               AS PIS_VALOR,
                    B.COFINS_ALIQUOTA                                                                         AS COFINS_ALIQUOTA,
                    B.COFINS_VALOR                                                                            AS COFINS_VALOR,
                    'Per�odo de '+CONVERT(VARCHAR, @DATA_INI, 103 )+' a '+ CONVERT (VARCHAR, @DATA_FIM, 103)  AS PERIODO,
                    'NF_COMPRA'                                                                               AS FORMULARIO,
                    48                                                                                        AS FORMULARIO_ID,
                    B.ESTADO_NOTA                                                                             AS ESTADO_NOTA ,
                    F.MOVIMENTO                                                                               AS DATA_RECEBIMENTO
            
              FROM NF_COMPRA          A WITH(NOLOCK)
              
              JOIN (
                             SELECT 
                             A.NF_COMPRA,
                             'NF_COMPRA'                 AS FORMULARIO           ,
                             'Autorizada'                AS ESTADO_NOTA          ,
                             SUM(A.ICMS_BASE_CALCULO   ) AS ICMS_BASE_CALCULO    ,
                             MAX(A.ICMS_ALIQUOTA       ) AS ICMS_ALIQUOTA        ,
                            
							 SUM(CASE WHEN A.ICMS_CREDITO = 'S'
									  THEN A.ICMS_VALOR 
							          ELSE 0
									   END)                  AS ICMS_VALOR           ,
                             SUM(A.ICMS_ST_BASE_CALCULO) AS ICMS_ST_BASE_CALCULO ,
                             MAX(A.ICMS_ST_ALIQUOTA    ) AS ICMS_ST_ALIQUOTA     ,
                             SUM(A.ICMS_ST_VALOR       ) AS ICMS_ST_VALOR        ,
                             MAX(A.PIS_ALIQUOTA        ) AS PIS_ALIQUOTA         ,
                             SUM(A.PIS_VALOR           ) AS PIS_VALOR            ,
                             MAX(A.COFINS_ALIQUOTA     ) AS COFINS_ALIQUOTA      ,
                             SUM(A.COFINS_VALOR        ) AS COFINS_VALOR
                
                               FROM NF_COMPRA_PRODUTOS A WITH(NOLOCK)
                               JOIN NF_COMPRA          B WITH(NOLOCK) ON B.NF_COMPRA       = A.NF_COMPRA
                               JOIN OPERACOES_FISCAIS  C WITH(NOLOCK) ON C.OPERACAO_FISCAL = A.OPERACAO_FISCAL
                              WHERE
                                     (     ( C.RECEBIMENTO = 'S' AND B.RECEBIMENTO IS NOT NULL )
                                       OR  ( C.RECEBIMENTO = 'N'                               )
                                     )
                                      AND B.PROCESSAR = 'S'

                           GROUP BY A.NF_COMPRA                             
                                        
                   )                        B              ON B.NF_COMPRA  = A.NF_COMPRA
              JOIN NF_COMPRA_TOTAIS         C WITH(NOLOCK) ON C.NF_COMPRA  = A.NF_COMPRA
              JOIN ENTIDADES                E WITH(NOLOCK) ON E.ENTIDADE   = A.ENTIDADE
              JOIN ENTIDADES                D WITH(NOLOCK) ON D.ENTIDADE   = A.EMPRESA
              LEFT 
              JOIN RECEBIMENTOS_MOVIMENTOS  F WITH(NOLOCK) ON F.NF_COMPRA  = A.NF_COMPRA
                                                          --AND F.NF_SERIE   = A.NF_SERIE
                                                          --AND F.ENTIDADE   = A.ENTIDADE
              LEFT 
              JOIN NFE_CABECALHO            G WITH(NOLOCK) ON G.CHAVE_NFE  = A.CHAVE_NFE

              
              WHERE 1=1
              AND ( ISNULL(A.DATA_RECEBIMENTO,A.MOVIMENTO)   >= @DATA_INI                OR @DATA_INI              IS NULL )
              AND ( ISNULL(A.DATA_RECEBIMENTO,A.MOVIMENTO)   <= @DATA_FIM                OR @DATA_FIM              IS NULL )
              AND ( A.EMPRESA                                >= @EMPRESA_INI             OR @EMPRESA_INI           IS NULL )
              AND ( A.EMPRESA                                <= @EMPRESA_FIM             OR @EMPRESA_FIM           IS NULL )
              AND ( A.ENTIDADE                                = @EMITENTE                OR @EMITENTE              IS NULL )
              AND ( @FORMULARIO_PROCESSO                      = 48                       OR @FORMULARIO_PROCESSO   IS NULL )
              AND ( F.MOVIMENTO                              >= @DATA_RECEBIMENTO_INI    OR @DATA_RECEBIMENTO_INI  IS NULL )
              AND ( F.MOVIMENTO                              <= @DATA_RECEBIMENTO_FIM    OR @DATA_RECEBIMENTO_FIM  IS NULL )
              --AND G.CHAVE_NFE IS NULL


UNION ALL

           SELECT 
                    A.MOVIMENTO                                                                               AS MOVIMENTO,
                    CASE WHEN A.EMITIR_NFE = 'S'                                                                   
                         THEN A.NF_NUMERO
                          ELSE A.NF_NUMERO_CLIENTE                                                                  
                    END                                                                                       AS NF_NUMERO , --MARIA QUIRINO 19/10/2017 TICKET 30258
                    A.NF_SERIE                                                                                AS SERIE,
                    A.EMISSAO                                                                                 AS EMISSAO,
                    A.EMPRESA                                                                                 AS EMPRESA,
                    D.NOME_FANTASIA                                                                           AS NOME_EMPRESA,
                    A.ENTIDADE                                                                                AS ENTIDADE,
                    E.NOME_FANTASIA                                                                           AS NOME_EMITENTE,
                    A.NF_FATURAMENTO_DEVOLUCAO                                                                AS NF_COMPRA,
                    C.TOTAL_GERAL                                                                             AS TOTAL_GERAL,
                    B.ICMS_BASE_CALCULO                                                                       AS BASE_CALCULO_ICMS,
                    B.ICMS_ALIQUOTA                                                                           AS AL�QUOTA_ICMS,
                    B.ICMS_VALOR                                                                              AS IMPOSTO_CRED,
                    B.ICMS_ST_BASE_CALCULO                                                                    AS BASE_CALCULO_ST,
                    B.ICMS_ST_VALOR                                                                           AS VALOR_ICMS_ST,
                    B.PIS_ALIQUOTA                                                                            AS PIS_ALIQUOTA,
                    B.PIS_VALOR                                                                               AS PIS_VALOR,
                    B.COFINS_ALIQUOTA                                                                         AS COFINS_ALIQUOTA,
                    B.COFINS_VALOR                                                                            AS COFINS_VALOR,
                    'Per�odo de '+CONVERT(VARCHAR, @DATA_INI, 103 )+' a '+ CONVERT (VARCHAR, @DATA_FIM, 103)  AS PERIODO,
                    'NF_FATURAMENTO_DEVOLUCOES'                                                               AS FORMULARIO,
                    177982                                                                                    AS FORMULARIO_ID,
                    B.ESTADO_NOTA                                                                             AS ESTADO_NOTA ,
                    A.MOVIMENTO                                                                               AS DATA_RECEBIMENTO
            
              FROM NF_FATURAMENTO_DEVOLUCOES          A WITH(NOLOCK)
              
              JOIN (
                             SELECT 
                             A.NF_FATURAMENTO_DEVOLUCAO,
                             'NF_FATURAMENTO_DEVOLUCOES' AS FORMULARIO           ,
                             'Autorizada'                AS ESTADO_NOTA          ,
                             SUM(A.ICMS_BASE_CALCULO   ) AS ICMS_BASE_CALCULO    ,
                             MAX(A.ICMS_ALIQUOTA       ) AS ICMS_ALIQUOTA        ,
                             SUM(A.ICMS_VALOR          ) AS ICMS_VALOR           ,
                             SUM(A.ICMS_ST_BASE_CALCULO) AS ICMS_ST_BASE_CALCULO ,
                             MAX(A.ICMS_ST_ALIQUOTA    ) AS ICMS_ST_ALIQUOTA     ,
                             SUM(A.ICMS_ST_VALOR       ) AS ICMS_ST_VALOR        ,
                             MAX(A.PIS_ALIQUOTA        ) AS PIS_ALIQUOTA         ,
                             SUM(A.PIS_VALOR           ) AS PIS_VALOR            ,
                             MAX(A.COFINS_ALIQUOTA     ) AS COFINS_ALIQUOTA      ,
                             SUM(A.COFINS_VALOR        ) AS COFINS_VALOR
                
                               FROM NF_FATURAMENTO_DEVOLUCOES_PRODUTOS A WITH(NOLOCK)
                               JOIN NF_FATURAMENTO_DEVOLUCOES          B WITH(NOLOCK) ON B.NF_FATURAMENTO_DEVOLUCAO       = A.NF_FATURAMENTO_DEVOLUCAO
                               JOIN OPERACOES_FISCAIS                  C WITH(NOLOCK) ON C.OPERACAO_FISCAL = A.OPERACAO_FISCAL
                              WHERE 1=1
                                    AND B.NOTA_EMITIDA_CLIENTE = 'S'

                           GROUP BY A.NF_FATURAMENTO_DEVOLUCAO                             
                                        
                   )                                     B              ON B.NF_FATURAMENTO_DEVOLUCAO  = A.NF_FATURAMENTO_DEVOLUCAO
              JOIN NF_FATURAMENTO_DEVOLUCOES_TOTAIS      C WITH(NOLOCK) ON C.NF_FATURAMENTO_DEVOLUCAO  = A.NF_FATURAMENTO_DEVOLUCAO
              JOIN ENTIDADES                E WITH(NOLOCK) ON E.ENTIDADE   = A.ENTIDADE
              JOIN ENTIDADES                D WITH(NOLOCK) ON D.ENTIDADE   = A.EMPRESA
              LEFT 
              JOIN NFE_CABECALHO            G WITH(NOLOCK) ON G.CHAVE_NFE  = A.CHAVE_NFE

              
              WHERE 1=1
              AND ( A.MOVIMENTO   >= @DATA_INI                OR @DATA_INI              IS NULL )
              AND ( A.MOVIMENTO   <= @DATA_FIM                OR @DATA_FIM              IS NULL )
              AND ( A.EMPRESA     >= @EMPRESA_INI             OR @EMPRESA_INI           IS NULL )
              AND ( A.EMPRESA     <= @EMPRESA_FIM             OR @EMPRESA_FIM           IS NULL )
              AND ( A.ENTIDADE     = @EMITENTE                OR @EMITENTE              IS NULL )
              AND ( @FORMULARIO_PROCESSO    = 177982          OR @FORMULARIO_PROCESSO   IS NULL )

UNION ALL


           SELECT 
                    CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                             AS MOVIMENTO,
                    B.NF_NUMERO                                                                               AS NUMERO,
                    B.SERIE                                                                                   AS SERIE,
                    CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                             AS EMISSAO,
                    A.EMPRESA                                                                                 AS EMPRESA,
                    D.NOME_FANTASIA                                                                           AS NOME_EMPRESA,
                    ISNULL(E.ENTIDADE,0)                                                                      AS ENTIDADE,
                    B.NOME_DESTINATARIO+' - '+B.CPF_CNPJ_DESTINATARIO                                         AS NOME_EMITENTE,
                    B.REG_NUM                                                                                 AS REG_NUM,
                    B.TOTAL_LIQUIDO                                                                           AS TOTAL_GERAL,
                    B.ICMS_BASE_CALCULO                                                                       AS BASE_CALCULO_ICMS,
                    B.ICMS_ALIQUOTA                                                                           AS AL�QUOTA_ICMS,
                    B.ICMS_VALOR                                                                              AS IMPOSTO_CRED,
                    B.ICMS_ST_BASE_CALCULO                                                                    AS BASE_CALCULO_ST,
                    B.ICMS_ST_VALOR                                                                           AS VALOR_ICMS_ST,
                    B.PIS_ALIQUOTA                                                                            AS PIS_ALIQUOTA,
                    B.PIS_VALOR                                                                               AS PIS_VALOR,
                    B.COFINS_ALIQUOTA                                                                         AS COFINS_ALIQUOTA,
                    B.COFINS_VALOR                                                                            AS COFINS_VALOR,
                    'Per�odo de '+CONVERT(VARCHAR, @DATA_INI, 103 )+' a '+ CONVERT (VARCHAR, @DATA_FIM, 103)  AS PERIODO,
                    B.NOME_FORMULARIO,
                    G.NUMID                                                                                   AS FORMULARIO_ID,
                    B.ESTADO_NOTA,
                    F.MOVIMENTO                                                                               AS DATA_RECEBIMENTO

            
              FROM NFE_CABECALHO          A WITH(NOLOCK)
              JOIN (
                            SELECT 
                                    A.REG_MASTER_ORIGEM                                                                        AS REG_NUM,
                                    A.NOME_FORMULARIO                                                                          AS FORMULARIO,
                                    CASE WHEN B.REGISTRO_NFE          IS NULL                                    THEN 'N�o Autorizada' 
                                         WHEN C.NF_ESTADO             IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL  THEN 'Cancelada'
                                         WHEN D.NFE_INUTILIZACAO_NOTA IS NOT NULL                                THEN 'Inutilizada'
                                                                                                                 ELSE 'Autorizada' 
                                    END                                                                                        AS ESTADO_NOTA,
                                    CASE WHEN C.NF_ESTADO IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL 
                                          THEN 0.00
                                          ELSE F.ICMS_BASE_CALCULO   
                                    END                                                                                      AS ICMS_BASE_CALCULO,
                                    0.00                                                                                          AS ICMS_ALIQUOTA,
                                    CASE WHEN C.NF_ESTADO IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL 
                                          THEN 0.00
                                          ELSE F.ICMS_VALOR
                                    END                                                                                      AS ICMS_VALOR,
                                    CASE WHEN C.NF_ESTADO IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL 
                                          THEN 0.00
                                          ELSE F.ICMS_ST_BASE 
                                    END                                                                                      AS ICMS_ST_BASE_CALCULO,
                                    0.00                                                                                          AS ICMS_ST_ALIQUOTA,
                                    CASE WHEN C.NF_ESTADO IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL 
                                          THEN 0.00
                                          ELSE F.ICMS_ST_VALOR
                                    END                                                                                      AS ICMS_ST_VALOR,
                                    0.00                                                                                          AS PIS_ALIQUOTA,
                                    CASE WHEN C.NF_ESTADO IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL 
                                          THEN 0.00
                                          ELSE F.TOTAL_PIS
                                    END                                                                                      AS PIS_VALOR,
                                    0.00                                                                                          AS COFINS_ALIQUOTA,
                                    CASE WHEN C.NF_ESTADO IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL 
                                          THEN 0.00
                                          ELSE F.TOTAL_COFINS
                                    END                                                                                      AS COFINS_VALOR,
                                    A.XML_nNF_B08                                                                              AS NF_NUMERO,
                                    A.XML_serie_B07                                                                            AS SERIE,
                                    A.XML_natOp_B04                                                                            AS OPERACAO_FISCAL_DESCRICAO,
                                    DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_C02)                                                        AS CNPJ_EMITENTE,
                                    A.XML_xFant_C04                                                                            AS NOME_FANTASIA,
                                    ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))              AS CPF_CNPJ_DESTINATARIO,
                                    UPPER(A.XML_XNOME_E04)                                                                     AS NOME_DESTINATARIO,
                                    A.XML_UF_E12                                                                               AS UF_DESTINATARIO,
                                    A.NOME_FORMULARIO                                                                          AS NOME_FORMULARIO,
                                    A.CHAVE_NFE                                                                                AS CHAVE_NFE,
                                    F.TOTAL_BRUTO,
                                    F.TOTAL_DESCONTOS,
                                    CASE WHEN B.REGISTRO_NFE          IS NULL                                    THEN 0.00 --'N�o Autorizada' 
                                         WHEN C.NF_ESTADO             IS NOT NULL OR G.REGISTRO_NFE IS NOT NULL  THEN 0.00 --'Cancelada'
                                         WHEN D.NFE_INUTILIZACAO_NOTA IS NOT NULL                                THEN 0.00 --'Inutilizada'
                                                                                                                 ELSE F.TOTAL_LIQUIDO
                                    END                                                                                        AS TOTAL_LIQUIDO,
                                    A.REGISTRO_NFE
                             
                              FROM NFE_CABECALHO            A WITH(NOLOCK)
                                LEFT                            
                              JOIN NFE_LOG                  B WITH(NOLOCK) ON A.REGISTRO_NFE      = B.REGISTRO_NFE
                                                                            AND B.STATUS            = 'XML DO DESTINATARIO'
                                LEFT                            
                              JOIN NOTAS_FISCAIS_ESTADO     C WITH(NOLOCK) ON C.NF_NUMERO         = A.XML_nNF_B08
                                                                            AND C.NF_SERIE          = A.XML_serie_B07
                                                                            AND C.TIPO              = 1
                                                                            AND C.ESTADO            = 2
                                                                            AND C.CHAVE             = A.REG_MASTER_ORIGEM 
                                LEFT
                              JOIN NFE_INUTILIZACOES_NOTAS  D WITH(NOLOCK) ON D.EMPRESA            = A.EMPRESA
                                                                            AND D.NF_NUMERO_INICIAL >= A.XML_nNF_B08
                                                                          AND D.NF_NUMERO_FINAL   <= A.XML_nNF_B08
                                JOIN (
                                       SELECT DISTINCT
                                       REGISTRO_NFE
                                         FROM NFE_ITENS WITH(NOLOCK)
                                       WHERE SUBSTRING ( XML_CFOP_I08,1,1 ) IN ( 1,2,3 )
                                     )                        E              ON E.REGISTRO_NFE       = A.REGISTRO_NFE
                                JOIN (
                                          SELECT 
                                          REGISTRO_NFE    AS REGISTRO_NFE  ,
                                          XML_vBC_W03     AS ICMS_BASE_CALCULO,
                                          XML_vICMS_W04   AS ICMS_VALOR ,
                                          XML_vBCST_W05   AS ICMS_ST_BASE ,
                                          XML_vST_W06     AS ICMS_ST_VALOR   ,
                                          XML_vProd_W07   AS TOTAL_BRUTO ,
                                          XML_vDesc_W10   AS TOTAL_DESCONTOS ,
                                          XML_vNF_W16     AS TOTAL_LIQUIDO,
                                          XML_vIPI_W12    AS TOTAL_IPI,
                                          XML_vPIS_W13    AS TOTAL_PIS,
                                          XML_vCOFINS_W14    AS TOTAL_COFINS                          
                                            FROM NFE_RODAPE WITH(NOLOCK)
                                     )                        F              ON F.REGISTRO_NFE       = A.REGISTRO_NFE
                               LEFT                            
                              JOIN NFE_CANCELAMENTOS        G WITH(NOLOCK)   ON G.REGISTRO_NFE          = A.REGISTRO_NFE  
                                                                            AND G.STATUS = 4                            



              
              WHERE 1=1
              AND ( CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)   >= @DATA_INI    OR @DATA_INI    IS NULL )
              AND ( CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)   <= @DATA_FIM    OR @DATA_FIM    IS NULL )
              AND ( A.EMPRESA                                       >= @EMPRESA_INI OR @EMPRESA_INI IS NULL )
              AND ( A.EMPRESA                                       <= @EMPRESA_FIM OR @EMPRESA_FIM IS NULL )   

                
                   )                  B              ON B.REGISTRO_NFE  = A.REGISTRO_NFE
              JOIN ENTIDADES          D WITH(NOLOCK) ON D.ENTIDADE      = A.EMPRESA
              LEFT 
              JOIN #TEMP_ENTIDADES    E              ON DBO.NUMERICO(E.INSCRICAO_FEDERAL) = DBO.NUMERICO(B.CPF_CNPJ_DESTINATARIO)
              LEFT
              JOIN FORMULARIOS_LOCKUP G WITH(NOLOCK) ON G.FORMULARIO    = B.NOME_FORMULARIO
              LEFT 
              JOIN RECEBIMENTOS_MOVIMENTOS  F WITH(NOLOCK) ON F.NF_NUMERO  = B.NF_NUMERO
                                                          AND F.NF_SERIE   = B.SERIE
                                                          AND F.ENTIDADE   = E.ENTIDADE

              WHERE 1=1
              AND ( E.ENTIDADE     = @EMITENTE                OR @EMITENTE              IS NULL )
              AND ( G.NUMID        = @FORMULARIO_PROCESSO     OR @FORMULARIO_PROCESSO   IS NULL )
              AND ( F.MOVIMENTO   >= @DATA_RECEBIMENTO_INI    OR @DATA_RECEBIMENTO_INI  IS NULL )
              AND ( F.MOVIMENTO   <= @DATA_RECEBIMENTO_FIM    OR @DATA_RECEBIMENTO_FIM  IS NULL )
                
UNION ALL

           SELECT 
                    CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                             AS MOVIMENTO,
                    B.NF_NUMERO                                                                               AS NUMERO,
                    B.SERIE                                                                                   AS SERIE,
                    CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                             AS EMISSAO,
                    B.ENTIDADE                                                                                AS EMPRESA,
                    B.NOME_FANTASIA                                                                           AS NOME_EMPRESA,
                    A.EMPRESA                                                                                 AS ENTIDADE,
                    B.NOME_FANTASIA_2+' - '+ DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_C02)                              AS NOME_EMITENTE,
                    B.REG_NUM                                                                                 AS REG_NUM,
                    B.TOTAL_LIQUIDO                                                                           AS TOTAL_GERAL,
                    B.ICMS_BASE_CALCULO                                                                       AS BASE_CALCULO_ICMS,
                    B.ICMS_ALIQUOTA                                                                           AS AL�QUOTA_ICMS,
                    B.ICMS_VALOR                                                                              AS IMPOSTO_CRED,
                    B.ICMS_ST_BASE_CALCULO                                                                    AS BASE_CALCULO_ST,
                    B.ICMS_ST_VALOR                                                                           AS VALOR_ICMS_ST,
                    B.PIS_ALIQUOTA                                                                            AS PIS_ALIQUOTA,
                    B.PIS_VALOR                                                                               AS PIS_VALOR,
                    B.COFINS_ALIQUOTA                                                                         AS COFINS_ALIQUOTA,
                    B.COFINS_VALOR                                                                            AS COFINS_VALOR,
                    'Per�odo de '+CONVERT(VARCHAR, @DATA_INI, 103 )+' a '+ CONVERT (VARCHAR, @DATA_FIM, 103)  AS PERIODO,
                    'NF_COMPRA'                                                                               AS NOME_FORMULARIO,
                    769791                                                                                    AS FORMULARIO_ID,
                    B.ESTADO_NOTA,
                    F.MOVIMENTO AS DATA_RECEBIMENTO

            
              FROM NFE_CABECALHO          A WITH(NOLOCK)
              JOIN (

                             SELECT
                                    A.REG_MASTER_ORIGEM                                                                        AS REG_NUM,
                                    A.NOME_FORMULARIO                                                                          AS FORMULARIO,
                                    CASE WHEN B.REGISTRO_NFE          IS NULL     THEN 'N�o Autorizada' 
                                         WHEN C.NF_ESTADO             IS NOT NULL THEN 'Cancelada'
                                         WHEN D.NFE_INUTILIZACAO_NOTA IS NOT NULL THEN 'Inutilizada'
                                                                                  ELSE 'Autorizada' 
                                      END                                                                                      AS ESTADO_NOTA,
                                   CASE WHEN C.ESTADO IS NULL
                                        THEN F.ICMS_BASE_CALCULO
                                        ELSE 0.00
                                   END                                                                                         AS ICMS_BASE_CALCULO,
                                    0.00                                                                                          AS ICMS_ALIQUOTA,
                                     CASE WHEN C.ESTADO IS NULL
                                        THEN F.ICMS_VALOR 
                                        ELSE 0.00
                                     END                                                                                       AS ICMS_VALOR,
                                    CASE WHEN C.ESTADO IS NULL
                                        THEN F.ICMS_ST_BASE
                                        ELSE 0.00
                                    END                                                                                        AS ICMS_ST_BASE_CALCULO,
                                    0.00                                                                                       AS ICMS_ST_ALIQUOTA,
                                     CASE WHEN C.ESTADO IS NULL
                                        THEN F.ICMS_ST_VALOR
                                        ELSE 0.00  
                                      END                                                                                      AS ICMS_ST_VALOR,
                                    0.00                                                                                       AS PIS_ALIQUOTA,
                                     CASE WHEN C.ESTADO IS NULL
                                        THEN F.TOTAL_PIS
                                        ELSE 0.00
                                      END                                                                                      AS PIS_VALOR,
                                    0.00                                                                                       AS COFINS_ALIQUOTA,
                                    CASE WHEN C.ESTADO IS NULL
                                        THEN F.TOTAL_COFINS 
                                        ELSE 0.00 
                                     END                                                                                       AS COFINS_VALOR,
                                    A.XML_nNF_B08                                                                              AS NF_NUMERO,
                                    A.XML_serie_B07                                                                            AS SERIE,
                                    A.XML_natOp_B04                                                                            AS OPERACAO_FISCAL_DESCRICAO,
                                    DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_C02)                                                        AS CNPJ_EMITENTE,
                                    A.XML_xFant_C04                                                                            AS NOME_FANTASIA_2,
                                    ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))              AS CPF_CNPJ_DESTINATARIO,
                                    UPPER(A.XML_XNOME_E04)                                                                     AS NOME_DESTINATARIO,
                                    A.XML_UF_E12                                                                               AS UF_DESTINATARIO,
                                    A.NOME_FORMULARIO                                                                          AS NOME_FORMULARIO,
                                    A.CHAVE_NFE                                                                                AS CHAVE_NFE,
                                    F.TOTAL_BRUTO,
                                    F.TOTAL_DESCONTOS,
                                    CASE WHEN B.REGISTRO_NFE          IS NULL     THEN 0.00 
                                         WHEN C.NF_ESTADO             IS NOT NULL THEN 0.00
                                         WHEN D.NFE_INUTILIZACAO_NOTA IS NOT NULL THEN 0.00
                                                                                  ELSE F.TOTAL_LIQUIDO 
                                    END                                                                                       AS TOTAL_LIQUIDO,
                                    A.REGISTRO_NFE,
                                    G.ENTIDADE,
                                    G.NOME_FANTASIA,
                                    G.INSCRICAO_FEDERAL
                             
                              FROM NFE_CABECALHO            A WITH(NOLOCK)
                                LEFT                            
                              JOIN NFE_LOG                  B WITH(NOLOCK) ON A.REGISTRO_NFE      = B.REGISTRO_NFE
                                                                            AND B.STATUS            = 'XML DO DESTINATARIO'
                                LEFT                            
                              JOIN NOTAS_FISCAIS_ESTADO     C WITH(NOLOCK) ON C.NF_NUMERO         = A.XML_nNF_B08
                                                                            AND C.NF_SERIE          = A.XML_serie_B07
                                                                            AND C.TIPO              = 1
                                                                            AND C.ESTADO            = 2 
                                                                            AND C.CHAVE             = A.REG_MASTER_ORIGEM                                                                               
                                LEFT
                              JOIN NFE_INUTILIZACOES_NOTAS  D WITH(NOLOCK) ON D.EMPRESA            = A.EMPRESA
                                                                            AND D.NF_NUMERO_INICIAL >= A.XML_nNF_B08
                                                                          AND D.NF_NUMERO_FINAL   <= A.XML_nNF_B08
                                JOIN (
                                       SELECT DISTINCT
                                       REGISTRO_NFE
                                         FROM NFE_ITENS WITH(NOLOCK)
                                       WHERE SUBSTRING ( XML_CFOP_I08,1,1 ) NOT IN ( 1,2,3 )
                                     )                        E              ON E.REGISTRO_NFE       = A.REGISTRO_NFE
                                JOIN (
                                          SELECT 
                                          REGISTRO_NFE    AS REGISTRO_NFE  ,
                                          XML_vBC_W03     AS ICMS_BASE_CALCULO,
                                          XML_vICMS_W04   AS ICMS_VALOR ,
                                          XML_vBCST_W05   AS ICMS_ST_BASE ,
                                          XML_vST_W06     AS ICMS_ST_VALOR   ,
                                          XML_vProd_W07   AS TOTAL_BRUTO ,
                                          XML_vDesc_W10   AS TOTAL_DESCONTOS ,
                                          XML_vNF_W16     AS TOTAL_LIQUIDO,
                                          XML_vIPI_W12    AS TOTAL_IPI,
                                          XML_vPIS_W13    AS TOTAL_PIS,
                                          XML_vCOFINS_W14 AS TOTAL_COFINS                          
                                          FROM NFE_RODAPE WITH(NOLOCK)
                                     )                        F              ON F.REGISTRO_NFE       = A.REGISTRO_NFE
                                JOIN ENTIDADES                G WITH(NOLOCK) ON G.INSCRICAO_FEDERAL  = ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))
                                LEFT
                                JOIN NF_COMPRA                H WITH(NOLOCK) ON H.NF_FATURAMENTO     = A.REG_MASTER_ORIGEM
              
              WHERE 1=1
              AND ( ISNULL( ISNULL(H.DATA_RECEBIMENTO,H.MOVIMENTO), CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE) )   >= @DATA_INI                           OR @DATA_INI                           IS NULL )
              AND ( ISNULL( ISNULL(H.DATA_RECEBIMENTO,H.MOVIMENTO), CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE) )   <= @DATA_FIM                           OR @DATA_FIM                           IS NULL )
              AND ( ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))                     >= @INSCRICAO_FEDERAL_DESTINATARIO_INI OR @INSCRICAO_FEDERAL_DESTINATARIO_INI IS NULL )
              AND ( ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))                     <= @INSCRICAO_FEDERAL_DESTINATARIO_FIM OR @INSCRICAO_FEDERAL_DESTINATARIO_FIM IS NULL )
              
              AND G.ENTIDADE < 1000

              AND H.NF_COMPRA IS NULL

                                      )                  B              ON B.REGISTRO_NFE  = A.REGISTRO_NFE
                                    JOIN ENTIDADES       D WITH(NOLOCK) ON D.ENTIDADE      = A.EMPRESA
              LEFT 
              JOIN RECEBIMENTOS_MOVIMENTOS  F WITH(NOLOCK) ON F.NF_NUMERO  = B.NF_NUMERO
                                                          AND F.NF_SERIE   = B.SERIE
                                                          AND F.ENTIDADE   = D.ENTIDADE
              LEFT 
              JOIN EMPRESAS_USUARIAS        G WITH(NOLOCK) ON G.EMPRESA_USUARIA = A.EMPRESA
              LEFT 
              JOIN EMPRESAS_USUARIAS        H WITH(NOLOCK) ON H.EMPRESA_USUARIA = B.ENTIDADE

              WHERE 1=1
              AND G.EMPRESA_CONTABIL       <> H.EMPRESA_CONTABIL --  SOMENTE COMPRAS
              AND ( B.ENTIDADE              = @EMITENTE                OR @EMITENTE              IS NULL )
              AND ( @FORMULARIO_PROCESSO    = 769791                   OR @FORMULARIO_PROCESSO   IS NULL )
              AND ( F.MOVIMENTO            >= @DATA_RECEBIMENTO_INI    OR @DATA_RECEBIMENTO_INI  IS NULL )
              AND ( F.MOVIMENTO            <= @DATA_RECEBIMENTO_FIM    OR @DATA_RECEBIMENTO_FIM  IS NULL ) 
        
             
UNION ALL
               --Creio estar aqui
                    
           SELECT 
                    CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                             AS MOVIMENTO,
                    B.NF_NUMERO                                                                               AS NUMERO,
                    B.SERIE                                                                                   AS SERIE,
                    CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                             AS EMISSAO,
                    B.ENTIDADE                                                                                AS EMPRESA,
                    B.NOME_FANTASIA                                                                           AS NOME_EMPRESA,
                    A.EMPRESA                                                                                 AS ENTIDADE,
                    B.NOME_FANTASIA_2+' - '+ DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_C02)                              AS NOME_EMITENTE,
                    B.REG_NUM                                                                                 AS REG_NUM,
                    B.TOTAL_LIQUIDO                                                                           AS TOTAL_GERAL,
                    B.ICMS_BASE_CALCULO                                                                       AS BASE_CALCULO_ICMS,
                    B.ICMS_ALIQUOTA                                                                           AS AL�QUOTA_ICMS,
                    B.ICMS_VALOR                                                                              AS IMPOSTO_CRED,
                    B.ICMS_ST_BASE_CALCULO                                                                    AS BASE_CALCULO_ST,
                    B.ICMS_ST_VALOR                                                                           AS VALOR_ICMS_ST,
                    B.PIS_ALIQUOTA                                                                            AS PIS_ALIQUOTA,
                    B.PIS_VALOR                                                                               AS PIS_VALOR,
                    B.COFINS_ALIQUOTA                                                                         AS COFINS_ALIQUOTA,
                    B.COFINS_VALOR                                                                            AS COFINS_VALOR,
                    'Per�odo de '+CONVERT(VARCHAR, @DATA_INI, 103 )+' a '+ CONVERT (VARCHAR, @DATA_FIM, 103)  AS PERIODO,
                    'TRANSFERENCIA'                                                                           AS NOME_FORMULARIO,
                    769791                                                                                    AS FORMULARIO_ID,
                    B.ESTADO_NOTA,
                    F.MOVIMENTO AS DATA_RECEBIMENTO

            
              FROM NFE_CABECALHO          A WITH(NOLOCK)
              JOIN (

                             SELECT
                                    A.REG_MASTER_ORIGEM                                                                        AS REG_NUM,
                                    A.NOME_FORMULARIO                                                                          AS FORMULARIO,
                                    CASE WHEN B.REGISTRO_NFE          IS NULL     THEN 'N�o Autorizada' 
                                         WHEN C.NF_ESTADO             IS NOT NULL THEN 'Cancelada'
                                         WHEN D.NFE_INUTILIZACAO_NOTA IS NOT NULL THEN 'Inutilizada'
                                                                                  ELSE 'Autorizada' 
                                      END                                                                                      AS ESTADO_NOTA,
                                    CASE WHEN C.ESTADO IS NULL
                                         THEN F.ICMS_BASE_CALCULO
                                         ELSE 0.00
                                     END                                                                                       AS ICMS_BASE_CALCULO,
                                    0                                                                                          AS ICMS_ALIQUOTA,
                                    CASE WHEN C.ESTADO IS NULL
                                         THEN F.ICMS_VALOR 
                                         ELSE 0.00
                                     END                                                                                       AS ICMS_VALOR,
                                     CASE WHEN C.ESTADO IS NULL
                                          THEN F.ICMS_ST_BASE 
                                          ELSE 0.00
                                      END                                                                                      AS ICMS_ST_BASE_CALCULO,
                                    0                                                                                          AS ICMS_ST_ALIQUOTA,
                                     CASE WHEN C.ESTADO IS NULL
                                          THEN F.ICMS_ST_VALOR
                                          ELSE 0.00
                                      END                                                                                      AS ICMS_ST_VALOR,
                                    0                                                                                          AS PIS_ALIQUOTA,
                                     CASE WHEN C.ESTADO IS NULL
                                          THEN F.TOTAL_PIS
                                          ELSE 0.00
                                      END                                                                                      AS PIS_VALOR,
                                    0                                                                                          AS COFINS_ALIQUOTA,
                                     CASE WHEN C.ESTADO IS NULL
                                          THEN F.TOTAL_COFINS
                                          ELSE 0.00
                                      END                                                                                      AS COFINS_VALOR,
                                    A.XML_nNF_B08                                                                              AS NF_NUMERO,
                                    A.XML_serie_B07                                                                            AS SERIE,
                                    A.XML_natOp_B04                                                                            AS OPERACAO_FISCAL_DESCRICAO,
                                    DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_C02)                                                        AS CNPJ_EMITENTE,
                                    A.XML_xFant_C04                                                                            AS NOME_FANTASIA_2,
                                    ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))              AS CPF_CNPJ_DESTINATARIO,
                                    UPPER(A.XML_XNOME_E04)                                                                     AS NOME_DESTINATARIO,
                                    A.XML_UF_E12                                                                               AS UF_DESTINATARIO,
                                    A.NOME_FORMULARIO                                                                          AS NOME_FORMULARIO,
                                    A.CHAVE_NFE                                                                                AS CHAVE_NFE,
                                    F.TOTAL_BRUTO,
                                    F.TOTAL_DESCONTOS,
                                    CASE WHEN B.REGISTRO_NFE          IS NULL     THEN 0.00 
                                         WHEN C.NF_ESTADO             IS NOT NULL THEN 0.00
                                         WHEN D.NFE_INUTILIZACAO_NOTA IS NOT NULL THEN 0.00
                                                                                  ELSE F.TOTAL_LIQUIDO
                                    END                                                                                        AS TOTAL_LIQUIDO,
                                    A.REGISTRO_NFE,
                                    G.ENTIDADE,
                                    G.NOME_FANTASIA,
                                    G.INSCRICAO_FEDERAL
                             
                              FROM NFE_CABECALHO            A WITH(NOLOCK)
                                LEFT                            
                              JOIN NFE_LOG                  B WITH(NOLOCK) ON A.REGISTRO_NFE      = B.REGISTRO_NFE
                                                                            AND B.STATUS            = 'XML DO DESTINATARIO'
                                LEFT                            
                              JOIN NOTAS_FISCAIS_ESTADO     C WITH(NOLOCK) ON C.NF_NUMERO         = A.XML_nNF_B08
                                                                            AND C.NF_SERIE          = A.XML_serie_B07
                                                                            AND C.TIPO              = 1
                                                                            AND C.ESTADO            = 2    
                                                                            AND C.CHAVE             = A.REG_MASTER_ORIGEM
                                LEFT
                              JOIN NFE_INUTILIZACOES_NOTAS  D WITH(NOLOCK) ON D.EMPRESA            = A.EMPRESA
                                                                            AND D.NF_NUMERO_INICIAL >= A.XML_nNF_B08
                                                                          AND D.NF_NUMERO_FINAL   <= A.XML_nNF_B08
                                JOIN (
                                       SELECT DISTINCT
                                       REGISTRO_NFE
                                         FROM NFE_ITENS WITH(NOLOCK)
                                       WHERE SUBSTRING ( XML_CFOP_I08,1,1 ) NOT IN ( 1,2,3 )
                                     )                        E              ON E.REGISTRO_NFE       = A.REGISTRO_NFE
                                JOIN (
                                          SELECT 
                                          REGISTRO_NFE    AS REGISTRO_NFE  ,
                                          XML_vBC_W03     AS ICMS_BASE_CALCULO,
                                          XML_vICMS_W04   AS ICMS_VALOR ,
                                          XML_vBCST_W05   AS ICMS_ST_BASE ,
                                          XML_vST_W06     AS ICMS_ST_VALOR   ,
                                          XML_vProd_W07   AS TOTAL_BRUTO ,
                                          XML_vDesc_W10   AS TOTAL_DESCONTOS ,
                                          XML_vNF_W16     AS TOTAL_LIQUIDO,
                                          XML_vIPI_W12    AS TOTAL_IPI,
                                          XML_vPIS_W13    AS TOTAL_PIS,
                                          XML_vCOFINS_W14 AS TOTAL_COFINS                          
                                          FROM NFE_RODAPE WITH(NOLOCK)
                                     )                        F              ON F.REGISTRO_NFE       = A.REGISTRO_NFE
                                JOIN ENTIDADES                G WITH(NOLOCK) ON G.INSCRICAO_FEDERAL  = ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))



              
              WHERE 1=1
              AND ( CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                  >= @DATA_INI                           OR @DATA_INI                           IS NULL )
              AND ( CAST(SUBSTRING(A.XML_dhEmi_B09,1,10) AS DATE)                                  <= @DATA_FIM                           OR @DATA_FIM                           IS NULL )
              AND ( ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))  >= @INSCRICAO_FEDERAL_DESTINATARIO_INI OR @INSCRICAO_FEDERAL_DESTINATARIO_INI IS NULL )
              AND ( ISNULL(DBO.FN_MASCARA_CNPJ(A.XML_CNPJ_E02),DBO.FN_MASCARA_CPF(A.XML_CPF_E03))  <= @INSCRICAO_FEDERAL_DESTINATARIO_FIM OR @INSCRICAO_FEDERAL_DESTINATARIO_FIM IS NULL )

              AND G.ENTIDADE <= 1000
              AND A.EMPRESA <> G.ENTIDADE
                                      )                  B              ON B.REGISTRO_NFE  = A.REGISTRO_NFE
                                    JOIN ENTIDADES       D WITH(NOLOCK) ON D.ENTIDADE      = A.EMPRESA
              LEFT 
              JOIN RECEBIMENTOS_MOVIMENTOS  F WITH(NOLOCK) ON F.NF_NUMERO  = B.NF_NUMERO
                                                          AND F.NF_SERIE   = B.SERIE
                                                          AND F.ENTIDADE   = D.ENTIDADE
              LEFT 
              JOIN EMPRESAS_USUARIAS        G WITH(NOLOCK) ON G.EMPRESA_USUARIA = A.EMPRESA
              LEFT 
              JOIN EMPRESAS_USUARIAS        H WITH(NOLOCK) ON H.EMPRESA_USUARIA = B.ENTIDADE
                                                           
              WHERE 1=1
              AND G.EMPRESA_CONTABIL        = H.EMPRESA_CONTABIL --  SOMENTE TRANSFERENCIAS
              AND ( B.ENTIDADE              = @EMITENTE                OR @EMITENTE              IS NULL )
              AND ( @FORMULARIO_PROCESSO    = 769791                   OR @FORMULARIO_PROCESSO   IS NULL )
              AND ( F.MOVIMENTO            >= @DATA_RECEBIMENTO_INI    OR @DATA_RECEBIMENTO_INI  IS NULL )
              AND ( F.MOVIMENTO            <= @DATA_RECEBIMENTO_FIM    OR @DATA_RECEBIMENTO_FIM  IS NULL ) 
              


ORDER BY A.EMPRESA,  FORMULARIO, MOVIMENTO, NUMERO