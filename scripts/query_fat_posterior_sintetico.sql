 DECLARE @ENTIDADE                   NUMERIC(15,0)
 DECLARE @VENDEDOR                   NUMERIC(15,0)        
 DECLARE @CUSTO_BASE_FATURAMENTO     NUMERIC(15,0) 

 
            
DECLARE @MOSTRAR_OBSERVACAO   VARCHAR(1)   = :MOSTRAR_OBSERVACAO 
DECLARE @DATA_INICIAL   DATETIME    = CASE WHEN (:DATA_INICIAL = '30/12/1899' OR :DATA_INICIAL ='') THEN '01/01/1900'    ELSE :DATA_INICIAL     END
DECLARE @DATA_FINAL     DATETIME    = CASE WHEN (:DATA_FINAL   = '30/12/1899' OR :DATA_FINAL ='')   THEN '31/12/2999'    ELSE :DATA_FINAL       END  
IF ISNUMERIC(:ENTIDADE)                                        = 1 SET @ENTIDADE             =  :ENTIDADE            ELSE SET @ENTIDADE           = NULL
IF ISNUMERIC(:VENDEDOR)                                        = 1 SET @VENDEDOR             =  :VENDEDOR            ELSE SET @VENDEDOR           = NULL
IF ISNUMERIC(:VENDEDOR)                                        = 1 SET @VENDEDOR             =  :VENDEDOR            ELSE SET @VENDEDOR           = NULL
IF ISNUMERIC(:CUSTO_BASE_FATURAMENTO)                          = 1 SET @CUSTO_BASE_FATURAMENTO =  :CUSTO_BASE_FATURAMENTO  ELSE SET @CUSTO_BASE_FATURAMENTO   = NULL


--DECLARE @DATA_INICIAL   DATETIME  = '01/01/2017'
--DECLARE @DATA_FINAL     DATETIME  = '31/12/2018'
--DECLARE @MOSTRAR_OBSERVACAO   VARCHAR(1)   = 's'
--SET @ENTIDADE      = 96159 
----SET @CUSTO_BASE_FATURAMENTO = 1



    SELECT 
            A.APROVACAO_FATURAMENTO_FUTURO                                        AS DOCUMENTO             ,
            A.DATA_HORA                                                           AS DATA_HORA             ,
            CAST(O.USUARIO  AS VARCHAR) + ' - '+ O.NOME                           AS APROVADOR             ,
            CAST(M.VENDEDOR AS VARCHAR) /*+ ' - '+ N.NOME*/                       AS VENDEDOR              ,
            CAST(E.ENTIDADE AS VARCHAR) + ' - '+ E.NOME                           AS CLIENTE               ,
            MAX(L.PEDIDO_VENDA)                                                   AS PEDIDO_VENDA          ,
            SUM(L.VALOR_UNITARIO *
            (CASE WHEN ISNULL(L.FATOR_VENDA,1) <> 1 THEN D.CONFERENCIA / ISNULL(L.FATOR_VENDA,1) 
                  WHEN ISNULL(L.FATOR_VENDA,1) = 1 AND ISNULL(P.UNIDADE_MEDIDA,'XX') = 'UN' OR
                                                       ISNULL(P.UNIDADE_MEDIDA,'XX') = 'CPR'   THEN L.QUANTIDADE_DIGITADA ELSE D.CONFERENCIA END)
            
            
            ) 
                                                                                  AS TOTAL,
            SUM((D.CONFERENCIA*ISNULL(L.FATOR,1))                 )               AS QUANTIDADE            ,--verificar
            CASE WHEN @MOSTRAR_OBSERVACAO = 'S' THEN CAST(A.OBSERVACAO AS VARCHAR(250)) ELSE '' END
                                                                                  AS OBSERVACAO ,
			R.DESCRICAO                                                           AS CENTRO_CUSTO_FATURAMENTO              
                 
  FROM APROVACOES_FATURAMENTOS_FUTUROS          A WITH(NOLOCK)
  JOIN DEPOSITO_FATURAMENTOS_ITENS              B WITH(NOLOCK) ON B.DEPOSITO_FATURAMENTO  = A.DEPOSITO_FATURAMENTO
  JOIN ESTOQUE_TRANSFERENCIAS                   C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA
  JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES        D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA
                                                              AND D.FORMULARIO_ORIGEM     = 543062
  JOIN ENTIDADES                                E WITH(NOLOCK) ON E.ENTIDADE              = C.ENTIDADE
  JOIN ENTIDADES                                G WITH(NOLOCK) ON G.ENTIDADE              = A.EMPRESA_USUARIA
  JOIN ENDERECOS                                H WITH(NOLOCK) ON H.ENTIDADE              = A.EMPRESA_USUARIA
  LEFT
  JOIN ENDERECOS                                I WITH(NOLOCK) ON I.ENTIDADE              = C.ENTIDADE
  JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA         L WITH(NOLOCK) ON L.PEDIDO_VENDA          = C.PEDIDO_VENDA
                                                              AND L.PRODUTO               = D.PRODUTO
  JOIN PEDIDOS_VENDAS                           M WITH(NOLOCK) ON M.PEDIDO_VENDA          = C.PEDIDO_VENDA
  JOIN VENDEDORES                               N WITH(NOLOCK) ON N.VENDEDOR              = M.VENDEDOR
  JOIN USUARIOS                                 O WITH(NOLOCK) ON O.USUARIO               = A.USUARIO_LOGADO 
  LEFT 
  JOIN FATORES_APRESENTACOES                    P WITH(NOLOCK) ON P.FATOR_APRESENTACAO    = L.FATOR_APRESENTACAO
  LEFT
  JOIN ESTORNOS_FATURAMENTOS_FUTUROS            X WITH(NOLOCK) ON X.APROVACAO_FATURAMENTO_FUTURO = A.APROVACAO_FATURAMENTO_FUTURO
  LEFT
  JOIN DEPOSITO_FATURAMENTOS                    Q WITH(NOLOCK) ON Q.DEPOSITO_FATURAMENTO    = A.DEPOSITO_FATURAMENTO
  LEFT
  JOIN CENTROS_CUSTO_FATURAMENTO                R WITH(NOLOCK) ON Q.CENTRO_CUSTO_FATURAMENTO = R.CENTRO_CUSTO_FATURAMENTO

WHERE 1=1
AND X.ESTORNO_FATURAMENTO_FUTURO IS NULL -- SOMENTE APROVAÇÕES EM ABERTO
AND ( CAST(A.DATA_HORA AS DATE) >= @DATA_INICIAL           OR @DATA_INICIAL               IS NULL )
AND ( CAST(A.DATA_HORA AS DATE) <= @DATA_FINAL             OR @DATA_FINAL                 IS NULL ) 
AND ( A.ENTIDADE                 = @ENTIDADE               OR @ENTIDADE                   IS NULL )
AND ( M.VENDEDOR                 = @VENDEDOR               OR @VENDEDOR                   IS NULL )
AND ( Q.CENTRO_CUSTO_FATURAMENTO = @CUSTO_BASE_FATURAMENTO OR @CUSTO_BASE_FATURAMENTO     IS NULL )
                                  
GROUP BY
            A.APROVACAO_FATURAMENTO_FUTURO               ,
            A.DATA_HORA                                  ,
            CAST(O.USUARIO  AS VARCHAR) + ' - '+ O.NOME  ,
            CAST(M.VENDEDOR AS VARCHAR) /*+ ' - '+ N.NOME*/  ,
            CAST(E.ENTIDADE AS VARCHAR) + ' - '+ E.NOME  ,
            CAST(A.OBSERVACAO AS VARCHAR(250))           ,
		    R.DESCRICAO    
ORDER BY
            CAST(M.VENDEDOR AS VARCHAR) /*+ ' - '+ N.NOME*/  , 
            CAST(E.ENTIDADE AS VARCHAR) + ' - '+ E.NOME  ,
            A.APROVACAO_FATURAMENTO_FUTURO               ,
            A.DATA_HORA                                  ,
            CAST(O.USUARIO  AS VARCHAR) + ' - '+ O.NOME