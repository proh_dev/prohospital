/* ====================================================== */
/* CONSULTA DE NOTAS FISCAIS ELETRONICAS DE ENTRADAS      */
/* EMITIDAS NO PERIODO CONSOLIDADO                        */                
/* ====================================================== */
DECLARE @MOVIMENTO_INI   DATETIME
DECLARE @MOVIMENTO_FIM   DATETIME
DECLARE @EMPRESA_INI     NUMERIC
DECLARE @EMPRESA_FIM     NUMERIC

SET @MOVIMENTO_INI  = '01/01/2018'
SET @MOVIMENTO_FIM  = '31/01/2018'
SET @EMPRESA_INI    = 1
SET @EMPRESA_FIM    = 1000

--SET @MOVIMENTO_INI = :MOVIMENTO_INI
--SET @MOVIMENTO_FIM = :MOVIMENTO_INI

--IF ISNUMERIC(:EMPRESA_INI) = 1         
--    SET @EMPRESA_INI = :EMPRESA_INI              
--  ELSE  
--    SET @EMPRESA_INI = NULL       


--IF ISNUMERIC(:EMPRESA_FIM) = 1 
--    SET @EMPRESA_FIM = :EMPRESA_FIM
-- ELSE  
--    SET @EMPRESA_FIM = NULL

      SELECT A.ENTIDADE                 AS ENTIDADE ,
             C.NOME                     AS CREDOR , 
             SUM (A.VALOR)              AS VALOR
        FROM TITULOS_PAGAR         A WITH (NOLOCK)
   LEFT JOIN EMPRESAS_USUARIAS     B WITH (NOLOCK)ON B.EMPRESA_USUARIA           = A.EMPRESA
   LEFT JOIN ENTIDADES             C WITH (NOLOCK)ON C.ENTIDADE                  = A.ENTIDADE
   LEFT JOIN NF_COMPRA             D WITH (NOLOCK)ON D.NF_COMPRA                 = A.REG_MASTER_ORIGEM
        JOIN NF_COMPRA_PARCELAS    E WITH (NOLOCK)ON D.NF_COMPRA                 = E.NF_COMPRA
       WHERE D.MOVIMENTO >= @MOVIMENTO_INI AND
             D.MOVIMENTO <= @MOVIMENTO_FIM AND
             D.EMPRESA   >= @EMPRESA_INI  AND
             D.EMPRESA   <= @EMPRESA_FIM  AND
             E.CLASSIF_FINANCEIRA IN (SELECT CLASSIF_COMERCIALIZAVEIS FROM PARAMETROS_FINANCEIRO) 
GROUP BY A.ENTIDADE , C.NOME
ORDER BY C.NOME