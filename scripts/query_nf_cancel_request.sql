DECLARE @SOLICITACAO_CANCELAMENTO_NF NUMERIC(15,0)

--SET @SOLICITACAO_CANCELAMENTO_NF = 1

SET @SOLICITACAO_CANCELAMENTO_NF = :SOLICITACAO_CANCELAMENTO_NF


------------------------------------------
--CHECAGEM SE A NOTA FOI EMITIDA  SEFAZ --
------------------------------------------

SELECT TOP 1 'ATEN��O! Nota Fiscal n�o Processada no SEFAZ. N�o pode solicitar cancelamento'

  FROM SOLICITACOES_CANCELAMENTOS_NF A WITH(NOLOCK)
  LEFT 
  JOIN NFE_CABECALHO B WITH(NOLOCK) ON  A.NF_NUMERO = B.XML_nNF_B08                             
                                    AND A.NF_SERIE  = B.XML_serie_B07
                                    AND A.EMPRESA   = B.EMPRESA 
  LEFT
  JOIN NFE_LOG C WITH(NOLOCK)       ON C.REGISTRO_NFE = B.REGISTRO_NFE
                                    AND  C.STATUS    = 'XML DO DESTINATARIO'
												     

  WHERE A.SOLICITACAO_CANCELAMENTO_NF = :SOLICITACAO_CANCELAMENTO_NF
    AND C.REGISTRO_NFE_ERRO IS NULL
 


 UNION ALL
---------------------------------------------
--CHECANDO SE A NOTA FOI DENEGADA NO SEFAZ --
---------------------------------------------
SELECT TOP 1 'ATEN��O! Nota Fiscal DENEGADA no SEFAZ. N�o pode solicitar cancelamento'
FROM SOLICITACOES_CANCELAMENTOS_NF A WITH(NOLOCK)
LEFT																					     
JOIN NFE_CABECALHO B WITH(NOLOCK) ON  A.NF_NUMERO = B.XML_nNF_B08                             
                                 AND A.NF_SERIE  = B.XML_serie_B07
                                 AND A.EMPRESA   = B.EMPRESA 			 					
LEFT																                               
JOIN NFE_LOG C WITH(NOLOCK)       ON	 B.REGISTRO_NFE      = C.REGISTRO_NFE
																				

WHERE A.SOLICITACAO_CANCELAMENTO_NF = :SOLICITACAO_CANCELAMENTO_NF
 AND B.STATUS = 14 																					     

