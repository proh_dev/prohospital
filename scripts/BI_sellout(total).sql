    SELECT A.MOVIMENTO                       AS DATA , 
           MONTH ( A.MOVIMENTO )             AS MES , 
           YEAR  ( A.MOVIMENTO )             AS ANO , 
           A.EMPRESA                         AS EMPRESA , 
           H.EMPRESA_USUARIA                 AS REDE , 
           H.NOME_FANTASIA                   AS NOME_REDE , 
           F.FILIAL                          AS FILIAL , 
           ISNULL ( A.VENDEDOR , 0 )         AS VENDEDOR , 
           ISNULL (B.NOME , 'NAO INFORMADO') AS NOME ,
           I.DESCRICAO                       AS SECAO , 
           J.DESCRICAO                       AS GRUPO , 
           K.DESCRICAO                       AS SUBGRUPO , 
           L.DESCRICAO                       AS MARCA ,
           SUM ( A.QUANTIDADE )              AS QUANTIDADE ,
           SUM ( A.VENDA_BRUTA )             AS VENDA_BRUTA , 
           SUM ( A.DESCONTO )                AS DESCONTO , 
         CASE WHEN SUM ( A.VENDA_BRUTA ) * 100 > 0 AND SUM ( A.DESCONTO ) > 0
         THEN
         ( SUM ( A.DESCONTO ) / 
           SUM ( A.VENDA_BRUTA ) ) * 100     
         ELSE 0.00                       END AS PDESCONTO , 
           SUM ( A.VENDA_LIQUIDA )           AS VENDA_LIQUIDA, 
           C.DESCRICAO                       AS PRODUTOS,
		   C.CODIGO_REFERENCIA               AS REFERENCIA,
		   N.ESTADO                          AS ESTADO,
		   O.DESCRICAO                       AS GRUPO_CLIENTE,
		   P.DESCRICAO                       AS FAMILIA_PRODUTO,
		   M.NOME                            AS CLIENTE,
		   S.DESCRICAO                       AS GRUPO_MARCA
      FROM VENDAS_DIARIAS          A WITH(NOLOCK)
      LEFT  
	  JOIN VENDEDORES              B WITH(NOLOCK) ON B.VENDEDOR                      = A.VENDEDOR
      JOIN PRODUTOS                C WITH(NOLOCK) ON C.PRODUTO                       = A.PRODUTO 
      JOIN EMPRESAS_USUARIAS       F WITH(NOLOCK) ON F.EMPRESA_USUARIA               = A.EMPRESA
      JOIN EMPRESAS_USUARIAS       H WITH(NOLOCK) ON H.EMPRESA_USUARIA               = F.EMPRESA_CONTABIL     
      LEFT
	  JOIN SECOES_PRODUTOS         I WITH(NOLOCK) ON I.SECAO_PRODUTO                 = C.SECAO_PRODUTO
      LEFT
	  JOIN GRUPOS_PRODUTOS         J WITH(NOLOCK) ON J.GRUPO_PRODUTO                 = C.GRUPO_PRODUTO
      LEFT
	  JOIN SUBGRUPOS_PRODUTOS      K WITH(NOLOCK) ON K.SUBGRUPO_PRODUTO              = C.SUBGRUPO_PRODUTO
      LEFT
	  JOIN MARCAS                  L WITH(NOLOCK) ON C.MARCA                         = L.MARCA
      LEFT
	  JOIN ENTIDADES               M WITH(NOLOCK) ON A.CLIENTE                       = M.ENTIDADE
      LEFT
	  JOIN ENDERECOS               N WITH(NOLOCK) ON M.ENTIDADE                      = N.ENTIDADE
      LEFT
	  JOIN CLASSIFICACOES_CLIENTES O WITH(NOLOCK) ON M.CLASSIFICACAO_CLIENTE         = O.CLASSIFICACAO_CLIENTE
      LEFT
	  JOIN FAMILIAS_PRODUTOS       P WITH(NOLOCK) ON C.FAMILIA_PRODUTO               = P.FAMILIA_PRODUTO
	  LEFT
      JOIN GRUPOS_MARCAS_DETALHE   R WITH(NOLOCK) ON L.MARCA                         = R.MARCA
      LEFT                                                                               
      JOIN GRUPOS_MARCAS           S WITH(NOLOCK) ON R.GRUPO_MARCA                   = S.GRUPO_MARCA

     WHERE A.MOVIMENTO      >= '01/01/2018'--:DATA_INICIAL
       AND A.MOVIMENTO      <= '31/01/2018'--:DATA_FINAL
  GROUP BY A.MOVIMENTO , F.NOME_FANTASIA , A.EMPRESA , ISNULL ( A.VENDEDOR, 0 ) , 
           ISNULL (B.NOME , 'NAO INFORMADO '), B.NOME , H.EMPRESA_USUARIA , 
           H.NOME_FANTASIA , F.FILIAL , MONTH ( A.MOVIMENTO ) , YEAR ( A.MOVIMENTO ) , 
           I.DESCRICAO , J.DESCRICAO , K.DESCRICAO , L.DESCRICAO, C.DESCRICAO, C.CODIGO_REFERENCIA,N.ESTADO,O.DESCRICAO,P.DESCRICAO,M.NOME,S.DESCRICAO      
    HAVING ( SUM ( A.QUANTIDADE ) <> 0 ) 												  									 
  ORDER BY F.NOME_FANTASIA , A.EMPRESA , NOME
 