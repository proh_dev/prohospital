CREATE PROCEDURE [dbo].[RETORNO_EMAIL_APROV_CANC_NFE] ( @NF_CANCELAMENTO NUMERIC(15,0) ) AS                  
BEGIN                      
                
SET NOCOUNT ON      
  
DECLARE @NF_CANCELMENTO                     NUMERIC(15,0) = 2839 

DECLARE @EMPRESA_SOLICITANTE                VARCHAR(50)				                    
DECLARE @TITULO_EMAIL                       VARCHAR(50)  
DECLARE @tableHTML                          NVARCHAR(MAX)   
DECLARE @APROVADOR                          VARCHAR(50)  
DECLARE @NF_NUMERO                          VARCHAR(50)  
DECLARE @EMAIL                              VARCHAR(250) 
DECLARE @JUSTIFICATIVA                      VARCHAR(250)
DECLARE @REFATURA_NOTA						VARCHAR(30)
DECLARE @CLIENTE     						VARCHAR(250)
DECLARE @SOLICITACAO                        VARCHAR(250)

  

  
SET @TITULO_EMAIL = 'Retorno de cancelamento de Nota'
  
SELECT @APROVADOR               = CAST (B.APROVADOR       AS VARCHAR (50)) + ' - ' + C.NOME,
       @EMPRESA_SOLICITANTE     = CAST (A.EMPRESA         AS VARCHAR (50)) + ' - ' + F.NOME, 
	   @SOLICITACAO             = CAST (B.SOLICITACAO     AS VARCHAR (50)), 
       @NF_NUMERO               = CAST (A.NF_NUMERO       AS VARCHAR (50)),
	   @JUSTIFICATIVA           = B.JUSTIFICATIVA,
       @EMAIL                   = E.E_MAIL
    

   FROM CANCELAMENTOS_NOTAS_FISCAIS     A WITH(NOLOCK)  
   JOIN VW_SOLICITACAO_APROVACAO_NF     B WITH(NOLOCK) ON A.NF_NUMERO      = B.NF_NUMERO
                                                      AND A.NF_SERIE       = B.NF_SERIE
													  AND A.EMPRESA        = B.EMPRESA
   
   JOIN USUARIOS                        C WITH(NOLOCK) ON B.APROVADOR      = C.USUARIO
   JOIN FUNCIONARIOS                    D WITH(NOLOCK) ON C.ENTIDADE       = D.ENTIDADE
   JOIN FUNCIONARIOS_DADOS_PESSOAIS     E WITH(NOLOCK) ON C.ENTIDADE       = E.ENTIDADE
   JOIN EMPRESAS_USUARIAS               F WITH(NOLOCK) ON B.EMPRESA        = F.EMPRESA_USUARIA
  WHERE 1=1
        AND A.NF_CANCELAMENTO = @NF_CANCELMENTO
		AND A.NF_NUMERO       = B.NF_NUMERO
		--AND A.EMPRESA = @EMPRESA

  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;
              }  
              </style>'                    +  
    N'<H1 class="formato" >Nota Cancelada com Sucesso <br>    <br>  
 Referente � Solicita��o de Cancelamento:'	         + @SOLICITACAO                                  + '   <br> <br>      
 Aprovado por :							 '           + @APROVADOR	                                 + '   <br> <br> 
 Nota n�:								 '           + @NF_NUMERO                                    + '   <br> <br>  
 Empresa:								 '           + @EMPRESA_SOLICITANTE                          + '   <br> <br>
 Justificativa:							 '           + @JUSTIFICATIVA                                + '   <br> <br>
 
  
  
  </H1>'
  
  
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML';  
  
  
END 