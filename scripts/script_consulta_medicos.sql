
DECLARE @DATA_INI      DATETIME
DECLARE @DATA_FIM      DATETIME
DECLARE @MEDICO        NUMERIC
DECLARE @CR            VARCHAR(255)

--SET @EMPRESA       = 5
--SET @SECAO_PRODUTO = 8
--SET @DATA_INI      = '01/09/2018'
--SET @DATA_FIM      = '30/09/2018'
--SET @MEDICO        = 71
--SET @CR              = '1234'



--SELECT DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)
--SELECT DATEADD (dd, -1, DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) + 1, 0))
IF ISNUMERIC(:CR) = 1 SET @CR = :CR  ELSE SET @CR = NULL
SET @DATA_INI      = (SELECT  DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0) )
SET @DATA_FIM      = DBO.Ultimo_Dia(GETDATE())
IF ISNUMERIC(:MEDICO) = 1 SET @MEDICO = :MEDICO  ELSE SET @MEDICO = NULL

                  

SELECT 
       B.LOJA                                                         AS LOJA,
       B.DATA                                                         AS DATA,
       B.PREVENDA                                                     AS PREVENDA,
       CAST(A.VENDEDOR         AS VARCHAR) + '-' + K.NOME             AS VENDEDOR,
       A.NUMERO_RECEITA                                               AS NUMERO_RECEITA,
       A.DATA_RECEITA                                                 AS DATA_RECEITA,
       CAST(B.CLIENTE          AS VARCHAR) + '-' + L.NOME             AS CLIENTE,
	   F.CR                                                           AS REGISTRO_MEDICO,
       A.MEDICO                                                       AS COD_MEDICO,
       CAST(A.MEDICO           AS VARCHAR) + '-' + A.NOME_PRESCRITOR  AS MEDICO,
       A.PRODUTO                                                      AS PRODUTO,
       E.DESCRICAO                                                    AS DESCRICAO,
       A.PRECO_LIQUIDO                                                AS PRECO_LIQUIDO,
       A.QUANTIDADE                                                   AS QUANTIDADE,
       A.TOTAL_ITEM                                                   AS TOTAL_ITEM,
       CAST(E.SECAO_PRODUTO    AS VARCHAR) + '-' + M.DESCRICAO        AS SECAO,
       CAST(E.GRUPO_PRODUTO    AS VARCHAR) + '-' + G.DESCRICAO        AS GRUPO,
       CAST(E.SUBGRUPO_PRODUTO AS VARCHAR) + '-' + H.DESCRICAO        AS SUBGRUPO,
       CAST(E.MARCA               AS VARCHAR) + '-' + J.DESCRICAO        AS MARCA,
       CAST(I.GRUPO_MARCA      AS VARCHAR) + '-' + I1.DESCRICAO       AS GRUPO_MARCA,
       N.TOTAL_PRESCRICOES,
	   CASE 
	   WHEN F.PONTUA = 'S'  AND F.PONTUACAO IS NOT NULL THEN
	   ROUND ((A.TOTAL_ITEM * ISNULL(F.PONTUACAO,1)),0) 
	   WHEN F.PONTUA = 'S'  AND F.PONTUACAO_PERCENT IS NOT NULL THEN  
	   A.TOTAL_ITEM * (F.PONTUACAO_PERCENT / 100)               
	   ELSE 0 END AS PONTOS,
	   CAST(DATEPART(MONTH, B.DATA) AS varchar) + '/' + CAST(DATEPART(YEAR, B.DATA) AS varchar)  AS MES_ANO                                                   

  FROM PDV_PREVENDAS_ITENS   A WITH(NOLOCK) 
  JOIN PDV_PREVENDAS         B WITH(NOLOCK) ON B.PREVENDA         = A.PREVENDA
                                           AND B.LOJA             = A.LOJA
  JOIN PDV_VENDAS            C WITH(NOLOCK) ON C.PREVENDA         = B.PREVENDA
                                           AND C.LOJA             = B.LOJA
  JOIN PRODUTOS              E WITH(NOLOCK) ON E.PRODUTO          = A.PRODUTO
  LEFT
  JOIN MEDICOS               F WITH(NOLOCK) ON F.MEDICO           = A.MEDICO
  JOIN GRUPOS_PRODUTOS       G WITH(NOLOCK) ON G.GRUPO_PRODUTO    = E.GRUPO_PRODUTO
  JOIN SUBGRUPOS_PRODUTOS    H WITH(NOLOCK) ON H.SUBGRUPO_PRODUTO = E.SUBGRUPO_PRODUTO
  LEFT           
  JOIN GRUPOS_MARCAS_DETALHE I WITH(NOLOCK) ON I.MARCA            = E.MARCA
  LEFT
  JOIN GRUPOS_MARCAS        I1 WITH(NOLOCK) ON I1.GRUPO_MARCA     = I.GRUPO_MARCA
  JOIN MARCAS                J WITH(NOLOCK) ON J.MARCA            = E.MARCA
  JOIN VENDEDORES            K WITH(NOLOCK) ON K.VENDEDOR         = A.VENDEDOR
  LEFT
  JOIN ENTIDADES             L WITH(NOLOCK) ON L.ENTIDADE         = B.CLIENTE
  JOIN SECOES_PRODUTOS       M WITH(NOLOCK) ON M.SECAO_PRODUTO    = E.SECAO_PRODUTO
  LEFT
  JOIN 
  (
SELECT A.MEDICO,  SUM(A.QUANTIDADE) TOTAL_PRESCRICOES
 FROM PDV_PREVENDAS_ITENS   A WITH(NOLOCK) 
  JOIN PDV_PREVENDAS         B WITH(NOLOCK) ON B.PREVENDA         = A.PREVENDA
                                           AND B.LOJA             = A.LOJA
  JOIN PDV_VENDAS            C WITH(NOLOCK) ON C.PREVENDA         = B.PREVENDA
                                           AND C.LOJA             = B.LOJA
  JOIN PRODUTOS              E WITH(NOLOCK) ON E.PRODUTO          = A.PRODUTO
  LEFT
  JOIN MEDICOS               F WITH(NOLOCK) ON F.MEDICO           = A.MEDICO
  JOIN GRUPOS_PRODUTOS       G WITH(NOLOCK) ON G.GRUPO_PRODUTO    = E.GRUPO_PRODUTO
  JOIN SUBGRUPOS_PRODUTOS    H WITH(NOLOCK) ON H.SUBGRUPO_PRODUTO = E.SUBGRUPO_PRODUTO
  LEFT           
  JOIN GRUPOS_MARCAS_DETALHE I WITH(NOLOCK) ON I.MARCA            = E.MARCA
  LEFT
  JOIN GRUPOS_MARCAS        I1 WITH(NOLOCK) ON I1.GRUPO_MARCA     = I.GRUPO_MARCA
  JOIN MARCAS                J WITH(NOLOCK) ON J.MARCA            = E.MARCA
  JOIN VENDEDORES            K WITH(NOLOCK) ON K.VENDEDOR         = A.VENDEDOR
  LEFT
  JOIN ENTIDADES             L WITH(NOLOCK) ON L.ENTIDADE         = B.CLIENTE
  JOIN SECOES_PRODUTOS       M WITH(NOLOCK) ON M.SECAO_PRODUTO    = E.SECAO_PRODUTO
WHERE 1=1
 AND ( A.MEDICO         = @MEDICO        OR @MEDICO        IS NULL ) 
 AND C.MOVIMENTO     >= @DATA_INI
 AND C.MOVIMENTO     <= @DATA_FIM
 AND ( F.CR         = @CR        OR @CR        IS NULL ) 

 GROUP BY A.MEDICO

  )                             N ON N.MEDICO = A.MEDICO


WHERE 1=1
 AND ( A.MEDICO         = @MEDICO        OR @MEDICO        IS NULL )
 AND C.MOVIMENTO       >= @DATA_INI
 AND C.MOVIMENTO       <= @DATA_FIM
 AND (F.CR           LIKE @CR            OR @CR            IS NULL)