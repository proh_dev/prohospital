-----------------------------
-- DECLARA��O DE VARIAVEIS --
-----------------------------

DECLARE @MANUTENCAO_GRUPO_TRIBUTARIO NUMERIC(15)
DECLARE @EAN                         VARCHAR(13)
DECLARE @PRODUTO                     NUMERIC = :PRODUTO

DECLARE @PRODUTOS                 AS TYPE_PARAMS_PRODUTOS  --VARIAVEL DO TIPO TABLE PARA SERVIR DE TEMPORARIA NA CHAMADA DA PROCEDURE


------------------------------------------------
-- GRAVACAO DA ULTIMA ALTERACAO - ATUALIZACAO -- 
------------------------------------------------

    UPDATE PRODUTOS_ULTIMA_ALTERACAO 
       SET ULTIMA_ALTERACAO = GETDATE()
      FROM PRODUTOS                  A WITH(NOLOCK)
INNER JOIN PRODUTOS_ULTIMA_ALTERACAO B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO
     WHERE A.PRODUTO  IN(SELECT PRODUTO
							FROM PRODUTOS
							WHERE PRODUTO = @PRODUTO)

---------------------------------------------
-- GRAVACAO DA ULTIMA ALTERACAO - INSERCAO --
---------------------------------------------

INSERT INTO PRODUTOS_ULTIMA_ALTERACAO
          ( PRODUTO
          , ULTIMA_ALTERACAO )

     SELECT DISTINCT
            A.PRODUTO
          , GETDATE()

       FROM PRODUTOS                  A WITH(NOLOCK)
  LEFT JOIN PRODUTOS_ULTIMA_ALTERACAO B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO

      WHERE B.PRODUTO IS NULL
        AND A.PRODUTO  IN(SELECT PRODUTO
							FROM PRODUTOS
							WHERE PRODUTO = @PRODUTO)

/*
-------------------------------------------------
--ROTINA PARA GERAR O EAN INTERNO
-------------------------------------------------

--DEFINE 12 EM DIGITO VERIFICADOR--
SELECT @EAN = CAST(900000000000 + A.PRODUTO AS VARCHAR(12))
  FROM PRODUTOS        A WITH(NOLOCK)
WHERE A.PRODUTO = @PRODUTO 

--EAN FINAL COM DIGITO VERIFICADOR--
SELECT @EAN = @EAN + DBO.EAN13_DIGITO ( @EAN )


--INSERE O EAN INTERNO--
INSERT INTO PRODUTOS_EAN ( 
  
            PRODUTO, 
			EAN,
			QUANTIDADE_EMBALAGEM,
			EAN_INTERNO )

SELECT A.PRODUTO, 
       @EAN AS EAN,
	   1    AS QUANTIDADE_EMBALAGEM,
	   'S'  AS EAN_INTERNO
  FROM PRODUTOS        A WITH(NOLOCK)
LEFT JOIN PRODUTOS_EAN B WITH(NOLOCK) ON B.EAN     = @EAN

WHERE A.PRODUTO = @PRODUTO
  AND B.PRODUTO IS NULL
  AND ISNULL(LEN(@EAN),0) = 13

  */

------------------------------------------------------------------
-- ATUALIZA TABELA DE TRANSFERENCIA PRODUTOS_DCB PARA LOJAS --
------------------------------------------------------------------

DELETE PDV_PRODUTOS_DCB WHERE PRODUTO = @PRODUTO

INSERT PDV_PRODUTOS_DCB ( PRODUTO , 
                          DCB , 
                          INDICACAO ,
                          LOJA,
                          ENVIADO )
                   SELECT DISTINCT 
                          A.PRODUTO ,
                          A.DCB , 
                          B.INDICACAO ,
                          Z.EMPRESA_USUARIA,
                          'N' AS ENVIADO
                     FROM PRODUTOS_DCB      A WITH(NOLOCK)
                     JOIN DCB_MEDICAMENTOS  B WITH(NOLOCK) ON B.DCB = A.DCB
                     JOIN PARAMETROS_VENDAS Z WITH(NOLOCK) ON Z.DATA_INICIO_ERPM_PDV <= GETDATE()
                     JOIN PRODUTOS          C WITH(NOLOCK) ON C.PRODUTO               = A.PRODUTO
                    WHERE A.PRODUTO  IN(SELECT PRODUTO
											FROM PRODUTOS
											WHERE PRODUTO = @PRODUTO)
                      AND C.ENVIAR_LOJAS = 'S'

        
-------------------------------------------------------------          
-- INSERINDO PRODUTOS QUE AINDA N�O CONSTAM NA TABELA      --          
-- DE PRODUTOS_PARAMETROS_EMPRESAS                         --          
-------------------------------------------------------------          
          
INSERT INTO PRODUTOS_PARAMETROS_EMPRESAS          
(          
PRODUTO                    ,          
EMPRESA                    ,          
PRECO_VENDA                ,          
DESCONTO_PADRAO            ,          
DESCONTO_MAXIMO            ,   
TIPO_ICMS                  ,          
SITUACAO_PRODUTO           ,          
ULTIMA_ATUALIZACAO         ,          
CLASSIF_FISCAL_CODIGO      ,          
IVA                        ,          
IVA_AJUSTADO               ,          
ESTOQUE_MINIMO             ,          
VALIDADE_MINIMO            ,          
ACRESCIMO                  ,          
TIPO_BONIFICACAO           ,          
BONIFICACAO                ,
CODIGO_CEST           
)          
          
SELECT A.PRODUTO          AS PRODUTO                    ,          
       B.EMPRESA_USUARIA  AS EMPRESA                    ,                
       0.00               AS PRECO_VENDA                ,          
       0.00               AS DESCONTO_PADRAO            ,          
       0.00               AS DESCONTO_MAXIMO            ,  
       0                  AS TIPO_ICMS                  ,          
       A.SITUACAO_PRODUTO AS SITUACAO_PRODUTO           ,          
       GETDATE()          AS ULTIMA_ATUALIZACAO         ,          
       A.NCM              AS CLASSIF_FISCAL_CODIGO      ,          
       0.00               AS IVA                        ,            
       0.00               AS IVA_AJUSTADO               ,               
       0.00               AS ESTOQUE_MINIMO             ,          
       '31/12/2099'           
                          AS VALIDADE_MINIMO            ,          
       0.00               AS ACRESCIMO                  ,          
       'V'                AS TIPO_BONIFICACAO           ,          
       0.00               AS BONIFICACAO                ,
	   CASE WHEN CONVERT(DATE,GETDATE()) >= ISNULL(D.CALCULAR_CEST_APARTIR,'01.07.2017')  
            THEN A.CODIGO_CEST  
            ELSE NULL  
       END                AS CODIGO_CEST           
                 
  FROM PRODUTOS          A WITH(NOLOCK)          
  JOIN EMPRESAS_USUARIAS B WITH(NOLOCK) ON B.EMPRESA_USUARIA > 0  
                                       AND B.ATIVO = 'S'        
  LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS C WITH(NOLOCK) ON C.PRODUTO = A.PRODUTO          
                                                       AND C.EMPRESA = B.EMPRESA_USUARIA          
  LEFT 
  JOIN PARAMETROS_FISCAIS           D WITH(NOLOCK) ON D.EMPRESA_USUARIA = B.EMPRESA_USUARIA                                                                 
WHERE C.PRODUTO_PARAMETRO_EMPRESA IS NULL
  AND A.PRODUTO  IN(SELECT PRODUTO
					FROM PRODUTOS
					WHERE PRODUTO = @PRODUTO)


---------------------------------------------------------------------------
-- ATUALIZAR PRODUTOS_VENDAS DE PRODUTOS LIGADOS AO PRODUTO_CONCENTRADOR --
---------------------------------------------------------------------------
-- COLOCADO POR FILIPE OLIVEIRA EM 04/03/2016                            --


     IF OBJECT_ID('TEMPDB..#TEMP_01') IS NOT NULL DROP TABLE #TEMP_01   

SELECT 
        A.PRECO_MAXIMO      ,
        A.DESCONTO_MAXIMO	,
        A.DESCONTO_PADRAO	,
        A.MARKUP			,
        A.PRECO_VENDA		,
        A.TIPO_BONIFICACAO  ,
        A.VALOR_BONIFICACAO ,
        A.GRUPO_PRECO		,
        A.PRECO_FABRICA	    ,
        A.TIPO_COMISSAO	    ,
        A.COMISSAO		    ,
        A.TAXA_MOTOBOY      ,
		A.PRODUTO

INTO #TEMP_01

FROM PRODUTOS_VENDAS A WITH(NOLOCK)

WHERE A.PRODUTO  IN(SELECT PRODUTO
					FROM PRODUTOS
					WHERE PRODUTO = @PRODUTO)


UPDATE PRODUTOS_VENDAS
SET 
PRECO_MAXIMO      = C.PRECO_MAXIMO      ,
DESCONTO_MAXIMO	  = C.DESCONTO_MAXIMO	,
DESCONTO_PADRAO	  = C.DESCONTO_PADRAO	,
MARKUP			  = C.MARKUP			,
PRECO_VENDA		  = C.PRECO_VENDA		,
TIPO_BONIFICACAO  = C.TIPO_BONIFICACAO  ,
VALOR_BONIFICACAO = C.VALOR_BONIFICACAO ,
GRUPO_PRECO		  = C.GRUPO_PRECO		,
PRECO_FABRICA	  = C.PRECO_FABRICA	    ,
TIPO_COMISSAO	  = C.TIPO_COMISSAO	    ,
COMISSAO		  = C.COMISSAO		    ,
TAXA_MOTOBOY	  = C.TAXA_MOTOBOY	  

FROM PRODUTOS_VENDAS A WITH(NOLOCK)
JOIN PRODUTOS        B WITH(NOLOCK) ON B.PRODUTO     = A.PRODUTO
JOIN #TEMP_01        C              ON C.PRODUTO     = B.PRODUTO_CONCENTRADOR
                                   AND C.GRUPO_PRECO = A.GRUPO_PRECO



----------------------------------------------------------------------------------------
-- ATUALIZAR PRODUTOS_PARAMETROS_EMPRESAS DE PRODUTOS LIGADOS AO PRODUTO_CONCENTRADOR --
----------------------------------------------------------------------------------------
-- COLOCADO POR FILIPE OLIVEIRA EM 07/03/2016                                         --


     IF OBJECT_ID('TEMPDB..#TEMP_02') IS NOT NULL DROP TABLE #TEMP_02   

SELECT 
        A.PRECO_MAXIMO      ,
        A.DESCONTO_MAXIMO	,
        A.DESCONTO_PADRAO	,
        A.PRECO_VENDA		,
        A.TIPO_BONIFICACAO  ,
        A.PRECO_FABRICA	    ,
        A.TIPO_COMISSAO	    ,
        A.COMISSAO		    ,
		A.PRODUTO           ,
		A.EMPRESA

INTO #TEMP_02

FROM PRODUTOS_PARAMETROS_EMPRESAS A WITH(NOLOCK)

WHERE A.PRODUTO IN(SELECT PRODUTO
					FROM PRODUTOS
					WHERE PRODUTO = @PRODUTO)


UPDATE PRODUTOS_PARAMETROS_EMPRESAS
SET 
PRECO_MAXIMO      = C.PRECO_MAXIMO      ,
DESCONTO_MAXIMO	  = C.DESCONTO_MAXIMO	,
DESCONTO_PADRAO	  = C.DESCONTO_PADRAO	,
PRECO_VENDA		  = C.PRECO_VENDA		,
TIPO_BONIFICACAO  = C.TIPO_BONIFICACAO  ,
PRECO_FABRICA	  = C.PRECO_FABRICA	    ,
TIPO_COMISSAO	  = C.TIPO_COMISSAO	    ,
COMISSAO		  = C.COMISSAO		    

FROM PRODUTOS_PARAMETROS_EMPRESAS A WITH(NOLOCK)
JOIN PRODUTOS                     B WITH(NOLOCK) ON B.PRODUTO     = A.PRODUTO
JOIN #TEMP_02                     C              ON C.PRODUTO     = B.PRODUTO_CONCENTRADOR
                                                AND C.EMPRESA     = A.EMPRESA


UPDATE PRODUTOS_PARAMETROS_EMPRESAS
SET 
PRECO_MAXIMO      = C.PRECO_MAXIMO      ,
DESCONTO_MAXIMO	  = C.DESCONTO_MAXIMO	,
DESCONTO_PADRAO	  = C.DESCONTO_PADRAO	,
PRECO_VENDA		  = C.PRECO_VENDA		,
TIPO_BONIFICACAO  = C.TIPO_BONIFICACAO  ,
PRECO_FABRICA	  = C.PRECO_FABRICA	    ,
TIPO_COMISSAO	  = C.TIPO_COMISSAO	    ,
COMISSAO		  = C.COMISSAO		    

FROM PRODUTOS_PARAMETROS_EMPRESAS A WITH(NOLOCK)
JOIN PRODUTOS                     B WITH(NOLOCK) ON B.PRODUTO         = A.PRODUTO
JOIN GRUPOS_PRECOS_EMPRESAS       Z WITH(NOLOCK) ON Z.EMPRESA_USUARIA = A.EMPRESA
JOIN #TEMP_01                     C              ON C.PRODUTO         = A.PRODUTO
                                                AND C.GRUPO_PRECO     = Z.GRUPO_PRECO


------------------------------------------------------------
-- INSERIR CORRELATOS E SUBSTITUIDOS DE MANEIRA RECIPROCA --
-- COLOCADO POR FILIPE OLIVEIRA --
------------------------------------------------------------

INSERT INTO PRODUTOS_CORRELATOS
(
PRODUTO,
CORRELATO,
ECOMMERCE
)
SELECT 
A.CORRELATO   AS PRODUTO   ,
A.PRODUTO     AS CORRELATO ,
A.ECOMMERCE   AS ECOMMERCE

  FROM PRODUTOS_CORRELATOS A WITH(NOLOCK)
  LEFT
  JOIN PRODUTOS_CORRELATOS B WITH(NOLOCK) ON B.CORRELATO = A.PRODUTO
 WHERE A.PRODUTO = @PRODUTO
 AND B.PRODUTO_CORRELATO IS NULL



INSERT INTO PRODUTOS_SUBSTITUTOS
(
PRODUTO,
SUBSTITUTO,
ECOMMERCE
)
SELECT 
A.SUBSTITUTO   AS PRODUTO   ,
A.PRODUTO      AS SUBSTITUTO ,
A.ECOMMERCE    AS ECOMMERCE

  FROM PRODUTOS_SUBSTITUTOS A WITH(NOLOCK)
  LEFT
  JOIN PRODUTOS_SUBSTITUTOS B WITH(NOLOCK) ON B.SUBSTITUTO = A.PRODUTO
 WHERE A.PRODUTO IN(SELECT PRODUTO
					 FROM PRODUTOS
					 WHERE PRODUTO = @PRODUTO)
 AND B.PRODUTO_SUBSTITUTO IS NULL


------------------------------------------------------------
------------------------------------------------------------


-- CRIA��O DE ROTINA PARA ATUALIZAR ENCARTES --
-- POR FILIPE OLIVEIRA --

DECLARE @TABELA_ENCARTE NUMERIC(15,0)

IF OBJECT_ID('TEMPDB..#TABELAS_ENCARTES') IS NOT NULL DROP TABLE #TABELAS_ENCARTES

 SELECT A.TABELA_ENCARTE 
   INTO #TABELAS_ENCARTES  
   FROM TABELAS_ENCARTES_PRODUTOS A WITH(NOLOCK)
    WHERE A.PRODUTO = @PRODUTO



WHILE EXISTS(SELECT TOP 1 1 FROM #TABELAS_ENCARTES)
BEGIN
    SELECT TOP 1 @TABELA_ENCARTE = A.TABELA_ENCARTE FROM #TABELAS_ENCARTES A
    DELETE FROM #TABELAS_ENCARTES WHERE TABELA_ENCARTE = @TABELA_ENCARTE
    
    EXEC USP_PRECIFICACAO_ENCARTE   @TABELA_ENCARTE

END


----------------------------------------------------------------------
-- ATUALIZA��O DA TABELA PRODUTOS EMPRESA                           --
-- CONFORME PARAMETROS DO GRUPO TRIBUTARIO                          --
-- COLOCADO POR VICTOR EM 09/04/2013 ,                              --
-- DESCRICAO ALTERADA N�O ERA ENVIADA A LOJA                        --
----------------------------------------------------------------------
-- ALTERADO PARA PROCEDURE PARA FAZER CHAMADA EM OUTROS FORMUL�RIOS --
-- ALBERTO EM 26/12/2013                                            --
----------------------------------------------------------------------

/* COMENTADO POR FELIPE PORFIRIO EM 18/07/2018 - A TRIGGER DA TABELA PRODUTOS, J� FAZ ESSE PROCESSO. TICKET N�45034 */

--INSERT INTO @PRODUTOS ( PRODUTO ) 
--     SELECT @PRODUTO AS PRODUTO
	 
--	 UNION ALL

--	 SELECT PRODUTO 
--	 FROM PRODUTOS A WITH(NOLOCK)
--	 WHERE PRODUTO_CONCENTRADOR = @PRODUTO

--EXEC ATUALIZAR_PRODUTOS_PARAMETROS_EMPRESAS @PRODUTOS



-------------------------------------------------------------
--    INSERINDO DADOS NA TABELA DE GRUPOS_TERMO_ACORDO_CE  --
-------------------------------------------------------------


--------------------------------------------------------------------------
--    LIMPAR DADOS NA TABELA DE GRUPOS_TERMO_ACORDO_CE ANTES DO MERGE   --
--------------------------------------------------------------------------

DELETE GRUPOS_TERMO_ACORDO_CE_PRODUTOS 
  FROM GRUPOS_TERMO_ACORDO_CE_PRODUTOS          Z WITH(NOLOCK)
  WHERE Z.GRUPO_TERMO_ACORDO_CE_PRODUTO IN (
                                            SELECT GRUPO_TERMO_ACORDO_CE_PRODUTO
                                              FROM GRUPOS_TERMO_ACORDO_CE_PRODUTOS A WITH(NOLOCK)
                                             WHERE A.PRODUTO = @PRODUTO
                                           )


INSERT INTO GRUPOS_TERMO_ACORDO_CE_PRODUTOS
(
FORMULARIO_ORIGEM,
TAB_MASTER_ORIGEM,
REG_MASTER_ORIGEM,
REG_LOG_INCLUSAO,
GRUPO_TERMO_ACORDO_CE,
PRODUTO
)

SELECT 
       FORMULARIO_ORIGEM,
       TAB_MASTER_ORIGEM,
       REG_MASTER_ORIGEM,
       REG_LOG_INCLUSAO,
       GRUPO_TERMO_ACORDO_CE,
       PRODUTO
  FROM PRODUTOS A WITH(NOLOCK)
 WHERE A.PRODUTO IN (SELECT PRODUTO 
				      FROM PRODUTOS
					  WHERE PRODUTO = @PRODUTO)

 -------------------------------------------------------------
 -------------------------------------------------------------