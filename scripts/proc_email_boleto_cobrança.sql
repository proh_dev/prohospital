ALTER PROCEDURE [dbo].[ENVIO_EMAIL_APRV_CANC] ( @APROVACAO_CANCELAMENTO NUMERIC(15,0), @EMPRESA NUMERIC(15,0) ) AS                      
BEGIN                      
                
SET NOCOUNT ON      
  
--DECLARE @APROVACAO_CANCELAMENTO             NUMERIC(15,0) = 12  --:APROVACAO_CANCELAMENTO
--DECLARE @EMPRESA                            NUMERIC(15,0) = 1000--:EMPRESA


DECLARE @EMPRESA_SOLICITANTE                VARCHAR(50)				                    
DECLARE @TITULO_EMAIL                       VARCHAR(50)  
DECLARE @tableHTML                          NVARCHAR(MAX)   
DECLARE @SOLICITANTE                        VARCHAR(50)  
DECLARE @NF_NUMERO                          VARCHAR(50)  
DECLARE @EMAIL                              VARCHAR(250) 
DECLARE @JUSTIFICATIVA                      VARCHAR(250)
DECLARE @REFATURA_NOTA						VARCHAR(30)
DECLARE @CLIENTE     						VARCHAR(250)
DECLARE @BOLETO                             NUMERIC

  
--SET 
  
SELECT  
       @BOLETO                  = CAST ( @APROVACAO_CANCELAMENTO AS VARCHAR (50) )  ,
       @EMAIL                   = 'pedro.mota@prohospital.com.br',--B.EMAIL,
	   @TITULO_EMAIL            = 'Shopping Prohospital Boleto ' + CAST ( @APROVACAO_CANCELAMENTO AS VARCHAR (50) )  
  

FROM EMISSOES_BOLETOS            A WITH(NOLOCK)  
   JOIN PARAMETROS_FISCAIS                  B WITH(NOLOCK) ON A.EMPRESA                     = B.EMPRESA_USUARIA
   JOIN APROVACOES_CANCELAMENTOS_NF_ITENS   C WITH(NOLOCK) ON A.APROVACAO_CANCELAMENTO      = C.APROVACAO_CANCELAMENTO
   JOIN SOLICITACOES_CANCELAMENTOS_NF       D WITH(NOLOCK) ON C.SOLICITACAO_CANCELAMENTO_NF = D.SOLICITACAO_CANCELAMENTO_NF
   LEFT									    										        
   JOIN USUARIOS                            E WITH(NOLOCK) ON A.USUARIO_LOGADO              = E.USUARIO
   JOIN EMPRESAS_USUARIAS                   F WITH(NOLOCK) ON A.EMPRESA                     = F.EMPRESA_USUARIA
   JOIN ENTIDADES                           G WITH(NOLOCK) ON G.ENTIDADE                    = D.CLIENTE
   
  WHERE 1=1
        AND A.APROVACAO_CANCELAMENTO =  @APROVACAO_CANCELAMENTO 
		AND A.EMPRESA                =  @EMPRESA
		AND C.APROVADO               = 'S'
  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;  
              }  
              </style>'                    +  
    N'<H1 class="formato" > Nova Aprova��o de Cancelamento de Nota <br>   <br>
    Aprova��o n� '       + CAST(@APROVACAO_CANCELAMENTO      AS VARCHAR) + '   <br>  
  
  </H1>' +  
    N'<table border="1" class="formato">' +  
    N'<tr>                                                                                                  													
           <th>SOLICITANTE</th>                                                  
           <th>EMPRESA_SOLICITANTE</th>  
           <th>NF_NUMERO</th>  
		   <th>NF_SERIE</th>
		   <th>JUSTIFICATIVA</th>  
           <th>REFATURA_NOTA</th>  
           <th>CLIENTE</th>
		 
      </tr>'                             +  
  
    CAST ( (   
  
 SELECT
      td = CAST (A.USUARIO_LOGADO  AS VARCHAR (50)) + ' - ' + E.NOME  , '',
	  td = CAST (A.EMPRESA         AS VARCHAR (50)) + ' - ' + F.NOME  , '',
      td = CAST (C.NF_NUMERO       AS VARCHAR (50))                   , '',
	  td = CAST (C.NF_SERIE        AS VARCHAR (50))                   , '',
	  td = CAST (D.JUSTIFICATIVA   AS VARCHAR (250))                  , '',
	  td = CASE WHEN D.REFATURA_NOTA = 'S' THEN 'Sim' ELSE 'N�o' END  , '',
      td = CAST (D.CLIENTE         AS VARCHAR (50)) + ' - ' + G.NOME  
	     

FROM APROVACOES_CANCELAMENTOS_NF            A WITH(NOLOCK)  
   JOIN APROVACOES_CANCELAMENTOS_NF_ITENS   C WITH(NOLOCK) ON A.APROVACAO_CANCELAMENTO      = C.APROVACAO_CANCELAMENTO
   JOIN SOLICITACOES_CANCELAMENTOS_NF       D WITH(NOLOCK) ON C.SOLICITACAO_CANCELAMENTO_NF = D.SOLICITACAO_CANCELAMENTO_NF
   LEFT									    										        
   JOIN USUARIOS                            E WITH(NOLOCK) ON A.USUARIO_LOGADO              = E.USUARIO
   JOIN EMPRESAS_USUARIAS                   F WITH(NOLOCK) ON A.EMPRESA                     = F.EMPRESA_USUARIA
   JOIN ENTIDADES                           G WITH(NOLOCK) ON G.ENTIDADE                    = D.CLIENTE
   
  WHERE 1=1
        AND A.APROVACAO_CANCELAMENTO = @APROVACAO_CANCELAMENTO 
		AND A.EMPRESA                = @EMPRESA
		AND C.APROVADO               = 'S'
  
              FOR XML PATH('tr'), TYPE   
    ) AS NVARCHAR(MAX) ) +  
    N'</table>' ;  
  

if @EMAIL IS NOT NULL
  BEGIN   

   EXEC MSDB.DBO.SP_SEND_DBMAIL  
     
     @RECIPIENTS   = @EMAIL,  
     @SUBJECT      = @TITULO_EMAIL ,  
     @BODY         = @tableHTML,  
     @BODY_FORMAT  = 'HTML';  

   END        
  
--SELECT 
--@EMAIL,  
--@TITULO_EMAIL ,  
--@tableHTML

             
END 