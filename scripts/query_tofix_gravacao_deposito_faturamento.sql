---------------------------------------------------------------
-- Desativado esperando configura��o para subida a produ��o --
---------------------------------------------------------------


--IF :PROCESSAR = 'S'
--BEGIN
    
     ----EXEC USP_GRAVACAO_FINAL_DEPOSITO_FATURAMENTOS :DEPOSITO_FATURAMENTO
  --   EXEC USP_GRAVACAO_FINAL_DEPOSITO_FATURAMENTOS_CROSSDOCKING :DEPOSITO_FATURAMENTO

--END

------------------------------------------------------
   -- Grava��o final antiga vinda da Homologa��o --
------------------------------------------------------

BEGIN TRAN
--ROLLBACK
--COMMIT


SET DATEFORMAT  DMY

DECLARE @PROCESSAR VARCHAR(1)
    SET @PROCESSAR = 'S' -- :PROCESSAR
 
IF @PROCESSAR = 'S' 

BEGIN

----------------------
-- VARIAVEIS LOCAIS --
----------------------

DECLARE @FORMULARIO_ORIGEM                     NUMERIC(15)
DECLARE @TAB_MASTER_ORIGEM                     NUMERIC(15)
DECLARE @DEPOSITO_FATURAMENTO                  NUMERIC(15)
DECLARE @ENTIDADE                              NUMERIC(15)
DECLARE @ENTIDADE_EMPRESA                      NUMERIC(15)
DECLARE @TRANSPORTADORA                        NUMERIC(15)
DECLARE @ESTADO_EMPRESA                        VARCHAR(5)
DECLARE @ESTADO_PADRAO                         VARCHAR(5)
DECLARE @ALIQUOTA_INTERNA                      NUMERIC(7,2)
DECLARE @EMITIR_NFE                            VARCHAR(1)
DECLARE @NF_ESPECIE                            VARCHAR(10)
DECLARE @NF_SERIE                              NUMERIC
DECLARE @OPERACAO_FISCAL_PEDIDO_VAREJO_VENDA   NUMERIC(15)
DECLARE @OPERACAO_FISCAL_PEDIDO_VAREJO_TRANSF  NUMERIC(15)
DECLARE @CENTRO_ESTOQUE                        NUMERIC(15)
DECLARE @EMPRESA                               NUMERIC(15)
DECLARE @EMISSAO                               DATETIME
DECLARE @NF_NUMERO_INICIAL                     NUMERIC
DECLARE @NF_NUMERO                             NUMERIC
DECLARE @DEPOSITO                              NUMERIC
DECLARE @DECRETO_35346                         VARCHAR(1)
DECLARE @CONDICAO_PAGAMENTO                    VARCHAR(255)
DECLARE @ATUALIZAR_ESTOQUE_TRANSF_NOTA         VARCHAR(1)
DECLARE @OPERACAO_FISCAL_PEDIDO_VAREJO_MANUAL  NUMERIC
DECLARE @EMPRESA_DESTINO                       NUMERIC
DECLARE @MENSAGEM                              VARCHAR(MAX)

DECLARE @FORMULARIO_ORIGEM_DEPOSITO			   NUMERIC(15)
DECLARE @TAB_MASTER_ORIGEM_DEPOSITO			   NUMERIC(15)
DECLARE @PEDIDO_VENDA						   NUMERIC(15)

SET @FORMULARIO_ORIGEM_DEPOSITO = 0--:FORMULARIO_ORIGEM
SET @TAB_MASTER_ORIGEM_DEPOSITO = 0--:TAB_MASTER_ORIGEM


SET @EMISSAO = CONVERT(VARCHAR(10),GETDATE(), 103)

SET @DEPOSITO                              = 31 -- :EMPRESA_ORIGEM
SET @DEPOSITO_FATURAMENTO                  = 40270 -- :DEPOSITO_FATURAMENTO
SET @NF_ESPECIE                            ='NFE' -- :NF_ESPECIE
SET @NF_SERIE                              ='1' -- :NF_SERIE
SET @ENTIDADE                              = NULL -- :ENTIDADE
SET @OPERACAO_FISCAL_PEDIDO_VAREJO_MANUAL  = NULL -- :OPERACAO_FISCAL_MANUAL
SET @EMPRESA_DESTINO                       = 32 --:EMPRESA_DESTINO

--SET @DEPOSITO                             =:EMPRESA_ORIGEM
--SET @DEPOSITO_FATURAMENTO                 =:DEPOSITO_FATURAMENTO
--SET @NF_ESPECIE                           =:NF_ESPECIE
--SET @NF_SERIE                             =:NF_SERIE
--SET @ENTIDADE                             =:ENTIDADE
--SET @OPERACAO_FISCAL_PEDIDO_VAREJO_MANUAL =:OPERACAO_FISCAL_MANUAL
--SET @EMPRESA_DESTINO                      =:EMPRESA_DESTINO



SET @EMITIR_NFE  = 'N'

--DEFINE EMPRESA--

SELECT @EMPRESA = @DEPOSITO

IF @EMPRESA IS NULL SET @EMPRESA = 1

SELECT @DECRETO_35346 = A.DECRETO_35346
  FROM PARAMETROS_FISCAIS A WITH(NOLOCK)
 WHERE A.EMPRESA_USUARIA = @EMPRESA

--VERIFICA SE GERA NOTA PELO PARAMETRO DA EMPRESA--

SELECT @ATUALIZAR_ESTOQUE_TRANSF_NOTA = A.ATUALIZAR_ESTOQUE_TRANSF_NOTA
  FROM PARAMETROS_ESTOQUE A WITH(NOLOCK)
 WHERE A.EMPRESA_USUARIA = @EMPRESA
 
 IF @ATUALIZAR_ESTOQUE_TRANSF_NOTA IS NULL SET @ATUALIZAR_ESTOQUE_TRANSF_NOTA = 'N'

 
---------------------------------------------------------------------------
--INICIA A GERACAO DA NOTA CASO O FLAG ATUALIZAR_ESTOQUE_TRANSF_NOTA = 'S' 
---------------------------------------------------------------------------


---------------------------------------------------------------------------
--INICIA A GERACAO DA NOTA CASO O FLAG ATUALIZAR_ESTOQUE_TRANSF_NOTA = 'S' 
---------------------------------------------------------------------------

IF @ATUALIZAR_ESTOQUE_TRANSF_NOTA = 'S'

BEGIN
  
     ------------------------------------------------------------------
     -- VERIFICA SE A ULTIMA NOTA FOI CANCELADA
     ------------------------------------------------------------------

     IF OBJECT_ID('TEMPDB..#ULTIMA_NF'        ) IS NOT NULL DROP TABLE #ULTIMA_NF

     SELECT B.ESTOQUE_TRANSFERENCIA,
            MAX(A.NF_FATURAMENTO) AS NF_FATURAMENTO

      INTO #ULTIMA_NF

       FROM NF_FATURAMENTO              A WITH(NOLOCK)
       JOIN NF_FATURAMENTO_TRANSF       B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
  LEFT JOIN CANCELAMENTOS_NOTAS_FISCAIS C WITH(NOLOCK) ON C.CHAVE          = A.NF_FATURAMENTO
                                                      AND C.TIPO           = 1 
  LEFT JOIN NF_FATURAMENTO_DEVOLUCOES   F WITH(NOLOCK) ON F.NF_FATURAMENTO_ORIGEM = A.NF_FATURAMENTO
  LEFT JOIN NFE_CABECALHO               G WITH(NOLOCK) ON G.FORMULARIO_ORIGEM     = 177982
                                                      AND G.REG_MASTER_ORIGEM     = F.NF_FATURAMENTO_DEVOLUCAO

 WHERE A.DEPOSITO_FATURAMENTO      = @DEPOSITO_FATURAMENTO
   AND C.NF_CANCELAMENTO           IS NOT NULL
   AND F.NF_FATURAMENTO_DEVOLUCAO  IS NOT NULL



     GROUP BY B.ESTOQUE_TRANSFERENCIA
    

     -------------------------------------------------------------------
     --DEFINE AS ORIGENS --
     -------------------------------------------------------------------

     SELECT @FORMULARIO_ORIGEM = A.NUMID
       FROM FORMULARIOS A WITH (NOLOCK)
      WHERE FORMULARIO = 'NF_FATURAMENTO'

     SELECT @TAB_MASTER_ORIGEM = A.NUMID
      FROM TABELAS A WITH (NOLOCK)
     WHERE TABELA = 'NF_FATURAMENTO'

             
     -------------------------------------------------------------------
     --DEFINE ESTADO DA EMPRESA EMISSORA DA NOTA--
     -------------------------------------------------------------------             

     SELECT @ESTADO_PADRAO   = MAX(ISNULL(A.ESTADO_PADRAO, 'RJ') )
       FROM PARAMETROS_FISCAIS A WITH(NOLOCK) 
      WHERE A.EMPRESA_USUARIA = @EMPRESA
 
     -------------------------------------------------------------------
     --DEFINE ESTADO DA EMPRESA EMISSORA DA NOTA--
     -------------------------------------------------------------------             

        SELECT @ESTADO_EMPRESA   = MAX(ISNULL(B.ESTADO, @ESTADO_PADRAO) )
          FROM EMPRESAS_USUARIAS A WITH(NOLOCK) 
     LEFT JOIN ENDERECOS         B WITH(NOLOCK) ON B.ENTIDADE = A.ENTIDADE
         WHERE A.EMPRESA_USUARIA = @EMPRESA

    
-------------------------------------------------------------------
--DEFINE OPERACAO FISCAL DA NOTA --
-------------------------------------------------------------------             
  DECLARE @OPERACAO_FISCAL_PEDIDO_VAREJO TABLE(
             ROMANEIO NUMERIC(15),
             OPERACAO_FISCAL NUMERIC(15))

     INSERT INTO @OPERACAO_FISCAL_PEDIDO_VAREJO
            ( ROMANEIO,
              OPERACAO_FISCAL
             )
     
     SELECT B.ESTOQUE_TRANSFERENCIA AS ROMANEIO,
            F.OPERACAO_FISCAL

      FROM DEPOSITO_FATURAMENTOS            A WITH(NOLOCK)
      JOIN DEPOSITO_FATURAMENTOS_ITENS      B WITH(NOLOCK) ON A.DEPOSITO_FATURAMENTO           = B.DEPOSITO_FATURAMENTO
      JOIN ESTOQUE_TRANSFERENCIAS           C WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA          = C.ESTOQUE_TRANSFERENCIA
      JOIN SOLICITACOES_FATURAMENTOS        D WITH(NOLOCK) ON C.SOLICITACAO_FATURAMENTO        = D.SOLICITACAO_FATURAMENTO
      JOIN PEDIDOS_VENDAS                   E WITH(NOLOCK) ON D.PEDIDO_VENDA                   = E.PEDIDO_VENDA
      JOIN TIPOS_PEDIDOS_VENDAS             F WITH(NOLOCK) ON F.TIPO_PEDIDO_VENDA              = E.TIPO_PEDIDO
      WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO 


IF NOT EXISTS (SELECT 1 FROM @OPERACAO_FISCAL_PEDIDO_VAREJO )
    BEGIN 
        SELECT @OPERACAO_FISCAL_PEDIDO_VAREJO_VENDA   = MAX(A.OPERACAO_FISCAL)
          FROM PARAMETROS_VENDAS     A WITH(NOLOCK)
          JOIN EMPRESAS_USUARIAS     B WITH(NOLOCK) ON B.EMPRESA_USUARIA     = A.EMPRESA_USUARIA
                                                   AND B.EMPRESA_USUARIA     = @EMPRESA
    


        SELECT @OPERACAO_FISCAL_PEDIDO_VAREJO_TRANSF = MAX(A.OPERACAO_TRANSF)
          FROM PARAMETROS_VENDAS A WITH(NOLOCK)
          JOIN EMPRESAS_USUARIAS B WITH(NOLOCK) ON  B.EMPRESA_USUARIA = A.EMPRESA_USUARIA
                                                AND B.EMPRESA_USUARIA = @EMPRESA
      END 
      
     -------------------------------------------------------------------
     -- DEFINE ALIQUOTAS DE ICMS --
     -------------------------------------------------------------------             

     --ALIQUOTA INTERNA--

     SELECT @ALIQUOTA_INTERNA = B.ALIQUOTA_ICMS 
       FROM ALIQUOTAS_ICMS_INTERESTADUAIS           A WITH(NOLOCK)
       JOIN ALIQUOTAS_ICMS_INTERESTADUAIS_DESTINOS  B WITH(NOLOCK) ON B.ALIQUOTA_INTERESTADUAL = A.ALIQUOTA_INTERESTADUAL
                                                                   AND A.ESTADO                 = B.ESTADO_DESTINO
  
      WHERE A.ESTADO = ISNULL(@ESTADO_EMPRESA, @ESTADO_PADRAO)

     IF @ALIQUOTA_INTERNA IS NULL SET @ALIQUOTA_INTERNA = 0

     -------------------------------------------------------------------
     -- DEFINE ENTIDADE DA EMPRESA USUARIA
     -------------------------------------------------------------------      

     SELECT @ENTIDADE_EMPRESA = A.ENTIDADE
       FROM EMPRESAS_USUARIAS A WITH(NOLOCK) 
      WHERE A.EMPRESA_USUARIA = @EMPRESA

     --------------------------------
     -- DELETA TABELAS TEMPORARIAS --
     --------------------------------

     IF OBJECT_ID('TEMPDB..#NF_FATURAMENTO'        ) IS NOT NULL DROP TABLE #NF_FATURAMENTO
     IF OBJECT_ID('TEMPDB..#PRODUTOS_CONFERIDOS'   ) IS NOT NULL DROP TABLE #PRODUTOS_CONFERIDOS
     IF OBJECT_ID('TEMPDB..#NF_FATURAMENTO_MASTER' ) IS NOT NULL DROP TABLE #NF_FATURAMENTO_MASTER
     IF OBJECT_ID('TEMPDB..#NF_FATURAMENTO_ITENS'  ) IS NOT NULL DROP TABLE #NF_FATURAMENTO_ITENS
     IF OBJECT_ID('TEMPDB..#ITENS_PEDIDOS'         ) IS NOT NULL DROP TABLE #ITENS_PEDIDOS
     IF OBJECT_ID('TEMPDB..#VENDA_CONTROLADA'      ) IS NOT NULL DROP TABLE #VENDA_CONTROLADA
     IF OBJECT_ID('TEMPDB..#PRECOS_PRODUTOS'       ) IS NOT NULL DROP TABLE #PRECOS_PRODUTOS
    

     -------------------------------------------------------------------
     -- SEPARA ROMANEIOS DE PRODUTOS COM VENDA CONTROLADA             --
     -------------------------------------------------------------------
         SELECT DISTINCT 
                B.ESTOQUE_TRANSFERENCIA
              , CASE WHEN ISNULL(D.VENDA_CONTROLADA, 'N') = 'S'
                     THEN 'S'
                     ELSE 'N'
                END AS VENDA_CONTROLADA

           INTO #VENDA_CONTROLADA

           FROM DEPOSITO_FATURAMENTOS_ITENS       A WITH(NOLOCK) 
     INNER JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA
     INNER JOIN PRODUTOS                          C WITH(NOLOCK) ON C.PRODUTO               = B.PRODUTO
     LEFT  JOIN PRODUTOS_FARMACIA                 D WITH(NOLOCK) ON D.PRODUTO               = B.PRODUTO

          WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO
            AND B.CONFERENCIA          > 0
                                
     -------------------------------------------------------------------
     -- GERACAO DA NOTA FISCAL --
     -------------------------------------------------------------------
        SELECT DISTINCT     	
               @FORMULARIO_ORIGEM               AS FORMULARIO_ORIGEM
             , @TAB_MASTER_ORIGEM               AS TAB_MASTER_ORIGEM
             , @DEPOSITO_FATURAMENTO            AS DEPOSITO_FATURAMENTO
             , @EMPRESA                         AS EMPRESA
             , GETDATE()                        AS DATA_HORA
             , H.USUARIO_LOGADO                 AS USUARIO_LOGADO

             , CASE WHEN K.PEDIDO_VENDA IS NOT NULL
                    THEN K.ENTIDADE
                    ELSE ISNULL(A.ENTIDADE, F.ENTIDADE) END  AS ENTIDADE
             , ISNULL(A.EMPRESA_DESTINO,0)      AS EMPRESA_DESTINO
             , @EMISSAO                         AS EMISSAO
             , @EMISSAO                         AS MOVIMENTO
             , CASE WHEN @OPERACAO_FISCAL_PEDIDO_VAREJO_MANUAL IS NOT NULL
                    THEN @OPERACAO_FISCAL_PEDIDO_VAREJO_MANUAL                                        
                    ELSE 
                    CASE WHEN L.OPERACAO_FISCAL IS NOT NULL
                              THEN L.OPERACAO_FISCAL
                    ELSE 
                    CASE WHEN M.OPERACAO_FISCAL IS NOT NULL
                              THEN M.OPERACAO_FISCAL
                              ELSE CASE WHEN ( ISNULL(F.EMPRESA_CONTABIL,0) <> ISNULL(G.EMPRESA_CONTABIL,0) )
                                        THEN   @OPERACAO_FISCAL_PEDIDO_VAREJO_VENDA
                                        ELSE   @OPERACAO_FISCAL_PEDIDO_VAREJO_TRANSF
                                   END
                            END
                         END
               END                              AS OPERACAO_FISCAL
             , 0                                AS NF_NUMERO
             , @NF_ESPECIE                      AS NF_ESPECIE
             , @NF_SERIE                        AS NF_SERIE
             , NULL                             AS PEDIDO_CLIENTE
             , A.DEPOSITO_FATURAMENTO_ITEM      AS ORDEM_FATURAMENTO
             , I.VENDA_CONTROLADA               AS VENDA_CONTROLADA
             , B.CENTRO_ESTOQUE_DESTINO         AS CENTRO_ESTOQUE_DESTINO
             , B.CENTRO_ESTOQUE_ORIGEM          AS CENTRO_ESTOQUE_ORIGEM
             , J.SOLICITACAO_FATURAMENTO        AS SOLICITACAO_FATURAMENTO
             , J.PEDIDO_VENDA                   AS PEDIDO_VENDA
             , ISNULL(D.CONTRIBUINTE_ICMS, 'N') AS CONTRIBUINTE_ICMS
			 , A.ESTOQUE_TRANSFERENCIA
			 , K.VENDEDOR
			 , K.OPERADOR  --INCLUSO POR �TALO M. VIEIRA DIA 05/05/18 PARA SABER QUAL OPERADOR DIGITOU O PEDIDO - CODIGO IR� SER INSERIDO TB NAS VENDAS_ANALITICAS
			 , D.NFE_CONSUMIDOR_FINAL
			 , D.NFE_TIPO_INDICADOR_IE
             , K1.PEDIDO_VENDA                  AS PEDIDO_PAI
			 , J1.SOLICITACAO_FATURAMENTO       AS SOLICITACAO_PAI                					       
          
		  INTO #NF_FATURAMENTO

          FROM DEPOSITO_FATURAMENTOS_ITENS       A WITH(NOLOCK)
          JOIN ESTOQUE_TRANSFERENCIAS            B WITH(NOLOCK)  ON B.ESTOQUE_TRANSFERENCIA                = A.ESTOQUE_TRANSFERENCIA
     LEFT JOIN #ULTIMA_NF                        C WITH(NOLOCK)  ON C.ESTOQUE_TRANSFERENCIA                = B.ESTOQUE_TRANSFERENCIA                        
     LEFT JOIN ENTIDADES                         D WITH(NOLOCK)  ON D.ENTIDADE                             = A.ENTIDADE
     LEFT JOIN EMPRESAS_USUARIAS                 F WITH(NOLOCK)  ON F.ENTIDADE                             = A.ENTIDADE
          JOIN DEPOSITO_FATURAMENTOS             H WITH(NOLOCK)  ON H.DEPOSITO_FATURAMENTO                 = A.DEPOSITO_FATURAMENTO
          JOIN EMPRESAS_USUARIAS                 G WITH(NOLOCK)  ON G.EMPRESA_USUARIA                      = @EMPRESA
          JOIN #VENDA_CONTROLADA                 I WITH(NOLOCK)  ON I.ESTOQUE_TRANSFERENCIA                = A.ESTOQUE_TRANSFERENCIA
     LEFT JOIN MOTIVOS_TRANSFERENCIAS            M WITH(NOLOCK)  ON M.MOTIVO                               = B.MOTIVO
     LEFT JOIN SOLICITACOES_FATURAMENTOS         J WITH(NOLOCK)  ON J.SOLICITACAO_FATURAMENTO              = B.SOLICITACAO_FATURAMENTO
     LEFT JOIN PEDIDOS_VENDAS                    K WITH(NOLOCK)  ON K.PEDIDO_VENDA                         = J.PEDIDO_VENDA
     LEFT JOIN @OPERACAO_FISCAL_PEDIDO_VAREJO    L               ON A.ESTOQUE_TRANSFERENCIA                = L.ROMANEIO
	 LEFT JOIN PEDIDOS_VENDAS                    K1 WITH(NOLOCK) ON K1.PEDIDO_PAI                          = J.PEDIDO_VENDA
	 LEFT JOIN SOLICITACOES_FATURAMENTOS         J1 WITH(NOLOCK) ON J1.PEDIDO_VENDA                        = K1.PEDIDO_VENDA

         WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO
           AND C.NF_FATURAMENTO IS NULL           


      --------------------------------------------------------------
      --DEFINICAO SE O CLIENTE � OU N�O CONTRINUINTE DO ICMS
      --------------------------------------------------------------

      UPDATE #NF_FATURAMENTO
       
         SET CONTRIBUINTE_ICMS  = 'N'

        FROM PESSOAS_FISICAS  A WITH(NOLOCK)
        JOIN #NF_FATURAMENTO  B WITH(NOLOCK) ON B.ENTIDADE = A.ENTIDADE


     IF NOT EXISTS (SELECT TOP 1 1
                      FROM #NF_FATURAMENTO A )
     BEGIN

          SELECT @MENSAGEM = 'N�o H� Dados para Gera��o da Nota Fiscal'

          RAISERROR ( @MENSAGEM,15,-1)
          RETURN      
     END 


     ----------------------------------------
     -- GERA TABELA COM PRE�OS DE PRODUTOS --
     ----------------------------------------
     SELECT B.ESTOQUE_TRANSFERENCIA                                       AS ESTOQUE_TRANSFERENCIA,
            E.PRODUTO                                                     AS PRODUTO,

            'N'                                                           AS CONVERTER_FATOR_NF,

            ISNULL(J.PRECO_MAXIMO, E.VALOR_UNITARIO)                      AS VALOR_UNITARIO,


           -- E.TOTAL_PRODUTO / ( CASE WHEN  ISNULL(E.CONVERTER_FATOR_NF, 'N') = 'S'
			        --                 THEN  E.QUANTIDADE * G.FATOR_FATURAMENTO
								   --  ELSE  E.QUANTIDADE
							    --END )                                     AS VALOR_UNITARIO,        
								    
            0.00                                                          AS DESCONTO,


		    ISNULL(J.PRECO_MAXIMO,E.VALOR_UNITARIO)                       AS VALOR_UNITARIO_BRUTO,


           -- E.TOTAL_PRODUTO / ( CASE WHEN  ISNULL(E.CONVERTER_FATOR_NF, 'N') = 'S'
			        --                 THEN  E.QUANTIDADE * G.FATOR_FATURAMENTO
								   --  ELSE  E.QUANTIDADE
							    --END )                                     AS VALOR_UNITARIO_BRUTO,
            
            CASE WHEN ISNULL(F.VENDA_CONTROLADA, 'N') = 'S'
                 THEN 'S'
                 ELSE 'N'
            END                                             AS VENDA_CONTROLADA,
            1                                               AS TIPO_FATURAMENTO,
            E.COMISSAO                                      AS COMISSAO
        
       INTO #PRECOS_PRODUTOS
  
       FROM DEPOSITO_FATURAMENTOS_ITENS            A WITH(NOLOCK)
       JOIN ESTOQUE_TRANSFERENCIAS                 B WITH(NOLOCK) ON A.ESTOQUE_TRANSFERENCIA           = B.ESTOQUE_TRANSFERENCIA
       JOIN SOLICITACOES_FATURAMENTOS              C WITH(NOLOCK) ON B.SOLICITACAO_FATURAMENTO         = C.SOLICITACAO_FATURAMENTO
       JOIN PEDIDOS_VENDAS                         D WITH(NOLOCK) ON C.PEDIDO_VENDA                    = D.PEDIDO_VENDA
       JOIN VW_PEDIDOS_VENDAS_PRODUTOS_MOEDA_REAL  E WITH(NOLOCK) ON D.PEDIDO_VENDA                    = E.PEDIDO_VENDA
  LEFT JOIN PRODUTOS_FARMACIA                      F WITH(NOLOCK) ON F.PRODUTO                         = E.PRODUTO
       JOIN PRODUTOS                               G WITH(NOLOCK) ON G.PRODUTO                         = E.PRODUTO
	   JOIN DEPOSITO_FATURAMENTOS                  H WITH(NOLOCK) ON H.DEPOSITO_FATURAMENTO            = A.DEPOSITO_FATURAMENTO
  LEFT JOIN OPERACOES_FISCAIS                      I WITH(NOLOCK) ON I.OPERACAO_FISCAL                 = H.OPERACAO_FISCAL_MANUAL
  LEFT JOIN TABELAS_PRECOS_MAXIMO_PRODUTOS         J WITH(NOLOCK) ON J.TABELA_PRECO_MAXIMO             = I.TABELA_PRECO_MAXIMO
                                                                 AND J.PRODUTO                         = E.PRODUTO
 
 WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO
   AND E.QUANTIDADE <> 0 


	UNION ALL
     
     
        SELECT A.ESTOQUE_TRANSFERENCIA                                                                       AS ESTOQUE_TRANSFERENCIA
             , C.PRODUTO                                                                                     AS PRODUTO
			 , 'N'                                                                                           AS CONVERTER_FATOR_NF

             , MAX( CASE WHEN DBO.MAIOR_ZERO ( ISNULL( E.CUSTO_CONTABIL,0), ISNULL(D.ULTIMO_CUSTO, 0 )  )  > 0 
                         THEN DBO.MAIOR_ZERO ( ISNULL( E.CUSTO_CONTABIL,0), ISNULL(D.ULTIMO_CUSTO, 0 )  ) 
                         ELSE DBO.MAIOR_ZERO ( ISNULL( D.ULTIMO_CUSTO  ,0), ISNULL(G.PRECO_FABRICA,0 )  ) 
                    END)                                                                                     AS VALOR_UNITARIO
             , 0.00                                                                                          AS DESCONTO
             , MAX( CASE WHEN DBO.MAIOR_ZERO ( ISNULL( E.CUSTO_CONTABIL,0), ISNULL(D.ULTIMO_CUSTO, 0 )  )  > 0 
                         THEN DBO.MAIOR_ZERO ( ISNULL( E.CUSTO_CONTABIL,0), ISNULL(D.ULTIMO_CUSTO, 0 )  ) 
                         ELSE DBO.MAIOR_ZERO ( ISNULL( D.ULTIMO_CUSTO  ,0), ISNULL(G.PRECO_FABRICA,0 )  ) 
                    END)                                                                                     AS VALOR_UNITARIO_BRUTO

             , CASE WHEN ISNULL(H.VENDA_CONTROLADA, 'N') = 'S'
                    THEN 'S'
                    ELSE 'N'
               END                                                                                           AS VENDA_CONTROLADA
             , 2                                                                                             AS TIPO_FATURAMENTO
             , 0                                                                                             AS COMISSAO

          FROM DEPOSITO_FATURAMENTOS_ITENS       A WITH(NOLOCK)
          JOIN ESTOQUE_TRANSFERENCIAS            B WITH(NOLOCK) ON  A.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA
          JOIN ESTOQUE_TRANSFERENCIAS_PRODUTOS   C WITH(NOLOCK) ON  A.ESTOQUE_TRANSFERENCIA = C.ESTOQUE_TRANSFERENCIA
     LEFT JOIN ULTIMO_CUSTO                      D WITH(NOLOCK) ON  D.PRODUTO               = C.PRODUTO
          JOIN PRODUTOS                          F WITH(NOLOCK) ON  F.PRODUTO               = C.PRODUTO
     LEFT JOIN CUSTO_MEDIO_MENSAL                E WITH(NOLOCK) ON  E.PRODUTO               = C.PRODUTO
                                                                AND E.MES                   = MONTH(@EMISSAO)
                                                                AND E.ANO                   = YEAR (@EMISSAO)
     LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS      G WITH(NOLOCK) ON  G.PRODUTO               = C.PRODUTO   
                                                                AND G.EMPRESA               = @EMPRESA
     LEFT JOIN PRODUTOS_FARMACIA                 H WITH(NOLOCK) ON  H.PRODUTO               = C.PRODUTO      
     LEFT JOIN SOLICITACOES_FATURAMENTOS         I WITH(NOLOCK) ON  B.SOLICITACAO_FATURAMENTO = I.SOLICITACAO_FATURAMENTO

         WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO
           AND I.PEDIDO_VENDA IS NULL

      GROUP BY A.ESTOQUE_TRANSFERENCIA
             , C.PRODUTO
             , CASE WHEN ISNULL(H.VENDA_CONTROLADA, 'N') = 'S'
                    THEN 'S'
                    ELSE 'N' END


     -------------------------------------------------------------------
     -- CHEGA PRODUTOS SEM PRECO HERE ERROR
     -------------------------------------------------------------------

     SET @MENSAGEM = ''


     IF EXISTS ( SELECT TOP 1 1
                   FROM #PRECOS_PRODUTOS A 
                  WHERE A.VALOR_UNITARIO = 0  

                  )
     BEGIN

          SELECT @MENSAGEM = 'Produto ' + CAST(A.PRODUTO AS VARCHAR(20)) + ' - ' + B.DESCRICAO + ' Est� sem Pre�o'
            FROM #PRECOS_PRODUTOS  A 
            JOIN PRODUTOS          B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO
           WHERE A.VALOR_UNITARIO = 0  

          RAISERROR ( @MENSAGEM,15,-1)
          RETURN      
     END 


     IF NOT EXISTS (SELECT TOP 1 1
                      FROM #PRECOS_PRODUTOS A )
     BEGIN

          SELECT @MENSAGEM = 'Nenhum Produto possui Pre�o'

          RAISERROR ( @MENSAGEM,15,-1)
          RETURN      
     END 
     

     -------------------------------------------------
     -- GERA TABELA RESUMIDA DE PRODUTOS CONFERIDOS --
     -------------------------------------------------
        SELECT Y.PRODUTO                                 AS PRODUTO
             , Y.LOTE_VALIDADE                           AS LOTE_VALIDADE
             , A.ESTOQUE_TRANSFERENCIA                   AS ESTOQUE_TRANSFERENCIA
             , C.VALOR_UNITARIO                          AS VALOR_UNITARIO
             , C.DESCONTO                                AS DESCONTO
													     
             , ROUND(Y.CONFERENCIA,4)                    AS CONFERENCIA

             , ROUND(Y.CONFERENCIA,4)                    AS QUANTIDADE
 
             , ROUND(Y.CONFERENCIA_2,4)                  AS QUANTIDADE_ESTOQUE

             , ROUND(Y.CONFERENCIA,4) * C.VALOR_UNITARIO AS TOTAL_PRODUTOS

             , A.CENTRO_ESTOQUE_ORIGEM                   AS CENTRO_ESTOQUE_ORIGEM
             , C.VENDA_CONTROLADA                        AS VENDA_CONTROLADA
             , C.TIPO_FATURAMENTO                        AS TIPO_FATURAMENTO
             , C.VALOR_UNITARIO_BRUTO                    AS VALOR_UNITARIO_BRUTO
			 , C.CONVERTER_FATOR_NF                      AS CONVERTER_FATOR_NF

			 , CASE WHEN C.CONVERTER_FATOR_NF = 'S'
			        THEN D.UNIDADE_MEDIDA_FATOR
					ELSE D.UNIDADE_MEDIDA
			   END AS UNIDADE_MEDIDA
			 
			 , ISNULL(Y.PESO_BRUTO_CONFERIDO,0)       AS PESO_BRUTO_CONFERIDO
			 , ISNULL(Y.PESO_LIQUIDO_CONFERIDO,0)     AS PESO_LIQUIDO_CONFERIDO
   
                 
          INTO #PRODUTOS_CONFERIDOS
                            
          FROM ESTOQUE_TRANSFERENCIAS A WITH(NOLOCK)
          JOIN (    SELECT A.ESTOQUE_TRANSFERENCIA
                        , A.PRODUTO
						--, NULL AS LOTE_VALIDADE
                        , A.LOTE_VALIDADE
                        , SUM( 
						      --ROUND ( CASE WHEN ISNULL(E.FATOR,1) <> 1 THEN A.CONFERENCIA / ISNULL(F.FATOR_VENDA,1) ELSE A.CONFERENCIA END ,0 , 1 ) ) AS CONFERENCIA -- ALTERADO 12092017
							  ROUND ( CASE WHEN ISNULL(( ISNULL(B.MENOR_UNIDADE_VENDA,1) / F.FATOR_VENDA ),1) <> 1 THEN A.CONFERENCIA * ISNULL(( ISNULL(B.MENOR_UNIDADE_VENDA,1) / F.FATOR_VENDA ),1) ELSE A.CONFERENCIA END ,0 , 0 ) ) AS CONFERENCIA

						, SUM( 
						       CASE WHEN C.TIPO_CONTROLE_UNIDADE = 1  --UNIDADE--
						            THEN 0.00
									ELSE 0.00
							   END) AS PESO_BRUTO_CONFERIDO

						, SUM( 
						       CASE WHEN C.TIPO_CONTROLE_UNIDADE = 1  --UNIDADE--
						            THEN 0.00
									ELSE 0.00
							   END) AS PESO_LIQUIDO_CONFERIDO
                        , SUM( A.CONFERENCIA ) AS CONFERENCIA_2


                     FROM ESTOQUE_TRANSFERENCIAS_TRANSACOES A WITH(NOLOCK)
					 JOIN PRODUTOS                          B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO
					 JOIN UNIDADES_MEDIDA                   C WITH(NOLOCK) ON C.UNIDADE_MEDIDA = B.UNIDADE_MEDIDA
					 JOIN ESTOQUE_TRANSFERENCIAS            D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA
					 LEFT
					 JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA  E WITH(NOLOCK) ON E.PEDIDO_VENDA          = D.PEDIDO_VENDA
                                                                          AND E.PRODUTO               = A.PRODUTO
                     LEFT
					 JOIN FATORES_APRESENTACOES             F WITH(NOLOCK) ON F.FATOR_APRESENTACAO    = E.FATOR_APRESENTACAO

                    WHERE A.CONFERENCIA            > 0 
                      AND A.ESTOQUE_TRANSFERENCIA IN ( SELECT DISTINCT A.ESTOQUE_TRANSFERENCIA FROM #NF_FATURAMENTO A  )

                 GROUP BY A.ESTOQUE_TRANSFERENCIA
                        , A.PRODUTO
						, A.LOTE_VALIDADE


               )                      Y              ON  Y.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA
          LEFT
		  JOIN SOLICITACOES_FATURAMENTOS_PRODUTOS V WITH(NOLOCK) ON V.SOLICITACAO_FATURAMENTO               = A.SOLICITACAO_FATURAMENTO
                                                                AND ISNULL(V.PRODUTO_SUBSTITUTO, V.PRODUTO) = Y.PRODUTO

		  LEFT
		  JOIN #PRECOS_PRODUTOS                   C WITH(NOLOCK) ON  C.ESTOQUE_TRANSFERENCIA = Y.ESTOQUE_TRANSFERENCIA
                                                                 AND C.PRODUTO               = ISNULL(V.PRODUTO, Y.PRODUTO )
  									              
          LEFT
		  JOIN PRODUTOS                           D WITH(NOLOCK) ON  D.PRODUTO               = C.PRODUTO



     ----------------------------------------------------------------
     -- GERACAO ITENS DO PEDIDO - PARA USAR NOS CALCULOS SEGUINTES --
     ----------------------------------------------------------------

         SELECT IDENTITY(INT,1,1)                                                                           AS ID                    
              , C.ESTOQUE_TRANSFERENCIA                                                                     AS ESTOQUE_TRANSFERENCIA
              , C.PRODUTO                                                                                   AS PRODUTO
              , C.VALOR_UNITARIO                                                                            AS VALOR_UNITARIO
              , C.QUANTIDADE                                                                                AS QUANTIDADE
              , 1                                                                                           AS QUANTIDADE_EMBALAGEM
              , C.QUANTIDADE_ESTOQUE                                                                        AS QUANTIDADE_ESTOQUE
			  , C.UNIDADE_MEDIDA                                                                            AS UNIDADE_MEDIDA
              , ( C.VALOR_UNITARIO * C.QUANTIDADE ) - ( C.TOTAL_PRODUTOS )                                  AS TOTAL_DESCONTO
              , C.TOTAL_PRODUTOS                                                                            AS TOTAL_PRODUTO
              , ISNULL (X.SITUACAO_TRIBUTARIA, E.SITUACAO_TRIBUTARIA )                                      AS SITUACAO_TRIBUTARIA
			  , 'N'                                                                                         AS IPI_ICMS
              , C.TOTAL_PRODUTOS                                                                            AS TOTAL_GERAL
              , C.LOTE_VALIDADE                                                                             AS LOTE_VALIDADE
              , C.CENTRO_ESTOQUE_ORIGEM                                                                     AS CENTRO_ESTOQUE_ORIGEM
              , C.VENDA_CONTROLADA                                                                          AS VENDA_CONTROLADA
              , I.NCM                                                                                       AS CLASSIF_FISCAL_CODIGO
			  , X.TIPO_ICMS                                                                                 AS TIPO_ICMS
			  , A.OPERACAO_FISCAL                                                                           AS OPERACAO_FISCAL_ITEM
              , L.GRUPO_TRIBUTARIO
              , X.GRUPO_TRIBUTARIO_PARAMETRO
			  , C.PESO_BRUTO_CONFERIDO
			  , C.PESO_LIQUIDO_CONFERIDO

           INTO #ITENS_PEDIDOS

           FROM #NF_FATURAMENTO                         A WITH(NOLOCK)
     INNER JOIN #PRODUTOS_CONFERIDOS                    C WITH(NOLOCK) ON  C.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA
                                                                       AND C.VENDA_CONTROLADA       = A.VENDA_CONTROLADA   

     LEFT  JOIN PRODUTOS_PARAMETROS_EMPRESAS            E WITH(NOLOCK) ON  E.PRODUTO                = C.PRODUTO
                                                                       AND E.EMPRESA                = A.EMPRESA
     INNER JOIN PRODUTOS                                I WITH(NOLOCK) ON  I.PRODUTO                = C.PRODUTO
     LEFT  JOIN GRUPOS_PRODUTOS                         K WITH(NOLOCK) ON  K.GRUPO_PRODUTO          = I.GRUPO_PRODUTO
           JOIN ENTIDADES                               F WITH(NOLOCK) ON  F.ENTIDADE               = A.ENTIDADE
     LEFT  JOIN ENDERECOS                               G WITH(NOLOCK) ON  G.ENTIDADE               = F.ENTIDADE
     LEFT  JOIN ESTADOS                                 H WITH(NOLOCK) ON  H.ESTADO                 = ISNULL(G.ESTADO, @ESTADO_PADRAO)
     LEFT  JOIN GRUPOS_TRIBUTARIOS                      L WITH(NOLOCK) ON  L.GRUPO_TRIBUTARIO       = I.GRUPO_TRIBUTARIO                                                                            
     LEFT  JOIN GRUPOS_TRIBUTARIOS_PARAMETROS           X WITH(NOLOCK) ON  X.GRUPO_TRIBUTARIO       = I.GRUPO_TRIBUTARIO
                                                                       AND X.OPERACAO_FISCAL        = A.OPERACAO_FISCAL
                                                                       AND X.ESTADO_ORIGEM          = @ESTADO_EMPRESA
                                                                       AND X.ESTADO_DESTINO         = CASE WHEN (A.CONTRIBUINTE_ICMS = 'S'
																	                                         OR G.ESTADO = 'EX')
                                                                                                           THEN G.ESTADO
                                                                                                           ELSE @ESTADO_EMPRESA
                                                                                                      END


     -------------------------------------------------------------------
     -- Checa se a Master NF foi Gerada
     ------------------------------------------------------------------
      IF EXISTS ( SELECT TOP 1 1
                   FROM #NF_FATURAMENTO A 
                  WHERE A.ENTIDADE      IS NULL )
     BEGIN
          RAISERROR ( 'Verifique os Par�metros para gera��o do cabe�alho da NF.',15,-1)
          RETURN      
     END 

     -------------------------------------------------------------------
     -- CHEGA SE A TABELA #ITENS_PEDIDOS FOI GERADA --
     -------------------------------------------------------------------
	 DECLARE @MENSAGEM_GT VARCHAR(1000)


     IF EXISTS ( SELECT TOP 1 1
                   FROM #ITENS_PEDIDOS A 
                  WHERE A.GRUPO_TRIBUTARIO_PARAMETRO IS NULL )
     BEGIN

	     SELECT @MENSAGEM_GT = 'Produto ' + CAST(A.PRODUTO AS VARCHAR(20)) + ' - ' + B.DESCRICAO + ' Est� sem GT para essa Nota'
            FROM #ITENS_PEDIDOS    A 
            JOIN PRODUTOS          B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO
           WHERE A.GRUPO_TRIBUTARIO_PARAMETRO IS NULL


          RAISERROR ( @MENSAGEM_GT,15,-1)
          RETURN      
     END  

     IF NOT EXISTS ( SELECT TOP 1 1 FROM #ITENS_PEDIDOS )
     BEGIN
          RAISERROR ( 'Itens da Nota N�o Gerados, Favor Verificar os Par�mentros Fiscais e De Entidades !!!',15,-1)
          RETURN      
     END 
     
               
     --------------------------------------
     -- GERACAO DOS ITENS DO NOTA FISCAL --
     --------------------------------------

         SELECT B.FORMULARIO_ORIGEM                                                                                 AS FORMULARIO_ORIGEM
              , B.TAB_MASTER_ORIGEM                                                                                 AS TAB_MASTER_ORIGEM
              , A.ESTOQUE_TRANSFERENCIA                                                                             AS ESTOQUE_TRANSFERENCIA
              , A.PRODUTO                                                                                           AS PRODUTO
              , ISNULL(X.ICMS_ST_IVA         , 0   )                                                                AS IVA
              , ISNULL(X.ICMS_ST_IVA         , 0   )                                                                AS IVA_AJUSTADO
              , ISNULL(A.QUANTIDADE          , 1   )                                                                AS QUANTIDADE
              , ISNULL(A.UNIDADE_MEDIDA      , 'UN')                                                                AS UNIDADE_MEDIDA
              , ISNULL(A.QUANTIDADE_EMBALAGEM, 1   )                                                                AS QUANTIDADE_EMBALAGEM
              , ISNULL(A.QUANTIDADE_ESTOQUE  , 1   )                                                                AS QUANTIDADE_ESTOQUE
              , A.OPERACAO_FISCAL_ITEM                                                                              AS OPERACAO_FISCAL
              , A.VALOR_UNITARIO                                                                                    AS VALOR_UNITARIO
              , A.TOTAL_PRODUTO                                                                                     AS TOTAL_PRODUTO
              , 3                                                                                                   AS TIPO_DESCONTO
              , 0.00                                                                                                AS DESCONTO
              , A.TOTAL_DESCONTO                                                                                    AS TOTAL_DESCONTO

              , CASE WHEN X.IPI_ALIQUOTA > 0
                     THEN 1 -- TRIBUTADO
                     ELSE 3 -- OUTROS
                END                                                                                                 AS IPI_TIPO_VENDA
              , X.IPI_BASE_ORIGINAL                                                                                 AS IPI_BASE_ORIGINAL
              ,	X.IPI_REDUCAO_BASE                                                                                  AS IPI_REDUCAO_BASE
              , X.IPI_VALOR_REDUCAO                                                                                 AS IPI_VALOR_REDUCAO
              , X.IPI_BASE_CALCULO                                                                                  AS IPI_BASE_CALCULO
			  , X.SITUACAO_TRIBUTARIA_IPI                                                                           AS SITUACAO_TRIBUTARIA_IPI

              , X.IPI_ALIQUOTA                                                                                      AS ALIQUOTA_IPI
              , X.IPI_VALOR                                                                                         AS IPI_VALOR
              , X.IPI_DEVIDO                                                                                        AS IPI_DEVIDO
              , X.IPI_ISENTO                                                                                        AS IPI_ISENTO
              , X.IPI_OUTROS                                                                                        AS IPI_OUTROS
              , X.IPI_VALOR_CONTABIL                                                                                AS IPI_VALOR_CONTABIL
              , X.CLASSIF_FISCAL_CODIGO                                                                             AS CLASSIF_FISCAL_CODIGO
              , X.CLASSIF_FISCAL_LETRA                                                                              AS CLASSIF_FISCAL_LETRA
              , A.TIPO_ICMS                                                                                         AS ICMS_TIPO_VENDA
              , X.SITUACAO_TRIBUTARIA                                                                               AS SITUACAO_TRIBUTARIA
              , X.ICMS_BASE_ORIGINAL                                                                                AS ICMS_BASE_ORIGINAL
              , X.ICMS_REDUCAO_BASE                                                                                 AS ICMS_REDUCAO_BASE
              , X.ICMS_VALOR_REDUCAO                                                                                AS ICMS_VALOR_REDUCAO

              , X.ICMS_BASE_CALCULO                                                                                 AS ICMS_BASE_CALCULO
              , X.ICMS_ALIQUOTA                                                                                     AS ICMS_ALIQUOTA
              , X.ICMS_OUTROS                                                                                       AS ICMS_OUTROS
              , X.ICMS_ISENTO                                                                                       AS ICMS_ISENTO

              , ISNULL (F.CENTRO_ESTOQUE_TRANSITO, A.CENTRO_ESTOQUE_ORIGEM)                                         AS OBJETO_CONTROLE
              
			  
			  , CASE WHEN G.TIPO_CONTROLE_UNIDADE = 1 --UNIDADE 
			         THEN ISNULL(E.PESO_BRUTO,0) * A.QUANTIDADE_ESTOQUE
					 ELSE ISNULL(A.PESO_BRUTO_CONFERIDO,0)
			    END                                                                                                 AS PESO_BRUTO

			  , CASE WHEN G.TIPO_CONTROLE_UNIDADE = 1  
			         THEN ISNULL(E.PESO_LIQUIDO,0) * A.QUANTIDADE_ESTOQUE
					 ELSE ISNULL(A.PESO_LIQUIDO_CONFERIDO,0)
			    END                                                                                                 AS PESO_LIQUIDO

              , A.TOTAL_GERAL                                                                                       AS TOTAL_LIQUIDO
              , X.PIS_ALIQUOTA                                                                                      AS PIS_ALIQUOTA
              , X.PIS_VALOR                                                                                         AS PIS_VALOR
			  , X.SITUACAO_TRIBUTARIA_PIS                                                                           AS SITUACAO_TRIBUTARIA_PIS
			  , X.PIS_BASE_CALCULO                                                                                  AS PIS_BASE_CALCULO
              , X.COFINS_ALIQUOTA                                                                                   AS COFINS_ALIQUOTA
              , X.COFINS_VALOR                                                                                      AS COFINS_VALOR
              , X.SITUACAO_TRIBUTARIA_COFINS                                                                        AS SITUACAO_TRIBUTARIA_COFINS
			  , X.COFINS_BASE_CALCULO                                                                               AS COFINS_BASE_CALCULO
			  , A.LOTE_VALIDADE                                                                                     AS LOTE_VALIDADE
              , X.ICMS_ST_BASE_CALCULO                                                                              AS ICMS_SUBST_BASE                                                                                
              ,X.ICMS_ST_VALOR                                                                                      AS ICMS_SUBST_VALOR
              ,0.00                                                                                                 AS ICMS_BASE_DECRETO_35346
              ,0.00                                                                                                 AS ICMS_ALIQUOTA_DECRETO_35346
			  ,0.00                                                                                                 AS ICMS_VALOR_DECRETO_35346
              , A.VENDA_CONTROLADA                                                                                  AS VENDA_CONTROLADA
              , 0                                                                                                   AS QUANTIDADE_APRESENTACAO
              , 0                                                                                                   AS QUANTIDADE_APRESENTACAO_SOLICITADA
              , X.ICMS_ST_BASE_CALCULO                                                                              AS ICMS_ST_BASE_CALCULO
              , X.ICMS_ST_IVA                                                                                       AS ICMS_ST_IVA
              , X.ICMS_ST_ALIQUOTA                                                                                  AS ICMS_ST_ALIQUOTA
              , X.ICMS_ST_FATOR_REDUCAO                                                                             AS ICMS_ST_FATOR_REDUCAO
              , 0.00                                                                                                AS VALOR_UNITARIO_BRUTO
              , X.MODALIDADE_BC_ICMS                                                                                AS MODALIDADE_BC_ICMS
			  , X.MODALIDADE_BC_ICMS_ST                                                                             AS MODALIDADE_BC_ICMS_ST

           INTO #NF_FATURAMENTO_ITENS

           FROM #ITENS_PEDIDOS          A WITH(NOLOCK)
           JOIN #NF_FATURAMENTO         B WITH(NOLOCK) ON  B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA
                                                       AND B.VENDA_CONTROLADA      = A.VENDA_CONTROLADA
           JOIN PRODUTOS                D WITH(NOLOCK) ON  D.PRODUTO               = A.PRODUTO
		   JOIN UNIDADES_MEDIDA         G WITH(NOLOCK) ON  G.UNIDADE_MEDIDA        = A.UNIDADE_MEDIDA

     LEFT  JOIN PRODUTOS_ESPECIFICACOES E WITH(NOLOCK) ON  E.PRODUTO               = A.PRODUTO
	 LEFT  JOIN ESTOQUE_TRANSFERENCIAS  F WITH(NOLOCK) ON  F.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA
  
     CROSS APPLY DBO.DADOS_FISCAIS_PRODUTOS ( A.PRODUTO ,
                                              B.EMPRESA ,
                                              B.ENTIDADE ,
                                              A.OPERACAO_FISCAL_ITEM ,
                                              A.TOTAL_PRODUTO  ,
	  	                                      A.IPI_ICMS   ,
                                              A.QUANTIDADE, 
                                              A.TOTAL_DESCONTO) X


     -------------------------------------------------------------------
     -- GERACAO DOS MASTER DA NOTA --
     -------------------------------------------------------------------

     SELECT DISTINCT  
            A.FORMULARIO_ORIGEM                                  AS FORMULARIO_ORIGEM
          , A.TAB_MASTER_ORIGEM                                  AS TAB_MASTER_ORIGEM
          , A.EMPRESA                                            AS EMPRESA
          , A.DATA_HORA                                          AS DATA_HORA
          , A.USUARIO_LOGADO                                     AS USUARIO_LOGADO
          , A.ENTIDADE                                           AS ENTIDADE
          , A.NF_ESPECIE                                         AS NF_ESPECIE
          , @NF_SERIE                                            AS NF_SERIE
          , IDENTITY(INT,1,1)                                    AS NF_NUMERO
          , A.EMISSAO                                            AS EMISSAO
          , A.MOVIMENTO                                          AS MOVIMENTO
          , A.EMISSAO                                            AS SAIDA
          , NULL                                                 AS PEDIDO_CLIENTE
          , A.OPERACAO_FISCAL                                    AS OPERACAO_FISCAL
          , A.DEPOSITO_FATURAMENTO                               AS DEPOSITO_FATURAMENTO
          , B.VENDA_CONTROLADA                                   AS VENDA_CONTROLADA   
          ,ISNULL(A.SOLICITACAO_PAI, A.SOLICITACAO_FATURAMENTO)  AS SOLICITACAO_FATURAMENTO
          ,ISNULL(A.PEDIDO_PAI, A.PEDIDO_VENDA)                  AS PEDIDO_VENDA
		  ,A.VENDEDOR
		  ,A.OPERADOR --INCLUSO POR �TALO M. VIEIRA DIA 05/05/18 PARA SABER QUAL OPERADOR DIGITOU O PEDIDO - CODIGO IR� SER INSERIDO TB NAS VENDAS_ANALITICAS
		  ,A.NFE_CONSUMIDOR_FINAL

       INTO #NF_FATURAMENTO_MASTER

       FROM #NF_FATURAMENTO   A WITH(NOLOCK)
       JOIN #VENDA_CONTROLADA B WITH(NOLOCK)ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA

 
     -------------------------------------------------------------------
     -- GERACAO DOS MASTER DA NOTA --
     -------------------------------------------------------------------

       if object_id('tempdb..#NF_GERADAS') is not null            
       DROP TABLE #NF_GERADAS 
         
       CREATE TABLE #NF_GERADAS (    
                                  NF_FATURAMENTO NUMERIC(15),
                                  PEDIDO_VENDA   NUMERIC(15)
                                ) 


     
    INSERT INTO NF_FATURAMENTO
               ( FORMULARIO_ORIGEM
               , TAB_MASTER_ORIGEM
               , EMPRESA
               , DATA_HORA
               , USUARIO_LOGADO
               , ENTIDADE
               , NF_ESPECIE
               , NF_SERIE
               , NF_NUMERO
               , EMISSAO
               , MOVIMENTO
               , SAIDA
               , OPERACAO_FISCAL
               , OPERACAO_FISCAL_MASTER
               , DEPOSITO_FATURAMENTO
               , VENDA_CONTROLADA
               , NFE_TIPO_EMISSAO
               , NFE_TIPO_FINALIDADE
               , EMITIR_NFE
               , PEDIDO_VENDA
               , SOLICITACAO_FATURAMENTO
			   , VENDEDOR
			   , OPERADOR --INCLUSO POR �TALO M. VIEIRA DIA 05/05/18 PARA SABER QUAL OPERADOR DIGITOU O PEDIDO - CODIGO IR� SER INSERIDO TB NAS VENDAS_ANALITICAS
			   , NFE_CONSUMIDOR_FINAL ) 


    --GERACAO DO OUTPUT --  
    OUTPUT INSERTED.NF_FATURAMENTO,
           INSERTED.PEDIDO_VENDA
  
      INTO #NF_GERADAS  
  
      ( 
        NF_FATURAMENTO,
        PEDIDO_VENDA
      )           

           
     SELECT A.FORMULARIO_ORIGEM
          , A.TAB_MASTER_ORIGEM
          , @DEPOSITO                   AS EMPRESA
          , A.DATA_HORA
          , A.USUARIO_LOGADO
          , A.ENTIDADE
          , A.NF_ESPECIE
          , A.NF_SERIE
          , 0 AS NF_NUMERO
          , A.EMISSAO
          , A.MOVIMENTO
          , A.SAIDA
          , A.OPERACAO_FISCAL
          , A.OPERACAO_FISCAL           AS OPERACAO_FISCAL_MASTER
          , A.DEPOSITO_FATURAMENTO
          , A.VENDA_CONTROLADA
          , CASE WHEN @NF_SERIE = '900'
                 THEN 2
                 ELSE 1   
            END                         AS NFE_TIPO_EMISSAO
          , 1                           AS NFE_TIPO_FINALIDADE
          , 'N'                         AS EMITIR_NFE
          , A.PEDIDO_VENDA    
          , A.SOLICITACAO_FATURAMENTO
		  , A.VENDEDOR
		  , A.OPERADOR --INCLUSO POR �TALO M. VIEIRA DIA 05/05/18 PARA SABER QUAL OPERADOR DIGITOU O PEDIDO - CODIGO IR� SER INSERIDO TB NAS VENDAS_ANALITICAS
		  , A.NFE_CONSUMIDOR_FINAL

       FROM #NF_FATURAMENTO_MASTER A WITH(NOLOCK)


     -------------------------------------------------------------------
     -- GERACAO DA TABELA DE TRANSFERENCIA DA NOTA --
     -------------------------------------------------------------------

     INSERT INTO NF_FATURAMENTO_TRANSF
               ( FORMULARIO_ORIGEM
               , TAB_MASTER_ORIGEM
               , NF_FATURAMENTO
               , ESTOQUE_TRANSFERENCIA )

     SELECT DISTINCT
            C.FORMULARIO_ORIGEM
          , C.TAB_MASTER_ORIGEM
          , C.NF_FATURAMENTO
          , D.ESTOQUE_TRANSFERENCIA 

       FROM #NF_GERADAS             A WITH(NOLOCK)
       JOIN NF_FATURAMENTO          C WITH(NOLOCK) ON C.NF_FATURAMENTO                                = A.NF_FATURAMENTO
	   JOIN #NF_FATURAMENTO         D WITH(NOLOCK) ON D.VENDA_CONTROLADA                              = C.VENDA_CONTROLADA
	                                              AND D.ENTIDADE                                      = C.ENTIDADE
												  AND D.EMPRESA                                       = C.EMPRESA
												  AND ISNULL(ISNULL(D.PEDIDO_PAI, D.PEDIDO_VENDA),0)  = ISNULL(C.PEDIDO_VENDA,0)


     -------------------------------------------------------------------
     -- GERACAO DOS ITENS DA NOTA --
     -----------------------------------------------------------------
     
     INSERT INTO NF_FATURAMENTO_PRODUTOS
               ( FORMULARIO_ORIGEM
               , TAB_MASTER_ORIGEM
               , REG_MASTER_ORIGEM
               , NF_FATURAMENTO
               , PRODUTO
               , QUANTIDADE
               , UNIDADE_MEDIDA
               , QUANTIDADE_EMBALAGEM
               , QUANTIDADE_ESTOQUE
               , OPERACAO_FISCAL
               , VALOR_UNITARIO
               , TIPO_DESCONTO
               , DESCONTO
               , TOTAL_DESCONTO
               , TOTAL_PRODUTO
               , IPI_TIPO_VENDA
               , IPI_BASE_ORIGINAL
               , IPI_REDUCAO_BASE
               , IPI_VALOR_REDUCAO
               , IPI_BASE_CALCULO
               , IPI_ALIQUOTA
               , IPI_VALOR
               , IPI_DEVIDO
               , IPI_ISENTO
               , IPI_OUTROS
               , IPI_VALOR_CONTABIL
			   , SITUACAO_TRIBUTARIA_IPI
               , CLASSIF_FISCAL_CODIGO
               , CLASSIF_FISCAL_LETRA
               , ICMS_TIPO_VENDA
               , SITUACAO_TRIBUTARIA
               , ICMS_BASE_ORIGINAL
               , ICMS_REDUCAO_BASE
               , ICMS_VALOR_REDUCAO
               , ICMS_BASE_CALCULO
               , ICMS_ALIQUOTA
               , ICMS_VALOR
               , ICMS_DEVIDO
               , ICMS_ISENTO
               , ICMS_OUTROS
               , OBJETO_CONTROLE
               , PESO_BRUTO
               , PESO_LIQUIDO
               , MARKUP
               , MARGEM
			   , PIS_BASE_CALCULO
               , PIS_ALIQUOTA
               , PIS_VALOR
			   , SITUACAO_TRIBUTARIA_PIS
			   , COFINS_BASE_CALCULO
               , COFINS_ALIQUOTA
               , COFINS_VALOR
			   , SITUACAO_TRIBUTARIA_COFINS
               , IVA
               , IVA_AJUSTADO
               , LOTE_VALIDADE
               , LOTE
               , VALIDADE_DIGITACAO
               , VALIDADE
               , ICMS_SUBST_BASE
               , ICMS_SUBST_VALOR
               , ICMS_BASE_DECRETO_35346
               , ICMS_ALIQUOTA_DECRETO_35346
               , ICMS_VALOR_DECRETO_35346
               , ESTOQUE_TRANSFERENCIA
               , QUANTIDADE_MULTIPLO
               , ICMS_ST_IVA
               , ICMS_ST_BASE_CALCULO
               , ICMS_ST_ALIQUOTA
               , ICMS_ST_FATOR_REDUCAO
               , ICMS_ST_VALOR 
               , MOD_DETERMINACAO_BC_ICMS
	           , MOD_DETERMINACAO_BC_ICMS_ST
			   -- NFE 4.0 --
               , REGISTRO_MS
			   ,DATA_FABRICACAO
	           )
              
        SELECT A.FORMULARIO_ORIGEM                           AS FORMULARIO_ORIGEM
             , A.TAB_MASTER_ORIGEM                           AS TAB_MASTER_ORIGEM
             , B.NF_FATURAMENTO                              AS REG_MASTER_ORIGEM
             , B.NF_FATURAMENTO                              AS NF_FATURAMENTO
             , A.PRODUTO                                     AS PRODUTO
             , A.QUANTIDADE                                  AS QUANTIDADE
             , A.UNIDADE_MEDIDA                              AS UNIDADE_MEDIDA
             , A.QUANTIDADE_EMBALAGEM                        AS QUANTIDADE_EMBALAGEM
             , A.QUANTIDADE_ESTOQUE * A.QUANTIDADE_EMBALAGEM AS QUANTIDADE_ESTOQUE
             , A.OPERACAO_FISCAL                             AS OPERACAO_FISCAL
             , A.VALOR_UNITARIO                              AS VALOR_UNITARIO
             , A.TIPO_DESCONTO                               AS TIPO_DESCONTO
             , A.DESCONTO                                    AS DESCONTO
             , A.TOTAL_DESCONTO                              AS TOTAL_DESCONTO
             , A.TOTAL_PRODUTO                               AS TOTAL_PRODUTO
             , A.IPI_TIPO_VENDA                              AS IPI_TIPO_VENDA
             , A.IPI_BASE_ORIGINAL                           AS IPI_BASE_ORIGINAL
             , A.IPI_REDUCAO_BASE                            AS IPI_REDUCAO_BASE
             , A.IPI_VALOR_REDUCAO                           AS IPI_VALOR_REDUCAO
             , A.IPI_BASE_CALCULO                            AS IPI_BASE_CALCULO
             , A.ALIQUOTA_IPI                                AS ALIQUOTA_IPI
             , A.IPI_VALOR                                   AS IPI_VALOR
             , A.IPI_DEVIDO                                  AS IPI_DEVIDO
             , A.IPI_ISENTO                                  AS IPI_ISENTO
             , A.IPI_OUTROS                                  AS IPI_OUTROS
             , A.IPI_VALOR_CONTABIL                          AS IPI_VALOR_CONTABIL
			 , A.SITUACAO_TRIBUTARIA_IPI                     AS SITUACAO_TRIBUTARIA_IPI 
             , A.CLASSIF_FISCAL_CODIGO                       AS CLASSIF_FISCAL_CODIGO
             , A.CLASSIF_FISCAL_LETRA                        AS ICMS_TIPO_VENDA
             , A.ICMS_TIPO_VENDA                             AS ICMS_TIPO_VENDA
             , A.SITUACAO_TRIBUTARIA                         AS SITUACAO_TRIBUTARIA
             , A.ICMS_BASE_ORIGINAL                          AS ICMS_BASE_ORIGINAL
             , A.ICMS_REDUCAO_BASE                           AS ICMS_REDUCAO_BASE
             , A.ICMS_VALOR_REDUCAO                          AS ICMS_VALOR_REDUCAO
             , A.ICMS_BASE_CALCULO                           AS ICMS_BASE_CALCULO
             , A.ICMS_ALIQUOTA                               AS ICMS_ALIQUOTA
             , A.ICMS_BASE_CALCULO * (A.ICMS_ALIQUOTA/100)   AS ICMS_VALOR
             , A.ICMS_BASE_CALCULO * (A.ICMS_ALIQUOTA/100)   AS ICMS_DEVIDO
             , A.ICMS_ISENTO                                 AS ICMS_ISENTO
             , A.ICMS_OUTROS                                 AS ICMS_OUTROS
             , A.OBJETO_CONTROLE                             AS OBJETO_CONTROLE
             , A.PESO_BRUTO                                  AS PESO_BRUTO
             , A.PESO_LIQUIDO                                AS PESO_LIQUIDO
             , 0                                             AS MARKUP
             , 0                                             AS MARGEM
			 , A.PIS_BASE_CALCULO                            AS PIS_BASE_CALCULO                  
             , A.PIS_ALIQUOTA                                AS PIS_ALIQUOTA
             , A.PIS_VALOR                                   AS PIS_VALOR
			 , A.SITUACAO_TRIBUTARIA_PIS                     AS SITUACAO_TRIBUTARIA_PIS
			 , A.COFINS_BASE_CALCULO                         AS COFINS_BASE_CALCULO                  
             , A.COFINS_ALIQUOTA                             AS COFINS_ALIQUOTA
             , A.COFINS_VALOR                                AS COFINS_VALOR
			 , A.SITUACAO_TRIBUTARIA_COFINS                  AS SITUACAO_TRIBUTARIA_COFINS
             , A.IVA                                         AS IVA
             , A.IVA_AJUSTADO                                AS IVA_AJUSTADO
             , E.LOTE_VALIDADE                               AS LOTE_VALIDADE
             , E.LOTE                                        AS LOTE
             , E.VALIDADE_DIGITACAO                          AS VALIDADE_DIGITACAO
             , E.VALIDADE                                    AS VALIDADE
             , A.ICMS_SUBST_BASE                             AS ICMS_SUBST_BASE
             , A.ICMS_SUBST_VALOR                            AS ICMS_SUBST_VALOR
             , A.ICMS_BASE_DECRETO_35346                     AS ICMS_BASE_DECRETO_35346
             , A.ICMS_ALIQUOTA_DECRETO_35346                 AS ICMS_ALIQUOTA_DECRETO_35346
             , A.ICMS_VALOR_DECRETO_35346                    AS ICMS_VALOR_DECRETO_35346
             , A.ESTOQUE_TRANSFERENCIA                       AS ESTOQUE_TRANSFERENCIA
             , 1                                             AS QUANTIDADE_MULTIPLO
             , A.ICMS_ST_IVA                                 AS ICMS_ST_IVA
             , A.ICMS_ST_BASE_CALCULO                        AS ICMS_ST_BASE_CALCULO
             , A.ICMS_ST_ALIQUOTA                            AS ICMS_ST_ALIQUOTA
             , A.ICMS_ST_FATOR_REDUCAO                       AS ICMS_ST_FATOR_REDUCAO               
             , A.ICMS_SUBST_VALOR                            AS ICMS_ST_VALOR
             , A.MODALIDADE_BC_ICMS                          AS MOD_DETERMINACAO_BC_ICMS
			 , A.MODALIDADE_BC_ICMS_ST                       AS MOD_DETERMINACAO_BC_ICMS_ST
			 -- NFE 4.0 --
             , J.REGISTRO_MS                                 AS REGISTRO_MS
			 , E.VALIDADE - ISNULL(L.PRAZO_VALIDADE_DIAS,0)  AS DATA_FABRICACAO

          FROM NF_FATURAMENTO                    B WITH(NOLOCK)
          JOIN #NF_GERADAS                       X WITH(NOLOCK) ON X.NF_FATURAMENTO        = B.NF_FATURAMENTO
		  JOIN NF_FATURAMENTO_TRANSF             Y WITH(NOLOCK) ON Y.NF_FATURAMENTO        = B.NF_FATURAMENTO
          JOIN #NF_FATURAMENTO_ITENS             A WITH(NOLOCK) ON A.ESTOQUE_TRANSFERENCIA = Y.ESTOQUE_TRANSFERENCIA
                                                               AND A.VENDA_CONTROLADA      = B.VENDA_CONTROLADA
          JOIN PRODUTOS                          C WITH(NOLOCK) ON A.PRODUTO               = C.PRODUTO 
     LEFT JOIN PRODUTOS_LOTE_VALIDADE            E WITH(NOLOCK) ON E.LOTE_VALIDADE         = A.LOTE_VALIDADE
	 LEFT JOIN ESTOQUE_TRANSFERENCIAS            F WITH(NOLOCK) ON F.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA
	 -- NFE 4.0 --
     LEFT JOIN PRODUTOS_FARMACIA                 J WITH(NOLOCK) ON J.PRODUTO               = C.PRODUTO 
	 LEFT JOIN PRODUTOS_RASTREABILIDADE          L WITH(NOLOCK) ON L.PRODUTO               = C.PRODUTO
	 
	  
      ORDER BY B.NF_FATURAMENTO
             , C.DESCRICAO

     -------------------------------------------------------------------
     -- GERACAO DA TABELA DE TOTAIS DA NOTA --
     -------------------------------------------------------------------
     
     INSERT INTO NF_FATURAMENTO_TOTAIS
               ( FORMULARIO_ORIGEM
               , TAB_MASTER_ORIGEM
               , REG_MASTER_ORIGEM
               , NF_FATURAMENTO
               , TOTAL_PRODUTOS
               , TOTAL_IPI
               , SUB_TOTAL
               , TOTAL_SERVICOS
               , TOTAL_FRETE
               , ICMS_FRETE
               , TOTAL_DESPESAS
               , ICMS_DESPESAS
               , TOTAL_SEGURO
               , ICMS_SEGURO
               , TOTAL_GERAL
               , ICMS_BASE_CALCULO_PRODUTOS
               , ICMS_VALOR_PRODUTOS
               , ICMS_BASE_CALCULO
               , ICMS_VALOR
               , ICMS_BASE_SUBST
               , ICMS_VALOR_SUBST
               , TOTAL_PIS_VALOR
               , TOTAL_COFINS_VALOR )

       SELECT B.FORMULARIO_ORIGEM                           AS FORMULARIO_ORIGEM
            , B.TAB_MASTER_ORIGEM                           AS TAB_MASTER_ORIGEM
            , A.NF_FATURAMENTO                              AS REG_MASTER_ORIGEM
            , A.NF_FATURAMENTO                              AS NF_FATURAMENTO
            , SUM(A.TOTAL_PRODUTO)                          AS TOTAL_PRODUTO
            , SUM(A.IPI_VALOR    )                          AS IPI_VALOR
            , SUM(A.TOTAL_PRODUTO)                          AS SUB_TOTAL
            , 0                                             AS TOTAL_SERVICO
            , 0                                             AS TOTAL_FRETE
            , 0                                             AS ICMS_FRETE
            , 0                                             AS TOTAL_DESPESAS
            , 0                                             AS ICMS_DESPESAS
            , 0                                             AS TOTAL_SEGURO
            , 0                                             AS ICMS_SEGURO
            
            , SUM(A.TOTAL_PRODUTO) +
              SUM(A.ICMS_ST_VALOR) +
              SUM(A.IPI_VALOR    )                          AS TOTAL_GERAL
            
            , SUM(CASE WHEN A.ICMS_BASE_DECRETO_35346 = 0
                       THEN A.ICMS_BASE_CALCULO
                       ELSE 0
                  END ) +
              SUM(CASE WHEN A.ICMS_BASE_CALCULO = 0
                       THEN A.ICMS_BASE_DECRETO_35346
                       ELSE 0
                  END )                                     AS ICMS_BASE_CALCULO_PRODUTOS
            , SUM(CASE WHEN A.ICMS_VALOR_DECRETO_35346 = 0
                       THEN A.ICMS_VALOR
                       ELSE 0
                  END ) +
              SUM(CASE WHEN A.ICMS_VALOR = 0
                       THEN A.ICMS_VALOR_DECRETO_35346
                       ELSE 0
                  END )                                     AS ICMS_VALOR_PRODUTOS
            , SUM(CASE WHEN A.ICMS_VALOR_DECRETO_35346 = 0
                       THEN A.ICMS_BASE_CALCULO
                       ELSE 0
                  END )+
              SUM(CASE WHEN A.ICMS_BASE_CALCULO = 0
                       THEN A.ICMS_VALOR_DECRETO_35346
                       ELSE 0
                  END )                                     AS ICMS_BASE_CALCULO
            , SUM(CASE WHEN A.ICMS_VALOR_DECRETO_35346 = 0
                       THEN A.ICMS_VALOR
                       ELSE 0
                  END ) +
              SUM(CASE WHEN A.ICMS_VALOR = 0
                       THEN A.ICMS_VALOR_DECRETO_35346
                       ELSE 0
                  END )                                     AS ICMS_VALOR

            , SUM(A.ICMS_ST_BASE_CALCULO )                  AS ICMS_ST_BASE_CALCULO
            , SUM(A.ICMS_ST_VALOR        )                  AS ICMS_ST_VALOR
            , SUM(A.PIS_VALOR            )                  AS PIS_VALOR
            , SUM(A.COFINS_VALOR         )                  AS COFINS_VALOR

         FROM NF_FATURAMENTO_PRODUTOS A WITH(NOLOCK)
         JOIN NF_FATURAMENTO          B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
         JOIN #NF_GERADAS             X WITH(NOLOCK) ON X.NF_FATURAMENTO = B.NF_FATURAMENTO
   
     GROUP BY A.NF_FATURAMENTO
            , B.FORMULARIO_ORIGEM
            , B.TAB_MASTER_ORIGEM        


-------------------------------------------------
-- INSERCAO DO DADOS DO DESTINATARIO --
-------------------------------------------------

IF
    ISNULL((SELECT ENTIDADE FROM ENDERECOS_ENTREGA 
                WHERE ENTIDADE = @ENTIDADE),0) <> 0
        BEGIN
        
        INSERT INTO NF_FATURAMENTO_ENTIDADE ( 

            FORMULARIO_ORIGEM ,
            TAB_MASTER_ORIGEM ,
            REG_MASTER_ORIGEM ,
            NF_FATURAMENTO ,
            ENTIDADE ,
            NOME ,
            INSCRICAO_FEDERAL ,
            INSCRICAO_ESTADUAL ,
            INSCRICAO_MUNICIPAL ,
            PAIS ,
            CEP ,
            TIPO_ENDERECO ,
            ENDERECO ,
            NUMERO ,
            COMPLEMENTO ,
            BAIRRO ,
            CIDADE ,
            ESTADO ,
            TELEFONE )

SELECT      A.FORMULARIO_ORIGEM ,
            A.TAB_MASTER_ORIGEM ,
            A.REG_MASTER_ORIGEM ,
            A.NF_FATURAMENTO ,
            A.ENTIDADE ,
            C.NOME ,
            C.INSCRICAO_FEDERAL ,
            C.INSCRICAO_ESTADUAL ,
            C.INSCRICAO_MUNICIPAL ,
            B.PAIS ,
            B.CEP ,
            B.TIPO_ENDERECO ,
            B.ENDERECO ,
            B.NUMERO ,
            B.COMPLEMENTO ,
            B.BAIRRO ,
            B.CIDADE ,
            B.ESTADO ,
            NULL AS TELEFONE

 FROM NF_FATURAMENTO                A WITH(NOLOCK)
  JOIN #NF_GERADAS                  X WITH(NOLOCK) ON X.NF_FATURAMENTO   = A.NF_FATURAMENTO
  JOIN ENDERECOS_ENTREGA            B WITH(NOLOCK) ON B.ENTIDADE         = A.ENTIDADE
  JOIN ENTIDADES                    C WITH(NOLOCK) ON C.ENTIDADE         = A.ENTIDADE
  LEFT JOIN NF_FATURAMENTO_ENTIDADE D WITH(NOLOCK) ON D.NF_FATURAMENTO   = A.NF_FATURAMENTO
 WHERE D.NF_FATURAMENTO IS NULL
       
                
        END
ELSE

       BEGIN
        INSERT INTO NF_FATURAMENTO_ENTIDADE ( 

            FORMULARIO_ORIGEM ,
            TAB_MASTER_ORIGEM ,
            REG_MASTER_ORIGEM ,
            NF_FATURAMENTO ,
            ENTIDADE ,
            NOME ,
            INSCRICAO_FEDERAL ,
            INSCRICAO_ESTADUAL ,
            INSCRICAO_MUNICIPAL ,
            PAIS ,
            CEP ,
            TIPO_ENDERECO ,
            ENDERECO ,
            NUMERO ,
            COMPLEMENTO ,
            BAIRRO ,
            CIDADE ,
            ESTADO ,
            TELEFONE )

SELECT      A.FORMULARIO_ORIGEM ,
            A.TAB_MASTER_ORIGEM ,
            A.REG_MASTER_ORIGEM ,
            A.NF_FATURAMENTO ,
            A.ENTIDADE ,
            C.NOME ,
            C.INSCRICAO_FEDERAL ,
            C.INSCRICAO_ESTADUAL ,
            C.INSCRICAO_MUNICIPAL ,
            B.PAIS ,
            B.CEP ,
            B.TIPO_ENDERECO ,
            B.ENDERECO ,
            B.NUMERO ,
            B.COMPLEMENTO ,
            B.BAIRRO ,
            B.CIDADE ,
            B.ESTADO ,
            NULL AS TELEFONE

 FROM NF_FATURAMENTO              A WITH(NOLOCK)
 JOIN #NF_GERADAS                 X WITH(NOLOCK) ON X.NF_FATURAMENTO   = A.NF_FATURAMENTO
 JOIN ENDERECOS                   B WITH(NOLOCK) ON B.ENTIDADE         = A.ENTIDADE
 JOIN ENTIDADES                   C WITH(NOLOCK) ON C.ENTIDADE         = A.ENTIDADE
LEFT JOIN NF_FATURAMENTO_ENTIDADE D WITH(NOLOCK) ON D.NF_FATURAMENTO   = A.NF_FATURAMENTO
WHERE D.NF_FATURAMENTO IS NULL

END





-------------------------------------------------------------------
--DEFINE A TRANSPORTADORA--
-------------------------------------------------------------------

IF OBJECT_ID('TEMPDB..#TRANSPORTADORAS_NF'        ) IS NOT NULL DROP TABLE #TRANSPORTADORAS_NF

SELECT A.NF_FATURAMENTO,       
       ISNULL(B.TRANSPORTADORA, @TRANSPORTADORA ) AS TRANSPORTADORA
       
 INTO #TRANSPORTADORAS_NF

 FROM NF_FATURAMENTO           A WITH(NOLOCK)
 JOIN PEDIDOS_VENDAS           B WITH(NOLOCK) ON B.PEDIDO_VENDA         = A.PEDIDO_VENDA
 JOIN #NF_GERADAS              X WITH(NOLOCK) ON X.NF_FATURAMENTO       = A.NF_FATURAMENTO

-------------------------------------------------------------------
--DEFINE A VOLUMES--
-------------------------------------------------------------------

IF OBJECT_ID('TEMPDB..#TEMP_VOLUMES'        ) IS NOT NULL DROP TABLE #TEMP_VOLUMES

SELECT X.NF_FATURAMENTO,
       SUM(X.VOLUMES) AS VOLUMES,
	   SUM(X.TOTAL_PESO_BRUTO) AS TOTAL_PESO_BRUTO,
	   SUM(X.TOTAL_PESO_LIQUIDO) AS TOTAL_PESO_LIQUIDO

  INTO #TEMP_VOLUMES

  FROM ( 


SELECT A.NF_FATURAMENTO,       
       COUNT(*) AS VOLUMES,
       SUM(ISNULL(E.TOTAL_PESO_BRUTO,0) ) AS TOTAL_PESO_BRUTO,
       SUM(ISNULL(E.TOTAL_PESO_LIQUIDO,0))AS TOTAL_PESO_LIQUIDO
       
 FROM NF_FATURAMENTO                    A WITH(NOLOCK)
 JOIN NF_FATURAMENTO_TRANSF             B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
 JOIN #NF_GERADAS                       C WITH(NOLOCK) ON C.NF_FATURAMENTO = A.NF_FATURAMENTO
 JOIN CONFERENCIAS_ESTOQUE_TRANF        D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA     = B.ESTOQUE_TRANSFERENCIA
 JOIN CONFERENCIAS_ESTOQUE_TRANF_VOLUME E WITH(NOLOCK) ON E.CONFERENCIA_TRANSFERENCIA = D.CONFERENCIA_TRANSFERENCIA

 WHERE D.TIPO_CONFERENCIA <> 3 --COLETOR--
 
 GROUP BY A.NF_FATURAMENTO

 UNION ALL

SELECT A.NF_FATURAMENTO,       
       SUM(ISNULL(F.QUANTIDADE,0)) AS VOLUMES,
       SUM(ISNULL(F.PESO_KG,0))    AS TOTAL_PESO_BRUTO,
       SUM(ISNULL(F.PESO_KG,0))    AS TOTAL_PESO_LIQUIDO
       
 FROM NF_FATURAMENTO                    A WITH(NOLOCK)
 JOIN NF_FATURAMENTO_TRANSF             B WITH(NOLOCK) ON B.NF_FATURAMENTO            = A.NF_FATURAMENTO
 JOIN #NF_GERADAS                       C WITH(NOLOCK) ON C.NF_FATURAMENTO            = A.NF_FATURAMENTO
 JOIN CONFERENCIAS_ESTOQUE_TRANF        D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA     = B.ESTOQUE_TRANSFERENCIA
 JOIN CONFERENCIAS_ESTOQUE_TRANF_MANUAL E WITH(NOLOCK) ON E.CONFERENCIA_TRANSFERENCIA = D.CONFERENCIA_TRANSFERENCIA
 JOIN PRODUTOS_PESAGENS                 F WITH(NOLOCK) ON F.CODIGO_BARRAS             = E.CODIGO_BARRAS

 WHERE D.TIPO_CONFERENCIA = 3 --COLETOR--
 
 GROUP BY A.NF_FATURAMENTO

UNION ALL

SELECT A.NF_FATURAMENTO                     AS NF_FATURAMENTO    ,
       SUM(ISNULL(D.QUANTIDADE_PECA,1))     AS VOLUMES           ,
       SUM(ISNULL(D.TOTAL_PESO_BRUTO,0))    AS TOTAL_PESO_BRUTO  ,
       SUM(ISNULL(D.TOTAL_PESO_BRUTO,0))    AS TOTAL_PESO_LIQUIDO
       
  FROM NF_FATURAMENTO                    A WITH(NOLOCK)
  JOIN #NF_GERADAS                       X WITH(NOLOCK) ON X.NF_FATURAMENTO            = A.NF_FATURAMENTO
  JOIN NF_FATURAMENTO_TRANSF             B WITH(NOLOCK) ON A.NF_FATURAMENTO            = B.NF_FATURAMENTO
  JOIN (
           SELECT DISTINCT CHECKOUT, MAX(ESTOQUE_TRANSFERENCIA) AS ESTOQUE_TRANSFERENCIA
                     FROM WMS_CHECKOUT_RESUMO WITH(NOLOCK)
                 GROUP BY CHECKOUT
       )                                 C              ON B.ESTOQUE_TRANSFERENCIA     = C.ESTOQUE_TRANSFERENCIA
  JOIN WMS_CHECKOUT_VOLUMES              D WITH(NOLOCK) ON C.CHECKOUT                  = D.CHECKOUT

 WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO

 GROUP BY A.NF_FATURAMENTO

 ) X
 
 GROUP BY X.NF_FATURAMENTO 

-------------------------------------------------
-- INSERCAO DO TRANSPORTADOR NF DE FATURAMENTO --
-------------------------------------------------

INSERT INTO NF_FATURAMENTO_TRANSPORTES ( FORMULARIO_ORIGEM ,
                                         TAB_MASTER_ORIGEM ,
                                         REG_MASTER_ORIGEM ,
                                         NF_FATURAMENTO ,
                                         ENTIDADE ,
                                         VOLUME ,
                                         ESPECIE ,
                                         MARCA ,
                                         PESO_BRUTO ,
                                         PESO_LIQUIDO ,
                                         TIPO_FRETE ,
                                         PLACA ,
                                         UF_VEICULO,
                                         CODIGO_ANTT )

                                  SELECT A.FORMULARIO_ORIGEM     AS FORMULARIO_ORIGEM          ,
                                         A.TAB_MASTER_ORIGEM     AS TAB_MASTER_ORIGEM          ,
                                         A.NF_FATURAMENTO        AS REG_MASTER_ORIGEM          ,
                                         A.NF_FATURAMENTO        AS NF_FATURAMENTO    ,

										 ISNULL(G.TRANSPORTADORA,H.ENTIDADE)
										                         AS ENTIDADE          ,  
                                         --CASE WHEN ISNULL(X.VOLUMES,0) = 0
                                         --     THEN SUM(D.QUANTIDADE)
                                         --     ELSE X.VOLUMES END AS VOLUME        ,
										 ISNULL(Z.VOLUME,1)     AS VOLUMES          ,
                                         'CX'                    AS ESPECIE           ,
                                         NULL                    AS MARCA ,
                                         ISNULL(X.TOTAL_PESO_BRUTO, SUM(D.PESO_BRUTO))       
										                         AS PESO_BRUTO        ,
                                         ISNULL(X.TOTAL_PESO_LIQUIDO, SUM(D.PESO_LIQUIDO))     
										                         AS PESO_LIQUIDO      ,

                                         1                       AS TIPO_FRETE        ,
                                         NULL                    AS PLACA             ,
                                         CASE WHEN G.TRANSPORTADORA = @ENTIDADE_EMPRESA
                                              THEN ISNULL(@ESTADO_EMPRESA, @ESTADO_PADRAO)
                                              ELSE ISNULL(E.ESTADO, @ESTADO_PADRAO )
                                         END                     AS UF_VEICULO,
                                         NULL                    AS CODIGO_ANTT
                                        
                                    FROM NF_FATURAMENTO             A WITH(NOLOCK)
                                    JOIN #NF_GERADAS               Y WITH(NOLOCK)ON Y.NF_FATURAMENTO        = A.NF_FATURAMENTO 
                                    JOIN NF_FATURAMENTO_PRODUTOS    D WITH(NOLOCK)ON D.NF_FATURAMENTO        = A.NF_FATURAMENTO
                               LEFT JOIN #TRANSPORTADORAS_NF        G WITH(NOLOCK)ON G.NF_FATURAMENTO        = A.NF_FATURAMENTO
                               LEFT JOIN ENDERECOS                  E WITH(NOLOCK)ON E.ENTIDADE              = G.TRANSPORTADORA
                               LEFT JOIN NF_FATURAMENTO_TRANSPORTES F WITH(NOLOCK)ON F.NF_FATURAMENTO        = A.NF_FATURAMENTO
                               LEFT JOIN #TEMP_VOLUMES              X WITH(NOLOCK)ON X.NF_FATURAMENTO        = A.NF_FATURAMENTO
							   LEFT JOIN EMPRESAS_USUARIAS          H WITH(NOLOCK)ON H.EMPRESA_USUARIA       = A.EMPRESA
							   LEFT JOIN NF_FATURAMENTO_TRANSF      B WITH(NOLOCK)ON B.NF_FATURAMENTO        = A.NF_FATURAMENTO
                               LEFT  JOIN (
                                          SELECT DISTINCT CHECKOUT, MAX(ESTOQUE_TRANSFERENCIA) AS ESTOQUE_TRANSFERENCIA
                                                    FROM WMS_CHECKOUT_RESUMO WITH(NOLOCK)
                                                GROUP BY CHECKOUT
                                      )                                 W              ON W.ESTOQUE_TRANSFERENCIA     = B.ESTOQUE_TRANSFERENCIA
                               LEFT  JOIN (
                                          SELECT DISTINCT CHECKOUT, MAX(VOLUME) AS VOLUME
                                                    FROM WMS_CHECKOUT_VOLUMES WITH(NOLOCK)
                                                GROUP BY CHECKOUT
                                          )                             Z               ON Z.CHECKOUT                  = W.CHECKOUT
                               
                                   WHERE F.NF_FATURAMENTO IS NULL

                                  GROUP BY A.FORMULARIO_ORIGEM  ,
                                           A.TAB_MASTER_ORIGEM  ,
                                           A.NF_FATURAMENTO     ,
                                           ISNULL(E.ESTADO, @ESTADO_PADRAO ),
                                           X.VOLUMES,
					                       X.TOTAL_PESO_BRUTO,
                                           X.TOTAL_PESO_LIQUIDO,
                                           G.TRANSPORTADORA,
										   H.ENTIDADE ,
										   ISNULL(Z.VOLUME,1)                                          



-------------------------------------------------
-- INSERCAO DO VOLUMES  NF DE FATURAMENTO --
-------------------------------------------------
INSERT INTO NF_FATURAMENTO_VOLUMES
          ( FORMULARIO_ORIGEM
          , TAB_MASTER_ORIGEM
          , REG_MASTER_ORIGEM
          , NF_FATURAMENTO
          , TOTAL_PESO_BRUTO
          , TOTAL_PESO_LIQUIDO
          , TOTAL_PECAS
          , VOLUME )

SELECT DISTINCT 
       A.FORMULARIO_ORIGEM
     , A.TAB_MASTER_ORIGEM
     , A.NF_FATURAMENTO AS REG_MASTER_ORIGEM
     , A.NF_FATURAMENTO
     , D.TOTAL_PESO_BRUTO
     , D.TOTAL_PESO_LIQUIDO
     , D.QUANTIDADE_PECA
     , D.VOLUME
       
  FROM NF_FATURAMENTO                    A WITH(NOLOCK)
  JOIN #NF_GERADAS                       X WITH(NOLOCK) ON X.NF_FATURAMENTO            = A.NF_FATURAMENTO
  JOIN NF_FATURAMENTO_TRANSF             B WITH(NOLOCK) ON A.NF_FATURAMENTO            = B.NF_FATURAMENTO
  JOIN (
           SELECT DISTINCT CHECKOUT, MAX(ESTOQUE_TRANSFERENCIA) AS ESTOQUE_TRANSFERENCIA
                     FROM WMS_CHECKOUT_RESUMO WITH(NOLOCK)
                 GROUP BY CHECKOUT
       )                                 C              ON B.ESTOQUE_TRANSFERENCIA     = C.ESTOQUE_TRANSFERENCIA
  JOIN WMS_CHECKOUT_VOLUMES              D WITH(NOLOCK) ON C.CHECKOUT                  = D.CHECKOUT

 WHERE A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO

										   
------------------------------------
-- INSERCAO DA OBSERVACAO DA NOTA --
------------------------------------



INSERT INTO NF_FATURAMENTO_OBSERVACOES ( FORMULARIO_ORIGEM,
                                         TAB_MASTER_ORIGEM,
                                         REG_MASTER_ORIGEM,
                                         NF_FATURAMENTO,
                                         OBSERVACAO_NF,
                                         OBSERVACAO_COMPLEMENTAR )
										 
                                  --PORTARIA DE CONTROLADOS--

                                  SELECT A.FORMULARIO_ORIGEM        AS FORMULARIO_ORIGEM  ,
                                         A.TAB_MASTER_ORIGEM        AS TAB_MASTER_ORIGEM  ,
                                         A.NF_FATURAMENTO           AS REG_MASTER_ORIGEM  ,
                                         A.NF_FATURAMENTO           AS NF_FATURAMENTO     ,
                                         NULL                       AS OBSERVACAO_NF      , 
                                         DBO.GERA_OBS_NF_FATURAMENTO( A.NF_FATURAMENTO) AS  OBSERVACAO_COMPLEMENTAR 
                                         
                                         
                                    FROM NF_FATURAMENTO                        A WITH(NOLOCK)
                                     JOIN #NF_GERADAS                          Y WITH(NOLOCK)ON Y.NF_FATURAMENTO      = A.NF_FATURAMENTO 
                                  LEFT JOIN NF_FATURAMENTO_OBSERVACOES         B WITH(NOLOCK) ON B.NF_FATURAMENTO     = A.NF_FATURAMENTO
                                 
                                 WHERE 1 = 1
                                   AND B.NF_FATURAMENTO IS NULL   

                    

----------------------------------------------
--  INSERCAO DA FORMA DE PAGAMENTO: BOLETO  --
----------------------------------------------
 
	DELETE NF_FATURAMENTO_BOLETOS
	  FROM NF_FATURAMENTO_BOLETOS A WITH(NOLOCK)  
	  JOIN #NF_GERADAS            B WITH(NOLOCK)  ON B.NF_FATURAMENTO = A.NF_FATURAMENTO

	  INSERT INTO NF_FATURAMENTO_BOLETOS 

				( FORMULARIO_ORIGEM,
				  TAB_MASTER_ORIGEM,
				  REG_MASTER_ORIGEM,
				  NF_FATURAMENTO,
				  VALOR,
				  VENCIMENTO,
				  CLASSIF_FINANCEIRA,
				  TITULO,
				  DIAS,
				  PERCENTUAL,
				  MULTA_ATRASO,
				  JUROS_ATRASO,
				  DESCONTO_VALOR,
				  DESCONTO_ATE,
				  CONTA_BANCARIA,
				  ANTECIPACAO_RECEBER)
	       
		   SELECT A.FORMULARIO_ORIGEM                    AS FORMULARIO_ORIGEM,
				  A.TAB_MASTER_ORIGEM                    AS TAB_MASTER_ORIGEM,
				  A.NF_FATURAMENTO                       AS REG_MASTER_ORIGEM,
				  A.NF_FATURAMENTO                       AS NF_FATURAMENTO,
				  ((((B.TOTAL_GERAL/100) * D.PERCENTUAL_TOTAL)/100) * D.PERCENTUAL_PARCELA) 
														 AS VALOR,
				  D.VENCIMENTO                           AS VENCIMENTO,
				  6                                      AS CLASSIF_FINANCEIRA,	
				  A.NF_NUMERO                            AS TITULO,		  
				  ISNULL(DATEDIFF(DD, A.EMISSAO, D.VENCIMENTO),0)  AS DIAS,
				  D.PERCENTUAL_PARCELA                   AS PERCENTUAL,
				  0                                      AS MULTA_ATRASO,
				  0                                      AS JUROS_ATRASO,
				  0                                      AS DESCONTO_VALOR,
				  NULL                                   AS DESCONTO_ATE,
				  4                                      AS CONTA_BANCARIA,
				  NULL                                   AS ANTECIPACAO_RECEBER
	            
			 FROM NF_FATURAMENTO          A WITH(NOLOCK)
			 JOIN NF_FATURAMENTO_TOTAIS   B WITH(NOLOCK) ON B.NF_FATURAMENTO  = A.NF_FATURAMENTO
			 JOIN #NF_GERADAS             C WITH(NOLOCK) ON C.PEDIDO_VENDA    = A.PEDIDO_VENDA
														AND C.NF_FATURAMENTO  = A.NF_FATURAMENTO

             JOIN OPERACOES_FISCAIS       X WITH(NOLOCK) ON X.OPERACAO_FISCAL  = A.OPERACAO_FISCAL
			                                            AND X.GERAR_FINANCEIRO = 'S'

	  CROSS APPLY RETORNA_CONDICAO_PAGAMENTO_PEDIDO_VENDA(C.PEDIDO_VENDA, 2) D
			WHERE D.PERCENTUAL_PARCELA IS NOT NULL

----------------------------------------------
--  INSERCAO DA FORMA DE PAGAMENTO: CARTAO  --
----------------------------------------------
 
	DELETE NF_FATURAMENTO_CARTOES
	  FROM NF_FATURAMENTO_CARTOES A WITH(NOLOCK)  
	  JOIN #NF_GERADAS            B WITH(NOLOCK)  ON B.NF_FATURAMENTO = A.NF_FATURAMENTO

	  INSERT INTO NF_FATURAMENTO_CARTOES 
				( FORMULARIO_ORIGEM,
				  TAB_MASTER_ORIGEM,
				  REG_MASTER_ORIGEM,
				  NF_FATURAMENTO,
				  BANDEIRA_CARTAO,
				  PARCELAS,
				  CODIGO_AUTORIZACAO,
				  VALOR )
	       
		   SELECT A.FORMULARIO_ORIGEM         AS FORMULARIO_ORIGEM,
				  A.TAB_MASTER_ORIGEM         AS TAB_MASTER_ORIGEM,
				  A.NF_FATURAMENTO            AS REG_MASTER_ORIGEM ,
				  A.NF_FATURAMENTO            AS NF_FATURAMENTO,
				  D.CARTAO_BANDEIRA_CARTAO    AS BANDEIRA_CARTAO,
				  D.NUMERO_PARCELA            AS PARCELAS,
				  D.CARTAO_CODIGO_AUTORIZACAO AS CODIGO_AUTORIZACAO,
				  ((((B.TOTAL_GERAL/100) * D.PERCENTUAL_TOTAL)/100) * D.PERCENTUAL_PARCELA) AS VALOR
				  --D.CLASSIF_FINANCEIRA        AS CLASSIF_FINANCEIRA	
	            
			 FROM NF_FATURAMENTO          A WITH(NOLOCK)
			 JOIN NF_FATURAMENTO_TOTAIS   B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
			 JOIN #NF_GERADAS             C WITH(NOLOCK) ON C.PEDIDO_VENDA   = A.PEDIDO_VENDA
														AND C.NF_FATURAMENTO = A.NF_FATURAMENTO

             JOIN OPERACOES_FISCAIS       X WITH(NOLOCK) ON X.OPERACAO_FISCAL  = A.OPERACAO_FISCAL
			                                            AND X.GERAR_FINANCEIRO = 'S'

	  CROSS APPLY RETORNA_CONDICAO_PAGAMENTO_PEDIDO_VENDA(C.PEDIDO_VENDA, 7) D
			WHERE D.PERCENTUAL_PARCELA IS NOT NULL

----------------------------------------------
--  INSERCAO DA FORMA DE PAGAMENTO: CHEQUE  --
----------------------------------------------

	DELETE NF_FATURAMENTO_CHEQUES
	  FROM NF_FATURAMENTO_CHEQUES A WITH(NOLOCK)  
	  JOIN #NF_GERADAS            B WITH(NOLOCK)  ON B.NF_FATURAMENTO = A.NF_FATURAMENTO

	  INSERT INTO NF_FATURAMENTO_CHEQUES 
				( FORMULARIO_ORIGEM,
				  TAB_MASTER_ORIGEM,
				  REG_MASTER_ORIGEM,
				  NF_FATURAMENTO,
				  CMC7,
				  BANCO,
				  AGENCIA,
				  CHEQUE_NUMERO,
				  CONTA, 
				  CHEQUE_INSCRICAO_FEDERAL,
				  CHEQUE_NOME,
				  VALOR,
				  VENCIMENTO,
				  CHEQUE_ENTIDADE )
	       
		   SELECT A.FORMULARIO_ORIGEM         AS FORMULARIO_ORIGEM,
				  A.TAB_MASTER_ORIGEM         AS TAB_MASTER_ORIGEM,
				  A.NF_FATURAMENTO            AS REG_MASTER_ORIGEM ,
				  A.NF_FATURAMENTO            AS NF_FATURAMENTO,
				  D.CMC7                      AS CMC7,
				  D.BANCO                     AS BANCO,
				  D.AGENCIA                   AS AGENCIA,
				  D.CHEQUE_NUMERO             AS CHEQUE_NUMERO,
				  D.CONTA                     AS CONTA, 
				  D.CHEQUE_INSCRICAO_FEDERAL  AS CHEQUE_INSCRICAO_FEDERAL,
				  D.CHEQUE_NOME               AS CHEQUE_NOME,
				  ((((B.TOTAL_GERAL/100) * D.PERCENTUAL_TOTAL)/100) * D.PERCENTUAL_PARCELA) AS VALOR,
				  D.VENCIMENTO                AS VENCIMENTO,
				  A.ENTIDADE                  AS CHEQUE_ENTIDADE
	              
			 FROM NF_FATURAMENTO          A WITH(NOLOCK)
			 JOIN NF_FATURAMENTO_TOTAIS   B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
			 JOIN #NF_GERADAS             C WITH(NOLOCK) ON C.PEDIDO_VENDA   = A.PEDIDO_VENDA
														AND C.NF_FATURAMENTO = A.NF_FATURAMENTO

             JOIN OPERACOES_FISCAIS       X WITH(NOLOCK) ON X.OPERACAO_FISCAL  = A.OPERACAO_FISCAL
			                                            AND X.GERAR_FINANCEIRO = 'S'

	  CROSS APPLY RETORNA_CONDICAO_PAGAMENTO_PEDIDO_VENDA(C.PEDIDO_VENDA, 4) D
			WHERE D.PERCENTUAL_PARCELA IS NOT NULL


----------------------------------------------
-- INSERCAO DA FORMA DE PAGAMENTO: DEPOSITO --
----------------------------------------------

	DELETE NF_FATURAMENTO_DEPOSITOS
	  FROM NF_FATURAMENTO_DEPOSITOS A WITH(NOLOCK)  
	  JOIN #NF_GERADAS              B WITH(NOLOCK)  ON B.NF_FATURAMENTO = A.NF_FATURAMENTO

	  INSERT INTO NF_FATURAMENTO_DEPOSITOS 
				( FORMULARIO_ORIGEM,
				  TAB_MASTER_ORIGEM,
				  REG_MASTER_ORIGEM,
				  NF_FATURAMENTO,
				  DIAS,
				  PERCENTUAL,
				  TITULO,
				  CONTA_BANCARIA,
				  VALOR,
				  VENCIMENTO,
				  CLASSIF_FINANCEIRA)
	                                    
		   SELECT A.FORMULARIO_ORIGEM         AS FORMULARIO_ORIGEM,
				  A.TAB_MASTER_ORIGEM         AS TAB_MASTER_ORIGEM,
				  A.NF_FATURAMENTO            AS REG_MASTER_ORIGEM ,
				  A.NF_FATURAMENTO            AS NF_FATURAMENTO,
				  ISNULL(DATEDIFF(DD, A.EMISSAO, D.VENCIMENTO),0)  AS DIAS,
				  D.PERCENTUAL_PARCELA        AS PERCENTUAL,  
				  C.NF_FATURAMENTO            AS TITULO,
				  ISNULL(D.CONTA_BANCARIA,4)  AS CONTA_BANCARIA, 
				  ((((B.TOTAL_GERAL/100) * D.PERCENTUAL_TOTAL)/100) * D.PERCENTUAL_PARCELA) AS VALOR,
				  D.VENCIMENTO                AS VENCIMENTO,
				  6                           AS CLASSIF_FINANCEIRA

			 FROM NF_FATURAMENTO          A WITH(NOLOCK)
			 JOIN NF_FATURAMENTO_TOTAIS   B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO
			 JOIN #NF_GERADAS             C WITH(NOLOCK) ON C.PEDIDO_VENDA   = A.PEDIDO_VENDA
														AND C.NF_FATURAMENTO = A.NF_FATURAMENTO

             JOIN OPERACOES_FISCAIS       X WITH(NOLOCK) ON X.OPERACAO_FISCAL  = A.OPERACAO_FISCAL
			                                            AND X.GERAR_FINANCEIRO = 'S'

	  CROSS APPLY RETORNA_CONDICAO_PAGAMENTO_PEDIDO_VENDA(C.PEDIDO_VENDA, 3) D
			WHERE D.PERCENTUAL_PARCELA IS NOT NULL
			 
 
 --GERA TABELA DE FORMAS DE PAGAMENTO--
DELETE NF_FATURAMENTO_FORMAS_PAGAMENTOS
  FROM NF_FATURAMENTO_FORMAS_PAGAMENTOS A WITH(NOLOCK)  
  JOIN #NF_GERADAS                      B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO

INSERT INTO NF_FATURAMENTO_FORMAS_PAGAMENTOS 
          ( FORMULARIO_ORIGEM,
            TAB_MASTER_ORIGEM,
            REG_MASTER_ORIGEM,
            NF_FATURAMENTO,
            DINHEIRO,
            BOLETO,
            CARTAO,
            CHEQUE,
            DEPOSITO,
            VALE_CREDITO )

     SELECT A.FORMULARIO_ORIGEM     AS FORMULARIO_ORIGEM,
            A.TAB_MASTER_ORIGEM     AS TAB_MASTER_ORIGEM,
            A.NF_FATURAMENTO        AS REG_MASTER_ORIGEM ,
            A.NF_FATURAMENTO        AS NF_FATURAMENTO,
            0                       AS DINHEIRO,     --TODO O VALOR REFERENTE AO 
                                                     --PAGAMENTO EM DINHEIRO � 
                                                     --INCLUIDO NO PAGAMENTO EM BOLETO
            ISNULL(SUM(C.VALOR),0)  AS BOLETO,
            ISNULL(SUM(D.VALOR),0)  AS CARTAO,
            ISNULL(SUM(E.VALOR),0)  AS CHEQUE,
            ISNULL(SUM(F.VALOR),0)  AS DEPOSITO,            
            0                       AS VALE_CREDITO

       FROM NF_FATURAMENTO           A WITH(NOLOCK)
       JOIN #NF_GERADAS              B WITH(NOLOCK) ON B.NF_FATURAMENTO = A.NF_FATURAMENTO  
  LEFT JOIN NF_FATURAMENTO_BOLETOS   C WITH(NOLOCK) ON C.NF_FATURAMENTO = B.NF_FATURAMENTO
  LEFT JOIN NF_FATURAMENTO_CARTOES   D WITH(NOLOCK) ON D.NF_FATURAMENTO = B.NF_FATURAMENTO
  LEFT JOIN NF_FATURAMENTO_CHEQUES   E WITH(NOLOCK) ON E.NF_FATURAMENTO = B.NF_FATURAMENTO
  LEFT JOIN NF_FATURAMENTO_DEPOSITOS F WITH(NOLOCK) ON F.NF_FATURAMENTO = B.NF_FATURAMENTO
       JOIN OPERACOES_FISCAIS        X WITH(NOLOCK) ON X.OPERACAO_FISCAL  = A.OPERACAO_FISCAL
			                                       AND X.GERAR_FINANCEIRO = 'S'
       
     GROUP BY A.FORMULARIO_ORIGEM,
              A.TAB_MASTER_ORIGEM,
              A.NF_FATURAMENTO
             
  END
	
	SELECT
		@PEDIDO_VENDA = D.PEDIDO_VENDA
	FROM
		DEPOSITO_FATURAMENTOS			A WITH(NOLOCK)
		JOIN DEPOSITO_FATURAMENTOS_ITENS	B WITH(NOLOCK) ON B.DEPOSITO_FATURAMENTO = A.DEPOSITO_FATURAMENTO
		JOIN ESTOQUE_TRANSFERENCIAS		C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = B.ESTOQUE_TRANSFERENCIA
		JOIN SOLICITACOES_FATURAMENTOS		D WITH(NOLOCK) ON D.SOLICITACAO_FATURAMENTO = C.SOLICITACAO_FATURAMENTO
	WHERE
		A.DEPOSITO_FATURAMENTO = @DEPOSITO_FATURAMENTO

------------------------------------------------------------------
--ATUALIZA DESCRICAO MANUAL DE PRODUTO SE HOUVER NO PEDIDO --
-------------------------------------------------------------------
IF OBJECT_ID('TEMPDB..#UPDATE_NF_FATURAMENTO_PRODUTOS_001') IS NOT NULL DROP TABLE #UPDATE_NF_FATURAMENTO_PRODUTOS_001

SELECT A.NF_FATURAMENTO_PRODUTO,
       C.DESCRICAO_MANUAL

  INTO #UPDATE_NF_FATURAMENTO_PRODUTOS_001

  FROM NF_FATURAMENTO_PRODUTOS           A WITH(NOLOCK)
  JOIN #NF_GERADAS                       X WITH(NOLOCK) ON X.NF_FATURAMENTO       = A.NF_FATURAMENTO
  JOIN NF_FATURAMENTO                    B WITH(NOLOCK) ON B.NF_FATURAMENTO       = A.NF_FATURAMENTO
  JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA  C WITH(NOLOCK) ON C.PEDIDO_VENDA         = B.PEDIDO_VENDA
                                                       AND C.PRODUTO              = A.PRODUTO
 WHERE LEN(C.DESCRICAO_MANUAL) > 3

UPDATE A
   SET DESCRICAO_MANUAL = X.DESCRICAO_MANUAL
  FROM NF_FATURAMENTO_PRODUTOS             A WITH(NOLOCK)
  JOIN #UPDATE_NF_FATURAMENTO_PRODUTOS_001 X WITH(NOLOCK) ON X.NF_FATURAMENTO_PRODUTO = A.NF_FATURAMENTO_PRODUTO

IF OBJECT_ID('TEMPDB..#UPDATE_NF_FATURAMENTO_PRODUTOS_002') IS NOT NULL DROP TABLE #UPDATE_NF_FATURAMENTO_PRODUTOS_002

SELECT A.NF_FATURAMENTO_PRODUTO,
       UNIDADE_MEDIDA_DANFE = ISNULL(C.UNIDADE_MEDIDA_DANFE, D.UNIDADE_MEDIDA),
       DESCRICAO_MANUAL     = ISNULL(C.DESCRICAO_MANUAL, E.DESCRICAO) + ' ' + D.DESCRICAO

  INTO #UPDATE_NF_FATURAMENTO_PRODUTOS_002

  FROM NF_FATURAMENTO_PRODUTOS           A WITH(NOLOCK)
  JOIN #NF_GERADAS                       X WITH(NOLOCK) ON X.NF_FATURAMENTO       = A.NF_FATURAMENTO
  JOIN NF_FATURAMENTO                    B WITH(NOLOCK) ON B.NF_FATURAMENTO       = A.NF_FATURAMENTO
  JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA  C WITH(NOLOCK) ON C.PEDIDO_VENDA         = B.PEDIDO_VENDA
                                                       AND C.PRODUTO              = A.PRODUTO
  JOIN FATORES_APRESENTACOES             D WITH(NOLOCK) ON D.FATOR_APRESENTACAO   = C.FATOR_APRESENTACAO
  JOIN PRODUTOS                          E WITH(NOLOCK) ON E.PRODUTO              = A.PRODUTO
 WHERE C.FATOR <> 1

UPDATE A
   SET UNIDADE_MEDIDA_DANFE = X.UNIDADE_MEDIDA_DANFE,
       DESCRICAO_MANUAL     = X.DESCRICAO_MANUAL
  FROM NF_FATURAMENTO_PRODUTOS             A WITH(NOLOCK)
  JOIN #UPDATE_NF_FATURAMENTO_PRODUTOS_002 X WITH(NOLOCK) ON X.NF_FATURAMENTO_PRODUTO = A.NF_FATURAMENTO_PRODUTO

-----------------------------------------
--   ATUALIZA��O DO STATUS DO PEDIDO   --
-----------------------------------------

--	EXEC USP_PEDIDO_VENDA_UDP_STATUS @FORMULARIO_ORIGEM_DEPOSITO, @TAB_MASTER_ORIGEM_DEPOSITO, @PEDIDO_VENDA


END