DECLARE @DESCRICAO          VARCHAR(255) 
DECLARE @MARCA              NUMERIC(15,0)                 
DECLARE @GRUPO              NUMERIC(15,0)
DECLARE @SUBGRUPO           NUMERIC(15,0)    
DECLARE @PRODUTO            NUMERIC(15,0)
DECLARE @GRUPO_MARCA        NUMERIC(15,0)


--IF ISNUMERIC(:MARCA)        = 1 SET @MARCA        = :MARCA        ELSE SET @MARCA    = NULL
--IF ISNUMERIC(:GRUPO)        = 1 SET @GRUPO        = :GRUPO        ELSE SET @GRUPO    = NULL
--IF ISNUMERIC(:SUBGRUPO)     = 1 SET @SUBGRUPO     = :SUBGRUPO     ELSE SET @SUBGRUPO = NULL
--IF ISNUMERIC(:PRODUTO)      = 1 SET @PRODUTO      = :PRODUTO      ELSE SET @PRODUTO  = NULL
--IF ISNUMERIC(:GRUPO_MARCA)  = 1 SET @GRUPO_MARCA  = :GRUPO_MARCA  ELSE SET @GRUPO_MARCA = NULL
               
                                  
--SET @DESCRICAO = :DESCRICAO
     

SET @MARCA    = 95
SET @GRUPO    = NULL
SET @SUBGRUPO = NULL
SET @PRODUTO  = NULL 
                                    
                                        
SELECT 
       1                         AS CENTRO_ESTOQUE_INI,
       1000                      AS CENTRO_ESTOQUE_FIM,                                                      
       A.PRODUTO                 AS PRODUTO_INI,      
       A.PRODUTO                 AS PRODUTO_FIM,          
       A.PRODUTO                 AS PRODUTO,
       A.DESCRICAO               AS DESCRICAO_PRODUTO ,
       A.CODIGO_REFERENCIA       AS REFERENCIA,
       A.DESCRICAO_REDUZIDA      AS DESCRICAO_REDUZIDA_PRODUTO ,
       B.EAN                     AS EAN,
       C.DESCRICAO               AS DESCRICAO_MARCA,
       E.DESCRICAO               AS DESCRICAO_GRUPO_PRODUTO,
       F.DESCRICAO               AS DESCRICAO_SUBGRUPO_PRODUTO,
       D.DESCRICAO               AS DESCRICAO_SECAO_PRODUTO,
       O.DESCRICAO               AS DESCRICAO_CLASSE,
       M.DESCRICAO               AS DESCRICAO_STATUS_COMPRA,
       J.DESCONTO_PADRAO         AS DESCONTO_PADRAO,
       J.PRECO_VENDA             AS PRECO_VENDA,
       ISNULL(Z.ESTOQUE_TOTAL,0) AS ESTOQUE_TOTAL,
       1                         AS QT_REG,
	   H.DESCRICAO               AS FAMILIA_PRODUTO
       
FROM PRODUTOS                   A WITH(NOLOCK)
LEFT JOIN (
SELECT MAX(EAN) EAN , PRODUTO
  FROM PRODUTOS_EAN A WITH(NOLOCK)
  GROUP BY PRODUTO
          )                     B             ON B.PRODUTO             = A.PRODUTO
LEFT JOIN MARCAS                C WITH(NOLOCK)ON C.MARCA               = A.MARCA
LEFT JOIN SECOES_PRODUTOS       D WITH(NOLOCK)ON D.SECAO_PRODUTO       = A.SECAO_PRODUTO
LEFT JOIN GRUPOS_PRODUTOS       E WITH(NOLOCK)ON E.GRUPO_PRODUTO       = A.GRUPO_PRODUTO
LEFT JOIN SUBGRUPOS_PRODUTOS    F WITH(NOLOCK)ON F.SUBGRUPO_PRODUTO    = A.SUBGRUPO_PRODUTO
LEFT JOIN SITUACOES_PRODUTOS    G WITH(NOLOCK)ON G.SITUACAO_PRODUTO    = A.SITUACAO_PRODUTO
LEFT JOIN FAMILIAS_PRODUTOS     H WITH(NOLOCK)ON H.FAMILIA_PRODUTO     = A.FAMILIA_PRODUTO
LEFT JOIN SAZONALIDADES         I WITH(NOLOCK)ON I.SAZONALIDADE        = A.SAZONALIDADE
LEFT JOIN CLASSES_PRODUTOS      O WITH(NOLOCK)ON O.CLASSE_PRODUTO      = A.CLASSE_PRODUTO
LEFT JOIN STATUS_COMPRA         M WITH(NOLOCK)ON M.STATUS_COMPRA       = A.SITUACAO_PRODUTO_COMPRA
LEFT JOIN PRODUTOS_VENDAS       J WITH(NOLOCK)ON J.PRODUTO             = A.PRODUTO 
                                             AND J.GRUPO_PRECO         =  1
LEFT JOIN (
               SELECT 
               PRODUTO,
               SUM(ESTOQUE_SALDO) AS ESTOQUE_TOTAL
               FROM ESTOQUE_ATUAL
               GROUP BY PRODUTO
          )                     Z ON Z.PRODUTO = A.PRODUTO        
           
LEFT JOIN GRUPOS_MARCAS_DETALHE P WITH(NOLOCK)ON P.MARCA               = C.MARCA
LEFT JOIN GRUPOS_MARCAS         Q WITH(NOLOCK)ON Q.GRUPO_MARCA         = P.GRUPO_MARCA          
 
WHERE 1=1
AND ( A.PRODUTO           = @PRODUTO                  OR @PRODUTO     IS NULL )
AND ( A.MARCA             = @MARCA                    OR @MARCA       IS NULL )
AND ( A.GRUPO_PRODUTO     = @GRUPO                    OR @GRUPO       IS NULL )
AND ( A.SUBGRUPO_PRODUTO  = @SUBGRUPO                 OR @SUBGRUPO    IS NULL )
AND ( A.DESCRICAO         LIKE '%' + @DESCRICAO + '%' OR @DESCRICAO   IS NULL )
AND ( Q.GRUPO_MARCA       = @GRUPO_MARCA              OR @GRUPO_MARCA IS NULL )

ORDER BY 1
