DECLARE @EMPRESA_CONTABIL   NUMERIC(15)
DECLARE @VENCIMENTO_INI     DATETIME
DECLARE @VENCIMENTO_FIM     DATETIME      
DECLARE @MODALIDADE         NUMERIC(15)  
DECLARE @CARTOES            VARCHAR(1) = 'T'

  

--SET     @VENCIMENTO_INI     = '01/01/2000'
--SET     @VENCIMENTO_FIM     = '27/03/2018'
--SET     @EMPRESA_CONTABIL   = 5
--SET     @MODALIDADE         = NULL

        
SET     @VENCIMENTO_INI     = :VENCIMENTO_INI    
SET     @VENCIMENTO_FIM     = :VENCIMENTO_FIM  
SET     @EMPRESA_CONTABIL   = CASE WHEN ISNUMERIC(:EMPRESA_CONTABIL   ) = 1 THEN :EMPRESA_CONTABIL   ELSE NULL       END  
SET     @MODALIDADE         = CASE WHEN ISNUMERIC(:MODALIDADE         ) = 1 THEN :MODALIDADE         ELSE NULL       END
SET     @CARTOES            = CASE WHEN :CARTOES NOT LIKE ''                THEN :CARTOES            ELSE 'T'        END 


 SELECT A.TITULO_RECEBER   AS TITULO_RECEBER ,       
          A.EMPRESA          AS EMPRESA ,
          D.EMPRESA_CONTABIL AS REDE , 
          E.NOME_FANTASIA    AS NOME_REDE , 
          D.FILIAL           AS LOJA   , 
          A.TITULO           AS TITULO ,
          A.MOVIMENTO        AS MOVIMENTO  , 
          A.VENCIMENTO       AS VENCIMENTO , 
          B.NOME             AS NOME ,
          A.VALOR            AS VALOR , 
          C.SALDO            AS SALDO,
          D.EMPRESA_CONTABIL AS EMPRESA_CONTABIL,
          F.DESCRICAO        AS MODALIDADE,
          G.ENTIDADE,
		  ISNULL(H.FORMULARIO,'IMPORTACAO')  AS ORIGEM

     FROM TITULOS_RECEBER         A WITH(NOLOCK)
LEFT JOIN ENTIDADES               B WITH(NOLOCK) ON B.ENTIDADE        = A.ENTIDADE
LEFT JOIN TITULOS_RECEBER_SALDO   C WITH(NOLOCK) ON C.TITULO_RECEBER  = A.TITULO_RECEBER
LEFT JOIN EMPRESAS_USUARIAS       D WITH(NOLOCK) ON D.EMPRESA_USUARIA = A.EMPRESA
LEFT JOIN EMPRESAS_USUARIAS       E WITH(NOLOCK) ON E.EMPRESA_USUARIA = D.EMPRESA_CONTABIL
LEFT JOIN MODALIDADES_TITULOS     F WITH(NOLOCK) ON F.MODALIDADE      = A.MODALIDADE   
LEFT JOIN (
            SELECT DISTINCT ENTIDADE FROM BANDEIRAS_CARTOES WITH(NOLOCK)
          )                       G              ON G.ENTIDADE        = A.ENTIDADE
 LEFT JOIN FORMULARIOS             H WITH(NOLOCK) ON H.NUMID          = A.FORMULARIO_ORIGEM
    WHERE ISNULL ( C.SITUACAO_TITULO , 0 ) < 2
      AND A.VENCIMENTO >= @VENCIMENTO_INI
      AND A.VENCIMENTO <= @VENCIMENTO_FIM
      AND ( A.EMPRESA                                          = @EMPRESA_CONTABIL OR @EMPRESA_CONTABIL IS NULL )
      AND ( A.MODALIDADE                                       = @MODALIDADE       OR @MODALIDADE       IS NULL )
      AND ( CASE WHEN G.ENTIDADE IS NULL THEN 'N' ELSE 'S' END = @CARTOES          OR @CARTOES          = 'T'   )
 ORDER BY A.VENCIMENTO, B.NOME