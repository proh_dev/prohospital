/*Altera��o de relat�rio
Adicionada modalidade de or�amento
data 23/02/2018
Autor - Yno� Pedro*/
DECLARE @TOTAL_PRODUTOS NUMERIC(15,2)
DECLARE @TOTAL_DESCONTO NUMERIC(15,2)
DECLARE @PEDIDO_COMPRA  NUMERIC
DECLARE @LIQUIDO        NVARCHAR

   SET @PEDIDO_COMPRA = :PEDIDO_COMPRA
                
SELECT @TOTAL_PRODUTOS = SUM(A.QUANTIDADE * A.VALOR_UNITARIO ) ,
       @TOTAL_DESCONTO = SUM(A.TOTAL_DESCONTO)               

  FROM PEDIDOS_COMPRAS_PRODUTOS A WITH(NOLOCK)
 WHERE PEDIDO_COMPRA = @PEDIDO_COMPRA
 

   SELECT A.PEDIDO_COMPRA       AS PEDIDO_COMPRA ,
          A.SUGESTAO_COMPRA     AS SUGESTAO_COMPRA,
          A.CODIGO_ANTIGO       AS CODIGO_ANTIGO,
          A.EMPRESA             AS EMPRESA ,
          A.DATA_ENTREGA        AS DATA_ENTREGA,
          A.DATA_HORA           AS DATA_HORA ,
          A.USUARIO_LOGADO      AS USUARIO_LOGADO ,
          ISNULL(L.NOME, G.NOME)AS COMPRADOR,
          A.ENTIDADE            AS FORNECEDOR ,
          B.NOME                AS NOME_FORNECEDOR ,
          B.NOME_FANTASIA       AS NOME_FANTASIA_FORNECEDOR ,
          B.INSCRICAO_FEDERAL   AS INSCRICAO_FEDERAL_FORNECEDOR ,
          B.INSCRICAO_ESTADUAL  AS INSCRICAO_ESTADUAL_FORNECEDOR ,
          B.INSCRICAO_MUNICIPAL AS INSCRICAO_MUNICIPAL_FORNECEDOR ,
          C.PAIS                AS PAIS ,
          C.CEP                 AS CEP ,
          C.TIPO_ENDERECO       AS TIPO_ENDERECO ,
          C.ENDERECO            AS ENDERECO ,
          C.NUMERO              AS NUMERO ,
          C.COMPLEMENTO         AS COMPLEMENTO ,
          C.BAIRRO              AS BAIRRO ,
          C.CIDADE              AS CIDADE ,
          C.ESTADO              AS ESTADO , 
          H.NOME                AS NOME_USUARIA ,
          H.NOME_FANTASIA       AS FANTASIA_USUARIA ,
          H.ENTIDADE            AS ENTIDADE_USUARIA ,
          H.INSCRICAO_FEDERAL   AS INSCRICAO_FEDERAL_EMPRESA ,
          H.INSCRICAO_ESTADUAL  AS INSCRICAO_ESTADUAL_EMPRESA ,
          H.INSCRICAO_MUNICIPAL AS INSCRICAO_MUNICIPAL_EMPRESA ,
          E.PAIS                AS PAIS_USUARIA ,
          E.CEP                 AS CEP_USUARIA ,
          E.TIPO_ENDERECO       AS TIPO_ENDERECO_USUARIA ,
          E.ENDERECO            AS ENDERECO_USUARIA ,
          E.NUMERO              AS NUMERO_USUARIA ,
          E.COMPLEMENTO         AS COMPLEMENTO_USUARIA ,
          E.BAIRRO              AS BAIRRO_USUARIA ,
          E.CIDADE              AS CIDADE_USUARIA ,
          E.ESTADO              AS ESTADO_USUARIA ,

		  CASE WHEN A.ORCAMENTO = 'N'
		  THEN
          @TOTAL_PRODUTOS
		  ELSE 0.00 END          AS TOTAL_PRODUTOS ,

          TOTAL_IPI              AS TOTAL_IPI ,

		  CASE WHEN A.ORCAMENTO = 'N' 
		  THEN
          @TOTAL_PRODUTOS       -
          @TOTAL_DESCONTO       +
          TOTAL_IPI       
		  ELSE 0.00 END          AS SUB_TOTAL ,

          TOTAL_SERVICOS         AS TOTAL_SERVICOS ,

          @TOTAL_DESCONTO        AS TOTAL_DESCONTO,

		  CASE WHEN A.ORCAMENTO = 'N'
		  THEN
          @TOTAL_PRODUTOS - 
		  @TOTAL_DESCONTO 
		  ELSE 0.00  END         AS TOTAL_GERAL,

          TOTAL_DESC_FINANCEIRO AS TOTAL_DESC_FINANCEIRO , 
          TOTAL_REPASSE         AS TOTAL_REPASSE , 
          ICMS_BASE_CALCULO     AS ICMS_BASE_CALCULO ,
          ICMS_VALOR            AS ICMS_VALOR ,
          ICMS_BASE_SUBST       AS ICMS_BASE_SUBST ,
          F.ICMS_VALOR_SUBST    AS TOTAL_ICMS_SUBST ,
		  CASE WHEN A.ORCAMENTO = 'N'
		  THEN 
          F.TOTAL_GERAL
		  ELSE 0.00 END          AS LIQUIDO , 

          ( SELECT MAX(CAST(N.DDD AS VARCHAR(5)) + ' - ' + CAST(N.NUMERO AS VARCHAR(12)))
              FROM TELEFONES N WITH(NOLOCK)
             WHERE N.ENTIDADE = H.ENTIDADE
               AND N.TIPO_TELEFONE = 1 ) AS FONE_EMPRESA,

          ( SELECT MAX(CAST(N.DDD AS VARCHAR(5)) + ' - ' + CAST(N.NUMERO AS VARCHAR(12)))
              FROM TELEFONES N WITH(NOLOCK)
             WHERE N.ENTIDADE = H.ENTIDADE
               AND N.TIPO_TELEFONE = 2 ) AS FAX_EMPRESA,

          ( SELECT MAX(I.EMAIL)
              FROM EMAIL I WITH(NOLOCK)
             WHERE I.ENTIDADE = A.ENTIDADE 
               AND I.PREFERENCIAL = 'S') AS EMAIL,

          ( SELECT MAX(CAST(H.DDD AS VARCHAR(5)) + ' - ' + CAST(H.NUMERO AS VARCHAR(12)))
              FROM TELEFONES H WITH(NOLOCK)
             WHERE H.ENTIDADE = A.ENTIDADE
               AND H.TIPO_TELEFONE = 1 ) AS FONE_FORNECEDOR,

          ( SELECT MAX(CAST(H.DDD AS VARCHAR(5)) + ' - ' + CAST(H.NUMERO AS VARCHAR(12)))
              FROM TELEFONES H WITH(NOLOCK)
             WHERE H.ENTIDADE = A.ENTIDADE
               AND H.TIPO_TELEFONE = 2) AS FAX_FORNECEDOR,

          ISNULL(K.OPERACAO_FISCAL, R.OPERACAO_FISCAL )        AS OPERACAO_FISCAL,
          
          DBO.CONCATENA_PEDIDOS_ENCOMENDAS ( A.PEDIDO_COMPRA ) AS ENCOMENDAS,
          
          ISNULL( CAST(M.OBSERVACAO AS VARCHAR(8000)), '') AS OBSERVACAO_PADRAO,
  
          --ISNULL( CAST(Q.OBSERVACAO AS VARCHAR(8000)), '') + ' ' + CHAR(13) + CHAR(10) + CHAR(10) +
          ISNULL( CAST(P.OBSERVACAO AS VARCHAR(8000)), '') AS OBSERVACAO_PEDIDO         ,
          
          -- DADOS DA ENTREGA--      
          'Destinat�rio.: ' + CAST(ISNULL(Y.ENTIDADE,0)       AS VARCHAR(20) ) + '-' + ISNULL(Y.NOME_FANTASIA,'') + CHAR(10) + 
          'Endere�o.....: ' + CAST(ISNULL(X.TIPO_ENDERECO,'') AS VARCHAR(20) ) + ' ' + 
                              ISNULL(X.ENDERECO,'')                            + ' ' + 
                              CAST(ISNULL(X.NUMERO,0) AS VARCHAR(20))                + CHAR(10) + 
         
          'Cep...........: ' + ISNULL(X.CEP,'')                                         + CHAR(10) + 
          'Cidade........: ' + ISNULL(X.CIDADE,'')                                   + CHAR(10) +     
          'Complemento...: ' + ISNULL(X.COMPLEMENTO,'')                             + CHAR(10) + 
          'Bairro........: ' + ISNULL(X.BAIRRO,'')                                  + CHAR(10) + 
          'Estado........: ' + ISNULL(X.ESTADO,'')                                  AS ENDERECO_ENTREGA,
          
 
 
          -- DADOS DO TRANSPORTE--
          
          'Transp.......: ' + CAST(ISNULL(Z.TRANSPORTADOR,0)  AS VARCHAR(20) ) + '-' + ISNULL(W.NOME,'') + CHAR(10) + 
          'Endere�o.....: ' + CAST(ISNULL(Z.TIPO_ENDERECO,'') AS VARCHAR(20) ) + ' ' + 
                              ISNULL(Z.ENDERECO,'')                            + ' ' + 
                              CAST(ISNULL(Z.NUMERO,0) AS VARCHAR(20))                + CHAR(10) + 
         
          'Cep..........: ' + ISNULL(Z.CEP,'')                                       + CHAR(10) + 
          'Cidade.......: ' + ISNULL(Z.CIDADE,'')                                  + CHAR(10) +     
          'Complemento..: ' + ISNULL(Z.COMPLEMENTO,'')                             + CHAR(10) + 
          'Bairro.......: ' + ISNULL(Z.BAIRRO,'')                                  + CHAR(10) + 
          'Estado.......: ' + ISNULL(Z.ESTADO,'')                                  + CHAR(10) + 
          'Fone  .......: ' + ISNULL(CAST(Z.TELEFONE AS VARCHAR(20)),'')         AS TRANSPORTADORA,
          CAST(O.CONTEUDO AS image)                                              AS LOGO,
          S.PEDIDO_COMPRA_PRODUTO ,
          S.PEDIDO_COMPRA ,
          S.REFERENCIA,
          S.PRODUTO , 
          S.CODIGO_ANTIGO   ,
          S.DESCRICAO , 
          S.DESCRICAO_COMPLEMENTAR ,
          S.QUANTIDADE ,
          S.UNIDADE_MEDIDA ,
		  S.VALOR_UNITARIO ,
          S.DESCONTO,
          S.TOTAL_DESCONTO ,
          S.TOTAL_PRODUTO ,
          S.IPI_ALIQUOTA ,
          S.IPI_VALOR ,
          S.ICMS_ALIQUOTA ,
          S.QUANTIDADE_EMBALAGEM,
          S.TOTAL_GERAL , 
          S.DESCONTO_FINANCEIRO , 
          S.REPASSE ,
		  S.TOTAL_LIQUIDO,
          S.LIQUIDO_UNITARIO,

		  CASE WHEN A.ORCAMENTO = 'N' 
        	 THEN 'PEDIDO DE COMPRA'
	         ELSE
			 'OR�AMENTO'
	         END AS TIPO_RELATORIO,

          S.CODIGO_REFERENCIA
          

     FROM PEDIDOS_COMPRAS        A WITH(NOLOCK)
LEFT JOIN ENTIDADES              B WITH(NOLOCK)ON B.ENTIDADE        = A.ENTIDADE 
LEFT JOIN ENDERECOS              C WITH(NOLOCK)ON C.ENTIDADE        = A.ENTIDADE       
LEFT JOIN EMPRESAS_USUARIAS      D WITH(NOLOCK)ON D.EMPRESA_USUARIA = A.EMPRESA
LEFT JOIN ENTIDADES              H WITH(NOLOCK)ON H.ENTIDADE        = D.ENTIDADE
LEFT JOIN ENDERECOS              E WITH(NOLOCK)ON E.ENTIDADE        = H.ENTIDADE 
LEFT JOIN PEDIDOS_COMPRAS_TOTAIS F WITH(NOLOCK)ON F.PEDIDO_COMPRA   = A.PEDIDO_COMPRA
LEFT JOIN USUARIOS               G WITH(NOLOCK)ON G.USUARIO         = A.USUARIO_LOGADO
LEFT JOIN ENTIDADES              J WITH(NOLOCK)ON J.ENTIDADE        = G.ENTIDADE
     JOIN PEDIDOS_GERADOS        I WITH(NOLOCK)ON I.PEDIDO_COMPRA   = A.PEDIDO_COMPRA
LEFT JOIN COMPRADORES            L WITH(NOLOCK)ON L.COMPRADOR       = A.COMPRADOR
LEFT JOIN ( SELECT TOP 1 B.DESCRICAO AS OPERACAO_FISCAL
              FROM PEDIDOS_COMPRAS_PRODUTOS A WITH(NOLOCK),
                   OPERACOES_FISCAIS        B WITH(NOLOCK)
             WHERE A.OPERACAO_FISCAL = B.OPERACAO_FISCAL
               AND A.PEDIDO_COMPRA   = @PEDIDO_COMPRA ) K ON 1 = 1

LEFT JOIN ( SELECT TOP 1 B.DESCRICAO AS OPERACAO_FISCAL
              FROM PEDIDOS_COMPRAS_SERVICOS A WITH(NOLOCK),
                   OPERACOES_FISCAIS        B WITH(NOLOCK)
             WHERE A.OPERACAO_FISCAL = B.OPERACAO_FISCAL
               AND A.PEDIDO_COMPRA   = @PEDIDO_COMPRA ) R ON 1 = 1        


LEFT JOIN PARAMETROS_ISO M WITH(NOLOCK) ON M.GRUPO_EMPRESARIAL = 1
                                       AND M.TIPO_ISO = 1 -- PEDIDO_COMPRA    
                                       
LEFT JOIN PEDIDOS_COMPRAS_OBSERVACOES  P WITH(NOLOCK) ON P.PEDIDO_COMPRA = A.PEDIDO_COMPRA

LEFT JOIN PARAMETROS_OBS_PEDIDO_COMPRA Q WITH(NOLOCK) ON Q.GRUPO_EMPRESARIAL = 1 

LEFT JOIN PEDIDOS_COMPRAS_ENDERECOS_ENTREGAS X WITH(NOLOCK) ON X.PEDIDO_COMPRA = A.PEDIDO_COMPRA
LEFT JOIN ENTIDADES                          Y WITH(NOLOCK) ON X.ENTIDADE = Y.ENTIDADE


LEFT JOIN PEDIDOS_COMPRAS_TRANSPORTE         Z WITH(NOLOCK) ON Z.PEDIDO_COMPRA = A.PEDIDO_COMPRA
LEFT JOIN ENTIDADES                          W WITH(NOLOCK) ON W.ENTIDADE = Z.TRANSPORTADOR
LEFT JOIN GED                                O WITH(NOLOCK) ON O.FORMULARIO_ORIGEM          = 9
                                                           AND O.REG_MASTER_ORIGEM          = A.EMPRESA
                                                           AND O.OBSERVACOES            LIKE 'logo'
JOIN (
SELECT A1.PEDIDO_COMPRA_PRODUTO                                   AS PEDIDO_COMPRA_PRODUTO ,
       A1.PEDIDO_COMPRA                                           AS PEDIDO_COMPRA ,
       ISNULL(A1.REFERENCIA,'')                                   AS REFERENCIA,
       A1.PRODUTO                                                 AS PRODUTO , 
       B1.CODIGO_ANTIGO                                           AS CODIGO_ANTIGO   ,
       B1.DESCRICAO+ ISNULL('('+E1.DESCRICAO_FORNECEDOR+')','')   AS DESCRICAO , 
       A1.DESCRICAO_COMPLEMENTAR                                  AS DESCRICAO_COMPLEMENTAR ,
       A1.QUANTIDADE                                              AS QUANTIDADE ,
       A1.UNIDADE_MEDIDA                                          AS UNIDADE_MEDIDA ,
	   CASE WHEN D1.ORCAMENTO = 'N'
	   THEN
       A1.VALOR_UNITARIO
	   ELSE 0.00 END                                              AS VALOR_UNITARIO ,
       A1.DESCONTO                                                AS DESCONTO,
       A1.TOTAL_DESCONTO                                          AS TOTAL_DESCONTO ,
       A1.TOTAL_PRODUTO                                           AS TOTAL_PRODUTO ,
       A1.IPI_ALIQUOTA                                            AS IPI_ALIQUOTA ,
       A1.IPI_VALOR                                               AS IPI_VALOR ,
       A1.ICMS_ALIQUOTA                                           AS ICMS_ALIQUOTA ,
       A1.QUANTIDADE_EMBALAGEM                                    AS QUANTIDADE_EMBALAGEM,
       A1.TOTAL_PRODUTO + A1.IPI_VALOR                            AS TOTAL_GERAL , 
       C1.DESCONTO_FINANCEIRO                                     AS DESCONTO_FINANCEIRO , 
       C1.REPASSE                                                 AS REPASSE ,
	   
	   CASE WHEN D1.ORCAMENTO = 'N'
	   THEN 
       A1.TOTAL_PRODUTO + A1.IPI_VALOR  -
     ( A1.TOTAL_PRODUTO * ( C1.DESCONTO_FINANCEIRO / 100 ) ) -
     ( A1.TOTAL_PRODUTO * ( C1.REPASSE             / 100 ) )   
	   ELSE 0.00 END                                              AS TOTAL_LIQUIDO ,

	   CASE WHEN D1.ORCAMENTO  = 'N'
	   THEN
     ( A1.TOTAL_PRODUTO + A1.IPI_VALOR     -
     ( A1.TOTAL_PRODUTO * ( C1.DESCONTO_FINANCEIRO / 100 ) ) -
     ( A1.TOTAL_PRODUTO * ( C1.REPASSE             / 100 ) ) ) / 
       A1.QUANTIDADE                                            
	   ELSE 0.00 END                                              AS LIQUIDO_UNITARIO,
       B1.CODIGO_REFERENCIA                                       AS CODIGO_REFERENCIA

  FROM PEDIDOS_COMPRAS_PRODUTOS  A1 WITH(NOLOCK) 
  JOIN PRODUTOS                  B1 WITH(NOLOCK) ON B1.PRODUTO        = A1.PRODUTO
LEFT JOIN PEDIDOS_COMPRAS_TOTAIS C1 WITH(NOLOCK) ON C1.PEDIDO_COMPRA  = A1.PEDIDO_COMPRA
LEFT JOIN PEDIDOS_COMPRAS        D1 WITH(NOLOCK) ON D1.PEDIDO_COMPRA  = A1.PEDIDO_COMPRA
LEFT JOIN PRODUTOS_FORNECEDORES  E1 WITH(NOLOCK) ON E1.PRODUTO        = A1.PRODUTO
                                                AND E1.ENTIDADE       = D1.ENTIDADE
 WHERE 1=1
   AND A1.PEDIDO_COMPRA = @PEDIDO_COMPRA
   


     )                            S ON S.PEDIDO_COMPRA = A.PEDIDO_COMPRA

    WHERE 1=1
	  AND A.PEDIDO_COMPRA = @PEDIDO_COMPRA
	   
	  
	  
	     