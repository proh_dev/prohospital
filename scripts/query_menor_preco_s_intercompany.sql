DECLARE @PRODUTO  NUMERIC = :PRODUTO 


 SELECT TOP 1                                                
        Z.PRODUTO                 AS PRODUTO,  
        A.DESCRICAO,
        Z.REG_MASTER_ORIGEM       AS CHAVE_MASTER,          
        B.DESCRICAO               AS PROCESSO_ORIGEM,
                                                      
        Z.FORMULARIO_ORIGEM,        
        Z.TAB_MASTER_ORIGEM,   
        Z.REG_MASTER_ORIGEM,   
                                               
        Z.EMPRESA,           
        C.NF_NUMERO,
        
        Z.PRECO_COMPRA            AS ULTIMA_COMPRA,  
        Z.DESCONTO                AS DESCONTO,  

        Z.PRECO_COMPRA,
        Z.VALOR_IPI,
        Z.ICMS_SUBST_VALOR,
        Z.ICMS_SUBST_VALOR_PAGAR,
        Z.DESPESAS,
        Z.FRETE,
        Z.SEGURO,
        Z.REPASSE,
        Z.VALOR_PIS,
        Z.VALOR_COFINS,
        --Z.CUSTO,
        Z.PRECO_COMPRA+
        Z.VALOR_IPI                AS CUSTO,              
                        
        CONVERT(DATE,Z.DATA_HORA) AS DATA,  
        Z.CHAVE,  
        Z.SEQUENCIA,  
        Z.ORDEM,
        C.ENTIDADE,
        D.QUANTIDADE_ESTOQUE,

CASE WHEN E.ESTADO LIKE 'CE' THEN '50%' ELSE '70%' END PERCENTUAL,
CASE WHEN E.ESTADO LIKE 'CE' THEN  ( Z.PRECO_COMPRA+Z.VALOR_IPI ) * 0.5 + ( Z.PRECO_COMPRA+Z.VALOR_IPI ) ELSE ( Z.PRECO_COMPRA+Z.VALOR_IPI ) * 0.7 + ( Z.PRECO_COMPRA+Z.VALOR_IPI )
END                                  AS PRECO_MINIMO
             
   FROM PRODUTOS A WITH(NOLOCK)  
              
   JOIN (  
                                          
 SELECT X.PRODUTO,  
        X.DATA_HORA,  
        X.DESCONTO,  
        X.FORMULARIO_ORIGEM,  
        X.TAB_MASTER_ORIGEM,  
        X.REG_MASTER_ORIGEM,  
        X.EMPRESA,
        X.CHAVE,  
        X.SEQUENCIA,  
        X.PRECO_COMPRA,
        X.VALOR_IPI,
        X.ICMS_SUBST_VALOR,
        X.ICMS_SUBST_VALOR_PAGAR,
        X.DESPESAS,
        X.FRETE,
        X.SEGURO,
        X.REPASSE,
        X.VALOR_PIS,
        X.VALOR_COFINS,
        X.CUSTO,                                                       
        ROW_NUMBER() OVER ( PARTITION BY  X.PRODUTO ORDER BY X.PRODUTO, X.DATA_HORA DESC, X.SEQUENCIA, X.CHAVE DESC )  AS ORDEM  
               
   FROM (   
           --PEGA OS UNITARIOS DA NOTA--  
           SELECT B.PRODUTO,  
                  A.MOVIMENTO AS DATA_HORA,  
                  A.FORMULARIO_ORIGEM,  
                  A.TAB_MASTER_ORIGEM,  
                  A.NF_COMPRA AS REG_MASTER_ORIGEM,  
                  A.EMPRESA,
                  B.NF_COMPRA_PRODUTO AS CHAVE,  
                  CASE WHEN B.TOTAL_DESCONTO > 0    
                       THEN CASE WHEN B.TIPO_DESCONTO = 3    
                                 THEN B.DESCONTO    
                                 ELSE ROUND( ( 1 - (B.TOTAL_PRODUTO / (B.QUANTIDADE * B.VALOR_UNITARIO) ) ) * 100 , 2 )    
                            END    
                       ELSE 0    
                   END                           AS DESCONTO,    
                               
                  D.VALOR_PRODUTO_LIQUIDO AS PRECO_COMPRA,                                                                     
                  D.VALOR_IPI,
                  D.ICMS_SUBST_VALOR,
                  D.ICMS_SUBST_VALOR_PAGAR,
                  D.DESPESAS,
                  D.FRETE,
                  D.SEGURO,
                  D.REPASSE,
                  D.VALOR_PIS,
                  D.VALOR_COFINS,                                    
                  D.CUSTO,  
                  1 AS SEQUENCIA  
          
             FROM NF_COMPRA                       A WITH(NOLOCK)                                              
             JOIN NF_COMPRA_PRODUTOS              B WITH(NOLOCK) ON B.NF_COMPRA         = A.NF_COMPRA  
             JOIN OPERACOES_FISCAIS               C WITH(NOLOCK) ON C.OPERACAO_FISCAL   = B.OPERACAO_FISCAL  
             JOIN ESPELHO                         D WITH(NOLOCK) ON D.NF_COMPRA_PRODUTO = B.NF_COMPRA_PRODUTO          
            WHERE 1 = 1  
              AND C.TIPO_OPERACAO   IN (1,10)  
              AND B.PRODUTO = @PRODUTO

              
           UNION ALL  
               
             --PEGA UNIT�RIO DA ALTERA��O DE CUSTO--  
           SELECT A.PRODUTO,  
                  B.DATA_HORA AS MOVIMENTO,  
                  A.FORMULARIO_ORIGEM,  
                  A.TAB_MASTER_ORIGEM,  
                  A.ALTERACAO_CUSTO         AS REG_MASTER_ORIGEM,  
                  A.EMPRESA,
                  A.ALTERACAO_CUSTO_EMPRESA AS CHAVE,  
                  0 AS DESCONTO,  
                  A.CUSTO_NOVO AS PRECO_COMPRA,  
                  0 AS VALOR_IPI,
                  0 AS ICMS_SUBST_VALOR,
                  0 AS ICMS_SUBST_VALOR_PAGAR,
                  0 AS DESPESAS,
                  0 AS FRETE,
                  0 AS SEGURO,
                  0 AS REPASSE,
                  0 AS VALOR_PIS,
                  0 AS VALOR_COFINS,                                                      
                  A.CUSTO_NOVO AS CUSTO,  
                  2 AS SEQUENCIA          
                      
             FROM ALTERACOES_CUSTOS_EMPRESAS  A WITH(NOLOCK)                                              
             JOIN ALTERACOES_CUSTOS           B WITH(NOLOCK) ON B.ALTERACAO_CUSTO = A.ALTERACAO_CUSTO
            WHERE 1 = 1  
              AND A.PRODUTO = @PRODUTO

          
         
           UNION ALL  
                         
           --CUSTO INICIAL - TODOS--  
           SELECT A.PRODUTO,  
                  NULL AS MOVIMENTO,  
                  NULL AS FORMULARIO_ORIGEM,  
                  NULL AS TAB_MASTER_ORIGEM,  
                  NULL AS REG_MASTER_ORIGEM,  
                  NULL AS EMPRESA,
                  A.PRODUTO AS CHAVE,  
                  0 AS DESCONTO,  
                  A.CUSTO AS PRECO_COMPRA,    
                  0 AS VALOR_IPI,
                  0 AS ICMS_SUBST_VALOR,
                  0 AS ICMS_SUBST_VALOR_PAGAR,
                  0 AS DESPESAS,
                  0 AS FRETE,
                  0 AS SEGURO,
                  0 AS REPASSE,
                  0 AS VALOR_PIS,
                  0 AS VALOR_COFINS,                                
                  A.CUSTO   AS CUSTO,  
                  4 AS SEQUENCIA                            
                      
            FROM CUSTO_INICIAL A WITH(NOLOCK)                     
            WHERE 1 = 1   
              AND A.PRODUTO = @PRODUTO

          
        )  X         
        
        ) Z ON Z.PRODUTO            = A.PRODUTO  

 LEFT JOIN FORMULARIOS          B WITH(NOLOCK) ON B.NUMID            = Z.FORMULARIO_ORIGEM
 LEFT JOIN NF_COMPRA          C WITH(NOLOCK) ON C.NF_COMPRA          = Z.REG_MASTER_ORIGEM
                                            AND C.TAB_MASTER_ORIGEM  = Z.TAB_MASTER_ORIGEM
                                            AND C.FORMULARIO_ORIGEM  = Z.FORMULARIO_ORIGEM
 LEFT JOIN NF_COMPRA_PRODUTOS D WITH(NOLOCK) ON D.NF_COMPRA          = C.NF_COMPRA
                                            AND D.PRODUTO            = A.PRODUTO
 LEFT JOIN ENDERECOS          E WITH(NOLOCK) ON E.ENTIDADE           = C.ENTIDADE
                  
    ORDER BY ORDEM           
