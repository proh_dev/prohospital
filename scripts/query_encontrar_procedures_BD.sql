select * 
  from master.information_schema.routines 
 where routine_type = 'procedure' 
   and Left(Routine_Name, 3) NOT IN ('sp_', 'xp_', 'ms_')