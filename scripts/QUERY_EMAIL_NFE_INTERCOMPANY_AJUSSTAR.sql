/*Consulta de notas sem Recebimento
  Data: 30-06-2018*/




  DECLARE @DATA_INI DATE
  DECLARE @DATA_FIM DATE
  DECLARE @EMPRESA NUMERIC
  DECLARE @NF_NUMERO VARCHAR(30)

                              

  SET @DATA_INI = GETDATE()-1
  SET @DATA_FIM = GETDATE()
  SET @EMPRESA  =  NULL
  SET @NF_NUMERO = NULL

if object_id('tempdb..#NFE_CANCELAMENTOS') is not null    
   DROP TABLE #NFE_CANCELAMENTOS    

SELECT TOP 1000 A.* INTO #NFE_CANCELAMENTOS 
  FROM NFE_CANCELAMENTOS A WITH(NOLOCK)
 WHERE 1 = 1
  AND (A.EMPRESA = @EMPRESA OR @EMPRESA IS NULL )
  AND (SUBSTRING(A.IDNOTA,26,9) = DBO.ZEROS(@NF_NUMERO,9) OR @NF_NUMERO IS NULL )
ORDER BY NFE_CANCELAMENTO DESC  

--------------------------------------------
 --Captando MSG Sefaz e o protocolo de retorno--
--------------------------------------------

DECLARE @RESULTADO TABLE (   CODIGO_RETORNO         VARCHAR(30) ,
                             DESCRICAO_RETORNO      VARCHAR(255),
                             CHAVE_NF_CANCELADA     VARCHAR(60),
                             PROTOCOLO_CANCELAMENTO VARCHAR(60) ,
                             NFE_CANCELAMENTO       NUMERIC )
DECLARE @NFE_CANCELAMENTO NUMERIC(15)


WHILE EXISTS (SELECT TOP 1 1 FROM #NFE_CANCELAMENTOS )
BEGIN 
SELECT TOP 1 @NFE_CANCELAMENTO = NFE_CANCELAMENTO FROM #NFE_CANCELAMENTOS
   INSERT INTO @RESULTADO 
   SELECT 
          CODIGO_RETORNO         
         ,DESCRICAO_RETORNO      
         ,CHAVE_NF_CANCELADA     
         ,PROTOCOLO_CANCELAMENTO 
         ,@NFE_CANCELAMENTO AS NFE_CANCELAMENTO 

 FROM DBO.NFE_MOTIVO_ERRO_CANCELAMENTO(@NFE_CANCELAMENTO)
           WHERE DESCRICAO_RETORNO IS NOT NULL
   DELETE FROM #NFE_CANCELAMENTOS WHERE NFE_CANCELAMENTO = @NFE_CANCELAMENTO
END 

  SELECT DISTINCT
         769791                    AS FORMULARIO_ORIGEM
        ,753289                    AS TAB_MASTER_ORIGEM
        ,A.NF_FATURAMENTO          AS REG_MASTER_ORIGEM

        ,A.NF_FATURAMENTO            AS NF
        ,A.EMPRESA                   AS EMPRESA
        ,A.ENTIDADE                  AS ENTIDADE
        ,A.NF_SERIE                  AS SERIE
        ,A.NF_NUMERO                 AS NF_NUMERO
        ,A.OPERACAO_FISCAL           AS OPERACAO
        ,B.XML_natOp_B04             AS DESCRICAO
        ,B.CHAVE_NFE                 AS CHAVE
        ,CASE WHEN E.STATUS = 4  --AND (F.CODIGO_RETORNO IN ('135','101','155' , '151'))
            THEN 'NOTA CANCELADA'
            ELSE 'NOTA AUTORIZADA'
          END                      AS STATUS_CANCELAMENTO
        ,B.XML_dhEmi_B09,DATEDIFF(DAY,B.XML_dhEmi_B09,GETDATE() ) AS DIFEREN�A
        ,B.XML_UF_C12


    FROM NF_FATURAMENTO                              A WITH(NOLOCK)
    JOIN NFE_CABECALHO                               B WITH(NOLOCK) ON  B.XML_nNF_B08          = A.NF_NUMERO
                                                                   AND  B.XML_serie_B07        = A.NF_SERIE
                                                                   AND  B.EMPRESA              = A.EMPRESA
                                                                   						       
    LEFT 																				       
    JOIN ESTOQUE_TRANSFERENCIAS_RECEBIMENTOS         C WITH(NOLOCK) ON  A.NF_NUMERO            = C.NF_NUMERO
    LEFT																				       
    JOIN CANCELAMENTOS_NOTAS_FISCAIS                 D WITH(NOLOCK) ON  D.NF_NUMERO            = A.NF_NUMERO    
    LEFT
    JOIN NFE_CANCELAMENTOS                           E WITH(NOLOCK) ON     E.FORMULARIO_ORIGEM = D.FORMULARIO_ORIGEM AND             
                                                                        E.TAB_MASTER_ORIGEM    = D.TAB_MASTER_ORIGEM AND
                                                                        E.REG_MASTER_ORIGEM    = D.REG_MASTER_ORIGEM
	LEFT
	JOIN RECEBIMENTOS_VOLUMES_NF                     F WITH (NOLOCK) ON F.NF_NUMERO            = A.NF_NUMERO -- COMENTADO POR YNOA PEDRO 17/09/2018 PARA CONSIDERAR OS RECEBIMENTOS ENTRE LOJAS NA OP FISCAL 12
    LEFT
    JOIN @RESULTADO                                  G              ON  G.NFE_CANCELAMENTO     = E.NFE_CANCELAMENTO --B.CHAVE_NF_CANCELADA = IDNOTA
	


                                                                 


    WHERE 1=1                                                                    
      AND (A.EMISSAO                >= @DATA_INI)
      AND (A.EMISSAO                <= @DATA_FIM)
      AND (C.ESTOQUE_RECEBIMENTO     IS NULL OR F.RECEBIMENTO IS NULL)
      AND A.EMPRESA                 IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) )
      AND A.ENTIDADE                IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) WHERE EMPRESA_USUARIA < 1000 )
      AND A.REG_MASTER_ORIGEM       IS NOT NULL        
      AND A.OPERACAO_FISCAL         NOT IN (156,140,166,148)    
      AND B.XML_UF_C12 = 'CE' 
      AND DATEDIFF(DAY,B.XML_dhEmi_B09,GETDATE() ) >= 5

      OR B.XML_UF_C12 <> 'CE' 
      AND DATEDIFF(DAY,B.XML_dhEmi_B09,GETDATE() ) >= 13
      AND (A.EMISSAO                >= @DATA_INI)
      AND (A.EMISSAO                <= @DATA_FIM)
      AND C.ESTOQUE_RECEBIMENTO     IS NULL 
      AND A.EMPRESA                 IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) )
      AND A.ENTIDADE                IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) WHERE EMPRESA_USUARIA < 1000 )
      AND A.REG_MASTER_ORIGEM       IS NOT NULL        
      AND A.OPERACAO_FISCAL         NOT IN (156,140,166,148)    
       


UNION ALL
      
      
  SELECT DISTINCT
         769791                      AS FORMULARIO_ORIGEM
        ,753289                      AS TAB_MASTER_ORIGEM
        ,A.NF_FATURAMENTO            AS REG_MASTER_ORIGEM

        ,A.NF_COMPRA                  AS NF
        ,A.EMPRESA                    AS EMPRESA
        ,A.ENTIDADE                   AS ENTIDADE
        ,A.NF_SERIE                   AS SERIE
        ,A.NF_NUMERO                  AS NF_NUMERO
        ,A.OPERACAO_FISCAL            AS OPERACAO
        ,B.XML_natOp_B04              AS DESCRICAO
        ,B.CHAVE_NFE                  AS CHAVE
        ,CASE WHEN E.STATUS = 4  
            THEN 'NOTA CANCELADA'
            ELSE 'NOTA AUTORIZADA'
          END                      AS STATUS_CANCELAMENTO
        ,B.XML_dhEmi_B09,DATEDIFF(DAY,B.XML_dhEmi_B09,GETDATE() ) AS DIFEREN�A
        ,B.XML_UF_C12


    FROM NF_COMPRA                                   A WITH(NOLOCK)
    JOIN NFE_CABECALHO                               B WITH(NOLOCK) ON  B.XML_nNF_B08    = A.NF_NUMERO
                                                                   AND  B.XML_serie_B07  = A.NF_SERIE
                                                                   AND  B.EMPRESA        = A.EMPRESA
                                                                   
    LEFT
    JOIN CANCELAMENTOS_NOTAS_FISCAIS                 D WITH(NOLOCK) ON  D.NF_NUMERO      = A.NF_NUMERO    
    LEFT
    JOIN NFE_CANCELAMENTOS                           E WITH(NOLOCK) ON  E.FORMULARIO_ORIGEM    = D.FORMULARIO_ORIGEM AND             
                                                                        E.TAB_MASTER_ORIGEM    = D.TAB_MASTER_ORIGEM AND
                                                                        E.REG_MASTER_ORIGEM    = D.REG_MASTER_ORIGEM
    LEFT
    JOIN @RESULTADO                                  F              ON  F.NFE_CANCELAMENTO     = E.NFE_CANCELAMENTO --B.CHAVE_NF_CANCELADA = IDNOTA
    LEFT
    JOIN NF_FATURAMENTO                              G              ON  G.NF_NUMERO  = A.NF_NUMERO
                                                                   AND  G.NF_SERIE   = A.NF_SERIE
                                                                   AND  G.EMPRESA    = A.EMPRESA

                                                                 


    WHERE 1=1                                                                    
      AND (G.EMISSAO                >= @DATA_INI)
      AND (G.EMISSAO                <= @DATA_FIM)
      AND A.EMPRESA                  = 1000 
      AND A.ENTIDADE                IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) )
      AND A.REG_MASTER_ORIGEM       IS NOT NULL        
      AND A.SISTEMA_LEGADO           = 'N'   
      AND (A.NF_NUMERO = @NF_NUMERO OR @NF_NUMERO IS NULL)
      AND B.XML_UF_C12 = 'CE' 
      AND DATEDIFF(DAY,B.XML_dhEmi_B09,GETDATE() ) >= 5

      OR B.XML_UF_C12 <> 'CE' 
      AND DATEDIFF(DAY,B.XML_dhEmi_B09,GETDATE() ) >= 13
      AND (G.EMISSAO                >= @DATA_INI)
      AND (G.EMISSAO                <= @DATA_FIM)
      AND A.EMPRESA                  = 1000 
      AND A.ENTIDADE                IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) )
      AND A.REG_MASTER_ORIGEM       IS NOT NULL        
      AND A.SISTEMA_LEGADO           = 'N'  
      AND (A.NF_NUMERO = @NF_NUMERO OR @NF_NUMERO IS NULL)

      
      ORDER BY DIFEREN�A DESC


 


