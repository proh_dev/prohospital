/**********************************************/
/*RELATORIO DE PEDIDOS DE COMPRAS PENDENTES  **/
/*DESENVOLVEDOR: YNOA PEDRO                  **/
/*DATA: 17/09/2018                           **/
/**********************************************/ 
DECLARE @DATA_INICIAL DATETIME
DECLARE @DATA_FINAL   DATETIME
DECLARE @EMPRESA      NUMERIC
DECLARE @FORNECEDOR   NUMERIC
DECLARE @COMPRADOR    NUMERIC



/*----------------------------------------------*
A vari�vel STATUS vai indicar o estado atual do Pedido.
0 ou NULL - Traz todos
1      - Pendentes
2      - Encerrados
*/


SET @DATA_INICIAL                    = :DATA_INICIAL
SET @DATA_FINAL                      = :DATA_FINAL
SET @EMPRESA                         = (CASE WHEN ISNUMERIC(:EMPRESA)         = 1 THEN :EMPRESA     ELSE NULL END)
SET @FORNECEDOR                      = (CASE WHEN ISNUMERIC(:FORNECEDOR)      = 1 THEN :FORNECEDOR  ELSE NULL END)
SET @COMPRADOR                       = (CASE WHEN ISNUMERIC(:COMPRADOR )      = 1 THEN :COMPRADOR   ELSE NULL END)


--SET @EMPRESA               = 1000
----SET @FORNECEDOR            = 2109
--SET @DATA_INICIAL          = '01/01/2018'
--SET @DATA_FINAL            = '27/09/2018'
--SET @COMPRADOR             = 4


if OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL DROP TABLE #TEMP
if OBJECT_ID('TEMPDB..#TEMP2')IS NOT NULL DROP TABLE #TEMP2
if OBJECT_ID('TEMPDB..#TEMP3')IS NOT NULL DROP TABLE #TEMP3


----------------------------------------------------------------
-- temp criada para capturar os dados basicos do pedido       --
----------------------------------------------------------------


SELECT 
       A.PEDIDO_COMPRA
      ,B.EMPRESA
      ,B.FORMULARIO_ORIGEM
      ,B.TAB_MASTER_ORIGEM
      ,D.PRODUTO
      ,A.PEDIDO_COMPRA_PRODUTO
      ,SUM(A.PEDIDO)             AS PEDIDO
      ,SUM(A.RECEBIMENTO)        AS RECEBIMENTO
      ,SUM(A.CANCELAMENTO)       AS CANCELAMENTO
      ,B.ENTIDADE                AS FORNECEDOR
      ,C.COMPRADOR               AS COMPRADOR 
	  ,D.VALOR_UNITARIO          AS VALOR_UNITARIO 
	  ,B.CONDICOES_PAGAMENTO     AS CONDICOES_PAGAMENTO

     INTO #TEMP 
     FROM PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES A WITH(NOLOCK)
     JOIN PEDIDOS_COMPRAS                     B WITH(NOLOCK) ON B.PEDIDO_COMPRA = A.PEDIDO_COMPRA        
LEFT JOIN COMPRADORES                         C WITH(NOLOCK) ON B.COMPRADOR     = C.COMPRADOR
     JOIN PEDIDOS_COMPRAS_PRODUTOS            D WITH(NOLOCK) ON B.PEDIDO_COMPRA = D.PEDIDO_COMPRA




    WHERE CONVERT(DATE, B.DATA_HORA ) >= @DATA_INICIAL
      AND CONVERT(DATE, B.DATA_HORA ) <= @DATA_FINAL
      AND (B.EMPRESA  = @EMPRESA    OR @EMPRESA    IS NULL)
      AND (B.ENTIDADE = @FORNECEDOR OR @FORNECEDOR IS NULL)
	  AND (C.COMPRADOR = @COMPRADOR OR @COMPRADOR  IS NULL)
	  GROUP BY
	   A.PEDIDO_COMPRA
	  ,B.EMPRESA
	  ,B.FORMULARIO_ORIGEM
	  ,B.TAB_MASTER_ORIGEM
	  ,D.PRODUTO
	  ,A.PEDIDO_COMPRA_PRODUTO
	  ,B.ENTIDADE                
	  ,C.COMPRADOR               
	  ,D.VALOR_UNITARIO  
	  ,B.CONDICOES_PAGAMENTO  

	     

----------------------------------------------------------------
-- temp criada para capturar o Status da PEDIDO               --
----------------------------------------------------------------

 SELECT   A.PEDIDO_COMPRA_PRODUTO   AS PEDIDO_COMPRA_PRODUTO
         ,A.PEDIDO_COMPRA
         ,A.PRODUTO                 AS PRODUTO
         ,'PENDENTE'                AS ESTADO
         ,MAX(A.SALDO)              AS SALDO
		 ,C.CONDICOES_PAGAMENTO     AS CONDICOES_PAGAMENTO
		 ,C.VALOR_UNITARIO          AS VALOR_UNITARIO
         INTO #TEMP2
         FROM PEDIDOS_COMPRAS_PRODUTOS_SALDO  A WITH(NOLOCK) 
         JOIN #TEMP                           C WITH(NOLOCK)  ON A.PEDIDO_COMPRA_PRODUTO = C.PEDIDO_COMPRA_PRODUTO               
                                                             AND A.PRODUTO               = C.PRODUTO
          GROUP BY A.PEDIDO_COMPRA_PRODUTO
                  ,A.PRODUTO  
                  ,A.SALDO   
                  ,A.PEDIDO_COMPRA   
				  ,C.CONDICOES_PAGAMENTO
				  ,C.VALOR_UNITARIO 
          HAVING  MAX(A.SALDO) > 0 


----------------------------------------------------------------
-- temp criada para capturar os totais dos pedidos de vendas  --
----------------------------------------------------------------

DECLARE @TOTAL_PEDIDO MONEY

SELECT @TOTAL_PEDIDO = SUM(A.TOTAL_GERAL)            
       FROM PEDIDOS_COMPRAS_TOTAIS A WITH(NOLOCK)
CROSS APPLY ( SELECT DISTINCT B.PEDIDO_COMPRA 
                FROM #TEMP2 B WITH(NOLOCK)
               WHERE B.PEDIDO_COMPRA = A.PEDIDO_COMPRA
            ) C

----------------------------------------------------------------------
-- temp criada para capturar os totais produtos pendentes pagos     --
----------------------------------------------------------------------

DECLARE @TOTAL_PEDIDO_PENDENTE MONEY

SELECT @TOTAL_PEDIDO_PENDENTE = C.PENDENTE
       FROM  PEDIDOS_COMPRAS_TOTAIS         A WITH(NOLOCK)	  
CROSS APPLY ( SELECT DISTINCT B.PEDIDO_COMPRA, (B.VALOR_UNITARIO * B.SALDO ) AS PENDENTE
                FROM #TEMP2 B WITH(NOLOCK)
               WHERE B.PEDIDO_COMPRA = A.PEDIDO_COMPRA

            ) C 
 
     
----------------------------------------------------------------
-- Select Final da Query                                      --R
----------------------------------------------------------------
DECLARE @TOTAL_PAGO MONEY 
SELECT
    
 DISTINCT
	       A.FORMULARIO_ORIGEM                              AS FORMULARIO_ORIGEM 
          ,A.TAB_MASTER_ORIGEM                              AS TAB_MASTER_ORIGEM
          ,A.PEDIDO_COMPRA                                  AS REG_MASTER_ORIGEM 
          ,A.PEDIDO_COMPRA                                  AS PEDIDO_COMPRA              
          ,A.EMPRESA                                        AS EMPRESA 
          ,A.PRODUTO                                        AS PRODUTO 
          ,D.DESCRICAO                                      AS DESCRICAO 
          ,E.MARCA                                          AS COD_MARCA
          ,E.DESCRICAO                                      AS MARCA
          ,A.PEDIDO                                         AS PEDIDO  
          ,A.RECEBIMENTO                                    AS ATENDIDO 
          ,A.CANCELAMENTO                                   AS ENCERRAMENTO
          ,C.ESTADO                                         AS ESTADO 
          ,C.SALDO                                          AS SALDO
          ,A.FORNECEDOR                                     AS COD_FORNECEDOR
          ,G.NOME                                           AS FORNECEDOR
          ,A.COMPRADOR                                      AS COD_COMPRADOR
          ,H.NOME                                           AS COMPRADOR
          ,A.VALOR_UNITARIO                                 AS VALOR_UNITARIO
          ,J.TOTAL_GERAL                                    AS TOTAL_GERAL
          ,@TOTAL_PEDIDO                                    AS TOTAL_PEDIDOS
		  ,(A.VALOR_UNITARIO * C.SALDO)                     AS TOTAL_PENDENTE_PEDIDO
          ,CASE WHEN A.CONDICOES_PAGAMENTO = 124
		        THEN (A.VALOR_UNITARIO * C.SALDO)
				ELSE 0 END                                   AS TOTAL_SALDO_PAGO 

		  ,  (A.VALOR_UNITARIO * C.SALDO)   - ( CASE WHEN A.CONDICOES_PAGAMENTO = 124
						        THEN (A.VALOR_UNITARIO * C.SALDO)
           				      	ELSE 0    END )      AS TOTAL_SALDO_PAGAR

           

          
     FROM #TEMP                             A WITH(NOLOCK)

     JOIN #TEMP2                            C WITH(NOLOCK) ON C.PEDIDO_COMPRA_PRODUTO = A.PEDIDO_COMPRA_PRODUTO
                                                          AND C.PRODUTO               = A.PRODUTO     
     JOIN PRODUTOS                          D WITH(NOLOCK) ON D.PRODUTO               = A.PRODUTO
LEFT JOIN MARCAS                            E WITH(NOLOCK) ON E.MARCA                 = D.MARCA
LEFT JOIN PRODUTOS_FORNECEDORES             F WITH(NOLOCK) ON F.PRODUTO               = D.PRODUTO
LEFT JOIN ENTIDADES                         G WITH(NOLOCK) ON A.FORNECEDOR            = G.ENTIDADE
LEFT JOIN COMPRADORES                       H WITH(NOLOCK) ON H.COMPRADOR             = A.COMPRADOR
LEFT JOIN PEDIDOS_COMPRAS_TOTAIS            J WITH(NOLOCK) ON J.PEDIDO_COMPRA         = A.PEDIDO_COMPRA



   GROUP BY  A.FORMULARIO_ORIGEM        
            ,A.TAB_MASTER_ORIGEM        
            ,A.PEDIDO_COMPRA            
            ,A.PEDIDO_COMPRA            
            ,A.EMPRESA                  
            ,A.PRODUTO                  
            ,D.DESCRICAO                
            ,E.MARCA                    
            ,E.DESCRICAO                
            ,A.FORNECEDOR               
            ,G.NOME                     
            ,A.COMPRADOR                
            ,C.ESTADO                   
            ,C.SALDO                    
            ,H.NOME                     
            ,A.PEDIDO                   
            ,A.RECEBIMENTO              
            ,A.VALOR_UNITARIO            
            ,J.TOTAL_GERAL    
			,A.PEDIDO                                  
			,A.RECEBIMENTO                             
			,A.CANCELAMENTO   
			,A.CONDICOES_PAGAMENTO   


  ORDER BY A.FORNECEDOR , PEDIDO_COMPRA             