-----------------------------------------------------------------------
--              QUERY CONVENIOS USUARIOS                             --
--              DESENVOLVEDOR: YNOA PEDRO                            --
-----------------------------------------------------------------------

------------------------------VARIAVEIS DE PRODUCAO---------------------------------------------
DECLARE @FUNCIONARIO NUMERIC
SET     @FUNCIONARIO = :ENTIDADE
------------------------------VARIAVEIS DE PRODUCAO---------------------------------------------

------------------------------VARIAVEIS DE HOMOLOGACAO------------------------------------------
--DECLARE @FUNCIONARIO NUMERIC
--SET     @FUNCIONARIO = 114124
------------------------------VARIAVEIS DE HOMOLOGACAO------------------------------------------


SELECT  DISTINCT
         A.ENTIDADE                                         AS FUNCIONARIO
        ,A.NOME                                             AS NOME
        ,A.INSCRICAO_FEDERAL                                AS CPF
        ,B.TITULO                                           AS DOCUMENTO
        ,B.EMISSAO                                          AS EMISSAO
		,B.VENCIMENTO                                       AS VENCIMENTO
        ,B.VALOR                                            AS VALOR_DOCUMENTO
        ,C.SALDO                                            AS SALDO
        ,CASE WHEN C.SITUACAO_TITULO = 1
              THEN 'TITULO � PAGAR'
              ELSE 'TITULO PAGO' END                        AS ESTADO_DOCUMENTO
	    ,B.EMPRESA                                          AS EMPRESA
		,E.NOME                                             AS EMPRESA_N
	
         
     FROM FUNCIONARIOS                                      A WITH(NOLOCK)
     JOIN TITULOS_RECEBER                                   B WITH(NOLOCK) ON B.ENTIDADE        = A.ENTIDADE
     JOIN TITULOS_RECEBER_SALDO                             C WITH(NOLOCK) ON B.TITULO_RECEBER  = C.TITULO_RECEBER

LEFT JOIN EMPRESAS_USUARIAS                                 E WITH(NOLOCK) ON E.EMPRESA_USUARIA = B.EMPRESA

WHERE 1=1
  AND (B.ENTIDADE = @FUNCIONARIO OR @FUNCIONARIO IS NULL)
    

UNION ALL

SELECT DISTINCT
         A.ENTIDADE                                         AS FUNCIONARIO
        ,A.NOME                                             AS NOME
        ,A.INSCRICAO_FEDERAL                                AS CPF
        ,D.ECF_CUPOM                                        AS DOCUMENTO
        ,D.MOVIMENTO                                        AS EMISSAO
		,D.MOVIMENTO                                        AS VENCIMENTO
        ,D.SUBTOTAL                                         AS VALOR_DOCUMENTO
        ,CASE WHEN D.STATUS = 'A'
              THEN D.SUBTOTAL
              WHEN D.STATUS = 'C'
              THEN 0
              END                                                  AS SALDO 
        ,CASE WHEN D.STATUS = 'A'
              THEN 'TITULO EM ABERTO'
              WHEN D.STATUS = 'C'
              THEN 'TITULO PAGO'
              END                                           AS ESTADO_DOCUMENTO
		,D.LOJA                                             AS EMPRESA
		,F.NOME                                             AS EMPRESA_N
     

     FROM FUNCIONARIOS                             A WITH(NOLOCK)
     JOIN ENTIDADES_CONVENIOS_CARTOES              B WITH(NOLOCK) ON A.ENTIDADE        = B.FUNCIONARIO
     JOIN PDV_CONVENIOS                            C WITH(NOLOCK) ON C.CLIENTE         = B.CARTAO_CONVENIO
     JOIN PDV_VENDAS                               D WITH(NOLOCK) ON D.VENDA           = C.VENDA
                                                                 AND D.LOJA            = C.LOJA
                                                                 AND D.MOVIMENTO       = C.MOVIMENTO
                                                                 AND D.CAIXA           = C.CAIXA

LEFT JOIN EMPRESAS_USUARIAS                        F WITH(NOLOCK) ON F.EMPRESA_USUARIA = D.LOJA

WHERE 1=1
  AND (B.FUNCIONARIO = @FUNCIONARIO OR @FUNCIONARIO IS NULL)

     