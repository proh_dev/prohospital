ALTER PROCEDURE [dbo].[EMAIL_ENVIO_DESPACHO_PENDENTE] AS     
BEGIN                        
                  
DECLARE @tableHTML       NVARCHAR(MAX)  
DECLARE @EMAIL           VARCHAR(250)    
DECLARE @TITULO_EMAIL    VARCHAR(50) = 'Solicita��es Pendentes de Separa��o'  
DECLARE @EMPRESA_ORIGEM  VARCHAR(50)  
DECLARE @EMPRESA_DESTINO NUMERIC(15)  
  
IF OBJECT_ID('TEMPDB..#APROVACOES' ) IS NOT NULL DROP TABLE #APROVACOES  
IF OBJECT_ID('TEMPDB..#DADOS_ENVIO') IS NOT NULL DROP TABLE #DADOS_ENVIO  
IF OBJECT_ID('TEMPDB..#EMPRESAS'   ) IS NOT NULL DROP TABLE #EMPRESAS  
  
SELECT DISTINCT  
       B.SOLICITACAO_TRANSFERENCIA  
  INTO #APROVACOES  
  FROM APROVACAO_SOLICITACAO_TRANSF          A WITH(NOLOCK)  
  JOIN APROVACAO_SOLICITACAO_TRANSF_PRODUTOS B WITH(NOLOCK) ON A.APROVACAO_SOLICITACAO_TRANSF = B.APROVACAO_SOLICITACAO_TRANSF  
  
  
SELECT   
      A.SOLICITACAO_TRANSFERENCIA              AS SOLICITACAO  
     ,A.DATA_PREVISAO_SEPARACAO                AS DATA_PREVISTA  
     ,A.MOVIMENTO                              AS MOVIMENTO  
     ,A.CENTRO_ESTOQUE_ORIGEM                  AS ORIGEM_COD  
     ,D.NOME_FANTASIA                          AS ORIGEM  
     ,A.FILIAL                                 AS DESTINO_COD  
     ,E.NOME_FANTASIA                          AS DESTINO  
	 --,'ynoa.pedro@outlook.com'                 AS EMAIL
	 ,G.PRODUTO                                AS PRODUTO
	 ,H.DESCRICAO                              AS DESCRICAO
     ,F.EMAIL                                  AS EMAIL  
  INTO #DADOS_ENVIO  
     FROM SOLICITACAO_TRANSFERENCIAS                     A WITH(NOLOCK)   
LEFT JOIN #APROVACOES                                    B WITH(NOLOCK) ON A.SOLICITACAO_TRANSFERENCIA    = B.SOLICITACAO_TRANSFERENCIA  
     JOIN EMPRESAS_USUARIAS                              D WITH(NOLOCK) ON A.CENTRO_ESTOQUE_ORIGEM        = D.EMPRESA_USUARIA                 
     JOIN EMPRESAS_USUARIAS                              E WITH(NOLOCK) ON A.CENTRO_ESTOQUE_DESTINO       = E.EMPRESA_USUARIA  
     JOIN PARAMETROS_ESTOQUE                             F WITH(NOLOCK) ON E.EMPRESA_USUARIA              = F.EMPRESA_USUARIA  
LEFT JOIN SOLICITACAO_TRANSFERENCIAS_PRODUTOS            G WITH(NOLOCK) ON A.SOLICITACAO_TRANSFERENCIA    = G.SOLICITACAO_TRANSFERENCIA
LEFT JOIN PRODUTOS                                       H WITH(NOLOCK) ON G.PRODUTO                      = H.PRODUTO
    WHERE 1=1  
      AND DATEDIFF(DAY,A.DATA_PREVISAO_SEPARACAO,GETDATE())  > 1  
      AND B.SOLICITACAO_TRANSFERENCIA IS NULL  


	  SELECT * FROM #DADOS_ENVIO
  
---------------------------------------------------------------  
  
SELECT DISTINCT  
       A.DESTINO AS NOME_ORIGEM  
   ,A.DESTINO_COD  
   ,A.EMAIL  
  INTO #EMPRESAS  
  FROM #DADOS_ENVIO A WITH(NOLOCK)  
  
  
WHILE EXISTS( SELECT TOP 1 1 FROM #EMPRESAS (NOLOCK) )  
BEGIN  
  
 SELECT TOP 1 @EMPRESA_ORIGEM = NOME_ORIGEM , @EMPRESA_DESTINO = A.DESTINO_COD , @EMAIL = A.EMAIL FROM #EMPRESAS A WITH(NOLOCK) ORDER BY A.DESTINO_COD ASC  
  
  
SET @tableHTML =         
      
    N'<style type="text/css">          
              .formato {          
               font-family: Verdana, Geneva, sans-serif;          
               font-size: 14px;       
      font-style: normal;      
      font-weight: bold;         
              }          
              .formato2 {          
               font-family: Verdana, Geneva, sans-serif;          
               font-size: 14px;       
      font-style: normal;      
      font-weight: normal;         
              }          
              .alinhameto {          
               text-align: center;          
              }          
              </style>'                    +          
            N'<H1 class="formato" >Solicita��o Pendentes de Separa��o <br>          
   EMPRESA: '+ @EMPRESA_ORIGEM + '<br> <br>      
   Voc� est� recebendo uma c�pia do checkout realizado nesta data.   <br>       
   Caso haja alguma diverg�ncia com quantidades e/ou valores descritos abaixo, pedimos entrar em contato com nosso representante. <br><br>       
     </H1>           
    ' +          
    N'<table border="1" class="formato">' +          
    N'<tr>           
           <th>SOLICITACAO</th>          
           <th>DATA PREVISTA</th>          
           <th>ORIGEM</th>          
           <th>DESTINO</th>          
           <th>MOVIMENTO</th>   
		   <th>PRODUTOS</th>  
      </tr>'                              +          
          
    CAST ( (           
          
  SELECT           
    td = CAST( A.SOLICITACAO  AS VARCHAR(30) )       ,  '' ,          
    td = CONVERT( VARCHAR , A.DATA_PREVISTA , 103 )  ,  '' ,          
    td = A.ORIGEM             ,  '' ,          
    td = A.DESTINO            ,  '' ,          
    td = CONVERT( VARCHAR , A.MOVIMENTO , 103 )      ,  ''  ,
    td = CONVERT(VARCHAR, A.PRODUTO     , 200 ) + '-' + A.DESCRICAO     , '' 
          
   FROM #DADOS_ENVIO A WITH(NOLOCK) WHERE A.DESTINO_COD = @EMPRESA_DESTINO     
          
              FOR XML PATH('tr'), TYPE           
    ) AS NVARCHAR(MAX) )     
       
       
 ;  
  
  
  
   DELETE FROM #EMPRESAS WHERE DESTINO_COD = @EMPRESA_DESTINO  
  
EXEC MSDB.DBO.SP_SEND_DBMAIL          
          
  @BLIND_COPY_RECIPIENTS   = @EMAIL,          
  @SUBJECT                 = @TITULO_EMAIL ,          
  @BODY                    = @tableHTML,          
  @BODY_FORMAT             = 'HTML';     
    
  WAITFOR DELAY '00:00:05'    
  
  
END  
  
END