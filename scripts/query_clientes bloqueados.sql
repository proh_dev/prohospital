/*=================================================*/
/*Consulta de Clientes Bloqueados ou Desbloqueados */
/*Desenvolvedor : Yno� Pedro                       */
/*Data: 17/05/2018                                 */
/*=================================================*/
DECLARE @ENTIDADE   NUMERIC 

--IF ISNUMERIC(:ENTIDADE)= 1 SET @ENTIDADE  = :ENTIDADE  ELSE SET @ENTIDADE  = NULL

--SET @ENTIDADE = NULL


--SELECT * FROM BLOQUEIO_DESBLOQUEIO_CLIENTES

 
SELECT 638644                                      AS FORMULARIO_ORIGEM  , 
	   637827                                      AS TAB_MASTER_ORIGEM  ,
	   A.REG_MASTER_ORIGEM                                               ,

       A.BLOQUEIO_DESBLOQUEIO_CLIENTE              AS BLOQUEIO           ,
       A.DATA_HORA                                 AS DATA_BLOQUEIO      ,
       C.NOME                                      AS BLOQUEADOR         ,
	   B.ENTIDADE                                  AS CLIENTE            ,
	   D.NOME                                      AS CLIENTE_BLOQUEADO  ,
	   F.DESCRICAO                                 AS TIPO               ,
	   E.DESCRICAO                                 AS MOTIVO             ,
	   B.JUSTIFICATIVA                             AS JUSTIFICATIVA      ,
	   B.DESCRICAO                                 AS DESCRICAO   
	   
	FROM  BLOQUEIO_DESBLOQUEIO_CLIENTES             A WITH(NOLOCK)
	JOIN BLOQUEIO_DESBLOQUEIO_CLIENTES_DETALHE      B WITH(NOLOCK) ON A.BLOQUEIO_DESBLOQUEIO_CLIENTE = B.BLOQUEIO_DESBLOQUEIO_CLIENTE
	JOIN USUARIOS                                   C WITH(NOLOCK) ON A.USUARIO_LOGADO               = C.USUARIO
	JOIN ENTIDADES                                  D WITH(NOLOCK) ON B.ENTIDADE                     = D.ENTIDADE
	JOIN MOTIVOS_BLOQ_DESB_CLIENTES                 E WITH(NOLOCK) ON B.MOTIVO                       = E.MOTIVO
	JOIN TIPOS_BLOQUEIO_DESB_CLIENTE                F WITH(NOLOCK) ON B.TIPO_BLOQUEIO_DESB_CLIENTE   = F.TIPO_BLOQUEIO_DESB_CLIENTE    

WHERE 1=1
  AND (B.ENTIDADE = @ENTIDADE OR @ENTIDADE IS NULL) 

