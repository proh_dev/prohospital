DECLARE @EMPRESA NUMERIC(15,0)

SET @EMPRESA = 1

SELECT 
       A.EMPRESA, 
       A.NF_COMPRA, 
       A.NF_NUMERO, 
       A.ENTIDADE, 
       D.NOME,
       A.EMISSAO, 
       A.MOVIMENTO, 
       A.PEDIDO_COMPRA, 
       A.RECEBIMENTO,
       DATEDIFF(DAY,A.EMISSAO,CONVERT(VARCHAR,GETDATE(),103)) DIAS_PENDENCIA,
       CASE
         WHEN A.PEDIDO_COMPRA         IS NULL     THEN 'Sem Pedido Vinculado!'
         WHEN A.RECEBIMENTO           IS NULL     THEN 'Pendente Recebimento!'
         WHEN A.PEDIDO_COMPRA         IS NOT NULL 
          AND A.RECEBIMENTO           IS NOT NULL 
          AND C.RECEBIMENTO_APROVACAO IS NULL     THEN 'Pendente Aprova��o do Recebimento!'
         WHEN A.PEDIDO_COMPRA         IS NOT NULL 
          AND A.RECEBIMENTO           IS NOT NULL                                 
          AND C.RECEBIMENTO_APROVACAO IS NOT NULL THEN 'Pendente Processamento Fiscal!'
       END                                                    AS NOTA_STATUS

  FROM NF_COMPRA                                A WITH(NOLOCK)
  LEFT 									        
  JOIN RECEBIMENTOS_FISICOS_LANCAMENTOS         B WITH(NOLOCK) ON B.RECEBIMENTO           = A.RECEBIMENTO
  LEFT 									        									      
  JOIN APROVACOES_RECEB_VOLUMES_ITENS           C WITH(NOLOCK) ON C.RECEBIMENTO           = A.RECEBIMENTO
  JOIN ENTIDADES                                D WITH(NOLOCK) ON D.ENTIDADE              = A.ENTIDADE
  JOIN ENDERECOS                                E WITH(NOLOCK) ON E.ENTIDADE              = D.ENTIDADE
  JOIN ENDERECOS                                F WITH(NOLOCK) ON F.ENTIDADE              = A.EMPRESA
  LEFT									        									      
  JOIN NF_FATURAMENTO                           G WITH(NOLOCK) ON A.NF_NUMERO             = G.NF_NUMERO
                                                              AND A.NF_SERIE              = G.NF_SERIE 
                                                              AND A.ENTIDADE              = G.EMPRESA 
  LEFT									        									      
  JOIN NF_FATURAMENTO_DEVOLUCOES                H WITH(NOLOCK) ON H.NF_FATURAMENTO_ORIGEM = G.NF_FATURAMENTO
  LEFT 
  JOIN NF_COMPRA_DEVOLUCOES_NOTAS_RELACIONADAS  I WITH(NOLOCK) ON A.NF_COMPRA             =  I.NF_COMPRA


  WHERE 1=1
  AND A.PROCESSAR      = 'N'
  AND A.SISTEMA_LEGADO = 'N'
  AND A.EMPRESA        = @EMPRESA
  AND H.NF_FATURAMENTO_DEVOLUCAO IS NULL  
  AND I.NF_COMPRA_DEVOLUCAO_NOTA_RELACIONADA IS NULL

  AND DATEDIFF(DAY,A.EMISSAO,CONVERT(VARCHAR,GETDATE(),103)) >= CASE WHEN E.ESTADO <> F.ESTADO THEN 16 ELSE 4 END

ORDER BY A.EMPRESA, A.EMISSAO
