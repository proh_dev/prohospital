-----------------------------------------------------
-- QUERY VALE CREDITOS CONSULTA USADOS E CANCELADOS --
-- DESENVOLVEDOR: YNOA PEDRO                        --
-- DATA 18-10-2018                                  --
------------------------------------------------------
DECLARE @EMPRESA           NUMERIC
DECLARE @DATA_CRIADO_INI   DATETIME
DECLARE @DATA_CRIADO_FIM   DATETIME
DECLARE @DATA_USADO_INI    DATETIME
DECLARE @DATA_USADO_FIM    DATETIME
DECLARE @ENTIDADE          NUMERIC
DECLARE @TIPO              NUMERIC

----SET @EMPRESA = 2
SET @DATA_CRIADO_INI = '01/01/2000' 
SET @DATA_CRIADO_FIM = '31/10/2018'
--SET @DATA_USADO_INI  = '01/01/2017'  
--SET @DATA_USADO_FIM  = '31/10/2018'  
--SET @ENTIDADE =  188114
SET @TIPO     =  3

--VARIAVEL TIPO INDICA VALES 1- USADOS 2-CANCELADOS VAZIO =  TODOS)
--IF ISNUMERIC(:EMPRESA ) = 1 SET @EMPRESA  =  :EMPRESA  ELSE SET @EMPRESA = NULL
--IF ISNUMERIC(:TIPO ) = 1 SET @TIPO  =  :TIPO  ELSE SET @TIPO = NULL
--IF ISNUMERIC(:ENTIDADE ) = 1 SET @ENTIDADE  =  :ENTIDADE  ELSE SET @ENTIDADE = NULL
--IF ISDATE(:DATA_CRIADO_INI)   = 1   BEGIN SET @DATA_CRIADO_INI      = :DATA_CRIADO_INI     END   ELSE SET @DATA_CRIADO_INI       = NULL 
--IF ISDATE(:DATA_CRIADO_FIM)   = 1   BEGIN SET @DATA_CRIADO_FIM      = :DATA_CRIADO_FIM     END   ELSE SET @DATA_CRIADO_FIM       = NULL 
                                                                                             
--IF ISDATE(:DATA_USADO_INI)   = 1   BEGIN SET @DATA_USADO_INI      = :DATA_USADO_INI     END   ELSE SET @DATA_USADO_INI       = NULL 
--IF ISDATE(:DATA_USADO_FIM )   = 1   BEGIN SET @DATA_USADO_FIM       = :DATA_USADO_FIM      END   ELSE SET @DATA_USADO_FIM        = NULL 
                                                                                             

 
	 
SELECT 
         A.CREDITO_CLIENTE                        AS CREDITO
		,C.EMPRESA                                AS EMPRESA
		,E.NOME                                   AS EMPRESA_NOME
	    ,A.ENTIDADE                               AS ENTIDADE
		,D.NOME                                   AS CLIENTE
		,A.NUMERO_CREDITO                         AS VALE
		,A.VALOR                                  AS VALOR_VALE
		,A.DATA_HORA                              AS DATA_CRIADA
		,B.DATA_HORA                              AS DATA_USO

        FROM CREDITOS_CLIENTES A
   LEFT JOIN CREDITOS_CLIENTES_USADOS             B WITH(NOLOCK) ON A.CREDITO_CLIENTE    = B.CREDITO_CLIENTE 
   LEFT JOIN DEV_PRODUTOS_CAIXAS                  C WITH(NOLOCK) ON A.NUMERO_CREDITO     = C.DEVOLUCAO_PRODUTO
   LEFT JOIN ENTIDADES                            D WITH(NOLOCK) ON A.ENTIDADE           = D.ENTIDADE
   LEFT JOIN EMPRESAS_USUARIAS                    E WITH(NOLOCK) ON C.EMPRESA            = E.EMPRESA_USUARIA
   LEFT JOIN CANCELAMENTOS_VALE_CREDITO           F WITH(NOLOCK) ON C.DEVOLUCAO_PRODUTO  = F.DEVOLUCAO_PRODUTO


   WHERE 1=1
     AND (A.DATA_HORA  >= @DATA_CRIADO_INI   OR @DATA_CRIADO_INI IS NULL) 
	 AND (A.DATA_HORA  <= @DATA_CRIADO_FIM   OR @DATA_CRIADO_FIM IS NULL)
	 AND (B.DATA_HORA  >= @DATA_USADO_INI    OR @DATA_USADO_INI  IS NULL) 
     AND (B.DATA_HORA  <= @DATA_USADO_FIM    OR @DATA_USADO_FIM  IS NULL)

	 AND (A.ENTIDADE    = @ENTIDADE           OR @ENTIDADE        IS NULL)
	 AND (C.EMPRESA     = @EMPRESA            OR @EMPRESA         IS NULL)
	 AND ( 
	                (@TIPO = 1   AND F.CANCELAMENTO_VALE_CREDITO IS NULL     AND B.CREDITO_CLIENTE_USADO IS NOT NULL )
	           OR   (@TIPO = 2   AND F.CANCELAMENTO_VALE_CREDITO IS NULL     AND B.CREDITO_CLIENTE_USADO IS NULL     )
	           OR   (@TIPO = 3   AND F.CANCELAMENTO_VALE_CREDITO IS NOT NULL AND B.CREDITO_CLIENTE_USADO IS NULL     )
	           OR   (@TIPO IS NULL)  
	     )