DECLARE @PRODUTO  NUMERIC 
DECLARE @EMPRESA NUMERIC
--COLOCAR 30% E ESTOQUE POSITIVO
--SET @PERCENTUAL = :PERCENTUAL/100

--SET @EMPRESA = 1000
IF ISNUMERIC(:EMPRESA)= 1 SET @EMPRESA  = :EMPRESA  ELSE SET @EMPRESA  = NULL

--SET @PRODUTO =12026
--SET @PRODUTO =:PRODUTO       

      
 SELECT                                           
        23                               AS FORMULARIO_ORIGEM     ,
		62                               AS TAB_MASTER_ORIGEM     ,
		Z.PRODUTO                        AS REG_MASTER_ORIGEM     ,
		
		Z.PRODUTO                        AS PRODUTO               ,  
        A.DESCRICAO 			         AS PRODUTO_DESC          ,
        Z.REG_MASTER_ORIGEM              AS CHAVE_MASTER          ,				
        --B.DESCRICAO                      AS PROCESSO_ORIGEM       ,
                                                             
        Z.FORMULARIO_ORIGEM                                       ,               
        Z.TAB_MASTER_ORIGEM                                       ,   	         
        Z.REG_MASTER_ORIGEM                                       ,   	         
                                                      
        Z.EMPRESA				         AS EMPRESA               ,
        --C.NF_NUMERO 			         AS NUMERO_NF             ,
        						         
        --Z.DESCONTO                       AS DESCONTO              ,  
              					         
        Z.PRECO_COMPRA                   AS PRECO_COMPRA          ,		
		Z1.PRECO_COMPRA                  AS PRIMEIRA_COMPRA       ,	         
        --Z.DESPESAS                       AS DESPESAS              ,				         
        --Z.FRETE                          AS FRETE                 ,				         
        --Z.SEGURO                         AS SEGURO                ,				         
        --Z.REPASSE                        AS REPASSE               ,				         
        --Z.CUSTO                          AS CUSTO                 ,      			         
        --Z.CUSTO_GERENCIAL                AS CUSTO_GERENCIAL       ,                       
        CONVERT(DATE,Z.DATA_HORA)        AS DATA                  ,  
        --Z.SEQUENCIA                      AS SEQUENCIA             ,  			         
        --Z.ORDEM                          AS ORDEM                 ,				         
        C.ENTIDADE                       AS ENTIDADE              ,				         
        E.NOME                           AS NOME                  ,
        D.QUANTIDADE_ESTOQUE             AS QUANTIDADE_ESTOQUE  
             
   FROM PRODUTOS A WITH(NOLOCK)  
              
   JOIN (  
                                          
 SELECT DISTINCT 
        X.PRODUTO                 ,  
        X.DATA_HORA               ,  
        X.DESCONTO                ,  
        X.FORMULARIO_ORIGEM       ,  
        X.TAB_MASTER_ORIGEM       ,  
        X.REG_MASTER_ORIGEM       ,  
        X.EMPRESA                 , 
        X.SEQUENCIA               ,   
        X.PRECO_COMPRA            ,
        X.DESPESAS                ,
        X.FRETE                   ,
        X.SEGURO                  ,
        X.REPASSE                 ,
        X.CUSTO                   ,     
        X.CUSTO_GERENCIAL         ,                                                  
        ROW_NUMBER() OVER ( PARTITION BY  X.PRODUTO ORDER BY X.PRODUTO, X.DATA_HORA DESC, X.SEQUENCIA DESC )  AS ORDEM  
               
   FROM (   
           --PEGA OS UNITARIOS DA NOTA--  
           SELECT DISTINCT
                  B.PRODUTO,  
                  A.MOVIMENTO AS DATA_HORA,  
                  A.FORMULARIO_ORIGEM,  
                  A.TAB_MASTER_ORIGEM,  
                  A.NF_COMPRA AS REG_MASTER_ORIGEM,  
                  A.EMPRESA, 
                  CASE WHEN B.TOTAL_DESCONTO > 0    
                       THEN CASE WHEN B.TIPO_DESCONTO = 3    
                                 THEN B.DESCONTO    
                                 ELSE ROUND( ( 1 - (B.TOTAL_PRODUTO / (B.QUANTIDADE * B.VALOR_UNITARIO) ) ) * 100 , 2 )    
                            END    
                       ELSE 0    
                   END                           AS DESCONTO,    
                               
                  
				  D.VALOR_PRODUTO_LIQUIDO        AS PRECO_COMPRA,                                                                     
                  D.DESPESAS,
                  D.FRETE,
                  D.SEGURO,
                  D.REPASSE,                                  
                  D.CUSTO,  
                  D.CUSTO_GERENCIAL,
                  1 AS SEQUENCIA  
          
             FROM NF_COMPRA                       A WITH(NOLOCK)                                              
             JOIN NF_COMPRA_PRODUTOS              B WITH(NOLOCK) ON B.NF_COMPRA         = A.NF_COMPRA  
             JOIN OPERACOES_FISCAIS               C WITH(NOLOCK) ON C.OPERACAO_FISCAL   = B.OPERACAO_FISCAL  
             JOIN ESPELHO                         D WITH(NOLOCK) ON D.NF_COMPRA_PRODUTO = B.NF_COMPRA_PRODUTO          
            WHERE 1 = 1  
              AND C.TIPO_OPERACAO   IN (1,10)  
              --AND B.PRODUTO = @PRODUTO

              
           UNION ALL  
               
             --PEGA UNIT�RIO DA ALTERA��O DE CUSTO--  
           SELECT 
                  A.PRODUTO,  
                  B.DATA_HORA AS MOVIMENTO,  
                  A.FORMULARIO_ORIGEM,  
                  A.TAB_MASTER_ORIGEM,  
                  A.ALTERACAO_CUSTO         AS REG_MASTER_ORIGEM,  
                  A.EMPRESA,
               --   A.ALTERACAO_CUSTO_EMPRESA AS CHAVE,  
                  0 AS DESCONTO,  
                  A.CUSTO_NOVO AS PRECO_COMPRA,  
                  0 AS DESPESAS,
                  0 AS FRETE,
                  0 AS SEGURO,
                  0 AS REPASSE,                                              
                  A.CUSTO_NOVO AS CUSTO, 
                  A.CUSTO_NOVO AS CUSTO_GERENCIAL, 
                  2 AS SEQUENCIA          
                      
             FROM ALTERACOES_CUSTOS_EMPRESAS  A WITH(NOLOCK)                                              
             JOIN ALTERACOES_CUSTOS           B WITH(NOLOCK) ON B.ALTERACAO_CUSTO = A.ALTERACAO_CUSTO
            WHERE 1 = 1  
              --AND A.PRODUTO = @PRODUTO

          
         
           UNION ALL  
                         
           --CUSTO INICIAL - TODOS--  
           SELECT 
                    A.PRODUTO,  
                  NULL AS MOVIMENTO,  
                  NULL AS FORMULARIO_ORIGEM,  
                  NULL AS TAB_MASTER_ORIGEM,  
                  NULL AS REG_MASTER_ORIGEM,  
                  NULL AS EMPRESA,
               --   A.PRODUTO AS CHAVE,  
                  0 AS DESCONTO,  
                  A.CUSTO AS PRECO_COMPRA,    
                  0 AS DESPESAS,
                  0 AS FRETE,
                  0 AS SEGURO,
                  0 AS REPASSE,                               
                  A.CUSTO   AS CUSTO, 
                  A.CUSTO   AS CUSTO_GERENCIAL, 
                  4 AS SEQUENCIA                            
                      
            FROM CUSTO_INICIAL A WITH(NOLOCK)                     
            WHERE 1 = 1   
              --AND A.PRODUTO = @PRODUTO

          
        )  X         
        
        ) Z ON Z.PRODUTO            = A.PRODUTO  

JOIN (  SELECT 
              A.PRODUTO,  
              NULL AS MOVIMENTO,  
              NULL AS FORMULARIO_ORIGEM,  
              NULL AS TAB_MASTER_ORIGEM,  
              NULL AS REG_MASTER_ORIGEM,  
              NULL AS EMPRESA, 
              0 AS DESCONTO,  
             A.CUSTO AS PRECO_COMPRA,  
              0 AS DESPESAS,
              0 AS FRETE,
              0 AS SEGURO,
              0 AS REPASSE,                               
              A.CUSTO   AS CUSTO, 
              A.CUSTO   AS CUSTO_GERENCIAL, 
              4 AS SEQUENCIA                            
                      
            FROM CUSTO_INICIAL A WITH(NOLOCK)                     
            WHERE 1 = 1   ) Z1 ON Z1.PRODUTO = A.PRODUTO
                                           

 LEFT JOIN FORMULARIOS          B WITH(NOLOCK) ON B.NUMID                = Z.FORMULARIO_ORIGEM
 LEFT JOIN NF_COMPRA            C WITH(NOLOCK) ON C.NF_COMPRA            = Z.REG_MASTER_ORIGEM
                                            AND C.TAB_MASTER_ORIGEM      = Z.TAB_MASTER_ORIGEM
                                            AND C.FORMULARIO_ORIGEM      = Z.FORMULARIO_ORIGEM
 LEFT JOIN (
SELECT A.NF_COMPRA, A.PRODUTO, SUM(A.QUANTIDADE_ESTOQUE) AS QUANTIDADE_ESTOQUE
  FROM NF_COMPRA_PRODUTOS A WITH(NOLOCK)
GROUP BY A.NF_COMPRA, A.PRODUTO
           )                                    D ON D.NF_COMPRA            = C.NF_COMPRA
                                                 AND D.PRODUTO              = A.PRODUTO
 LEFT JOIN ENTIDADES            E WITH(NOLOCK) ON E.ENTIDADE = C.ENTIDADE

 WHERE 1=1
   AND(Z.PRECO_COMPRA - Z1.PRECO_COMPRA > Z1.PRECO_COMPRA * 0.30 OR Z1.PRECO_COMPRA - Z.PRECO_COMPRA > Z1.PRECO_COMPRA * 0.30)
   AND(Z.EMPRESA = @EMPRESA OR @EMPRESA IS NULL)
   AND D.QUANTIDADE_ESTOQUE > 0
                  
    ORDER BY ORDEM