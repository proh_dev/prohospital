--CREATE PROCEDURE [dbo].[GERAR_SUGESTOES_COMPRAS]   ( @SUGESTAO_COMPRA      NUMERIC(15),              
--                                                    @DIAS_CURVA_A         NUMERIC(15) = 0,              
--                                                    @DIAS_CURVA_B         NUMERIC(15) = 0,              
--                                                    @DIAS_CURVA_C         NUMERIC(15) = 0,              
--                                                    @DIAS_CURVA_D         NUMERIC(15) = 0,              
--                                                    @DIAS_CURVA_E         NUMERIC(15) = 0,              
--                                                    @EFETIVIDADE          NUMERIC(15,2),              
--                                                    @TIPO_PRECO           NUMERIC(15),              
--                                                    @MOVIMENTO            DATETIME,              
--                                                    @GRUPO_COMPRA         NUMERIC(15),              
--                                                    @TIPO_RELATORIO       NUMERIC(15),              
--                                                    @ESTOQUE_ZERADO       VARCHAR(1),              
--                                                    @PEDIDOS_PENDENTES    VARCHAR(1),              
--                                                    @LEADTIME_INTERNO     NUMERIC(15),              
--                                                    @TIPO_CALCULO         NUMERIC(15),              
--                                                    @APENAS_CONTROLADOS   VARCHAR(1) = 'N',      
--                                                    @MENOS_CONTROLADOS    VARCHAR(1) = 'N',      
--                                                    @ZERAR_QUANTIDADE     VARCHAR(1) = 'N',            
--                                                    @PROCESSAR            VARCHAR(1) = 'N',            
--                                                    @PROCESSAR_PARCIAL    VARCHAR(1) = 'N',      
--                                                    @PROGRAMACAO_SUGESTAO NUMERIC(15) ,           
--                                                    @FORNECEDOR           NUMERIC(15) = NULL      
              
              
              
--) AS                 
            
              
--IF (@PROCESSAR = 'S' OR @PROCESSAR_PARCIAL = 'S')            
            
--BEGIN              
                
                
---------------------------------------------------------              
--ROTINA QUE GERA A SUGESTAO DE COMPRAS PARA O DEPOSITO--              
---------------------------------------------------------              
              
DECLARE @SUGESTAO_COMPRA   NUMERIC              
DECLARE @DIAS_CURVA_A      NUMERIC              
DECLARE @DIAS_CURVA_B      NUMERIC              
DECLARE @DIAS_CURVA_C      NUMERIC              
DECLARE @DIAS_CURVA_D      NUMERIC              
DECLARE @DIAS_CURVA_E      NUMERIC              
DECLARE @EFETIVIDADE       NUMERIC              
DECLARE @TIPO_PRECO        NUMERIC              
DECLARE @MOVIMENTO         DATETIME              
DECLARE @GRUPO_COMPRA      NUMERIC              
DECLARE @TIPO_RELATORIO    NUMERIC              
DECLARE @ESTOQUE_ZERADO    VARCHAR(1)              
DECLARE @PEDIDOS_PENDENTES VARCHAR(1)                      
DECLARE @LEADTIME_INTERNO  NUMERIC              
DECLARE @PROGRAMACAO_SUGESTAO NUMERIC              
DECLARE @TIPO_CALCULO          NUMERIC           
DECLARE @ZERAR_QUANTIDADE   VARCHAR(1)         
DECLARE @PROCESSAR_PARCIAL VARCHAR(1)      
DECLARE @APENAS_CONTROLADOS VARCHAR(1)      
DECLARE @MENOS_CONTROLADOS VARCHAR(1)      
DECLARE @FORNECEDOR           NUMERIC(15) = NULL      
              
DECLARE @DEPOSITO          NUMERIC              
DECLARE @EMPRESA_CONTABIL  NUMERIC              
DECLARE @FORMULARIO_ORIGEM NUMERIC              
DECLARE @TAB_MASTER_ORIGEM NUMERIC              
DECLARE @REG_MASTER_ORIGEM NUMERIC              
DECLARE @MES               NUMERIC(2)              
DECLARE @ANO               NUMERIC(4)              
DECLARE @DATA_INI          DATETIME              
DECLARE @DATA_FIM          DATETIME               
DECLARE @TIPO_COMPRA       VARCHAR(1)              
DECLARE @DIAS_CONSIDERAR_TRANSITO  NUMERIC(6,2)           
DECLARE @MENSAGEM VARCHAR(1000)         
DECLARE @CENTRO_ESTOQUE_CENTRAL NUMERIC         
        
      
      
----DEFINE FOR E TAB ORIGEM--      
--SELECT @FORMULARIO_ORIGEM =  A.NUMID FROM FORMULARIOS A WHERE A.FORMULARIO = 'SUGESTOES_COMPRAS'      
--SELECT @TAB_MASTER_ORIGEM =  A.NUMID FROM TABELAS     A WHERE A.TABELA     = 'SUGESTOES_COMPRAS'      
              
              
SET @SUGESTAO_COMPRA      = 2009          --:SUGESTAO_COMPRA              
SET @DIAS_CURVA_A         = 40           --:DIAS_CURVA_A              
SET @DIAS_CURVA_B         = 40           --:DIAS_CURVA_B              
SET @DIAS_CURVA_C         = 40           --:DIAS_CURVA_C              
SET @DIAS_CURVA_D         = 30           --:DIAS_CURVA_D              
SET @DIAS_CURVA_E         = 30           --:DIAS_CURVA_E              
SET @GRUPO_COMPRA         = 103           --:GRUPO_COMPRA              
SET @MOVIMENTO            = '03/05/2018' --:MOVIMENTO              
SET @TIPO_PRECO           = 4  --:TIPO_PRECO              
SET @TIPO_RELATORIO       = 1            --:TIPO_RELATORIO              
SET @EFETIVIDADE          = 100          --:EFETIVIDADE              
SET @PEDIDOS_PENDENTES    = 'S'          --:PEDIDOS_PENDENTES              
SET @ESTOQUE_ZERADO       = 'N'          --:ESTOQUE_ZERADO               
SET @LEADTIME_INTERNO     = 5           --:LEADTIME              
SET @TIPO_CALCULO         = 3               
SET @PROGRAMACAO_SUGESTAO = NULL        
SET @ZERAR_QUANTIDADE     = 'N'            
SET @PROCESSAR_PARCIAL    = 'N'      
SET @APENAS_CONTROLADOS   = 'N'      
SET @MENOS_CONTROLADOS    = 'N'      
      
      
      
DECLARE @PRODUTO           NUMERIC(15) = NULL--62056      
              
-------------------------------------------------------------              
--VERIFICA SE A SUGESTAO FOI GERADA, SENAO, GERA A MASTER--              
-------------------------------------------------------------              
              
IF @SUGESTAO_COMPRA IS NULL              
              
BEGIN              
            
            
INSERT INTO SUGESTOES_COMPRAS ( FORMULARIO_ORIGEM,              
                                TAB_MASTER_ORIGEM,              
                                REG_MASTER_ORIGEM,              
                                DATA_HORA,              
                                USUARIO_LOGADO,              
                                DIAS_CURVA_A,              
                                DIAS_CURVA_B,              
                                DIAS_CURVA_C,              
                                DIAS_CURVA_D,              
                                DIAS_CURVA_E,              
                                MOVIMENTO,              
                                DESCRICAO,              
                                GRUPO_COMPRA,              
                                TIPO_PRECO,              
                                TIPO_RELATORIO,              
                                EFETIVIDADE,              
                                LEADTIME,              
                                ESTOQUE_ZERADO,              
                                PEDIDOS_PENDENTES,              
                                TIPO_CALCULO,              
                                DATA_HORA_PROCESSAMENTO,              
                                PROGRAMACAO_SUGESTAO,            
                                ZERAR_QUANTIDADE_COMPRA ,             
                                PROCESSAR        ,            
                                PROCESSAR_PARCIAL,            
                                FINALIZAR )            
              
                       SELECT   @FORMULARIO_ORIGEM AS FORMULARIO_ORIGEM,              
                                @TAB_MASTER_ORIGEM AS TAB_MASTER_ORIGEM,              
                                NULL               AS REG_MASTER_ORIGEM,              
                                GETDATE()          AS DATA_HORA,              
                                3                  AS USUARIO_LOGADO,         
                                @DIAS_CURVA_A      AS DIAS_CURVA_A,              
                                @DIAS_CURVA_B      AS DIAS_CURVA_B,              
                                @DIAS_CURVA_C      AS DIAS_CURVA_C,            
                                @DIAS_CURVA_D      AS DIAS_CURVA_D,              
                                @DIAS_CURVA_E      AS DIAS_CURVA_E,              
                                @MOVIMENTO         AS MOVIMENTO,              
                                'SUGESTAO PROGRAMADA: ' + ISNULL(A.DESCRICAO,'' ) AS  DESCRICAO,              
                                @GRUPO_COMPRA      AS GRUPO_COMPRA,              
                                @TIPO_PRECO        AS TIPO_PRECO,              
                                @TIPO_RELATORIO    AS TIPO_RELATORIO,              
                                @EFETIVIDADE       AS EFETIVIDADE,              
                                @LEADTIME_INTERNO  AS LEADTIME,              
                                @ESTOQUE_ZERADO    AS ESTOQUE_ZERADO,              
                                @PEDIDOS_PENDENTES AS PEDIDOS_PENDENTES,              
                                @TIPO_CALCULO      AS TIPO_CALCULO,              
                                GETDATE()          AS DATA_HORA_PROCESSAMENTO,              
                                @PROGRAMACAO_SUGESTAO  AS PROGRAMACAO_SUGESTAO  ,            
                                @ZERAR_QUANTIDADE      AS ZERAR_QUANTIDADE,              
                                'N'                    AS PROCESSAR,            
                                'N'                    AS PROCESSAR_PARCIAL,            
                                'N'                    AS FINALIZAR                       
                                              
                          FROM GRUPOS_COMPRAS A WITH(NOLOCK)              
                         WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
                                       
              
SET @SUGESTAO_COMPRA = SCOPE_IDENTITY()      
              
--------------------------------------------------------------              
--INSERE OS PARAMETROS DE FILIAIS DA SUGESTAO--              
--------------------------------------------------------------              
              
DELETE FROM SUGESTOES_COMPRAS_EMPRESAS WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_EMPRESAS (               
              
            SUGESTAO_COMPRA ,               
            EMPRESA ,               
            FILIAL,               
            NOME_FANTASIA,              
            EMPRESA_COMPRA,               
            TIPO_COMPRA,               
            EMPRESA_DEMANDA,      
            CENTRO_ESTOQUE,      
            CENTRO_ESTOQUE_CENTRAL )              
                   
     --PEGA EMPRESAS DA PROGRAMACAO--                                      
     SELECT DISTINCT               
            @SUGESTAO_COMPRA  AS SUGESTAO_COMPRA ,               
            B.EMPRESA         AS EMPRESA,              
            C.FILIAL          AS FILIAL,               
            C.NOME_FANTASIA   AS NOME_FANTASIA,              
            CASE WHEN A.TIPO_COMPRA = 'C'               
                 THEN A.EMPRESA_COMPRA_CENTRAL              
                 ELSE B.EMPRESA              
            END               AS EMPRESA_COMPRA,                              
            A.TIPO_COMPRA     AS TIPO_COMPRA,              
            B.EMPRESA         AS EMPRESA_DEMANDA,      
            D.OBJETO_CONTROLE_COMPRAS AS CENTRO_ESTOQUE,                  
            ISNULL(E.OBJETO_CONTROLE_CENTRAL, D.OBJETO_CONTROLE_CENTRAL) AS CENTRO_ESTOQUE_CENTRAL        
                     
  FROM PROGRAMACOES_SUGESTOES             A WITH(NOLOCK)              
  JOIN PROGRAMACOES_SUGESTOES_EMPRESAS    B WITH(NOLOCK) ON B.PROGRAMACAO_SUGESTAO = A.PROGRAMACAO_SUGESTAO              
  JOIN EMPRESAS_USUARIAS                  C WITH(NOLOCK) ON C.EMPRESA_USUARIA      = B.EMPRESA              
  JOIN PARAMETROS_COMPRAS                 D WITH(NOLOCK) ON D.EMPRESA_USUARIA      = B.EMPRESA          
  LEFT JOIN PARAMETROS_COMPRAS            E WITH(NOLOCK) ON E.EMPRESA_USUARIA      = A.EMPRESA_COMPRA_CENTRAL      
            
 WHERE A.PROGRAMACAO_SUGESTAO  = @PROGRAMACAO_SUGESTAO              
   AND A.TODAS_EMPRESAS_ATIVAS = 'N'            
         
                 
UNION ALL              
              
     --PEGA EMPRESAS ATIVAS DO CADASTRO--              
     SELECT DISTINCT               
            @SUGESTAO_COMPRA  AS SUGESTAO_COMPRA ,               
            C.EMPRESA_USUARIA AS EMPRESA,              
            C.FILIAL          AS FILIAL,               
            C.NOME_FANTASIA   AS NOME_FANTASIA,                          
            CASE WHEN A.TIPO_COMPRA = 'C'               
                 THEN A.EMPRESA_COMPRA_CENTRAL              
                 ELSE C.EMPRESA_USUARIA              END               AS EMPRESA_COMPRA,               
                               
            A.TIPO_COMPRA     AS TIPO_COMPRA,              
            C.EMPRESA_USUARIA AS EMPRESA_DEMANDA   ,      
            D.OBJETO_CONTROLE_COMPRAS AS CENTRO_ESTOQUE,                  
            ISNULL(E.OBJETO_CONTROLE_CENTRAL, D.OBJETO_CONTROLE_CENTRAL) AS CENTRO_ESTOQUE_CENTRAL        
                     
  FROM PROGRAMACOES_SUGESTOES             A WITH(NOLOCK)              
  JOIN EMPRESAS_USUARIAS                  C WITH(NOLOCK) ON C.ATIVO         = 'S'              
                                                        AND C.COMPRA_DIRETA = 'S'           
      
  JOIN PARAMETROS_COMPRAS                 D WITH(NOLOCK) ON D.EMPRESA_USUARIA      = C.EMPRESA_USUARIA          
  LEFT JOIN PARAMETROS_COMPRAS            E WITH(NOLOCK) ON E.EMPRESA_USUARIA      = A.EMPRESA_COMPRA_CENTRAL      
          
                                                                 
 WHERE A.PROGRAMACAO_SUGESTAO  = @PROGRAMACAO_SUGESTAO              
   AND A.TODAS_EMPRESAS_ATIVAS = 'S'                 
   AND C.EMPRESA_COMPRA <> C.EMPRESA_USUARIA              
                 
 ORDER BY EMPRESA          
                
--------------------------------------------------------------              
--INSERE OS PARAMETROS DO GRUPO DE COMPRAS              
--------------------------------------------------------------              
              
--------------------------------------------              
--MARCAS              
--------------------------------------------              
              
DELETE SUGESTOES_COMPRAS_MARCAS                
 WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_MARCAS (              
                
            SUGESTAO_COMPRA,              
            MARCA,              
            TIPO )              
                          
SELECT      @SUGESTAO_COMPRA,              
            A.MARCA ,              
            A.TIPO AS TIPO                      
              
  FROM GRUPOS_COMPRAS_MARCAS A WITH(NOLOCK),              
       GRUPOS_COMPRAS        B WITH(NOLOCK)              
                     
 WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
   AND A.GRUPO_COMPRA = B.GRUPO_COMPRA              
                 
--------------------------------------------              
--SECOES              
--------------------------------------------              
              
DELETE SUGESTOES_COMPRAS_SECOES                
 WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_SECOES (              
                
            SUGESTAO_COMPRA,              
            SECAO_PRODUTO,              
            TIPO )              
                          
SELECT      @SUGESTAO_COMPRA,            
            A.SECAO_PRODUTO ,              
            A.TIPO AS TIPO              
              
              
  FROM GRUPOS_COMPRAS_SECOES A WITH(NOLOCK),              
        GRUPOS_COMPRAS       B WITH(NOLOCK)              
                     
 WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
   AND A.GRUPO_COMPRA = B.GRUPO_COMPRA              
              
--------------------------------------------              
--GRUPOS_PRODUTOS              
--------------------------------------------              
              
DELETE SUGESTOES_COMPRAS_GRUPOS              
 WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_GRUPOS (              
                
            SUGESTAO_COMPRA,              
            GRUPO_PRODUTO,              
            TIPO )              
                          
SELECT      @SUGESTAO_COMPRA,              
            A.GRUPO_PRODUTO ,              
            A.TIPO AS TIPO              
              
  FROM GRUPOS_COMPRAS_GRUPOS A WITH(NOLOCK),              
       GRUPOS_COMPRAS        B WITH(NOLOCK)              
                     
 WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
   AND A.GRUPO_COMPRA = B.GRUPO_COMPRA              
                 
             --------------------------------------------              
--SUBGRUPOS              
--------------------------------------------              
              
DELETE SUGESTOES_COMPRAS_SUBGRUPOS              
 WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_SUBGRUPOS (              
                
            SUGESTAO_COMPRA,              
            SUBGRUPO_PRODUTO,              
            TIPO )              
                          
SELECT      @SUGESTAO_COMPRA,              
            A.SUBGRUPO_PRODUTO ,              
            A.TIPO AS TIPO              
              
  FROM GRUPOS_COMPRAS_SUBGRUPOS A WITH(NOLOCK),              
       GRUPOS_COMPRAS           B WITH(NOLOCK)              
                     
 WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
   AND A.GRUPO_COMPRA = B.GRUPO_COMPRA              
      
--------------------------------------------              
--CLASSES DE PRODUTOS      
--------------------------------------------              
              
DELETE SUGESTOES_COMPRAS_CLASSES            
 WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_CLASSES (              
                
            SUGESTAO_COMPRA,              
            CLASSE_PRODUTO,              
            TIPO )              
                          
SELECT      @SUGESTAO_COMPRA,              
            A.CLASSE_PRODUTO ,              
            A.TIPO AS TIPO              
              
  FROM GRUPOS_COMPRAS_CLASSES   A WITH(NOLOCK),              
       GRUPOS_COMPRAS           B WITH(NOLOCK)              
                     
 WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
   AND A.GRUPO_COMPRA = B.GRUPO_COMPRA          
      
--------------------------------------------              
--PRODUTOS              
--------------------------------------------              
              
DELETE SUGESTOES_COMPRAS_PRODUTOS              
 WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA              
              
INSERT INTO SUGESTOES_COMPRAS_PRODUTOS (              
                
            SUGESTAO_COMPRA,              
            PRODUTO,              
            TIPO )              
                          
SELECT      @SUGESTAO_COMPRA,              
            A.PRODUTO ,              
            A.TIPO              
              
  FROM GRUPOS_COMPRAS_PRODUTOS  A WITH(NOLOCK),              
       GRUPOS_COMPRAS           B WITH(NOLOCK)              
                     
 WHERE A.GRUPO_COMPRA = @GRUPO_COMPRA              
   AND A.GRUPO_COMPRA = B.GRUPO_COMPRA              
                     
              
END --FIM GERACAO NUMERO SUGESTAO E PARAMETROS DO GRUPO COMPRAS --              
              
      
--*************************************************************************              
------------INICIO EFETIVO DOS C�LCULOS DA SUGESTAO DE COMPRAS-------------              
--*************************************************************************              
      
------------------------------------------------------      
--PEGA O PARAMETRO DE DIAS PARA ANALISE DE TRANSITO--      
------------------------------------------------------      
            
SELECT @DIAS_CONSIDERAR_TRANSITO = A.DIAS_CONSIDERAR_TRANSITO              
  FROM PARAMETROS A WITH(NOLOCK)              
 WHERE A.GRUPO_EMPRESARIAL = 1            
       
------------------------------------------------      
-- ATRIBUI��O DE VALORES NAS VARIAVEIS LOCAIS --      
-- CONFORME PAR�METROS DA SUGEST�O DE COMPRA  --      
------------------------------------------------      
      
SET @DATA_FIM = CAST( CONVERT(VARCHAR,GETDATE(), 103 ) AS DATETIME )            
SET @DATA_INI = DATEADD( DAY, -@DIAS_CONSIDERAR_TRANSITO, @DATA_FIM )            
            
-----------------------------------------------------------------      
-- ADI��O DO LEADTIME INTERNO NOS DIAS DE ESTOQUE POR CURVA    --      
-----------------------------------------------------------------      
      
SET @DIAS_CURVA_A = ISNULL(@DIAS_CURVA_A,0) + ISNULL(@LEADTIME_INTERNO,0)            
SET @DIAS_CURVA_B = ISNULL(@DIAS_CURVA_B,0) + ISNULL(@LEADTIME_INTERNO,0)            
SET @DIAS_CURVA_C = ISNULL(@DIAS_CURVA_C,0) + ISNULL(@LEADTIME_INTERNO,0)            
SET @DIAS_CURVA_D = ISNULL(@DIAS_CURVA_D,0) + ISNULL(@LEADTIME_INTERNO,0)            
SET @DIAS_CURVA_E = ISNULL(@DIAS_CURVA_E,0) + ISNULL(@LEADTIME_INTERNO,0)           
      
------------------------------------------------------------------      
-- SE N�O TIVER VALOR_BASE NO MES CORRENTE, PEGA O MES ANTERIOR --      
-- PARA EVITAR PROBLEMA NA VIRADA DO MES                        --      
------------------------------------------------------------------      
      
IF NOT EXISTS ( SELECT COUNT(*)       
                  FROM VALOR_BASE WITH(NOLOCK)       
                 WHERE MES = @MES       
                   AND ANO = @ANO  )      
BEGIN      
      
      SELECT TOP 1 @MES = A.MES,      
                   @ANO = A.ANO      
        FROM VALOR_BASE A WITH(NOLOCK)      
       ORDER BY A.ANO DESC, A.MES DESC      
      
END       
      
      
IF @MOVIMENTO IS NULL SET @MOVIMENTO = CONVERT( DATE, GETDATE() )      
      
SET @MES = MONTH(@MOVIMENTO)      
SET @ANO = YEAR (@MOVIMENTO)      
      
      
-------------------------------------------------------      
-- ATRIBUI��O DO N�MERO DO CD E EMPRESA CONT�BIL     --      
-- CONFORME PAR�METROS DA SUGEST�O DE COMPRA         --      
-------------------------------------------------------      
      
SELECT TOP 1 @DEPOSITO               = A.EMPRESA_COMPRA ,       
             @EMPRESA_CONTABIL       = B.EMPRESA_CONTABIL,      
             @CENTRO_ESTOQUE_CENTRAL = A.CENTRO_ESTOQUE_CENTRAL,      
             @TIPO_COMPRA            = A.TIPO_COMPRA      
                                
  FROM SUGESTOES_COMPRAS_EMPRESAS A WITH(NOLOCK) ,      
       EMPRESAS_USUARIAS          B WITH(NOLOCK)       
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.EMPRESA         = B.EMPRESA_USUARIA      
      
      
-------------------------------------------------------      
-- SE N�O FOI POSS�VEL DEFINIR CD E EMPRESA CONT�BIL --      
-- PELOS PAR�METROS, ENT�O ATRIBUI OS DADOS DA 1o.   --      
-- EMPRESA USU�RIA CADASTRADA NO SISTEMA             --      
-------------------------------------------------------      
      
IF @DEPOSITO          IS NULL SET @DEPOSITO         = ( SELECT TOP 1 EMPRESA_USUARIA FROM EMPRESAS_USUARIAS )      
IF @EMPRESA_CONTABIL  IS NULL SET @EMPRESA_CONTABIL = ( SELECT TOP 1 EMPRESA_USUARIA FROM EMPRESAS_USUARIAS )      
IF @TIPO_COMPRA       IS NULL SET @TIPO_COMPRA      = 'L'      
      
     -----------------------------------------      
     -- VALIDACAO DOS PARAMENTROS VIA RAISE --      
     -----------------------------------------      
      
     SELECT TOP 1 @MENSAGEM = X.MENSAGEM      
      
       FROM (      
      
              SELECT TOP 1'Para Compras do Tipo Central Deve ser Utilizado o Tipo C�lculo = 2 ou 3' AS MENSAGEM      
                FROM SUGESTOES_COMPRAS A WITH(NOLOCK)       
                JOIN ( SELECT TOP 1 A.SUGESTAO_COMPRA      
                         FROM SUGESTOES_COMPRAS_EMPRESAS A WITH(NOLOCK)       
                        WHERE A.SUGESTAO_COMPRA =@SUGESTAO_COMPRA      
                          AND @TIPO_COMPRA     = 'C'      
                     ) X ON X.SUGESTAO_COMPRA = A.SUGESTAO_COMPRA         
               WHERE @TIPO_CALCULO NOT IN (2, 3)      
        
               UNION ALL      
      
              SELECT TOP 1'Para Compras do Tipo Loja Deve ser Utilizado o Tipo C�lculo 1 ou 2' AS MENSAGEM      
                FROM SUGESTOES_COMPRAS A WITH(NOLOCK)       
                JOIN ( SELECT TOP 1 A.SUGESTAO_COMPRA      
                         FROM SUGESTOES_COMPRAS_EMPRESAS A WITH(NOLOCK)       
                        WHERE A.SUGESTAO_COMPRA =@SUGESTAO_COMPRA      
                          AND @TIPO_COMPRA     = 'L'      
                      ) X ON X.SUGESTAO_COMPRA = A.SUGESTAO_COMPRA         
                WHERE @TIPO_CALCULO > 3      
        
               UNION ALL      
      
              SELECT TOP 1'Tipo Pre�o Exige que Informe o Fornecedor' AS MENSAGEM      
                FROM SUGESTOES_COMPRAS A WITH(NOLOCK)       
               WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
        AND A.TIPO_PRECO = 3      
     AND A.FORNECEDOR IS NULL      
            
            ) X      
        
      WHERE X.MENSAGEM IS NOT NULL      
            
          
     -----------------------------      
     -- CHECAGENS DE PARAMETROS --      
     -----------------------------      
      
     IF @MENSAGEM IS NOT NULL      
     BEGIN      
          RAISERROR ( @MENSAGEM ,15,-1)      
          RETURN      
     END            
            
            
------------------------------------      
-- ELIMINA AS TABELAS TEMPOR�RIAS --      
------------------------------------      
      
if object_id('tempdb..#PEDIDOS_PENDENTES') is not null                 DROP TABLE #PEDIDOS_PENDENTES      
if object_id('tempdb..#PRODUTOS') is not null                          DROP TABLE #PRODUTOS      
if object_id('tempdb..#PEDIDOS_PENDENTES_DEPOSITO') is not null        DROP TABLE #PEDIDOS_PENDENTES_DEPOSITO      
if object_id('tempdb..#NECESSIDADE_LOJA') is not null                  DROP TABLE #NECESSIDADE_LOJA      
if object_id('tempdb..#DEMANDA_DIA_PONDERADA') is not null             DROP TABLE #DEMANDA_DIA_PONDERADA      
if object_id('tempdb..#ESTOQUE_REDE') is not null                      DROP TABLE #ESTOQUE_REDE      
if object_id('tempdb..#ESTOQUE_LOJA') is not null                      DROP TABLE #ESTOQUE_LOJA      
if object_id('tempdb..#ESTOQUE_DEPOSITO') is not null                  DROP TABLE #ESTOQUE_DEPOSITO      
if object_id('tempdb..#CURVA_PRODUTO_DEPOSITO') is not null            DROP TABLE #CURVA_PRODUTO_DEPOSITO      
if object_id('tempdb..#ENCOMENDAS') is not null                        DROP TABLE #ENCOMENDAS      
if object_id('tempdb..#ESTOQUE_MOSTRUARIO') is not null                DROP TABLE #ESTOQUE_MOSTRUARIO      
if object_id('tempdb..#CENTROS_ESTOQUE') is not null                   DROP TABLE #CENTROS_ESTOQUE      
if object_id('tempdb..#PRODUTOS_FILTROS') is not null                  DROP TABLE #PRODUTOS_FILTROS      
if object_id('tempdb..#ULTIMA_COMPRA') is not null                     DROP TABLE #ULTIMA_COMPRA      
if object_id('tempdb..#COMPRA_SUGERIDA') is not null                   DROP TABLE #COMPRA_SUGERIDA      
if object_id('tempdb..#FORNECEDORES_PRECOS_DESCONTOS') is not null     DROP TABLE #FORNECEDORES_PRECOS_DESCONTOS      
      
      
-----------------------------------------------------      
--FAZ COPIA DOS DADOS PARA GARANTIR AS ALTERA��ES DO      
--USUARIO      
-----------------------------------------------------      
      
     if object_id('tempdb..#SUGESTAO_COMPRA_RESULTADO_PARCIAL') is not null DROP TABLE #SUGESTAO_COMPRA_RESULTADO_PARCIAL      
      
     ------------------------------------------      
     -- PEGA ITENS COM ALTERA�AO NA SUGESTAO --      
     ------------------------------------------      
     SELECT A.SUGESTAO_COMPRA_RESULTADO,      
            A.EMPRESA,      
            A.PRODUTO      
      
       INTO #SUGESTAO_COMPRA_RESULTADO_PARCIAL      
      
       FROM SUGESTOES_COMPRAS_RESULTADO A WITH(NOLOCK)      
      WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
        AND A.PRESERVAR_REGISTRO = 'S'      
        AND @PROCESSAR_PARCIAL   = 'S'      
      
            
-----------------------------------------------------      
-- GERA TABELA TEMPOR�RIA DOS CENTROS DE ESTOQUE   --      
-- DAS EMPRESAS SELECIONADAS NA SUGEST�O DE COMPRA --      
-----------------------------------------------------      
      
SELECT DISTINCT      
       A.EMPRESA_USUARIA AS EMPRESA,      
       A.EMPRESA_COMPRA  AS EMPRESA_COMPRA,      
       B.OBJETO_CONTROLE_COMPRAS AS CENTRO_ESTOQUE      
      
  INTO #CENTROS_ESTOQUE      
    
  FROM EMPRESAS_USUARIAS  A WITH(NOLOCK)      
  JOIN PARAMETROS_COMPRAS B WITH(NOLOCK) ON B.EMPRESA_USUARIA = A.EMPRESA_USUARIA      
      
 WHERE A.COMPRA_DIRETA = 'S'      
   AND A.ATIVO         = 'S'      
   AND A.EMPRESA_COMPRA = @DEPOSITO      
      
---------------------------------------------------------      
-- GERA TABELA TEMPOR�RIA DE PRODUTOS CONFORME FILTROS --      
---------------------------------------------------------      
    
    
      
   SELECT DISTINCT      
          A.MARCA,       
          A.SECAO_PRODUTO,       
          A.GRUPO_PRODUTO,       
          A.SUBGRUPO_PRODUTO,      
          A.CLASSE_PRODUTO               
    
     INTO #PRODUTOS_FILTROS      
    
     FROM PRODUTOS                    A WITH(NOLOCK)      
LEFT JOIN SUGESTOES_COMPRAS_MARCAS    B WITH(NOLOCK) ON B.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                                                    AND B.TIPO <> '-'      
LEFT JOIN SUGESTOES_COMPRAS_GRUPOS    C WITH(NOLOCK) ON C.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                                                    AND C.TIPO <> '-'      
LEFT JOIN SUGESTOES_COMPRAS_SUBGRUPOS D WITH(NOLOCK) ON D.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                                                    AND D.TIPO <> '-'        
LEFT JOIN SUGESTOES_COMPRAS_SECOES    E WITH(NOLOCK) ON E.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                 AND E.TIPO <> '-'       
LEFT JOIN SUGESTOES_COMPRAS_CLASSES   F WITH(NOLOCK) ON F.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                                                    AND F.TIPO <> '-'                                                          
    WHERE ( ( ( B.MARCA   IS NOT NULL AND B.MARCA            = A.MARCA            )  OR B.MARCA             IS NULL )AND       
            ( ( C.GRUPO_PRODUTO    IS NOT NULL AND C.GRUPO_PRODUTO    = A.GRUPO_PRODUTO    )  OR C.GRUPO_PRODUTO     IS NULL )AND       
            ( ( D.SUBGRUPO_PRODUTO IS NOT NULL AND D.SUBGRUPO_PRODUTO = A.SUBGRUPO_PRODUTO )  OR D.SUBGRUPO_PRODUTO  IS NULL )AND             
            ( ( E.SECAO_PRODUTO    IS NOT NULL AND E.SECAO_PRODUTO    = A.SECAO_PRODUTO    )  OR E.SECAO_PRODUTO     IS NULL )AND      
            ( ( F.CLASSE_PRODUTO   IS NOT NULL AND F.CLASSE_PRODUTO   = A.CLASSE_PRODUTO   )  OR F.CLASSE_PRODUTO    IS NULL ) ) AND      
              ( B.MARCA            IS NOT NULL OR       
                C.GRUPO_PRODUTO    IS NOT NULL OR       
                D.SUBGRUPO_PRODUTO IS NOT NULL OR      
                E.SECAO_PRODUTO    IS NOT NULL OR      
                F.CLASSE_PRODUTO   IS NOT NULL   )      
      
--AND ( A.PRODUTO = @PRODUTO OR @PRODUTO IS NULL )      
      
-------------------------------------------------------------------------------------------------      
-- DELETA OS PRODUTOS DAS MARCAS COM SINAL DE MENOS DA TABELA TEMPOR�RIA DE FILTRO DE PRODUTOS --      
-------------------------------------------------------------------------------------------------      
DELETE #PRODUTOS_FILTROS      
  FROM SUGESTOES_COMPRAS_MARCAS A WITH(NOLOCK)      
  JOIN #PRODUTOS_FILTROS        B ON B.MARCA = A.MARCA      
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.TIPO = '-'        
         
-------------------------------------------------------------------------------------------------      
-- DELETA OS PRODUTOS DAS SE��ES COM SINAL DE MENOS DA TABELA TEMPOR�RIA DE FILTRO DE PRODUTOS --      
-------------------------------------------------------------------------------------------------      
      
DELETE #PRODUTOS_FILTROS      
  FROM SUGESTOES_COMPRAS_SECOES A WITH(NOLOCK)      
  JOIN #PRODUTOS_FILTROS        B ON B.SECAO_PRODUTO = A.SECAO_PRODUTO      
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.TIPO = '-'        
         
-------------------------------------------------------------------------------------------------      
-- DELETA OS PRODUTOS DOS GRUPOS COM SINAL DE MENOS DA TABELA TEMPOR�RIA DE FILTRO DE PRODUTOS --      
-------------------------------------------------------------------------------------------------      
      
DELETE #PRODUTOS_FILTROS      
  FROM SUGESTOES_COMPRAS_GRUPOS A WITH(NOLOCK)      
 JOIN #PRODUTOS_FILTROS        B ON B.GRUPO_PRODUTO = A.GRUPO_PRODUTO      
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.TIPO = '-'        
         
----------------------------------------------------------------------------------------------------      
-- DELETA OS PRODUTOS DOS SUBGRUPOS COM SINAL DE MENOS DA TABELA TEMPOR�RIA DE FILTRO DE PRODUTOS --      
----------------------------------------------------------------------------------------------------      
DELETE #PRODUTOS_FILTROS      
  FROM SUGESTOES_COMPRAS_SUBGRUPOS A WITH(NOLOCK)      
  JOIN #PRODUTOS_FILTROS           B ON B.SUBGRUPO_PRODUTO = A.SUBGRUPO_PRODUTO      
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.TIPO = '-'            
      
----------------------------------------------------------------------------------------------------      
-- DELETA OS PRODUTOS DAS CLASSES COM SINAL DE MENOS DA TABELA TEMPOR�RIA DE FILTRO DE PRODUTOS --      
----------------------------------------------------------------------------------------------------      
DELETE #PRODUTOS_FILTROS      
  FROM SUGESTOES_COMPRAS_CLASSES A WITH(NOLOCK)      
  JOIN #PRODUTOS_FILTROS           B ON B.CLASSE_PRODUTO = A.CLASSE_PRODUTO      
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.TIPO = '-'            
      
-------------------------------------------------      
-- GERA TABELA ANAL�TICA DE PRODUTOS FILTRADOS --      
-------------------------------------------------      
               
SELECT DISTINCT       
       B.PRODUTO,      
       B.MARCA,      
 B.PRODUTO AS PRODUTO_DEMANDA      
  INTO #PRODUTOS      
  FROM #PRODUTOS_FILTROS  A WITH(NOLOCK)                                           
  JOIN PRODUTOS           B WITH(NOLOCK) ON B.MARCA            = A.MARCA      
                                        AND B.SECAO_PRODUTO    = A.SECAO_PRODUTO      
                                        AND B.GRUPO_PRODUTO    = A.GRUPO_PRODUTO      
                                        AND B.SUBGRUPO_PRODUTO = A.SUBGRUPO_PRODUTO       
                                        AND B.CLASSE_PRODUTO   = A.CLASSE_PRODUTO       
WHERE 1=1      
--AND ( B.PRODUTO = @PRODUTO OR @PRODUTO IS NULL )      
      
-----------------------------------------------------------------------------------------------      
-- INSERE NA TABELA ANAL�TICA DE PRODUTOS FILTRADOS CONFORME MARCA, SE��O, GRUPO E SUB-GRUPO --      
-- OS PRODUTOS DEFINIDOS PELO USU�RIO NO �LTIMO N�VEL DE FILTRO                              --      
-----------------------------------------------------------------------------------------------      
      
INSERT INTO #PRODUTOS (       
            PRODUTO,       
            MARCA,       
PRODUTO_DEMANDA )      
     SELECT DISTINCT       
            A.PRODUTO  AS PRODUTO ,       
            C.MARCA    AS MARCA ,       
            A.PRODUTO  AS PRODUTO_DEMANDA      
       FROM SUGESTOES_COMPRAS_PRODUTOS A WITH(NOLOCK)      
  LEFT JOIN #PRODUTOS                  B ON B.PRODUTO = A.PRODUTO      
       JOIN  PRODUTOS                  C ON C.PRODUTO = A.PRODUTO      
      WHERE B.PRODUTO IS NULL                                            
        AND A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
        AND A.TIPO <> '-'      
      
--------------------------------------------------------------------------------------      
-- DELETA OS PRODUTOS COM SINAL DE MENOS DA TABELA TEMPOR�RIA DE FILTRO DE PRODUTOS --      
--------------------------------------------------------------------------------------      
      
DELETE #PRODUTOS      
  FROM SUGESTOES_COMPRAS_PRODUTOS A WITH(NOLOCK)      
  JOIN #PRODUTOS                  B ON B.PRODUTO = A.PRODUTO                                      
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
   AND A.TIPO = '-'                           
      
      
-----------------------------------------------------      
-- GERA TABELA DE ENCOMENDAS DE PRODUTOS DAS LOJAS --      
-----------------------------------------------------      
      
  SELECT A.PRODUTO         AS PRODUTO ,      
         A.EMPRESA         AS EMPRESA ,      
         SUM(A.QUANTIDADE) AS ENCOMENDA       
    INTO #ENCOMENDAS      
 FROM SUGESTOES_COMPRAS_ENCOMENDAS A WITH(NOLOCK)      
   WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
GROUP BY A.PRODUTO, A.EMPRESA      
      
--INSERE PRODUTOS DA ENCOMENDA--      
      
INSERT INTO #PRODUTOS (       
            PRODUTO,       
            MARCA,       
            PRODUTO_DEMANDA )      
                  
     SELECT DISTINCT       
            A.PRODUTO  AS PRODUTO ,       
            C.MARCA    AS MARCA ,       
            A.PRODUTO  AS PRODUTO_DEMANDA      
       FROM #ENCOMENDAS                A WITH(NOLOCK)      
  LEFT JOIN #PRODUTOS                  B ON B.PRODUTO = A.PRODUTO      
       JOIN  PRODUTOS                  C ON C.PRODUTO = A.PRODUTO      
      WHERE B.PRODUTO IS NULL                                            
      
      
----------------------------------------------------------------      
--ELIMINA OS PRODUTOS CONFORME FLAG DE APENAS VENDA CONTROLADA--      
----------------------------------------------------------------      
DELETE #PRODUTOS      
  FROM      #PRODUTOS         A WITH(NOLOCK)       
  LEFT JOIN PRODUTOS_FARMACIA B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO      
       
 WHERE @APENAS_CONTROLADOS = 'S'      
   AND ISNULL( B.VENDA_CONTROLADA, 'N' )  <> 'S'      
      
---------------------------------------------------------------      
--ELIMINA OS PRODUTOS CONFORME FLAG DE MENOS VENDA CONTROLADA--      
---------------------------------------------------------------      
DELETE #PRODUTOS      
  FROM      #PRODUTOS         A WITH(NOLOCK)       
  LEFT JOIN PRODUTOS_FARMACIA B WITH(NOLOCK)  ON B.PRODUTO = A.PRODUTO      
       
 WHERE @MENOS_CONTROLADOS = 'S'      
   AND ISNULL( B.VENDA_CONTROLADA, 'N' ) = 'S'      
      
---------------------------------------------------------------      
--ELIMINA OS PRODUTOS CONFORME FLAG DE MENOS VENDA CONTROLADA--      
---------------------------------------------------------------      
DELETE #PRODUTOS      
  FROM      #PRODUTOS         A WITH(NOLOCK)       
  LEFT JOIN PRODUTOS_FARMACIA B WITH(NOLOCK)  ON B.PRODUTO = A.PRODUTO      
       
 WHERE @MENOS_CONTROLADOS = 'S'      
   AND ISNULL( B.VENDA_CONTROLADA, 'N' ) = 'S'      
      
------------------------------------------------------------------      
--GERA TABELA DE PRECOS E DESCONTOS POR FORNECEDOR--      
------------------------------------------------------------------      
       
 if @TIPO_PRECO = 4 SET @FORNECEDOR = 0      
      
      
 SELECT A.PRODUTO,      
        MAX( CASE WHEN X.TIPO_PRECO = 2 --TABELADO      
                  THEN B.PRECO_FABRICA      
       ELSE E.VALOR_UNITARIO      
             END ) AS PRECO_COMPRA,      
      
        MAX(E.PERCENTUAL_DESCONTO) AS DESCONTO_COMPRA      
      
   INTO #FORNECEDORES_PRECOS_DESCONTOS      
      
   FROM #PRODUTOS                      A WITH(NOLOCK)      
     JOIN PRODUTOS                     X WITH(NOLOCK)ON X.PRODUTO         = A.PRODUTO      
LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS B WITH(NOLOCK)ON B.PRODUTO         = A.PRODUTO      
     JOIN SUGESTOES_COMPRAS_EMPRESAS   C WITH(NOLOCK)ON C.EMPRESA         = B.EMPRESA      
                                                    AND C.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
      
     JOIN VW_PRODUTOS_PRECOS_DESCONTOS_COMPRAS        E WITH(NOLOCK)ON E.PRODUTO  = A.PRODUTO      
                                                                   AND E.ENTIDADE = @FORNECEDOR      
      
    WHERE @TIPO_PRECO in (3,4) --MIX FORNECEDOR--      
      
  GROUP BY A.PRODUTO      
      
------------------------------------------------------------------      
-- DEFINICAO DA CURVA DO DEPOSITO POR PRODUTO E DIAS DE ESTOQUE --      
-- QUANDO TIPO DE SUGEST�O = 'C' OU SEJA COMPRA CENTRALIZADA    --      
------------------------------------------------------------------      
      
SELECT DISTINCT      
       TMP.PRODUTO,      
       TMP.CURVA,      
       TMP.DIAS_ESTOQUE      
  INTO #CURVA_PRODUTO_DEPOSITO      
  FROM ( SELECT DISTINCT       
                A.PRODUTO        AS PRODUTO,      
                A.CURVA_DEPOSITO AS CURVA,      
                CASE WHEN ISNULL(A.CURVA_DEPOSITO, 'C') = 'A' THEN @DIAS_CURVA_A      
                     WHEN ISNULL(A.CURVA_DEPOSITO, 'C') = 'B' THEN @DIAS_CURVA_B      
                     ELSE                                          @DIAS_CURVA_C       
                     END         AS DIAS_ESTOQUE       
           FROM CURVA_DEMANDA A WITH(NOLOCK), #PRODUTOS B WITH(NOLOCK)      
          WHERE A.MES = @MES      
            AND A.ANO = @ANO      
            AND A.PRODUTO        = B.PRODUTO      
            AND A.EMPRESA_COMPRA = @DEPOSITO      
            AND @TIPO_COMPRA    <> 'L' ) TMP      
       ORDER BY PRODUTO      
      
-------------------------------------------------------------      
-- GERA TABELA DE ESTOQUE DA REDE PARA COMPRA CENTRALIZADA --      
--                                                         --      
-- SALDO DE ESTOQUE DA REDE � O TOTAL DE ESTOQUE DE TODAS  --      
-- AS LOJAS QUE SER�O ATENDIDAS PELO CENTRO DE DISTRIB.    --      
-------------------------------------------------------------      
      
    SELECT C.PRODUTO,      
           SUM( CASE WHEN C.ESTOQUE_SALDO < 0      
                     THEN 0      
                     ELSE C.ESTOQUE_SALDO      
                     END) AS ESTOQUE_REDE             
            
      INTO #ESTOQUE_REDE      
            
      FROM #CENTROS_ESTOQUE  A WITH(NOLOCK)      
INNER JOIN ESTOQUE_ATUAL     C WITH(NOLOCK) ON C.CENTRO_ESTOQUE = A.CENTRO_ESTOQUE      
INNER JOIN PRODUTOS          D WITH(NOLOCK) ON D.PRODUTO        = C.PRODUTO      
INNER JOIN #PRODUTOS         E WITH(NOLOCK) ON E.PRODUTO        = C.PRODUTO      
      
  GROUP BY C.PRODUTO      
      
CREATE INDEX ESTOQUE_REDE ON #ESTOQUE_REDE (PRODUTO)      
      
      
-----------------------------------------------------------------------      
-- GERA TEMPOR�RIA COM O SALDO DE ESTOQUE, MINIMO E STATUS DE COMPRA --      
-- DOS PRODUTOS POR LOJA                                             --      
-----------------------------------------------------------------------      
      
    SELECT B.EMPRESA                                         AS EMPRESA ,      
           A.PRODUTO                                         AS PRODUTO ,      
      
           MAX( CASE WHEN ( ISNULL(I.ESTOQUE_MINIMO,0) < A.FATOR_EMBALAGEM ) AND ( ISNULL(I.ESTOQUE_MINIMO,0) > 0 )      
                     THEN A.FATOR_EMBALAGEM      
                     ELSE ISNULL(I.ESTOQUE_MINIMO,0)       
                 END )                                       AS ESTOQUE_MINIMO,     
                           
           MAX(ISNULL( I.ESTOQUE_MAXIMO, 0) )                AS ESTOQUE_MAXIMO,                           
                 
           MAX(ISNULL(I.ESTOQUE_SEGURANCA,0))                AS ESTOQUE_SEGURANCA,      
                 
           MAX (I.VALIDADE_MINIMO )                          AS VALIDADE_MINIMO,      
      
           SUM(CASE WHEN ISNULL(E.ESTOQUE_SALDO,0) < 0       
                    THEN 0       
                    ELSE ISNULL(E.ESTOQUE_SALDO ,0) END )    AS ESTOQUE_SALDO,      
      
           SUM(ISNULL(E.ESTOQUE_SALDO ,0))                   AS ESTOQUE_SALDO_REAL,      
                          
           ISNULL (I.SITUACAO_PRODUTO_COMPRA, 1)             AS SITUACAO_PRODUTO_COMPRA      
                 
      INTO #ESTOQUE_LOJA        
            
      FROM PRODUTOS                   A WITH(NOLOCK)      
INNER JOIN #PRODUTOS                  H WITH(NOLOCK) ON H.PRODUTO         = A.PRODUTO      
INNER JOIN SUGESTOES_COMPRAS_EMPRESAS B WITH(NOLOCK) ON B.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
      
LEFT JOIN ESTOQUE_ATUAL               E WITH(NOLOCK) ON E.CENTRO_ESTOQUE  = B.CENTRO_ESTOQUE      
                                                    AND E.PRODUTO         = A.PRODUTO      
      
LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS I WITH(NOLOCK) ON I.PRODUTO        = A.PRODUTO      
                                                     AND I.EMPRESA        = B.EMPRESA_DEMANDA                                                     
      
      
  GROUP BY B.EMPRESA , A.PRODUTO, ISNULL(I.SITUACAO_PRODUTO_COMPRA, 1)      
      
      
UPDATE #ESTOQUE_LOJA SET ESTOQUE_MINIMO = 0 WHERE VALIDADE_MINIMO < CONVERT(DATE, GETDATE() )      
      
      
CREATE INDEX ESTOQUE_EMPRESA_01 ON #ESTOQUE_LOJA (PRODUTO)      
CREATE INDEX ESTOQUE_EMPRESA_02 ON #ESTOQUE_LOJA (EMPRESA)      
CREATE INDEX ESTOQUE_EMPRESA_03 ON #ESTOQUE_LOJA (PRODUTO, EMPRESA)      
      
-----------------------------------------------------------------------      
-- GERA TEMPOR�RIA COM O SALDO DE ESTOQUE, MINIMO E STATUS DE COMPRA --      
-- DOS PRODUTOS NO CENTRO DE DISTRIBUI��O                            --      
-----------------------------------------------------------------------      
      
     SELECT @DEPOSITO                                       AS EMPRESA ,      
            A.PRODUTO                                       AS PRODUTO ,      
            MAX(CASE WHEN ISNULL(G.ESTOQUE_MINIMO,0) <> 0 AND ISNULL(G.ESTOQUE_MINIMO,0) < A.FATOR_EMBALAGEM       
                     THEN A.FATOR_EMBALAGEM      
                     ELSE ISNULL(G.ESTOQUE_MINIMO,0) END )  AS ESTOQUE_MINIMO,      
      
            MAX( G.VALIDADE_MINIMO )                        AS VALIDADE_MINIMO,      
      
            SUM(CASE WHEN ISNULL(C.ESTOQUE_SALDO,0) < 0       
                     THEN 0      
                     ELSE ISNULL(C.ESTOQUE_SALDO,0) END )   AS ESTOQUE_SALDO      
                  
       INTO #ESTOQUE_DEPOSITO      
      
       FROM PRODUTOS                   A WITH(NOLOCK)      
 INNER JOIN #PRODUTOS                  B WITH(NOLOCK) ON B.PRODUTO         = A.PRODUTO       
  LEFT JOIN (      
               SELECT PRODUTO, SUM(ESTOQUE_SALDO) ESTOQUE_SALDO FROM ESTOQUE_ATUAL WITH(NOLOCK) GROUP BY PRODUTO      
            )                          C              ON C.PRODUTO         = A.PRODUTO                                                              
  LEFT JOIN PRODUTOS_LOCAIS            G WITH(NOLOCK) ON G.PRODUTO         = A.PRODUTO      
                                                     AND G.OBJETO_CONTROLE = @CENTRO_ESTOQUE_CENTRAL      
      
   GROUP BY A.PRODUTO       
   ORDER BY PRODUTO      
      
CREATE INDEX ESTOQUE_EMPRESA_01 ON #ESTOQUE_DEPOSITO (PRODUTO)      
CREATE INDEX ESTOQUE_EMPRESA_02 ON #ESTOQUE_DEPOSITO (EMPRESA)  CREATE INDEX ESTOQUE_EMPRESA_03 ON #ESTOQUE_DEPOSITO (PRODUTO, EMPRESA)      
      
UPDATE #ESTOQUE_DEPOSITO SET ESTOQUE_SALDO = 0 WHERE ESTOQUE_SALDO < 0      
      
             
-----------------------------------------------------      
-- GERA TEMPOR�RIA DOS PEDIDOS PENDENTES DAS LOJAS --      
-----------------------------------------------------      
      
  SELECT D.PRODUTO                            AS PRODUTO ,        
         A.EMPRESA                            AS EMPRESA ,       
         SUM  ( D.SALDO )                     AS PEDIDOS      
               
    INTO #PEDIDOS_PENDENTES      
      
    FROM SUGESTOES_COMPRAS_EMPRESAS     A WITH(NOLOCK)      
    JOIN PEDIDOS_COMPRAS                B WITH(NOLOCK) ON B.EMPRESA       = A.EMPRESA      
    JOIN PEDIDOS_COMPRAS_PRODUTOS       C WITH(NOLOCK) ON C.PEDIDO_COMPRA = B.PEDIDO_COMPRA      
    JOIN #PRODUTOS                      X WITH(NOLOCK) ON X.PRODUTO       = C.PRODUTO      
    JOIN PEDIDOS_COMPRAS_PRODUTOS_SALDO D WITH(NOLOCK) ON D.PEDIDO_COMPRA_PRODUTO = C.PEDIDO_COMPRA_PRODUTO      
                                                      AND D.PRODUTO               = C.PRODUTO      
      
   WHERE D.SALDO            > 0      
     AND @PEDIDOS_PENDENTES = 'S'      
     AND A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
           
GROUP BY D.PRODUTO , A.EMPRESA      
      
CREATE INDEX PEDIDOS_PENDENTES_01 ON #PEDIDOS_PENDENTES (PRODUTO)      
CREATE INDEX PEDIDOS_PENDENTES_03 ON #PEDIDOS_PENDENTES (PRODUTO, EMPRESA)      
      
UPDATE #PEDIDOS_PENDENTES SET PEDIDOS = 0 WHERE PEDIDOS < 0      
      
      
---------------------------------------------------------------------      
-- GERA TEMPOR�RIA DOS PEDIDOS PENDENTES DO CENTRO DE DISTRIBUI��O --      
---------------------------------------------------------------------      
  SELECT D.PRODUTO                            AS PRODUTO ,        
         SUM  ( D.SALDO )                     AS PEDIDOS      
               
    INTO #PEDIDOS_PENDENTES_DEPOSITO      
      
    FROM PEDIDOS_COMPRAS                A WITH(NOLOCK)      
    JOIN PEDIDOS_COMPRAS_PRODUTOS       C WITH(NOLOCK) ON C.PEDIDO_COMPRA = A.PEDIDO_COMPRA      
    JOIN #PRODUTOS     X WITH(NOLOCK) ON X.PRODUTO       = C.PRODUTO      
    JOIN PEDIDOS_COMPRAS_PRODUTOS_SALDO D WITH(NOLOCK) ON D.PEDIDO_COMPRA_PRODUTO = C.PEDIDO_COMPRA_PRODUTO      
                                                      AND D.PRODUTO               = C.PRODUTO      
      
   WHERE D.SALDO            > 0      
     AND @PEDIDOS_PENDENTES = 'S'      
     AND A.EMPRESA          = @DEPOSITO      
           
GROUP BY D.PRODUTO       
      
CREATE INDEX PEDIDOS_PENDENTES_01 ON #PEDIDOS_PENDENTES_DEPOSITO (PRODUTO)      
      
UPDATE #PEDIDOS_PENDENTES_DEPOSITO SET PEDIDOS = 0 WHERE PEDIDOS < 0      
      
      
--------------------------------------------------------              
--DEFINICAO DO TRANSFERENCIAS PENDENTES NO CD         
--------------------------------------------------------              
--PEGA TODAS AS TRANSFERENCIAS QUE  EST�O PENDENTES DE SAIDA DO CD      
--OU SEJA, AINDA N�O FORAM FATURADAS--        
--------------------------------------------------------                 
          
if object_id('tempdb..#PENDENCIAS_TRANSF_SAIDAS_DEPOSITO') is not null                
   DROP TABLE #PENDENCIAS_TRANSF_SAIDAS_DEPOSITO              
          
SELECT A.PRODUTO,          
       SUM(A.TRANSITO_ORIGEM) AS TRANSF_PENDENTE_SAIDA_DEPOSITO          
             
  INTO #PENDENCIAS_TRANSF_SAIDAS_DEPOSITO          
                 
  FROM ESTOQUE_TRANSFERENCIAS_RASTREABILIDADE A WITH(NOLOCK)      
 WHERE A.CENTRO_ESTOQUE_ORIGEM = @CENTRO_ESTOQUE_CENTRAL      
   AND @TIPO_COMPRA = 'C'              
 GROUP BY A.PRODUTO          
HAVING SUM(A.TRANSITO_ORIGEM) > 0       
      
--FIM TRANSITO DE SAIDA DO CD--              
      
/*    
      
--------------------------------------------------------              
--DEFINICAO DO TRANSFERENCIAS PENDENTES DE SAIDA DAS LOJAS        
--------------------------------------------------------              
--PEGA TODAS AS TRANSFERENCIAS QUE  EST�O PENDENTES DE SAIDA DAS LOJAS      
--OU SEJA, AINDA N�O FORAM FATURADAS--        
--------------------------------------------------------   
 if object_id('tempdb..#PENDENCIAS_TRANSF_SAIDAS_LOJAS') is not null                
            DROP TABLE #PENDENCIAS_TRANSF_SAIDAS_LOJAS              
          
SELECT A.PRODUTO,          
       B.EMPRESA,      
       SUM(A.TRANSITO_ORIGEM) AS TRANSF_PENDENTE_SAIDA_LOJA          
             
  INTO #PENDENCIAS_TRANSF_SAIDAS_LOJAS          
                 
  FROM ESTOQUE_TRANSFERENCIAS_RASTREABILIDADE A WITH(NOLOCK)      
  JOIN CENTROS_ESTOQUE                        B WITH(NOLOCK) ON B.OBJETO_CONTROLE = A.CENTRO_ESTOQUE_ORIGEM      
  JOIN SUGESTOES_COMPRAS_EMPRESAS             C WITH(NOLOCK) ON C.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                                                            AND C.EMPRESA         = B.EMPRESA        
 GROUP BY A.PRODUTO , B.EMPRESA         
HAVING SUM(A.TRANSITO_ORIGEM) > 0       
      
--FIM TRANSITO DE SAIDA DAS LOJAS--                    
          
--------------------------------------------------------              
--DEFINICAO DO TRANSFERENCIAS PENDENTES DE ENTRADA DAS LOJAS        
--------------------------------------------------------              
--PEGA TODAS AS TRANSFERENCIAS QUE  EST�O PENDENTES DE ENTRADA DAS LOJAS      
--OU SEJA, AINDA N�O FORAM RECEBIDAS--        
--------------------------------------------------------          
      
IF object_id('tempdb..#PENDENCIAS_TRANSF_ENTRADAS_LOJAS') IS NOT NULL                
    DROP TABLE #PENDENCIAS_TRANSF_ENTRADAS_LOJAS            
          
          
SELECT A.PRODUTO,          
       B.EMPRESA,      
       SUM(A.TRANSITO_DESTINO) AS TRANSF_PENDENTE_ENTRADA_LOJA          
             
  INTO #PENDENCIAS_TRANSF_ENTRADAS_LOJAS          
                 
  FROM ESTOQUE_TRANSFERENCIAS_RASTREABILIDADE A WITH(NOLOCK)      
  JOIN CENTROS_ESTOQUE                        B WITH(NOLOCK) ON B.OBJETO_CONTROLE = A.CENTRO_ESTOQUE_DESTINO      
  JOIN SUGESTOES_COMPRAS_EMPRESAS             C WITH(NOLOCK) ON C.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
                                                            AND C.EMPRESA         = B.EMPRESA        
 GROUP BY A.PRODUTO , B.EMPRESA         
HAVING SUM(A.TRANSITO_DESTINO) > 0      
            
--FIM TRANSITO ENTRADA LOJA--                 
                
  */    
    
--------------------------------------------------------      
--CONSOLIDA PENDENCIAS DE COMPRAS DAS LOJAS      
--------------------------------------------------------      
      
IF object_id('tempdb..#PENDENCIAS_COMPRAS_LOJAS') IS NOT NULL      
    DROP TABLE #PENDENCIAS_COMPRAS_LOJAS      
          
SELECT X.PRODUTO,      
       X.EMPRESA,      
       SUM(X.PEDIDOS) AS PEDIDOS      
      
  INTO #PENDENCIAS_COMPRAS_LOJAS      
      
  FROM (      
      
SELECT A.PRODUTO,      
       A.EMPRESA,      
       A.PEDIDOS      
             
 FROM #PEDIDOS_PENDENTES  A WITH(NOLOCK)      
      
 ) X      
       
 GROUP BY X.PRODUTO, X.EMPRESA      
       
       
--------------------------------------------------------      
--CONSOLIDA PENDENCIAS DE COMPRAS DO DEPOSITO      
--------------------------------------------------------      
      
IF object_id('tempdb..#PENDENCIAS_COMPRAS_DEPOSITO') IS NOT NULL      
    DROP TABLE #PENDENCIAS_COMPRAS_DEPOSITO      
          
SELECT X.PRODUTO,      
       SUM(X.PEDIDOS) AS PEDIDOS      
      
  INTO #PENDENCIAS_COMPRAS_DEPOSITO      
      
  FROM (      
      
SELECT A.PRODUTO,      
       A.PEDIDOS      
             
 FROM #PEDIDOS_PENDENTES_DEPOSITO A WITH(NOLOCK)      
      
 ) X      
       
 GROUP BY X.PRODUTO      
      
----------------------------------------      
-- GERA TABELA TEMPORARIA DA SUGESTAO --      
----------------------------------------      
      
   SELECT DISTINCT       
          @FORMULARIO_ORIGEM                                              AS FORMULARIO_ORIGEM,      
          @TAB_MASTER_ORIGEM                                              AS TAB_MASTER_ORIGEM,      
          @REG_MASTER_ORIGEM                                              AS REG_MASTER_ORIGEM,      
          @SUGESTAO_COMPRA                                   AS SUGESTAO_COMPRA ,       
          CASE WHEN @TIPO_COMPRA <> 'L' THEN @DEPOSITO ELSE S.EMPRESA END AS EMPRESA_COMPRA  ,      
          S.EMPRESA                                                       AS EMPRESA,       
          C.FILIAL                                                        AS FILIAL,       
          A.PRODUTO                                                       AS PRODUTO,       
          A.DESCRICAO_REDUZIDA                                            AS DESCRICAO_VISUAL,      
          ISNULL ( E.MES_1 , 0 )                                          AS MES_1 ,       
          ISNULL ( E.MES_2 , 0 )                                          AS MES_2 ,       
          ISNULL ( E.MES_3 , 0 )                                          AS MES_3 ,       
          ISNULL ( E.MES_4 , 0 )                                          AS MES_4 ,       
          ISNULL(ISNULL(E.CURVA,F.CURVA), 'E')                            AS CURVA ,       
                
          CASE WHEN ISNULL(F.CURVA, 'E') = 'A' THEN @DIAS_CURVA_A      
               WHEN ISNULL(F.CURVA, 'E') = 'B' THEN @DIAS_CURVA_B      
               WHEN ISNULL(F.CURVA, 'E') = 'C' THEN @DIAS_CURVA_C      
               WHEN ISNULL(F.CURVA, 'E') = 'D' THEN @DIAS_CURVA_D      
               ELSE @DIAS_CURVA_E      
          END                                                             AS DIAS_ESTOQUE ,      
                
                
          ISNULL ( E.DEMANDA_DIA , 0 )                                    AS DEMANDA_DIA ,       
          ISNULL ( E.DEMANDA_DIA_PONDERADA , 0 )                          AS DEMANDA_DIA_PONDERADA ,      
          ISNULL ( CASE WHEN G.ESTOQUE_SALDO < 0       
                        THEN 0       
                        ELSE G.ESTOQUE_SALDO END , 0 )                    AS ESTOQUE_SALDO ,       
          ISNULL(G.ESTOQUE_SALDO_REAL,0)                                  AS ESTOQUE_SALDO_REAL,      
                              
          ISNULL ( E.VALOR_BASE,0)                                        AS VALOR_BASE,                             
          ISNULL ( N.PEDIDOS , 0 )                                        AS PEDIDOS_PENDENTES ,      
                
          --ISNULL ( D.TRANSF_PENDENTE_SAIDA_LOJA , 0 )                     AS TRANSF_PENDENTE_SAIDA_LOJA ,      
                
          --ISNULL ( B.TRANSF_PENDENTE_ENTRADA_LOJA , 0 )                   AS TRANSF_PENDENTE_ENTRADA_LOJA ,      
    
          0                                                                AS TRANSF_PENDENTE_SAIDA_LOJA ,      
                                                                 
          0                                                                AS TRANSF_PENDENTE_ENTRADA_LOJA ,      
      
          ROUND  ( ISNULL ( E.DEMANDA_DIA_PONDERADA , 0 ) *       
                 ( CASE WHEN ISNULL(F.CURVA, 'E') = 'A' THEN @DIAS_CURVA_A      
                        WHEN ISNULL(F.CURVA, 'E') = 'B' THEN @DIAS_CURVA_B      
                        WHEN ISNULL(F.CURVA, 'E') = 'C' THEN @DIAS_CURVA_C      
                        WHEN ISNULL(F.CURVA, 'E') = 'D' THEN @DIAS_CURVA_D      
                                                        ELSE @DIAS_CURVA_E END ) , 0 ) AS ESTOQUE_SUGERIDO_ORIGINAL ,      
                                                 
          ROUND  ( ISNULL ( E.DEMANDA_DIA_PONDERADA , 0 ) * ( CASE WHEN ISNULL(F.CURVA, 'E') = 'A' THEN @DIAS_CURVA_A      
                                                                   WHEN ISNULL(F.CURVA, 'E') = 'B' THEN @DIAS_CURVA_B      
                                                                   WHEN ISNULL(F.CURVA, 'E') = 'C' THEN @DIAS_CURVA_C      
                                                                   WHEN ISNULL(F.CURVA, 'E') = 'D' THEN @DIAS_CURVA_D      
                                                                   ELSE @DIAS_CURVA_E END ) ,0 )           
                                                                           
                                                                       AS ESTOQUE_SUGERIDO ,      
      
          CAST(0.00 AS NUMERIC(15,2))                                     AS COMPRA_SUGERIDA,                                                
          CAST(0.00 AS NUMERIC(15,2))                                     AS ESTOQUE_SUGERIDO_DEPOSITO,      
          ISNULL(Y.ENCOMENDA,0)                                           AS ENCOMENDA,      
          A.FATOR_EMBALAGEM                                               AS FATOR_EMBALAGEM ,      
          A.EMBALAGEM_INDUSTRIA                                           AS EMBALAGEM_INDUSTRIA ,      
          CAST(NULL AS DATETIME )                                         AS ULTIMA_COMPRA ,      
          ISNULL ( CASE WHEN @TIPO_PRECO = 1       
                        THEN O.PRECO_FABRICA       
                        ELSE dbo.MAIOR_ZERO(M.CUSTO, O.PRECO_FABRICA) END , 0 ) *       
                             A.FATOR_EMBALAGEM                            AS PRECO_COMPRA_BRUTO ,       
          CAST(0 AS NUMERIC(15,2))                       AS DESCONTO_COMPRA ,       
          ISNULL ( CASE WHEN @TIPO_PRECO = 1       
                        THEN O.PRECO_FABRICA       
                        ELSE dbo.MAIOR_ZERO(M.CUSTO, O.PRECO_FABRICA) END , 0 ) *       
                             A.FATOR_EMBALAGEM                            AS PRECO_COMPRA ,       
          0                                                               AS ICMS_COMPRA ,      
          0                                                               AS REPASSE ,      
          ISNULL ( O.PRECO_MAXIMO , 0 )                                   AS PRECO_MAXIMO ,      
          ISNULL ( O.DESCONTO_PADRAO , 0 )                                AS DESCONTO_VENDA ,       
          ISNULL ( O.PRECO_VENDA , 0 )                                    AS PRECO_VENDA ,       
          ISNULL ( CASE WHEN O.IVA = 0 THEN O.ALIQUOTA_ICMS       
                                       ELSE O.ICMS_SUBSTITUTO END , 0 )   AS ICMS_VENDA ,       
          ISNULL ( O.IVA , 0 )                                            AS IVA ,       
          ISNULL ( M.CUSTO , 0 )                                          AS CUSTO_ULTIMA_COMPRA ,      
          ISNULL ( O.PRECO_FABRICA , 0 ) * A.FATOR_EMBALAGEM              AS PRECO_FABRICA ,      
      
          ISNULL(G.ESTOQUE_MINIMO,0)        AS ESTOQUE_MINIMO ,                
          ISNULL(G.VALIDADE_MINIMO, CONVERT(DATE, GETDATE() ) )           AS VALIDADE_MINIMO ,      
          G.VALIDADE_MINIMO                                        AS VALIDADE_MINIMO_VISUAL ,      
      
          ISNULL(P.ESTOQUE_MINIMO,0)                                      AS ESTOQUE_MINIMO_CD,                     
          ISNULL(G.VALIDADE_MINIMO, CONVERT(DATE, GETDATE() ) )           AS VALIDADE_MINIMO_CD,      
          P.VALIDADE_MINIMO                                               AS VALIDADE_MINIMO_CD_VISUAL ,      
                                    
          ISNULL(G.ESTOQUE_MAXIMO,0)                                      AS ESTOQUE_MAXIMO ,          
          ISNULL(G.ESTOQUE_SEGURANCA,0)                                   AS ESTOQUE_SEGURANCA,            
          M.CUSTO                                                         AS ULTIMO_CUSTO,      
          SUBSTRING(A.DESCRICAO,1,255)                                    AS DESCRICAO ,      
          ISNULL(I.ESTOQUE_REDE,0)                                        AS ESTOQUE_REDE ,               
          'N'                                                             AS OBRIGATORIO_APROVACAO,               
          @TIPO_COMPRA                                                    AS TIPO_COMPRA,               
          CAST(NULL AS NUMERIC(15))                                       AS ULTIMO_PEDIDO_COMPRA,      
          CAST(NULL AS NUMERIC(15))                                       AS ULTIMO_FORNECEDOR,      
          CAST(0.00 AS NUMERIC(15,2))         AS ULTIMA_QTDE_COMPRA,      
          CAST(0.00 AS NUMERIC(15,2))                                     AS ULTIMO_VALOR_COMPRA,      
          CAST(0.00 AS NUMERIC(15,2))                                     AS ULTIMO_DESCONTO_COMPRA  ,      
          X.PRODUTO_DEMANDA                                               AS PRODUTO_DEMANDA ,      
          CAST(0.00 AS NUMERIC(15,2))                                     AS EXCESSO_LOJA_INTEGRADO,      
          CAST(0.00 AS NUMERIC(15,2))                                     AS NECESSIDADE_LOJA_INTEGRADO    ,      
          ISNULL(S.INTEGRADO_EXCESSO, 'N')                                AS INTEGRADO_EXCESSO,      
          ISNULL(S.INTEGRADO_NECESSIDADE, 'N')                            AS INTEGRADO_NECESSIDADE,      
          NULL                                                            AS ULTIMA_NF_COMPRA,      
                
          CASE WHEN @TIPO_COMPRA = 'C'      
               THEN @CENTRO_ESTOQUE_CENTRAL      
               ELSE S.CENTRO_ESTOQUE      
          END AS CENTRO_ESTOQUE,      
                
         CAST ( 0 AS NUMERIC(15,2)) AS VALOR_BASE_DEMANDA,      
         CAST ( 0 AS NUMERIC(15,2)) AS ESTOQUE_EXCESSO,      
   ISNULL(W.FALTEIRO,'N')     AS FALTEIRO,      
   W.QTD_FALTEIRO             AS QTD_FALTEIRO,      
          ROUND  ( ISNULL ( E.DEMANDA_DIA_PONDERADA , 0 ) * ( CASE WHEN ISNULL(F.CURVA, 'E') = 'A' THEN @DIAS_CURVA_A      
                                                                     WHEN ISNULL(F.CURVA, 'E') = 'B' THEN @DIAS_CURVA_B      
                                                                     WHEN ISNULL(F.CURVA, 'E') = 'C' THEN @DIAS_CURVA_C      
                                                                     WHEN ISNULL(F.CURVA, 'E') = 'D' THEN @DIAS_CURVA_D      
                                                                     ELSE @DIAS_CURVA_E END ) ,0 )      
                                    AS SUGERIDO_TIPO_CALCULO_1,      
          ROUND  ( ISNULL ( E.DEMANDA_DIA_PONDERADA , 0 ) * ( CASE WHEN ISNULL(F.CURVA, 'E') = 'A' THEN @DIAS_CURVA_A      
                                                                     WHEN ISNULL(F.CURVA, 'E') = 'B' THEN @DIAS_CURVA_B      
                                                                     WHEN ISNULL(F.CURVA, 'E') = 'C' THEN @DIAS_CURVA_C      
                                                                     WHEN ISNULL(F.CURVA, 'E') = 'D' THEN @DIAS_CURVA_D      
                                                        ELSE @DIAS_CURVA_E END ) ,0 )               
         AS SUGERIDO_TIPO_CALCULO_2      
                
                
    INTO #NECESSIDADE_LOJA      
      
     FROM PRODUTOS                                   A WITH(NOLOCK)      
               
     JOIN #PRODUTOS                                  X WITH(NOLOCK) ON X.PRODUTO          = A.PRODUTO      
     JOIN SUGESTOES_COMPRAS_EMPRESAS                 S WITH(NOLOCK) ON S.SUGESTAO_COMPRA  = @SUGESTAO_COMPRA      
LEFT JOIN VALOR_BASE                                 E WITH(NOLOCK) ON E.PRODUTO          = X.PRODUTO_DEMANDA      
                                                                   AND E.MES              = @MES      
                                                                   AND E.ANO              = @ANO                                                         
                                                                   AND E.EMPRESA          = ISNULL(S.EMPRESA_DEMANDA, S.EMPRESA )      
LEFT JOIN SUGESTOES_COMPRAS_PRODUTOS_FALTEIROS_LOJAS W WITH(NOLOCK) ON W.SUGESTAO_COMPRA  = S.SUGESTAO_COMPRA      
                                                                   AND W.PRODUTO          = X.PRODUTO_DEMANDA      
                                                                   AND W.EMPRESA          = ISNULL(S.EMPRESA_DEMANDA, S.EMPRESA )                                                            
LEFT JOIN CURVA_DEMANDA                              F WITH(NOLOCK) ON F.PRODUTO       = X.PRODUTO_DEMANDA      
                                                                   AND F.MES              = @MES      
                                                                   AND F.ANO              = @ANO                                                         
                                                                   AND F.EMPRESA          = ISNULL(S.EMPRESA_DEMANDA, S.EMPRESA )      
                                                                         
                                                                                                                             
     JOIN EMPRESAS_USUARIAS                          C WITH(NOLOCK) ON C.EMPRESA_USUARIA  = S.EMPRESA      
LEFT JOIN #ESTOQUE_LOJA                              G WITH(NOLOCK) ON G.EMPRESA          = S.EMPRESA       
                                                                   AND G.PRODUTO          = A.PRODUTO      
                                                                         
LEFT JOIN #ESTOQUE_REDE                              I WITH(NOLOCK) ON I.PRODUTO          = A.PRODUTO      
                
LEFT JOIN #ESTOQUE_DEPOSITO                          P WITH(NOLOCK) ON P.PRODUTO          = A.PRODUTO      
                
     JOIN SITUACOES_PRODUTOS                         J WITH(NOLOCK) ON J.SITUACAO_PRODUTO = A.SITUACAO_PRODUTO      
                                        AND J.SUGESTAO_COMPRA  = 'S'      
                                                                         
LEFT JOIN ULTIMA_COMPRA_PRODUTOS_EMPRESAS            M WITH(NOLOCK) ON M.PRODUTO          = A.PRODUTO      
                                                                   AND M.EMPRESA          = S.EMPRESA_COMPRA      
                                                              
LEFT JOIN #PENDENCIAS_COMPRAS_LOJAS                  N WITH(NOLOCK) ON N.PRODUTO          = A.PRODUTO      
                                                                   AND N.EMPRESA          = S.EMPRESA      
LEFT JOIN PRODUTOS_PARAMETROS_EMPRESAS               O WITH(NOLOCK) ON O.PRODUTO          = A.PRODUTO      
                                                                   AND O.EMPRESA          = C.EMPRESA_USUARIA      
                                 
LEFT JOIN GRUPOS_COMPRAS_PRODUTOS                    R WITH(NOLOCK) ON R.PRODUTO          = A.PRODUTO       
                                                                   AND R.GRUPO_COMPRA     = @GRUPO_COMPRA        
LEFT JOIN #ENCOMENDAS            Y WITH(NOLOCK) ON Y.EMPRESA       = S.EMPRESA      
                                                                   AND Y.PRODUTO          = A.PRODUTO               
      
--LEFT JOIN #PENDENCIAS_TRANSF_SAIDAS_LOJAS            D WITH(NOLOCK) ON D.EMPRESA          = S.EMPRESA      
--                                                                   AND D.PRODUTO          = A.PRODUTO      
      
--LEFT JOIN #PENDENCIAS_TRANSF_ENTRADAS_LOJAS          B WITH(NOLOCK) ON B.EMPRESA          = S.EMPRESA      
--                                                                   AND B.PRODUTO          = A.PRODUTO      
    
    WHERE ISNULL(G.SITUACAO_PRODUTO_COMPRA,1) = 1      
      AND ISNULL(A.SITUACAO_PRODUTO_COMPRA,1) = 1      
      AND ISNULL(A.SITUACAO_PRODUTO,1) = 1      
            
 ORDER BY DESCRICAO, FILIAL      
      
      
CREATE NONCLUSTERED INDEX [IX_#NECESSIDADE_LOJA_001]      
ON [dbo].[#NECESSIDADE_LOJA] ([EMPRESA_COMPRA],[PRODUTO])      
INCLUDE ([DESCONTO_COMPRA],[PRECO_COMPRA],[PRECO_FABRICA])      
      
      
--------------------------      
--ATUALIZA O VALOR BASE      
--------------------------      
      
UPDATE #NECESSIDADE_LOJA      
      
   SET VALOR_BASE_DEMANDA =       
         
                    CASE WHEN @TIPO_CALCULO = 1 --EMAX ESEG      
                         THEN CASE WHEN ISNULL(A.VALIDADE_MINIMO, CONVERT(DATE, GETDATE() ) ) < CONVERT(DATE,GETDATE())      
                                   THEN ISNULL(A.ESTOQUE_MAXIMO,0)      
                                   ELSE DBO.MAIOR_VALOR ( ISNULL(A.ESTOQUE_MAXIMO,0), ISNULL(A.ESTOQUE_MINIMO,0) )      
                              END      
      
                         ELSE CASE WHEN ISNULL(A.VALIDADE_MINIMO, CONVERT(DATE, GETDATE() ) ) < CONVERT(DATE,GETDATE())      
                                   THEN ISNULL(A.ESTOQUE_SUGERIDO,0)      
                                   ELSE DBO.MAIOR_VALOR ( ISNULL(A.ESTOQUE_MINIMO,0), ISNULL(A.ESTOQUE_SUGERIDO,0))      
                              END      
                    END      
                
  FROM #NECESSIDADE_LOJA A       
      
      
      
--------------------------------------------------      
-- GERA TEMPOR�RIA DA ULTIMA COMPRA POR PRODUTO --      
--------------------------------------------------      
      
----------------------------------------------------------      
-- ATUALIZA��O DOS DADOS DA �LTIMA COMPRA NA TEMPOR�RIA --      
-- NECESSIDADE_LOJA                                     --      
----------------------------------------------------------      
      
----------------------------------      
-- ATUALIZA DADOS DA ULTIMA COMPRA --      
------------------------------------      
      
if object_id('tempdb..#ULTIMA_COMPRA') is not null      
   DROP TABLE #ULTIMA_COMPRA      
      
--LOJA --      
SELECT A.PRODUTO,      
       A.EMPRESA,      
       A.DATA,      
       A.ULTIMA_COMPRA      * B.FATOR_EMBALAGEM AS PRECO_COMPRA,      
       A.DESCONTO,      
       A.PRECO_COMPRA_BRUTO * B.FATOR_EMBALAGEM AS PRECO_COMPRA_BRUTO,      
       A.QUANTIDADE_ESTOQUE,      
       A.FORMULARIO_ORIGEM,       
       A.TAB_MASTER_ORIGEM,       
       A.REG_MASTER_ORIGEM      
       
  INTO #ULTIMA_COMPRA      
      
  FROM ULTIMO_CUSTO_EMPRESA              A WITH(NOLOCK)      
  JOIN #NECESSIDADE_LOJA                 B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO      
                                                       AND B.EMPRESA = A.EMPRESA      
      
 WHERE @TIPO_COMPRA = 'L'      
      
UNION ALL      
      
SELECT A.PRODUTO,      
       A.EMPRESA,      
       A.DATA,      
       A.ULTIMA_COMPRA      * B.FATOR_EMBALAGEM AS PRECO_COMPRA,      
       A.DESCONTO,      
       A.PRECO_COMPRA_BRUTO * B.FATOR_EMBALAGEM AS PRECO_COMPRA_BRUTO,      
       A.QUANTIDADE_ESTOQUE,      
       A.FORMULARIO_ORIGEM,       
       A.TAB_MASTER_ORIGEM,       
       A.REG_MASTER_ORIGEM      
      
  FROM ULTIMO_CUSTO_EMPRESA   A WITH(NOLOCK)      
  JOIN PRODUTOS               B WITH(NOLOCK) ON B.PRODUTO = A.PRODUTO      
 WHERE @TIPO_COMPRA <> 'L'      
   AND A.EMPRESA = @DEPOSITO      
         
         
UPDATE #NECESSIDADE_LOJA      
      
   SET ULTIMO_PEDIDO_COMPRA   = C.PEDIDO_COMPRA     ,       
       ULTIMO_FORNECEDOR      = C.ENTIDADE          ,      
       ULTIMA_QTDE_COMPRA     = A.QUANTIDADE_ESTOQUE        ,      
       ULTIMO_VALOR_COMPRA    = A.PRECO_COMPRA_BRUTO    ,      
             
       ULTIMO_DESCONTO_COMPRA = A.DESCONTO,      
                                      
       DESCONTO_COMPRA        = CASE WHEN @TIPO_PRECO = 1 OR @FORNECEDOR > 0       
                                     THEN D.DESCONTO_COMPRA      
                                     ELSE A.DESCONTO      
                                END,      
      
       ULTIMA_COMPRA          = A.DATA ,      
      
       PRECO_COMPRA           = ISNULL ( CASE WHEN @TIPO_PRECO = 1      
                                              THEN D.PRECO_COMPRA      
                                              ELSE DBO.MAIOR_ZERO(ISNULL(A.PRECO_COMPRA,0) , ISNULL(D.PRECO_COMPRA,0) )       
                                         END , 0 ),      
      
       PRECO_COMPRA_BRUTO     = ISNULL ( CASE WHEN @TIPO_PRECO = 1      
                                              THEN D.PRECO_COMPRA_BRUTO      
                                              ELSE DBO.MAIOR_ZERO(ISNULL(A.PRECO_COMPRA_BRUTO,0) , ISNULL(D.PRECO_COMPRA,0) )       
                                         END , 0 ) ,      
       ULTIMA_NF_COMPRA       = C.NF_COMPRA            
  FROM #ULTIMA_COMPRA     A WITH(NOLOCK)      
  JOIN NF_COMPRA          C WITH(NOLOCK) ON C.FORMULARIO_ORIGEM = A.FORMULARIO_ORIGEM      
                                        AND C.TAB_MASTER_ORIGEM = A.TAB_MASTER_ORIGEM      
                                        AND C.NF_COMPRA         = A.REG_MASTER_ORIGEM      
      
  JOIN #NECESSIDADE_LOJA  D WITH(NOLOCK) ON D.PRODUTO           = A.PRODUTO      
      AND D.EMPRESA_COMPRA    = A.EMPRESA      
      
      
       
--APLICA OS PRECOS E DESCONTOS DO MIX DE FORNECEDORES--      
      
UPDATE #NECESSIDADE_LOJA      
       
  SET PRECO_COMPRA_BRUTO = CASE WHEN B.PRECO_COMPRA > 0 THEN B.PRECO_COMPRA * A.FATOR_EMBALAGEM ELSE A.PRECO_COMPRA END,      
      PRECO_COMPRA       = CASE WHEN B.PRECO_COMPRA > 0 THEN B.PRECO_COMPRA * A.FATOR_EMBALAGEM ELSE A.PRECO_COMPRA END,      
      DESCONTO_COMPRA    = B.DESCONTO_COMPRA         
      
  FROM #NECESSIDADE_LOJA                 A       
  JOIN #FORNECEDORES_PRECOS_DESCONTOS    B ON B.PRODUTO = A.PRODUTO      
      
      
--------------------------------------------------------------------------      
-- ATUALIZACAO DA NECESSIDADE CONFORME MINIMO DE CADA PRODUTO E EMPRESA --      
--------------------------------------------------------------------------      
      
SELECT A.PRODUTO,      
       A.EMPRESA,      
       A.VALIDADE_MINIMO,      
       A.ESTOQUE_MINIMO,      
       A.ESTOQUE_MAXIMO,      
       A.ESTOQUE_SALDO,      
       A.PEDIDOS_PENDENTES,      
             
       A.DEMANDA_DIA_PONDERADA,      
      
      ( (       
   CASE WHEN @TIPO_CALCULO <> 3 THEN      
   ( CASE WHEN A.VALIDADE_MINIMO >= CONVERT(DATE, GETDATE() )      
                 THEN CASE WHEN @TIPO_CALCULO = 1       
                           THEN CASE WHEN DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO ) >  0      
                                     THEN CASE WHEN ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES - A.TRANSF_PENDENTE_SAIDA_LOJA + A.TRANSF_PENDENTE_ENTRADA_LOJA ) <= DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SEGURANCA )       
              THEN DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                                               ELSE 0      
                                          END      
                                     ELSE 0      
                                END      
                                                
                           ELSE DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SUGERIDO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                      END      
                            
                 ELSE CASE WHEN @TIPO_CALCULO = 1       
                           THEN CASE WHEN ( A.ESTOQUE_MAXIMO ) >  0      
                             THEN CASE WHEN ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES  + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA ) <= A.ESTOQUE_SEGURANCA      
                                              THEN ( A.ESTOQUE_MAXIMO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                                              ELSE 0      
                                          END      
                                     ELSE 0          
                                 END      
                           ELSE DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SUGERIDO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                       END      
                       
              END       
      )       ELSE      
DBO.MAIOR_VALOR (      
       ( CASE WHEN A.VALIDADE_MINIMO >= CONVERT(DATE, GETDATE() )      
                 THEN CASE WHEN @TIPO_CALCULO = 1       
                           THEN CASE WHEN DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO ) >  0      
                                  THEN CASE WHEN ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES - A.TRANSF_PENDENTE_SAIDA_LOJA + A.TRANSF_PENDENTE_ENTRADA_LOJA ) <= DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SEGURANCA )       
                                               THEN DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                                               ELSE 0      
                                          END      
                                     ELSE 0      
                                END      
                                                
                           ELSE DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SUGERIDO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                      END      
                            
                 ELSE CASE WHEN @TIPO_CALCULO = 1       
                           THEN CASE WHEN ( A.ESTOQUE_MAXIMO ) >  0      
                                     THEN CASE WHEN ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES  + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA ) <= A.ESTOQUE_SEGURANCA      
                                              THEN ( A.ESTOQUE_MAXIMO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
                                              ELSE 0      
                                          END      
                                     ELSE 0          
                                 END      
                           ELSE 0      
                       END      
                       
              END       
      )      
                ,      
                DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SUGERIDO ) - ( A.ESTOQUE_SALDO + A.PEDIDOS_PENDENTES + A.TRANSF_PENDENTE_ENTRADA_LOJA - A.TRANSF_PENDENTE_SAIDA_LOJA )      
    )      
              END               
      ) ) AS COMPRA_SUGERIDA,      
                    
     ( CASE WHEN A.VALIDADE_MINIMO >= CAST(CONVERT(VARCHAR(10), GETDATE(), 103 ) AS DATETIME )      
            THEN CASE WHEN @TIPO_CALCULO = 1       
     THEN DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_MAXIMO )      
                      ELSE DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SUGERIDO )      
                 END      
                       
            ELSE CASE WHEN @TIPO_CALCULO = 1       
                      THEN A.ESTOQUE_MAXIMO      
                      ELSE DBO.MAIOR_VALOR ( A.ESTOQUE_MINIMO, A.ESTOQUE_SUGERIDO )      
                 END                            
        END ) AS ESTOQUE_SUGERIDO,                    
              
        CASE WHEN A.INTEGRADO_EXCESSO = 'S'      
             THEN CASE WHEN ( A.ESTOQUE_SALDO - A.ESTOQUE_MAXIMO ) > 0       
                       THEN ( A.ESTOQUE_SALDO - A.ESTOQUE_MAXIMO ) * -1      
                       ELSE 0      
                 END      
    ELSE 0      
     END  AS EXCESSO_LOJA_INTEGRADO        ,      
              
              
        CASE WHEN A.ESTOQUE_SALDO > A.VALOR_BASE_DEMANDA      
             THEN A.ESTOQUE_SALDO - A.VALOR_BASE_DEMANDA      
             ELSE 0      
        END AS ESTOQUE_EXCESSO      
      
    INTO #COMPRA_SUGERIDA      
      
    FROM #NECESSIDADE_LOJA            A WITH(NOLOCK)      
      
      
      
UPDATE #COMPRA_SUGERIDA SET COMPRA_SUGERIDA = 0 WHERE COMPRA_SUGERIDA < 0      
      
      
UPDATE #NECESSIDADE_LOJA      
      
   SET COMPRA_SUGERIDA           = CASE WHEN A.COMPRA_SUGERIDA + B.ENCOMENDA < 0      
                                        THEN 0      
                                        ELSE A.COMPRA_SUGERIDA + B.ENCOMENDA      
                                   END,      
                                         
       ESTOQUE_SUGERIDO          = A.ESTOQUE_SUGERIDO,                               
                
       ESTOQUE_SUGERIDO_DEPOSITO = CASE WHEN @TIPO_COMPRA = 'L'      
                                        THEN 0      
                                        ELSE ( B.DEMANDA_DIA_PONDERADA ) * B.DIAS_ESTOQUE      
                                   END  + ( CASE WHEN B.VALIDADE_MINIMO_CD >= GETDATE()      
                                                 THEN B.ESTOQUE_MINIMO_CD      
                                                 ELSE 0      
                                            END )      ,      
                    
       EXCESSO_LOJA_INTEGRADO    = A.EXCESSO_LOJA_INTEGRADO,      
             
       NECESSIDADE_LOJA_INTEGRADO= CASE WHEN B.INTEGRADO_NECESSIDADE = 'S' AND @TIPO_COMPRA = 'C'      
                                        THEN A.COMPRA_SUGERIDA      
                                        ELSE 0      
                                    END       ,      
                                          
       ESTOQUE_EXCESSO             = A.ESTOQUE_EXCESSO         
             
             
  FROM #COMPRA_SUGERIDA  A , #NECESSIDADE_LOJA B      
 WHERE A.PRODUTO = B.PRODUTO      
   AND A.EMPRESA = B.EMPRESA      
       
      
----------------------------------------------------------------------      
-- EXLUSAO DE REGISTROS GERADOS ANTERIORMENTE - RESULTADO ANAL�TICO --      
----------------------------------------------------------------------      
       
IF EXISTS ( SELECT TOP 1 *      
              FROM SUGESTOES_COMPRAS_RESULTADO_ANALITICO A WITH(NOLOCK)       
             WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA)      
 BEGIN      
       
     DELETE SUGESTOES_COMPRAS_RESULTADO_ANALITICO      
       FROM SUGESTOES_COMPRAS_RESULTADO_ANALITICO         A WITH(NOLOCK)      
       JOIN ( SELECT SUGESTAO_COMPRA_RESULTADO       
                FROM SUGESTOES_COMPRAS_RESULTADO_ANALITICO WITH(NOLOCK)      
               WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA ) B ON B.SUGESTAO_COMPRA_RESULTADO = A.SUGESTAO_COMPRA_RESULTADO      
      
   END        
        
--------------------------------------------------      
---- GERA��O DO RESULTADO ANAL�TICO DA SUGEST�O --      
--------------------------------------------------      
      
INSERT INTO SUGESTOES_COMPRAS_RESULTADO_ANALITICO (       
      
            FORMULARIO_ORIGEM ,      
           TAB_MASTER_ORIGEM ,      
            REG_MASTER_ORIGEM ,       
            SUGESTAO_COMPRA ,      
            EMPRESA ,      
            FILIAL ,      
            PRODUTO ,      
            MES_1 ,      
            MES_2 ,      
            MES_3 ,      
            MES_4 ,      
            CURVA ,      
            DIAS_ESTOQUE ,      
            DEMANDA_DIA ,      
            DEMANDA_DIA_PONDERADA ,      
ESTOQUE_SALDO ,      
            PEDIDOS_PENDENTES ,      
            TRANSF_PENDENTE_SAIDA,      
            TRANSF_PENDENTE_ENTRADA,      
            ESTOQUE_SUGERIDO ,      
            ENCOMENDA,      
            FATOR_EMBALAGEM ,      
            EMBALAGEM_INDUSTRIA ,      
            COMPRA_SUGERIDA ,      
            PRECO_COMPRA_BRUTO ,      
            DESCONTO_COMPRA ,      
            PRECO_COMPRA ,      
            ICMS_COMPRA ,      
            PRECO_MAXIMO ,      
            DESCONTO_VENDA ,      
            PRECO_VENDA ,      
      ICMS_VENDA ,      
            IVA ,      
            CUSTO_ULTIMA_COMPRA ,      
            PRECO_FABRICA ,      
            ESTOQUE_MINIMO ,      
            VALIDADE_MINIMO ,      
           ESTOQUE_REDE ,      
            DESCRICAO_VISUAL,      
    ESTOQUE_SEGURANCA,      
            ESTOQUE_MAXIMO,      
            EXCESSO_LOJA_INTEGRADO,      
            NECESSIDADE_LOJA_INTEGRADO ,      
            ESTOQUE_EXCESSO )      
      
     SELECT A.FORMULARIO_ORIGEM ,      
            A.TAB_MASTER_ORIGEM ,      
            A.REG_MASTER_ORIGEM ,      
            A.SUGESTAO_COMPRA ,      
            A.EMPRESA ,      
            A.FILIAL ,      
            A.PRODUTO ,      
            A.MES_1 ,      
            A.MES_2 ,      
            A.MES_3 ,      
            A.MES_4 ,      
            A.CURVA ,      
            A.DIAS_ESTOQUE ,      
            A.DEMANDA_DIA ,      
            A.DEMANDA_DIA_PONDERADA ,      
            A.ESTOQUE_SALDO ,      
            A.PEDIDOS_PENDENTES ,      
            A.TRANSF_PENDENTE_SAIDA_LOJA,      
            A.TRANSF_PENDENTE_ENTRADA_LOJA,      
            A.ESTOQUE_SUGERIDO ,      
            A.ENCOMENDA,      
            A.FATOR_EMBALAGEM ,      
            A.EMBALAGEM_INDUSTRIA ,      
            A.COMPRA_SUGERIDA ,      
            A.PRECO_COMPRA_BRUTO ,      
            A.DESCONTO_COMPRA ,      
            A.PRECO_COMPRA ,      
            A.ICMS_COMPRA ,      
            A.PRECO_MAXIMO ,      
            A.DESCONTO_VENDA ,      
            A.PRECO_VENDA ,      
            A.ICMS_VENDA ,      
            A.IVA ,      
            A.CUSTO_ULTIMA_COMPRA ,      
            A.PRECO_FABRICA ,      
            A.ESTOQUE_MINIMO ,      
            A.VALIDADE_MINIMO_VISUAL ,      
            A.ESTOQUE_REDE ,      
            A.DESCRICAO_VISUAL,      
            A.ESTOQUE_SEGURANCA,      
            A.ESTOQUE_MAXIMO,      
            A.EXCESSO_LOJA_INTEGRADO,      
            A.NECESSIDADE_LOJA_INTEGRADO,      
            A.ESTOQUE_EXCESSO      
      
       FROM #NECESSIDADE_LOJA                  A      
   WHERE @TIPO_COMPRA <> 'L'      
      
   ORDER BY PRODUTO, FILIAL      
      
      
      
------------------------------------------------------------------      
-- GERA TABELA TEM DE RESULTADO--      
------------------------------------------------------------------      
      
if object_id('tempdb..#RESULTADO') is not null  DROP TABLE #RESULTADO      
      
     SELECT A.SUGESTAO_COMPRA                                 AS SUGESTAO_COMPRA ,       
            A.EMPRESA_COMPRA                                  AS EMPRESA ,      
            B.FILIAL                                          AS FILIAL  ,      
            A.PRODUTO                                         AS PRODUTO ,       
            A.PRODUTO                                         AS PRODUTO_VISIVEL ,       
            A.DESCRICAO                                       AS DESCRICAO_VISIVEL,      
            ISNULL(G.DESCRICAO, 'ND')                         AS DESCRICAO_MARCA,      
            SUM(ISNULL ( A.MES_1 , 0 ))                       AS MES_1 ,       
            SUM(ISNULL ( A.MES_2 , 0 ))                       AS MES_2 ,       
            SUM(ISNULL ( A.MES_3 , 0 ))                       AS MES_3 ,       
            SUM(ISNULL ( A.MES_4 , 0 ))                       AS MES_4 ,       
            CASE WHEN A.TIPO_COMPRA = 'L'       
                 THEN ISNULL(A.CURVA, 'C')      
              ELSE ISNULL(E.CURVA, 'C') END                   AS CURVA,      
                    
            MAX( ISNULL(A.DIAS_ESTOQUE,0) )                   AS DIAS_ESTOQUE,                            
            SUM(ISNULL(A.DEMANDA_DIA,0))                      AS DEMANDA_DIA,          
            SUM(ISNULL(A.DEMANDA_DIA_PONDERADA,0))            AS DEMANDA_DIA_PONDERADA,      
      
            MAX( CASE WHEN A.TIPO_COMPRA <> 'L'        
                      THEN ISNULL(C.ESTOQUE_SALDO,0)      
                      ELSE ISNULL(A.ESTOQUE_SALDO,0) END )    AS ESTOQUE_SALDO ,      
                             
            SUM(ISNULL(A.ESTOQUE_SALDO,0))                    AS ESTOQUE_LOJAS ,      
      
      
            MAX( ISNULL(C.ESTOQUE_SALDO,0) )                  AS ESTOQUE_CD,      
        
            MAX(CASE WHEN A.TIPO_COMPRA <> 'L'        
                     THEN ISNULL(D.PEDIDOS,0)      
        ELSE ISNULL(A.PEDIDOS_PENDENTES,0) END ) AS PEDIDOS_PENDENTES ,       
                           
      
            MAX(CASE WHEN A.TIPO_COMPRA <> 'L'        
                     THEN ISNULL(I.TRANSF_PENDENTE_SAIDA_DEPOSITO,0)      
                     ELSE ISNULL(A.TRANSF_PENDENTE_SAIDA_LOJA,0) END ) AS TRANSF_PENDENTE_SAIDA ,       
      
      
            MAX(CASE WHEN A.TIPO_COMPRA <> 'L'        
                     THEN 0      
                     ELSE ISNULL(A.TRANSF_PENDENTE_ENTRADA_LOJA,0) END ) AS TRANSF_PENDENTE_ENTRADA ,       
                                                                          
            SUM( CASE WHEN A.TIPO_COMPRA <> 'L'        
               THEN ISNULL(A.ESTOQUE_SUGERIDO_DEPOSITO, 0 )      
                      ELSE ISNULL(A.ESTOQUE_SUGERIDO,0) END ) AS ESTOQUE_SUGERIDO ,       
      
            --SUM( ISNULL(A.ESTOQUE_SUGERIDO,0) )               AS ESTOQUE_SUGERIDO ,       
                            
            A.FATOR_EMBALAGEM                                 AS FATOR_EMBALAGEM ,      
            A.EMBALAGEM_INDUSTRIA                             AS EMBALAGEM_INDUSTRIA ,      
            A.ULTIMA_COMPRA                                   AS ULTIMA_COMPRA ,      
            SUM(A.ENCOMENDA)                                  AS ENCOMENDA,      
      
            ROUND( ( CASE WHEN A.TIPO_COMPRA = 'L'      
                          THEN MAX(A.COMPRA_SUGERIDA)       
                          ELSE CASE WHEN( DBO.MAIOR_VALOR ( SUM(A.COMPRA_SUGERIDA) - MAX(ISNULL(C.ESTOQUE_SALDO,0)),  SUM(A.ESTOQUE_SUGERIDO_DEPOSITO ) - ( MAX(ISNULL(C.ESTOQUE_SALDO,0)) + MAX(ISNULL(I.TRANSF_PENDENTE_SAIDA_DEPOSITO,0)) + MAX(ISNULL(D.PEDIDOS,0)) + SUM(ISNULL(A.EXCESSO_LOJA_INTEGRADO,0)) + MAX(A.ESTOQUE_REDE) ) ) ) >= 1      
                                    THEN( DBO.MAIOR_VALOR ( SUM(A.COMPRA_SUGERIDA) - MAX(ISNULL(C.ESTOQUE_SALDO,0)),  SUM(A.ESTOQUE_SUGERIDO_DEPOSITO ) - ( MAX(ISNULL(C.ESTOQUE_SALDO,0)) + MAX(ISNULL(I.TRANSF_PENDENTE_SAIDA_DEPOSITO,0)) + MAX(ISNULL(D.PEDIDOS,0)) + SUM(ISNULL(A.EXCESSO_LOJA_INTEGRADO,0)) + MAX(A.ESTOQUE_REDE) ) ) )      
                                    ELSE 0      
                               END      
                      END ) / A.FATOR_EMBALAGEM, 0 )                             AS COMPRA_SUGERIDA   ,      
   --CASE WHEN      
   --         ROUND( ( CASE WHEN A.TIPO_COMPRA = 'L'      
   --                                 THEN  MAX(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                           ELSE  SUM(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                   END ) / A.FATOR_EMBALAGEM, 0 )       
   --         > 0      
   --THEN      
   --         ROUND( ( CASE WHEN A.TIPO_COMPRA = 'L'      
   --                                 THEN  MAX(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                           ELSE  SUM(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                   END ) / A.FATOR_EMBALAGEM, 0 )       
   --ELSE 0      
   --END           
   --                                    AS COMPRA_SUGERIDA   ,      
               
            CASE WHEN ISNULL(@ZERAR_QUANTIDADE,'N') = 'N'       
                 THEN ROUND( ( CASE WHEN A.TIPO_COMPRA = 'L'      
                                    THEN MAX(A.COMPRA_SUGERIDA)       
                               ELSE CASE WHEN( DBO.MAIOR_VALOR ( SUM(A.COMPRA_SUGERIDA) - MAX(ISNULL(C.ESTOQUE_SALDO,0)),  SUM(A.ESTOQUE_SUGERIDO_DEPOSITO ) - ( MAX(ISNULL(C.ESTOQUE_SALDO,0)) + MAX(ISNULL(I.TRANSF_PENDENTE_SAIDA_DEPOSITO,0)) + MAX(ISNULL(
  
D.PEDIDOS,0)) + SUM(ISNULL(A.EXCESSO_LOJA_INTEGRADO,0)) + MAX(A.ESTOQUE_REDE) ) ) ) >= 1      
                                         THEN( DBO.MAIOR_VALOR ( SUM(A.COMPRA_SUGERIDA) - MAX(ISNULL(C.ESTOQUE_SALDO,0)),  SUM(A.ESTOQUE_SUGERIDO_DEPOSITO ) - ( MAX(ISNULL(C.ESTOQUE_SALDO,0)) + MAX(ISNULL(I.TRANSF_PENDENTE_SAIDA_DEPOSITO,0)) + MAX(ISNULL(
  
D.PEDIDOS,0)) + SUM(ISNULL(A.EXCESSO_LOJA_INTEGRADO,0)) + MAX(A.ESTOQUE_REDE) ) ) )      
                             ELSE 0      
                END      
                                END ) / A.FATOR_EMBALAGEM, 0 )      
                  ELSE 0      
            END                                                   AS COMPRA_AUTORIZADA  ,       
   --CASE WHEN      
   --         CASE WHEN ISNULL(@ZERAR_QUANTIDADE,'N') = 'N'       
   --              THEN ROUND( ( CASE WHEN A.TIPO_COMPRA = 'L'      
   --                                 THEN MAX(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                           ELSE  SUM(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                             END ) / A.FATOR_EMBALAGEM, 0 )      
   --               ELSE 0      
   --         END  >0      
   --THEN        
   --                                                             CASE WHEN ISNULL(@ZERAR_QUANTIDADE,'N') = 'N'       
   --              THEN ROUND( ( CASE WHEN A.TIPO_COMPRA = 'L'      
   --                                 THEN  MAX(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                           ELSE  SUM(A.COMPRA_SUGERIDA) - SUM( A.ESTOQUE_EXCESSO )      
   --                             END ) / A.FATOR_EMBALAGEM, 0 )      
   --               ELSE 0      
   --         END       
   --ELSE 0      
   --END                                                   AS COMPRA_AUTORIZADA  ,       
                                                                                                
            MAX(A.PRECO_COMPRA_BRUTO)                             AS PRECO_COMPRA_BRUTO ,       
            MAX(A.DESCONTO_COMPRA   )                             AS DESCONTO_COMPRA ,      
                  
   MAX(A.PRECO_COMPRA_BRUTO * ( 1 - ( A.DESCONTO_COMPRA / 100 )  ) )      
                                                                  AS PRECO_COMPRA ,       
            MAX(A.PRECO_MAXIMO      )                             AS PRECO_MAXIMO ,      
            MAX(A.DESCONTO_VENDA    )                             AS DESCONTO_VENDA ,       
            MAX(A.PRECO_VENDA       )                             AS PRECO_VENDA ,                   
            CAST (0.00 AS NUMERIC(15,2))                          AS TOTAL_SUGESTAO ,      
            CAST (0.00 AS NUMERIC(15,2))                          AS TOTAL_AUTORIZADO ,      
      
            MAX(A.CUSTO_ULTIMA_COMPRA)       AS CUSTO_ULTIMA_COMPRA ,      
            MAX(A.PRECO_FABRICA )            AS PRECO_FABRICA ,      
      
            MAX( CASE WHEN A.TIPO_COMPRA <> 'L'        
                      THEN ISNULL(A.ESTOQUE_MINIMO_CD,0)      
                      ELSE ISNULL(A.ESTOQUE_MINIMO,0)      
                  END )                      AS ESTOQUE_MINIMO ,       
      
            --SUM(ISNULL(A.ESTOQUE_MINIMO,0))  AS ESTOQUE_MINIMO ,               
            MAX( CASE WHEN A.TIPO_COMPRA <> 'L'        
                      THEN A.VALIDADE_MINIMO_CD_VISUAL      
                      ELSE A.VALIDADE_MINIMO_VISUAL      
                 END )                       AS VALIDADE_MINIMO ,       
      
            MAX( CASE WHEN A.TIPO_COMPRA <> 'L'        
                      THEN 0      
                      ELSE ISNULL(A.ESTOQUE_MAXIMO,0)      
                  END )                       AS ESTOQUE_MAXIMO ,       
      
      
            MAX( CASE WHEN A.TIPO_COMPRA <> 'L'        
                      THEN 0      
                      ELSE ISNULL(A.ESTOQUE_SEGURANCA,0)      
                 END )                       AS ESTOQUE_SEGURANCA,       
                    
            MAX(A.ESTOQUE_REDE)              AS ESTOQUE_REDE,      
                 
            SUM( ROUND( A.COMPRA_SUGERIDA / A.FATOR_EMBALAGEM ,0 ) )       
                                             AS NECESSIDADE_LOJA,      
            A.ULTIMO_PEDIDO_COMPRA  ,      
            A.ULTIMO_FORNECEDOR     ,      
            A.ULTIMA_QTDE_COMPRA    ,      
            A.ULTIMO_VALOR_COMPRA   ,      
            A.ULTIMO_DESCONTO_COMPRA ,      
            A.PRODUTO_DEMANDA     ,      
            SUM(A.EXCESSO_LOJA_INTEGRADO)           AS EXCESSO_LOJA_INTEGRADO,      
            SUM( ROUND( A.NECESSIDADE_LOJA_INTEGRADO / A.FATOR_EMBALAGEM ,0 ) )       
                                     AS NECESSIDADE_LOJA_INTEGRADO,      
            A.ULTIMA_NF_COMPRA       AS ULTIMA_NF_COMPRA,      
            MAX(A.CENTRO_ESTOQUE)    AS CENTRO_ESTOQUE,      
            SUM(A.ESTOQUE_EXCESSO)   AS ESTOQUE_EXCESSO,      
   A.FALTEIRO,      
   A.QTD_FALTEIRO      
                        
   INTO #RESULTADO               
      
      
    FROM #NECESSIDADE_LOJA                   A WITH(NOLOCK)      
    JOIN EMPRESAS_USUARIAS                   B WITH(NOLOCK) ON B.EMPRESA_USUARIA  = A.EMPRESA_COMPRA      
      
LEFT JOIN #ESTOQUE_DEPOSITO                  C WITH(NOLOCK) ON C.PRODUTO          = A.PRODUTO      
        
LEFT JOIN #PENDENCIAS_COMPRAS_DEPOSITO       D WITH(NOLOCK) ON D.PRODUTO          = A.PRODUTO      
      
LEFT JOIN #PENDENCIAS_TRANSF_SAIDAS_DEPOSITO I WITH(NOLOCK) ON I.PRODUTO          = A.PRODUTO      
                                                                 
LEFT JOIN #CURVA_PRODUTO_DEPOSITO            E WITH(NOLOCK) ON E.PRODUTO          = A.PRODUTO      
     JOIN PRODUTOS                           F WITH(NOLOCK) ON F.PRODUTO          = A.PRODUTO      
LEFT JOIN MARCAS                             G WITH(NOLOCK) ON G.MARCA            = F.MARCA      
LEFT JOIN #SUGESTAO_COMPRA_RESULTADO_PARCIAL H              ON H.EMPRESA          = A.EMPRESA_COMPRA      
                                                           AND H.PRODUTO          = A.PRODUTO      
      
WHERE H.SUGESTAO_COMPRA_RESULTADO IS NULL      
      
GROUP BY    A.SUGESTAO_COMPRA           ,         
            A.EMPRESA_COMPRA            ,         
            B.FILIAL                    ,         
            A.PRODUTO                   ,         
            A.PRODUTO                   ,         
            A.DESCRICAO                 ,         
            ISNULL(G.DESCRICAO, 'ND')   ,         
            A.TIPO_COMPRA,      
      
            CASE WHEN A.TIPO_COMPRA = 'L'       
                 THEN ISNULL(A.CURVA, 'C')      
                 ELSE ISNULL(E.CURVA, 'C') END,      
                    
                            
            A.FATOR_EMBALAGEM          ,          
            A.EMBALAGEM_INDUSTRIA      ,          
            A.ULTIMA_COMPRA            ,          
            A.ULTIMO_PEDIDO_COMPRA  ,      
            A.ULTIMO_FORNECEDOR     ,      
            A.ULTIMA_QTDE_COMPRA    ,      
            A.ULTIMO_VALOR_COMPRA   ,      
            A.ULTIMO_DESCONTO_COMPRA ,      
            A.PRODUTO_DEMANDA     ,      
            A.ULTIMA_NF_COMPRA      ,      
            --A.ESTOQUE_EXCESSO,       
   A.FALTEIRO,      
   A.QTD_FALTEIRO        
      
         
    SELECT * FROM #NECESSIDADE_LOJA WHERE PRODUTO = 68682 AND EMPRESA IN (9)      
      
------------------------------------------------------------------      
-- EXLUSAO DE REGISTROS GERADOS ANTERIORMENTE - RESULTADO FINAL --      
------------------------------------------------------------------      
      
IF EXISTS ( SELECT TOP 1 1      
              FROM SUGESTOES_COMPRAS_RESULTADO A WITH(NOLOCK)       
             WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA)      
 BEGIN      
      
DELETE SUGESTOES_COMPRAS_RESULTADO      
  FROM SUGESTOES_COMPRAS_RESULTADO  A WITH(NOLOCK)      
       JOIN ( SELECT SUGESTAO_COMPRA_RESULTADO      
                FROM SUGESTOES_COMPRAS_RESULTADO WITH(NOLOCK)      
               WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA ) B ON B.SUGESTAO_COMPRA_RESULTADO = A.SUGESTAO_COMPRA_RESULTADO       
  LEFT JOIN #SUGESTAO_COMPRA_RESULTADO_PARCIAL            C ON A.SUGESTAO_COMPRA_RESULTADO = C.SUGESTAO_COMPRA_RESULTADO      
       
  WHERE C.SUGESTAO_COMPRA_RESULTADO IS NULL       
        
END      
      
------------------------------------------------      
-- GERA��O DO RESULTADO ANAL�TICO DA SUGEST�O --      
------------------------------------------------      
      
INSERT INTO SUGESTOES_COMPRAS_RESULTADO (       
            SUGESTAO_COMPRA ,       
            EMPRESA ,      
            FILIAL ,       
            PRODUTO ,       
            PRODUTO_VISIVEL,      
            DESCRICAO_VISIVEL,      
            DESCRICAO_MARCA,      
            MES_1 ,       
            MES_2 ,       
            MES_3 ,       
   MES_4 ,       
            CURVA ,      
            DIAS_ESTOQUE ,       
            DEMANDA_DIA ,       
     DEMANDA_DIA_PONDERADA ,       
            ESTOQUE_SALDO ,       
            ESTOQUE_LOJAS,      
            ESTOQUE_CD,      
            PEDIDOS_PENDENTES ,       
            TRANSF_PENDENTE_SAIDA,      
            TRANSF_PENDENTE_ENTRADA   ,      
            ESTOQUE_SUGERIDO ,      
            FATOR_EMBALAGEM ,       
            EMBALAGEM_INDUSTRIA ,       
            ULTIMA_COMPRA ,       
            ENCOMENDA,      
            COMPRA_SUGERIDA ,       
            COMPRA_AUTORIZADA ,      
            PRECO_COMPRA_BRUTO ,       
            DESCONTO_COMPRA ,       
            PRECO_COMPRA ,       
            PRECO_MAXIMO ,       
            DESCONTO_VENDA ,       
            PRECO_VENDA ,       
            TOTAL_SUGESTAO ,      
            TOTAL_AUTORIZADO ,       
            CUSTO_ULTIMA_COMPRA ,       
            PRECO_FABRICA,      
            ESTOQUE_MINIMO ,      
            VALIDADE_MINIMO ,       
            ESTOQUE_MAXIMO,      
            ESTOQUE_SEGURANCA,      
            ESTOQUE_REDE ,      
            NECESSIDADE_LOJA,      
            ULTIMO_PEDIDO_COMPRA  ,      
            ULTIMO_FORNECEDOR     ,      
            ULTIMA_QTDE_COMPRA    ,      
            ULTIMO_VALOR_COMPRA   ,      
            ULTIMO_DESCONTO_COMPRA,      
            PRODUTO_DEMANDA,      
            EXCESSO_LOJA_INTEGRADO,      
            NECESSIDADE_LOJA_INTEGRADO,      
            ULTIMA_NF_COMPRA,      
            CENTRO_ESTOQUE,      
            ESTOQUE_EXCESSO,       
   A.FALTEIRO,      
   A.QTD_FALTEIRO,      
   MARCA_DESCRICAO,      
   FANTASIA_DESCRICAO    )      
      
     SELECT A.SUGESTAO_COMPRA ,       
            A.EMPRESA ,      
            A.FILIAL ,       
            A.PRODUTO ,       
            A.PRODUTO_VISIVEL,      
            A.DESCRICAO_VISIVEL,      
            A.DESCRICAO_MARCA,      
            A.MES_1 ,       
            A.MES_2 ,       
            A.MES_3 ,       
            A.MES_4 ,       
            A.CURVA ,      
            A.DIAS_ESTOQUE ,       
            A.DEMANDA_DIA ,       
            A.DEMANDA_DIA_PONDERADA ,       
            A.ESTOQUE_SALDO ,       
            A.ESTOQUE_LOJAS,      
            A.ESTOQUE_CD,      
            A.PEDIDOS_PENDENTES ,       
            A.TRANSF_PENDENTE_SAIDA,      
            A.TRANSF_PENDENTE_ENTRADA   ,      
            A.ESTOQUE_SUGERIDO ,      
            A.FATOR_EMBALAGEM ,       
            A.EMBALAGEM_INDUSTRIA ,       
            A.ULTIMA_COMPRA ,       
            A.ENCOMENDA,      
            A.COMPRA_SUGERIDA ,       
            A.COMPRA_AUTORIZADA ,      
            A.PRECO_COMPRA_BRUTO ,       
            A.DESCONTO_COMPRA ,       
            A.PRECO_COMPRA ,       
            A.PRECO_MAXIMO ,       
            A.DESCONTO_VENDA ,       
            A.PRECO_VENDA ,       
                  
   A.COMPRA_SUGERIDA   * A.PRECO_COMPRA   AS TOTAL_SUGESTAO,      
   A.COMPRA_AUTORIZADA * A.PRECO_COMPRA   AS TOTAL_AUTORIZADO ,      
          
            A.CUSTO_ULTIMA_COMPRA ,       
            A.PRECO_FABRICA,      
            A.ESTOQUE_MINIMO ,      
            A.VALIDADE_MINIMO ,       
            A.ESTOQUE_MAXIMO,      
            A.ESTOQUE_SEGURANCA,      
            A.ESTOQUE_REDE ,      
            A.NECESSIDADE_LOJA,      
            A.ULTIMO_PEDIDO_COMPRA  ,      
            A.ULTIMO_FORNECEDOR     ,      
            A.ULTIMA_QTDE_COMPRA    ,      
            A.ULTIMO_VALOR_COMPRA   ,      
            A.ULTIMO_DESCONTO_COMPRA,      
            A.PRODUTO_DEMANDA,      
            A.EXCESSO_LOJA_INTEGRADO,      
            A.NECESSIDADE_LOJA_INTEGRADO,      
            A.ULTIMA_NF_COMPRA,      
            A.CENTRO_ESTOQUE,      
            A.ESTOQUE_EXCESSO ,       
   A.FALTEIRO,      
   A.QTD_FALTEIRO ,      
   LEFT(C.DESCRICAO,60)     AS MARCA_DESCRICAO,      
   LEFT(D.NOME_FANTASIA,30) AS FANTASIA_DESCRICAO      
          
    FROM #RESULTADO             A       
 LEFT JOIN PRODUTOS          B WITH(NOLOCK) ON B.PRODUTO         = A.PRODUTO      
 LEFT JOIN MARCAS            C WITH(NOLOCK) ON C.MARCA           = B.MARCA      
    LEFT JOIN EMPRESAS_USUARIAS D WITH(NOLOCK) ON D.EMPRESA_USUARIA = A.EMPRESA      
          
   WHERE ( ( @TIPO_RELATORIO = 1 ) OR       
           ( @TIPO_RELATORIO = 2 AND A.COMPRA_SUGERIDA > 0 ) )       
                   
     AND ( ( @ESTOQUE_ZERADO = 'N') OR       
           ( @ESTOQUE_ZERADO = 'S' AND A.ESTOQUE_SALDO = 0 ) )      
      
  --AND A.PRODUTO = 69912    
                                                                      
ORDER BY DESCRICAO_VISIVEL, EMPRESA                 
      
    
    
    
--END --FIM PROCESSAR      
      
------------------------------------------------      
--GARANTE A GERACAO DOS TOTAIS      
------------------------------------------------      
      
if object_id('tempdb..#SUGESTOES_COMPRAS_RESULTADO_TOTAIS') is not null DROP TABLE #SUGESTOES_COMPRAS_RESULTADO_TOTAIS      
      
 SELECT A.SUGESTAO_COMPRA_RESULTADO,      
                SUM(A.COMPRA_AUTORIZADA * A.PRECO_COMPRA) AS COMPRA_AUTORIZADA_VALOR,      
                SUM(A.COMPRA_SUGERIDA   * A.PRECO_COMPRA) AS TOTAL_SUGESTAO      
               
           INTO  #SUGESTOES_COMPRAS_RESULTADO_TOTAIS      
               
           FROM SUGESTOES_COMPRAS_RESULTADO A WITH(NOLOCK)      
          WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
       
       GROUP BY A.SUGESTAO_COMPRA_RESULTADO      
      
      
--ATUALIZA OS TOTAIS DOS ITENS--      
UPDATE SUGESTOES_COMPRAS_RESULTADO      
      
   SET TOTAL_AUTORIZADO        = X.COMPRA_AUTORIZADA_VALOR,      
       TOTAL_SUGESTAO          = X.TOTAL_SUGESTAO      
      
  FROM SUGESTOES_COMPRAS_RESULTADO A WITH(NOLOCK)      
  JOIN #SUGESTOES_COMPRAS_RESULTADO_TOTAIS X ON X.SUGESTAO_COMPRA_RESULTADO = A.SUGESTAO_COMPRA_RESULTADO       
      
--END      
      
if object_id('tempdb..#TEMP_TOTAIS') is not null DROP TABLE #TEMP_TOTAIS      
      
SELECT SUGESTAO_COMPRA_TOTAL            
      
  INTO #TEMP_TOTAIS      
      
 FROM SUGESTOES_COMPRAS_TOTAIS WITH(NOLOCK)            
WHERE SUGESTAO_COMPRA = @SUGESTAO_COMPRA      
      
      
--GERAR TOTAIS--          
DELETE SUGESTOES_COMPRAS_TOTAIS            
  FROM SUGESTOES_COMPRAS_TOTAIS  A WITH(NOLOCK)            
 JOIN  #TEMP_TOTAIS              B ON B.SUGESTAO_COMPRA_TOTAL = A.SUGESTAO_COMPRA_TOTAL             
            
                  
INSERT INTO SUGESTOES_COMPRAS_TOTAIS (SUGESTAO_COMPRA,            
                                      COMPRA_AUTORIZADA,            
                                      COMPRA_AUTORIZADA_VALOR)            
            
SELECT A.SUGESTAO_COMPRA              AS SUGESTAO_COMPRA ,            
       SUM(A.COMPRA_AUTORIZADA)       AS COMPRA_AUTORIZADA,            
       SUM(A.TOTAL_AUTORIZADO )       AS COMPRA_AUTORIZADA_VALOR            
  FROM SUGESTOES_COMPRAS_RESULTADO A WITH(NOLOCK)            
 WHERE A.SUGESTAO_COMPRA = @SUGESTAO_COMPRA            
GROUP BY A.SUGESTAO_COMPRA 