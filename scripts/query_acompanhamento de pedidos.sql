-------------------------------------------------------------------
--         Consulta de acompanhamento de Pedidos                 --
--         Desenvolvida por Yno� Pedro                           --
--         21/08/2018                                            --
-------------------------------------------------------------------
DECLARE @PEDIDO_VENDA NUMERIC

--SET @PEDIDO_VENDA = 25778

IF ISNUMERIC(:PEDIDO_VENDA) = 1 
	SET @PEDIDO_VENDA = :PEDIDO_VENDA
	ELSE
	SET @PEDIDO_VENDA    = NULL


SELECT 
	   A.PEDIDO_VENDA                  AS PEDIDO_VENDA
	  ,A.EMPRESA                       AS COD_EMPRESA
	  ,D.NOME                          AS EMPRESA
	  ,A.ENTIDADE                      AS COD_CLIENTE
	  ,C.NOME                          AS CLIENTE
      ,B.DESCRICAO                     AS STATUS_PEDIDO



FROM PEDIDOS_VENDAS                    A WITH(NOLOCK)
JOIN PEDIDOS_VENDAS_STATUS             B WITH(NOLOCK) ON A.PEDIDO_VENDA_STATUS = B.PEDIDO_VENDA_STATUS
LEFT 
JOIN ENTIDADES                         C WITH(NOLOCK) ON A.ENTIDADE            = C.ENTIDADE
LEFT
JOIN EMPRESAS_USUARIAS                 D WITH(NOLOCK) ON A.EMPRESA             = D.EMPRESA_USUARIA 


WHERE 1=1
  AND (A.PEDIDO_VENDA = @PEDIDO_VENDA OR @PEDIDO_VENDA IS NULL)                                                      


