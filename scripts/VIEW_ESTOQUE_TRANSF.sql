--SELECT * FROM ESTOQUE_TRANSFERENCIAS_RASTREABILIDADE

--sp_helptext ESTOQUE_TRANSFERENCIAS_RASTREABILIDADE

ALTER VIEW [dbo].[ESTOQUE_TRANSFERENCIAS_RASTREABILIDADE]     

    
AS      
    
-------------------------------------------------    
--QUANTIDADE ORIGINAL DO ROMANEIO    
-------------------------------------------------    
    
SELECT X.ESTOQUE_TRANSFERENCIA,    
       X.CENTRO_ESTOQUE_ORIGEM,    
       X.CENTRO_ESTOQUE_DESTINO,    
       X.MOVIMENTO,    
       X.PRODUTO,    
       MAX(X.NF_FATURAMENTO) AS NF_FATURAMENTO,    
       MAX(X.ESTOQUE_RECEBIMENTO) AS ESTOQUE_RECEBIMENTO,    
       SUM(X.QTDE_ROM   ) AS QTDE_ROM,    
       SUM(X.QTDE_CONF  ) AS QTDE_CONF,    
       SUM(X.QTDE_NF    ) AS QTDE_NF,    
       SUM(X.QTDE_RECB  ) AS QTDE_RECB,    
           
       CASE WHEN SUM(X.QTDE_NF    ) > 0     
            THEN 0    
            ELSE CASE WHEN SUM(X.QTDE_CONF  ) > 0     
                      THEN SUM(X.QTDE_CONF  )    
                      ELSE SUM(X.QTDE_ROM   )    
                 END    
       END AS TRANSITO_ORIGEM,    
                
       CASE WHEN SUM(X.QTDE_RECB    ) > 0     
            THEN 0    
            ELSE CASE WHEN SUM(X.QTDE_NF  ) > 0     
                      THEN SUM(X.QTDE_NF  )    
                      ELSE CASE WHEN SUM(X.QTDE_CONF  ) > 0     
                                THEN SUM(X.QTDE_CONF  )     
                                ELSE SUM(X.QTDE_ROM   )    
                           END    
                 END     
                          
       END AS TRANSITO_DESTINO    
           
    
  FROM (   

       
---------------------------------------------------    
--QUANTIDADE ROMANEIO 
---------------------------------------------------     
    
SELECT A.ESTOQUE_TRANSFERENCIA,    
       A.CENTRO_ESTOQUE_DESTINO,    
       A.CENTRO_ESTOQUE_ORIGEM,    
       A.MOVIMENTO,    
       B.PRODUTO,    
       0                          AS NF_FATURAMENTO,    
       0                          AS ESTOQUE_RECEBIMENTO,    
       SUM(B.QUANTIDADE_UNITARIA) -    
       ISNULL(C.ACERTO_FALTA,0)   AS QTDE_ROM,    
       0                          AS QTDE_CONF,    
       0                          AS QTDE_NF,    
       0                          AS QTDE_RECB    
                               
  FROM ESTOQUE_TRANSFERENCIAS                  A WITH(NOLOCK)    
  JOIN ESTOQUE_TRANSFERENCIAS_PRODUTOS         B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
  LEFT JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES  C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
                                                             AND C.PRODUTO               = B.PRODUTO    
                                                             AND C.ACERTO_FALTA          > 0    
  LEFT JOIN NF_FATURAMENTO_TRANSF              D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
  --LEFT JOIN NF_FATURAMENTO_SEFAZ               E WITH(NOLOCK) ON E.NF_FATURAMENTO        = D.NF_FATURAMENTO    
  --                                                           AND E.STATUS LIKE '%Autorizado o uso da NF-e%'    
  LEFT JOIN NF_FATURAMENTO                     F WITH(NOLOCK) ON F.NF_FATURAMENTO        = D.NF_FATURAMENTO    
  LEFT JOIN DEPOSITO_FATURAMENTOS              G WITH(NOLOCK) ON G.DEPOSITO_FATURAMENTO  = F.DEPOSITO_FATURAMENTO    
WHERE 1 = 1    
   AND A.CANCELADO             <> 'S'    
   --AND G.DEPOSITO_FATURAMENTO IS NOT NULL    
     
GROUP BY A.ESTOQUE_TRANSFERENCIA,    
         A.CENTRO_ESTOQUE_DESTINO,    
         A.CENTRO_ESTOQUE_ORIGEM,    
         A.MOVIMENTO,    
         B.PRODUTO,    
         ISNULL(C.ACERTO_FALTA,0)    
     
HAVING SUM(B.QUANTIDADE_UNITARIA) - ISNULL(C.ACERTO_FALTA,0) > 0    
    
   UNION ALL    
       
--CONFERENCIAS_ESTOQUE_TRANF
---------------------------------------------------    
--QUANTIDADE CHECKOUT DO ROMANEIO    
---------------------------------------------------    
    
SELECT A.ESTOQUE_TRANSFERENCIA,    
       A.CENTRO_ESTOQUE_DESTINO,    
       A.CENTRO_ESTOQUE_ORIGEM,    
       A.MOVIMENTO,    
       B.PRODUTO,           
       0                         AS NF_FATURAMENTO,      
       0                         AS ESTOQUE_RECEBIMENTO,                
       0                         AS QTDE_ROM,    
       B.CONFERENCIA             AS QTDE_CONF,    
       CASE WHEN C.EMPRESA = D.EMPRESA    
            THEN B.CONFERENCIA    
            ELSE 0    
       END                       AS QTDE_NF,    
       0  AS QTDE_RECB        
        
  FROM ESTOQUE_TRANSFERENCIAS               A WITH(NOLOCK)    
  JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES    B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
  JOIN CENTROS_ESTOQUE                      C WITH(NOLOCK) ON C.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_ORIGEM    
  JOIN CENTROS_ESTOQUE                      D WITH(NOLOCK) ON D.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_DESTINO    
       
 WHERE 1 = 1     
   AND A.CANCELADO             <> 'S'       
   AND B.CONFERENCIA > 0      
       
   UNION ALL    
       
---------------------------------------------------    
--QUANTIDADE DA NOTA OK--    
---------------------------------------------------    
    
SELECT A.ESTOQUE_TRANSFERENCIA,    
       A.CENTRO_ESTOQUE_DESTINO,    
       A.CENTRO_ESTOQUE_ORIGEM,    
       A.MOVIMENTO,           
       B.PRODUTO,           
       B.NF_FATURAMENTO,    
       0                         AS ESTOQUE_RECEBIMENTO,           
       0                         AS QTDE_ROM,    
       0                         AS QTDE_CONF,    
       B.QUANTIDADE_ESTOQUE      AS QTDE_NF,    
       0                         AS QTDE_RECB    
                                
  FROM ESTOQUE_TRANSFERENCIAS               A WITH(NOLOCK)    
  JOIN NF_FATURAMENTO_PRODUTOS              B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
  JOIN CENTROS_ESTOQUE                      C WITH(NOLOCK) ON C.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_ORIGEM    
  JOIN CENTROS_ESTOQUE                      D WITH(NOLOCK) ON D.OBJETO_CONTROLE       = A.CENTRO_ESTOQUE_DESTINO    
    
  LEFT JOIN CANCELAMENTOS_NOTAS_FISCAIS     X WITH(NOLOCK) ON X.CHAVE = B.NF_FATURAMENTO    
                                                          AND X.TIPO  = 1       
    
 WHERE 1 = 1     
   AND A.CANCELADO             <> 'S'        
   AND X.NF_CANCELAMENTO IS NULL    
   AND C.EMPRESA <> D.EMPRESA  --TEM NOTA APENAS QUANDO S�O EMPRESAS DIFERENTES-- GEMADA 09/02/2014--    
       
   UNION ALL    
       
-----------------------------------------------------    
----QUANTIDADE RECEBIDA NO DESTINO COM ORIGEM DE NOTA    
-----------------------------------------------------    
    
--SELECT A.ESTOQUE_TRANSFERENCIA,    
--       A.CENTRO_ESTOQUE_DESTINO,    
--       A.CENTRO_ESTOQUE_ORIGEM,    
--       A.MOVIMENTO,           
--       B.PRODUTO,           
--       0                     AS NF_FATURAMENTO,       
--       Y.ESTOQUE_RECEBIMENTO AS ESTOQUE_RECEBIMENTO,        
--       0                     AS QTDE_ROM,    
--       0                     AS QTDE_CONF,    
--       0                     AS QTDE_NF,    
--       B.QUANTIDADE_ESTOQUE  AS QTDE_RECB    
                                
--  FROM ESTOQUE_TRANSFERENCIAS                   A WITH(NOLOCK)    
--  JOIN NF_FATURAMENTO_PRODUTOS                  B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
--  LEFT JOIN ESTOQUE_TRANSFERENCIAS_RECEBIMENTOS Y WITH(NOLOCK) ON Y.NF_FATURAMENTO        = B.NF_FATURAMENTO    
--  LEFT JOIN CANCELAMENTOS_NOTAS_FISCAIS         X WITH(NOLOCK) ON X.CHAVE                 = B.NF_FATURAMENTO    
--                                                              AND X.TIPO                  = 1       
-- WHERE 1 = 1    
--   AND A.CANCELADO             <> 'S'        
--   AND X.NF_CANCELAMENTO IS NULL      
          
--   UNION ALL    
       
-----------------------------------------------------    
----QUANTIDADE RECEBIDA NO DESTINO MOV INTERNA--    
-----------------------------------------------------    
    
SELECT A.ESTOQUE_TRANSFERENCIA,    
       A.CENTRO_ESTOQUE_DESTINO,    
       A.CENTRO_ESTOQUE_ORIGEM,    
       A.MOVIMENTO,        
       B.PRODUTO,              
       0                         AS NF_FATURAMENTO,          
       F.ESTOQUE_RECEBIMENTO     AS ESTOQUE_RECEBIMENTO,                   
       0                         AS QTDE_ROM,    
       0                         AS QTDE_CONF,    
       0                         AS QTDE_NF,    
       B.CONFERENCIA             AS QTDE_RECB    
                                
  FROM ESTOQUE_TRANSFERENCIAS                    A WITH(NOLOCK)    
  JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES         B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
  LEFT 
  JOIN NF_FATURAMENTO_TRANSF                     C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA
  LEFT
  JOIN NF_FATURAMENTO                            D WITH(NOLOCK) ON D.NF_FATURAMENTO        = C.NF_FATURAMENTO 
  LEFT 
  JOIN NFE_CABECALHO                             E WITH(NOLOCK) ON E.XML_nNF_B08    = D.NF_NUMERO      
															   AND E.XML_serie_B07  = D.NF_SERIE       
															   AND E.EMPRESA        = D.EMPRESA  

  LEFT 
  JOIN ESTOQUE_TRANSFERENCIAS_RECEBIMENTOS      F WITH(NOLOCK) ON F.ID_NOTA = E.CHAVE_NFE
    
 WHERE 1 = 1    
   AND A.CANCELADO             <> 'S'        
   AND B.CONFERENCIA > 0   

  --UNION ALL    
     
------------------------------------------------------    
--QUANTIDADE RECEBIDA PELA NF DE COMPRA ENRE EMPESAS--    
------------------------------------------------------    
    
--SELECT A.ESTOQUE_TRANSFERENCIA,    
--       A.CENTRO_ESTOQUE_DESTINO,    
--       A.CENTRO_ESTOQUE_ORIGEM,    
--       A.MOVIMENTO,        
--       B.PRODUTO,              
--       0                         AS NF_FATURAMENTO,          
--       D.NF_COMPRA               AS ESTOQUE_RECEBIMENTO,                   
--       0                         AS QTDE_ROM,    
--       0                         AS QTDE_CONF,    
--       0                         AS QTDE_NF,    
--       B.CONFERENCIA             AS QTDE_RECB    
                                
--  FROM ESTOQUE_TRANSFERENCIAS                 A WITH(NOLOCK)    
--  JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES      B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA    
--  JOIN DEPOSITO_FATURAMENTOS_ITENS            X WITH(NOLOCK) ON X.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA    
--  JOIN NF_FATURAMENTO                         C WITH(NOLOCK) ON C.DEPOSITO_FATURAMENTO   = X.DEPOSITO_FATURAMENTO    
--  JOIN NF_COMPRA                              D WITH(NOLOCK) ON D.NF_NUMERO              = C.NF_NUMERO    
--                                                            AND D.NF_SERIE               = C.NF_SERIE    
--                                                            AND D.ENTIDADE               = C.EMPRESA    
--  JOIN RECEBIMENTOS_VOLUMES                   E WITH(NOLOCK) ON E.RECEBIMENTO            = D.RECEBIMENTO    
--  JOIN RECEBIMENTOS_FISICOS_LANCAMENTOS       F WITH(NOLOCK) ON F.RECEBIMENTO            = E.RECEBIMENTO    
    
-- WHERE 1 = 1    
--   AND A.CANCELADO <> 'S'        
--   AND B.CONFERENCIA > 0     
-- SELECT A.ESTOQUE_TRANSFERENCIA,    
--       A.CENTRO_ESTOQUE_DESTINO,    
--       A.CENTRO_ESTOQUE_ORIGEM,    
--       A.MOVIMENTO,        
--       B.PRODUTO,              
--       0                         AS NF_FATURAMENTO,          
--       0                         AS ESTOQUE_RECEBIMENTO,                   
--       0                         AS QTDE_ROM,    
--       0                         AS QTDE_CONF,    
--       0                         AS QTDE_NF,    
--       B.CONFERENCIA             AS QTDE_RECB    
                                
--  FROM ESTOQUE_TRANSFERENCIAS                 A WITH(NOLOCK)    
--  JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES      B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA    
--  JOIN DEPOSITO_FATURAMENTOS_ITENS            X WITH(NOLOCK) ON X.ESTOQUE_TRANSFERENCIA  = A.ESTOQUE_TRANSFERENCIA    
--  JOIN NF_FATURAMENTO                         C WITH(NOLOCK) ON C.DEPOSITO_FATURAMENTO   = X.DEPOSITO_FATURAMENTO  
--  JOIN NFE_CABECALHO                          D WITH(NOLOCK) ON D.XML_nNF_B08            = C.NF_NUMERO
--															AND D.XML_serie_B07			 = C.NF_SERIE 
--															AND D.EMPRESA 				 = C.EMPRESA  

--  JOIN ESTOQUE_TRANSFERENCIAS_RECEBIMENTOS    E WITH(NOLOCK) ON  D.CHAVE_NFE = E.ID_NOTA
  
  
  
  
-- WHERE 1 = 1    
--   AND A.CANCELADO <> 'S'        
--   AND B.CONFERENCIA > 0 
   
--GROUP BY
--A.ESTOQUE_TRANSFERENCIA, 
--A.CENTRO_ESTOQUE_DESTINO,
--A.CENTRO_ESTOQUE_ORIGEM, 
--A.MOVIMENTO,        
--B.PRODUTO,        
--B.CONFERENCIA      
    
---------------------------------------------------------    
--QUANTIDADE RECEBIDA COM DEPOSITO FATURAMENTO INV�LIDO--    
---------------------------------------------------------    
    
--SELECT A.ESTOQUE_TRANSFERENCIA,    
--       A.CENTRO_ESTOQUE_DESTINO,    
--       A.CENTRO_ESTOQUE_ORIGEM,    
--       A.MOVIMENTO,    
--       B.PRODUTO,    
--       0                          AS NF_FATURAMENTO,    
--       0                          AS ESTOQUE_RECEBIMENTO,    
--       SUM(B.QUANTIDADE_UNITARIA) -    
--       ISNULL(C.ACERTO_FALTA,0)   AS QTDE_ROM,    
--       0                          AS QTDE_CONF,    
--       0                          AS QTDE_NF,    
--       SUM(ISNULL(H.QUANTIDADE_ESTOQUE, (B.QUANTIDADE_UNITARIA) - ISNULL(C.ACERTO_FALTA,0)))  AS QTDE_RECB    
                               
--  FROM ESTOQUE_TRANSFERENCIAS                  A WITH(NOLOCK)    
--  JOIN ESTOQUE_TRANSFERENCIAS_PRODUTOS         B WITH(NOLOCK) ON B.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
--  LEFT JOIN ESTOQUE_TRANSFERENCIAS_TRANSACOES  C WITH(NOLOCK) ON C.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
--                                                             AND C.PRODUTO               = B.PRODUTO    
--                                                             AND C.ACERTO_FALTA          > 0    
--  LEFT JOIN NF_FATURAMENTO_TRANSF              D WITH(NOLOCK) ON D.ESTOQUE_TRANSFERENCIA = A.ESTOQUE_TRANSFERENCIA    
--  --LEFT JOIN NF_FATURAMENTO_SEFAZ               E WITH(NOLOCK) ON E.NF_FATURAMENTO        = D.NF_FATURAMENTO    
--  --                                                           AND E.STATUS LIKE '%Autorizado o uso da NF-e%'    
--  LEFT JOIN NF_FATURAMENTO                     F WITH(NOLOCK) ON F.NF_FATURAMENTO        = D.NF_FATURAMENTO    
--  LEFT JOIN DEPOSITO_FATURAMENTOS              G WITH(NOLOCK) ON G.DEPOSITO_FATURAMENTO  = F.DEPOSITO_FATURAMENTO    
--  LEFT JOIN NF_FATURAMENTO_PRODUTOS            H WITH(NOLOCK) ON H.NF_FATURAMENTO        = F.NF_FATURAMENTO     
--                                                             AND H.PRODUTO               = B.PRODUTO    
--WHERE 1 = 1    
--   AND A.CANCELADO <> 'S'    
--   AND F.DEPOSITO_FATURAMENTO IS NULL     
      
--GROUP BY A.ESTOQUE_TRANSFERENCIA,    
--   A.CENTRO_ESTOQUE_DESTINO,    
--         A.CENTRO_ESTOQUE_ORIGEM,    
--         A.MOVIMENTO,    
--         B.PRODUTO,    
--         ISNULL(C.ACERTO_FALTA,0)    
     
--HAVING SUM(B.QUANTIDADE_UNITARIA) - ISNULL(C.ACERTO_FALTA,0) > 0    
     
) X    
    
   GROUP BY  X.ESTOQUE_TRANSFERENCIA,    
             X.CENTRO_ESTOQUE_ORIGEM,    
             X.CENTRO_ESTOQUE_DESTINO,    
             X.MOVIMENTO,    
             X.PRODUTO    
    