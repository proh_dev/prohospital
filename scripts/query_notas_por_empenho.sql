/**************************************************/
--CONSULTA DETALHAMENTO DE EMPENHO               --
-- DESENVOLVIDA POR YNOA PEDRO 24/09/2018        --
/**************************************************/

DECLARE @EMPENHO VARCHAR(20)

SET @EMPENHO = :EMPENHO
--SET @EMPENHO = '1200'

--TITULOS_RECEBER_FICHA_FINANCEIRA
--769791 FORMULARIO_ORIGEM  
--753289 TAB_MASTER_ORIGEM  



--NF_FATURAMENTO_OBSERVACOES
--TITULOS_RECEBER
--666569 NUMID FORM
--139 TAB MASTER

SELECT  769791                              AS FORMULARIO_ORIGEM,
        753289                              AS TAB_MASTER_ORIGEM,
		B.NF_FATURAMENTO                    AS REG_MASTER_ORIGEM,
		B.NF_FATURAMENTO                    AS NF_FATURAMENTO,
		B.EMPRESA                           AS EMPRESA,
		B.ENTIDADE                          AS ENTIDADE,
		B.NF_NUMERO                         AS NF_NUMERO,
		C.EMPENHO                           AS EMPENHO,
		C.PREGAO                            AS PREGAO

 
FROM      TITULOS_RECEBER                    A WITH(NOLOCK)
     JOIN NF_FATURAMENTO                     B WITH(NOLOCK) ON B.TAB_MASTER_ORIGEM   = 753289
                                                           AND B.NF_FATURAMENTO      = A.REG_MASTER_ORIGEM
     JOIN NF_FATURAMENTO_OBSERVACOES         C WITH(NOLOCK) ON B.NF_FATURAMENTO      = C.NF_FATURAMENTO

	 WHERE C.EMPENHO LIKE @EMPENHO
