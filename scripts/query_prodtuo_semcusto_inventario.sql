---------------------------------------------------
--PRODUTOS SEM CUSTO                             --
--DESENVOLVIDA POR YNOA PEDRO                    --
--20/08/2018                                     --
---------------------------------------------------
DECLARE @INVETARIO NUMERIC

--SET @INVETARIO = :INVENTARIO_ESCOPO
SET @INVETARIO = 1401


SELECT B.PRODUTO              AS COD_PRODUTO
      ,B.DESCRICAO            AS PRODUTO
	  ,B.UNIDADE_MEDIDA       AS UND
	  ,B.FAMILIA_PRODUTO      AS FAMILIA
	  ,B.GRUPO_PRODUTO        AS GRUPO
	  ,C.DESCRICAO            AS MARCA
	  ,B.CUSTO_BASE           AS CUSTO_ATUAL
	  
	   

	   
FROM INVENTARIOS_ESCOPOS            X WITH(NOLOCK)
JOIN INVENTARIOS_ESCOPOS_PRODUTOS   A WITH(NOLOCK) ON X.INVENTARIO_ESCOPO   = A.INVENTARIO_ESCOPO
JOIN PRODUTOS                       B WITH(NOLOCK) ON A.PRODUTO             = B.PRODUTO
LEFT		                        
JOIN MARCAS                         C WITH(NOLOCK) ON B.MARCA               = B.MARCA
LEFT				               
JOIN GRUPOS_PRODUTOS                D WITH(NOLOCK) ON B.GRUPO_PRODUTO       = D.GRUPO_PRODUTO
LEFT				               
JOIN FAMILIAS_PRODUTOS              E WITH(NOLOCK) ON B.FAMILIA_PRODUTO     = E.FAMILIA_PRODUTO
					               

WHERE 1=1
  AND A.INVENTARIO_ESCOPO = @INVETARIO
  AND (B.CUSTO_BASE = 0 OR B.CUSTO_BASE IS NULL)