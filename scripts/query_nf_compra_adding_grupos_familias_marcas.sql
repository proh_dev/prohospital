/* ====================================================== */
/* CONSULTA DE NOTAS FISCAIS COMPRAS                      */
/* AUTOR: ANDREI TOLEDO                                   */
/* DATA : 15-04-2015                                      */
/* ====================================================== */  
/* ====================================================== */
/* CONSULTA DE NOTAS FISCAIS COMPRAS                      */
/* VERS�O 2                                               */
/* AUTOR: YNOA PEDRO                                      */
/* DATA : 10-02-2018                                      */
/* ====================================================== */  
DECLARE @EMPRESA_INI        NUMERIC(15,0) 
DECLARE @EMPRESA_FIM        NUMERIC(15,0) 
DECLARE @ENTIDADE           NUMERIC(15,0) 
DECLARE @DATA_INICIAL       DATE             
DECLARE @DATA_FINAL         DATE
DECLARE @GRUPO_MARCA        NUMERIC(15,0)
DECLARE @FAMILIA_PRODUTO    NUMERIC(15,0)
DECLARE @NF_COMPRA          NUMERIC(15,0)
DECLARE @MARCA				NUMERIC(15,0)
DECLARE @SECAO				NUMERIC(15,0)
DECLARE @GRUPO_PRODUTO		NUMERIC(15,0)
DECLARE @SUB_GRUPO			NUMERIC(15,0)

--SET @EMPRESA_INI 	 = 1
--SET @EMPRESA_FIM 	 = 1000
--SET @ENTIDADE        = null   
--SET @DATA_INICIAL	 = '01/01/2018'
--SET @DATA_FINAL      = '31/01/2018'
--SET @GRUPO_MARCA     = NULL
--SET @FAMILIA_PRODUTO = NULL
--SET @NF_COMPRA       = NULL
--SET @MARCA			 = NULL
--SET @SECAO			 = NULL
--SET @GRUPO_PRODUTO	 = NULL
--SET @SUB_GRUPO       = NULL

SET @DATA_INICIAL         = :DATA_INICIAL
SET @DATA_FINAL           = :DATA_FINAL  

IF ISNUMERIC(:EMPRESA_INI) = 1         
    SET @EMPRESA_INI = :EMPRESA_INI              
  ELSE  
    SET @EMPRESA_INI = NULL       

IF ISNUMERIC(:EMPRESA_FIM) = 1 
    SET @EMPRESA_FIM = :EMPRESA_FIM
 ELSE  
    SET @EMPRESA_FIM = NULL

IF ISNUMERIC(:ENTIDADE) = 1 
    SET @ENTIDADE = :ENTIDADE
 ELSE  
    SET @ENTIDADE = NULL  

IF ISNUMERIC(:GRUPO_MARCA) = 1 
    SET @GRUPO_MARCA = :GRUPO_MARCA
 ELSE  
    SET @GRUPO_MARCA = NULL  

IF ISNUMERIC(:FAMILIA_PRODUTO) = 1 
    SET @FAMILIA_PRODUTO = :FAMILIA_PRODUTO
 ELSE  
    SET @FAMILIA_PRODUTO = NULL  

IF ISNUMERIC(:NF_COMPRA ) = 1 
    SET @NF_COMPRA  = :NF_COMPRA 
 ELSE  
    SET @NF_COMPRA  = NULL  

IF ISNUMERIC(:MARCA ) = 1 
    SET @MARCA  = :MARCA 
 ELSE  
    SET @MARCA  = NULL 

IF ISNUMERIC(:SECAO ) = 1 
    SET @SECAO  = :SECAO 
 ELSE  
    SET @SECAO  = NULL   

IF ISNUMERIC(:GRUPO_PRODUTO ) = 1 
    SET @GRUPO_PRODUTO  = :GRUPO_PRODUTO 
 ELSE  
    SET @GRUPO_PRODUTO  = NULL  

IF ISNUMERIC(:SUB_GRUPO ) = 1 
    SET @SUB_GRUPO  = :SUB_GRUPO 
 ELSE  
    SET @SUB_GRUPO  = NULL  



SELECT DISTINCT
    A.NF_COMPRA                                                                    AS REG_MASTER_ORIGEM
   ,A.FORMULARIO_ORIGEM       
   ,A.TAB_MASTER_ORIGEM

   ,A.MOVIMENTO                                                                     AS MOVIMENTO
   ,A.EMPRESA                                                                       AS EMPRESA
   ,A.ENTIDADE                                                                      AS ENTIDADE
   ,G.NOME                                                                          AS NOME_ENTIDADE
   ,A.NF_NUMERO                                                                     AS NF_NUMERO
   ,K.VALOR_PRODUTO_LIQUIDO                                                         AS TOTAL_VALOR_PRODUTO
   ,K.QUANTIDADE                                                                    AS QUANTIDADE_PRODUTO
   ,K.QUANTIDADE_ESTOQUE                                                            AS QUANTIDADE_ESTOQUE
   ,K.PRODUTO                                                                       AS PRODUTO
   ,D.DESCRICAO                                                                     AS DESCRICAO
   ,K.FRETE                                                                         AS FRETE
   ,B.CLASSIF_FISCAL_CODIGO                                                         AS NCM
   ,(K.VALOR_PRODUTO_LIQUIDO + ISNULL(K.FRETE,0)) * K.QUANTIDADE_ESTOQUE            AS TOTAL_FINAL       
   ,ISNULL(CASE WHEN ISNULL(E.ESTADO,'RJ') = J.ESTADO 
                   THEN CASE WHEN SUBSTRING(B.SITUACAO_TRIBUTARIA,2,2) IN ('10','60','30','70') 
                             THEN ISNULL(C.CFOP_INTERNA_ST, '0.000')
                             ELSE ISNULL(C.CFOP_INTERNA, '0.000') 
                        END
                   ELSE CASE WHEN SUBSTRING(B.SITUACAO_TRIBUTARIA,2,2) IN ('10','60','30','70')
                             THEN ISNULL(C.CFOP_INTERESTADUAL_ST,'0.000')                    
                             ELSE ISNULL(C.CFOP_INTERESTADUAL,'0.000') 
                        END 
              END,'9.999')                                                          AS CFOP
   ,B.SITUACAO_TRIBUTARIA                                                           AS CST
   ,B.ICMS_ALIQUOTA                                                                 AS ICMS_ALIQUOTA
   ,K.ICMS_VALOR                                                                    AS ICMS_VALOR -- VALOR_CREDITO
   ,Q.DESCRICAO                                                                     AS FAMILIA_PRODUTO
   ,L.DESCRICAO                                                                     AS MARCA
   ,S.DESCRICAO                                                                     AS GRUPO_MARCA
   ,M.DESCRICAO                                                                     AS SECAO
   ,N.DESCRICAO                                                                     AS GRUPO_PRODUTO
   ,O.DESCRICAO                                                                     AS SUB_GRUPO
      
   FROM NF_COMPRA                  A WITH(NOLOCK)
   JOIN NF_COMPRA_PRODUTOS         B WITH(NOLOCK)ON B.NF_COMPRA             = A.NF_COMPRA
   JOIN ESPELHO                    K WITH(NOLOCK)ON K.NF_COMPRA_PRODUTO     = B.NF_COMPRA_PRODUTO
   ---- opera��o fiscal / produto										    
   JOIN OPERACOES_FISCAIS          C WITH(NOLOCK)ON C.OPERACAO_FISCAL       = B.OPERACAO_FISCAL
   JOIN PRODUTOS                   D WITH(NOLOCK)ON D.PRODUTO               = K.PRODUTO
   ---- entidade														    
  LEFT JOIN ENDERECOS              E WITH(NOLOCK)ON E.ENTIDADE              = A.ENTIDADE
  LEFT JOIN ENTIDADES              G WITH(NOLOCK)ON G.ENTIDADE              = A.ENTIDADE
  LEFT JOIN ENTIDADES_FISCAL       P WITH(NOLOCK)ON P.ENTIDADE              = A.ENTIDADE
  LEFT JOIN SITUACOES_TRIBUTARIAS  Y WITH(NOLOCK)ON Y.SITUACAO_TRIBUTARIA   = B.SITUACAO_TRIBUTARIA
  ---- empresa usuaria													    
 INNER JOIN EMPRESAS_USUARIAS      I WITH(NOLOCK)ON I.EMPRESA_USUARIA       = A.EMPRESA
  LEFT JOIN ENDERECOS              J WITH(NOLOCK)ON J.ENTIDADE              = I.ENTIDADE
  ---- grupos de marcas e compras
   JOIN MARCAS                     L WITH(NOLOCK)ON L.MARCA                 = D.MARCA
   JOIN SECOES_PRODUTOS            M WITH(NOLOCK)ON M.SECAO_PRODUTO         = D.SECAO_PRODUTO
   JOIN GRUPOS_PRODUTOS            N WITH(NOLOCK)ON N.GRUPO_PRODUTO         = D.GRUPO_PRODUTO
   JOIN SUBGRUPOS_PRODUTOS         O WITH(NOLOCK)ON O.SUBGRUPO_PRODUTO      = D.SUBGRUPO_PRODUTO
   LEFT					      	   								    
   JOIN FAMILIAS_PRODUTOS          Q WITH(NOLOCK)ON Q.FAMILIA_PRODUTO       = D.FAMILIA_PRODUTO
   LEFT						      
   JOIN GRUPOS_MARCAS_DETALHE      R WITH(NOLOCK)ON R.MARCA                 = L.MARCA
   LEFT							   						           
   JOIN GRUPOS_MARCAS              S WITH(NOLOCK)ON S.GRUPO_MARCA           = R.GRUPO_MARCA



         
  WHERE 1=1
        AND (ISNULL(A.PROCESSAR,'N')                    = 'S'                                          )
        AND (A.MOVIMENTO                               >=  @DATA_INICIAL                               )  
        AND (A.MOVIMENTO                               <=  @DATA_FINAL                                 )  
        AND (A.EMPRESA                                 >=  @EMPRESA_INI     OR @EMPRESA_INI     IS NULL) 
        AND (A.EMPRESA                                 <=  @EMPRESA_FIM     OR @EMPRESA_FIM     IS NULL)  
        AND (A.ENTIDADE                                 =  @ENTIDADE        OR @ENTIDADE        IS NULL)  
		AND (D.GRUPO_PRODUTO                            =  @GRUPO_MARCA     OR @GRUPO_MARCA     IS NULL)
		AND (D.FAMILIA_PRODUTO                          =  @FAMILIA_PRODUTO OR @FAMILIA_PRODUTO IS NULL)
		AND (A.NF_COMPRA                                =  @NF_COMPRA       OR @NF_COMPRA       IS NULL)
    

                                 
ORDER BY A.MOVIMENTO      