  DECLARE @DATA_INI DATE
  DECLARE @DATA_FIM DATE
  DECLARE @EMPRESA NUMERIC

 SET @DATA_INI = :DATA_INICIAL
 SET @DATA_FIM = :DATA_FINAL
 IF ISNUMERIC(:EMPRESA)= 1 SET @EMPRESA  = :EMPRESA  ELSE SET @EMPRESA  = NULL
  
   --SET @DATA_INI = '01/01/2018'
   --SET @DATA_FIM = '31/01/2018'
   --SET @EMPRESA  = 5

  
   SELECT A.LOJA             AS EMPRESA , 
          C.EMPRESA_CONTABIL AS REDE ,
          D.NOME_FANTASIA    AS NOME_REDE , 
          C.FILIAL           AS LOJA ,  
          A.ECF_CAIXA        AS CAIXA ,
          A.ECF_CUPOM        AS CUPOM ,                                 
          A.MOVIMENTO        AS MOVIMENTO,            
          F.NOME             AS OPERADOR , 
          E.DESCRICAO        AS DESCRICAO ,
          B.VALOR            AS VALOR ,
          B.TROCO            AS TROCO ,
          B.VALOR - B.TROCO  AS VALOR_LIQUIDO ,
          B.PARCELAS         AS PARCELAS , 
          B.BANDEIRA         AS BANDEIRA , 
          G.DESCRICAO        AS DESCR_BANDEIRA,
          A.CLIENTE,
          I.NOME
     FROM PDV_VENDAS          A WITH(NOLOCK)  
     JOIN PDV_FINALIZADORAS   B WITH(NOLOCK) ON B.MOVIMENTO       = A.MOVIMENTO 
                                            AND B.VENDA           = A.VENDA 
                                            AND B.LOJA            = A.LOJA 
                                            AND B.CAIXA           = A.CAIXA
     JOIN EMPRESAS_USUARIAS   C WITH(NOLOCK) ON C.EMPRESA_USUARIA = A.LOJA
     JOIN EMPRESAS_USUARIAS   D WITH(NOLOCK) ON D.EMPRESA_USUARIA = C.EMPRESA_CONTABIL
     JOIN TIPOS_FINALIZADORAS E WITH(NOLOCK) ON E.TIPO            = B.TIPO
LEFT JOIN VENDEDORES          F WITH(NOLOCK) ON F.VENDEDOR        = A.OPERADOR
LEFT JOIN BANDEIRAS           G WITH(NOLOCK) ON G.CODIGO          = B.BANDEIRA
LEFT JOIN PDV_FINALIZADORAS   H WITH(NOLOCK) ON H.MOVIMENTO       = B.MOVIMENTO 
                                            AND H.VENDA           = B.VENDA 
                                            AND H.LOJA            = B.LOJA 
                                            AND H.CAIXA           = B.CAIXA
                                            AND H.ECF_CUPOM       = B.ECF_CUPOM
                                            AND H.STATUS = 'C'   
LEFT JOIN ENTIDADES           I WITH(NOLOCK) ON I.ENTIDADE        = A.CLIENTE                                            
    WHERE A.MOVIMENTO >=  @DATA_INI
      AND A.MOVIMENTO <=  @DATA_FIM
      AND H.REGISTRO IS NULL       
	  AND (A.EMPRESA = @EMPRESA OR  @EMPRESA IS NULL)
ORDER BY A.LOJA , C.FILIAL , A.ECF_CAIXA , A.ECF_CUPOM  