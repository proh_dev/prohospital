--------------DESENVOLVIMENTO------------------------------
--            DECLARA��O DAS VARIAVEIS LOCAIS            --
-----------------------------------------------------------

DECLARE @NF_COMPRA			NUMERIC(15)
DECLARE @EMPRESA			NUMERIC(15)
DECLARE @RECEBIMENTO			NUMERIC(15)
DECLARE @PROCESSAR			VARCHAR(01)
DECLARE @PEDIDO_COMPRA			NUMERIC(15)
DECLARE @NOTAS_DEBITO_NF_COMPRA		NUMERIC(15)
DECLARE @ENCERRAR_PEDIDO_TOTAL		VARCHAR(1)

DECLARE @FORMULARIO_ORIGEM		NUMERIC(15)
DECLARE @TAB_MASTER_ORIGEM		NUMERIC(15)
DECLARE @EMITIR_NFE			VARCHAR(01)
DECLARE @AUTORIZACAO_NFE_NF_COMPRA	NUMERIC(15)
DECLARE @NF_NUMERO			NUMERIC(15)
DECLARE @NF_ESPECIE			VARCHAR(03)
DECLARE @NF_SERIE			VARCHAR(03)

DECLARE @GERACAO_NOTA_ENTRADA           NUMERIC(15)

SET @NF_COMPRA				= :NF_COMPRA
SET @RECEBIMENTO			= :RECEBIMENTO
SET @PEDIDO_COMPRA			= :PEDIDO_COMPRA
SET @PROCESSAR			        = :PROCESSAR
SET @ENCERRAR_PEDIDO_TOTAL		= :ENCERRAR_PEDIDO_TOTAL
SET @EMPRESA				= :EMPRESA

SET @FORMULARIO_ORIGEM			= :FORMULARIO_ORIGEM
SET @TAB_MASTER_ORIGEM			= :TAB_MASTER_ORIGEM
SET @EMITIR_NFE				= :EMITIR_NFE
SET @AUTORIZACAO_NFE_NF_COMPRA          = :AUTORIZACAO_NFE_NF_COMPRA
SET @NF_NUMERO				= :NF_NUMERO
SET @NF_ESPECIE				= :NF_ESPECIE
SET @NF_SERIE				= :NF_SERIE

SET @GERACAO_NOTA_ENTRADA               = :GERACAO_NOTA_ENTRADA

--ATUALIZA��O DO CODIGO CEST
EXEC USP_ATUALIZAR_CEST @NF_COMPRA,'NF_COMPRA'

--SET @NF_COMPRA         = 10
--SET @RECEBIMENTO       = 11
--SET @PEDIDO_COMPRA     = 14
--SET @PROCESSAR         = 'N'


---------------------------------
-- ROTINAS DE RATEIOS          --
---------------------------------

EXEC GERAR_RATEIO_FRETE_NF_COMPRA         @NF_COMPRA 
EXEC GERAR_RATEIO_DESPESAS_NF_COMPRA      @NF_COMPRA
EXEC GERAR_RATEIO_SEGURO_NF_COMPRA        @NF_COMPRA
EXEC GERAR_RATEIO_REPASSE_NF_COMPRA       @NF_COMPRA
EXEC GERAR_DADOS_CONHECIMENTO             @NF_COMPRA --GERAR DADOS DO CONHECIMENTO E RATEIO DO FRETE INFORMADO NO CONHECIMENTO

-- ** RATEIO DAS RETEN��ES

EXEC GERAR_RATEIO_IRRF_NF_COMPRA          @NF_COMPRA
EXEC GERAR_RATEIO_ISS_NF_COMPRA 		  @NF_COMPRA
EXEC GERAR_RATEIO_INSS_NF_COMPRA		  @NF_COMPRA
EXEC GERAR_RATEIO_PIS_NF_COMPRA 		  @NF_COMPRA
EXEC GERAR_RATEIO_COFINS_NF_COMPRA 		  @NF_COMPRA
EXEC GERAR_RATEIO_CSSL_NF_COMPRA 		  @NF_COMPRA
EXEC GERAR_RATEIO_10833_NF_COMPRA 		  @NF_COMPRA


                         --JAQUELINE RUFINO - 02/04/2015--
-----------------------------------------------------------------------------------------
-- ENCONTRA ALIQUOTA DE PIS COFINS NO GRUPO TRIBUTARIO DO PRODUTO                      --
-- BUSCANDO PELA OPERA��O FISCAL DE VENDA NO MESMO ESTADO DA EMPRESA USU�RIA           --
-- OU SEJA, ALIQUOTA DE PIS COFINS NA VENDA DENTRO DO ESTADO                           --
-- POR�M, SE A ALIQUOTA DA EMPRESA FOR DIFERENTE, ASSUME-A (REGIMES DIFERENCIADOS)     --
-- CASO A ALIQUOTA DO PRODUTO NO GT DE VENDA FOR ZERO, ASSUME ZERO PARA NF_COMPRA      --
-- ALBERTO 06/03/2015                                                                  --
--                                                                                     --
-- FOI ADICIONADO OS CAMPOS DE BASE DE CALCULO DE PIS E COFINS NO UPDATE PARA ZER�-LOS --
-- CASO A ALIQUOTA DE PIS/COFINS SEJAM ZERADAS.                                        --
-- JAQUELINE RUFINO - 06/04/2015                                                       --
-----------------------------------------------------------------------------------------

UPDATE NF_COMPRA_PRODUTOS SET
        
		PIS_BASE_CALCULO    = CASE WHEN ISNULL(G.ALIQUOTA_PIS,0) > 0 
								   THEN B.TOTAL_PRODUTO
								   ELSE 0
							   END,

		PIS_ALIQUOTA        = CASE WHEN ISNULL(G.ALIQUOTA_PIS,0) > 0 
						 	       THEN ISNULL(H.ALIQUOTA_PIS,G.ALIQUOTA_PIS)
								   ELSE ISNULL(G.ALIQUOTA_PIS,0)
							   END ,

		PIS_VALOR           = CONVERT(NUMERIC(15,2),( B.PIS_BASE_CALCULO * ( CASE WHEN ISNULL(G.ALIQUOTA_PIS,0) > 0 
																			      THEN ISNULL(H.ALIQUOTA_PIS,G.ALIQUOTA_PIS)
																			      ELSE ISNULL(G.ALIQUOTA_PIS,0)
																		      END / 100.00 ) ) ) ,

		COFINS_BASE_CALCULO = CASE WHEN ISNULL(G.ALIQUOTA_COFINS,0) > 0 
						  		   THEN B.TOTAL_PRODUTO
						 		   ELSE 0
							   END ,

		COFINS_ALIQUOTA     = CASE WHEN ISNULL(G.ALIQUOTA_COFINS,0) > 0 
								   THEN ISNULL(H.ALIQUOTA_COFINS,G.ALIQUOTA_COFINS)
								   ELSE ISNULL(G.ALIQUOTA_COFINS,0)
							   END ,

		COFINS_VALOR        = CONVERT(NUMERIC(15,2),( B.COFINS_BASE_CALCULO * ( CASE WHEN ISNULL(G.ALIQUOTA_COFINS,0) > 0 
																				     THEN ISNULL(H.ALIQUOTA_COFINS,G.ALIQUOTA_COFINS)
																				     ELSE ISNULL(G.ALIQUOTA_COFINS,0)
																			     END / 100.00 ) ) )

	FROM NF_COMPRA                          A WITH(NOLOCK)
	JOIN NF_COMPRA_PRODUTOS                 B WITH(NOLOCK) ON B.NF_COMPRA        = A.NF_COMPRA
	JOIN PRODUTOS                           C WITH(NOLOCK) ON C.PRODUTO          = B.PRODUTO
	JOIN PARAMETROS_VENDAS                  D WITH(NOLOCK) ON D.EMPRESA_USUARIA  = A.EMPRESA
	JOIN EMPRESAS_USUARIAS                  E WITH(NOLOCK) ON E.EMPRESA_USUARIA  = A.EMPRESA
	JOIN ENDERECOS                          F WITH(NOLOCK) ON F.ENTIDADE         = E.ENTIDADE         -- ENDERE�O DA EMPRESA USU�RIA
	LEFT JOIN GRUPOS_TRIBUTARIOS_PARAMETROS G WITH(NOLOCK) ON G.GRUPO_TRIBUTARIO = C.GRUPO_TRIBUTARIO
								  						  AND G.OPERACAO_FISCAL  = ISNULL(D.OPERACAO_FISCAL_ECF,D.OPERACAO_FISCAL)
													   	  AND G.ESTADO_ORIGEM    = F.ESTADO            
														  AND G.ESTADO_DESTINO   = F.ESTADO
	LEFT JOIN PARAMETROS_FISCAIS            H WITH(NOLOCK) ON H.EMPRESA_USUARIA  = A.EMPRESA

	WHERE A.NF_COMPRA = @NF_COMPRA


-----------------------------------------
-- S� ACIONAR ROTINAS SE PROCESSAR = S --
-----------------------------------------

IF @PROCESSAR = 'S'
BEGIN

    ------------------------------------------------------
     -- LIMPEZA DO FINANCEIRO AO RETIRAR AS PARCELAS     --
     ------------------------------------------------------

     DECLARE @CONT INT
      SELECT @CONT = COUNT(*) 
        FROM NF_COMPRA_PARCELAS WITH(NOLOCK)
       WHERE NF_COMPRA = @NF_COMPRA

      DELETE TITULOS_PAGAR
        FROM TITULOS_PAGAR A WITH(NOLOCK) 
		WHERE A.TITULO_PAGAR IN 
		(
		SELECT A.TITULO_PAGAR 
		FROM TITULOS_PAGAR A WITH(NOLOCK) , NF_COMPRA B WITH(NOLOCK)
       WHERE A.REG_MASTER_ORIGEM = B.NF_COMPRA
         AND A.TAB_MASTER_ORIGEM = B.TAB_MASTER_ORIGEM
         AND B.NF_COMPRA         = @NF_COMPRA
         AND @CONT               = 0
		)

     ----------------------------------------------------------
     --       ATUALIZA��O DA TABELA DE REFERENCIA NO         --
     --                  CADASTRO DE PRODUTO                 --
     ----------------------------------------------------------

     if object_id('tempdb..#REFERENCIA') is not null DROP TABLE #REFERENCIA

     SELECT A.PRODUTO             AS PRODUTO             , 
            B.ENTIDADE            AS ENTIDADE            , 
            MAX(A.VALOR_UNITARIO) AS VALOR_UNITARIO      ,
            A.DESCONTO            AS DESCONTO            ,      
            A.SITUACAO_TRIBUTARIA AS SITUACAO_TRIBUTARIA ,
            A.ICMS_ALIQUOTA       AS ICMS_ALIQUOTA       ,
            A.IPI_ALIQUOTA        AS IPI_ALIQUOTA        ,
            MAX(A.REFERENCIA)     AS REFERENCIA          ,
            MAX(A.DESCRICAO)      AS DESCRICAO_FORNECEDOR
       INTO #REFERENCIA
       FROM NF_COMPRA_PRODUTOS    A WITH(NOLOCK)
       JOIN NF_COMPRA             B WITH(NOLOCK) ON A.NF_COMPRA = B.NF_COMPRA
  LEFT JOIN PRODUTOS_FORNECEDORES C WITH(NOLOCK) ON C.PRODUTO   = A.PRODUTO
                                                AND C.ENTIDADE  = B.ENTIDADE
      WHERE A.NF_COMPRA = @NF_COMPRA
   GROUP BY A.PRODUTO , 
            B.ENTIDADE , 
            A.DESCONTO,
            A.ICMS_ALIQUOTA,
            A.IPI_ALIQUOTA,
            A.SITUACAO_TRIBUTARIA 

     -- ALTERA��O DE REFERENCIAS EXISTENTES --

     UPDATE PRODUTOS_FORNECEDORES
        SET ENTIDADE             = B.ENTIDADE        ,
            VALOR_UNITARIO       = B.VALOR_UNITARIO  ,
            ALIQUOTA_ICMS        = B.ICMS_ALIQUOTA   ,
            ALIQUOTA_IPI         = IPI_ALIQUOTA      ,
            SITUACAO_TRIBUTARIA  = B.SITUACAO_TRIBUTARIA,
            DESCRICAO_FORNECEDOR = B.DESCRICAO_FORNECEDOR
   
       FROM PRODUTOS_FORNECEDORES A WITH(NOLOCK) ,
            #REFERENCIA           B WITH(NOLOCK) 
      WHERE A.PRODUTO  = B.PRODUTO
        AND A.ENTIDADE = B.ENTIDADE

     -- INCLUS�O DE NOVAS REFERENCIAS --

     INSERT INTO PRODUTOS_FORNECEDORES ( PRODUTO , 
                                         ENTIDADE , 
                                         TIPO_FORNECEDOR ,  
                                         VALOR_UNITARIO , 
                                         PERCENTUAL_DESCONTO ,
                                         SITUACAO_TRIBUTARIA ,
                                         ALIQUOTA_ICMS , 
                                         ALIQUOTA_IPI , 
                                         REFERENCIA ,
                                         DESCRICAO_FORNECEDOR
                                         )
                                  SELECT A.PRODUTO ,
                                         A.ENTIDADE ,
                                         1 ,
                                         A.VALOR_UNITARIO ,
                                         0.00 AS DESCONTO,
                                         A.SITUACAO_TRIBUTARIA ,
                                         A.ICMS_ALIQUOTA ,
                                         A.IPI_ALIQUOTA ,
                                         A.REFERENCIA ,
                                         A.DESCRICAO_FORNECEDOR --ALEXSANDER 10/02/17
                                    FROM #REFERENCIA           A WITH(NOLOCK)  
                               LEFT JOIN PRODUTOS_FORNECEDORES B WITH(NOLOCK) ON A.PRODUTO  = B.PRODUTO
                                                                             AND A.ENTIDADE = B.ENTIDADE                 
                                   WHERE B.PRODUTO IS NULL


     --------------------------------------------------------------
     -- GERACAO DA NOTA DE DEBITO                                --
     --------------------------------------------------------------

     --EXEC GERAR_NOTAS_DEBITO_NF_COMPRA @NF_COMPRA

     --------------------------------------------------------------
     -- GERACAO DOS PRODUTOS PARA DEVOLUCAO                      --
     --------------------------------------------------------------

     EXEC GERAR_DEVOLUCOES_COMPRAS @NF_COMPRA

     --------------------------------------------------------------
     -- GERACAO DO ESPELHO DE CUSTO DE NOTAS FISCAIS             --
     --------------------------------------------------------------

     EXEC GERAR_ESPELHO_CUSTO @NF_COMPRA
     
     
     --------------------------------------------------------------
     -- PROCESSAMENTO DO ULTIMO CUSTO            --
     --------------------------------------------------------------

     EXEC PROCESSAMENTO_ULTIMO_CUSTO NULL, @NF_COMPRA
    
     --------------------------------------------------------------
     -- GERA COMPRAS ANALITICAS  - 10/06/2014 BRUNO SILVINO --
     --------------------------------------------------------------
     EXEC GERAR_COMPRAS_ANALITICAS :FORMULARIO_ORIGEM,:TAB_MASTER_ORIGEM,:NF_COMPRA


     --------------------------------------------------------------
     -- GERA A BAIXA DO PEDIDO DE COMPRAS                        --
     --------------------------------------------------------------
	 IF @ENCERRAR_PEDIDO_TOTAL = 'S'
	 BEGIN
     EXEC BAIXAR_PEDIDO_COMPRA @NF_COMPRA, @ENCERRAR_PEDIDO_TOTAL 
	 END


   END --PROCESSAR = S

   ------------------------------------------------------
   -- ATUALIZA VALIDADE DIGITACAO COM BASE NA VALIDADE --
   ------------------------------------------------------
   UPDATE NF_COMPRA_PRODUTOS
      SET VALIDADE_DIGITACAO = RIGHT (CONVERT(VARCHAR(10),VALIDADE,103),7)
	WHERE NF_COMPRA = @NF_COMPRA

  --------------------------------------------------------------------
     --GERA A PRODUTOS LOTE VALIDADE
  --------------------------------------------------------------------

     INSERT INTO PRODUTOS_LOTE_VALIDADE ( 

            FORMULARIO_ORIGEM ,
            TAB_MASTER_ORIGEM ,
            REG_MASTER_ORIGEM ,
            PRODUTO ,
            LOTE ,
            VALIDADE_DIGITACAO ,
            VALIDADE  )
            
     SELECT DISTINCT
            A.FORMULARIO_ORIGEM,
            A.TAB_MASTER_ORIGEM,
            A.REG_MASTER_ORIGEM,
            B.PRODUTO,
            B.LOTE,
            B.VALIDADE_DIGITACAO ,
            B.VALIDADE

            FROM NF_COMPRA               A WITH(NOLOCK) 
            JOIN NF_COMPRA_PRODUTOS      B WITH(NOLOCK) ON B.NF_COMPRA          = A.NF_COMPRA  
       LEFT JOIN PRODUTOS_LOTE_VALIDADE  C WITH(NOLOCK) ON C.PRODUTO            = B.PRODUTO
                                                       AND C.LOTE               = B.LOTE
                                                       AND C.VALIDADE_DIGITACAO = B.VALIDADE_DIGITACAO
      WHERE A.NF_COMPRA = @NF_COMPRA
        AND B.VALIDADE IS NOT NULL
        AND C.LOTE_VALIDADE IS NULL







---------------------------------------------------------------------
-- ATUALIZA CUSTO DAS TABELAS DE PRE�OS SOMENTE PARA DISTRIBUIDORA --
---------------------------------------------------------------------

IF @PROCESSAR = 'S' AND @EMPRESA = 1000

BEGIN 

MERGE TABELAS_PRECOS_MAXIMO_PRODUTOS A
     USING (
             SELECT 
                    B.TABELA_PRECO_MAXIMO                          AS TABELA_PRECO_MAXIMO , 
                    A.PRODUTO                                      AS PRODUTO,
                    MAX(A.CUSTO + (A.CUSTO * B.PERCENTUAL / 100 )) AS PRECO_MAXIMO        ,
                    0.00                                           AS PRECO_FABRICA       ,
                    B.PERCENTUAL                                   AS PERCENTUAL          ,
                    MAX(A.CUSTO)                                   AS CUSTO
               FROM ESPELHO               A WITH(NOLOCK)
	           LEFT
	           JOIN TABELAS_PRECOS_MAXIMO B WITH(NOLOCK) ON B.ATUALIZA_ON_LINE = 'S'
               LEFT
               JOIN OPERACOES_FISCAIS     C WITH(NOLOCK) ON C.OPERACAO_FISCAL = A.OPERACAO_FISCAL
              WHERE 1=1
                AND A.NF_COMPRA = @NF_COMPRA
				AND C.ATUALIZAR_CUSTO_ESTOQUE = 'S'
           GROUP BY B.TABELA_PRECO_MAXIMO,
                    A.PRODUTO,
                    B.PERCENTUAL
           ) B ON B.PRODUTO = A.PRODUTO AND B.TABELA_PRECO_MAXIMO = A.TABELA_PRECO_MAXIMO


WHEN MATCHED THEN
	UPDATE
		SET 
           PRECO_MAXIMO        = B.PRECO_MAXIMO        ,
           PRECO_FABRICA       = B.PRECO_FABRICA       ,
           PERCENTUAL          = B.PERCENTUAL          ,
           CUSTO               = B.CUSTO


WHEN NOT MATCHED THEN 
	INSERT
	(
       TABELA_PRECO_MAXIMO ,
       PRODUTO             ,
       PRECO_MAXIMO        ,
       PRECO_FABRICA       ,
       PERCENTUAL          ,
       CUSTO
    )
    VALUES
    (
       B.TABELA_PRECO_MAXIMO ,
       B.PRODUTO             ,
       B.PRECO_MAXIMO        ,
       B.PRECO_FABRICA       ,
       B.PERCENTUAL          ,
       B.CUSTO
    )	;

END

        ------------------------------------------------------------
	--GERA��O DA NFE -- Inclu�do 21/09/2017
	------------------------------------------------------------
	IF :EMITIR_NFE = 'S'  AND (@AUTORIZACAO_NFE_NF_COMPRA > 0  OR @GERACAO_NOTA_ENTRADA >0)
	BEGIN
       	
             --EQUALIZA A DATA DE EMISSAO
             UPDATE NF_COMPRA
                SET EMISSAO_NFE = DATEADD(DD,DATEDIFF(DD,CONVERT(DATE,CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET())),EMISSAO),CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET()) )
              WHERE NF_COMPRA = @NF_COMPRA
    
             --EQUALIZA A DATA DE SAIDA
             UPDATE NF_COMPRA
                SET SAIDA_NFE   = DATEADD(DD,DATEDIFF(DD,CONVERT(DATE,CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET())),EMISSAO  ),CONVERT(DATETIMEOFFSET(0),SYSDATETIMEOFFSET()) )
              WHERE NF_COMPRA = @NF_COMPRA

         IF ISNULL(@NF_NUMERO ,0) = 0
         BEGIN
            EXEC USP_CONTROLE_SEQUENCIA_NF_NUMERO @EMPRESA, @NF_SERIE, @NF_ESPECIE, 1, @NF_NUMERO OUTPUT, NULL, @FORMULARIO_ORIGEM, @NF_COMPRA, @TAB_MASTER_ORIGEM
            
            UPDATE NF_COMPRA
               SET NF_NUMERO = @NF_NUMERO
             WHERE NF_COMPRA = @NF_COMPRA
	       AND ISNULL(NF_NUMERO, 0) = 0
            
         END
	  
             ---------------------------
             -- GERANDO NFE_CABECALHO --
             ---------------------------
             DECLARE @REGISTRO_NFE NUMERIC(15)
             EXEC GERAR_NFE_NF_COMPRA @FORMULARIO_ORIGEM, 
                                      @TAB_MASTER_ORIGEM,
                                      @NF_COMPRA,
                                      @REGISTRO_NFE OUTPUT

             ------------------------------------------
             --ENVIA COMANDO PARA ACIONAR ROBO NF-E
             ----------------------------------------
             INSERT INTO #NFE_COMANDOS ( COMANDO )
                 VALUES ( 'PROCESSAR_NFE=' + RTRIM( LTRIM( STR( @REGISTRO_NFE ) ) ) )


	END -- EMITIR = S