-- Modifica��o Realizada por Ynoa Pedro 24/08/2018
DECLARE @INICIAL    DATETIME
DECLARE @FINAL        DATETIME
DECLARE @EMPRESA    NUMERIC
DECLARE @FORNECEDOR NUMERIC
DECLARE @T            NUMERIC


/*----------------------------------------------*
A vari�vel T vai indicar o estado atual do Pedido.
0 ou NULL - Traz todos
1      - Pendentes
2      - Encerrados
Adicionada por Ynoa Pedro 24/08/2018
*/


SET @INICIAL = :INICIAL
SET @FINAL   = :FINAL
SET @EMPRESA             = (CASE WHEN ISNUMERIC(:EMPRESA)     = 1 THEN :EMPRESA     ELSE NULL END)
SET @FORNECEDOR          = (CASE WHEN ISNUMERIC(:FORNECEDOR)  = 1 THEN :FORNECEDOR  ELSE NULL END)
SET @T                   = (CASE WHEN ISNUMERIC(:T)           = 1 THEN :T           ELSE NULL END)


--SET @EMPRESA          = 5
--SET @FORNECEDOR       = 69141
--SET @INICIAL          = '01/01/2015'
--SET @FINAL            = '31/08/2018'
--SET @T                = 1

if OBJECT_ID('TEMPDB..#TEMP') IS NOT NULL DROP TABLE #TEMP
if OBJECT_ID('TEMPDB..#TEMP2')IS NOT NULL DROP TABLE #TEMP2
if OBJECT_ID('TEMPDB..#TEMP3')IS NOT NULL DROP TABLE #TEMP3

SELECT A.PEDIDO_COMPRA,
       B.EMPRESA,
       B.FORMULARIO_ORIGEM,
       B.TAB_MASTER_ORIGEM,
       A.PRODUTO,
       A.PEDIDO_COMPRA_PRODUTO,
       A.PEDIDO,
       A.RECEBIMENTO,
       A.CANCELAMENTO,
       CONVERT(DATE, B.DATA_HORA ) AS DATA_PEDIDO,
       
       CASE WHEN A.RECEBIMENTO > 0 
            THEN A.DATA
            ELSE NULL
       END                         AS ULTIMA_ENTREGA,
       
       B.ENTIDADE                AS FORNECEDOR,
       C.COMPRADOR               AS COMPRADOR


     INTO #TEMP      
     FROM PEDIDOS_COMPRAS_PRODUTOS_TRANSACOES A WITH(NOLOCK)
     JOIN PEDIDOS_COMPRAS                     B WITH(NOLOCK) ON B.PEDIDO_COMPRA = A.PEDIDO_COMPRA
LEFT JOIN COMPRADORES                         C WITH(NOLOCK) ON B.COMPRADOR     = C.COMPRADOR
    WHERE CONVERT(DATE, B.DATA_HORA ) >= @INICIAL
      AND CONVERT(DATE, B.DATA_HORA ) <= @FINAL
      AND (B.EMPRESA  = @EMPRESA    OR @EMPRESA    IS NULL)


----------------------------------------------------------------
-- temp criada para capturar o Status da PEDIDO               --
-- adicionada por ynoa pedro 27/08/2018                       --
----------------------------------------------------------------

 SELECT   A.PEDIDO_COMPRA_PRODUTO   AS PEDIDO_COMPRA_PRODUTO
         ,A.PRODUTO                 AS PRODUTO
         ,'PENDENTE'                AS ESTADO
         --,MAX(A.SALDO)              AS SALDO
         INTO #TEMP2
         FROM PEDIDOS_COMPRAS_PRODUTOS_SALDO  A WITH(NOLOCK) 
         JOIN #TEMP                           C WITH(NOLOCK)  ON A.PEDIDO_COMPRA_PRODUTO = C.PEDIDO_COMPRA_PRODUTO                                                      AND A.PRODUTO               = C.PRODUTO
          WHERE ( @T = 1 OR @T IS NULL)
          GROUP BY A.PEDIDO_COMPRA_PRODUTO
                   ,A.PRODUTO  
                   ,A.SALDO       
          HAVING  MAX(A.SALDO) > 0 

      UNION ALL

SELECT    A.PEDIDO_COMPRA_PRODUTO     AS PEDIDO_COMPRA_PRODUTO
         ,A.PRODUTO                 AS PRODUTO
         ,'ENCERRADO'               AS ESTADO
         --,MAX(A.SALDO)              AS SALDO
           FROM PEDIDOS_COMPRAS_PRODUTOS_SALDO A WITH(NOLOCK) 
          JOIN #TEMP                           C WITH(NOLOCK)   ON A.PEDIDO_COMPRA_PRODUTO = C.PEDIDO_COMPRA_PRODUTO
                                                              AND A.PRODUTO               = C.PRODUTO

          WHERE ( @T = 2 OR @T IS NULL)
          GROUP BY A.PEDIDO_COMPRA_PRODUTO
                   ,A.PRODUTO  
                   ,A.SALDO       
          HAVING  MAX(A.SALDO) <= 0

----------------------------------------------------------------
-- temp criada para capturar Saldo Do Pedido                  --
-- adicionada por ynoa pedro 24/08/2018                       --
----------------------------------------------------------------

 SELECT   A.PEDIDO_COMPRA_PRODUTO     AS PEDIDO_COMPRA_PRODUTO
         ,A.PRODUTO                 AS PRODUTO
		 ,MAX(A.SALDO)              AS SALDO
         INTO #TEMP3
         FROM PEDIDOS_COMPRAS_PRODUTOS_SALDO  A WITH(NOLOCK) 
         JOIN #TEMP                           C WITH(NOLOCK)  ON A.PEDIDO_COMPRA_PRODUTO = C.PEDIDO_COMPRA_PRODUTO                                    
		                                                     AND A.PRODUTO               = C.PRODUTO
		 GROUP BY A.PEDIDO_COMPRA_PRODUTO
         ,A.PRODUTO   



    
 


----------------------------------------------------------------
-- Select Final da Query                                      --
----------------------------------------------------------------



    SELECT A.FORMULARIO_ORIGEM                              AS FORMULARIO_ORIGEM ,
           A.TAB_MASTER_ORIGEM                              AS TAB_MASTER_ORIGEM,
           A.PEDIDO_COMPRA                                  AS REG_MASTER_ORIGEM ,
           A.PEDIDO_COMPRA                                  AS PEDIDO_COMPRA ,             
           A.EMPRESA                                        AS EMPRESA , 
           A.PRODUTO                                        AS PRODUTO ,
           D.DESCRICAO                                      AS DESCRICAO ,
           E.MARCA                                          AS COD_MARCA,
           E.DESCRICAO                                      AS MARCA,
           A.DATA_PEDIDO                                    AS DATA_PEDIDO , 
           MAX(A.ULTIMA_ENTREGA)                            AS ULTIMA_ENTREGA , 
           SUM ( A.PEDIDO )                                 AS PEDIDO , 
           SUM ( A.RECEBIMENTO )                            AS ATENDIDO ,
           SUM ( A.CANCELAMENTO)                            AS ENCERRAMENTO,

           --CASE WHEN  (MAX ( C.SALDO ) > 0 )
           --     THEN 'PENDENTE'
           --     ELSE 'ENCERRADO'
           --END                                              AS ESTADO ,
           C.ESTADO                                             AS ESTADO ,

           CASE WHEN (  A.PEDIDO  -  A.RECEBIMENTO   ) < 0 
                THEN 0
                ELSE  ( A.PEDIDO   -A.RECEBIMENTO   )
           END                                              AS NAO_ANTENDIDO ,
           I.SALDO                                          AS SALDO,
           A.FORNECEDOR                                     AS COD_FORNECEDOR,
           G.NOME                                           AS FORNECEDOR,
           A.COMPRADOR                                      AS COD_COMPRADOR,
           H.NOME                                           AS COMPRADOR
           

          
     FROM #TEMP                             A WITH(NOLOCK)

     JOIN PEDIDOS_COMPRAS_PRODUTOS          B WITH(NOLOCK) ON B.PEDIDO_COMPRA_PRODUTO = A.PEDIDO_COMPRA_PRODUTO
                                                          AND B.PRODUTO               = A.PRODUTO
     JOIN #TEMP2                            C WITH(NOLOCK) ON C.PEDIDO_COMPRA_PRODUTO = A.PEDIDO_COMPRA_PRODUTO
                                                          AND C.PRODUTO               = A.PRODUTO     
     JOIN PRODUTOS                          D WITH(NOLOCK) ON D.PRODUTO               = A.PRODUTO
LEFT JOIN MARCAS                            E WITH(NOLOCK) ON E.MARCA                 = D.MARCA
LEFT JOIN PRODUTOS_FORNECEDORES             F WITH(NOLOCK) ON F.PRODUTO               = D.PRODUTO
LEFT JOIN ENTIDADES                         G WITH(NOLOCK) ON A.FORNECEDOR            = G.ENTIDADE
LEFT JOIN COMPRADORES                       H WITH(NOLOCK) ON H.COMPRADOR             = A.COMPRADOR
     JOIN #TEMP3                            I WITH(NOLOCK) ON I.PEDIDO_COMPRA_PRODUTO = A.PEDIDO_COMPRA_PRODUTO
													      AND I.PRODUTO               = A.PRODUTO

    WHERE (F.ENTIDADE = @FORNECEDOR OR @FORNECEDOR IS NULL)



     



   GROUP BY  A.FORMULARIO_ORIGEM        ,
             A.TAB_MASTER_ORIGEM        ,
             A.PEDIDO_COMPRA            ,
             A.PEDIDO_COMPRA            ,
             A.EMPRESA                  ,
             A.PRODUTO                  ,
             D.DESCRICAO                ,
             E.MARCA                    ,
             E.DESCRICAO                ,
             A.DATA_PEDIDO              ,
             A.FORNECEDOR               ,
             G.NOME                     ,
             A.COMPRADOR                ,
             C.ESTADO                   ,
             I.SALDO                    ,
             H.NOME                     ,
			 A.PEDIDO                   ,
			 A.RECEBIMENTO
             

  ORDER BY PEDIDO_COMPRA 