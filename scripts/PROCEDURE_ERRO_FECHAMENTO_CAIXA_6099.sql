ALTER  PROCEDURE [dbo].[USP_RELATORIO_GERENCIAL_FECHAMENTO_CAIXA]
        AS
        
        DECLARE @RELATORIO          VARCHAR(MAX)
        DECLARE @ENTER              CHAR(2)
        DECLARE @ABERTURA           NUMERIC(15)
        DECLARE @TROCO_INICIAL      NUMERIC(15,2)
        DECLARE @CLIENTES_ATENDIDOS INT
        DECLARE @ITENS_VENDIDOS     INT
        DECLARE @VENDAS_CANCELADAS  INT 
        DECLARE @ITENS_CANCELADAS   INT
        DECLARE @TOTAL_VENDAS       NUMERIC(15,2)
        DECLARE @TICKET_MEDIO       NUMERIC(15,2)
        DECLARE @TROCO_MOTOBOY      NUMERIC(15,2)
        
        SET @ENTER = CHAR(13) + CHAR(10)
        
        
        DECLARE @TOTAL_SUPRIMENTO     NUMERIC(15,2)
        DECLARE @TOTAL_DINHEIRO_CAIXA NUMERIC(15,2)
        
        /*-----------------------------------------*/
        /* VARIAVEIS PARA ACUMULAR VALORES       --*/
        /* DE VENDAS AGRUPADOS POR FINALIZADORA  -- */
        /*-----------------------------------------*/
        DECLARE @TOTAL_DINHEIRO     NUMERIC(15,2)
        DECLARE @TOTAL_TROCO        NUMERIC(15,2)
        DECLARE @TOTAL_CHEQUE       NUMERIC(15,2)
        DECLARE @TOTAL_PREDATADO    NUMERIC(15,2)
        DECLARE @TOTAL_CARTAO_TEF   NUMERIC(15,2)
        DECLARE @TOTAL_CONVENIO     NUMERIC(15,2)
        DECLARE @TOTAL_CARTAO_PRO   NUMERIC(15,2)
        DECLARE @TOTAL_FATURADO     NUMERIC(15,2)
        DECLARE @TOTAL_VALE_CREDITO NUMERIC(15,2)
        DECLARE @TOTAL_CARTAO_OFF   NUMERIC(15,2)
        
        
        /*-----------------------------------*/
        /* VARIAVEIS PARA ACUMULAR VALORES --*/
        /* DE SANGRIAS POR MODALIDADE      --*/
        /*-----------------------------------*/
        DECLARE @TOTAL_SANGRIA_DINHEIRO     NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_SAQUE        NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_CHEQUE       NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_PREDATADO    NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_CARTAO_TEF   NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_CONVENIO     NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_CARTAO_PRO   NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_FATURADO     NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_VALE_CREDITO NUMERIC(15,2)
        DECLARE @TOTAL_SANGRIA_CARTAO_OFF   NUMERIC(15,2)
        
        /*-----------------------------------------------*/
        /* VARIAVEIS PARA ACUMULAR VALORES DE RECARGAS --*/
        /*-----------------------------------------------*/
        DECLARE @TOTAL_RECARGA           NUMERIC(15,2)
        DECLARE @TOTAL_RECARGA_RECEBIDO  NUMERIC(15,2)
        DECLARE @TOTAL_RECARGA_TROCO     NUMERIC(15,2)
        DECLARE @TOTAL_RECARGAS_DINHEIRO NUMERIC(15,2)
        
        SET @ABERTURA      = (SELECT MAX(ABERTURA) FROM CAIXAS WITH(NOLOCK) )
        
        SET @TROCO_INICIAL = (SELECT TROCO_INICIAL 
                                FROM CAIXAS WITH(NOLOCK) 
                               WHERE ABERTURA = @ABERTURA)
        
        
        SELECT @TOTAL_DINHEIRO     = SUM(DINHEIRO*FATOR),
               @TOTAL_TROCO        = SUM(TROCO*FATOR),
               @TOTAL_CHEQUE       = SUM(CHEQUE*FATOR),
               @TOTAL_PREDATADO    = SUM(PREDATADO*FATOR),
               @TOTAL_CARTAO_TEF   = SUM(CARTAO*FATOR),
               @TOTAL_CONVENIO     = SUM(CONVENIO*FATOR),
               @TOTAL_CARTAO_PRO   = SUM(CARTAO_PROPRIO*FATOR),
               @TOTAL_FATURADO     = SUM(FATURADO*FATOR),
               @TOTAL_VALE_CREDITO = SUM(VALE_CREDITO*FATOR),
               @TOTAL_CARTAO_OFF   = SUM(CARTAO_OFF*FATOR)
          FROM (
        
        SELECT A.TIPO,
               CASE WHEN A.STATUS = 'A' THEN 1 ELSE -1 END               AS FATOR,
               SUM(CASE WHEN A.TIPO = 1 THEN A.TROCO         ELSE 0 END) AS TROCO,
               SUM(CASE WHEN A.TIPO = 1 THEN A.VALOR-A.TROCO ELSE 0 END) AS DINHEIRO,
               SUM(CASE WHEN A.TIPO = 2 THEN A.VALOR         ELSE 0 END) AS CHEQUE,
               SUM(CASE WHEN A.TIPO = 3 THEN A.VALOR         ELSE 0 END) AS PREDATADO,
               SUM(CASE WHEN A.TIPO = 4 THEN A.VALOR         ELSE 0 END) AS CARTAO,
               SUM(CASE WHEN A.TIPO = 5 THEN A.VALOR         ELSE 0 END) AS CONVENIO,
               SUM(CASE WHEN A.TIPO = 6 THEN A.VALOR         ELSE 0 END) AS CARTAO_PROPRIO,
               SUM(CASE WHEN A.TIPO = 7 THEN A.VALOR         ELSE 0 END) AS FATURADO,
               SUM(CASE WHEN A.TIPO = 8 THEN A.VALOR         ELSE 0 END) AS VALE_CREDITO,
               SUM(CASE WHEN A.TIPO = 9 THEN A.VALOR         ELSE 0 END) AS CARTAO_OFF
              
          FROM FINALIZADORAS A WITH(NOLOCK)
          JOIN VENDAS        B WITH(NOLOCK) ON B.VENDA     = A.VENDA 
                                           AND B.LOJA      = A.LOJA
                                           AND B.CAIXA     = A.CAIXA
                                           AND B.MOVIMENTO = A.MOVIMENTO  
         WHERE A.ABERTURA = @ABERTURA
           AND B.OPERADOR_MOTOBOY IS NULL
         GROUP BY A.TIPO, A.STATUS
               
               ) A
        
        /*--------------------------------------------*/
        /* TOTAL DE RECARGAS AGRUPADO POR OEPRADORA --*/
        /*--------------------------------------------*/
        
        IF OBJECT_ID('TEMPDB..#RECARGAS_OPERADORAS') IS NOT NULL
           DROP TABLE #RECARGAS_OPERADORAS
           
        IF OBJECT_ID('TEMPDB..#RECARGAS_MODALIDADES') IS NOT NULL
           DROP TABLE #RECARGAS_MODALIDADES
        
        CREATE TABLE #RECARGAS_OPERADORAS (
               OPERADORA       VARCHAR(60)   NOT NULL,
               TOTAL           NUMERIC(15,2) NOT NULL,
               TOTAL_RECEBIDO  NUMERIC(15,2) NOT NULL,
               TOTAL_TROCO     NUMERIC(15,2) NOT NULL 
        )
        
        CREATE TABLE #RECARGAS_MODALIDADES (
               MODALIDADE VARCHAR(30)   NOT NULL,
               VALOR      NUMERIC(15,2) NOT NULL
        )
        
        INSERT INTO #RECARGAS_OPERADORAS (
               OPERADORA       ,
               TOTAL           ,
               TOTAL_RECEBIDO  ,
               TOTAL_TROCO     )         
        SELECT A.OPERADORA,
               
               CASE WHEN STATUS = 'A' THEN SUM( A.VALOR_RECARGA )
                                      ELSE SUM( A.VALOR_RECARGA ) * ( -1 ) 
               END AS VALOR_RECARGA ,
               CASE WHEN STATUS = 'A' THEN SUM( A.VALOR_RECEBIDO )
                                      ELSE SUM( A.VALOR_RECEBIDO ) * ( -1 )  
               END AS VALOR_RECEBIDO ,
               CASE WHEN STATUS = 'A' THEN SUM( A.TROCO ) 
                                      ELSE SUM( A.TROCO ) * ( -1 ) 
               END AS TROCO 
          FROM RECARGAS A WITH(NOLOCK)
         WHERE ABERTURA = @ABERTURA
         GROUP BY A.OPERADORA ,
                  A.STATUS
        
        
        INSERT INTO #RECARGAS_MODALIDADES (
               MODALIDADE,
               VALOR )
        SELECT CASE WHEN SUBSTRING(TIPO_PAGAMENTO, 1, 2) = '98' THEN '     Total Recarga Dinheiro'
                    WHEN SUBSTRING(TIPO_PAGAMENTO, 1, 2) = '02' THEN '     Total Recarga Cr�dito'
        			WHEN SUBSTRING(TIPO_PAGAMENTO, 1, 2) = '03' THEN '     Total Recarga Voucher'
                    WHEN SUBSTRING(TIPO_PAGAMENTO, 1, 2) = '01' THEN '     Total Recarga D�bito'            
                    WHEN SUBSTRING(TIPO_PAGAMENTO, 1, 2) = '05' THEN '     Total Recarga Conv�nio'            
               END                  AS PAGAMENTO,
               SUM( VALOR_RECARGA ) AS VALOR_RECARGA
          FROM RECARGAS WITH(NOLOCK)
         WHERE ABERTURA = @ABERTURA
        GROUP BY SUBSTRING(TIPO_PAGAMENTO, 1, 2)
        
        
        SELECT @TOTAL_RECARGA          = SUM(TOTAL),
               @TOTAL_RECARGA_RECEBIDO = SUM(TOTAL_RECEBIDO),
               @TOTAL_RECARGA_TROCO    = SUM(TOTAL_TROCO)
     FROM #RECARGAS_OPERADORAS
        
        
        
        SET @RELATORIO = '     -------------------------------------' + @ENTER +
                         '     ******** FECHAMENTO DE CAIXA ********' + @ENTER +
                         '     -------------------------------------' + @ENTER
                         
        SET @RELATORIO =  @RELATORIO +
                         '     Troco Inicial.............: ' + DBO.Formatacao(CONVERT(VARCHAR,@TROCO_INICIAL), 9, 'D', ' ') + @ENTER
                         
        SET @RELATORIO =  @RELATORIO +
                          '     -------------------------------------' + @ENTER +
                          '     *********** FINALIZADORAS ***********' + @ENTER +
                          '     -------------------------------------' + @ENTER +
                          '     Dinheiro..................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_DINHEIRO    ), 10, 'D', ' ') + @ENTER +
                          '     Troco.....................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_TROCO       ), 10, 'D', ' ') + @ENTER +
                          '     Cheque....................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_CHEQUE      ), 10, 'D', ' ') + @ENTER +
                          '     Cheque Predatado..........:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_PREDATADO   ), 10, 'D', ' ') + @ENTER +
                          '     Cartao TEF................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_CARTAO_TEF  ), 10, 'D', ' ') + @ENTER + 
          '     Convenio..................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_CONVENIO    ), 10, 'D', ' ') + @ENTER +
                          '     Cartao Proprio............:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_CARTAO_PRO  ), 10, 'D', ' ') + @ENTER +
                          '     Faturado..................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_FATURADO    ), 10, 'D', ' ') + @ENTER +
                          '     Vale Credito..............:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_VALE_CREDITO), 10, 'D', ' ') + @ENTER +
                          '     Cartao POS................:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_CARTAO_OFF  ), 10, 'D', ' ') + @ENTER 
        
        
        SET @RELATORIO =  @RELATORIO +
                          '     -------------------------------------' + @ENTER +
                          '     * RECARGAS DE CELULAR P/ OPERADORA  *' + @ENTER +
                          '     -------------------------------------' + @ENTER 
        
        SELECT @RELATORIO =  @RELATORIO +
               OPERADORA + REPLICATE('.', 30-LEN(OPERADORA)) + ':' + 
               DBO.Formatacao(CONVERT(VARCHAR, TOTAL  ), 10, 'D', ' ') + @ENTER  
          FROM #RECARGAS_OPERADORAS
        
        
        IF @TOTAL_RECARGA IS NULL
           SET @TOTAL_RECARGA = 0
        
        IF @TOTAL_RECARGA_RECEBIDO IS NULL
           SET @TOTAL_RECARGA_RECEBIDO = 0
           
        IF @TOTAL_RECARGA_TROCO IS NULL
           SET @TOTAL_RECARGA_TROCO = 0         
        
        SET @RELATORIO =  @RELATORIO + 
                          '     Total de Recargas ........:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_RECARGA)         , 10, 'D', ' ') + @ENTER + 
                          '     Total Recebido............:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_RECARGA_RECEBIDO), 10, 'D', ' ') + @ENTER +
                          '     Total Troco...............:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_RECARGA_TROCO)   , 10, 'D', ' ') + @ENTER 
        
        
        IF (SELECT COUNT(1) FROM #RECARGAS_MODALIDADES) > 0
        BEGIN
        
        	SELECT @RELATORIO =  @RELATORIO + 
        		   '     -------------------------------------' + @ENTER +
        		   '     * RECARGAS DE CELULAR P/ MODALIDADE *' + @ENTER +
        		   '     -------------------------------------' + @ENTER 
        
        	SELECT @RELATORIO =  @RELATORIO +  
        		   MODALIDADE + REPLICATE('.', 30-LEN(MODALIDADE)) + ':' + 
        		   DBO.Formatacao(CONVERT(VARCHAR, VALOR  ), 10, 'D', ' ') + @ENTER  
        	  FROM #RECARGAS_MODALIDADES
        
        END
        
        /*---------------------------------*/
        /* TOTAL DE VENDAS EM CART�O TEF --*/
        /*---------------------------------*/
        IF OBJECT_ID('TEMPDB..#CARTOES_TEF') IS NOT NULL
           DROP TABLE #CARTOES_TEF
           
        CREATE TABLE #CARTOES_TEF (
               MOVIMENTO    DATETIME      NOT NULL,
               TIPO         VARCHAR(30)   NOT NULL,
               BANDEIRA     VARCHAR(30)   NOT NULL,
               VALOR        NUMERIC(15,2) NOT NULL,
               TOTAL_VENDAS INT           NOT NULL
        )
        
        
        INSERT INTO #CARTOES_TEF (
               MOVIMENTO,
               TIPO,
               BANDEIRA,
               VALOR,
               TOTAL_VENDAS )
        SELECT A.MOVIMENTO ,
               A.TIPO ,
               A.DESCRICAO ,
               SUM( A.VALOR ) AS VALOR,
               B.VENDAS
          FROM ( SELECT A.MOVIMENTO ,
                        B.DESCRICAO                     AS TIPO ,
                        ISNULL( C.DESCRICAO, 'OUTROS' ) AS DESCRICAO ,
                        CASE WHEN E.STATUS = 'A' 
                           THEN CASE WHEN D.DOCUMENTO IS NULL 
                                   THEN SUM( A.VALOR - A.TROCO )
                                   ELSE 0
                                END
                           ELSE 0  
                        END AS VALOR
        
                   FROM FINALIZADORAS       A WITH(NOLOCK)
                   JOIN TIPOS_FINALIZADORAS B WITH(NOLOCK) ON A.TIPO     = B.TIPO_FINALIZADORA
              LEFT JOIN BANDEIRAS           C WITH(NOLOCK) ON A.BANDEIRA = C.CODIGO
              LEFT JOIN CANCELAMENTOS_TEF   D WITH(NOLOCK) ON CAST( A.NSU AS NUMERIC(15)) = CAST( D.DOCUMENTO AS NUMERIC(15))
                   JOIN VENDAS              E WITH(NOLOCK) ON A.VENDA    = E.VENDA
                       
                  WHERE A.ABERTURA = @ABERTURA
                    AND A.NSU <> '' 
                    AND A.TIPO = 4
                    AND E.OPERADOR_MOTOBOY IS NULL
               GROUP BY A.MOVIMENTO , B.DESCRICAO , A.STATUS , C.DESCRICAO , D.DOCUMENTO, E.STATUS ) A
        
         INNER JOIN (SELECT ISNULL( C.DESCRICAO, 'OUTROS' ) AS DESCRICAO, COUNT(*) AS VENDAS
                       FROM FINALIZADORAS       A WITH(NOLOCK)
                       JOIN TIPOS_FINALIZADORAS B WITH(NOLOCK) ON A.TIPO     = B.TIPO_FINALIZADORA
                  LEFT JOIN BANDEIRAS           C WITH(NOLOCK) ON A.BANDEIRA = C.CODIGO
                  LEFT JOIN CANCELAMENTOS_TEF   D WITH(NOLOCK) ON CAST( A.NSU AS NUMERIC(15)) = CAST( D.DOCUMENTO AS NUMERIC(15))
                      JOIN VENDAS               E WITH(NOLOCK) ON A.VENDA    = E.VENDA  
                     WHERE A.ABERTURA = @ABERTURA
                       AND A.NSU      <> '' 
                       AND A.TIPO     = 4
                       AND E.OPERADOR_MOTOBOY IS NULL
                      GROUP BY ISNULL( C.DESCRICAO, 'OUTROS' ) ) B   ON B.DESCRICAO = A.DESCRICAO
        
         GROUP BY A.MOVIMENTO, A.TIPO, A.DESCRICAO, B.VENDAS
         ORDER BY A.MOVIMENTO, A.TIPO
        
        IF (SELECT COUNT(1) FROM #CARTOES_TEF) > 0
        BEGIN
        SELECT @RELATORIO =  @RELATORIO + 
               '     -------------------------------------' + @ENTER +
               '     ************** CART�ES **************' + @ENTER +
               '     -------------------------------------' + @ENTER 
        
        SELECT @RELATORIO =  @RELATORIO +  
               '     ' + TIPO + ' ' + BANDEIRA + REPLICATE('.', 26-LEN(TIPO + ' ' + BANDEIRA)) + ':' + 
               DBO.Formatacao(CONVERT(VARCHAR, VALOR  ), 10, 'D', ' ') + @ENTER  
          FROM #CARTOES_TEF
        
        
       SELECT @RELATORIO =  @RELATORIO + 
               '     -------------------------------------' + @ENTER +
               '     ******** CART�ES QUANTIDADES ********' + @ENTER +
               '     -------------------------------------' + @ENTER 
        
        SELECT @RELATORIO =  @RELATORIO +  
               '     ' + TIPO + ' ' + BANDEIRA + REPLICATE('.', 26-LEN(TIPO + ' ' + BANDEIRA)) + ':' + 
               DBO.Formatacao(CONVERT(VARCHAR, TOTAL_VENDAS  ), 10, 'D', ' ') + @ENTER  
          FROM #CARTOES_TEF
        END
        
        /*----------------------------------*/
        /* TOTAL DE VENDAS EM CARTOES OFF --*/
        /*----------------------------------*/
        
        IF OBJECT_ID('TEMPDB..#CARTOES_OFF') IS NOT NULL
           DROP TABLE #CARTOES_OFF
           
        CREATE TABLE #CARTOES_OFF (
               MOVIMENTO    DATETIME      NOT NULL,
               TIPO         VARCHAR(30)   NOT NULL,
               BANDEIRA     VARCHAR(30)   NOT NULL,
               VALOR        NUMERIC(15,2) NOT NULL,
               TOTAL_VENDAS INT           NOT NULL
        )
        
        INSERT INTO #CARTOES_OFF (
               MOVIMENTO    ,
               TIPO         ,
               BANDEIRA     ,
               VALOR        ,
               TOTAL_VENDAS )
               
        SELECT A.MOVIMENTO ,
               A.TIPO ,
               A.DESCRICAO ,
               SUM( A.VALOR ) AS VALOR,
               B.VENDAS
          FROM ( SELECT A.MOVIMENTO ,
                        B.DESCRICAO                     AS TIPO ,
                        ISNULL( C.DESCRICAO, 'OUTROS' ) AS DESCRICAO ,
                        CASE WHEN A.STATUS = 'A'
                             THEN SUM  ( A.VALOR - A.TROCO )
                             ELSE SUM( ( A.VALOR - A.TROCO ) * (-1) )
                        END  AS VALOR
        
                   FROM FINALIZADORAS       A WITH(NOLOCK)
                   JOIN TIPOS_FINALIZADORAS B WITH(NOLOCK) ON A.TIPO      = B.TIPO_FINALIZADORA
              LEFT JOIN BANDEIRAS           C WITH(NOLOCK) ON A.BANDEIRA  = C.CODIGO
                   JOIN VENDAS              D WITH(NOLOCK) ON D.VENDA     = A.VENDA 
        								                  AND D.LOJA      = A.LOJA
        								                  AND D.CAIXA     = A.CAIXA
        								                  AND D.MOVIMENTO = A.MOVIMENTO
                  WHERE A.ABERTURA = @Abertura
                    AND A.TIPO = 9
                    AND D.OPERADOR_MOTOBOY IS NULL
               GROUP BY A.MOVIMENTO , B.DESCRICAO , A.STATUS , C.DESCRICAO ) A
        
          INNER JOIN (
                 SELECT ISNULL( C.DESCRICAO, 'OUTROS' ) AS DESCRICAO, COUNT(*) AS VENDAS
                   FROM FINALIZADORAS       A WITH(NOLOCK)
                   JOIN TIPOS_FINALIZADORAS B WITH(NOLOCK) ON A.TIPO     = B.TIPO_FINALIZADORA
              LEFT JOIN BANDEIRAS           C WITH(NOLOCK) ON A.BANDEIRA = C.CODIGO
                   JOIN VENDAS  D WITH(NOLOCK) ON D.VENDA     = A.VENDA 
        								                  AND D.LOJA      = A.LOJA
        								                  AND D.CAIXA     = A.CAIXA
        								                  AND D.MOVIMENTO = A.MOVIMENTO      
                  WHERE A.ABERTURA = @Abertura
                    AND A.TIPO     = 9
                    AND D.OPERADOR_MOTOBOY IS NULL
               GROUP BY ISNULL( C.DESCRICAO, 'OUTROS' )  ) B ON B.DESCRICAO = A.DESCRICAO
        
         GROUP BY A.MOVIMENTO, A.TIPO, A.DESCRICAO, B.VENDAS
         ORDER BY A.MOVIMENTO, A.TIPO
        
        
        IF (SELECT COUNT(1) FROM #CARTOES_OFF) > 0 
        BEGIN
        
        	SELECT @RELATORIO =  @RELATORIO + 
        		   '     -------------------------------------' + @ENTER +
        		   '     ************ CART�ES OFF ************' + @ENTER +
        		   '     -------------------------------------' + @ENTER 
        
        	/*SELECT @RELATORIO =  @RELATORIO +  */
        	/*	   '     ' + TIPO + ' ' + BANDEIRA + REPLICATE('.', 26-LEN(TIPO + ' ' + BANDEIRA)) + ':' + */
        	/*	   DBO.Formatacao(CONVERT(VARCHAR, VALOR  ), 10, 'D', ' ') + @ENTER  */
        	/*  FROM #CARTOES_OFF*/
        
        
        	/*SELECT @RELATORIO =  @RELATORIO + */
        	/*	   '     -------------------------------------' + @ENTER +*/
        	/*	   '     ****** CART�ES OFF QUANTIDADES ******' + @ENTER +*/
        	/*	   '     -------------------------------------' + @ENTER */
        
        	/*SELECT @RELATORIO =  @RELATORIO +  */
        	/*	   '     ' + TIPO + ' ' + BANDEIRA + REPLICATE('.', 26-LEN(TIPO + ' ' + BANDEIRA)) + ':' + */
        	/*	   DBO.Formatacao(CONVERT(VARCHAR, TOTAL_VENDAS  ), 10, 'D', ' ') + @ENTER  */
        	/*  FROM #CARTOES_OFF*/
        
        END
        
        /*--------------------------------------------------*/
        /* TOTAL DE RECEBIMENTOS DE TITULOS OR MODALIDADE --*/
        /*--------------------------------------------------*/
        IF OBJECT_ID('TEMPDB..#RECEBIMENTOS_TITULOS') IS NOT NULL
           DROP TABLE #RECEBIMENTOS_TITULOS
              
        CREATE TABLE #RECEBIMENTOS_TITULOS (
              TIPO   VARCHAR(30)   NOT NULL,
              VALOR  NUMERIC(15,2) NOT NULL
        )
        
        INSERT INTO #RECEBIMENTOS_TITULOS (
               TIPO,
               VALOR )
        SELECT TIPO,
               SUM(VALOR-TROCO) AS VALOR
          FROM RECEBIMENTOS_FINALIZADORAS A WITH(NOLOCK)
         WHERE ABERTURA = @ABERTURA
         GROUP BY TIPO
        
        SET @RELATORIO = @RELATORIO + 
                         '     -------------------------------------' + @ENTER +
                         '     ******* RECEBIMENTOS T�TULOS ********' + @ENTER +
                         '     -------------------------------------' + @ENTER 
        
        SELECT @RELATORIO =  @RELATORIO +  
               '     ' + A.DESCRICAO + REPLICATE('.', 26-LEN(A.DESCRICAO)) + ':' + 
               DBO.Formatacao(CONVERT(VARCHAR, ISNULL(VALOR,0)  ), 10, 'D', ' ') + @ENTER  
          FROM TIPOS_FINALIZADORAS A
          LEFT JOIN #RECEBIMENTOS_TITULOS B ON B.TIPO = A.TIPO_FINALIZADORA
         WHERE A.TIPO_FINALIZADORA IN (1,2,3,9)
        
        
        SELECT @TOTAL_RECARGAS_DINHEIRO = SUM(VALOR)
          FROM #RECEBIMENTOS_TITULOS A
         WHERE A.TIPO = 1
        
        /*-------------------------------------*/
        /* TOTAL DE RECEBIMENTOS DE SERVICOS --*/
        /* AGRUPADOS POR MODALIDADE          --*/
        /*-------------------------------------*/
        IF OBJECT_ID('TEMPDB..#RECEBIMENTOS_SERVICOS') IS NOT NULL
           DROP TABLE #RECEBIMENTOS_SERVICOS
              
        CREATE TABLE #RECEBIMENTOS_SERVICOS (
              TIPO   VARCHAR(30)   NOT NULL,
              VALOR  NUMERIC(15,2) NOT NULL
        )
        
        INSERT INTO #RECEBIMENTOS_SERVICOS (
               TIPO,
               VALOR )
        SELECT A.TIPO,
               SUM(VALOR-TROCO) AS VALOR
          FROM RECEBIMENTOS_SERVICOS_FINALIZADORAS   A WITH(NOLOCK)
          JOIN dbo.TIPOS_FINALIZADORAS B WITH(NOLOCK) ON B.TIPO_FINALIZADORA = A.TIPO
         WHERE ABERTURA = @ABERTURA
         GROUP BY A.TIPO
        
        
        SET @RELATORIO = @RELATORIO + 
                         '     -------------------------------------' + @ENTER +
                         '     ******* RECEBIMENTOS SERVICOS *******' + @ENTER +
                         '     -------------------------------------' + @ENTER 
        
        SELECT @RELATORIO =  @RELATORIO +  
               '     ' + A.DESCRICAO + REPLICATE('.', 26-LEN(A.DESCRICAO)) + ':' + 
               DBO.Formatacao(CONVERT(VARCHAR, ISNULL(VALOR,0)  ), 10, 'D', ' ') + @ENTER  
          FROM TIPOS_FINALIZADORAS A
          LEFT JOIN #RECEBIMENTOS_SERVICOS B ON B.TIPO = A.TIPO_FINALIZADORA
         WHERE A.TIPO_FINALIZADORA IN (1,9)
        
        
        
        
        
        /*------------------------------------*/
        /* TOTAL DE SANGRIAS POR MODALIDADE --*/
        /*------------------------------------*/
        SELECT 
               @TOTAL_SANGRIA_DINHEIRO     = SUM(VALOR),
               @TOTAL_SANGRIA_CHEQUE       = SUM(CHEQUES),
               @TOTAL_SANGRIA_PREDATADO    = SUM(PREDATADOS),
               @TOTAL_SANGRIA_CARTAO_TEF   = SUM(CARTOES_TEF),
               @TOTAL_SANGRIA_CONVENIO     = SUM(CONVENIOS),
               @TOTAL_SANGRIA_CARTAO_PRO   = SUM(CARTOES_PROPRIO),
               @TOTAL_SANGRIA_FATURADO     = SUM(FATURADOS),
               @TOTAL_SANGRIA_VALE_CREDITO = SUM(VALE_CREDITO),
               @TOTAL_SANGRIA_CARTAO_OFF   = SUM(CARTOES_POS)
          FROM SANGRIAS WITH(NOLOCK)
         WHERE ABERTURA = @ABERTURA
        
        SELECT @TOTAL_SANGRIA_SAQUE = SUM( (CASE WHEN A.STATUS = 'A' 
                                                 THEN 1 
                                                 ELSE -1 
                                            END)*TROCO)
          FROM FINALIZADORAS A WITH(NOLOCK)
          JOIN VENDAS        D WITH(NOLOCK) ON D.VENDA     = A.VENDA 
        								   AND D.LOJA      = A.LOJA
        								   AND D.CAIXA     = A.CAIXA
        								   AND D.MOVIMENTO = A.MOVIMENTO     
         WHERE A.ABERTURA = @ABERTURA   
           AND A.TIPO     = 4 /*TEF*/
           AND D.OPERADOR_MOTOBOY IS NULL
        
        
        
        SET @RELATORIO =  @RELATORIO +  
                          '     -------------------------------------' + @ENTER +
                          '     ************* SANGRIAS **************' + @ENTER +
                          '     -------------------------------------' + @ENTER +
                          '     Dinheiro..................:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_DINHEIRO    ,0)), 10, 'D', ' ') + @ENTER +
                          '     Compre Saque..............:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_SAQUE       ,0)), 10, 'D', ' ') + @ENTER +
                          '     Cheque....................:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_CHEQUE      ,0)), 10, 'D', ' ') + @ENTER +
                          '     Cheque Predatado..........:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_PREDATADO   ,0)), 10, 'D', ' ') + @ENTER +
                          '     Cartao TEF................:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_CARTAO_TEF  ,0)), 10, 'D', ' ') + @ENTER +
                          '     Convenio..................:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_CONVENIO    ,0)), 10, 'D', ' ') + @ENTER +
                          '     Cartao Proprio............:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_CARTAO_PRO  ,0)), 10, 'D', ' ') + @ENTER +
                          '     Faturado..................:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_FATURADO    ,0)), 10, 'D', ' ') + @ENTER +
                          '     Vale Credito..............:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_VALE_CREDITO,0)), 10, 'D', ' ') + @ENTER +
                          '     Cartao POS................:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SANGRIA_CARTAO_OFF  ,0)), 10, 'D', ' ') + @ENTER 
        
        
        /*------------------------*/
        /* TOTAL DE SUPRIMENTOS --*/
        /*------------------------*/
        
        SELECT @TOTAL_SUPRIMENTO = SUM(VALOR)
          FROM SUPRIMENTOS A WITH(NOLOCK)
         WHERE A.ABERTURA = @ABERTURA
         
        SET @RELATORIO = @RELATORIO + 
                         '     -------------------------------------' + @ENTER +
                         '     Suprimentos...............:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_SUPRIMENTO,0)), 10, 'D', ' ') + @ENTER + 
                         '     -------------------------------------' + @ENTER +
                         '     Troco Prox. Caixa.........:' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TROCO_INICIAL   ,0)), 10, 'D', ' ') + @ENTER + 
                         '     -------------------------------------' + @ENTER
        
        
        SELECT @TROCO_MOTOBOY = 
               SUM((CASE WHEN B.STATUS = 'A' 
                         THEN 1 
             ELSE -1 
                    END)*B.TROCO)
          FROM VENDAS        A WITH(NOLOCK)
          JOIN FINALIZADORAS B WITH(NOLOCK) ON A.VENDA     = B.VENDA 
                                           AND A.LOJA      = B.LOJA 
                                           AND A.CAIXA     = B.CAIXA
                                           AND A.MOVIMENTO = B.MOVIMENTO
         WHERE A.OPERADOR_MOTOBOY IS NOT NULL
           AND B.TIPO = 1
        
        IF (SELECT EXIBIR_TOTAL_DINHEIRO_CAIXA FROM PARAMETROS WITH(NOLOCK)) = 'S'
        BEGIN
             SET @TOTAL_DINHEIRO_CAIXA = 
                 ISNULL(@TROCO_INICIAL,0) 
         	   + (ISNULL(@TOTAL_DINHEIRO,0) - ISNULL(@TOTAL_SANGRIA_DINHEIRO,0) + ISNULL(@TOTAL_SUPRIMENTO,0))
         	   + ISNULL(@TOTAL_RECARGA,0) 
         	   + ISNULL(@TOTAL_RECARGAS_DINHEIRO,0)
        
        
             SET @RELATORIO = @RELATORIO + 
                              '     Total de Dinheiro         :' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_DINHEIRO_CAIXA,0)), 10, 'D', ' ') + @ENTER +
                              '     Total de Troco Motoboy    :' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TROCO_MOTOBOY       ,0)), 10, 'D', ' ') + @ENTER +
                              '     Total de Dinheiro no Caixa:' + DBO.Formatacao(CONVERT(VARCHAR, @TOTAL_DINHEIRO_CAIXA-ISNULL(@TROCO_MOTOBOY,0)), 10, 'D', ' ') + @ENTER
                              
        END
        ELSE
             SET @RELATORIO = @RELATORIO + '     Total de Troco Motoboy    :' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TROCO_MOTOBOY,0)), 10, 'D', ' ') + @ENTER
        
        
        SELECT @ITENS_VENDIDOS   = SUM(CASE WHEN A.STATUS = 'A' 
                                          THEN 1 
                                          ELSE 0 
                                       END),
               @ITENS_CANCELADAS = SUM(CASE WHEN A.STATUS = 'C' 
                                          THEN 1 
                                          ELSE 0
                                       END)
          FROM ITENS         A WITH(NOLOCK)
         WHERE A.ABERTURA = @ABERTURA
           
        
        
        SELECT @CLIENTES_ATENDIDOS = COUNT(1),
               
               @VENDAS_CANCELADAS  =
               SUM(CASE WHEN A.STATUS = 'C' 
                        THEN 1 
                        ELSE 0
                   END)
          FROM VENDAS A WITH(NOLOCK)  
         WHERE A.ABERTURA = @ABERTURA
           AND A.OPERADOR_MOTOBOY IS NULL
        
        
        SELECT @TOTAL_VENDAS =
               SUM( (CASE WHEN A.STATUS = 'A'
                          THEN 1
                          ELSE -1
                     END)* (VALOR-TROCO))
          FROM FINALIZADORAS A WITH(NOLOCK)
          JOIN VENDAS        D WITH(NOLOCK) ON D.VENDA     = A.VENDA 
        								   AND D.LOJA      = A.LOJA
        								   AND D.CAIXA     = A.CAIXA
        								   AND D.MOVIMENTO = A.MOVIMENTO     
         WHERE A.ABERTURA = @ABERTURA
           AND D.OPERADOR_MOTOBOY IS NULL
        
        SET @RELATORIO = @RELATORIO + 
                         '     Clientes Atendidos........:     ' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@CLIENTES_ATENDIDOS,0)), 05, 'D', '0') + @ENTER  +
                         '     Itens Vendidos............:     ' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@ITENS_VENDIDOS    ,0)), 05, 'D', '0') + @ENTER  +
                         '     Cupons Cancelados.........:     ' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@VENDAS_CANCELADAS ,0)), 05, 'D', '0') + @ENTER  +
                         '     Itens Cancelados..........:     ' + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@ITENS_CANCELADAS  ,0)), 05, 'D', '0') + @ENTER  +
                         '     Total de Venda............:'      + DBO.Formatacao(CONVERT(VARCHAR, ISNULL(@TOTAL_VENDAS      ,0)), 10, 'D', ' ') + @ENTER  
        
        
        IF @CLIENTES_ATENDIDOS > 0
        BEGIN
             SET @TICKET_MEDIO = @TOTAL_VENDAS / @CLIENTES_ATENDIDOS
             IF @TICKET_MEDIO IS NULL
                SET @TICKET_MEDIO = 0
                
             SET @RELATORIO = @RELATORIO + 
                         '     Ticket Medio..............:'      + DBO.Formatacao(CONVERT(VARCHAR, @TICKET_MEDIO), 10, 'D', ' ') + @ENTER
        END
        
        
        SELECT @RELATORIO = @RELATORIO + 
                         '     -------------------------------------' + @ENTER + 
                         '     Supervisor: ' + CONVERT(VARCHAR,A.SUPERVISOR) + ' - '   + B.NOME + @ENTER + 
                         '     Operador  : ' + CONVERT(VARCHAR,A.OPERADOR  ) + ' - '   + C.NOME + @ENTER + 
                         '     -------------------------------------' 
          FROM CAIXAS     A WITH(NOLOCK)
          JOIN OPERADORES B WITH(NOLOCK) ON B.OPERADOR = A.SUPERVISOR
          JOIN OPERADORES C WITH(NOLOCK) ON C.OPERADOR = A.OPERADOR
         WHERE A.ABERTURA = @ABERTURA
         
         
        
        SELECT @RELATORIO AS COMPROVANTE, 
               1          AS VIAS