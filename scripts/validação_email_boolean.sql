DECLARE @QUANTIDADE_SIM NUMERIC



SELECT @QUANTIDADE_SIM = COUNT(*)
 
 FROM APROVACOES_CANCELAMENTOS_NF_ITENS A WITH(NOLOCK)
 
WHERE A.APROVACAO_CANCELAMENTO = :APROVACAO_CANCELAMENTO
 
 AND A.APROVADO = 'S'



IF @QUANTIDADE_SIM >= 1



BEGIN 

 

 EXEC ENVIO_EMAIL_APRV_CANC :APROVACAO_CANCELAMENTO, :EMPRESA

END
   

