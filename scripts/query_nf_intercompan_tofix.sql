/*Consulta de notas sem Recebimento
  Desenvolvedor: Ynoa Pedro
  Data: 14-06-2018
  Alterações: 25-06-2018*/



  DECLARE @DATA_INI DATE
  DECLARE @DATA_FIM DATE
  DECLARE @EMPRESA NUMERIC
  DECLARE @NF_NUMERO VARCHAR(30) 



  --SET @DATA_INI = :DATA_INI
  --SET @DATA_FIM = :DATA_FIM
  --SET @EMPRESA  =  NULL

  SET @DATA_INI = '01/01/2018'
  SET @DATA_FIM = '31/12/2018'
  --SET @EMPRESA  =  10


if object_id('tempdb..#NFE_CANCELAMENTOS') is not null    
   DROP TABLE #NFE_CANCELAMENTOS    

SELECT TOP 1000 A.* INTO #NFE_CANCELAMENTOS 
  FROM NFE_CANCELAMENTOS A WITH(NOLOCK)
 WHERE 1 = 1
  AND (A.EMPRESA = @EMPRESA OR @EMPRESA IS NULL )
  --AND (SUBSTRING(A.IDNOTA,26,9) = DBO.ZEROS(@NF_NUMERO,9) OR @NF_NUMERO IS NULL )
ORDER BY NFE_CANCELAMENTO DESC  

----------------------------------------------
-- Captando MSG Sefaz e o protocolo de retorno
----------------------------------------------

DECLARE @RESULTADO TABLE (   CODIGO_RETORNO         VARCHAR(30) ,
                             DESCRICAO_RETORNO      VARCHAR(255),
                             CHAVE_NF_CANCELADA     VARCHAR(60),
                             PROTOCOLO_CANCELAMENTO VARCHAR(60) ,
                             NFE_CANCELAMENTO       NUMERIC )
DECLARE @NFE_CANCELAMENTO NUMERIC(15)


WHILE EXISTS (SELECT TOP 1 1 FROM #NFE_CANCELAMENTOS )
BEGIN 
SELECT TOP 1 @NFE_CANCELAMENTO = NFE_CANCELAMENTO FROM #NFE_CANCELAMENTOS
   INSERT INTO @RESULTADO 
   SELECT 
          CODIGO_RETORNO         
         ,DESCRICAO_RETORNO      
         ,CHAVE_NF_CANCELADA     
         ,PROTOCOLO_CANCELAMENTO 
         ,@NFE_CANCELAMENTO AS NFE_CANCELAMENTO 

 FROM DBO.NFE_MOTIVO_ERRO_CANCELAMENTO(@NFE_CANCELAMENTO)
           WHERE DESCRICAO_RETORNO IS NOT NULL
   DELETE FROM #NFE_CANCELAMENTOS WHERE NFE_CANCELAMENTO = @NFE_CANCELAMENTO
END 

  SELECT DISTINCT
         769791                    AS FORMULARIO_ORIGEM
        ,753289                    AS TAB_MASTER_ORIGEM
        ,A.NF_FATURAMENTO          AS REG_MASTER_ORIGEM

        ,A.NF_FATURAMENTO            AS NF
        ,A.EMPRESA                   AS EMPRESA
        ,A.ENTIDADE                  AS ENTIDADE
        ,A.NF_SERIE                  AS SERIE
        ,A.NF_NUMERO                 AS NF_NUMERO
        ,A.EMISSAO                   AS EMISSAO
        ,A.OPERACAO_FISCAL           AS OPERACAO
        ,B.XML_natOp_B04             AS DESCRICAO
        ,B.CHAVE_NFE                 AS CHAVE
        ,CASE WHEN E.STATUS = 4  --AND (F.CODIGO_RETORNO IN ('135','101','155' , '151'))
            THEN 'NOTA CANCELADA'
            ELSE 'NOTA AUTORIZADA'
          END                      AS STATUS_CANCELAMENTO,
		  I.TOTAL_GERAL              AS TOTAL_GERAL,
		  1                          AS CONT


    FROM NF_FATURAMENTO                              A WITH(NOLOCK)
    JOIN NFE_CABECALHO                               B WITH(NOLOCK) ON  B.XML_nNF_B08    = A.NF_NUMERO
                                                                   AND  B.XML_serie_B07  = A.NF_SERIE
                                                                   AND  B.EMPRESA        = A.EMPRESA
                                                                   
    LEFT 
    JOIN ESTOQUE_TRANSFERENCIAS_RECEBIMENTOS         C WITH(NOLOCK) ON  A.NF_NUMERO      = C.NF_NUMERO
	                                                               AND  A.ENTIDADE       = C.EMPRESA

    LEFT
    JOIN CANCELAMENTOS_NOTAS_FISCAIS                 D WITH(NOLOCK) ON  D.NF_NUMERO      = A.NF_NUMERO  
	                                                               AND  D.EMPRESA        = A.ENTIDADE  
    LEFT
    JOIN NFE_CANCELAMENTOS                           E WITH(NOLOCK) ON  E.FORMULARIO_ORIGEM    = D.FORMULARIO_ORIGEM AND             
                                                                        E.TAB_MASTER_ORIGEM    = D.TAB_MASTER_ORIGEM AND
                                                                        E.REG_MASTER_ORIGEM    = D.REG_MASTER_ORIGEM  AND
																		E.EMPRESA              = A.ENTIDADE
    LEFT
    JOIN @RESULTADO                                  F              ON  F.NFE_CANCELAMENTO     = E.NFE_CANCELAMENTO --B.CHAVE_NF_CANCELADA = IDNOTA
    LEFT
    JOIN RECEBIMENTOS_VOLUMES_NF                     G WITH(NOLOCK) ON  A.NF_NUMERO            = G.NF_NUMERO
    LEFT
    JOIN RECEBIMENTOS_VOLUMES                        H WITH(NOLOCK) ON  H.RECEBIMENTO          = G.RECEBIMENTO
	LEFT
	JOIN NF_FATURAMENTO_TOTAIS                       I WITH(NOLOCK) ON I.NF_FATURAMENTO        = A.NF_FATURAMENTO


    


                                                                 


    WHERE 1=1                                                                    
      AND (A.EMISSAO                >= @DATA_INI)
      AND (A.EMISSAO                <= @DATA_FIM)
      AND (C.ESTOQUE_RECEBIMENTO IS NULL AND H.RECEBIMENTO IS NULL)
      AND A.EMPRESA                 IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) )
      AND A.ENTIDADE                IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK) WHERE EMPRESA_USUARIA < 1000 )
      AND A.REG_MASTER_ORIGEM       IS NOT NULL        
      AND A.OPERACAO_FISCAL         NOT IN (156,140,166,148)    
      AND (A.NF_NUMERO = @NF_NUMERO OR @NF_NUMERO IS NULL)


UNION ALL


--BUSCAR INFO DA NF_FATURAMENTO

  SELECT DISTINCT
         769791                      AS FORMULARIO_ORIGEM
        ,753289                      AS TAB_MASTER_ORIGEM
        ,A.NF_FATURAMENTO            AS REG_MASTER_ORIGEM

        ,A.NF_FATURAMENTO             AS NF
        ,A.EMPRESA                    AS EMPRESA
        ,A.ENTIDADE                   AS ENTIDADE
        ,A.NF_SERIE                   AS SERIE
        ,A.NF_NUMERO                  AS NF_NUMERO
        ,A.EMISSAO                    AS EMISSAO
        ,A.OPERACAO_FISCAL            AS OPERACAO
        ,B.XML_natOp_B04              AS DESCRICAO
        ,B.CHAVE_NFE                  AS CHAVE
        ,CASE WHEN E.STATUS = 4  
            THEN 'NOTA CANCELADA'
            ELSE 'NOTA AUTORIZADA'
          END                        AS STATUS_CANCELAMENTO,
		  H.TOTAL_GERAL              AS TOTAL_GERAL,
		  1                          AS CONT



    FROM NF_FATURAMENTO                              A WITH(NOLOCK)
    LEFT
    JOIN NFE_CABECALHO                               B WITH(NOLOCK) ON  B.XML_nNF_B08          = A.NF_NUMERO
                                                                   AND  B.XML_serie_B07        = A.NF_SERIE
                                                                   AND  B.EMPRESA              = A.EMPRESA
                                                                                                  
    LEFT                                                                                       
    JOIN CANCELAMENTOS_NOTAS_FISCAIS                 D WITH(NOLOCK) ON  D.NF_NUMERO            = A.NF_NUMERO  
	                                                               AND  A.ENTIDADE             = D.EMPRESA 
																    
    LEFT
    JOIN NFE_CANCELAMENTOS                           E WITH(NOLOCK) ON  E.FORMULARIO_ORIGEM    = D.FORMULARIO_ORIGEM AND             
                                                                        E.TAB_MASTER_ORIGEM    = D.TAB_MASTER_ORIGEM AND
                                                                        E.REG_MASTER_ORIGEM    = D.REG_MASTER_ORIGEM AND
																		E.EMPRESA              = A.ENTIDADE
    LEFT
    JOIN @RESULTADO                                  F              ON  F.NFE_CANCELAMENTO     = E.NFE_CANCELAMENTO --B.CHAVE_NF_CANCELADA = IDNOTA
    LEFT
    JOIN NF_COMPRA                                   G WITH(NOLOCK) ON  G.NF_NUMERO            = A.NF_NUMERO
                                                                   AND  G.NF_SERIE             = A.NF_SERIE
                                                                   AND  G.EMPRESA              = A.ENTIDADE

	LEFT
	JOIN NF_FATURAMENTO_TOTAIS                       H WITH(NOLOCK) ON H.NF_FATURAMENTO = A.NF_FATURAMENTO
                                                                 


    WHERE 1=1                                                                    
      AND (A.EMISSAO                >= @DATA_INI)
      AND (A.EMISSAO                <= @DATA_FIM)
      AND A.EMPRESA                  IN ( SELECT EMPRESA_USUARIA FROM EMPRESAS_USUARIAS WITH(NOLOCK))   
      AND A.ENTIDADE                 = 1000  
      AND A.NF_FATURAMENTO            IS NOT NULL
      --AND A.REG_MASTER_ORIGEM       IS NOT NULL        
      AND G.SISTEMA_LEGADO           = 'N'
      AND G.RECEBIMENTO              IS NULL
      --AND G.OPERACAO_FISCAL         NOT IN (156,140,166,148)    
      AND (A.NF_NUMERO = @NF_NUMERO OR @NF_NUMERO IS NULL)


