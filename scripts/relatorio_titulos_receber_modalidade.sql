DECLARE @VENCIMENTO_INI          DATETIME
DECLARE @VENCIMENTO_FIM          DATETIME
DECLARE @EMPRESA_INI             NUMERIC
DECLARE @EMPRESA_FIM             NUMERIC
DECLARE @ENTIDADE_INI            NUMERIC
DECLARE @ENTIDADE_FIM            NUMERIC
DECLARE @EMPRESA_CONTABIL        NUMERIC
DECLARE @MODALIDADE              NUMERIC(15)    
DECLARE @VENDEDOR                NUMERIC(15)    
DECLARE @CARTOES                 VARCHAR(1) = 'T' 
DECLARE @MOSTRAR_PAGOS           VARCHAR(1) = 'N' 
DECLARE @COBRANCA_LOJA           VARCHAR(1) = 'N' 
DECLARE @COBRANCA_DISTRIBUIDORA  VARCHAR(1) = 'N' 
DECLARE @MOSTRAR_FUNCIONARIOS    VARCHAR(1) = 'N' 
DECLARE @GRUPO_CLIENTE           NUMERIC(15,0)
DECLARE @ORIGEM                  NUMERIC(15,0)

DECLARE @ENTIDADE_CONCENTRADORA NUMERIC(15) = CASE WHEN ISNUMERIC(:ENTIDADE_CONCENTRADORA) =  0 THEN NULL         ELSE :ENTIDADE_CONCENTRADORA END
                           
--SET  @VENCIMENTO_INI   = '01/01/2017'      
--SET  @VENCIMENTO_FIM   = '31/12/2017'
--SET  @EMPRESA_INI      = 1000
--SET  @EMPRESA_FIM      = 1000
--SET  @ENTIDADE_INI     = 1
--SET  @ENTIDADE_FIM     = 96159
--SET  @EMPRESA_CONTABIL = NULL
--SET  @MODALIDADE       = NULL
--SET  @MOSTRAR_PAGOS    = 'S'
----SET  @CARTOES          = NULL
--SET  @GRUPO_CLIENTE    = NULL
--DECLARE @ENTIDADE_CONCENTRADORA NUMERIC(15) = NULL



    SET @VENCIMENTO_INI          = CASE WHEN :VENCIMENTO_INI              = '' THEN '01/01/1900' ELSE :VENCIMENTO_INI     END 
    SET @VENCIMENTO_FIM          = CASE WHEN :VENCIMENTO_FIM              = '' THEN '31/12/2999' ELSE :VENCIMENTO_FIM     END 
    SET @EMPRESA_INI             = CASE WHEN ISNUMERIC(:EMPRESA_INI     ) = 0  THEN 0            ELSE :EMPRESA_INI        END 
    SET @EMPRESA_FIM             = CASE WHEN ISNUMERIC(:EMPRESA_FIM     ) = 0  THEN 99999        ELSE :EMPRESA_FIM        END 
    SET @ENTIDADE_INI            = CASE WHEN ISNUMERIC(:ENTIDADE_INI    ) = 0  THEN 0            ELSE :ENTIDADE_INI       END 
    SET @ENTIDADE_FIM            = CASE WHEN ISNUMERIC(:ENTIDADE_FIM    ) = 0  THEN 999999999999 ELSE :ENTIDADE_FIM       END 
    SET @EMPRESA_CONTABIL        = CASE WHEN ISNUMERIC(:EMPRESA_CONTABIL) = 0  THEN NULL         ELSE :EMPRESA_CONTABIL   END
    SET @MODALIDADE              = CASE WHEN ISNUMERIC(:MODALIDADE      ) = 0  THEN NULL         ELSE :MODALIDADE         END
	SET @ORIGEM                  = CASE WHEN ISNUMERIC(:ORIGEM      )     = 0  THEN NULL         ELSE :ORIGEM             END
    SET @VENDEDOR                = CASE WHEN ISNUMERIC(:VENDEDOR        ) = 0  THEN NULL         ELSE :VENDEDOR           END
    SET @GRUPO_CLIENTE           = CASE WHEN ISNUMERIC(:GRUPO_CLIENTE   ) = 0  THEN NULL         ELSE :GRUPO_CLIENTE      END
    SET @CARTOES                 = CASE WHEN :CARTOES NOT LIKE ''              THEN :CARTOES     ELSE 'T'                 END 
    SET @MOSTRAR_PAGOS           = CASE WHEN :MOSTRAR_PAGOS           NOT LIKE ''        THEN :MOSTRAR_PAGOS           ELSE 'N' END
    SET @COBRANCA_LOJA           = CASE WHEN :COBRANCA_LOJA           NOT LIKE ''        THEN :COBRANCA_LOJA           ELSE 'N' END
    SET @MOSTRAR_FUNCIONARIOS    = CASE WHEN :MOSTRAR_FUNCIONARIOS    NOT LIKE ''        THEN :MOSTRAR_FUNCIONARIOS    ELSE 'N' END
    SET @COBRANCA_DISTRIBUIDORA  = CASE WHEN :COBRANCA_DISTRIBUIDORA  NOT LIKE ''        THEN :COBRANCA_DISTRIBUIDORA  ELSE 'N' END


 
   SELECT 
          @VENCIMENTO_INI         AS VENCIMENTO_INICIAL , 
          @VENCIMENTO_FIM         AS VENCIMENTO_FINAL ,  
          @EMPRESA_INI            AS EMPRESA_INICIAL ,
          @EMPRESA_FIM            AS EMPRESA_FINAL ,
          @ENTIDADE_INI           AS ENTIDADE_INICIAL , 
          @ENTIDADE_FIM           AS ENTIDADE_FINAL ,
          A.TITULO_RECEBER        AS TITULO_RECEBER ,
          A.EMPRESA               AS EMPRESA ,    
          D.EMPRESA_USUARIA_NOME  AS EMPRESA_NOME,       
          A.TITULO                AS TITULO , 
          A.EMISSAO               AS EMISSAO , 
          A.MOVIMENTO             AS MOVIMENTO , 
          A.VENCIMENTO            AS VENCIMENTO , 
          cast(B.ENTIDADE AS VARCHAR) + ' - ' +
          B.NOME                      + ' - ' +
          B.INSCRICAO_FEDERAL  AS ENTIDADE ,
          A.VALOR                 AS VALOR_ORIGINAL ,
          C.SALDO                 AS SALDO ,
          D.EMPRESA_CONTABIL      AS EMPRESA_CONTABIL,     
          D.EMPRESA_CONTABIL_NOME AS EMPRESA_CONTABIL_NOME,
          E.DESCRICAO             AS MODALIDADE,
          A.ENTIDADE              AS ENTIDADE_FILTRO,
          CAST ( F.VENDEDOR AS VARCHAR)  + ' - '+      H.NOME AS VENDEDOR,
          F.VENDEDOR              AS COD_VENDEDOR,
          ISNULL(I.NUMID,0)                  AS NUMID,
          ISNULL(I.FORMULARIO,'IMPORTACAO')  AS ORIGEM,  --formulario origem 

          J.TIPO_ENDERECO + ' ' + J.ENDERECO + ', ' + CAST(J.NUMERO AS VARCHAR) + ISNULL(' - ' + J.COMPLEMENTO,'') + J.BAIRRO + ' ' + J.CIDADE + '/' + J.ESTADO
                              AS ENDERECO,
          '('+CAST(ISNULL( K.DDD , K1.DDD ) AS VARCHAR)+') ' + CAST( ISNULL( K.NUMERO , K1.NUMERO ) AS VARCHAR) + ' - ' + COALESCE( K.CONTATO , K1.CONTATO , '')
                              AS TELEFONE,
          CASE WHEN ( DATEDIFF(D, GETDATE(), A.VENCIMENTO ) ) < 0 THEN ( DATEDIFF(D, GETDATE(), A.VENCIMENTO ) ) * -1 ELSE 0 END
                              AS DIAS_ATRASO,
          F.OPERACAO_FISCAL,
          A.PREGAO,
          A.EMPENHO,
          B.CLASSIFICACAO_CLIENTE AS GRUPO_CLIENTE,
          CASE WHEN A.NF_LIQUIDADA = 'S' THEN 'LIQUIDADA' ELSE '' END AS NF_LIQUIDADA,
          B.NOME,
          O.DATA_RECEBIMENTO,
          CASE WHEN C.SALDO <= 0 
               THEN A.VALOR                 
               ELSE 0.00
          END                     AS VALOR_RECEBIDO

     FROM TITULOS_RECEBER             A (NOLOCK) 
LEFT JOIN ENTIDADES                   B (NOLOCK) ON A.ENTIDADE                = B.ENTIDADE
LEFT JOIN TITULOS_RECEBER_SALDO       C (NOLOCK) ON A.TITULO_RECEBER          = C.TITULO_RECEBER
     JOIN FN_EMPRESAS_USUARIAS(@EMPRESA_INI,@EMPRESA_FIM,@EMPRESA_CONTABIL) 
                                      D             ON D.EMPRESA_USUARIA      = A.EMPRESA
LEFT JOIN MODALIDADES_TITULOS         E WITH(NOLOCK)ON E.MODALIDADE           = A.MODALIDADE
LEFT JOIN (
            SELECT DISTINCT ENTIDADE FROM BANDEIRAS_CARTOES WITH(NOLOCK)
          )                           G              ON G.ENTIDADE            = A.ENTIDADE 
LEFT JOIN NF_FATURAMENTO              F WITH(NOLOCK) ON A.TAB_MASTER_ORIGEM   = 753289
                                                    AND F.NF_FATURAMENTO      = A.REG_MASTER_ORIGEM
LEFT JOIN VENDEDORES                  H WITH(NOLOCK) ON H.VENDEDOR            = F.VENDEDOR 
LEFT JOIN FORMULARIOS                 I WITH(NOLOCK) ON I.NUMID               = A.FORMULARIO_ORIGEM   --formulario origem 
LEFT JOIN ENDERECOS                   J WITH(NOLOCK) ON J.ENTIDADE            = A.ENTIDADE
OUTER APPLY (
             SELECT TOP 1 ENTIDADE, DDD AS DDD, NUMERO AS NUMERO , CONTATO
             FROM TELEFONES XX WITH(NOLOCK)
             WHERE TIPO_TELEFONE       = 6
               AND XX.ENTIDADE = A.ENTIDADE
          )                           K
OUTER APPLY (
             SELECT TOP 1 ENTIDADE, DDD AS DDD, NUMERO AS NUMERO , CONTATO
             FROM TELEFONES K2 WITH(NOLOCK)
             WHERE K2.ENTIDADE = A.ENTIDADE
          )                           K1
                                                    
LEFT JOIN NF_FATURAMENTO_OBSERVACOES  L WITH(NOLOCK) ON L.NF_FATURAMENTO      = F.NF_FATURAMENTO
LEFT JOIN LIQUIDACAO_NOTAS_DETALHE    M WITH(NOLOCK) ON M.NF_FATURAMENTO      = F.NF_FATURAMENTO
LEFT JOIN FUNCIONARIOS                N WITH(NOLOCK) ON N.ENTIDADE            = A.ENTIDADE
LEFT JOIN (
            SELECT 
                  A.TITULO_RECEBER   AS TITULO_RECEBER,
                  MAX(A.DATA)        AS DATA_RECEBIMENTO
            
              FROM TITULOS_RECEBER_TRANSACOES A WITH(NOLOCK)
              JOIN TITULOS_RECEBER_SALDO      B WITH(NOLOCK) ON B.TITULO_RECEBER = A.TITULO_RECEBER
            
                 WHERE 1=1
                   AND A.DEBITO > 0
                   AND B.SITUACAO_TITULO = 2
            GROUP BY A.TITULO_RECEBER
          )                           O                      ON O.TITULO_RECEBER = A.TITULO_RECEBER

    WHERE 1=1                                                                                                                                                  AND                                                                   
            ISNULL ( C.SITUACAO_TITULO , 0 ) < (CASE WHEN @MOSTRAR_PAGOS = 'S' THEN 3 ELSE 2 END)                                                              AND
            A.VENCIMENTO >= @VENCIMENTO_INI   																											       AND
            A.VENCIMENTO <= @VENCIMENTO_FIM   																											       AND
            A.EMPRESA    >= @EMPRESA_INI                        									  													       AND
            A.EMPRESA    <= @EMPRESA_FIM      																											       AND
            A.ENTIDADE   >= @ENTIDADE_INI     																											       AND
            A.ENTIDADE   <= @ENTIDADE_FIM       																										       AND
           (A.MODALIDADE             = @MODALIDADE OR @MODALIDADE IS NULL)  																			       AND
           (B.CLASSIFICACAO_CLIENTE  = @GRUPO_CLIENTE OR @GRUPO_CLIENTE IS NULL)  																		       AND
           (F.VENDEDOR                =  @VENDEDOR   OR @VENDEDOR   IS NULL)  																				   AND
		   (A.FORMULARIO_ORIGEM       = @ORIGEM   OR @ORIGEM IS NULL)                                                                                          AND
           ( CASE WHEN G.ENTIDADE IS NULL THEN 'N' ELSE 'S' END = @CARTOES               OR @CARTOES               = 'T'   )            					   AND
           ( CASE WHEN N.ENTIDADE IS NULL THEN 'N' ELSE 'S' END = @MOSTRAR_FUNCIONARIOS  OR @MOSTRAR_FUNCIONARIOS  = 'S'   )            					   AND
           (B.CONCENTRADOR = @ENTIDADE_CONCENTRADORA       OR           @ENTIDADE_CONCENTRADORA IS NULL         )			        					       AND																												   
           ( CASE WHEN @COBRANCA_LOJA            = 'S' THEN ISNULL(I.NUMID,0) ELSE NULL END NOT IN  ( 176897, 61, 70 ) OR @COBRANCA_LOJA          <> 'S'   )   AND         
           ( CASE WHEN @COBRANCA_DISTRIBUIDORA   = 'S' THEN ISNULL(I.NUMID,0) ELSE NULL END NOT IN  ( 176897, 72, 70 ) OR @COBRANCA_DISTRIBUIDORA <> 'S'   )          
                                                                       
 ORDER BY D.EMPRESA_CONTABIL, A.EMPRESA, B.NOME, CAST(B.ENTIDADE AS VARCHAR), A.VENCIMENTO