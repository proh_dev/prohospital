CREATE FUNCTION [dbo].[CONCATENA_PEDIDOS] ( @RECEBIMENTO NUMERIC(15) ) RETURNS VARCHAR(4000) AS  
  
BEGIN  
----------------------------------------------------------------------  
--ROTINA PARA CONCATENACAO DOS PEDIDOS DO RECEBIMENTO  
----------------------------------------------------------------------  
DECLARE @C_DESCRICAO              VARCHAR(100)  
DECLARE @C_CONCATENCAO            VARCHAR(4000)  
  
--------------------------------------------------  
--DECLARACAO DO CURSOR CONCATENA_ARTIGOS (2)--   
--------------------------------------------------  
DECLARE CURSOR_CONCATENA_PEDIDOS CURSOR SCROLL FOR  
  
SELECT DISTINCT   
       PEDIDO_COMPRA  
  FROM RECEBIMENTOS_VOLUMES_NF WITH(NOLOCK)  
 WHERE RECEBIMENTO = @RECEBIMENTO  
  
ORDER BY PEDIDO_COMPRA  
  
OPEN CURSOR_CONCATENA_PEDIDOS  
FETCH FIRST FROM CURSOR_CONCATENA_PEDIDOS  
    INTO @C_DESCRICAO  
  
   SET @C_CONCATENCAO = ''  
  
WHILE (@@FETCH_STATUS = 0)  
  
  
  BEGIN  
  
       SET @C_CONCATENCAO = @C_DESCRICAO + ' / ' + @C_CONCATENCAO  
  
  
FETCH NEXT FROM CURSOR_CONCATENA_PEDIDOS  
INTO @C_DESCRICAO  
  
END  
  
CLOSE CURSOR_CONCATENA_PEDIDOS  
DEALLOCATE CURSOR_CONCATENA_PEDIDOS  
  
     RETURN ( @C_CONCATENCAO )  
  
END  