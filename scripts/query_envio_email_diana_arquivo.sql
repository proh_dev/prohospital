ALTER PROCEDURE [dbo].[ENVIO_EMAIL_RETORNO_COBRANCA] ( @MOVIMENTO DATE) AS                      
BEGIN                      
                
SET NOCOUNT ON      

--DECLARE @MOVIMENTO DATE 
--SET @MOVIMENTO = '24/04/2018'
	                    
DECLARE @TITULO_EMAIL                       VARCHAR(50)  
DECLARE @EMAIL                              VARCHAR(250) 
DECLARE @CONTEUDO_EMAIL                     VARCHAR(250)
DECLARE @tableHTML                          NVARCHAR(MAX) 
DECLARE @DIA                                DATE
  
IF OBJECT_ID('TEMPDB..#DATA_HOJE') IS NOT NULL DROP TABLE #DATA_HOJE

SELECT @MOVIMENTO AS HOJE
INTO #DATA_HOJE

SELECT @TITULO_EMAIL   = 'Envio de arquivo atrasado  ' ,
	   @CONTEUDO_EMAIL = 'Envio de arquivo Bradesco N�O realizado ',
	   @EMAIL          =  C.EMAIL
	  
      
  FROM #DATA_HOJE                      A WITH(NOLOCK)
	  JOIN BRADESCO_COBRANCAS_RETORNOS B WITH(NOLOCK) ON B.DATA_HORA = A.HOJE
	  JOIN USUARIOS                    C WITH(NOLOCK) ON B.USUARIO_LOGADO = C.USUARIO 
	  LEFT 						       
	  JOIN FERIADOS                    D WITH(NOLOCK) ON D.DATA_HORA      = B.DATA_HORA

  WHERE 1=1
    AND D.FERIADO IS NULL
	AND B.BRADESCO_COBRANCA_RETORNO IS NULL
	AND (DATEPART(DW,A.HOJE ))  <> 1
	AND CAST(A.HOJE AS DATE) = CAST(@MOVIMENTO AS DATE)
	
	
		
  
SET @tableHTML =  
    N'<style type="text/css">  
              .formato {  
               font-family: Verdana, Geneva, sans-serif;  
             font-size: 14px;  
              }  
              .alinhameto {  
               text-align: center;
              }  
              </style>'                    +  
    N'<H1 class="formato" Envio de arquivo atrasado  <br> <br>  
	  <br>'+ @CONTEUDO_EMAIL                                      + '   <br> <br>  
 
  
  </H1>'
  

IF LEN(@EMAIL) > 3

BEGIN
EXEC MSDB.DBO.SP_SEND_DBMAIL  
  
  @RECIPIENTS   = @EMAIL,  
  @SUBJECT      = @TITULO_EMAIL ,  
  @BODY         = @tableHTML,  
  @BODY_FORMAT  = 'HTML';   
END
  
END 