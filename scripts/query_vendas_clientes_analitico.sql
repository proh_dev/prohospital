DECLARE @MOVIMENTO_INICIAL DATE        = :MOVIMENTO_INICIAL 
DECLARE @MOVIMENTO_FINAL   DATE        = :MOVIMENTO_FINAL                                
DECLARE @ENTIDADE          NUMERIC(15) = CASE WHEN ISNUMERIC(:ENTIDADE) = 1 THEN :ENTIDADE ELSE NULL END 

--DECLARE @MOVIMENTO_INICIAL DATE       
--DECLARE @MOVIMENTO_FINAL   DATE       
--DECLARE @ENTIDADE          NUMERIC(15)     

--SET @MOVIMENTO_INICIAL= '01/12/2017'
--SET @MOVIMENTO_FINAL  = '01/12/2018'
--SET @ENTIDADE         = 141220    
            
SELECT                                            
    B.NOME + ' - ' + RTRIM(CONVERT(CHAR, B.ENTIDADE)) + ' - ' +  B.INSCRICAO_FEDERAL AS NOME         ,
    C.NOME                                                                           AS EMPRESA      ,
    A.MOVIMENTO                                                                      AS DATA         ,
    A.CAIXA                                                                          AS DOCUMENTO    ,
    A.PRODUTO                                                                        AS PRODUTO      ,
    D.DESCRICAO_REDUZIDA                                                             AS DESCRICAO    ,
    A.QUANTIDADE                                                                     AS QUANTIDADE   ,
    A.VENDA_BRUTA                                                                    AS PRECO_BRUTO  ,
    A.DESCONTO                                                                       AS DESCONTO     ,
    CONVERT(NUMERIC(15,2), ((A.DESCONTO * 100) / A.VENDA_BRUTA))                     AS PERC_DESCONTO,
    (A.VENDA_BRUTA - A.DESCONTO)                                                     AS PRECO_LIQUIDO,
	( A.VENDA_LIQUIDA/
	A.QUANTIDADE      )                                                              AS VALOR_UNI,
    A.VENDA_LIQUIDA                                                                  AS TOTAL,
	E.DESCRICAO                                                                      AS MARCA,
	G.DESCRICAO                                                                      AS GRUPO_MARCA,
	H.CONCENTRADOR                                                                   AS CONCENTRADOR,
	J.DESCRICAO                                                                      AS CLASSIF_CLIENTE,
	I.ESTADO                                                                         AS ESTADO

FROM
    VENDAS_ANALITICAS                 A WITH(NOLOCK)
    JOIN ENTIDADES                    B WITH(NOLOCK) ON A.CLIENTE = B.ENTIDADE
    JOIN EMPRESAS_USUARIAS            C WITH(NOLOCK) ON A.EMPRESA = C.EMPRESA_USUARIA
    JOIN PRODUTOS                     D WITH(NOLOCK) ON A.PRODUTO = D.PRODUTO
    --JOIN UDF_USUARIO_EMPRESAS(@LOGIN) E              ON A.EMPRESA = E.EMPRESA_USUARIA
	JOIN MARCAS                       E WITH(NOLOCK) ON D.MARCA                         = E.MARCA
	LEFT						     
    JOIN GRUPOS_MARCAS_DETALHE        F WITH(NOLOCK) ON E.MARCA                         = F.MARCA
    LEFT                                                                                    
    JOIN GRUPOS_MARCAS                G WITH(NOLOCK) ON G.GRUPO_MARCA                   = F.GRUPO_MARCA
	LEFT
	JOIN PESSOAS_JURIDICAS            H WITH(NOLOCK) ON H.ENTIDADE                      = A.CLIENTE
	LEFT
	JOIN ENDERECOS                    I WITH(NOLOCK) ON I.ENTIDADE                      = A.CLIENTE
	LEFT
	JOIN CLASSIFICACOES_CLIENTES      J WITH(NOLOCK) ON J.CLASSIFICACAO_CLIENTE         = H.CLASSIFICACAO_CLIENTE
WHERE
    A.MOVIMENTO BETWEEN @MOVIMENTO_INICIAL AND @MOVIMENTO_FINAL
    AND (A.CLIENTE = @ENTIDADE OR @ENTIDADE IS NULL)    