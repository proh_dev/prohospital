DECLARE @PEDIDO_VENDA	NUMERIC(15)
DECLARE @ENTIDADE	NUMERIC(15)
-----------------------------------
DECLARE @MOVIMENTO	DATE
DECLARE @MOEDA		NUMERIC(15)
DECLARE @ENTER		CHAR    (2)
-----------------------------------

DECLARE @PEDIDOS_VENDAS TABLE 
(MOVIMENTO DATE , MOEDA NUMERIC(15))

----------------/-/----------------

SET @PEDIDO_VENDA = :PEDIDO_VENDA
SET @ENTIDADE     = :ENTIDADE
SET @MOVIMENTO    = GETDATE()
SET @ENTER        = CHAR(13) + CHAR(10)

-----------------------------------

SELECT 
	@MOEDA = A.MOEDA
FROM
	PEDIDOS_VENDAS_PARAMETROS_ENTIDADES	A WITH(NOLOCK)
WHERE
	A.ENTIDADE = @ENTIDADE

-----------------------------------

INSERT INTO @PEDIDOS_VENDAS(MOVIMENTO,MOEDA) VALUES(@MOVIMENTO, @MOEDA)

-----------------------------------

SELECT TOP 1
 'N�o � permitido alterar a Entidade ap�s incluir um produto no Pedido.'
FROM (
        SELECT TOP 1 1 A
        FROM 
            PEDIDOS_VENDAS                  A WITH(NOLOCK)
            JOIN PEDIDOS_VENDAS_PRODUTOS    B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
        WHERE 
            A.PEDIDO_VENDA = @PEDIDO_VENDA
        UNION ALL
        SELECT TOP 1 1
        FROM 
            PEDIDOS_VENDAS                          A WITH(NOLOCK)
            JOIN PEDIDOS_VENDAS_PRODUTOS_COMPLETA   B WITH(NOLOCK) ON A.PEDIDO_VENDA = B.PEDIDO_VENDA
        WHERE 
            A.PEDIDO_VENDA = @PEDIDO_VENDA
    )A

UNION ALL

   SELECT 'Entidade sem Endere�o Cadastrado!!! Revise o Cadastro Antes de Registrar o Pedido de Venda!!'
     FROM ENTIDADES      A WITH (NOLOCK)
LEFT JOIN ENDERECOS      B WITH (NOLOCK) ON B.ENTIDADE = A.ENTIDADE
    WHERE A.ENTIDADE = @ENTIDADE 
	  AND B.ENTIDADE IS NULL

UNION ALL

SELECT 
    'Alerta:' +
    'Moeda padr�o do cliente ('+E.NOME+') n�o teve sua cota��o di�ria estabelecida.' + @ENTER +
    'Para prosseguir com o pedido, favor realizar a cota��o di�ria.'
FROM 
	@PEDIDOS_VENDAS C 
	LEFT JOIN MOEDAS_COTACOES_DIARIAS_VALORES   B WITH (NOLOCK) ON CONVERT(DATE,C.MOVIMENTO) = B.DATA_COTACAO
                                                    AND C.MOEDA = B.MOEDA
	JOIN PEDIDOS_VENDAS_PARAMETROS_ENTIDADES    D WITH (NOLOCK) ON D.ENTIDADE = @ENTIDADE
	JOIN MOEDAS	   			    E WITH (NOLOCK) ON E.MOEDA = D.MOEDA
WHERE 
    B.MOEDA_COTACAO_VALOR IS NULL
--UNION ALL

---- debug
--SELECT 
--	'"'+ CONVERT(VARCHAR,B.ENTIDADE) + ' - ' + B.NOME + '"' + @ENTER +
--	'Se encontra em estado bloqueado' +
--	CASE WHEN A.MOTIVO IS NOT NULL
--	     THEN @ENTER + 'Motivo: ' + A.MOTIVO
--	     ELSE '.'
--	END
--FROM 
--	VIEW_STATUS_CLIENTE A WITH(NOLOCK)
--	JOIN ENTIDADES		B WITH(NOLOCK) ON B.ENTIDADE = A.ENTIDADE
--WHERE
--	A.ENTIDADE = @ENTIDADE
--AND A.TIPO = 1 -- BLOQUEADO
--AND 1 = 1 --Retirar ap�s realizar todos os cadastros     
UNION ALL

SELECT
	'"'+ CONVERT(VARCHAR,A.ENTIDADE) + ' - ' + A.NOME + '"' + @ENTER +
	'N�o tem limite cadastrado.'
FROM
	 ENTIDADES	       A WITH(NOLOCK)
LEFT
JOIN EMPRESAS_USUARIAS C WITH(NOLOCK)ON C.ENTIDADE = A.ENTIDADE
	 CROSS APPLY DBO.FN_ENTIDADES_LIMITES_SALDOS (@ENTIDADE) B
WHERE
	A.ENTIDADE = @ENTIDADE
AND
	ISNULL(B.LIMITE_TOTAL,0) <=0
AND C.EMPRESA_USUARIA IS NULL
AND 1= 2	--Retirar ap�s o cadastro em an�lise de cr�dito   
UNION ALL

SELECT
	'"'+ CONVERT(VARCHAR,A.ENTIDADE) + ' - ' + A.NOME + '"' + @ENTER +
	'N�o tem Saldo suficiente: R$' + CONVERT(VARCHAR,B.SALDO_TOTAL)
FROM
	 ENTIDADES	 A WITH(NOLOCK)
LEFT
JOIN EMPRESAS_USUARIAS C WITH(NOLOCK)ON C.ENTIDADE = A.ENTIDADE
	 CROSS APPLY DBO.FN_ENTIDADES_LIMITES_SALDOS (@ENTIDADE) B
WHERE
	A.ENTIDADE = @ENTIDADE
AND	ISNULL(B.SALDO_TOTAL,0) <=0
AND C.EMPRESA_USUARIA IS NULL
AND 1 = 2 --Retirar ap�s o cadastro em an�lise de cr�dito

UNION ALL

SELECT TOP 1
	'Vendedor desse cliente est� inativo/bloqueado! Favor atualize o cadastro!' 

FROM VENDEDORES_ENTIDADES	 A WITH(NOLOCK)
JOIN VENDEDORES              B WITH(NOLOCK) ON B.VENDEDOR  = A.VENDEDOR

WHERE 1=1
  AND A.ENTIDADE = @ENTIDADE
  AND B.CADASTRO_ATIVO = 'N'

UNION ALL

--RELACIONAR DUAS VALIDA��ES SEGUINTES COM CLASSIFICACOES CLIENTES  CONSIDERANDO CAMPO RECEM CRIADO
 
SELECT  'Alerta Cliente ainda n�o possui Email Cadastrado para Envio de Arquivo XML'

	FROM ENTIDADES                     A WITH(NOLOCK)
	LEFT
	JOIN EMAIL                         B WITH(NOLOCK) ON A.ENTIDADE = B.ENTIDADE
	
	
	WHERE A.ENTIDADE = @ENTIDADE
	  AND (B.EMAIL IS NULL 
	        OR( (SELECT COUNT(B.EMAIL)
	                            FROM  ENTIDADES  A WITH(NOLOCK)								
	                            JOIN EMAIL       B WITH(NOLOCK) ON @ENTIDADE = B.ENTIDADE
								WHERE B.NFE = 'S' 
								  AND @ENTIDADE = A.ENTIDADE 
								 
								  ) = 0 
		       OR(SELECT COUNT(B.EMAIL)
	                            FROM  ENTIDADES  A WITH(NOLOCK)
								
	                            JOIN EMAIL B WITH(NOLOCK) ON @ENTIDADE = B.ENTIDADE
								WHERE B.EMAIL_COBRANCA = 'S'
								 AND @ENTIDADE = A.ENTIDADE
								  ) = 0   )     
									
									
									)




      AND A.CLASSIFICACAO_CLIENTE IN (1,12)


UNION ALL


SELECT 'ALERTA: Cliente Possui os Seguintes Titulos: ' + CAST(B.TITULO_RECEBER AS VARCHAR) + '  Vencidos � Mais de 5 dias'
  FROM ENTIDADES                        A WITH(NOLOCK) 
  JOIN TITULOS_RECEBER                  B WITH(NOLOCK) ON A.ENTIDADE        = B.ENTIDADE
  JOIN TITULOS_RECEBER_SALDO            C WITH(NOLOCK) ON B.TITULO_RECEBER  = C.TITULO_RECEBER   
 WHERE A.ENTIDADE = @ENTIDADE
   AND ISNULL ( C.SITUACAO_TITULO , 0 ) < 2
   AND B.VENCIMENTO      <  GETDATE() - 5
   AND A.CLASSIFICACAO_CLIENTE = 12

UNION ALL

   SELECT 'Entidade sem Grupo de Cliente informado!!! Revise o Cadastro Antes de Registrar o Pedido de Venda!!'
     FROM ENTIDADES      A WITH (NOLOCK)
    WHERE A.ENTIDADE = @ENTIDADE 
	  AND A.CLASSIFICACAO_CLIENTE IS NULL